// diotestDlg.cpp : implementation file
//

#include "stdafx.h"
#include "diotest.h"
#include "diotestDlg.h"
#include "dio24.h"
#include "conio.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDiotestDlg dialog

CDiotestDlg::CDiotestDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDiotestDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDiotestDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CDiotestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDiotestDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CDiotestDlg, CDialog)
	//{{AFX_MSG_MAP(CDiotestDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_ADIR, OnAdir)
	ON_BN_CLICKED(IDC_BDIR, OnBdir)
	ON_BN_CLICKED(IDC_CHDIR, OnChdir)
	ON_BN_CLICKED(IDC_CLDIR, OnCldir)
	ON_BN_CLICKED(IDC_UPDATE, OnUpdate)
	ON_BN_CLICKED(IDC_PA0, OnPa0)
	ON_BN_CLICKED(IDC_PA1, OnPa1)
	ON_BN_CLICKED(IDC_PA2, OnPa2)
	ON_BN_CLICKED(IDC_PA3, OnPa3)
	ON_BN_CLICKED(IDC_PA4, OnPa4)
	ON_BN_CLICKED(IDC_PA5, OnPa5)
	ON_BN_CLICKED(IDC_PA6, OnPa6)
	ON_BN_CLICKED(IDC_PA7, OnPa7)
	ON_BN_CLICKED(IDC_PB0, OnPb0)
	ON_BN_CLICKED(IDC_PB1, OnPb1)
	ON_BN_CLICKED(IDC_PB2, OnPb2)
	ON_BN_CLICKED(IDC_PB3, OnPb3)
	ON_BN_CLICKED(IDC_PB4, OnPb4)
	ON_BN_CLICKED(IDC_PB5, OnPb5)
	ON_BN_CLICKED(IDC_PB6, OnPb6)
	ON_BN_CLICKED(IDC_PB7, OnPb7)
	ON_BN_CLICKED(IDC_PC0, OnPc0)
	ON_BN_CLICKED(IDC_PC1, OnPc1)
	ON_BN_CLICKED(IDC_PC2, OnPc2)
	ON_BN_CLICKED(IDC_PC3, OnPc3)
	ON_BN_CLICKED(IDC_PC4, OnPc4)
	ON_BN_CLICKED(IDC_PC5, OnPc5)
	ON_BN_CLICKED(IDC_PC6, OnPc6)
	ON_BN_CLICKED(IDC_PC7, OnPc7)
	ON_EN_KILLFOCUS(IDC_BOARDNUM, OnKillfocusBoardnum)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

DWORD ctrl_val = 0xff;

/////////////////////////////////////////////////////////////////////////////
// CDiotestDlg message handlers

BOOL CDiotestDlg::OnInitDialog()
{
DWORD a,b,c;

	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here

	//read each port and set the port checkboxes accordingly

_outp(BASE_ADDRESS+REG_CTRL,0xff);//set all input
a = _inp(BASE_ADDRESS+REG_A);
b = _inp(BASE_ADDRESS+REG_B);
c = _inp(BASE_ADDRESS+REG_C);

//set grayed and checks on each port.
//gray all (all inputs)
CWnd *cwnd;

cwnd = GetDlgItem(IDC_BOARDNUM);
cwnd->SetWindowText("0");

cwnd = GetDlgItem(IDC_ADIR);
CheckDlgButton(IDC_ADIR,0);
cwnd->SetWindowText("A Input");

cwnd = GetDlgItem(IDC_BDIR);
CheckDlgButton(IDC_BDIR,0);
cwnd->SetWindowText("B Input");

cwnd = GetDlgItem(IDC_CLDIR);
CheckDlgButton(IDC_CLDIR,0);
cwnd->SetWindowText("C Low Input");

cwnd = GetDlgItem(IDC_CHDIR);
CheckDlgButton(IDC_CHDIR,0);
cwnd->SetWindowText("C High Input");


cwnd = GetDlgItem(IDC_PA0);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PA0,a&1);
cwnd = GetDlgItem(IDC_PA1);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PA1,(a>>1)&1);
cwnd = GetDlgItem(IDC_PA2);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PA2,(a>>2)&1);
cwnd = GetDlgItem(IDC_PA3);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PA3,(a>>3)&1);
cwnd = GetDlgItem(IDC_PA4);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PA4,(a>>4)&1);
cwnd = GetDlgItem(IDC_PA5);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PA5,(a>>5)&1);
cwnd = GetDlgItem(IDC_PA6);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PA6,(a>>6)&1);
cwnd = GetDlgItem(IDC_PA7);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PA7,(a>>7)&1);

cwnd = GetDlgItem(IDC_PB0);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PB0,(b)&1);
cwnd = GetDlgItem(IDC_PB1);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PB1,(b>>1)&1);
cwnd = GetDlgItem(IDC_PB2);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PB2,(b>>2)&1);
cwnd = GetDlgItem(IDC_PB3);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PB3,(b>>3)&1);
cwnd = GetDlgItem(IDC_PB4);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PB4,(b>>4)&1);
cwnd = GetDlgItem(IDC_PB5);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PB5,(b>>5)&1);
cwnd = GetDlgItem(IDC_PB6);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PB6,(b>>6)&1);
cwnd = GetDlgItem(IDC_PB7);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PB7,(b>>7)&1);

cwnd = GetDlgItem(IDC_PC0);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PC0,(c)&1);
cwnd = GetDlgItem(IDC_PC1);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PC1,(c>>1)&1);
cwnd = GetDlgItem(IDC_PC2);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PC2,(c>>2)&1);
cwnd = GetDlgItem(IDC_PC3);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PC3,(c>>3)&1);
cwnd = GetDlgItem(IDC_PC4);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PC4,(c>>4)&1);
cwnd = GetDlgItem(IDC_PC5);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PC5,(c>>5)&1);
cwnd = GetDlgItem(IDC_PC6);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PC6,(c>>6)&1);
cwnd = GetDlgItem(IDC_PC7);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PC7,(c>>7)&1);








	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CDiotestDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CDiotestDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CDiotestDlg::OnAdir() 
{
DWORD a;
DWORD bstate;
CWnd *cwnd;

	// TODO: Add your control notification handler code here
bstate = SendDlgItemMessage(IDC_ADIR,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED)
{
ctrl_val = (ctrl_val&0xef)|0x10;
_outp(BASE_ADDRESS+REG_CTRL,ctrl_val);


cwnd = GetDlgItem(IDC_ADIR);
cwnd->SetWindowText("A Input");
	
a = _inp(BASE_ADDRESS+REG_A);

cwnd = GetDlgItem(IDC_PA0);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PA0,a&1);
cwnd = GetDlgItem(IDC_PA1);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PA1,(a>>1)&1);
cwnd = GetDlgItem(IDC_PA2);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PA2,(a>>2)&1);
cwnd = GetDlgItem(IDC_PA3);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PA3,(a>>3)&1);
cwnd = GetDlgItem(IDC_PA4);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PA4,(a>>4)&1);
cwnd = GetDlgItem(IDC_PA5);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PA5,(a>>5)&1);
cwnd = GetDlgItem(IDC_PA6);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PA6,(a>>6)&1);
cwnd = GetDlgItem(IDC_PA7);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PA7,(a>>7)&1);


}
else
{
//set to output
ctrl_val = (ctrl_val&0xef);
_outp(BASE_ADDRESS+REG_CTRL,ctrl_val);

cwnd = GetDlgItem(IDC_ADIR);
cwnd->SetWindowText("A Output");
	

cwnd = GetDlgItem(IDC_PA0);
cwnd->EnableWindow(TRUE);
cwnd = GetDlgItem(IDC_PA1);
cwnd->EnableWindow(TRUE);
cwnd = GetDlgItem(IDC_PA2);
cwnd->EnableWindow(TRUE);
cwnd = GetDlgItem(IDC_PA3);
cwnd->EnableWindow(TRUE);
cwnd = GetDlgItem(IDC_PA4);
cwnd->EnableWindow(TRUE);
cwnd = GetDlgItem(IDC_PA5);
cwnd->EnableWindow(TRUE);
cwnd = GetDlgItem(IDC_PA6);
cwnd->EnableWindow(TRUE);
cwnd = GetDlgItem(IDC_PA7);
cwnd->EnableWindow(TRUE);
a = 0;
bstate = SendDlgItemMessage(IDC_PA0,BM_GETSTATE,0,0L)&3;
if(bstate==BST_CHECKED) a = a | 1;
bstate = SendDlgItemMessage(IDC_PA1,BM_GETSTATE,0,0L)&3;
if(bstate==BST_CHECKED) a = a | 2;
bstate = SendDlgItemMessage(IDC_PA2,BM_GETSTATE,0,0L)&3;
if(bstate==BST_CHECKED) a = a | 4;
bstate = SendDlgItemMessage(IDC_PA3,BM_GETSTATE,0,0L)&3;
if(bstate==BST_CHECKED) a = a | 8;
bstate = SendDlgItemMessage(IDC_PA4,BM_GETSTATE,0,0L)&3;
if(bstate==BST_CHECKED) a = a | 16;
bstate = SendDlgItemMessage(IDC_PA5,BM_GETSTATE,0,0L)&3;
if(bstate==BST_CHECKED) a = a | 32;
bstate = SendDlgItemMessage(IDC_PA6,BM_GETSTATE,0,0L)&3;
if(bstate==BST_CHECKED) a = a | 64;
bstate = SendDlgItemMessage(IDC_PA7,BM_GETSTATE,0,0L)&3;
if(bstate==BST_CHECKED) a = a | 128;


_outp(BASE_ADDRESS+REG_A,a);

}

	
}

void CDiotestDlg::OnBdir() 
{
	// TODO: Add your control notification handler code here
DWORD b;
DWORD bstate;
CWnd *cwnd;

	// TODO: Add your control notification handler code here
bstate = SendDlgItemMessage(IDC_BDIR,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED)
{
ctrl_val = (ctrl_val&0xFD)|0x2;
_outp(BASE_ADDRESS+REG_CTRL,ctrl_val);


cwnd = GetDlgItem(IDC_BDIR);
cwnd->SetWindowText("B Input");

b = _inp(BASE_ADDRESS+REG_B);

cwnd = GetDlgItem(IDC_PB0);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PB0,b&1);
cwnd = GetDlgItem(IDC_PB1);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PB1,(b>>1)&1);
cwnd = GetDlgItem(IDC_PB2);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PB2,(b>>2)&1);
cwnd = GetDlgItem(IDC_PB3);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PB3,(b>>3)&1);
cwnd = GetDlgItem(IDC_PB4);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PB4,(b>>4)&1);
cwnd = GetDlgItem(IDC_PB5);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PB5,(b>>5)&1);
cwnd = GetDlgItem(IDC_PB6);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PB6,(b>>6)&1);
cwnd = GetDlgItem(IDC_PB7);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PB7,(b>>7)&1);


}
else
{
//set to output
ctrl_val = (ctrl_val&0xFD);
_outp(BASE_ADDRESS+REG_CTRL,ctrl_val);

cwnd = GetDlgItem(IDC_BDIR);
cwnd->SetWindowText("B Output");
	

cwnd = GetDlgItem(IDC_PB0);
cwnd->EnableWindow(TRUE);
cwnd = GetDlgItem(IDC_PB1);
cwnd->EnableWindow(TRUE);
cwnd = GetDlgItem(IDC_PB2);
cwnd->EnableWindow(TRUE);
cwnd = GetDlgItem(IDC_PB3);
cwnd->EnableWindow(TRUE);
cwnd = GetDlgItem(IDC_PB4);
cwnd->EnableWindow(TRUE);
cwnd = GetDlgItem(IDC_PB5);
cwnd->EnableWindow(TRUE);
cwnd = GetDlgItem(IDC_PB6);
cwnd->EnableWindow(TRUE);
cwnd = GetDlgItem(IDC_PB7);
cwnd->EnableWindow(TRUE);
b = 0;
bstate = SendDlgItemMessage(IDC_PB0,BM_GETSTATE,0,0L)&3;
if(bstate==BST_CHECKED) b = b | 1;
bstate = SendDlgItemMessage(IDC_PB1,BM_GETSTATE,0,0L)&3;
if(bstate==BST_CHECKED) b = b | 2;
bstate = SendDlgItemMessage(IDC_PB2,BM_GETSTATE,0,0L)&3;
if(bstate==BST_CHECKED) b = b | 4;
bstate = SendDlgItemMessage(IDC_PB3,BM_GETSTATE,0,0L)&3;
if(bstate==BST_CHECKED) b = b | 8;
bstate = SendDlgItemMessage(IDC_PB4,BM_GETSTATE,0,0L)&3;
if(bstate==BST_CHECKED) b = b | 16;
bstate = SendDlgItemMessage(IDC_PB5,BM_GETSTATE,0,0L)&3;
if(bstate==BST_CHECKED) b = b | 32;
bstate = SendDlgItemMessage(IDC_PB6,BM_GETSTATE,0,0L)&3;
if(bstate==BST_CHECKED) b = b | 64;
bstate = SendDlgItemMessage(IDC_PB7,BM_GETSTATE,0,0L)&3;
if(bstate==BST_CHECKED) b = b | 128;


_outp(BASE_ADDRESS+REG_B,b);

}

	

	
}

void CDiotestDlg::OnChdir() 
{
	// TODO: Add your control notification handler code here
DWORD c;
DWORD bstate;
CWnd *cwnd;

	// TODO: Add your control notification handler code here
bstate = SendDlgItemMessage(IDC_CHDIR,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED)
{
ctrl_val = (ctrl_val&0xF7)|0x8;
_outp(BASE_ADDRESS+REG_CTRL,ctrl_val);

cwnd = GetDlgItem(IDC_CHDIR);
cwnd->SetWindowText("C High Input");
	
c = _inp(BASE_ADDRESS+REG_C);

cwnd = GetDlgItem(IDC_PC4);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PC4,(c>>4)&1);
cwnd = GetDlgItem(IDC_PC5);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PC5,(c>>5)&1);
cwnd = GetDlgItem(IDC_PC6);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PC6,(c>>6)&1);
cwnd = GetDlgItem(IDC_PC7);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PC7,(c>>7)&1);


}
else
{
//set to output
ctrl_val = (ctrl_val&0xF7);
_outp(BASE_ADDRESS+REG_CTRL,ctrl_val);


cwnd = GetDlgItem(IDC_CHDIR);
cwnd->SetWindowText("C High Output");
	

cwnd = GetDlgItem(IDC_PC4);
cwnd->EnableWindow(TRUE);
cwnd = GetDlgItem(IDC_PC5);
cwnd->EnableWindow(TRUE);
cwnd = GetDlgItem(IDC_PC6);
cwnd->EnableWindow(TRUE);
cwnd = GetDlgItem(IDC_PC7);
cwnd->EnableWindow(TRUE);

c = _inp(BASE_ADDRESS+REG_C);
c = c & 0x0f;

bstate = SendDlgItemMessage(IDC_PC4,BM_GETSTATE,0,0L)&3;
if(bstate==BST_CHECKED) c = c | 16;
bstate = SendDlgItemMessage(IDC_PC5,BM_GETSTATE,0,0L)&3;
if(bstate==BST_CHECKED) c = c | 32;
bstate = SendDlgItemMessage(IDC_PC6,BM_GETSTATE,0,0L)&3;
if(bstate==BST_CHECKED) c = c | 64;
bstate = SendDlgItemMessage(IDC_PC7,BM_GETSTATE,0,0L)&3;
if(bstate==BST_CHECKED) c = c | 128;


_outp(BASE_ADDRESS+REG_C,c);

}

	

	
}

void CDiotestDlg::OnCldir() 
{
	// TODO: Add your control notification handler code here
DWORD c;
DWORD bstate;
CWnd *cwnd;

	// TODO: Add your control notification handler code here
bstate = SendDlgItemMessage(IDC_CLDIR,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED)
{
ctrl_val = (ctrl_val&0xFE)|0x1;
_outp(BASE_ADDRESS+REG_CTRL,ctrl_val);


cwnd = GetDlgItem(IDC_CLDIR);
cwnd->SetWindowText("C Low Input");
	
c = _inp(BASE_ADDRESS+REG_C);

cwnd = GetDlgItem(IDC_PC0);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PC0,c&1);
cwnd = GetDlgItem(IDC_PC1);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PC1,(c>>1)&1);
cwnd = GetDlgItem(IDC_PC2);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PC2,(c>>2)&1);
cwnd = GetDlgItem(IDC_PC3);
cwnd->EnableWindow(FALSE);
CheckDlgButton(IDC_PC3,(c>>3)&1);


}
else
{
//set to output
ctrl_val = (ctrl_val&0xFE);
_outp(BASE_ADDRESS+REG_CTRL,ctrl_val);


cwnd = GetDlgItem(IDC_CLDIR);
cwnd->SetWindowText("C Low Output");
	

cwnd = GetDlgItem(IDC_PC0);
cwnd->EnableWindow(TRUE);
cwnd = GetDlgItem(IDC_PC1);
cwnd->EnableWindow(TRUE);
cwnd = GetDlgItem(IDC_PC2);
cwnd->EnableWindow(TRUE);
cwnd = GetDlgItem(IDC_PC3);
cwnd->EnableWindow(TRUE);

c= _inp(BASE_ADDRESS+REG_C);
c = c & 0xf0;


bstate = SendDlgItemMessage(IDC_PC0,BM_GETSTATE,0,0L)&3;
if(bstate==BST_CHECKED) c = c | 1;
bstate = SendDlgItemMessage(IDC_PC1,BM_GETSTATE,0,0L)&3;
if(bstate==BST_CHECKED) c = c | 2;
bstate = SendDlgItemMessage(IDC_PC2,BM_GETSTATE,0,0L)&3;
if(bstate==BST_CHECKED) c = c | 4;
bstate = SendDlgItemMessage(IDC_PC3,BM_GETSTATE,0,0L)&3;
if(bstate==BST_CHECKED) c = c | 8;


_outp(BASE_ADDRESS+REG_C,c);

}

	
	
}

void CDiotestDlg::OnUpdate() 
{
DWORD a,b,c;

	// TODO: Add your control notification handler code here
a = _inp(BASE_ADDRESS+REG_A);
b = _inp(BASE_ADDRESS+REG_B);
c = _inp(BASE_ADDRESS+REG_C);


CheckDlgButton(IDC_PA0,a&1);
CheckDlgButton(IDC_PA1,(a>>1)&1);
CheckDlgButton(IDC_PA2,(a>>2)&1);
CheckDlgButton(IDC_PA3,(a>>3)&1);
CheckDlgButton(IDC_PA4,(a>>4)&1);
CheckDlgButton(IDC_PA5,(a>>5)&1);
CheckDlgButton(IDC_PA6,(a>>6)&1);
CheckDlgButton(IDC_PA7,(a>>7)&1);

CheckDlgButton(IDC_PB0,(b)&1);
CheckDlgButton(IDC_PB1,(b>>1)&1);
CheckDlgButton(IDC_PB2,(b>>2)&1);
CheckDlgButton(IDC_PB3,(b>>3)&1);
CheckDlgButton(IDC_PB4,(b>>4)&1);
CheckDlgButton(IDC_PB5,(b>>5)&1);
CheckDlgButton(IDC_PB6,(b>>6)&1);
CheckDlgButton(IDC_PB7,(b>>7)&1);

CheckDlgButton(IDC_PC0,(c)&1);
CheckDlgButton(IDC_PC1,(c>>1)&1);
CheckDlgButton(IDC_PC2,(c>>2)&1);
CheckDlgButton(IDC_PC3,(c>>3)&1);
CheckDlgButton(IDC_PC4,(c>>4)&1);
CheckDlgButton(IDC_PC5,(c>>5)&1);
CheckDlgButton(IDC_PC6,(c>>6)&1);
CheckDlgButton(IDC_PC7,(c>>7)&1);
	
}

void CDiotestDlg::OnPa0() 
{
	// TODO: Add your control notification handler code here
DWORD bstate;
DWORD a;

	// TODO: Add your control notification handler code here
bstate = SendDlgItemMessage(IDC_ADIR,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED)
{
//is input so leave alone
}
else
{
//is output so set/clear the bit
a = _inp(BASE_ADDRESS+REG_A);

bstate = SendDlgItemMessage(IDC_PA0,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED) a = a&0xFE;
else a = (a&0xfe)|1;

_outp(BASE_ADDRESS+REG_A,a);

}
}

void CDiotestDlg::OnPa1() 
{
DWORD bstate;
DWORD a;

	// TODO: Add your control notification handler code here
bstate = SendDlgItemMessage(IDC_ADIR,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED)
{
//is input so leave alone
}
else
{
//is output so set/clear the bit
a = _inp(BASE_ADDRESS+REG_A);

bstate = SendDlgItemMessage(IDC_PA1,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED) a = a&0xFD;
else a = (a&0xfD)|2;

_outp(BASE_ADDRESS+REG_A,a);
}
}

void CDiotestDlg::OnPa2() 
{
DWORD bstate;
DWORD a;

	// TODO: Add your control notification handler code here
bstate = SendDlgItemMessage(IDC_ADIR,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED)
{
//is input so leave alone
}
else
{
//is output so set/clear the bit
a = _inp(BASE_ADDRESS+REG_A);

bstate = SendDlgItemMessage(IDC_PA2,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED) a = a&0xFB;
else a = (a&0xfB)|4;

_outp(BASE_ADDRESS+REG_A,a);


}
	
}

void CDiotestDlg::OnPa3() 
{
	// TODO: Add your control notification handler code here
DWORD bstate;
DWORD a;

	// TODO: Add your control notification handler code here
bstate = SendDlgItemMessage(IDC_ADIR,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED)
{
//is input so leave alone
}
else
{
//is output so set/clear the bit
a = _inp(BASE_ADDRESS+REG_A);

bstate = SendDlgItemMessage(IDC_PA3,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED) a = a&0xF7;
else a = (a&0xf7)|8;

_outp(BASE_ADDRESS+REG_A,a);


}
	
}

void CDiotestDlg::OnPa4() 
{
	// TODO: Add your control notification handler code here
DWORD bstate;
DWORD a;

	// TODO: Add your control notification handler code here
bstate = SendDlgItemMessage(IDC_ADIR,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED)
{
//is input so leave alone
}
else
{
//is output so set/clear the bit
a = _inp(BASE_ADDRESS+REG_A);

bstate = SendDlgItemMessage(IDC_PA4,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED) a = a&0xEF;
else a = (a&0xEF)|0x10;

_outp(BASE_ADDRESS+REG_A,a);


}
	
}

void CDiotestDlg::OnPa5() 
{
	// TODO: Add your control notification handler code here
DWORD bstate;
DWORD a;

	// TODO: Add your control notification handler code here
bstate = SendDlgItemMessage(IDC_ADIR,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED)
{
//is input so leave alone
}
else
{
//is output so set/clear the bit
a = _inp(BASE_ADDRESS+REG_A);

bstate = SendDlgItemMessage(IDC_PA5,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED) a = a&0xDF;
else a = (a&0xDF)|0x20;

_outp(BASE_ADDRESS+REG_A,a);


}
	
}

void CDiotestDlg::OnPa6() 
{
	// TODO: Add your control notification handler code here
DWORD bstate;
DWORD a;

	// TODO: Add your control notification handler code here
bstate = SendDlgItemMessage(IDC_ADIR,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED)
{
//is input so leave alone
}
else
{
//is output so set/clear the bit
a = _inp(BASE_ADDRESS+REG_A);

bstate = SendDlgItemMessage(IDC_PA6,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED) a = a&0xBF;
else a = (a&0xBF)|0x40;

_outp(BASE_ADDRESS+REG_A,a);


}
	
}

void CDiotestDlg::OnPa7() 
{
	// TODO: Add your control notification handler code here
DWORD bstate;
DWORD a;

	// TODO: Add your control notification handler code here
bstate = SendDlgItemMessage(IDC_ADIR,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED)
{
//is input so leave alone
}
else
{
//is output so set/clear the bit
a = _inp(BASE_ADDRESS+REG_A);

bstate = SendDlgItemMessage(IDC_PA7,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED) a = a&0x7F;
else a = (a&0x7f)|0x80;

_outp(BASE_ADDRESS+REG_A,a);


}
}
	
void CDiotestDlg::OnPb0() 
{
	// TODO: Add your control notification handler code here
DWORD bstate;
DWORD b;

	// TODO: Add your control notification handler code here
bstate = SendDlgItemMessage(IDC_BDIR,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED)
{
//is input so leave alone
}
else
{
//is output so set/clear the bit

b = _inp(BASE_ADDRESS+REG_B);

bstate = SendDlgItemMessage(IDC_PB0,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED) b = b&0xFE;
else b = (b&0xfe)|1;

_outp(BASE_ADDRESS+REG_B,b);


}
}

void CDiotestDlg::OnPb1() 
{
DWORD bstate;
DWORD b;

	// TODO: Add your control notification handler code here
bstate = SendDlgItemMessage(IDC_BDIR,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED)
{
//is input so leave alone
}
else
{
//is output so set/clear the bit
b = _inp(BASE_ADDRESS+REG_B);

bstate = SendDlgItemMessage(IDC_PB1,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED) b = b&0xFD;
else b = (b&0xfD)|2;

_outp(BASE_ADDRESS+REG_B,b);

}
}

void CDiotestDlg::OnPb2() 
{
DWORD bstate;
DWORD b;

	// TODO: Add your control notification handler code here
bstate = SendDlgItemMessage(IDC_BDIR,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED)
{
//is input so leave alone
}
else
{
//is output so set/clear the bit
b = _inp(BASE_ADDRESS+REG_B);

bstate = SendDlgItemMessage(IDC_PB2,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED) b = b&0xFB;
else b = (b&0xfB)|4;

_outp(BASE_ADDRESS+REG_B,b);


}
	
}

void CDiotestDlg::OnPb3() 
{
	// TODO: Add your control notification handler code here
DWORD bstate;
DWORD b;

	// TODO: Add your control notification handler code here
bstate = SendDlgItemMessage(IDC_BDIR,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED)
{
//is input so leave alone
}
else
{
//is output so set/clear the bit
b = _inp(BASE_ADDRESS+REG_B);

bstate = SendDlgItemMessage(IDC_PB3,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED) b = b&0xF7;
else b = (b&0xf7)|8;

_outp(BASE_ADDRESS+REG_B,b);


}
	
}

void CDiotestDlg::OnPb4() 
{
	// TODO: Add your control notification handler code here
DWORD bstate;
DWORD b;

	// TODO: Add your control notification handler code here
bstate = SendDlgItemMessage(IDC_BDIR,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED)
{
//is input so leave alone
}
else
{
//is output so set/clear the bit
b = _inp(BASE_ADDRESS+REG_B);

bstate = SendDlgItemMessage(IDC_PB4,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED) b = b&0xEF;
else b = (b&0xEF)|0x10;

_outp(BASE_ADDRESS+REG_B,b);


}
	
}

void CDiotestDlg::OnPb5() 
{
	// TODO: Add your control notification handler code here
DWORD bstate;
DWORD b;

	// TODO: Add your control notification handler code here
bstate = SendDlgItemMessage(IDC_BDIR,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED)
{
//is input so leave alone
}
else
{
//is output so set/clear the bit
b = _inp(BASE_ADDRESS+REG_B);

bstate = SendDlgItemMessage(IDC_PB5,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED) b = b&0xDF;
else b = (b&0xDF)|0x20;

_outp(BASE_ADDRESS+REG_B,b);


}
	
}

void CDiotestDlg::OnPb6() 
{
	// TODO: Add your control notification handler code here
DWORD bstate;
DWORD b;

	// TODO: Add your control notification handler code here
bstate = SendDlgItemMessage(IDC_BDIR,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED)
{
//is input so leave alone
}
else
{
//is output so set/clear the bit
b = _inp(BASE_ADDRESS+REG_B);

bstate = SendDlgItemMessage(IDC_PB6,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED) b = b&0xBF;
else b = (b&0xBF)|0x40;

_outp(BASE_ADDRESS+REG_B,b);


}
	
}

void CDiotestDlg::OnPb7() 
{
	// TODO: Add your control notification handler code here
DWORD bstate;
DWORD b;

	// TODO: Add your control notification handler code here
bstate = SendDlgItemMessage(IDC_BDIR,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED)
{
//is input so leave alone
}
else
{
//is output so set/clear the bit
b = _inp(BASE_ADDRESS+REG_B);

bstate = SendDlgItemMessage(IDC_PB7,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED) b = b&0x7F;
else b = (b&0x7f)|0x80;

_outp(BASE_ADDRESS+REG_B,b);

}
}

void CDiotestDlg::OnPc0() 
{
	// TODO: Add your control notification handler code here
DWORD bstate;
DWORD c;

	// TODO: Add your control notification handler code here
bstate = SendDlgItemMessage(IDC_CLDIR,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED)
{
//is input so leave alone
}
else
{
//is output so set/clear the bit
c = _inp(BASE_ADDRESS+REG_C);

bstate = SendDlgItemMessage(IDC_PC0,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED) c = c&0xFE;
else c = (c&0xfe)|1;

_outp(BASE_ADDRESS+REG_C,c);


}
	
}

void CDiotestDlg::OnPc1() 
{
	// TODO: Add your control notification handler code here
DWORD bstate;
DWORD c;

	// TODO: Add your control notification handler code here
bstate = SendDlgItemMessage(IDC_CLDIR,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED)
{
//is input so leave alone
}
else
{
//is output so set/clear the bit
c = _inp(BASE_ADDRESS+REG_C);

bstate = SendDlgItemMessage(IDC_PC1,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED) c = c&0xFD;
else c = (c&0xfD)|0x02;

_outp(BASE_ADDRESS+REG_C,c);


}
	
}

void CDiotestDlg::OnPc2() 
{
	// TODO: Add your control notification handler code here
DWORD bstate;
DWORD c;

	// TODO: Add your control notification handler code here
bstate = SendDlgItemMessage(IDC_CLDIR,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED)
{
//is input so leave alone
}
else
{
//is output so set/clear the bit
c = _inp(BASE_ADDRESS+REG_C);

bstate = SendDlgItemMessage(IDC_PC2,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED) c = c&0xFB;
else c = (c&0xfB)|0x04;

_outp(BASE_ADDRESS+REG_C,c);


}
	
}

void CDiotestDlg::OnPc3() 
{
	// TODO: Add your control notification handler code here
DWORD bstate;
DWORD c;

	// TODO: Add your control notification handler code here
bstate = SendDlgItemMessage(IDC_CLDIR,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED)
{
//is input so leave alone
}
else
{
//is output so set/clear the bit
c = _inp(BASE_ADDRESS+REG_C);

bstate = SendDlgItemMessage(IDC_PC3,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED) c = c&0xF7;
else c = (c&0xf7)|0x08;

_outp(BASE_ADDRESS+REG_C,c);


}
	
}

void CDiotestDlg::OnPc4() 
{
	// TODO: Add your control notification handler code here
DWORD bstate;
DWORD c;

	// TODO: Add your control notification handler code here
bstate = SendDlgItemMessage(IDC_CHDIR,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED)
{
//is input so leave alone
}
else
{
//is output so set/clear the bit
c = _inp(BASE_ADDRESS+REG_C);

bstate = SendDlgItemMessage(IDC_PC4,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED) c = c&0xEF;
else c = (c&0xEF)|0x10;

_outp(BASE_ADDRESS+REG_C,c);


}
	
}

void CDiotestDlg::OnPc5() 
{
	// TODO: Add your control notification handler code here
DWORD bstate;
DWORD c;

	// TODO: Add your control notification handler code here
bstate = SendDlgItemMessage(IDC_CHDIR,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED)
{
//is input so leave alone
}
else
{
//is output so set/clear the bit
c = _inp(BASE_ADDRESS+REG_C);

bstate = SendDlgItemMessage(IDC_PC5,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED) c = c&0xDF;
else c = (c&0xDF)|0x20;

_outp(BASE_ADDRESS+REG_C,c);

	
}
}
void CDiotestDlg::OnPc6() 
{
	// TODO: Add your control notification handler code here
DWORD bstate;
DWORD c;

	// TODO: Add your control notification handler code here
bstate = SendDlgItemMessage(IDC_CHDIR,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED)
{
//is input so leave alone
}
else
{
//is output so set/clear the bit
c = _inp(BASE_ADDRESS+REG_C);

bstate = SendDlgItemMessage(IDC_PC6,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED) c = c&0xBF;
else c = (c&0xBF)|0x40;

_outp(BASE_ADDRESS+REG_C,c);

	
}
}
void CDiotestDlg::OnPc7() 
{
	// TODO: Add your control notification handler code here
DWORD bstate;
DWORD c;

	// TODO: Add your control notification handler code here
bstate = SendDlgItemMessage(IDC_CHDIR,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED)
{
//is input so leave alone
}
else
{
//is output so set/clear the bit

c = _inp(BASE_ADDRESS+REG_C);

bstate = SendDlgItemMessage(IDC_PC7,BM_GETSTATE,0,0L)&3;
if(bstate==BST_UNCHECKED) c = c&0x7F;
else c = (c&0x7F)|0x80;

_outp(BASE_ADDRESS+REG_C,c);

	
}
}

void CDiotestDlg::OnKillfocusBoardnum() 
{
	// TODO: Add your control notification handler code here
char buf[256];
unsigned board_val;

SendDlgItemMessage(IDC_BOARDNUM,WM_GETTEXT,256,(LPARAM)(LPCTSTR)buf);	
board_val = atoi(buf);

}
