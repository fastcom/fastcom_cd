This project demonstrates how to interface to the DIO24H card using Visual C
in a Win95/98 environment.  There are no special drivers involved, as all
that is needed is port io access.  The Standard _inp() and _outp() functions
will allow access to the DIO24H registers as shown by this example.
In fact you could probably use the dos program inside of W95/98 and have
it operate successfully, or recompile the dos dio24h.c program as a win32
console application, and it should also probably run.


========================================================================
       MICROSOFT FOUNDATION CLASS LIBRARY : diotest
========================================================================


AppWizard has created this diotest application for you.  This application
not only demonstrates the basics of using the Microsoft Foundation classes
but is also a starting point for writing your application.

This file contains a summary of what you will find in each of the files that
make up your diotest application.

diotest.h
    This is the main header file for the application.  It includes other
    project specific headers (including Resource.h) and declares the
    CDiotestApp application class.

diotest.cpp
    This is the main application source file that contains the application
    class CDiotestApp.

diotest.rc
    This is a listing of all of the Microsoft Windows resources that the
    program uses.  It includes the icons, bitmaps, and cursors that are stored
    in the RES subdirectory.  This file can be directly edited in Microsoft
	Developer Studio.

res\diotest.ico
    This is an icon file, which is used as the application's icon.  This
    icon is included by the main resource file diotest.rc.

res\diotest.rc2
    This file contains resources that are not edited by Microsoft 
	Developer Studio.  You should place all resources not
	editable by the resource editor in this file.

diotest.clw
    This file contains information used by ClassWizard to edit existing
    classes or add new classes.  ClassWizard also uses this file to store
    information needed to create and edit message maps and dialog data
    maps and to create prototype member functions.


/////////////////////////////////////////////////////////////////////////////

AppWizard creates one dialog class:

diotestDlg.h, diotestDlg.cpp - the dialog
    These files contain your CDiotestDlg class.  This class defines
    the behavior of your application's main dialog.  The dialog's
    template is in diotest.rc, which can be edited in Microsoft
	Developer Studio.


/////////////////////////////////////////////////////////////////////////////
Other standard files:

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named diotest.pch and a precompiled types file named StdAfx.obj.

Resource.h
    This is the standard header file, which defines new resource IDs.
    Microsoft Developer Studio reads and updates this file.

/////////////////////////////////////////////////////////////////////////////
Other notes:

AppWizard uses "TODO:" to indicate parts of the source code you
should add to or customize.

If your application uses MFC in a shared DLL, and your application is 
in a language other than the operating system's current language, you
will need to copy the corresponding localized resources MFC40XXX.DLL
from the Microsoft Visual C++ CD-ROM onto the system or system32 directory,
and rename it to be MFCLOC.DLL.  ("XXX" stands for the language abbreviation.
For example, MFC40DEU.DLL contains resources translated to German.)  If you
don't do this, some of the UI elements of your application will remain in the
language of the operating system.

/////////////////////////////////////////////////////////////////////////////
