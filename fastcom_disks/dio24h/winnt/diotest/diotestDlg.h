// diotestDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDiotestDlg dialog

class CDiotestDlg : public CDialog
{
// Construction
public:
	CDiotestDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CDiotestDlg)
	enum { IDD = IDD_DIOTEST_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDiotestDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CDiotestDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnAdir();
	afx_msg void OnBdir();
	afx_msg void OnChdir();
	afx_msg void OnCldir();
	afx_msg void OnUpdate();
	afx_msg void OnPa0();
	afx_msg void OnPa1();
	afx_msg void OnPa2();
	afx_msg void OnPa3();
	afx_msg void OnPa4();
	afx_msg void OnPa5();
	afx_msg void OnPa6();
	afx_msg void OnPa7();
	afx_msg void OnPb0();
	afx_msg void OnPb1();
	afx_msg void OnPb2();
	afx_msg void OnPb3();
	afx_msg void OnPb4();
	afx_msg void OnPb5();
	afx_msg void OnPb6();
	afx_msg void OnPb7();
	afx_msg void OnPc0();
	afx_msg void OnPc1();
	afx_msg void OnPc2();
	afx_msg void OnPc3();
	afx_msg void OnPc4();
	afx_msg void OnPc5();
	afx_msg void OnPc6();
	afx_msg void OnPc7();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
