Attribute VB_Name = "Module1"
Public Const BASE_ADDRESS = &H300
Public Const REG_A = BASE_ADDRESS + 0
Public Const REG_B = BASE_ADDRESS + 1
Public Const REG_C = BASE_ADDRESS + 2
Public Const REG_CTRL = BASE_ADDRESS + 3

Public Declare Function CreateDio Lib "diodrv.dll" Alias "#2" () As Long
Public Declare Function ReadDioReg Lib "diodrv.dll" Alias "#3" (ByVal port As Long) As Long
Public Declare Function WriteDioReg Lib "diodrv.dll" Alias "#4" (ByVal port As Long, ByVal value As Long) As Long
Public Declare Function CloseDio Lib "diodrv.dll" Alias "#1" () As Long

