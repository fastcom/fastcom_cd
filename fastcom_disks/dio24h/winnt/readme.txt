In this dir are the dio24hdrv.sys file and its install program (dioinstall.exe).
To load the driver simply type:
dioinstall -i
at a command prompt.
Once the driver is installed and started you can run the example programs.

In the diotest dir is the diotest.cpp/.exe. It is a MFC program that uses the 
dio24hdrv.sys driver to interact with the Fastcom:DIO24H-ISA board.  This 
example uses the standard win32 CreateFile(),DeviceIoControl() functions to 
talk to the driver.  You can interact with the DIO24 board by setting the 
direction of the A,B and C registers (to input or output). And you can set
the outputs (of the ports set to output) by clicking the check box by the 
corresponding port pin.  Clicking the refresh button will read the current
input values from ports A,B, and C and display their values in the checkboxes.
If you have a loopback connector with the port A pins wired to the port B pins,
then if you set port A to output (and B to input), and set a data pattern 
(by checking/unchecking checkboxes in the PA0-7 row) then when you click 
refresh the same pattern should show up on the PB0-7 checkboxes.


In the diodrv subdir is a C project (diodrv.dll) that encapsulates the necessary
Win32 functions to interact with the driver, such that these functions can 
be quickly and easily called from Visual Basic.  The exported functions are:
CreateDio - calls CreateFile() to open the DIO24H device.
CloseDio  - Calls CloseHandle() to close the DIO24H device.
ReadDioReg - Calls DeviceIoControl() to read a DIO24H register
WriteDioReg - Calls DeviceIoControl() to write to a DIO24H register

In the vb subdir is a project similar to the diotest.cpp MFC program, only 
written using visual basic, the resulting exe (test_dio.exe) uses the diodrv.dll
to accomplish much the same thing as the C++ program.

cg
11/12/99

