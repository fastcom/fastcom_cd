// diodrv.cpp : Defines the entry point for the DLL application.
//
//simple dll that exports the functions necessary to interface to the DIO24 from
//anything (ie VB)
//functions are :
//
// CreateDio()			-- opens dio device (activates driver)
// CloseDio()			-- closes dio device 
// ReadDioReg(reg)		-- gets the contents of an i/o address (presumably from the DIO24H registers)
// WriteDioReg(reg,val)	-- writes to an i/o address (again presumably to the DIO24H registers) 
//
#include "stdafx.h"
#include "diodrv.h"
#include "dio24.h"

//globals
ULONG refcount = 0;
HANDLE hDIO = NULL;

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
			break;
		case DLL_THREAD_ATTACH:
			refcount++;
			break;
		case DLL_THREAD_DETACH:
			refcount--;
			if((refcount==0)&&(hDIO!=NULL)) CloseHandle(hDIO);
			break;
		case DLL_PROCESS_DETACH:
			break;
    }
    return TRUE;
}



DIODRV_API DWORD __stdcall CreateDio(void)
{
if(hDIO==NULL)
{
//only create it if we haven't previously
    hDIO = CreateFile("\\\\.\\DIO24H", GENERIC_READ|GENERIC_WRITE,FILE_SHARE_READ|FILE_SHARE_WRITE,0,
                        OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
    if ( hDIO == INVALID_HANDLE_VALUE )
    {
	hDIO = NULL;
	//	MessageBox(NULL,"Cannot Open DIO24H","ERROR",MB_OK);
    return (-1);//failed return value
	}
}
return 0;//success return value
}

DIODRV_API DWORD __stdcall ReadDioReg(DWORD port)
{
DIOREG dregs;
DWORD cbBytesReturned;
DWORD pv;
dregs.reg = port;
dregs.val = 0;
pv = 0;
DeviceIoControl(hDIO, IOCTL_READ_REGISTER,&dregs, sizeof(DIOREG),&pv,sizeof(DWORD),&cbBytesReturned, NULL);
return pv;

}

DIODRV_API void __stdcall WriteDioReg(DWORD port, DWORD value)
{
DIOREG dregs;
DWORD cbBytesReturned;
dregs.reg = port;
dregs.val = (char)value;
DeviceIoControl(hDIO, IOCTL_WRITE_REGISTER,&dregs, sizeof(DIOREG),(LPVOID)NULL,0,&cbBytesReturned, NULL);
}

DIODRV_API void __stdcall CloseDio(void)
{
if(hDIO!=NULL)
	{
	CloseHandle(hDIO);
	hDIO = NULL;
	}
}

