//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by TTY.RC
//
#define IDS_BAUD115200                  813
#define IDS_BAUDUSER                    814
#define IDC_USER_RATE                   1000
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
