/* $Id$ */
/*
Copyright(c) 2003, Commtech, Inc.
getfeatures.c -- user mode function to get/display the onboard feature setting for a ESCC-PCIv3 port

usage:
 getfeatures port 

 The port can be any valid escc port (0,1)



*/


#include <windows.h>
#include <stdio.h>
#include <math.h> /* floor, pow */

#include "..\esccptest.h"



int main(int argc, char * argv[])
{
	//clkset clock;
	char nbuf[80];
	int port;
	HANDLE hDevice; 
	ULONG  desreg;
	unsigned long temp;
	
	
	if(argc<2) {
		printf("usage:  getfeatures port \r\n");
		exit(1);
	}
	
	port = atoi(argv[1]);
	
	sprintf(nbuf,"\\\\.\\ESCC%u",port);
	
	printf("Opening: %s\n",nbuf);
	
	if((hDevice = CreateFile (
		nbuf, 
		GENERIC_READ | GENERIC_WRITE,
		0,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
		NULL)) == INVALID_HANDLE_VALUE ) 
	{
		printf("Can't get a handle to esccpdrv @ %s\n",nbuf);
		exit(1);
	}
	
	
	//	Feature register (32 bit register at AMCCport) consists of:
	//	bit 0:  receive echo cancel if '0' RTS controls RD, if '1' RD allways on
	//  bit 1:  SD 485 control      if '0'/RTS controls SD, if '1' SD allways on
	//  bit 2:  TT 485 control      if '0'/RTS controls TT, if '1' SD allways on
	//  bit 3:  CTS disable         if '0' CTS allways active, if '1' CTS from connector
	//  bit 4:  txclk <= ST         if '0' txclk connected to ST, if '1' txclk output is tri-state
	//  bit 5:  txclk => TT         if '0' txclk connected to TT, if '1' TT output is '1'
	//  bit 16: busmode			    if '0' is 422/485, if '1' is busmode
	desreg = 0;
	if(DeviceIoControl(hDevice,IOCTL_ESCCDRV_GET_FEATURES,NULL,0,&desreg,sizeof(ULONG),&temp,NULL))
	{
		printf("des:%8.8x\r\n",desreg);
		if((desreg&1)==1)		printf("RX allways on\r\n");
		else					printf("RX echo	cancel ENABLED\r\n");
		if((desreg&2)==2)		printf("SD is RS-422\r\n");
		else					printf("SD is RS-485\r\n");
		if((desreg&4)==4)		printf("TT is RS-422\r\n");
		else					printf("TT is RS-485\r\n");
		if((desreg&8)==8)	    printf("CTS from connector\r\n");
		else					printf("CTS allways active\r\n");
		if((desreg&0x10)==0x10)	printf("ST disconnected\r\n");
		else					printf("ST connected to txclk pin\r\n");
		if((desreg&0x20)==0x20)	printf("TT disconnected\r\n");
		else					printf("TT connected to txclk pin\r\n");
		if((desreg&0x50000)==0x10000) printf("BUS mode operation\r\n");
		else if((desreg&0x50000)==0x50000) printf("BUS mode operation, gated&inverted\r\n");
		else                    printf("Normal RS-422/RS-485 operation\r\n");
	}
	else printf("failed:%8.8x\r\n",GetLastError());
	
	
	CloseHandle(hDevice);
	return 0;
}



/* $Id$ */