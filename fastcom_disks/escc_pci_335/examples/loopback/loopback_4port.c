/*++

Copyright (c) 2005 Commtech, Inc Wichita ,KS

Module Name:

    loopback.c

Abstract:

    A simple Win32 app that runs a loopback on one port of the escc-335 device

Environment:

    user mode only

--*/

#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "esccptest.h"


VOID
main(
    IN int   argc,
    IN char *argv[]
    )
/*++

Routine Description:

Arguments:

Return Value:

--*/
{
	HANDLE hDevice[4];			//handle to the escc driver/device
	
	DWORD nobyteswritten=0;	//number of bytes that the driver returns as written to the device
	struct setup esccsetup;	//setup structure for initializing the escc registers (see escctest.h)
	char data[4096];		//character array for data storage (passing to and from the driver)
	char rdata[4096];		//character array for data storage (passing to and from the driver)
	DWORD nobytestoread=0;	//the number of bytes to read from the driver
	DWORD nobytestowrite=0;	//the number of bytes to write to the driver
	DWORD nobytesread=0;		//the number of bytes that the driver put in data[]
	ULONG j=0,k=0;				//temp vars
	DWORD returnsize=0;		//temp vars
	ULONG i=0;				//temp vars
	BOOL t;					//temp vars
	OVERLAPPED  os,osr ;				//overlapped structure for use in the transmit routine
	char devname[80];
	unsigned devno;
	ULONG freq=0;
	ULONG loop=0;
	ULONG x=0,error=0;
	ULONG totalsent=0;
	ULONG totalread=0;
	ULONG totalerror=0;
	DWORD lerror;
	ULONG bgrval=0;

	srand( (unsigned)time( NULL ) );
		
	memset( &os, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
	
	// create I/O event used for overlapped write
	
	os.hEvent = CreateEvent( NULL,    // no security
		TRUE,    // explicit reset req
		FALSE,   // initial event reset
		NULL ) ; // no name
	if (os.hEvent == NULL)
	{
		MessageBox( NULL, "Failed to create event for thread!", "main Error!",
			MB_ICONEXCLAMATION | MB_OK ) ;
		return; 
	}
	
	memset( &osr, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
	
	// create I/O event used for overlapped write
	
	osr.hEvent = CreateEvent( NULL,    // no security
		TRUE,    // explicit reset req
		FALSE,   // initial event reset
		NULL ) ; // no name
	if (osr.hEvent == NULL)
	{
		MessageBox( NULL, "Failed to create event for thread!", "main Error!",
			MB_ICONEXCLAMATION | MB_OK ) ;
		return; 
	}
	
	nobytestoread = 4096;    //this should allways be 4096, it is the maximum frame size returned from the driver at one time
	nobytestowrite = 1023;	//this is the number of bytes to send to the driver to send as a single frame (can be from 1 to 4096)
    
	//this will start up the driver, it also gives us a handle to send
	//and receive data from the driver
	//note that it is created with the FILE_FLAG_OVERLAPPED set
	//if it is not then you must make sequential calls to the driver
	//(ie wait until one call has returned before making another call)
	//the read/write and status functions are set up to use overlapped, they 
	//will return immediatly with a ERROR_IO_PENDING or immediatly with the required data
	//
	devno = 0;
for(i=0;i<4;i++)
{	
	sprintf(devname,"\\\\.\\ESCC%u",devno+i);
    hDevice[i] = CreateFile (devname,
		GENERIC_READ | GENERIC_WRITE,
		0,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
		NULL
		);
	
    if (hDevice[i] == INVALID_HANDLE_VALUE)
    {
		//for some reason the driver won't load or isn't loaded
		printf ("Can't get a handle to esccdrv:%s\n",devname);
		exit(1);
		//abort and leave here!!!
	}
	//we got our handle and are ready to go
	printf("Created -- ESCC%u\n\r",devno+i);
}
	//this IOCTL function demonstrates how to set the clock generator on 
	//the escc-p card
	//this function will return TRUE unless an invalid parameter is given
	//
	//the 335 board has a range from 6-33MHz, set to 6, and divide down in bgr
	//note the ioctl changed as well
do
{
	for(i=0;i<4;i++)
	{
	freq = 16000000;
	printf("FREQ:%ld\n",freq);
	if(freq!=0)	t = DeviceIoControl(hDevice[i],IOCTL_ESCCDRV_SET_FREQ,&freq,sizeof(ULONG),NULL,0,&returnsize,NULL);
	memset(&esccsetup.cmdr,0,sizeof(struct setup));

	printf("HDLC settings:BGR=%ld\r\n",bgrval);
	//make to use HDLC settings here
	esccsetup.mode = 0x88;
	esccsetup.timr = 0x1f;
	esccsetup.xbcl = 0x00;
	esccsetup.xbch = 0x00;
	esccsetup.ccr0 = 0x80;
	esccsetup.ccr1 = 0x10; 
	esccsetup.ccr2 = 0x38; 
	esccsetup.ccr3 = 0x00;
	esccsetup.bgr = 7;	//16MHz / ((7+1) *2) =1Mbit/sec
	esccsetup.iva = 0;
	esccsetup.ipc = 0x03;
	esccsetup.imr0 = 0x04;//disable cdsc
	esccsetup.imr1 = 0x00;
	esccsetup.pvr = 0x0;
	esccsetup.pim = 0xff;
	esccsetup.pcr = 0xe0;
	esccsetup.xad1 = 0xff;
	esccsetup.xad2 = 0xff;
	esccsetup.rah1 = 0xff;
	esccsetup.rah2 = 0xff;
	esccsetup.ral1 = 0xff;
	esccsetup.ral2 = 0xff;
	esccsetup.rlcr = 0x00;
	esccsetup.pre = 0x00;
	esccsetup.ccr4 = 0x00;

	esccsetup.n_rbufs = 10;
	esccsetup.n_rfsize_max = 4096;
	esccsetup.n_tbufs = 10;
	esccsetup.n_tfsize_max = 4096;

	//when called this IOCTL will set the registers of the escc and
	//reset the TX and RX machines (issuing a separate XRES and RHR command)
	//if the resets time out the function will return FALSE, otherwise it will
	//return TRUE
	t = DeviceIoControl(hDevice[i],IOCTL_ESCCDRV_SETUP,&esccsetup,sizeof(struct setup),NULL,0,&returnsize,NULL);
	if(t==TRUE)printf("SETTINGS SUCCESSFUL:%lu\n\r",returnsize);
	if(t==FALSE)printf("SETTINGS FAILED:%lu\n\r",returnsize);

	DeviceIoControl(hDevice[i],IOCTL_ESCCDRV_FLUSH_RX,NULL,0,NULL,0,&returnsize,NULL);
	DeviceIoControl(hDevice[i],IOCTL_ESCCDRV_FLUSH_TX,NULL,0,NULL,0,&returnsize,NULL);

		
		for(j=0;j<1024;j++)
		{
			data[j] = rand()&0xff;//;(char)i;//fill the frame with the key
		}



printf("WRITE[%d]\n",i);
		t = WriteFile(hDevice[i],&data,nobytestowrite,&nobyteswritten,&os);//send the frame
		//		printf("WRITEFILE esccdrv%lu \n\r",nobyteswritten);//if nobyteswritten doesnt = sizeof(struct buf) something is wrong
		//		if(t==TRUE)printf("TX returned TRUE\n\r"); //if returned true then the IO request was started (txing has begun)
		if(t==FALSE)                    //if it returned FALSE then the IO request is queued (waiting for previous frame to get done sending)
		{                       //and we must wait until the os.event gets signaled before we try any more sending
			printf("TX returned FALSE\n\r");
			lerror = GetLastError();
			if (lerror == ERROR_IO_PENDING) //overlapped io indicates that the IRP isn't done being processed yet (status hasn't changed)
			{
				// wait for a second for this transmission to complete
				do
				{
					k = WaitForSingleObject( os.hEvent, 1000 );//1 second timeout
					if(k==WAIT_TIMEOUT)
					{
						printf("WRITE:TIMEOUT\r\n");
						//this will execute every 1 second
						//you could put a counter in here and if the 
						//driver takes an inordinate ammout of time
						//to complete, you could issue a flush TX command
						//and break out of this loop
					}
					if(k==WAIT_ABANDONED)
					{
						printf("WRITE:ABANDONED\r\n");
					}
				}while(k!=WAIT_OBJECT_0);
			}
			else printf("Write error #%d\n",lerror);
		}

		
	}
	Sleep(10000);
	}while(!_kbhit());

	getch();
	for(i=0;i<4;i++)
	{
		CancelIo(hDevice[i]);

		CloseHandle (hDevice[i]);// stops the escc from interrupting (ie shuts it down)
	}
	printf("exiting program\n\r");          //exit message
}                                               //done

							 

