/*++

Copyright (c) 2005 Commtech, Inc Wichita ,KS

Module Name:

    loopback.c

Abstract:

    A simple Win32 app that runs a loopback on one port of the escc-335 device

Environment:

    user mode only

--*/

#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "..\esccptest.h"


VOID
main(
    IN int   argc,
    IN char *argv[]
    )
/*++

Routine Description:

Arguments:

Return Value:

--*/
{
	HANDLE hDevice;			//handle to the escc driver/device
	
	DWORD nobyteswritten=0;	//number of bytes that the driver returns as written to the device
	struct setup esccsetup;	//setup structure for initializing the escc registers (see escctest.h)
	char data[4096];		//character array for data storage (passing to and from the driver)
	char rdata[4096];		//character array for data storage (passing to and from the driver)
	DWORD nobytestoread=0;	//the number of bytes to read from the driver
	DWORD nobytestowrite=0;	//the number of bytes to write to the driver
	DWORD nobytesread=0;		//the number of bytes that the driver put in data[]
	ULONG j=0,k=0;				//temp vars
	DWORD returnsize=0;		//temp vars
	ULONG i=0;				//temp vars
	BOOL t;					//temp vars
	OVERLAPPED  os,osr ;				//overlapped structure for use in the transmit routine
	char devname[80];
	unsigned devno;
	ULONG freq=0;
	ULONG loop=0;
	ULONG x=0,error=0;
	ULONG totalsent=0;
	ULONG totalread=0;
	ULONG totalerror=0;
	DWORD lerror;
	ULONG bgrval=0;

	srand( (unsigned)time( NULL ) );
		
	memset( &os, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
	
	// create I/O event used for overlapped write
	
	os.hEvent = CreateEvent( NULL,    // no security
		TRUE,    // explicit reset req
		FALSE,   // initial event reset
		NULL ) ; // no name
	if (os.hEvent == NULL)
	{
		MessageBox( NULL, "Failed to create event for thread!", "main Error!",
			MB_ICONEXCLAMATION | MB_OK ) ;
		return; 
	}
	
	memset( &osr, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
	
	// create I/O event used for overlapped write
	
	osr.hEvent = CreateEvent( NULL,    // no security
		TRUE,    // explicit reset req
		FALSE,   // initial event reset
		NULL ) ; // no name
	if (osr.hEvent == NULL)
	{
		MessageBox( NULL, "Failed to create event for thread!", "main Error!",
			MB_ICONEXCLAMATION | MB_OK ) ;
		return; 
	}
	
	nobytestoread = 4096;    //this should allways be 4096, it is the maximum frame size returned from the driver at one time
	nobytestowrite = 1023;	//this is the number of bytes to send to the driver to send as a single frame (can be from 1 to 4096)
    
	//this will start up the driver, it also gives us a handle to send
	//and receive data from the driver
	//note that it is created with the FILE_FLAG_OVERLAPPED set
	//if it is not then you must make sequential calls to the driver
	//(ie wait until one call has returned before making another call)
	//the read/write and status functions are set up to use overlapped, they 
	//will return immediatly with a ERROR_IO_PENDING or immediatly with the required data
	//
	if(argc<5)
	{
		printf("Usage: loopback device# freq bgr Mode\n\tdevice# = 0,1,2,3...\n\tMODE = a = async, b = bisync, h= hdlc\n");
		exit(1);
	}
	devno = 0;
	if(argc>1) devno = atoi(argv[1]);
	freq= atol(argv[2]);
	bgrval=atol(argv[3]);
	
	sprintf(devname,"\\\\.\\ESCC%u",devno);
    hDevice = CreateFile (devname,
		GENERIC_READ | GENERIC_WRITE,
		0,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
		NULL
		);
	
    if (hDevice == INVALID_HANDLE_VALUE)
    {
		//for some reason the driver won't load or isn't loaded
		printf ("Can't get a handle to esccdrv\n");
		exit(1);
		//abort and leave here!!!
	}
	//we got our handle and are ready to go
	printf("Created esccdrv--ESCC%u\n\r",devno);
	//this IOCTL function demonstrates how to set the clock generator on 
	//the escc-p card
	//this function will return TRUE unless an invalid parameter is given
	//
	//the 335 board has a range from 6-33MHz, set to 6, and divide down in bgr
	//note the ioctl changed as well
	//freq = 16000000;
	printf("FREQ:%ld\n",freq);
//	if(freq!=0)	t = DeviceIoControl(hDevice,IOCTL_ESCCDRV_SET_FREQ,&freq,sizeof(ULONG),NULL,0,&returnsize,NULL);
	
	if(argc>2)
	{
		if((argv[4][0]=='H')||(argv[4][0]=='h'))
		{
			printf("HDLC settings:BGR=%ld\r\n",bgrval);
			//make to use HDLC settings here
			esccsetup.mode = 0x88;
			esccsetup.timr = 0x1f;
			esccsetup.xbcl = 0x00;
			esccsetup.xbch = 0x00;
			esccsetup.ccr0 = 0x80;
			esccsetup.ccr1 = 0x10; 
			esccsetup.ccr2 = 0x38; 
			esccsetup.ccr3 = 0x00;
			esccsetup.bgr = bgrval;	//16MHz / ((7+1) *2) =1Mbit/sec
			esccsetup.iva = 0;
			esccsetup.ipc = 0x03;
			esccsetup.imr0 = 0x04;//disable cdsc
			esccsetup.imr1 = 0x00;
			esccsetup.pvr = 0x0;
			esccsetup.pim = 0xff;
			esccsetup.pcr = 0xe0;
			esccsetup.xad1 = 0xff;
			esccsetup.xad2 = 0xff;
			esccsetup.rah1 = 0xff;
			esccsetup.rah2 = 0xff;
			esccsetup.ral1 = 0xff;
			esccsetup.ral2 = 0xff;
			esccsetup.rlcr = 0x00;
			esccsetup.pre = 0x00;
			esccsetup.ccr4 = 0x00;
		}
		if((argv[4][0]=='A')||(argv[4][0]=='a'))
		{
			//make to use async settings here
			printf("ASYNC settings\r\n");
			esccsetup.mode = 0x08;
			esccsetup.timr = 0x1f;
			esccsetup.xbcl = 0x00;
			esccsetup.xbch = 0x00;
			esccsetup.ccr0 = 0x83;   //async
			esccsetup.ccr1 = 0x1f; //bit clock rate =16, clock mode 7
			esccsetup.ccr2 = 0x18;
			esccsetup.bgr = 0x00;	//16MHz / 16 / 1 = 1Mbit/sec
			esccsetup.iva = 0;
			esccsetup.ipc = 0x03;
			esccsetup.imr0 = 0x04;
			esccsetup.imr1 = 0x0;
			esccsetup.pvr = 0x0;
			esccsetup.pim = 0xff;
			esccsetup.pcr = 0xe0;
			esccsetup.tcr = 0x00;
			esccsetup.dafo = 0x00;	//N81
			esccsetup.rfc = 0x0c;	//1c stores parity/framing with data every other byte...
		}
		if((argv[4][0]=='B')||(argv[4][0]=='b'))
		{
			//make to use bisync settings here
			printf("BISYNC settings\r\n");
			esccsetup.mode = 0x3c;	//8bit bisync
			esccsetup.timr = 0x1f;
			esccsetup.synl = 0x00;   //first SYN char
			esccsetup.synh = 0x5e;	//second SYN char  
			esccsetup.tcr = 0xff;	//reset receive when line is idle (0xff)
			esccsetup.dafo = 0x00;   //no parity, 8 bit data
			esccsetup.rfc = 0x4d;	//don't save parity, don't store SYN char's 32 byte fifo trigger, TCD enabled
			esccsetup.ccr0 = 0x82;	//powerup, NRZ, BISYNC
			esccsetup.ccr1 = 0x16;	
			esccsetup.ccr2 = 0x38;	
			esccsetup.ccr3 = 0x00;	
			esccsetup.bgr = 0x07;	//16MHz / ((7+1) *2) =1Mbit/sec
			esccsetup.pre = 0x00;	//preamble (if enabled)
			esccsetup.iva = 0;
			esccsetup.ipc = 0x03;
			esccsetup.imr0 = 0x14;//disable cdsc
			esccsetup.imr1 = 0x0;
			esccsetup.pvr = 0x0;
			esccsetup.pim = 0xff;
			esccsetup.pcr = 0xe0;
			esccsetup.xbch = 0x00;
		}
	}

	esccsetup.n_rbufs = 10;
	esccsetup.n_rfsize_max = 4096;
	esccsetup.n_tbufs = 10;
	esccsetup.n_tfsize_max = 4096;

	//when called this IOCTL will set the registers of the escc and
	//reset the TX and RX machines (issuing a separate XRES and RHR command)
	//if the resets time out the function will return FALSE, otherwise it will
	//return TRUE
	t = DeviceIoControl(hDevice,IOCTL_ESCCDRV_SETUP,&esccsetup,sizeof(struct setup),NULL,0,&returnsize,NULL);
	if(t==TRUE)printf("SETTINGS SUCCESSFUL:%lu\n\r",returnsize);
	if(t==FALSE)printf("SETTINGS FAILED:%lu\n\r",returnsize);

	DeviceIoControl(hDevice,IOCTL_ESCCDRV_FLUSH_RX,NULL,0,NULL,0,&returnsize,NULL);
	DeviceIoControl(hDevice,IOCTL_ESCCDRV_FLUSH_TX,NULL,0,NULL,0,&returnsize,NULL);

	//now we enter the main loop of this thread        
	//all we are going to do is wait for a keyhit and when we 
	//get one we will fill a frame with that key and send it out
	//the escc using a WriteFile() to the escc device
	//if the [ESC] key is pressed the program will terminate
	do
	{
		if( (argv[4][0]=='B')||(argv[4][0]=='b') )
		{
			k = HUNT;
			DeviceIoControl(hDevice,IOCTL_ESCCDRV_CMDR,&k,sizeof(DWORD),NULL,0,&returnsize,NULL);
//			printf("HUNT command issued\n\r");
		}
		//here the user has pressed a key that was not t,r,i,p or [esc] so we will
		//take that character and fill a data buffer with it, then send that 
		//buffer out the escc as a frame.
		
		for(i=0;i<1024;i++)
		{
			data[i] = rand()&0xff;//;(char)i;//fill the frame with the key
			if((data[i] == (char)0xff)&&( (argv[4][0]=='B')||(argv[4][0]=='b') )) data[i] = (char)0x0aa;
		}


		t = ReadFile(hDevice,&rdata,nobytestoread,&nobytesread,&osr);
		
		if(t==FALSE)
		{
			lerror = GetLastError();
			if (lerror == ERROR_IO_PENDING) //overlapped io indicates that the IRP isn't done being processed yet (status hasn't changed)
			{
printf("blocked\n");
		


		t = WriteFile(hDevice,&data,nobytestowrite,&nobyteswritten,&os);//send the frame
		//		printf("WRITEFILE esccdrv%lu \n\r",nobyteswritten);//if nobyteswritten doesnt = sizeof(struct buf) something is wrong
		//		if(t==TRUE)printf("TX returned TRUE\n\r"); //if returned true then the IO request was started (txing has begun)
		if(t==FALSE)                    //if it returned FALSE then the IO request is queued (waiting for previous frame to get done sending)
		{                       //and we must wait until the os.event gets signaled before we try any more sending
			printf("TX returned FALSE\n\r");
			lerror = GetLastError();
			if (lerror == ERROR_IO_PENDING) //overlapped io indicates that the IRP isn't done being processed yet (status hasn't changed)
			{
				// wait for a second for this transmission to complete
				do
				{
					k = WaitForSingleObject( os.hEvent, 1000 );//1 second timeout
					if(k==WAIT_TIMEOUT)
					{
						printf("WRITE:TIMEOUT\r\n");
						//this will execute every 1 second
						//you could put a counter in here and if the 
						//driver takes an inordinate ammout of time
						//to complete, you could issue a flush TX command
						//and break out of this loop
					}
					if(k==WAIT_ABANDONED)
					{
						printf("WRITE:ABANDONED\r\n");
					}
				}while(k!=WAIT_OBJECT_0);
			}
			else printf("Write error #%d\n",lerror);
		}

//		Sleep(30);
		//start a read request by calling ReadFile() with the esccdevice handle
		//if it returns true then we received a frame 
		//if it returns false and ERROR_IO_PENDING then there are no
		//receive frames available so we wait until the overlapped struct
		//gets signaled.
				// wait for a receive frame to come in, note it will wait forever
				do
				{
					j = WaitForSingleObject( osr.hEvent, 1000 );//1 second timeout
					if(j==WAIT_TIMEOUT)
					{
						printf("READ:TIMEOUT\r\n");
						//this will execute every 1 second
						//you could put a counter in here and if the 
						//driver takes an inordinate ammout of time
						//to complete, you could issue a flush RX command
						//and break out of this loop
					}
					if(j==WAIT_ABANDONED)
					{
						printf("READ:ABANDONED\r\n");
					}
					
				}while(j!=WAIT_OBJECT_0);
				GetOverlappedResult(hDevice,&osr,&nobytesread,TRUE); //here to get the actual nobytesread!!!
			}
			else printf("Read error #%d",lerror);

			t=TRUE;
			
		}
		else
		{
		//printf("READ: returned TRUE!\n");
		}
		
		if( (argv[4][0]=='h')||(argv[4][0]=='H')||(argv[4][0]=='b')||(argv[4][0]=='B') )
		{
			//hdlc has one extra byte (RSTA) and bisync has one extra byte (TERMINATION character 0xff)
			
			if(nobytesread!=nobyteswritten+1)
			{
				printf("Byte Count ERROR rec:%d != sent:%d\n",nobytesread,nobyteswritten+1);
				error++;
			}
		}
		else
		{
			if(nobytesread!=nobyteswritten)
			{
				printf("Byte Count ERROR rec:%d != sent:%d\n",nobytesread,nobyteswritten);
				error++;
				DeviceIoControl(hDevice,IOCTL_ESCCDRV_FLUSH_RX,NULL,0,NULL,0,&returnsize,NULL);
				DeviceIoControl(hDevice,IOCTL_ESCCDRV_FLUSH_TX,NULL,0,NULL,0,&returnsize,NULL);

			}
		}
		
		if(nobytesread!=0)
		{
			if( (argv[4][0]=='h')||(argv[4][0]=='H')||(argv[4][0]=='b')||(argv[4][0]=='B') )
				{
					for(x=0;x<nobytesread-1;x++)
						if(rdata[x]!=data[x])	error++;
				}
				else 
				{
					//async
					for(x=0;x<nobytesread;x++)
						if(rdata[x]!=data[x])	error++;
				}
		}
		//printf("Found: %d errors\n",error);
		totalerror +=error;
		loop++;
		totalsent+=nobyteswritten;
		totalread+=nobytesread;
		printf("loop:%d\n",loop);
		
	}while(!_kbhit());

	getch();
	printf("Found %d errors out of %d frames\n",totalerror,loop);
	printf("Wrote %d bytes.\n",totalsent);
			if( (argv[4][0]=='h')||(argv[4][0]=='H')||(argv[4][0]=='b')||(argv[4][0]=='B') )
	{
		printf("Read %d bytes.\n",totalread-loop);
	}
	else
	{
		printf("Read %d bytes.\n",totalread);
	}

	//when we want to exit the program there is still the possibility that
	//a frame is being transmitted so we spin in this IOCTL function until
	//the driver reports that it is not transmitting
	//technically you could omit this if you are leaving for good
	//(ie are done computing for the day, and are about to shutdown NT)
	//but it might cause problems if the escc device is started again
	//without powering down, misc errors will occur when the device is 
	//re-opened as it was halted in mid transmitting
	//EXE interrupts are most likely
	//
	i = 0;
	j = 0;
	do
	{
		t = DeviceIoControl(hDevice,IOCTL_ESCCDRV_TX_ACTIVE,NULL,0,&j,sizeof(DWORD),&returnsize,NULL);
		//will return 0 in output buffer (j) if not active
		//will return 1 in output buffer (j) if active
	}while(j==1);//keep requesting until not active
	//carefull not to close the device while transmitting or receiving data

	Sleep(1000);
	CloseHandle (hDevice);// stops the escc from interrupting (ie shuts it down)

	printf("exiting program\n\r");          //exit message
}                                               //done

							 

