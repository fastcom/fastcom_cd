/*++

Copyright (c) 1998,1999 Commtech, Inc Wichita ,KS

Module Name:

    timetag.c

Abstract:

    A simple console app that will do receive only.
	Bare bones receive HDLC application.
	
	The TIME_TAG_FRAMES will do a KeQuerySystemTime() call followed by
	at gettsc() call (returns CPU TSC register).  The systemtime is 
	turned into a TIME_FIELDS struct via a call to RtlTimeToTimeFields() call
	This time value is GMT time. the result is stored in the received frame
	that is then packed as:
	
	SYSTEMTIME timetag;
	__int64 tsc_value;
	char data[];


	The systemtime value seems to only be updated every 15mS or so on my system
	(the DDK docs say that it is updated  "approx every 10mS")
	however if you have many frames received in close proximity to the 
	changeover, then the tsc_value can be associated with the systemtime
	value at the point of systemtime change, further calculations from that tsc count will be
	resonable.  If you don't have many frames being received (per 10~15mS timeframe)
	but you do occasionally receive two or more frames with the same timetag, you can
	take the tsc_count difference between the frames and get a very good
	time resolution between the frames (albeit both/all off from "true" systemtime anywhere from 0->15mS)
	
	If you need Local time use SystemTimeToTzSpecificLocalTime() to convert it.

Environment:

    user mode only

Notes:

    
Revision History:

    8/25/98             started
	2/13/03				modified to receive/display timetaged frames
	4/28/04		Modified for escc-pci-335
--*/



#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include "..\esccptest.h"


		

VOID main(int argc,char *argv[])
/*++

Routine Description:

Arguments:

Return Value:

--*/
{
	HANDLE hDevice;                 //handle to the escc driver/device
	struct clkset clk;
	
	struct setup esccsetup; //setup structure for initializing the escc registers (see escctest.h)
	char data[4096];                //character array for data storage (passing to and from the driver)
	DWORD nobytestoread;    //the number of bytes to read from the driver
	DWORD nobytesread;
	ULONG j,k;                              //temp vars
	DWORD returnsize;               //temp vars
	ULONG i;                                //temp vars
	ULONG timeout;
	BOOL t;                                 //temp vars
	OVERLAPPED  os;                         //overlapped structure for use in the receive routine
	DWORD rframecount;
	DWORD errorcount;
	char fname[64];
	SYSTEMTIME *tag;
	__int64 *tsc;
	char *buffer;
	ULONG  desfreq;	
	SYSTEMTIME taglast;
	__int64 tsclast = 0;
	double cpuspeed = 1000000000.0; //I was working on a dual 1GHz PIII, set appropriatly to your CPU clock
	double tdiff;
	
	memset(&taglast,0,sizeof(SYSTEMTIME));
	
	if(argc<2)
	{
		printf("usage:%s port\n",argv[0]);
		exit(1);
	}
	sprintf(fname,"\\\\.\\ESCC%d",atoi(argv[1]));
	
	memset( &os, 0, sizeof( OVERLAPPED ) ) ;        //wipe the overlapped struct
	
	// create I/O event used for overlapped structure
	
	os.hEvent = CreateEvent( NULL,    // no security
		TRUE,    // explicit reset req
		FALSE,   // initial event reset
		NULL ) ; // no name
	if (os.hEvent == NULL)
	{
		MessageBox( NULL, "Failed to create event for thread!", "main Error!",
			MB_ICONEXCLAMATION | MB_OK ) ;
		return; 
	}
	
	
	nobytestoread = 4096;    //this should allways be 4096, it is the maximum frame size returned from the driver at one time
	
    
	//this will start up the driver, it also gives us a handle to send
	//and receive data from the driver
	//note that it is created with the FILE_FLAG_OVERLAPPED set
	//if it is not then you must make sequential calls to the driver
	//(ie wait until one call has returned before making another call)
	//the read/write and status functions are set up to use overlapped, they 
	//will return immediatly with a ERROR_IO_PENDING or immediatly with the required data
	//
	
	
    hDevice = CreateFile (fname,
		GENERIC_READ | GENERIC_WRITE,
		0,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
		NULL
		);
	
    if (hDevice == INVALID_HANDLE_VALUE)
    {
		//for some reason the driver won't load or isn't loaded
		printf ("Can't get a handle to esccdrv\n");
		exit(1);
		//abort and leave here!!!
	}
	
	
	desfreq = 16000000;	//16MHz clock / 16 = 1Mbit/sec
	if(DeviceIoControl(hDevice,IOCTL_ESCCDRV_SET_FREQ,&desfreq,sizeof(ULONG),NULL,0,&returnsize,NULL))
	{
		printf("Actual Frequency Set to: %d\n",desfreq);
	}
	else printf("failed:%8.8x\r\n",GetLastError());
	
	
	
	//to initialize the escc to esccsetup parameters
	//this IOCTL function must be called to setup the escc internal 
	//registers prior to calling any of the read/write functions
	//for information reguarding what each register does refer to 
	//the SIEMENS 82532 data sheet.
	//
	
	//make to use HDLC settings here
	esccsetup.mode = 0x88;//transparent mode 0 receiver active
	esccsetup.timr = 0x1f;//not used
	esccsetup.xbcl = 0x00;//not used
	esccsetup.xbch = 0x00;//not using DMA note DMAR must = DMAT = 0 in ntinstall
	esccsetup.ccr0 = 0x80;//power up, NRZ, HDLC mode, no master clock	
	esccsetup.ccr1 = 0x16;//clock mode 6b output TxClk using BGR/16, input using DPLL
	esccsetup.ccr2 = 0x18;//(BDF=0, forces inputclock/1 as output of bgr)
	esccsetup.ccr3 = 0x00;//CRC appended to data
	esccsetup.ccr3 = 0x00;
	esccsetup.bgr = 0x00;
	esccsetup.iva = 0;
	esccsetup.ipc = 0x03;
	esccsetup.imr0 = 0x04;
	esccsetup.imr1 = 0x00;
	esccsetup.pvr = 0x0;
	esccsetup.pim = 0xff;
	esccsetup.pcr = 0xe0;
	esccsetup.xad1 = 0xff;
	esccsetup.xad2 = 0xff;
	esccsetup.rah1 = 0xff;
	esccsetup.rah2 = 0xff;
	esccsetup.ral1 = 0xff;
	esccsetup.ral2 = 0xff;
	esccsetup.rlcr = 0x00;
	esccsetup.pre = 0x00;
	esccsetup.ccr4 = 0x00;
	
	esccsetup.n_rbufs = 20;//20 receive buffers
	esccsetup.n_tbufs = 2;//2 transmit buffers
	esccsetup.n_rfsize_max = 4096;//size of receive buffers (max)
	esccsetup.n_tfsize_max = 4096;//size of transmit buffers (max)
	
	
	//when called this IOCTL will set the registers of the escc and
	//reset the TX and RX machines (issuing a separate XRES and RHR command)
	//if the resets time out the function will return FALSE, otherwise it will
	//return TRUE
	t = DeviceIoControl(hDevice,IOCTL_ESCCDRV_SETUP,&esccsetup,sizeof(struct setup),NULL,0,&returnsize,NULL);
	if(t==TRUE)printf("SETTINGS SUCCESSFUL:%lu\n\r",returnsize);
	if(t==FALSE)
	{
		printf("SETTINGS FAILED:%lu\n\r",returnsize);
		exit(1);
	}
	
	//other options are TIME_TAG_RME and TIME_TAG_SYN and TIME_TAG_OFF
	i=TIME_TAG_RFS;//set to take timestamp at receive frame start interrupt (two bytes after opening flag)
	
	t = DeviceIoControl(hDevice,IOCTL_ESCCDRV_TIMETAG,&i,sizeof(ULONG),NULL,0,&returnsize,NULL);
	if(t==TRUE)printf("TimeTag Init \n\r");
	if(t==FALSE)
	{
		printf("TimeTag init FAILED:%lx\n\r",GetLastError());
		exit(1);
	}
	
	errorcount = 0;
	rframecount= 0;
start:
	do
	{
		
		//start a read request by calling ReadFile() with the esccdevice handle
		//if it returns true then we received a frame 
		//if it returns false and ERROR_IO_PENDING then there are no
		//receive frames available so we wait until the overlapped struct
		//gets signaled. (indicating that a frame has been received)
		timeout = 0;
		t = ReadFile(hDevice,&data,nobytestoread,&nobytesread,&os);
		//if this returns true then your system is not reading the frames from the 
		//driver as fast as they are coming in.
		//if it returns false then the system is processing the frames quick enough
		
		if(t==FALSE)
		{
			if (GetLastError() == ERROR_IO_PENDING)
			{
				// wait for a receive frame to come in
				do
				{
					j = WaitForSingleObject( os.hEvent, 1000 );//1 second timeout
					if(j==WAIT_TIMEOUT)
					{
						//this will execute every 1 second that a frame is not received
						//you could put a counter in here and if the 
						//driver takes an inordinate ammout of time
						//to complete, you could issue a flush RX command
						//and break out of this loop
						timeout++;
						if(timeout > 60) 
						{
							printf("no data received for 1 minute...aborting\r\n");
							goto quitprog;
						}
						if(kbhit())
						{
							if(getch()==27)
							{
								printf("exiting on keypress\r\n");
								goto quitprog;
							}
						}
					}
					if(j==WAIT_ABANDONED)
					{
					}
					
				}while(j!=WAIT_OBJECT_0);//stay here until we get signaled
				
				
			}
		}                                                     
		GetOverlappedResult(hDevice,&os,&nobytesread,TRUE); //here to get the actual nobytesread!!!
		if(nobytesread!=0)
		{
			if((data[nobytesread-1]&0x20)!=0x20) errorcount++;//last byte in received frame is contents of RSTA register, this is a check of the CRC if CRC fails then errorcount increments
			rframecount++;
			//to maximize speed you should not print the data to the screen, but 
			//rather insert your processing on the received data here.
			
			//printf("%lu %lu \r",rframecount,errorcount);
			printf("received %u bytes:\n\r",nobytesread);    //display the number of bytes received
			
			tag = (SYSTEMTIME *)&data[0];
			tsc = (__int64 *)&data[sizeof(SYSTEMTIME)];
			buffer = (char *)&data[sizeof(SYSTEMTIME)+sizeof(__int64)];
			
			if((tag->wMonth==taglast.wMonth)&&(tag->wYear==taglast.wYear)&&
				(tag->wDay==taglast.wDay)&&(tag->wHour==taglast.wHour)&&
				(tag->wMinute==taglast.wMinute)&&(tag->wSecond==taglast.wSecond)&&
				(tag->wMilliseconds==taglast.wMilliseconds))
			{
			}
			else
			{
				//	printf("tag update\n");
				memcpy(&taglast,tag,sizeof(SYSTEMTIME));
				tsclast = tsc[0];
			}
			//			printf("%I64u %I64u %.2f\n",tsc[0],tsclast,cpuspeed);
			tdiff = (double)(tsc[0]-tsclast);
			//			printf("tdiff:%.2f\n",tdiff);
			tdiff = (tdiff*1000000.0)/cpuspeed;
			//			printf("tdiff:%.2f\n",tdiff);
			
			printf("%d/%d/%d %d:%d.%d.%d (%I64u)--+[%.2fuS]",tag->wMonth,tag->wDay,tag->wYear,tag->wHour,tag->wMinute,tag->wSecond,tag->wMilliseconds,tsc[0],tdiff);
			
			for(i=0;i<nobytesread-(sizeof(SYSTEMTIME)+sizeof(__int64));i++)printf("%x:",buffer[i]&0xff);  //display the buffer
			printf("\n\r");
			
			DeviceIoControl(hDevice,IOCTL_ESCCDRV_IMMEDIATE_STATUS,NULL,0,&k,sizeof(DWORD),&returnsize,NULL);
			if((k&~ST_RX_DONE)!=0) printf("\r\nSTATUS: %lx\r\n",k);
			//you could decode the returned here 
			//they are the ST_XXXXXX defines in the .h file
			//basically if you get a ST_OVF or a ST_RFO then you are having
			//problems getting the data through the system
			//(the RFO is a hardware overflow)
			//the OVF is a software (driver buffers) overflow.
		}
		else printf("received 0 bytes\r\n");
		
	}while(!kbhit());         
	j = getch();
	if(j!=27) goto start;
quitprog:
	printf("framecount, errorcount\r\n");
	printf("%lu %lu \r",rframecount,errorcount);//display received framecount +#crc errors
	printf("\r\n");
	
	
	//other options are TIME_TAG_RME and TIME_TAG_SYN and TIME_TAG_OFF
	i=TIME_TAG_OFF;//turn off timetagging
	
	t = DeviceIoControl(hDevice,IOCTL_ESCCDRV_TIMETAG,&i,sizeof(ULONG),NULL,0,&returnsize,NULL);
	if(t==TRUE)printf("TimeTag Init \n\r");
	if(t==FALSE)
	{
		printf("TimeTag init FAILED:%lx\n\r",GetLastError());
		exit(1);
	}
	
	CloseHandle (os.hEvent);
	CloseHandle (hDevice);// stops the escc from interrupting (ie shuts it down)
	
}                                               //done

							 


