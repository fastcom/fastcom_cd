/* $Id$ */
/*
Copyright(C) 2005, Commtech, Inc.
bisync_pattern_rx.c -- user code to receive a stream 0x969681... with 2098 bytes of data from the 0x81 on, then an indeterminate ammount of 0x96's of idle before the next start



 usage:
  bisync_pattern_rx port 

--ported to ESCC 2/25/2005

*/


#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "..\esccptest.h"

int main(int argc, char *argv[])
{
	HANDLE rDevice;
	ULONG t;
	ULONG k;
	DWORD nobytesread;
	char *rdata;
	ULONG size;
	OVERLAPPED  rq;
	int j,error;
	unsigned i;
	ULONG totalsent;
	ULONG totalread;
	ULONG totalerror;
	ULONG loop;
    char devname[25];
	SETUP esccsetup;
	BISYNCSTART bsp;
	ULONG temp;
	ULONG rcnt, rcnt_last;
	ULONG ret;
	rcnt=0;
	rcnt_last=0;
	if(argc<2)
	{
		printf("usage:\n");
		printf("bisync_pattern_rx port \n");
		exit(1);
	}
	size = 4096;	//must be at least 2144, (2099 takes 66 x 32byte locations, +32 bytes for the next interrupt of space)
					//anything bigger is just not used
	
	sprintf(devname,"\\\\.\\ESCC%d",atoi(argv[1]));
	printf("devicename:%s\n",devname);
	
	memset( &rq, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
	rq.hEvent = CreateEvent( NULL,    // no security
		TRUE,    // explicit reset req
		FALSE,   // initial event reset
		NULL ) ; // no name
	if (rq.hEvent == NULL)
	{
		printf("Failed to create event for read\n");
		exit(1);
	}
	rDevice = CreateFile (devname,
		GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL,
		OPEN_ALWAYS,
		FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
		NULL
		);
	
    if (rDevice == INVALID_HANDLE_VALUE)
    {
		//for some reason the driver won't load or isn't loaded
		printf ("Can't get a handle to %s\n",devname);
		CloseHandle(rq.hEvent);
		exit(1);
		//abort and leave here!!!
	}
	
	temp = 2099;
	DeviceIoControl(rDevice,IOCTL_ESCCDRV_SET_BISYNC_SIZE_CUTOFF_SIZE,&temp,sizeof(ULONG),NULL,0,&t,NULL);

	temp = 0x80000001; //enable the bisync cutoff length, and hunt on cutoff
	DeviceIoControl(rDevice,IOCTL_ESCCDRV_BISYNC_SIZE_CUTOFF_EN,&temp,sizeof(ULONG),NULL,0,&t,NULL);
	
	
	
	
	memset(&esccsetup,0,sizeof(SETUP));//wipe the settings structure (default everything to 0)
	
		esccsetup.mode = 0x3c;	//16bit bisync
		esccsetup.timr = 0x1f;
		esccsetup.synl = 0x96;   //first SYN char framesync is 0x9681....
		esccsetup.synh = 0x81;	 //second SYN char  
		esccsetup.tcr = 0xff;	 //not used
		esccsetup.dafo = 0x00;   //no parity, 8 bit data
		esccsetup.rfc = 0x6c;	 //don't save parity, store SYN char's, 32 byte fifo trigger, TCD disabled
		esccsetup.ccr0 = 0x82;	 //powerup, NRZ, BISYNC
		esccsetup.ccr1 = 0x10;	 //clock mode 0b
		esccsetup.ccr2 = 0x78;	 //assumed inclk=18.432MHz, bitrate = 19200
		esccsetup.ccr3 = 0x00;	 //not used
		esccsetup.bgr = 0xDF;    // rate = inclk/(n+1)*2 = 18432000/((479+1)*2) = 19200 bps
		esccsetup.pre = 0x00;	//preamble (not used)
		esccsetup.iva = 0;		//required
		esccsetup.ipc = 0x03;	//required
		esccsetup.imr0 = 0x04;  //disable cdsc
		esccsetup.imr1 = 0x0;	//
		esccsetup.pvr = 0x0;	//
		esccsetup.pim = 0xff;	//
		esccsetup.pcr = 0xe0;	//required
		esccsetup.xbch = 0x00;	//not used

	esccsetup.n_rbufs = 20;		//
	esccsetup.n_rfsize_max = 4096;//must match "size" above
	esccsetup.n_tbufs = 10;
	esccsetup.n_tfsize_max = 4096;
	
	t = DeviceIoControl(rDevice,IOCTL_ESCCDRV_SETUP,&esccsetup,sizeof(struct setup),NULL,0,&ret,NULL);
	if(t==TRUE)printf("SETTINGS SUCCESSFUL:%lu\n\r",ret);
	if(t==FALSE)printf("SETTINGS FAILED:%lu\n\r",ret);
	
	
	//allocate memory for read/write
	rdata = (char*)malloc(size+1);
	if(rdata==NULL)
	{
		printf("cannot allocate memory for data area\n");
		CloseHandle(rDevice);
		CloseHandle(rq.hEvent);
		exit(1);
	}
	
	
	
	//issue FLUSH_RX
	DeviceIoControl(rDevice,IOCTL_ESCCDRV_FLUSH_RX,NULL,0,NULL,0,&ret,NULL);
	//issue HUNT
		k=HUNT;
		DeviceIoControl(rDevice,IOCTL_ESCCDRV_CMDR,&k,sizeof(ULONG),NULL,0,&ret,NULL);
	
	totalerror=0;
	totalsent=0;
	totalread=0;
	loop=0;
	
	while(!kbhit())//main loop, do until someone hits a key
	{
		error=0;
		t = ReadFile(rDevice,&rdata[0],size,&nobytesread,&rq);
		if(t==FALSE)  
		{
			//	printf("read blocked\n");
			t=GetLastError();
			if(t==ERROR_IO_PENDING)
			{
				do
				{
					j = WaitForSingleObject( rq.hEvent, 5000 );//5 second timeout -- must be greater than size*8*(1/bitrate)*1000
					if(j==WAIT_TIMEOUT)
					{
						if(kbhit()) goto done;
						printf("Reciever Timeout.\r\n");
					}
					if(j==WAIT_ABANDONED)
					{
						printf("Reciever Abandoned.\r\n");
						goto close;
					}
				} while(j!=WAIT_OBJECT_0);
				GetOverlappedResult(rDevice,&rq,&nobytesread,TRUE);
			}
			else printf("READ ERROR: #%x\n",t);
		}
		if(nobytesread!=2099)//we are expecting this with the cutoff active
		{
			printf("received:%lu, expected %lu\n",nobytesread,size);
		}
		if(nobytesread!=0)
		{
			for(i=0;i<20;i++) printf("%2.2x:",rdata[i]&0xff);//display the first 20 bytes of incomming data (so we can see the counter)
			printf("\n");
			//this bit depends on the bisync_pattern_tx code, it is sending an ascii counter in the 
			//bytes after the sync, we just make sure that all reads have counts that make sense
			//this should display on the first read (as the count is obviously not going to be right)
			//and any time that the sequence gets out of sorts (stopped transmit, missing frames etc)
			rcnt=atol(&rdata[3]);
			if(rcnt!=rcnt_last+1) printf("out of sync:%d != %d\n",rcnt,rcnt_last+1);
			rcnt_last = rcnt;
		}
		loop++;
		totalread+=nobytesread;
	}
done:
	getch();//clear the keyboard buffer
	printf("Read  %lu bytes\n",totalread);
	printf("Count %lu\n",loop);
	
close:
	temp = 0; //disable the bisync cutoff length, and hunt on cutoff
	DeviceIoControl(rDevice,IOCTL_ESCCDRV_BISYNC_SIZE_CUTOFF_EN,&temp,sizeof(ULONG),NULL,0,&t,NULL);

	free(rdata);//done with the read buffer
	CloseHandle(rDevice);//done with the ESCC port
	CloseHandle(rq.hEvent);//done with the overlapped event
	return 0; //bye
}
/* $Id$ */