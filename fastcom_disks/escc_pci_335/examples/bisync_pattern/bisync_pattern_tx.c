/* $Id$ */
/*
Copyright(c) 2005, Commtech, Inc.
bisync_pattern_tx.c -- user program to send pattern 0x969681(counter)(random data)(300x 0x96)
					-- in a total repetative count of 2400 bytes

 usage:
  bisync_pattern_tx port 

port is the ESCC port to use

  2/25/2005 -ported to ESCC
*/


#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "..\esccptest.h"

int main(int argc, char *argv[])
{
	HANDLE wDevice;/* Handle for the ESCC port */
	ULONG t;
	DWORD nobyteswritten;
	char *tdata;
	ULONG size;
	OVERLAPPED  wq;
	int j,x,error,tosend;
	ULONG totalsent;
	ULONG totalread;
	ULONG totalerror;
	ULONG loop;
    char devname[25];
	FILE *fout=NULL;
	unsigned long fcount;
	SETUP esccsetup;
	ULONG passval[2];
	ULONG out;
	ULONG returnsize;
	ULONG temp;
	int i;
	
	srand( (unsigned)time( NULL ) );//seed the rng
	
	if(argc<2)
	{
		printf("usage:\n");
		printf("bisync_pattern_tx port \n");
		exit(1);
	}
	size = 2400;//block size to send to the port
	
    sprintf(devname,"\\\\.\\ESCC%d",atoi(argv[1]));
	printf("devicename:%s\n",devname);
	printf("blocksize:%lu\n",size);
	
	memset( &wq, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
	wq.hEvent = CreateEvent( NULL,    // no security
		TRUE,    // explicit reset req
		FALSE,   // initial event reset
		NULL ) ; // no name
	if (wq.hEvent == NULL)
	{
		printf("Failed to create event for thread!\n");
		return 1; 
	}
	//open the ESCC port
	wDevice = CreateFile (devname,
		GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL,
		OPEN_ALWAYS,
		FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
		NULL
		);
    if (wDevice == INVALID_HANDLE_VALUE)
    {
		printf ("Can't get a handle to esccdrv %s\n",devname);
		exit(1);
		//abort and leave here!!!
	}
	
	passval[0] = 18432000;
	if(	DeviceIoControl(wDevice,IOCTL_ESCCDRV_SET_FREQ,&passval[0],sizeof(ULONG),&out,sizeof(ULONG),&temp,NULL))
	{
		printf("frequency set: %d\n",passval[0]);
	}
	else printf("Problem Setting clock frequency.\n");/* check owners manual */       
	
	
	memset(&esccsetup,0,sizeof(SETUP));
		esccsetup.mode = 0xC0;
		esccsetup.timr = 0x1f;
		esccsetup.xbcl = 0x00;
		esccsetup.xbch = 0x00;
		esccsetup.ccr0 = 0x80;
		esccsetup.ccr1 = 0x17; 
		esccsetup.ccr2 = 0x78; 
		esccsetup.ccr3 = 0x00;
		esccsetup.bgr = 0xdf;
		esccsetup.iva = 0;
		esccsetup.ipc = 0x03;
		esccsetup.imr0 = 0x44;//disable cdsc
		esccsetup.imr1 = 0x0;
		esccsetup.pvr = 0x0;
		esccsetup.pim = 0xff;
		esccsetup.pcr = 0xe0;
		esccsetup.xad1 = 0xff;
		esccsetup.xad2 = 0xff;
		esccsetup.rah1 = 0xff;
		esccsetup.rah2 = 0xff;
		esccsetup.ral1 = 0xff;
		esccsetup.ral2 = 0xff;
		esccsetup.rlcr = 0x00;
		esccsetup.pre = 0x00;
		esccsetup.ccr4 = 0x00;


	esccsetup.n_rbufs = 10;
	esccsetup.n_rfsize_max = 4096;
	esccsetup.n_tbufs = 100;
	esccsetup.n_tfsize_max = 4096;

	t = DeviceIoControl(wDevice,IOCTL_ESCCDRV_SETUP,&esccsetup,sizeof(struct setup),NULL,0,&returnsize,NULL);
	if(t==TRUE)printf("SETTINGS SUCCESSFUL:%lu\n\r",returnsize);
	if(t==FALSE)printf("SETTINGS FAILED:%lu\n\r",returnsize);

	
	tdata = (char*)malloc(size+1);//allocate transmit data array
	if(tdata==NULL)
	{
		printf("unable to allocate data buffer\n");
		exit(1);
	}
	
	//start fresh
		DeviceIoControl(wDevice,IOCTL_ESCCDRV_FLUSH_TX,NULL,0,NULL,0,&t,NULL);
	
	totalerror=0;
	totalsent=0;
	totalread=0;
	loop=0;
	fcount=0;
	
	while(!kbhit())//main loop, do this until someone hits a key
	{
		error=0;
		// prefill the data array with randomness
		tosend = size;
		for(x=0;x<tosend;x++) tdata[x]=(UCHAR)(rand());
		
		//then fill in our frames with the start sequence and a frame counter
			tdata[0] = (UCHAR)0x96; //will be seen as SYNL
			tdata[1] = (UCHAR)0x96; //will be seen as SYNH
			tdata[2] = (UCHAR)0x81; //will be seen as the third byte of our pattern match
			sprintf(&tdata[3],"%8.8lu",fcount++);//ascii frame counter (to detect frame gaps)
			for(i=2100;i<tosend;i++) tdata[i]=(UCHAR)0x96;
		//send the block
		t = WriteFile(wDevice,&tdata[0],tosend,&nobyteswritten,&wq);
		if(t==FALSE)  
		{
			t=GetLastError();
			if(t==ERROR_IO_PENDING)
			{
				do
				{
					j = WaitForSingleObject( wq.hEvent, 10000 );//10 second timeout -- must be larger than size*8*(1/bitrate)*1000
					if(j==WAIT_TIMEOUT)
					{
						printf("Transmitter Timeout, Locked up?\r\n");
					}
					if(j==WAIT_ABANDONED)
					{
						printf("Tx Abandoned.\r\n");
						goto close;
					}
				} while(j!=WAIT_OBJECT_0);
				GetOverlappedResult(wDevice,&wq,&nobyteswritten,TRUE);
			}
			else printf("WRITE ERROR: #%d\n",t);
		}
		if(nobyteswritten!=size)
		{
			printf("unexpected actual:%lu, sent:%lu\n",nobyteswritten,size); 
		}
		loop++;
		totalsent+=nobyteswritten;
	}
	getch();//clear the keyboard buffer
	printf("Wrote %lu bytes\n",totalsent);
	printf("count %lu\n",loop);
	
close:
	free(tdata);//done with the transmit data buffer
	CloseHandle(wq.hEvent);//done with the overlapped event
	CloseHandle (wDevice);//done with the ESCC port
	return 0;//bye
}
/* $Id$ */
