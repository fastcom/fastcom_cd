#include "winsock2.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#define EP_PORT 5003
#define MAX_PENDING_CONNECTS 4

int main(int argc, char *argv[])
{
	SOCKET m_theSocket;
	SOCKET a_theSocket;
	char buf[4096];
	char *cb;
	int j;
	int i;
	WORD wVersionRequested;
	WSADATA wsaData;
	int err;
	struct sockaddr_in   tcpaddr;
	struct sockaddr_in   saddr;
	struct sockaddr_in   inaddr;
	int saddr_len;
	int inaddr_len;
	
	wVersionRequested = MAKEWORD( 2, 2 );
	
	err = WSAStartup( wVersionRequested, &wsaData );
	if ( err != 0 ) {
		/* Tell the user that we could not find a usable */
		/* WinSock DLL.                                  */
		exit(1);
	}
	m_theSocket = socket( AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (m_theSocket == INVALID_SOCKET)
	{
		printf("cannot get a socket\n");
		exit(1);
	};
	
	
	if(argc>1)
	{
		//if an ip address is given on the command line, then we are a client, and we will 
		//try to connect up to a server.
		
		tcpaddr.sin_addr.S_un.S_addr = inet_addr(argv[1]);
		tcpaddr.sin_family = AF_INET;
		tcpaddr.sin_port = htons(EP_PORT);
		if(connect(m_theSocket,(SOCKADDR*)&tcpaddr,sizeof(tcpaddr))==SOCKET_ERROR)
		{
			printf("error in connect\n");
			exit(1);
		}
		//here we have a connected socket, we can send/receive to/from it now
		//fire up the receive and transmit threads here
		
		sprintf(buf,"hello world\r\n");
		if(send(m_theSocket,buf,strlen(buf),0)==SOCKET_ERROR)
		{
			printf("error in send\n");
			exit(1);
		}
		shutdown(m_theSocket,SD_BOTH);
		closesocket(m_theSocket);
		WSACleanup();
		exit(0);
	}
	else
	{
		//otherwise we are a server, so setup to receive incomming socket connections
		
		tcpaddr.sin_family = AF_INET;
		tcpaddr.sin_port = htons(EP_PORT);
		tcpaddr.sin_addr.S_un.S_addr = htonl(INADDR_ANY);
		if (bind(m_theSocket,(SOCKADDR*)&tcpaddr, sizeof(tcpaddr)) == SOCKET_ERROR)
		{
			printf("unable to bind %8.8x\n",WSAGetLastError());
			
			exit(1);
		}
		else
		{
			if (listen(m_theSocket, MAX_PENDING_CONNECTS ) == SOCKET_ERROR)
			{
				printf("error while listen\n");
				exit(1);
			}
			saddr_len = sizeof(saddr);
			getsockname(m_theSocket,(SOCKADDR*)&saddr,&saddr_len);
			
			cb = inet_ntoa(saddr.sin_addr);
			printf("server, listening on : %s\r\n",cb);
			//at this point we are listening...what happens when someone connects?
			inaddr_len = sizeof(inaddr);
			a_theSocket = accept(m_theSocket,(SOCKADDR*)&inaddr,&inaddr_len);
			if(a_theSocket==INVALID_SOCKET)
			{
				printf("error accepting\n");
				exit(1);
			}
			else
			{
				
				cb = inet_ntoa(inaddr.sin_addr);
				printf("accepted: %s\r\n",cb);
				//at this point we have a client connected, so fire up the receive/transmit threads
				
				i = recv(a_theSocket,buf,sizeof(buf),0);
				
				printf("received:\r\n");
				for(j=0;j<i;j++) printf("%c",buf[j]);
				printf("\r\n");
				shutdown(a_theSocket,SD_BOTH);
				closesocket(a_theSocket);
			}
		}
	}
	closesocket(m_theSocket);
	WSACleanup();
};