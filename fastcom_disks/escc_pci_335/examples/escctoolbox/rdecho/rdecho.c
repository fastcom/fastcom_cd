/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
rdecho.c -- user mode function to turn on/off receive echo cancel function using the escctoolbox.dll

  Receive echo cancel is useful in some 2-wire 485 networks where it may be useful
  for the receiver to be disabled during transmits so that a station does not receive
  what it transmits.

To use the escctoolbox.DLL, you must do three things:

1. #include escctoolbox.h in your source code
2. Add the file escctoolbox.lib to your linker's Output/Library modules.  To do this in
   Visual Studio, Click on Settings in the Project menu.  In the Project settings dialog
   click on the Link tab.  In the Category drop down, choose General.  In the 
   Output/library modules box, add the path to escctoolbox.lib.  It is easiest to copy the 
   file to your working directory and just add "escctoolbox.lib"
3. The escctoolbox.dll file must reside in the same directory as your compiled program .exe

usage:
 rdecho port 0|1

 The port can be any valid escc port (0,1,2...) 

*/

#include "windows.h"
#include "..\escctoolbox.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"


int main(int argc, char *argv[])
{
	unsigned long port;
	DWORD ret;
	DWORD onoff;
	
	if(argc<3)
	{
		printf("usage:%s port [0|1]\r\n",argv[0]);
		exit(1);
	}
	port = atoi(argv[1]);
	onoff = atoi(argv[2]);
	if((ret = ESCCToolBox_Create(port))<0)		
	{
		printf("create:%d\n",ret);
		exit(1);
	}
	if((ret =ESCCToolBox_Set_RD_Echo_Cancel(port,onoff))<0)
	{
		printf("error in Set_RD_Echo_Cancel:%d\n",ret);
	}
	else 
	{
		if(onoff==0) printf("RD allways on\r\n");	//Disable RxEcho Cancel
		
		else printf("RD disabled while SD active\r\n");	//Enable RxEcho Cancel
	}
	
	ESCCToolBox_Destroy(port);
}
/* $Id$ */