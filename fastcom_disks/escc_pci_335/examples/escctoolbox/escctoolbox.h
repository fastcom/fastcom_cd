
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the ESCCTOOLBOX_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// ESCCTOOLBOX_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef ESCCTOOLBOX_EXPORTS
#define ESCCTOOLBOX_API __declspec(dllexport)
#else
#define ESCCTOOLBOX_API __declspec(dllimport)
#endif

#include "esccptest.h"

#define BOARD_UNKNOWN				0
#define BOARD_ESCC					1
#define BOARD_ESCC104				2
#define BOARD_ESCC104ET				3
#define BOARD_ESCCPCI				4
#define BOARD_ESCCPCMCIA			5	
#define BOARD_ESCCPCIV3				6
#define BOARD_ESCCPCIISO1			7

#define AUTOCLOCK	0
#define ICD2053B	1
#define ICS307		2
#define FS6131		3

#define MAXPORTS 10

#ifdef __cplusplus
extern "C" 
{
#endif
#define INPUT
#define OUTPUT

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Create(INPUT DWORD port);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Destroy(INPUT DWORD port);

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Block_Multiple_Io(INPUT DWORD port,INPUT DWORD onoff);

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Set_Clock(INPUT DWORD port,INPUT DWORD frequency);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Get_Clock(INPUT DWORD port);

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Read_Frame(INPUT DWORD port,OUTPUT char * rbuf,INPUT DWORD szrbuf,OUTPUT DWORD *retbytes,INPUT DWORD timeout);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Write_Frame(INPUT DWORD port,INPUT char * tbuf,INPUT DWORD numbytes,INPUT DWORD timeout);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Get_Status(INPUT DWORD port,OUTPUT DWORD *status,INPUT DWORD mask,INPUT DWORD timeout);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Get_IStatus(INPUT DWORD port,OUTPUT DWORD *status);

typedef void (__stdcall *readcallbackfn)(DWORD port,char *rdata,DWORD retbytes);
/* these are broken

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Register_Read_Callback(DWORD port,readcallbackfn fnptr,DWORD szread);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Register_Write_Callback(DWORD port,void (__stdcall *callbackfunction)(DWORD port,char * tbuf,DWORD *bytestowrite),DWORD szmaxwrite);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Register_Status_Callback(DWORD port,DWORD mask,void (__stdcall *callbackfunction)(DWORD port,DWORD status));
*/

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Flush_RX(INPUT DWORD port);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Flush_TX(INPUT DWORD port);

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Read_Register(INPUT DWORD port,INPUT DWORD regno);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Write_Register(INPUT DWORD port,INPUT DWORD regno,INPUT DWORD value);


ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Configure(INPUT DWORD port,INPUT SETUP *settings);

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Set_Txclk_TT(INPUT DWORD port,INPUT DWORD onoff);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Set_Txclk_ST(INPUT DWORD port,INPUT DWORD onoff);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Set_SD_485(INPUT DWORD port,INPUT DWORD onoff);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Set_TT_485(INPUT DWORD port,INPUT DWORD onoff);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Set_RD_Echo_Cancel(INPUT DWORD port,INPUT DWORD onoff);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Set_CTS_Disable(INPUT DWORD port,INPUT DWORD onoff);

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Bisync_Start_Pattern_Enable(INPUT DWORD port,INPUT DWORD onoff);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Bisync_Cutoff_Enable(INPUT DWORD port,INPUT DWORD onoff);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_LSB2MSB_Convert_Enable(INPUT DWORD port,INPUT DWORD onoff);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Bisync_End_Pattern_Enable(INPUT DWORD port,INPUT DWORD onoff);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Bisync_Start_Pattern(INPUT DWORD port,INPUT struct bisync_start_pattern pattern);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Bisync_Size_Cutoff(INPUT DWORD port,INPUT DWORD size);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Bisync_End_Pattern(INPUT DWORD port,INPUT struct bisync_start_pattern pattern);

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Postamble_Enable(INPUT DWORD port,INPUT DWORD onoff);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Postamble_Delay(INPUT DWORD port,INPUT DWORD delay);

#ifdef __cplusplus
}
#endif
