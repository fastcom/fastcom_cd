/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
sd485.c - user mode function to enable/disable RS485 on the SD signal using the escctoolbox.dll
		  when enabled, the tansmitter will be turned off when not transmitting so that
		  it does not drive the line

To use the escctoolbox.DLL, you must do three things:

1. #include escctoolbox.h in your source code
2. Add the file escctoolbox.lib to your linker's Output/Library modules.  To do this in
   Visual Studio, Click on Settings in the Project menu.  In the Project settings dialog
   click on the Link tab.  In the Category drop down, choose General.  In the 
   Output/library modules box, add the path to escctoolbox.lib.  It is easiest to copy the 
   file to your working directory and just add "escctoolbox.lib"
3. The escctoolbox.dll file must reside in the same directory as your compiled program .exe

usage:
 sd485 port 0|1

 The port can be any valid escc port (0,1,2...) 

*/

#include "windows.h"
#include "..\escctoolbox.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"


int main(int argc, char *argv[])
{
	unsigned long port;
	DWORD ret;
	DWORD onoff;
	
	if(argc<3)
	{
		printf("usage:%s port [0|1]\r\n",argv[0]);
		exit(1);
	}
	port = atoi(argv[1]);
	onoff = atoi(argv[2]);

	if((ret = ESCCToolBox_Create(port))<0)
	{
		printf("create:%d\n",ret);
		exit(1);
	}

	if((ret =ESCCToolBox_Set_SD_485(port,onoff))<0)
	{
		printf("error in Set_SD_485:%d\n",ret);
	}

	else 
	{
		if(onoff==0) printf("SD set to 422 mode\r\n");
		else printf("SD set to 485 mode\r\n");
	}
	
	ESCCToolBox_Destroy(port);
}
/* $Id$ */