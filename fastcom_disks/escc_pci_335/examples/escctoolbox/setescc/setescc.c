/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
setescc.c -- user mode function to setup the ESCC registers using the escctoolbox.dll

To use the escctoolbox.DLL, you must do three things:

1. #include escctoolbox.h in your source code
2. Add the file escctoolbox.lib to your linker's Output/Library modules.  To do this in
   Visual Studio, Click on Settings in the Project menu.  In the Project settings dialog
   click on the Link tab.  In the Category drop down, choose General.  In the 
   Output/library modules box, add the path to escctoolbox.lib.  It is easiest to copy the 
   file to your working directory and just add "escctoolbox.lib"
3. The escctoolbox.dll file must reside in the same directory as your compiled program .exe

usage:
 esccset port setfile

 The port can be any valid escc port (0,1,2...).

 The setfile is a text file that contains the settings to use.  See the default 
 settings files 'hdlcset', 'asyncset' & 'bisyncset' for your chosen mode).

 This could be modified so that the settings values were set directly by your program, 
 instead of opening an external file.

*/


#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include "..\escctoolbox.h" /* user code header */


int main(int argc, char *argv[])
{
	int port;
	int i;
	FILE *fp;
	char inputline[100],*ptr;
	unsigned long value;
	DWORD ret;
	SETUP settings;
	
	
	if(argc!=3)
	{
		printf("%s port settingsfile\n\n",argv[0]);
		printf(" The port is appended onto \\\\.\\ESCC\n");
		exit(1);
	}
	port=atoi(argv[1]);
	if((ret = ESCCToolBox_Create(port))<0)
	{
		printf("create:%d\n",ret);
		exit(1);
	}
	
	fp = fopen(argv[2],"r");
	if(fp==NULL)
	{
		printf("Cannot open file %s\n",argv[2]);
		exit(1);
	}
	
	memset(&settings,0,sizeof(SETUP));
	memset(inputline,0,sizeof(inputline));
	
	while(fgets(inputline,100,fp)!=NULL)
	{
		/* zero the comments so not to get confused with key words */
		/* # pound is the comment prefix */
		for(i=0;inputline[i]!='\n';i++)
		{
			if(inputline[i]=='#')
			{
				memset(&inputline[i],0, 100-i );	
				break;
			}
			
		}
		i = 0;
		while(inputline[i]!=0) 
		{
			inputline[i] = tolower(inputline[i]);
			i++;
		}
		
		if( (ptr = strchr(inputline,'='))!=NULL)
		{
			for(i=1;ptr[i]==' ' && ptr[i]!='\n';i++);
			value = (unsigned long)strtoul(ptr+i,NULL,16);
			//printf("value = 0x%lx\n",(long)value);
		}
		else	value=0;
		
		if(strncmp(inputline,"cmdr",4)==0) settings.cmdr = value;
		if(strncmp(inputline,"mode",4)==0) settings.mode = value;
		if(strncmp(inputline,"timr",4)==0) settings.timr = value;
		if(strncmp(inputline,"xbcl",4)==0) settings.xbcl = value;
		if(strncmp(inputline,"xbch",4)==0) settings.xbch = value;
		if(strncmp(inputline,"ccr0",4)==0) settings.ccr0 = value;
		if(strncmp(inputline,"ccr1",4)==0) settings.ccr1 = value;
		if(strncmp(inputline,"ccr2",4)==0) settings.ccr2 = value;
		if(strncmp(inputline,"ccr3",4)==0) settings.ccr3 = value;
		if(strncmp(inputline,"ccr4",4)==0) settings.ccr4 = value;
		if(strncmp(inputline,"tsax",4)==0) settings.tsax = value;
		if(strncmp(inputline,"tsar",4)==0) settings.tsar = value;
		if(strncmp(inputline,"xccr",4)==0) settings.xccr = value;
		if(strncmp(inputline,"rccr",4)==0) settings.rccr = value;
		if(strncmp(inputline,"bgr",3)==0) settings.bgr = value;
		if(strncmp(inputline,"iva",3)==0) settings.iva = value;
		if(strncmp(inputline,"ipc",3)==0) settings.ipc = value;
		if(strncmp(inputline,"imr0",4)==0) settings.imr0 = value;
		if(strncmp(inputline,"imr1",4)==0) settings.imr1 = value;
		if(strncmp(inputline,"pvr",3)==0) settings.pvr = value;
		if(strncmp(inputline,"pim",3)==0) settings.pim = value;
		if(strncmp(inputline,"pcr",3)==0) settings.pcr = value;
		if(strncmp(inputline,"xad1",4)==0) settings.xad1 = value;
		if(strncmp(inputline,"xad2",4)==0) settings.xad2 = value;
		if(strncmp(inputline,"rah1",4)==0) settings.rah1 = value;
		if(strncmp(inputline,"rah2",4)==0) settings.rah2 = value;
		if(strncmp(inputline,"ral1",4)==0) settings.ral1 = value;
		if(strncmp(inputline,"ral2",4)==0) settings.ral2 = value;
		if(strncmp(inputline,"rlcr",4)==0) settings.rlcr = value;
		if(strncmp(inputline,"aml",3)==0) settings.aml = value;
		if(strncmp(inputline,"amh",3)==0) settings.amh = value;
		if(strncmp(inputline,"pre",3)==0) settings.pre = value;
		if(strncmp(inputline,"xon",3)==0) settings.xon = value;
		if(strncmp(inputline,"xoff",4)==0) settings.xoff = value;
		if(strncmp(inputline,"tcr",3)==0) settings.tcr = value;
		if(strncmp(inputline,"dafo",4)==0) settings.dafo = value;
		if(strncmp(inputline,"rfc",3)==0) settings.rfc = value;
		if(strncmp(inputline,"tic",3)==0) settings.tic = value;
		if(strncmp(inputline,"mxn",3)==0) settings.mxn = value;
		if(strncmp(inputline,"mxf",3)==0) settings.mxf = value;
		if(strncmp(inputline,"synl",4)==0) settings.synl = value;
		if(strncmp(inputline,"synh",4)==0) settings.synh = value;
		if(strncmp(inputline,"rbufs",5)==0) settings.n_rbufs = value;
		if(strncmp(inputline,"tbufs",5)==0) settings.n_tbufs = value;
		if(strncmp(inputline,"rfsizemax",9)==0) settings.n_rfsize_max = value;
		if(strncmp(inputline,"tfsizemax",9)==0) settings.n_tfsize_max = value;
		memset(inputline,0,100);
	}
	
	if((ret=ESCCToolBox_Configure(port,&settings))<0)
	{
		printf("configure:%d\n",ret);
	}
	else	printf("Finished Settings.\n");	
	
	ESCCToolBox_Destroy(port);
	return 0;
}
/* $Id$ */