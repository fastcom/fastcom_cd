/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
write.c -- user mode function to write a text string to the escc port using the escctoolbox.dll

To use the escctoolbox.DLL, you must do three things:

1. #include escctoolbox.h in your source code
2. Add the file escctoolbox.lib to your linker's Output/Library modules.  To do this in
   Visual Studio, Click on Settings in the Project menu.  In the Project settings dialog
   click on the Link tab.  In the Category drop down, choose General.  In the 
   Output/library modules box, add the path to escctoolbox.lib.  It is easiest to copy the 
   file to your working directory and just add "escctoolbox.lib"
3. The escctoolbox.dll file must reside in the same directory as your compiled program .exe

usage:
 write port

 The port can be any valid escc port (0,1,2...) 
 
*/

#include "windows.h"
#include "..\escctoolbox.h"
#include "stdio.h"
#include "stdlib.h"


void main(int argc,char *argv[])
{
	DWORD ret;
	ULONG port;
	char buf[4096];
	ULONG count;
	
	if(argc<2)
	{
		printf("usage:%s port\r\n",argv[0]);
		exit(1);
	}
	port = atoi(argv[1]);
	if((ret = ESCCToolBox_Create(port))<0)
	{
		printf("create:%d\n",ret);
		exit(0);
	}
	ESCCToolBox_Flush_TX(port);
	printf("Enter text to send:\n");
	scanf("%s",&buf);
	count = strlen(buf);

/*	could also do something like this to send all ascii characters from 0 to 255:

	count=255;
	for(i=0;i<count;i++) buf[i] = (char)(i&0xff);
	
*/
	if((ret = ESCCToolBox_Write_Frame(port,buf,count,1000))<0) printf("write:%d\n",ret);
	Sleep(1000);
	ESCCToolBox_Destroy(port);
}
/* $Id$ */