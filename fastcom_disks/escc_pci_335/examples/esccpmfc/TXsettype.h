// TXsettype.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTXsettype dialog

class CTXsettype : public CDialog
{
// Construction
public:
	CTXsettype(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CTXsettype)
	enum { IDD = IDD_TXSETTYPE };
	int		m_choice;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTXsettype)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTXsettype)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
