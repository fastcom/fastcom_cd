// GetSetReg.cpp : implementation file
//

#include "stdafx.h"
#include "esccpmfc.h"
#include "GetSetReg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGetSetReg dialog


CGetSetReg::CGetSetReg(CWnd* pParent /*=NULL*/)
	: CDialog(CGetSetReg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CGetSetReg)
	m_regs = _T("");
	m_value = _T("");
	//}}AFX_DATA_INIT
}


void CGetSetReg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGetSetReg)
	DDX_CBString(pDX, IDC_REGS, m_regs);
	DDX_Text(pDX, IDC_REGVAL, m_value);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CGetSetReg, CDialog)
	//{{AFX_MSG_MAP(CGetSetReg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGetSetReg message handlers

void CGetSetReg::OnOK() 
{
	// TODO: Add extra validation here
	m_idx = SendDlgItemMessage(IDC_REGS,CB_GETCURSEL,0,0L);
	CDialog::OnOK();
}
