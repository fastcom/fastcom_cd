// esccpmfcView.cpp : implementation of the CEsccpmfcView class
//

#include "stdafx.h"
#include "esccpmfc.h"

#include "esccpmfcDoc.h"
#include "esccpmfcView.h"

//#include "esccptest.h"
#include "stdlib.h"
#include "stdio.h"
#include "devno.h"
#include "setdlg.h"
#include "clockset.h"
#include "dtrdsr.h"
#include "getsetreg.h"
#include "txsettype.h"
#include "getadd.h"
#include "getcmd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

UINT ReadProc( LPVOID lpData );
UINT StatProc( LPVOID lpData );

struct threaddata
{
HWND msgwnd;
HANDLE Hdevescc;
char *pdata;
DWORD status;
BOOL connected;
};

struct threaddata threadparam[6];
char dataarray[6][4096];

/////////////////////////////////////////////////////////////////////////////
// CEsccpmfcView

IMPLEMENT_DYNCREATE(CEsccpmfcView, CView)

BEGIN_MESSAGE_MAP(CEsccpmfcView, CView)
	ON_MESSAGE(WM_USER_READ_UPDATE, OnReadUpdate)
	ON_MESSAGE(WM_USER_STATUS_UPDATE, OnStatusUpdate)
	//{{AFX_MSG_MAP(CEsccpmfcView)
	ON_COMMAND(IDM_ADDESCC, OnAddescc)
	ON_COMMAND(IDM_CHANNELSET, OnChannelset)
	ON_WM_CHAR()
	ON_COMMAND(IDM_DISCONNECT, OnDisconnect)
	ON_COMMAND(IDM_SETCLOCK, OnSetclock)
	ON_WM_SIZE()
	ON_COMMAND(IDM_DTRDSR, OnDtrdsr)
	ON_COMMAND(IDM_WRREG, OnWrreg)
	ON_COMMAND(IDM_RDREG, OnRdreg)
	ON_COMMAND(IDM_FLUSHRX, OnFlushrx)
	ON_COMMAND(IDM_FLUSHTX, OnFlushtx)
	ON_COMMAND(IDM_STARTTIME, OnStarttime)
	ON_COMMAND(IDM_STOPTIME, OnStoptime)
	ON_COMMAND(IDM_SETTXTYPE, OnSettxtype)
	ON_COMMAND(IDM_WRCMD, OnWrcmd)
	ON_COMMAND(IDM_SETTXADD, OnSettxadd)
	ON_COMMAND(IDM_SETRXADD, OnSetrxadd)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEsccpmfcView construction/destruction

CEsccpmfcView::CEsccpmfcView()
{
	// TODO: add construction code here


}

CEsccpmfcView::~CEsccpmfcView()
{
unsigned i;
for(i=0;i<6;i++)threadparam[i].connected = FALSE;

}

BOOL CEsccpmfcView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CEsccpmfcView drawing

void CEsccpmfcView::OnDraw(CDC* pDC)
{
	CEsccpmfcDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	// TODO: add draw code for native data here



	CRect rect;
	CRect rect1;	//txdata rect
	CRect rect2;	//status rect
	CRect rect3;	//rxdata rect
	CRect rect4;
	CRect clrrect;
	RECT trect;
	CBrush brb;
	CBrush brg;
	CBrush oldbr;
	TEXTMETRIC Metric;

	GetClientRect(rect);
	trect.top = 0;
	trect.left = (rect.Width()/4)*3 ;//width of rect2;
	trect.right = rect.Width();
	trect.bottom = (rect.Height());//height of rect2
	rect2 = trect;
	trect.top = 0;
	trect.left = (rect.Width()/8)*7 ;//width of rect2;
	trect.right = rect.Width();
	trect.bottom = (rect.Height());//height of rect2
	rect4 = trect;

	trect.top = 0;
	trect.left = 0;
	trect.right = (rect.Width()/4) *3;//width of rect1
	trect.bottom = (rect.Height()/3);//Height of rect1
	rect1 = trect;
	trect.top = (rect.Height()/3);
	trect.left = 0;
	trect.right = (rect.Width()/4)*3;//width of rect3
	trect.bottom = rect.Height();//height of rect3
	rect3 = trect;
	clrrect = rect1;
	clrrect.DeflateRect(2,2);
	pDC->FillSolidRect(clrrect,RGB(255,255,255));
	clrrect = rect2;
	//clrrect.DeflateRect(2,2);
	//pDC->FillSolidRect(clrrect,RGB(255,255,255));
	clrrect = rect3;
	clrrect.DeflateRect(2,2);
	pDC->FillSolidRect(clrrect,RGB(255,255,255));


	pDC->DrawText(pDoc->txdata,pDoc->txdata.GetLength(),rect1,DT_LEFT|DT_WORDBREAK);
	pDC->DrawText(pDoc->data,pDoc->data.GetLength(),rect3,DT_LEFT|DT_WORDBREAK);

	//pDC->DrawText(pDoc->status,pDoc->status.GetLength(),rect2,DT_LEFT|DT_WORDBREAK);	
	//draw each status and if it is active or notactive (green is active, balck is not);
	brb.CreateSolidBrush(RGB(0,0,0));//should be black
	brg.CreateSolidBrush(RGB(0,255,0));//should be green
//create status's here in status window, using color by numbers
pDC->GetTextMetrics( &Metric );
trect = rect2;  

		
		if((pDoc->statusID&ST_RX_DONE)==ST_RX_DONE)
			{
			pDC->SetTextColor(RGB(0,255,0));
			pDC->DrawText("RXD\r\n",-1,rect2,DT_LEFT);
			}
		else
			{
			pDC->SetTextColor(RGB(0,0,0));
			pDC->DrawText("RXD\r\n",-1,rect2,DT_LEFT);
			}
		rect2.top += Metric.tmHeight;
		if((pDoc->statusID&ST_OVF)==ST_OVF)
			{
			pDC->SetTextColor(RGB(0,255,0));
			pDC->DrawText("BUFOV\r\n",-1,rect2,DT_LEFT);
			}
		else
			{
			pDC->SetTextColor(RGB(0,0,0));
			pDC->DrawText("BUFOV\r\n",-1,rect2,DT_LEFT);
			}
		rect2.top += Metric.tmHeight;
		if((pDoc->statusID&ST_RFS)==ST_RFS)
			{
			pDC->SetTextColor(RGB(0,255,0));
			pDC->DrawText("RFS\r\n",-1,rect2,DT_LEFT);
			}
		else
			{
			pDC->SetTextColor(RGB(0,0,0));
			pDC->DrawText("RFS\r\n",-1,rect2,DT_LEFT);
			}
		rect2.top += Metric.tmHeight;
		if((pDoc->statusID&ST_RX_TIMEOUT)==ST_RX_TIMEOUT)
			{
			pDC->SetTextColor(RGB(0,255,0));
			pDC->DrawText("RTO\r\n",-1,rect2,DT_LEFT);
			}
		else
			{
			pDC->SetTextColor(RGB(0,0,0));
			pDC->DrawText("RTO\r\n",-1,rect2,DT_LEFT);
			}
		rect2.top += Metric.tmHeight;
		if((pDoc->statusID&ST_RSC)==ST_RSC)
			{
			pDC->SetTextColor(RGB(0,255,0));
			pDC->DrawText("RSC\r\n",-1,rect2,DT_LEFT);
			}
		else
			{
			pDC->SetTextColor(RGB(0,0,0));
			pDC->DrawText("RSC\r\n",-1,rect2,DT_LEFT);
			}
		rect2.top += Metric.tmHeight;
		if((pDoc->statusID&ST_PERR)==ST_PERR)
			{
			pDC->SetTextColor(RGB(0,255,0));
			pDC->DrawText("PE\r\n",-1,rect2,DT_LEFT);
			}
		else
			{
			pDC->SetTextColor(RGB(0,0,0));
			pDC->DrawText("PE\r\n",-1,rect2,DT_LEFT);
			}
		rect2.top += Metric.tmHeight;
		if((pDoc->statusID&ST_PCE)==ST_PCE)
			{
			pDC->SetTextColor(RGB(0,255,0));
			pDC->DrawText("PROTE\r\n",-1,rect2,DT_LEFT);
			}
		else
			{
			pDC->SetTextColor(RGB(0,0,0));
			pDC->DrawText("PROTE\r\n",-1,rect2,DT_LEFT);
			}
		rect2.top += Metric.tmHeight;
		if((pDoc->statusID&ST_FERR)==ST_FERR)
			{
			pDC->SetTextColor(RGB(0,255,0));
			pDC->DrawText("FE\r\n",-1,rect2,DT_LEFT);
			}
		else
			{
			pDC->SetTextColor(RGB(0,0,0));
			pDC->DrawText("FE\r\n",-1,rect2,DT_LEFT);
			}
		rect2.top += Metric.tmHeight;
		if((pDoc->statusID&ST_SYN)==ST_SYN)
			{
			pDC->SetTextColor(RGB(0,255,0));
			pDC->DrawText("SYN\r\n",-1,rect2,DT_LEFT);
			}
		else
			{
			pDC->SetTextColor(RGB(0,0,0));
			pDC->DrawText("SYN\r\n",-1,rect2,DT_LEFT);
			}
		rect2.top += Metric.tmHeight;
		if((pDoc->statusID&ST_DPLLA)==ST_DPLLA)
			{
			pDC->SetTextColor(RGB(0,255,0));
			pDC->DrawText("DPLLA\r\n",-1,rect2,DT_LEFT);
			}
		else
			{
			pDC->SetTextColor(RGB(0,0,0));
			pDC->DrawText("DPLLA\r\n",-1,rect2,DT_LEFT);
			}
		rect2.top += Metric.tmHeight;
		if((pDoc->statusID&ST_CDSC)==ST_CDSC)
			{
			pDC->SetTextColor(RGB(0,255,0));
			pDC->DrawText("CDSC\r\n",-1,rect2,DT_LEFT);
			}
		else
			{
			pDC->SetTextColor(RGB(0,0,0));
			pDC->DrawText("CDSC\r\n",-1,rect2,DT_LEFT);
			}
		rect2.top += Metric.tmHeight;
		if((pDoc->statusID&ST_RFO)==ST_RFO)
			{
			pDC->SetTextColor(RGB(0,255,0));
			pDC->DrawText("RFO\r\n",-1,rect2,DT_LEFT);
			}
		else
			{
			pDC->SetTextColor(RGB(0,0,0));
			pDC->DrawText("RFO\r\n",-1,rect2,DT_LEFT);
			}
		rect2.top += Metric.tmHeight;
		if((pDoc->statusID&ST_EOP)==ST_EOP)
			{
			pDC->SetTextColor(RGB(0,255,0));
			pDC->DrawText("EOP\r\n",-1,rect4,DT_LEFT);
			}
		else
			{
			pDC->SetTextColor(RGB(0,0,0));
			pDC->DrawText("EOP\r\n",-1,rect4,DT_LEFT);
			}
		rect4.top += Metric.tmHeight;
		if((pDoc->statusID&ST_BRKD)==ST_BRKD)
			{
			pDC->SetTextColor(RGB(0,255,0));
			pDC->DrawText("BRKD\r\n",-1,rect4,DT_LEFT);
			}
		else
			{
			pDC->SetTextColor(RGB(0,0,0));
			pDC->DrawText("BRKD\r\n",-1,rect4,DT_LEFT);
			}
		rect4.top += Metric.tmHeight;
		if((pDoc->statusID&ST_ONLP)==ST_ONLP)
			{
			pDC->SetTextColor(RGB(0,255,0));
			pDC->DrawText("ONL\r\n",-1,rect4,DT_LEFT);
			}
		else
			{
			pDC->SetTextColor(RGB(0,0,0));
			pDC->DrawText("ONL\r\n",-1,rect4,DT_LEFT);
			}
		rect4.top += Metric.tmHeight;
		if((pDoc->statusID&ST_BRKT)==ST_BRKT)
			{
			pDC->SetTextColor(RGB(0,255,0));
			pDC->DrawText("BRKT\r\n",-1,rect4,DT_LEFT);
			}
		else
			{
			pDC->SetTextColor(RGB(0,0,0));
			pDC->DrawText("BRKT\r\n",-1,rect4,DT_LEFT);
			}
		rect4.top += Metric.tmHeight;
		if((pDoc->statusID&ST_ALLS)==ST_ALLS)
			{
			pDC->SetTextColor(RGB(0,255,0));
			pDC->DrawText("ALLS\r\n",-1,rect4,DT_LEFT);
			}
		else
			{
			pDC->SetTextColor(RGB(0,0,0));
			pDC->DrawText("ALLS\r\n",-1,rect4,DT_LEFT);
			}
		rect4.top += Metric.tmHeight;
		if((pDoc->statusID&ST_EXE)==ST_EXE)
			{
			pDC->SetTextColor(RGB(0,255,0));
			pDC->DrawText("XDU\r\n",-1,rect4,DT_LEFT);
			}
		else
			{
			pDC->SetTextColor(RGB(0,0,0));
			pDC->DrawText("XDU\r\n",-1,rect4,DT_LEFT);
			}
		rect4.top += Metric.tmHeight;
		if((pDoc->statusID&ST_TIN)==ST_TIN)
			{
			pDC->SetTextColor(RGB(0,255,0));
			pDC->DrawText("TIMER\r\n",-1,rect4,DT_LEFT);
			}
		else
			{
			pDC->SetTextColor(RGB(0,0,0));
			pDC->DrawText("TIMER\r\n",-1,rect4,DT_LEFT);
			}
		rect4.top += Metric.tmHeight;
		if((pDoc->statusID&ST_CTSC)==ST_CTSC)
			{
			pDC->SetTextColor(RGB(0,255,0));
			pDC->DrawText("CTSSC\r\n",-1,rect4,DT_LEFT);
			}
		else
			{
			pDC->SetTextColor(RGB(0,0,0));
			pDC->DrawText("CTSSC\r\n",-1,rect4,DT_LEFT);
			}
		rect4.top += Metric.tmHeight;
		if((pDoc->statusID&ST_XMR)==ST_XMR)
			{
			pDC->SetTextColor(RGB(0,255,0));
			pDC->DrawText("XMR\r\n",-1,rect4,DT_LEFT);
			}
		else
			{
			pDC->SetTextColor(RGB(0,0,0));
			pDC->DrawText("XMR\r\n",-1,rect4,DT_LEFT);
			}
		rect4.top += Metric.tmHeight;
		if((pDoc->statusID&ST_TX_DONE)==ST_TX_DONE)
			{
			pDC->SetTextColor(RGB(0,255,0));
			pDC->DrawText("TXD\r\n",-1,rect4,DT_LEFT);
			}
		else
			{
			pDC->SetTextColor(RGB(0,0,0));
			pDC->DrawText("TXD\r\n",-1,rect4,DT_LEFT);
			}
		rect4.top += Metric.tmHeight;
		//if((pDoc->statusID&ST_DMA_TC)==ST_DMA_TC) pDoc->status +="TC ";
		if((pDoc->statusID&ST_DSR1C)==ST_DSR1C)
			{
			pDC->SetTextColor(RGB(0,255,0));
			pDC->DrawText("DSR1\r\n",-1,rect4,DT_LEFT);
			}
		else
			{
			pDC->SetTextColor(RGB(0,0,0));
			pDC->DrawText("DSR1\r\n",-1,rect4,DT_LEFT);
			}
		rect4.top += Metric.tmHeight;
		if((pDoc->statusID&ST_DSR0C)==ST_DSR0C)
			{
			pDC->SetTextColor(RGB(0,255,0));
			pDC->DrawText("DSR0\r\n",-1,rect4,DT_LEFT);
			}
		else
			{
			pDC->SetTextColor(RGB(0,0,0));
			pDC->DrawText("DSR0\r\n",-1,rect4,DT_LEFT);
			}

		rect4.top += Metric.tmHeight;
		if((pDoc->statusID&ST_FUBAR_IRQ)==ST_FUBAR_IRQ)
			{
			pDC->SetTextColor(RGB(0,255,0));
			pDC->DrawText("FUBAR\r\n",-1,rect4,DT_LEFT);
			}
		else
			{
			pDC->SetTextColor(RGB(0,0,0));
			pDC->DrawText("FUBAR\r\n",-1,rect4,DT_LEFT);
			}

rect2 = trect;
	//oldbr = pDC->SelectObject(br);
	pDC->FrameRect(rect1,&brb);
	pDC->FrameRect(rect2,&brb);
	pDC->FrameRect(rect3,&brb);



}

/////////////////////////////////////////////////////////////////////////////
// CEsccpmfcView diagnostics

#ifdef _DEBUG
void CEsccpmfcView::AssertValid() const
{
	CView::AssertValid();
}

void CEsccpmfcView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CEsccpmfcDoc* CEsccpmfcView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CEsccpmfcDoc)));
	return (CEsccpmfcDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CEsccpmfcView message handlers

void CEsccpmfcView::OnAddescc() 
{
	// TODO: Add your command handler code here
	//pop a dialog and ask for the ESCC Device #
	//use that Device# to open the device, and create the necessary threads
CDevno ask;
USHORT device;


DWORD nobytestoread;	//the number of bytes to read from the driver
DWORD nobytestowrite;	//the number of bytes to write to the driver




char devname[80];

//step 1 get pointer to document.
CEsccpmfcDoc* pDoc = GetDocument();
ASSERT_VALID(pDoc);
if(pDoc->connected)
{
MessageBox("Must Disconnect First","",MB_OK);
}
else
{
pDoc->numbits = 23;
pDoc->clockbits = 0x5db2d8;

memset( &pDoc->ostx, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct

// create I/O event used for overlapped write

pDoc->ostx.hEvent = CreateEvent( NULL,    // no security
			    TRUE,    // explicit reset req
			    FALSE,   // initial event reset
			    NULL ) ; // no name
if (pDoc->ostx.hEvent == NULL)
   {
      MessageBox( "Failed to create event for TX!", "Error!",
		  MB_ICONEXCLAMATION | MB_OK ) ;
      return; 
   }


nobytestoread = 4096;    //this should allways be 4096, it is the maximum frame size returned from the driver at one time
nobytestowrite = 1024;	//this is the number of bytes to send to the driver to send as a single frame (can be from 1 to 4096)


//Step 2 get device # to open
ask.DoModal();
device = atoi(ask.m_Devno); //convert to integer
pDoc->pnum = device;

sprintf(devname,"\\\\.\\ESCC%u",device);
//set mdi child caption to this

    pDoc->hDevice = CreateFile (devname,
			  GENERIC_READ | GENERIC_WRITE,
			  0,
			  NULL,
			  OPEN_EXISTING,
			  FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
			  NULL
			  );

    if (pDoc->hDevice == INVALID_HANDLE_VALUE)
    {
		//for some reason the driver won't load or isn't loaded
	MessageBox("Can't get a handle to esccdrv","ERROR",MB_ICONEXCLAMATION | MB_OK ) ;
	return;
	//abort and leave here!!!
	}
pDoc->SetTitle(devname);
pDoc->connected = TRUE;

threadparam[pDoc->pnum].msgwnd = m_hWnd;
threadparam[pDoc->pnum].Hdevescc = pDoc->hDevice;
threadparam[pDoc->pnum].pdata = &dataarray[pDoc->pnum][0];
threadparam[pDoc->pnum].status = 0;
threadparam[pDoc->pnum].connected = TRUE;
pDoc->rT = AfxBeginThread(ReadProc,&threadparam[pDoc->pnum],THREAD_PRIORITY_NORMAL,0,0,NULL);
pDoc->sT = AfxBeginThread(StatProc,&threadparam[pDoc->pnum],THREAD_PRIORITY_NORMAL,0,0,NULL);

//any other init stuff goes here
}	
}

void CEsccpmfcView::OnChannelset() 
{
BOOL t;
ULONG returnsize;
	// TODO: Add your command handler code here
	//pop a dialog and get the device settings

//step 1 get pointer to document.
CEsccpmfcDoc* pDoc = GetDocument();
ASSERT_VALID(pDoc);



char buf[80];
Csetdlg set;
sprintf(buf,"%x",pDoc->Esccset.mode);
set.m_mode = buf;
sprintf(buf,"%x",pDoc->Esccset.timr);
set.m_timr = buf;
sprintf(buf,"%x",pDoc->Esccset.xbcl);
set.m_xbcl = buf;
sprintf(buf,"%x",pDoc->Esccset.xbch);
set.m_xbch = buf;
sprintf(buf,"%x",pDoc->Esccset.ccr0);
set.m_ccr0 = buf;
sprintf(buf,"%x",pDoc->Esccset.ccr1);
set.m_ccr1 = buf;
sprintf(buf,"%x",pDoc->Esccset.ccr2);
set.m_ccr2 = buf;
sprintf(buf,"%x",pDoc->Esccset.ccr3);
set.m_ccr3 = buf;
sprintf(buf,"%x",pDoc->Esccset.bgr);
set.m_bgr = buf;
sprintf(buf,"%x",pDoc->Esccset.iva);
set.m_iva = buf;
sprintf(buf,"%x",pDoc->Esccset.ipc);
set.m_ipc = buf;
sprintf(buf,"%x",pDoc->Esccset.imr0);
set.m_imr0 = buf;
sprintf(buf,"%x",pDoc->Esccset.imr1);
set.m_imr1 = buf;
sprintf(buf,"%x",pDoc->Esccset.pvr);
set.m_pvr = buf;
sprintf(buf,"%x",pDoc->Esccset.pim);
set.m_pim = buf;
sprintf(buf,"%x",pDoc->Esccset.pcr);
set.m_pcr = buf;
sprintf(buf,"%x",pDoc->Esccset.xad1);
set.m_xad1 = buf;
sprintf(buf,"%x",pDoc->Esccset.xad2);
set.m_xad2 = buf;
sprintf(buf,"%x",pDoc->Esccset.rah1);
set.m_rah1 = buf;
sprintf(buf,"%x",pDoc->Esccset.rah2);
set.m_rah2 = buf;
sprintf(buf,"%x",pDoc->Esccset.ral1);
set.m_ral1 = buf;
sprintf(buf,"%x",pDoc->Esccset.ral2);
set.m_ral2 = buf;
sprintf(buf,"%x",pDoc->Esccset.rlcr);
set.m_rlcr = buf;
sprintf(buf,"%x",pDoc->Esccset.pre);
set.m_pre = buf;
sprintf(buf,"%x",pDoc->Esccset.ccr4);
set.m_ccr4 = buf;
sprintf(buf,"%x",pDoc->Esccset.tsax);
set.m_tsax = buf;
sprintf(buf,"%x",pDoc->Esccset.tsar);
set.m_tsar = buf;
sprintf(buf,"%x",pDoc->Esccset.rccr);
set.m_rccr = buf;
sprintf(buf,"%x",pDoc->Esccset.xccr);
set.m_xccr = buf;
sprintf(buf,"%x",pDoc->Esccset.aml);
set.m_aml = buf;
sprintf(buf,"%x",pDoc->Esccset.amh);
set.m_amh = buf;

sprintf(buf,"%x",pDoc->Esccset.synl);
set.m_synl = buf;
sprintf(buf,"%x",pDoc->Esccset.synh);
set.m_synh = buf;

sprintf(buf,"%x",pDoc->Esccset.rfc);
set.m_rfc = buf;
sprintf(buf,"%x",pDoc->Esccset.dafo);
set.m_dafo = buf;
sprintf(buf,"%x",pDoc->Esccset.tcr);
set.m_tcr = buf;

sprintf(buf,"%x",pDoc->Esccset.mxn);
set.m_mxn = buf;
sprintf(buf,"%x",pDoc->Esccset.mxf);
set.m_mxf = buf;

sprintf(buf,"%u",pDoc->Esccset.n_rbufs);
set.m_nrbufs = buf;
sprintf(buf,"%u",pDoc->Esccset.n_tbufs);
set.m_ntbufs = buf;
sprintf(buf,"%u",pDoc->Esccset.n_rfsize_max);
set.m_rbufsize = buf;
sprintf(buf,"%u",pDoc->Esccset.n_tfsize_max);
set.m_tbufsize = buf;


if((pDoc->Esccset.ccr0&3)==0)set.m_type = 0;
if((pDoc->Esccset.ccr0&3)==2)set.m_type = 1;
if((pDoc->Esccset.ccr0&3)==3)set.m_type = 2;

if(set.DoModal()==IDOK)
{
//get set.m_ccr0...

	//do a ESCC_SETTINGS deviceIoControl call here with the 
	//above settings

sscanf((LPCTSTR)(set.m_nrbufs),"%u",&pDoc->Esccset.n_rbufs);
sscanf((LPCTSTR)(set.m_ntbufs),"%u",&pDoc->Esccset.n_tbufs);
sscanf((LPCTSTR)(set.m_rbufsize),"%u",&pDoc->Esccset.n_rfsize_max);
sscanf((LPCTSTR)(set.m_tbufsize),"%u",&pDoc->Esccset.n_tfsize_max);

//sprintf(buf,"%u,%u,%u,%u",pDoc->Esccset.n_rbufs,pDoc->Esccset.n_rfsize_max,pDoc->Esccset.n_tbufs,pDoc->Esccset.n_tfsize_max);
//MessageBox(buf,"",MB_OK);

if(set.m_type == 0)
{
//get hdlc settings
sscanf((LPCTSTR)(set.m_mode),"%x",&pDoc->Esccset.mode);
sscanf((LPCTSTR)(set.m_timr),"%x",&pDoc->Esccset.timr); 
sscanf((LPCTSTR)(set.m_xbcl),"%x",&pDoc->Esccset.xbcl); 
sscanf((LPCTSTR)(set.m_xbch),"%x",&pDoc->Esccset.xbch); 
sscanf((LPCTSTR)(set.m_ccr0),"%x",&pDoc->Esccset.ccr0); 
sscanf((LPCTSTR)(set.m_ccr1),"%x",&pDoc->Esccset.ccr1); 
sscanf((LPCTSTR)(set.m_ccr2),"%x",&pDoc->Esccset.ccr2); 
sscanf((LPCTSTR)(set.m_ccr3),"%x",&pDoc->Esccset.ccr3); 
sscanf((LPCTSTR)(set.m_bgr),"%x",&pDoc->Esccset.bgr); 
sscanf((LPCTSTR)(set.m_iva),"%x",&pDoc->Esccset.iva); 
sscanf((LPCTSTR)(set.m_ipc),"%x",&pDoc->Esccset.ipc); 
sscanf((LPCTSTR)(set.m_imr0),"%x",&pDoc->Esccset.imr0); 
sscanf((LPCTSTR)(set.m_imr1),"%x",&pDoc->Esccset.imr1); 
sscanf((LPCTSTR)(set.m_pvr),"%x",&pDoc->Esccset.pvr); 
sscanf((LPCTSTR)(set.m_pim),"%x",&pDoc->Esccset.pim); 
sscanf((LPCTSTR)(set.m_pcr),"%x",&pDoc->Esccset.pcr); 
sscanf((LPCTSTR)(set.m_xad1),"%x",&pDoc->Esccset.xad1); 
sscanf((LPCTSTR)(set.m_xad2),"%x",&pDoc->Esccset.xad2); 
sscanf((LPCTSTR)(set.m_rah1),"%x",&pDoc->Esccset.rah1); 
sscanf((LPCTSTR)(set.m_rah2),"%x",&pDoc->Esccset.rah2); 
sscanf((LPCTSTR)(set.m_ral1),"%x",&pDoc->Esccset.ral1); 
sscanf((LPCTSTR)(set.m_ral2),"%x",&pDoc->Esccset.ral2); 
sscanf((LPCTSTR)(set.m_rlcr),"%x",&pDoc->Esccset.rlcr); 
sscanf((LPCTSTR)(set.m_pre),"%x",&pDoc->Esccset.pre); 
sscanf((LPCTSTR)(set.m_ccr4),"%x",&pDoc->Esccset.ccr4); 

sscanf((LPCTSTR)(set.m_xccr),"%x",&pDoc->Esccset.xccr); 
sscanf((LPCTSTR)(set.m_rccr),"%x",&pDoc->Esccset.rccr); 
sscanf((LPCTSTR)(set.m_tsax),"%x",&pDoc->Esccset.tsax); 
sscanf((LPCTSTR)(set.m_tsar),"%x",&pDoc->Esccset.tsar); 
sscanf((LPCTSTR)(set.m_aml),"%x",&pDoc->Esccset.aml); 
sscanf((LPCTSTR)(set.m_amh),"%x",&pDoc->Esccset.amh); 

}
if(set.m_type == 1)
{
//do bisync settings
sscanf((LPCTSTR)(set.m_mode),"%x",&pDoc->Esccset.mode); 
sscanf((LPCTSTR)(set.m_timr),"%x",&pDoc->Esccset.timr);
sscanf((LPCTSTR)(set.m_synl),"%x",&pDoc->Esccset.synl); 
sscanf((LPCTSTR)(set.m_synh),"%x",&pDoc->Esccset.synh); 
sscanf((LPCTSTR)(set.m_tcr),"%x",&pDoc->Esccset.tcr); 
sscanf((LPCTSTR)(set.m_dafo),"%x",&pDoc->Esccset.dafo); 
sscanf((LPCTSTR)(set.m_rfc),"%x",&pDoc->Esccset.rfc); 
sscanf((LPCTSTR)(set.m_ccr0),"%x",&pDoc->Esccset.ccr0); 
sscanf((LPCTSTR)(set.m_ccr1),"%x",&pDoc->Esccset.ccr1); 
sscanf((LPCTSTR)(set.m_ccr2),"%x",&pDoc->Esccset.ccr2); 
sscanf((LPCTSTR)(set.m_ccr3),"%x",&pDoc->Esccset.ccr3); 
sscanf((LPCTSTR)(set.m_bgr),"%x",&pDoc->Esccset.bgr); 
sscanf((LPCTSTR)(set.m_pre),"%x",&pDoc->Esccset.pre); 
sscanf((LPCTSTR)(set.m_iva),"%x",&pDoc->Esccset.iva); 
sscanf((LPCTSTR)(set.m_ipc),"%x",&pDoc->Esccset.ipc); 
sscanf((LPCTSTR)(set.m_imr0),"%x",&pDoc->Esccset.imr0); 
sscanf((LPCTSTR)(set.m_imr1),"%x",&pDoc->Esccset.imr1); 
sscanf((LPCTSTR)(set.m_pvr),"%x",&pDoc->Esccset.pvr); 
sscanf((LPCTSTR)(set.m_pim),"%x",&pDoc->Esccset.pim); 
sscanf((LPCTSTR)(set.m_pcr),"%x",&pDoc->Esccset.pcr); 
sscanf((LPCTSTR)(set.m_xbch),"%x",&pDoc->Esccset.xbch); 
sscanf((LPCTSTR)(set.m_xccr),"%x",&pDoc->Esccset.xccr); 
sscanf((LPCTSTR)(set.m_rccr),"%x",&pDoc->Esccset.rccr); 
sscanf((LPCTSTR)(set.m_tsax),"%x",&pDoc->Esccset.tsax); 
sscanf((LPCTSTR)(set.m_tsar),"%x",&pDoc->Esccset.tsar);
sscanf((LPCTSTR)(set.m_ccr4),"%x",&pDoc->Esccset.ccr4); 

}

if(set.m_type == 2)
{
//do async settings
sscanf((LPCTSTR)(set.m_mode),"%x",&pDoc->Esccset.mode); 
sscanf((LPCTSTR)(set.m_timr),"%x",&pDoc->Esccset.timr); 
sscanf((LPCTSTR)(set.m_xbcl),"%x",&pDoc->Esccset.xbcl); 
sscanf((LPCTSTR)(set.m_xbch),"%x",&pDoc->Esccset.xbch); 
sscanf((LPCTSTR)(set.m_ccr0),"%x",&pDoc->Esccset.ccr0); 
sscanf((LPCTSTR)(set.m_ccr1),"%x",&pDoc->Esccset.ccr1); 
sscanf((LPCTSTR)(set.m_ccr2),"%x",&pDoc->Esccset.ccr2); 
sscanf((LPCTSTR)(set.m_bgr),"%x",&pDoc->Esccset.bgr); 
sscanf((LPCTSTR)(set.m_iva),"%x",&pDoc->Esccset.iva); 
sscanf((LPCTSTR)(set.m_ipc),"%x",&pDoc->Esccset.ipc); 
sscanf((LPCTSTR)(set.m_imr0),"%x",&pDoc->Esccset.imr0); 
sscanf((LPCTSTR)(set.m_imr1),"%x",&pDoc->Esccset.imr1); 
sscanf((LPCTSTR)(set.m_pvr),"%x",&pDoc->Esccset.pvr); 
sscanf((LPCTSTR)(set.m_pim),"%x",&pDoc->Esccset.pim); 
sscanf((LPCTSTR)(set.m_pcr),"%x",&pDoc->Esccset.pcr); 
sscanf((LPCTSTR)(set.m_tcr),"%x",&pDoc->Esccset.tcr); 
sscanf((LPCTSTR)(set.m_dafo),"%x",&pDoc->Esccset.dafo); 
sscanf((LPCTSTR)(set.m_rfc),"%x",&pDoc->Esccset.rfc); 
sscanf((LPCTSTR)(set.m_xccr),"%x",&pDoc->Esccset.xccr); 
sscanf((LPCTSTR)(set.m_rccr),"%x",&pDoc->Esccset.rccr); 
sscanf((LPCTSTR)(set.m_tsax),"%x",&pDoc->Esccset.tsax); 
sscanf((LPCTSTR)(set.m_tsar),"%x",&pDoc->Esccset.tsar); 
sscanf((LPCTSTR)(set.m_mxn),"%x",&pDoc->Esccset.mxn); 
sscanf((LPCTSTR)(set.m_mxf),"%x",&pDoc->Esccset.mxf); 
sscanf((LPCTSTR)(set.m_ccr4),"%x",&pDoc->Esccset.ccr4); 

}
	t = DeviceIoControl(pDoc->hDevice,IOCTL_ESCCDRV_SETUP,&pDoc->Esccset,sizeof(struct setup),NULL,0,&returnsize,NULL);
	//if(t==TRUE)printf("SETTINGS SUCCESSFUL:%lu\n\r",returnsize);
	if(t==FALSE)MessageBox("SETTINGS FAILED","ERROR",MB_ICONEXCLAMATION|MB_OK);
}
else MessageBox("Change Settings Canceled","",MB_OK);
}

void CEsccpmfcView::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	CEsccpmfcDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
USHORT i;
	
if(nChar=='\r')
{
//return pressed send the frame
BOOL t;
ULONG nobyteswritten;
ULONG k;

    t = WriteFile(pDoc->hDevice,pDoc->txdata.GetBuffer(0),pDoc->txdata.GetLength(),&nobyteswritten,&pDoc->ostx);//send the frame
	//printf("WRITEFILE esccdrv%lu \n\r",nobyteswritten);//if nobyteswritten doesnt = sizeof(struct buf) something is wrong
	//if(t==TRUE)printf("returned TRUE\n\r"); //if returned true then the IO request was started (txing has begun)
	if(t==FALSE)                    //if it returned FALSE then the IO request is queued (waiting for previous frame to get done sending)
		{                       //and we must wait until the os.event gets signaled before we try any more sending
		//printf("returned FALSE\n\r");
		if (GetLastError() == ERROR_IO_PENDING)  //IO_PENDING is the indication that the request was queued, if not this than a parameter error occured
			{
		// wait for a second for this transmission to complete
	    do
			{
			k = WaitForSingleObject( pDoc->ostx.hEvent, 1000 );//1 second timeout
			if(k==WAIT_TIMEOUT)
				{
				//this will execute every 1 second
				//you could put a counter in here and if the 
				//driver takes an inordinate ammout of time
				//to complete, you could issue a flush TX command
				//and break out of this loop
				}
			if(k==WAIT_ABANDONED)
				{
				}
			
			}while(k!=WAIT_OBJECT_0);
			}
		//here it is done sent..(well started anyway)
		}
//MessageBox("sent it","",MB_OK);
pDoc->txdata.ReleaseBuffer(-1);
	
pDoc->txdata = "";
Invalidate(FALSE);
}
else for(i=0;i<nRepCnt;i++) pDoc->txdata += nChar;
Invalidate(FALSE);
	CView::OnChar(nChar, nRepCnt, nFlags);
}

void CEsccpmfcView::OnDisconnect() 
{
	// TODO: Add your command handler code here
	CEsccpmfcDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

pDoc->connected = FALSE;
threadparam[pDoc->pnum].connected = FALSE;
pDoc->statusID = 0;
pDoc->data = "";
pDoc->txdata = "";
Invalidate(FALSE);

if(CloseHandle(pDoc->hDevice)==FALSE)
{
	MessageBox("close device failed",MB_OK);
}
//CloseHandle (pDoc->hreadThread);                //done with this thread
//CloseHandle (pDoc->hstatThread);                //done with this thread

pDoc->SetTitle("not connected");
}

void CEsccpmfcView::OnSetclock() 
{
	// TODO: Add your command handler code here
	CEsccpmfcDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	CClockSet cset;
	
	ULONG returnsize;
	char buf[80];
	sprintf(buf,"%u",pDoc->freq);
	cset.m_freq = buf;

	if(cset.DoModal()==IDOK)
	{
	sscanf((LPCTSTR)(cset.m_freq),"%u",&pDoc->freq); 
	DeviceIoControl(pDoc->hDevice,IOCTL_ESCCDRV_SET_FREQ,&pDoc->freq,sizeof(ULONG),NULL,0,&returnsize,NULL);
	}

}

void CEsccpmfcView::OnSize(UINT nType, int cx, int cy) 
{
	
	CView::OnSize(nType, cx, cy);	
	
}

void CEsccpmfcView::CalcWindowRect(LPRECT lpClientRect, UINT nAdjustType) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CView::CalcWindowRect(lpClientRect, nAdjustType);
}

void CEsccpmfcView::OnDtrdsr() 
{
	// TODO: Add your command handler code here
	dtrdsr dset;
	ULONG returnsize;
	ULONG k = 0; 
	CEsccpmfcDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	DeviceIoControl(pDoc->hDevice,IOCTL_ESCCDRV_GET_DSR,NULL,0,&k,sizeof(ULONG),&returnsize,NULL);
	k = k &0x03;
	if((k&1)==1) dset.m_dtr = TRUE;
	else dset.m_dtr = FALSE;
	if((k&2)==2) dset.m_dsr = TRUE;
	else dset.m_dsr = FALSE;
		
	if(dset.DoModal()==IDOK)
		{
		if(dset.m_dtr) k = 1; 
		else k = 0;
		DeviceIoControl(pDoc->hDevice,IOCTL_ESCCDRV_SET_DTR,&k,sizeof(ULONG),NULL,0,&returnsize,NULL);
		}

}

void CEsccpmfcView::OnWrreg() 
{
	// TODO: Add your command handler code here
	CGetSetReg dlg;
	struct regsingle rg;
	ULONG returnsize;
	CEsccpmfcDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);


	if(dlg.DoModal()==IDOK)
		{
		sscanf((LPCTSTR)(dlg.m_value),"%x",&rg.data);
		rg.port = dlg.m_idx + 0x20;
		DeviceIoControl(pDoc->hDevice,IOCTL_ESCCDRV_WRITE_REGISTER,&rg,sizeof(struct regsingle),NULL,0,&returnsize,NULL);
		}
}

void CEsccpmfcView::OnRdreg() 
{
	// TODO: Add your command handler code here
	CGetSetReg dlg;
	ULONG rg,pt;
	ULONG returnsize;
	CEsccpmfcDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	char buf[80];

	if(dlg.DoModal()==IDOK)
		{
		pt  = dlg.m_idx + 0x20;
		DeviceIoControl(pDoc->hDevice,IOCTL_ESCCDRV_READ_REGISTER,&pt,sizeof(ULONG),&rg,sizeof(ULONG),&returnsize,NULL);
		sprintf(buf,"port:%x:=%x",pt,rg);
		MessageBox(buf,"",MB_OK);
		}
	
}

void CEsccpmfcView::OnFlushrx() 
{
	// TODO: Add your command handler code here
ULONG returnsize;
CEsccpmfcDoc* pDoc = GetDocument();
ASSERT_VALID(pDoc);
DeviceIoControl(pDoc->hDevice,IOCTL_ESCCDRV_FLUSH_RX,NULL,0,NULL,0,&returnsize,NULL);
MessageBox("RX FLUSHED","",MB_OK);

}

void CEsccpmfcView::OnFlushtx() 
{
	// TODO: Add your command handler code here
ULONG returnsize;
CEsccpmfcDoc* pDoc = GetDocument();
ASSERT_VALID(pDoc);
DeviceIoControl(pDoc->hDevice,IOCTL_ESCCDRV_FLUSH_TX,NULL,0,NULL,0,&returnsize,NULL);
MessageBox("TX FLUSHED","",MB_OK);
	
}

void CEsccpmfcView::OnStarttime() 
{
	// TODO: Add your command handler code here
ULONG returnsize;
CEsccpmfcDoc* pDoc = GetDocument();
ASSERT_VALID(pDoc);
ULONG k;

k = 0x10;
DeviceIoControl(pDoc->hDevice,IOCTL_ESCCDRV_CMDR,&k,sizeof(DWORD),NULL,0,&returnsize,NULL);
	
}

void CEsccpmfcView::OnStoptime() 
{
	// TODO: Add your command handler code here
ULONG returnsize;
CEsccpmfcDoc* pDoc = GetDocument();
ASSERT_VALID(pDoc);
DeviceIoControl(pDoc->hDevice,IOCTL_ESCCDRV_STOP_TIMER,NULL,0,NULL,0,&returnsize,NULL);
	
}

void CEsccpmfcView::OnSettxtype() 
{
ULONG returnsize;
CEsccpmfcDoc* pDoc = GetDocument();
ASSERT_VALID(pDoc);
ULONG k;

	// TODO: Add your command handler code here
	CTXsettype dlg;
	if(dlg.DoModal()==IDOK)
		{
		if(dlg.m_choice == 0) k = 0;
		if(dlg.m_choice == 1) k = 1;
		DeviceIoControl(pDoc->hDevice,IOCTL_ESCCDRV_SET_TX_TYPE,&k,sizeof(ULONG),NULL,0,&returnsize,NULL);
		}
}

void CEsccpmfcView::OnWrcmd() 
{
	// TODO: Add your command handler code here
	CGetCmd dlg;
	ULONG k;
	ULONG returnsize;
	CEsccpmfcDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);


	if(dlg.DoModal()==IDOK)
		{
		sscanf((LPCTSTR)(dlg.m_cmd),"%x",&k);
		DeviceIoControl(pDoc->hDevice,IOCTL_ESCCDRV_CMDR,&k,sizeof(ULONG),NULL,0,&returnsize,NULL);
		}
	
}

void CEsccpmfcView::OnSettxadd() 
{
ULONG returnsize;
CEsccpmfcDoc* pDoc = GetDocument();
ASSERT_VALID(pDoc);
ULONG k;

	// TODO: Add your command handler code here
CGetAdd dlg;
if(dlg.DoModal()==IDOK)
	{
	sscanf((LPCTSTR)(dlg.m_add),"%x",&k);
	//set the address here
	DeviceIoControl(pDoc->hDevice,IOCTL_ESCCDRV_SET_TX_ADD,&k,sizeof(ULONG),NULL,0,&returnsize,NULL);
	}
}

void CEsccpmfcView::OnSetrxadd() 
{
	// TODO: Add your command handler code here
struct regsingle rg;
ULONG returnsize;
CEsccpmfcDoc* pDoc = GetDocument();
ASSERT_VALID(pDoc);
ULONG k;

	// TODO: Add your command handler code here
CGetAdd dlg;
if(dlg.DoModal()==IDOK)
	{
	sscanf((LPCTSTR)(dlg.m_add),"%x",&k);
	//set the address here
	rg.data = (unsigned char)(k&0xff);
	rg.port = 0x27;//ral1
	DeviceIoControl(pDoc->hDevice,IOCTL_ESCCDRV_WRITE_REGISTER,&rg,sizeof(struct regsingle),NULL,0,&returnsize,NULL);
	rg.data = (unsigned char)((k>>8)&0xff);
	rg.port = 0x26;//rah1
	DeviceIoControl(pDoc->hDevice,IOCTL_ESCCDRV_WRITE_REGISTER,&rg,sizeof(struct regsingle),NULL,0,&returnsize,NULL);
	}
	
}

LRESULT CEsccpmfcView::OnReadUpdate(WPARAM, LPARAM) 
{
CEsccpmfcDoc* pDoc = GetDocument();
ASSERT_VALID(pDoc);

pDoc->data = dataarray[pDoc->pnum];
Invalidate();
return 0;
}
LRESULT CEsccpmfcView::OnStatusUpdate(WPARAM, LPARAM) 
{
CEsccpmfcDoc* pDoc = GetDocument();
ASSERT_VALID(pDoc);

pDoc->statusID = threadparam[pDoc->pnum].status;
Invalidate();
return 0;
}

UINT StatProc( LPVOID lpData )
{
struct threaddata *tp;
DWORD j,k;	//temp storage
BOOL t;		//temp storage
DWORD returnsize;	//temp storage
OVERLAPPED  os ;	//overlapped struct for driver signaling

tp = (struct threaddata*)lpData;


				
memset( &os, 0, sizeof( OVERLAPPED ) ) ; //wipe the overlapped structure

// create I/O event used for overlapped read

os.hEvent = CreateEvent( NULL,    // no security
			    TRUE,    // explicit reset req
			    FALSE,   // initial event reset
			    NULL ) ; // no name
if (os.hEvent == NULL)
   {
      MessageBox(NULL,  "Failed to create event for thread!", "STAT Error!",
		  MB_ICONEXCLAMATION | MB_OK ) ;
      return ( FALSE ) ;
   }

//printf("status thread started\n\r");    //entry message
do
	{

	//gets the error status info from the driver
	//This IOCTL will not return until the status changes if the escc device
	//was opened with the non-overlapped io.
	//since we opened with overlapped io it will return immed either with
	//the status (if the status bits were set before the call) and the return value will be TRUE
	//or the return value will be FALSE with ERROR_IO_PENDING or a device busy error message
	//the device busy will be returned if a call is made to IOCTL_ESCCDRV_STATUS with an 
	//outstanding call in progress.
	//the return buffer will hold the current status upon completion of the IO request

	t = DeviceIoControl(tp->Hdevescc,IOCTL_ESCCDRV_STATUS,NULL,0,&j,sizeof(ULONG),&returnsize,&os);
	if(t==TRUE)
		{
		//printf("\r\nSTATUS:IMMED %lx\r\n",j);
		//the driver returned status immediatly meaning that the status happened
		//at some time before this call was initiated
//convert these into flags to dispaly on screen in window
		tp->status = j;
		::PostMessage(tp->msgwnd,WM_USER_STATUS_UPDATE,0,0);
		}
	else
		{
		if (GetLastError() == ERROR_IO_PENDING) //overlapped io indicates that the IRP isn't done being processed yet (status hasn't changed)
			{
			// wait for the status to change
			//note that this will block indefinitly unless you give a value
			//different than INFINITE to the waitforsingleobject() call
			//if the status never changes the call to closehandle() on the 
			//esccdevice will cause the cancel routine to cancel the io request
			//and the wait will complete (and if everything goes right the connected 
			//indicator will be false and the thread will terminate)
			do
				{
				k = WaitForSingleObject( os.hEvent, 1000 );//1 second timeout
				if(k==WAIT_TIMEOUT)
					{
					//printf("STATUS:TIMEOUT\r\n");
					//this will execute every 1 second
					//if you want do do some periodic processing here would
					//be a good place to put it...
					}
				if(k==WAIT_ABANDONED)
					{
					//printf("STATUS:ABANDONED\r\n");
					}
				
				}while((k!=WAIT_OBJECT_0)&&(tp->connected));//exit if we get signaled or if the main thread quits
				
				
				//printf("STATUS:SIGNALED:=%lx\r\n",j);
				
				if(tp->connected)                   //if not connected then j is invalid
				{
				GetOverlappedResult(tp->Hdevescc,&os,&returnsize,TRUE); //here to get the actual nobytesread!!!
				tp->status = j;
				t=TRUE;
			    ::PostMessage(tp->msgwnd,WM_USER_STATUS_UPDATE,0,0);
				}
			}
		}
	}while(tp->connected);              //keep making requests until we want to terminate
CloseHandle( os.hEvent ) ;              //we are terminating so close the event
//printf("exiting status thread\n\r");    //exit message
//MessageBox("Status thread done","",MB_OK);
return(FALSE);                          //done
}


UINT ReadProc( LPVOID lpData )
{
struct threaddata *tp;
DWORD j;			//temp
BOOL t;				//temp
DWORD nobytestoread;	//the number of bytes that can be put in data[] (max)
DWORD nobytesread;		//the number of bytes that the driver put in data[]
OVERLAPPED  os ;	//overlapped struct for overlapped I/O

tp = (struct threaddata*)lpData;

memset( &os, 0, sizeof( OVERLAPPED ) ) ;        //wipe the overlapped struct

// create I/O event used for overlapped read

os.hEvent = CreateEvent( NULL,    // no security
			    TRUE,    // explicit reset req
			    FALSE,   // initial event reset
			    NULL ) ; // no name
if (os.hEvent == NULL)
   {
      MessageBox(NULL,  "Failed to create event for thread!", "READ Error!",
		  MB_ICONEXCLAMATION | MB_OK ) ;
      return ( FALSE ) ;
   }

nobytestoread = 4096;		///should allways be 4096, read call will fail with invalid param if it is less than 4096
//printf("read thread started\n\r");     //entry message

do
	{
	//start a read request by calling ReadFile() with the esccdevice handle
	//if it returns true then we received a frame 
	//if it returns false and ERROR_IO_PENDING then there are no
	//receive frames available so we wait until the overlapped struct
	//gets signaled.

	t = ReadFile(tp->Hdevescc,tp->pdata,nobytestoread,&nobytesread,&os);
	
	if(t==TRUE)
		{
		//we have some data here so do something with it ... display it?
		//display our newly received frame
		//printf("received %u bytes:\n\r",nobytesread);
		tp->pdata[nobytesread-1] = 0;
		::PostMessage(tp->msgwnd,WM_USER_READ_UPDATE,0,0);

		}
	else
		{
		if (GetLastError() == ERROR_IO_PENDING)
			{
			// wait for a receive frame to come in, note it will wait forever
			do
				{
				j = WaitForSingleObject( os.hEvent, 1000 );//1 second timeout
				if(j==WAIT_TIMEOUT)
					{
					//this will execute every 1 second
					//you could put a counter in here and if the 
					//driver takes an inordinate ammout of time
					//to complete, you could issue a flush RX command
					//and break out of this loop
					}
				if(j==WAIT_ABANDONED)
					{
					}
				
				}while((j!=WAIT_OBJECT_0)&&(tp->connected));//stay here until we get signaled or the main thread exits
			
				if(tp->connected)
					{                                                       
					GetOverlappedResult(tp->Hdevescc,&os,&nobytesread,TRUE); //here to get the actual nobytesread!!!
					//printf("received %u bytes:\n\r",nobytesread);    //display the number of bytes received
					tp->pdata[nobytesread-1] = 0;
					
					t=TRUE;
					::PostMessage(tp->msgwnd,WM_USER_READ_UPDATE,0,0);
					}
			}
		}
	}while(tp->connected);              //do until we want to terminate

	CloseHandle( os.hEvent ) ;      //done with event
	//MessageBox("read thread done","",MB_OK);
	//printf("exiting read thread\n\r");      //exit messge
	return(TRUE);                   //outta here
	

}