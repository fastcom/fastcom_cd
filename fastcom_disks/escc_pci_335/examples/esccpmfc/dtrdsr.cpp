// dtrdsr.cpp : implementation file
//

#include "stdafx.h"
#include "esccpmfc.h"
#include "dtrdsr.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// dtrdsr dialog


dtrdsr::dtrdsr(CWnd* pParent /*=NULL*/)
	: CDialog(dtrdsr::IDD, pParent)
{
	//{{AFX_DATA_INIT(dtrdsr)
	m_dsr = FALSE;
	m_dtr = FALSE;
	//}}AFX_DATA_INIT
}


void dtrdsr::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(dtrdsr)
	DDX_Check(pDX, IDC_DSR, m_dsr);
	DDX_Check(pDX, IDC_DTR, m_dtr);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(dtrdsr, CDialog)
	//{{AFX_MSG_MAP(dtrdsr)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// dtrdsr message handlers
