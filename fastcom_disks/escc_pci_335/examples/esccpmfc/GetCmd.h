// GetCmd.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGetCmd dialog

class CGetCmd : public CDialog
{
// Construction
public:
	CGetCmd(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CGetCmd)
	enum { IDD = IDD_GETCMD };
	CString	m_cmd;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGetCmd)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CGetCmd)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
