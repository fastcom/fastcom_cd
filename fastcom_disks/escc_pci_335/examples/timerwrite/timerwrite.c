/*
Copyright (c) 1995,1997 Commtech, Inc Wichita ,KS

Module Name:

    timerwrite.c

Abstract:

    A simple Win32 app that uses the escc's hardware timer to write data.  You could easily
	use the timer for other purposes, but a timed write is easy to illustrate.

Environment:

    user mode only
*/

#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include "..\esccptest.h"


VOID main(IN int   argc, IN char *argv[])
{
	HANDLE hDevice;			//handle to the escc driver/device
	DWORD nobyteswritten;	//number of bytes that the driver returns as written to the device
	DWORD nobytestowrite = 128;	//the number of bytes to write to the driver
	struct setup esccsetup;	//setup structure for initializing the escc registers (see escctest.h)
	char data[128];		//character array for data storage (passing to and from the driver)
	OVERLAPPED  os ;		//overlapped structure for use in the transmit routine
	unsigned devnum;
	char devname[80];
	ULONG freq;
	DWORD returnsize;
	DWORD mask;
	ULONG output;
	BOOL t;
	int i;
	ULONG k;
	ULONG temp;
	
	memset(&os,0,sizeof(OVERLAPPED));	//wipe the overlapped structure
	memset(&esccsetup,0,sizeof(struct setup));	//wipe setup structure

	if(argc!=2)
	{
		printf("Usage:%s portnum (i.e. ESCC#)",argv[0]);
		exit(1);
	}
	
	//create I/O event used for overlapped write
	os.hEvent = CreateEvent( NULL,    // no security
		TRUE,    // explicit reset req
		FALSE,   // initial event reset
		NULL ) ; // no name
	if (os.hEvent == NULL)
	{
		printf("Failed to create event for thread!\n");
		exit(1); 
	}

	devnum = atoi(argv[1]);
	sprintf(devname,"\\\\.\\ESCC%u",devnum);
	hDevice = CreateFile (devname,
		  GENERIC_READ | GENERIC_WRITE,
		  0,
		  NULL,
		  OPEN_EXISTING,
		  FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
		  NULL
		  );

    if (hDevice == INVALID_HANDLE_VALUE)	//for some reason the driver won't load or isn't loaded
    {
		printf ("Can't get a handle to esccdrv\n");
		CloseHandle( os.hEvent );	//done with event
		exit(1);	//abort and leave here!!!
	}
	freq = 7372800;
	t = DeviceIoControl(hDevice,IOCTL_ESCCDRV_SET_FREQ,&freq,sizeof(ULONG),NULL,0,&returnsize,NULL);

	esccsetup.mode = 0x8a;
	esccsetup.timr = 0xf2;	//~500 msec timer t=512*(18+1)/19200
	esccsetup.xbcl = 0x00;
	esccsetup.xbch = 0x00;
	esccsetup.ccr0 = 0x80;
	esccsetup.ccr1 = 0x17;
	esccsetup.ccr2 = 0x38;
	esccsetup.ccr3 = 0x00;
	esccsetup.bgr = 0xbf;	//baud = 19200
	esccsetup.iva = 0;
	esccsetup.ipc = 0x03;
	esccsetup.imr0 = 0x04;//disable cdsc
	esccsetup.imr1 = 0x0;
	esccsetup.pvr = 0x0;
	esccsetup.pim = 0xff;
	esccsetup.pcr = 0xe0;
	esccsetup.xad1 = 0xff;
	esccsetup.xad2 = 0xff;
	esccsetup.rah1 = 0xff;
	esccsetup.rah2 = 0xff;
	esccsetup.ral1 = 0xff;
	esccsetup.ral2 = 0xff;
	esccsetup.rlcr = 0x00;
	esccsetup.pre = 0x00;
	esccsetup.ccr4 = 0x00;
	esccsetup.n_rbufs = 20;
	esccsetup.n_rfsize_max = 4096;
	esccsetup.n_tbufs = 20;
	esccsetup.n_tfsize_max = 128;

	t = DeviceIoControl(hDevice,IOCTL_ESCCDRV_SETUP,&esccsetup,sizeof(struct setup),NULL,0,&returnsize,NULL);
	if(t==TRUE)printf("SETTINGS SUCCESSFUL:%lu\n\r",returnsize);
	if(t==FALSE)printf("SETTINGS FAILED:%lu\n\r",returnsize);

//	DeviceIoControl(hDevice,IOCTL_ESCCDRV_START_TIMER,NULL,0,NULL,0,&returnsize,NULL);
	temp=0x10;
	DeviceIoControl(hDevice,IOCTL_ESCCDRV_CMDR,&temp,sizeof(DWORD),NULL,0,&returnsize,NULL);
	printf("timer command issued\n\r");

	for(i=0;i<128;i++)data[i] = (char)i;//fill the frame with the key
	do
	{
		mask = ST_TIN; //(mask out rxdone/txdone messages)
		t = DeviceIoControl(hDevice,IOCTL_ESCCDRV_STATUS,&mask,sizeof(ULONG),&output,sizeof(ULONG),&returnsize,&os);
		if(t==FALSE)
		{
			if (GetLastError() == ERROR_IO_PENDING) //overlapped io indicates that the IRP isn't done being processed yet (status hasn't changed)
			{
				do
				{
					k = WaitForSingleObject( os.hEvent, 1000 );//1 second timeout
					if(k==WAIT_TIMEOUT)
					{
						printf("STATUS:TIMEOUT\r\n");
						//this will execute every 1 second
						//if you want do do some periodic processing here would
						//be a good place to put it...
					}
					if(k==WAIT_ABANDONED)
					{
					}
					
				}while(k!=WAIT_OBJECT_0);//exit if we get signaled or if the main thread quits
				GetOverlappedResult(hDevice,&os,&returnsize,TRUE); //here to get the actual nobytesread!!!
			}
		}
		if((output&ST_TIN)==ST_TIN) printf("STATUS, Timer Expired\r\n");
		
		t = WriteFile(hDevice,&data,nobytestowrite,&nobyteswritten,&os);//send the frame
		if(t==TRUE)printf("TX returned TRUE\n\r"); //if returned true then the IO request was started (txing has begun)
		if(t==FALSE)                    //if it returned FALSE then the IO request is queued (waiting for previous frame to get done sending)
		{                       //and we must wait until the os.event gets signaled before we try any more sending
			printf("TX returned FALSE\n\r");
			if (GetLastError() == ERROR_IO_PENDING)  //IO_PENDING is the indication that the request was queued, if not this than a parameter error occured
			{
				// wait for a second for this transmission to complete
				do
				{
					k = WaitForSingleObject( os.hEvent, 1000 );//1 second timeout
					if(k==WAIT_TIMEOUT)
					{
						printf("Write timeout\n");
						//this will execute every 1 second
						//you could put a counter in here and if the 
						//driver takes an inordinate ammout of time
						//to complete, you could issue a flush TX command
						//and break out of this loop
					}
					if(k==WAIT_ABANDONED)
					{
					}
					
				}while(k!=WAIT_OBJECT_0);
			}
		}
	}while(!_kbhit());

	CloseHandle (hDevice);// stops the escc from interrupting (ie shuts it down)
	CloseHandle(os.hEvent);

}