/*++

Copyright (c) 1995,1997 Commtech, Inc Wichita ,KS

Module Name:

    escctest.c

Abstract:

    A simple Win32 app that uses the esccdrv device

Environment:

    user mode only

Notes:

    
Revision History:

    7-9-1997             started
	9-8-1999			switched from sending 10 1024 byte frames to 1.

--*/



#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include "..\esccptest.h"


DWORD FAR PASCAL StatProc( LPVOID lpData );
DWORD FAR PASCAL ReadProc( LPVOID lpData );
		
BOOL connected;

VOID
main(
    IN int   argc,
    IN char *argv[]
    )
/*++

Routine Description:

Arguments:

Return Value:

--*/
{
	HANDLE hDevice;			//handle to the escc driver/device
	
	DWORD nobyteswritten;	//number of bytes that the driver returns as written to the device
	struct setup esccsetup;	//setup structure for initializing the escc registers (see escctest.h)
	char data[4096];		//character array for data storage (passing to and from the driver)
	DWORD nobytestoread;	//the number of bytes to read from the driver
	DWORD nobytestowrite;	//the number of bytes to write to the driver
	ULONG j,k;				//temp vars
	DWORD returnsize;		//temp vars
	ULONG i;				//temp vars
	BOOL t;					//temp vars
	HANDLE        hstatThread ;	//handle to status thread 
	HANDLE            hreadThread ;	//handle to read thread
	DWORD            statID;		//status thread ID storage
	DWORD            readID;		//read thread ID storage
	DWORD         dwThreadID ;		//temp Thread ID storage
	LPVOID  phdev;					//pointer to device Handle
	OVERLAPPED  os ;				//overlapped structure for use in the transmit routine
	char devname[80];
	unsigned devno;
	ULONG regdata;
	ULONG regport;
	struct regsingle regs;
	ULONG freq;

	memset( &os, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
	
	// create I/O event used for overlapped write
	
	os.hEvent = CreateEvent( NULL,    // no security
		TRUE,    // explicit reset req
		FALSE,   // initial event reset
		NULL ) ; // no name
	if (os.hEvent == NULL)
	{
		MessageBox( NULL, "Failed to create event for thread!", "main Error!",
			MB_ICONEXCLAMATION | MB_OK ) ;
		return; 
	}
	
	
	nobytestoread = 4096;    //this should allways be 4096, it is the maximum frame size returned from the driver at one time
	nobytestowrite = 1024;	//this is the number of bytes to send to the driver to send as a single frame (can be from 1 to 4096)
    
	//this will start up the driver, it also gives us a handle to send
	//and receive data from the driver
	//note that it is created with the FILE_FLAG_OVERLAPPED set
	//if it is not then you must make sequential calls to the driver
	//(ie wait until one call has returned before making another call)
	//the read/write and status functions are set up to use overlapped, they 
	//will return immediatly with a ERROR_IO_PENDING or immediatly with the required data
	//
	if(argc<3)
	{
		printf("Usage:\r\nescctest device# Mode\r\ndevice# = 0,1,2,3...\r\nMODE = a = async, b = bisync, h= hdlc\r\n");
		exit(1);
	}
	devno = 0;
	if(argc>1) devno = atoi(argv[1]);
	
	sprintf(devname,"\\\\.\\ESCC%u",devno);
    hDevice = CreateFile (devname,
		GENERIC_READ | GENERIC_WRITE,
		0,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
		NULL
		);
	
    if (hDevice == INVALID_HANDLE_VALUE)
    {
		//for some reason the driver won't load or isn't loaded
		printf ("Can't get a handle to esccdrv\n");
		exit(1);
		//abort and leave here!!!
	}
	//we got our handle and are ready to go
	printf("Created esccdrv--ESCC%u\n\r",devno);
	//this IOCTL function demonstrates how to set the clock generator on 
	//the escc-p card
	//this function will return TRUE unless an invalid parameter is given
	//
	//the 335 board has a range from 6-33MHz, set to 6, and divide down in bgr
	//note the ioctl changed as well
	freq = 6000000;
	t = DeviceIoControl(hDevice,IOCTL_ESCCDRV_SET_FREQ,&freq,sizeof(ULONG),NULL,0,&returnsize,NULL);
	
	
	//this IOCTL function is used to get at individual registers
	//it takes a ULONG offset from the base address for the register to read
	//it returns the data from that register
	//note that this should be used sparingly as it forces a 
	//transition to the DIRQL of the ESCC ISR to execute, 
	//it is here mainly to do quick changes without having to reinitialize
	//the entire card, (changing addresses, baud rates, etc)
	regport = VSTR; //get version status register
	
	t = DeviceIoControl(hDevice,IOCTL_ESCCDRV_READ_REGISTER,&regport,sizeof(ULONG),&regdata,sizeof(ULONG),&returnsize,NULL);
	printf("ESCC 82532 version status:%x\r\n",regdata);
	
	//this IOCTL function is used to set individual registers
	regs.port = XAD1;
	regs.data = 0x01;//set the transmit address 1 to 0x01
	t = DeviceIoControl(hDevice,IOCTL_ESCCDRV_WRITE_REGISTER,&regs,sizeof(struct regsingle),NULL,0,&returnsize,NULL);
	//note that this will be overridden further down the line, it is just here so you
	//can see how to do it.
	
	//this IOCTL function is for information only it serves no real purpose
	//it will however, return the number of completed but unread READ buffers
	//being held in the driver
	//this function will return TRUE unless an invalid parameter is given
	//
	t = DeviceIoControl(hDevice,IOCTL_ESCCDRV_RX_READY,NULL,0,&j,sizeof(DWORD),&returnsize,NULL);
	//j will hold the number of receive buffers that are ready to be Read by ReadFile
	//
	printf("# receive buffers ready:%lu\n\r",j);
	
	//to initialize the escc to esccsetup parameters
	//this IOCTL function must be called to setup the escc internal 
	//registers prior to calling any of the read/write functions
	//for information reguarding what each register does refer to 
	//the SIEMENS 82532 data sheet.
	//
	printf("resetting\n\r");
	if(argc>2)
	{
		if((argv[2][0]=='H')||(argv[2][0]=='h'))
		{
			printf("HDLC settings\r\n");
			//make to use HDLC settings here
			esccsetup.mode = 0x88;
			esccsetup.timr = 0x1f;
			esccsetup.xbcl = 0x00;
			esccsetup.xbch = 0x00;
			esccsetup.ccr0 = 0x80;
			esccsetup.ccr1 = 0x16; 
			esccsetup.ccr2 = 0x38; 
			esccsetup.ccr3 = 0x00;
			esccsetup.bgr = 0x00;
			esccsetup.iva = 0;
			esccsetup.ipc = 0x03;
			esccsetup.imr0 = 0x04;//disable cdsc
			esccsetup.imr1 = 0x0;
			esccsetup.pvr = 0x0;
			esccsetup.pim = 0xff;
			esccsetup.pcr = 0xe0;
			esccsetup.xad1 = 0xff;
			esccsetup.xad2 = 0xff;
			esccsetup.rah1 = 0xff;
			esccsetup.rah2 = 0xff;
			esccsetup.ral1 = 0xff;
			esccsetup.ral2 = 0xff;
			esccsetup.rlcr = 0x00;
			esccsetup.pre = 0x00;
			esccsetup.ccr4 = 0x00;
			//clock mode 6b, bitrate = 1.544E6/((0+1)*2)/16 = 48250 bps
		}
		if((argv[2][0]=='A')||(argv[2][0]=='a'))
		{
			//make to use async settings here
			printf("ASYNC settings\r\n");
			esccsetup.mode = 0x08;
			esccsetup.timr = 0x1f;
			esccsetup.xbcl = 0x00;
			esccsetup.xbch = 0x00;
			esccsetup.ccr0 = 0x83;   //async
			esccsetup.ccr1 = 0x1f; //bit clock rate =16, clock mode 7
			esccsetup.ccr2 = 0x38;
			esccsetup.bgr = 0x00;	
			esccsetup.iva = 0;
			esccsetup.ipc = 0x03;
			esccsetup.imr0 = 0x04;
			esccsetup.imr1 = 0x0;
			esccsetup.pvr = 0x0;
			esccsetup.pim = 0xff;
			esccsetup.pcr = 0xe0;
			esccsetup.tcr = 0x00;
			esccsetup.dafo = 0x00;	//N81
			esccsetup.rfc = 0x0c;	//1c stores parity/framing with data every other byte...
			//clock mode 7b, bitrate = 1.544E6/((0+1)*2)/16 = 48250 bps , (the /16 comes from the bit clock rate bit of ccr1)
		}
		if((argv[2][0]=='B')||(argv[2][0]=='b'))
		{
			//make to use bisync settings here
			printf("BISYNC settings\r\n");
			esccsetup.mode = 0x3c;	//8bit bisync
			esccsetup.timr = 0x1f;
			esccsetup.synl = 0x00;   //first SYN char
			esccsetup.synh = 0x5e;	//second SYN char  
			esccsetup.tcr = 0xff;	//reset receive when line is idle (0xff)
			esccsetup.dafo = 0x00;   //no parity, 8 bit data
			esccsetup.rfc = 0x4d;	//don't save parity, don't store SYN char's 32 byte fifo trigger, TCD enabled
			esccsetup.ccr0 = 0x82;	//powerup, NRZ, BISYNC
			esccsetup.ccr1 = 0x17;	
			esccsetup.ccr2 = 0x038;	
			esccsetup.ccr3 = 0x00;	
			esccsetup.bgr = 0x07;
			esccsetup.pre = 0x00;	//preamble (if enabled)
			esccsetup.iva = 0;
			esccsetup.ipc = 0x03;
			esccsetup.imr0 = 0x04;//disable cdsc
			esccsetup.imr1 = 0x0;
			esccsetup.pvr = 0x0;
			esccsetup.pim = 0xff;
			esccsetup.pcr = 0xe0;
			esccsetup.xbch = 0x00;
			//clock mode 7b, bitrate = 6E6/((7+1)*2) 
		}
		if((argv[2][0]=='E')||(argv[2][0]=='e'))
		{
			printf("HDLC Extended Transparent Mode 0 settings\r\n");
			esccsetup.mode = 0xC0;
			esccsetup.timr = 0x1f;
			esccsetup.xbcl = 0x00;
			esccsetup.xbch = 0x00;
			esccsetup.ccr0 = 0x80;
			esccsetup.ccr1 = 0x17; 
			esccsetup.ccr2 = 0x18; 
			esccsetup.ccr3 = 0x00;
			esccsetup.bgr = 0x07;
			esccsetup.iva = 0;
			esccsetup.ipc = 0x03;
			esccsetup.imr0 = 0x04;//disable cdsc
			esccsetup.imr1 = 0x0;
			esccsetup.pvr = 0x0;
			esccsetup.pim = 0xff;
			esccsetup.pcr = 0xe0;
			esccsetup.xad1 = 0xff;
			esccsetup.xad2 = 0xff;
			esccsetup.rah1 = 0xff;
			esccsetup.rah2 = 0xff;
			esccsetup.ral1 = 0xff;
			esccsetup.ral2 = 0xff;
			esccsetup.rlcr = 0x00;
			esccsetup.pre = 0x00;
			esccsetup.ccr4 = 0x00;
		}
	}
	else
	{
		printf("HDLC settings\r\n");
		//make to use HDLC settings here
		esccsetup.mode = 0x88;
		esccsetup.timr = 0x1f;
		esccsetup.xbcl = 0x00;
		esccsetup.xbch = 0x00;
		esccsetup.ccr0 = 0x80;
		esccsetup.ccr1 = 0x16;
		esccsetup.ccr2 = 0x38;
		esccsetup.ccr3 = 0x00;
		esccsetup.bgr = 0x00;
		esccsetup.iva = 0;
		esccsetup.ipc = 0x03;
		esccsetup.imr0 = 0x04;//disable cdsc
		esccsetup.imr1 = 0x0;
		esccsetup.pvr = 0x0;
		esccsetup.pim = 0xff;
		esccsetup.pcr = 0xe0;
		esccsetup.xad1 = 0xff;
		esccsetup.xad2 = 0xff;
		esccsetup.rah1 = 0xff;
		esccsetup.rah2 = 0xff;
		esccsetup.ral1 = 0xff;
		esccsetup.ral2 = 0xff;
		esccsetup.rlcr = 0x00;
		esccsetup.pre = 0x00;
		esccsetup.ccr4 = 0x00;
	}

	esccsetup.n_rbufs = 10;
	esccsetup.n_rfsize_max = 4096;
	esccsetup.n_tbufs = 10;
	esccsetup.n_tfsize_max = 4096;

	//when called this IOCTL will set the registers of the escc and
	//reset the TX and RX machines (issuing a separate XRES and RHR command)
	//if the resets time out the function will return FALSE, otherwise it will
	//return TRUE
	t = DeviceIoControl(hDevice,IOCTL_ESCCDRV_SETUP,&esccsetup,sizeof(struct setup),NULL,0,&returnsize,NULL);
	if(t==TRUE)printf("SETTINGS SUCCESSFUL:%lu\n\r",returnsize);
	if(t==FALSE)printf("SETTINGS FAILED:%lu\n\r",returnsize);


	//this IOCTL is used to set the transmit type.
	//the options are to send transparent frames or information frames
	//(determines if tx is initiated with a XTF command or a XIF command)
	//this is only usefull in HDLC mode, it is ignored in ASYNC, and should allways be 
	//set to its default (XTF) for BISYNC.
	// 0 == XTF is used to transmit frames
	// 1 == XIF is used to transmit frames
	//
	// this should only be set when the escc is not transmitting, it will return FALSE if a frame
	// is being transmitted
	//
	// This command should only be used to send XIF frames if the escc is setup in AUTO mode
	//
	//on startup the tx_type defaults to XTF (so this call isn't really necessary except to show that it exists)

	k = 0; 
	t = DeviceIoControl(hDevice,IOCTL_ESCCDRV_SET_TX_TYPE,&k,sizeof(ULONG),NULL,0,&returnsize,NULL);


	//this IOCTL is used to change the state of the DTR line
	// if 0 is passed the DTR goes low
	// if 1 is passed thn DTR goes high
	//clear dtr here
	k = 0; 
	t = DeviceIoControl(hDevice,IOCTL_ESCCDRV_SET_DTR,&k,sizeof(ULONG),NULL,0,&returnsize,NULL);

	//this IOCTL is used to check the state of the DTR/DSR lines
	//on return the value will be:
	//bit 0 = DTR state
	//bit 1 = DSR state
	//all others should be masked.
	k = 0; 
	t = DeviceIoControl(hDevice,IOCTL_ESCCDRV_GET_DSR,NULL,0,&k,sizeof(ULONG),&returnsize,NULL);
	k = k &0x03;
	if((k&1)==1) printf("DTR SET\r\n");
	else printf("DTR not SET\r\n");
	if((k&2)==2) printf("DSR SET\r\n");
	else printf("DSR not SET\r\n");

	//set dtr here
	k = 1; 
	t = DeviceIoControl(hDevice,IOCTL_ESCCDRV_SET_DTR,&k,sizeof(ULONG),NULL,0,&returnsize,NULL);
	//this IOCTL is used to check the state of the DTR/DSR lines
	//on return the value will be:
	//bit 0 = DTR state
	//bit 1 = DSR state
	//all others should be masked.

	k = 0; 
	t = DeviceIoControl(hDevice,IOCTL_ESCCDRV_GET_DSR,NULL,0,&k,sizeof(ULONG),&returnsize,NULL);
	k = k & 0x3; //mask bits
	if((k&1)==1) printf("DTR SET\r\n");
	else printf("DTR not SET \r\n");
	if((k&2)==2) printf("DSR SET\r\n");
	else printf("DSR not SET\r\n");

	//now we will create the threads that will do all the work, it would be 
	//nice to create separate windows for each but that will have to wait for
	//another example.  Two threads will be created a dedicated thread to check on
	//the status of the escc and a dedicated thread to do READ function calls
	//startup support threads, the main thread (this one) will be used to transmit
	connected = TRUE;
	phdev = &hDevice;

	hreadThread =     CreateThread( (LPSECURITY_ATTRIBUTES) NULL,
								   0, 
								   (LPTHREAD_START_ROUTINE) ReadProc,
								   (LPVOID) phdev,
								   0, &dwThreadID );

	if(hreadThread==NULL)
	{
		printf("cannot start Data read thread\n\r");
		CloseHandle(hDevice);
		exit(1);
	}
	readID=dwThreadID;
	hstatThread =     CreateThread( (LPSECURITY_ATTRIBUTES) NULL,
								   0, 
								   (LPTHREAD_START_ROUTINE) StatProc,
								   (LPVOID) phdev,
								   0, &dwThreadID );

	if(hstatThread==NULL)
	{
		printf("cannot start status thread\n\r");
		CloseHandle(hDevice);
		exit(1);
	}
	statID=dwThreadID;





	//starts the escc timer (notice no parameters passed)
	//this function will start the timer (if in external mode)
	//this should force the status thread to become active with
	//a ST_TIN response at a time specified by the value of k and TIMR in the escc registers
	//this function will allways return TRUE 
	t = DeviceIoControl(hDevice,IOCTL_ESCCDRV_START_TIMER,NULL,0,NULL,0,&returnsize,NULL);

	//stops the escc timer (notice no parameters passed)
	//this function will stop the timer (if in external mode)
	//this should prevent a ST_TIN response if the timer was previously started (as above)
	//this function will allways return TRUE 
	t = DeviceIoControl(hDevice,IOCTL_ESCCDRV_STOP_TIMER,NULL,0,NULL,0,&returnsize,NULL);



	//now we enter the main loop of this thread        
	//all we are going to do is wait for a keyhit and when we 
	//get one we will fill a frame with that key and send it out
	//the escc using a WriteFile() to the escc device
	//if the [ESC] key is pressed the program will terminate
	do
	{
		printf("waiting for a key\n\r");
	kht:        
		j = getch();                            //wait for keyhit
		if((j&0xff)=='r')
		{
			//if the user presses r on the keyboard, we will flush the receive buffers.
			//this will clear all of the internal receive buffers and issue a RHR command
			//to the ESCC.  It will allways return TRUE unless the RHR command times out
			DeviceIoControl(hDevice,IOCTL_ESCCDRV_FLUSH_RX,NULL,0,NULL,0,&returnsize,NULL);
			printf("RX flushed\r\n");
			goto kht;
		}
		if((j&0xff)=='t')
		{
			//if the user presses t on the keyboard, we will flush the transmit buffers.
			//this will clear all of the internal transmit buffers and issue a XRES command
			//to the ESCC.  It will allways return TRUE unless the XRES command times out
			DeviceIoControl(hDevice,IOCTL_ESCCDRV_FLUSH_TX,NULL,0,NULL,0,&returnsize,NULL);
			printf("TX flushed\r\n");
			goto kht;
		}
		if((j&0xff)=='i')
		{
			//alternate way of issuing timer commands, via the IOCTL_ESCCDRV_CMDR interface,
			//this will take the value passed and send it directly to the escc CMDR register.
			//It should allways return TRUE
			//if the user presses i the this timer command will be issued
			k = 0x10;
			DeviceIoControl(hDevice,IOCTL_ESCCDRV_CMDR,&k,sizeof(DWORD),NULL,0,&returnsize,NULL);
			printf("timer command issued\n\r");
			goto kht;
		}
		if((j&0xff)=='p')
		{
			//if the user presses p then stop the timer.
			DeviceIoControl(hDevice,IOCTL_ESCCDRV_STOP_TIMER,NULL,0,NULL,0,&returnsize,NULL);
			printf("timer stopped\n\r");
			goto kht;
		}
		if(((argv[2][0]=='B')||(argv[2][0]=='b'))&&((j&0xff)=='h'))
		{
			k = HUNT;
			DeviceIoControl(hDevice,IOCTL_ESCCDRV_CMDR,&k,sizeof(DWORD),NULL,0,&returnsize,NULL);
			printf("HUNT command issued\n\r");
			goto kht;
		}
		if((j&0xff)!=27)                        //not esc?
		{
			//here the user has pressed a key that was not t,r,i,p or [esc] so we will
			//take that character and fill a data buffer with it, then send that 
			//buffer out the escc as a frame.
			
			
			for(i=0;i<1024;i++)data[i] = (char)j;//fill the frame with the key
			
			t = WriteFile(hDevice,&data,nobytestowrite,&nobyteswritten,&os);//send the frame
			printf("WRITEFILE esccdrv%lu \n\r",nobyteswritten);//if nobyteswritten doesnt = sizeof(struct buf) something is wrong
			if(t==TRUE)printf("TX returned TRUE\n\r"); //if returned true then the IO request was started (txing has begun)
			if(t==FALSE)                    //if it returned FALSE then the IO request is queued (waiting for previous frame to get done sending)
			{                       //and we must wait until the os.event gets signaled before we try any more sending
				printf("TX returned FALSE\n\r");
				if (GetLastError() == ERROR_IO_PENDING)  //IO_PENDING is the indication that the request was queued, if not this than a parameter error occured
				{
					// wait for a second for this transmission to complete
					do
					{
						k = WaitForSingleObject( os.hEvent, 1000 );//1 second timeout
						if(k==WAIT_TIMEOUT)
						{
							//this will execute every 1 second
							//you could put a counter in here and if the 
							//driver takes an inordinate ammout of time
							//to complete, you could issue a flush TX command
							//and break out of this loop
							
						}
						if(k==WAIT_ABANDONED)
						{
						}
						
					}while(k!=WAIT_OBJECT_0);
				}
			}               
			
		}
		
	}while((j&0xff)!=27);         //keep getting keys and sending frames until esc is pressed

	//when we want to exit the program there is still the possibility that
	//a frame is being transmitted so we spin in this IOCTL function until
	//the driver reports that it is not transmitting
	//technically you could omit this if you are leaving for good
	//(ie are done computing for the day, and are about to shutdown NT)
	//but it might cause problems if the escc device is started again
	//without powering down, misc errors will occur when the device is 
	//re-opened as it was halted in mid transmitting
	//EXE interrupts are most likely
	//
	i = 0;
	j = 0;
	do
	{
		t = DeviceIoControl(hDevice,IOCTL_ESCCDRV_TX_ACTIVE,NULL,0,&j,sizeof(DWORD),&returnsize,NULL);
		//will return 0 in output buffer (j) if not active
		//will return 1 in output buffer (j) if active
	}while(j==1);//keep requesting until not active
	//carefull not to close the device while transmitting or receiving data

	connected = FALSE;      //indicate to threads that we are leaving

	CloseHandle (hDevice);// stops the escc from interrupting (ie shuts it down)
	Sleep(5000);
	CloseHandle (hreadThread);                //done with this thread
	CloseHandle (hstatThread);                //done with this thread
	CloseHandle( os.hEvent ) ;      //done with event
	printf("exiting program\n\r");          //exit message
}                                               //done
						 

DWORD FAR PASCAL StatProc( LPHANDLE lpData )
{
	HANDLE hdev; //device handle to esccdrv
	DWORD j,k;	//temp storage
	BOOL t;		//temp storage
	DWORD returnsize;	//temp storage
	OVERLAPPED  os ;	//overlapped struct for driver signaling
	DWORD mask;
	hdev = (LPHANDLE)lpData[0];     //the escc device handle is passed in to this thread
				
	memset( &os, 0, sizeof( OVERLAPPED ) ) ; //wipe the overlapped structure
	
	// create I/O event used for overlapped read
	
	os.hEvent = CreateEvent( NULL,    // no security
		TRUE,    // explicit reset req
		FALSE,   // initial event reset
		NULL ) ; // no name
	if (os.hEvent == NULL)
	{
		MessageBox( NULL, "Failed to create event for thread!", "STAT Error!",
			MB_ICONEXCLAMATION | MB_OK ) ;
		return ( FALSE ) ;
	}
	
	printf("status thread started\n\r");    //entry message
	do
	{
		
		//gets the error status info from the driver
		//This IOCTL will not return until the status changes if the escc device
		//was opened with the non-overlapped io.
		//since we opened with overlapped io it will return immed either with
		//the status (if the status bits were set before the call) and the return value will be TRUE
		//or the return value will be FALSE with ERROR_IO_PENDING or a device busy error message
		//the device busy will be returned if a call is made to IOCTL_ESCCDRV_STATUS with an 
		//outstanding call in progress.
		//the return buffer will hold the current status upon completion of the IO request
		mask = 0xFFDFFFFE; //(mask out rxdone/txdone messages)
		t = DeviceIoControl(hdev,IOCTL_ESCCDRV_STATUS,&mask,sizeof(ULONG),&j,sizeof(ULONG),&returnsize,&os);
		if(t==FALSE)
		{
			if (GetLastError() == ERROR_IO_PENDING) //overlapped io indicates that the IRP isn't done being processed yet (status hasn't changed)
			{
				// wait for the status to change
				//note that this will block indefinitly unless you give a value
				//different than INFINITE to the waitforsingleobject() call
				//if the status never changes the call to closehandle() on the 
				//esccdevice will cause the cancel routine to cancel the io request
				//and the wait will complete (and if everything goes right the connected 
				//indicator will be false and the thread will terminate)
				do
				{
					k = WaitForSingleObject( os.hEvent, 1000 );//1 second timeout
					if(k==WAIT_TIMEOUT)
					{
						//printf("STATUS:TIMEOUT\r\n");
						//this will execute every 1 second
						//if you want do do some periodic processing here would
						//be a good place to put it...
					}
					if(k==WAIT_ABANDONED)
					{
						//printf("STATUS:ABANDONED\r\n");
					}
					
				}while((k!=WAIT_OBJECT_0)&&(connected));//exit if we get signaled or if the main thread quits
				//printf("STATUS:SIGNALED:=%lx\r\n",j);
				if(connected) GetOverlappedResult(hdev,&os,&returnsize,TRUE); //here to get the actual nobytesread!!!
			}
		}
		if(connected)                   //if not connected then j is invalid
		{
			if((j&ST_RX_DONE)==ST_RX_DONE) printf("STATUS, RX_DONE\r\n");
			if((j&ST_OVF)==ST_OVF) printf("STATUS, BUFFERS overflowed\n\r");
			if((j&ST_RFS)==ST_RFS) printf("STATUS, Receive Frame Start\r\n");
			if((j&ST_RX_TIMEOUT)==ST_RX_TIMEOUT) printf("STATUS, Receive Timeout\r\n");
			if((j&ST_RSC)==ST_RSC) printf("STATUS, Receive Status Change\r\n");
			if((j&ST_PERR)==ST_PERR) printf("STATUS, Parity Error\r\n");
			if((j&ST_PCE)==ST_PCE) printf("STATUS, Protocol Error\r\n");
			if((j&ST_FERR)==ST_FERR) printf("STATUS, Framing Error\r\n");
			if((j&ST_SYN)==ST_SYN) printf("STATUS, SYN detected\r\n");
			if((j&ST_DPLLA)==ST_DPLLA) printf("STATUS, DPLL Asynchronous\r\n");
			if((j&ST_CDSC)==ST_CDSC) printf("STATUS, Carrier Detect Change State\r\n");
			if((j&ST_RFO)==ST_RFO) printf("STATUS, Receive Frame Overflow(HARDWARE)\r\n");
			if((j&ST_EOP)==ST_EOP) printf("STATUS, End of Poll\r\n");
			if((j&ST_BRKD)==ST_BRKD) printf("STATUS, Break Detected\r\n");
			if((j&ST_ONLP)==ST_ONLP) printf("STATUS, On Loop\r\n");
			if((j&ST_BRKT)==ST_BRKT) printf("STATUS, Break Terminated\r\n");
			if((j&ST_ALLS)==ST_ALLS) printf("STATUS, All Sent\r\n");
			if((j&ST_EXE)==ST_EXE) printf("STATUS, Transmit Underrun\r\n");
			if((j&ST_TIN)==ST_TIN) printf("STATUS, Timer Expired\r\n");
			if((j&ST_CTSC)==ST_CTSC) printf("STATUS, CTS Changed State\r\n");
			if((j&ST_XMR)==ST_XMR) printf("STATUS, Transmit Message Repeat\r\n");
			if((j&ST_TX_DONE)==ST_TX_DONE) printf("STATUS, TX Done\r\n");
			if((j&ST_DMA_TC)==ST_DMA_TC) printf("STATUS, DMA TC reached\r\n");
			if((j&ST_DSR1C)==ST_DSR1C) printf("STATUS, Channel 1 DSR Changed\r\n");
			if((j&ST_DSR0C)==ST_DSR0C) printf("STATUS, Channel 0 DSR Changed\r\n");
		}
		
	}while(connected);              //keep making requests until we want to terminate
	CloseHandle( os.hEvent ) ;              //we are terminating so close the event
	printf("exiting status thread\n\r");    //exit message
	return(FALSE);                          //done
}


DWORD FAR PASCAL ReadProc( LPHANDLE lpData )
{
	HANDLE hdev;		//handle to the esccdrv device
	DWORD i,j;			//temp
	BOOL t;				//temp
	char data[4096];	//data storage for data from the driver
	DWORD nobytestoread;	//the number of bytes that can be put in data[] (max)
	DWORD nobytesread;		//the number of bytes that the driver put in data[]
	OVERLAPPED  os ;	//overlapped struct for overlapped I/O
	
	memset( &os, 0, sizeof( OVERLAPPED ) ) ;        //wipe the overlapped struct
	
	// create I/O event used for overlapped read
	
	os.hEvent = CreateEvent( NULL,    // no security
		TRUE,    // explicit reset req
		FALSE,   // initial event reset
		NULL ) ; // no name
	if (os.hEvent == NULL)
	{
		MessageBox( NULL, "Failed to create event for thread!", "READ Error!",
			MB_ICONEXCLAMATION | MB_OK ) ;
		return ( FALSE ) ;
	}
	
	hdev = (LPHANDLE)lpData[0];     //we get the escc device handle passed to us
	
	nobytestoread = 4096;		///should allways be 4096, read call will fail with invalid param if it is less than 4096
	printf("read thread started\n\r");     //entry message
	
	do
	{
		//start a read request by calling ReadFile() with the esccdevice handle
		//if it returns true then we received a frame 
		//if it returns false and ERROR_IO_PENDING then there are no
		//receive frames available so we wait until the overlapped struct
		//gets signaled.
		
		t = ReadFile(hdev,&data,nobytestoread,&nobytesread,&os);
		
		if(t==FALSE)
		{
			if (GetLastError() == ERROR_IO_PENDING)
			{
				// wait for a receive frame to come in, note it will wait forever
				do
				{
					j = WaitForSingleObject( os.hEvent, 1000 );//1 second timeout
					if(j==WAIT_TIMEOUT)
					{
						//this will execute every 1 second
						//you could put a counter in here and if the 
						//driver takes an inordinate ammout of time
						//to complete, you could issue a flush RX command
						//and break out of this loop
					}
					if(j==WAIT_ABANDONED)
					{
					}
					
				}while((j!=WAIT_OBJECT_0)&&(connected));//stay here until we get signaled or the main thread exits
				if(connected)GetOverlappedResult(hdev,&os,&nobytesread,TRUE); //here to get the actual nobytesread!!!
				
			}
			if(connected)
			{                                                       
				printf("received %u bytes:\n\r",nobytesread);    //display the number of bytes received
				for(i=0;i<nobytesread;i++)printf("%c",data[i]);  //display the buffer
				printf("\n\r");
				t=TRUE;
			}
			
		}
	}while(connected);              //do until we want to terminate
	
	CloseHandle( os.hEvent ) ;      //done with event
	printf("exiting read thread\n\r");      //exit messge
	return(TRUE);                   //outta here
}
