//Copyright (c) 1995,1997 Commtech, Inc. Wichtia, KS
// esccdrv.h  -- header file for esccdrv 
//
// The ESCC device driver IOCTLs
//
#define IOCTL_ESCCDRV_SET_CMDR_WAITTIME					0x830021a4
#define IOCTL_ESCCDRV_SET_GOBBLE						0x830021a0
#define IOCTL_ESCCDRV_TIMETAG							0x8300219c
#define IOCTL_ESCCDRV_RX_ECHO_CANCEL					0x83002198
#define IOCTL_ESCCDRV_POSTAMBLE_DELAY					0x83002190
#define IOCTL_ESCCDRV_POSTAMBLE_EN						0x8300218c
#define IOCTL_ESCCDRV_BLOCK_MULTIPLE_IO					0x83002188
#define IOCTL_ESCCDRV_GET_FREQ							0x83002184
#define IOCTL_ESCCDRV_GET_FEATURES						0x83002180
#define IOCTL_ESCCDRV_SET_FEATURES						0x8300217c
#define IOCTL_ESCCDRV_SET_FREQ							0x83002178
#define IOCTL_ESCCDRV_SET_BISYNC_END_PATTERN			0x83002174
#define IOCTL_ESCCDRV_SET_BISYNC_SIZE_CUTOFF_SIZE		0x83002170
#define IOCTL_ESCCDRV_SET_BISYNC_START_PATTERN			0x8300216c
#define IOCTL_ESCCDRV_BISYNC_END_PATTERN_MATCH_EN		0x83002168
#define IOCTL_ESCCDRV_LSB2MSB_CONVERT_EN				0x83002164
#define IOCTL_ESCCDRV_BISYNC_SIZE_CUTOFF_EN				0x83002160
#define IOCTL_ESCCDRV_BISYNC_START_PATTERN_MATCH_EN		0x8300215C
#define IOCTL_ESCCDRV_CANCEL_STATUS		0x8300213c
#define IOCTL_ESCCDRV_CANCEL_TX			0x83002138
#define IOCTL_ESCCDRV_CANCEL_RX			0x83002134
#define IOCTL_ESCCDRV_START_SYNC		0x83002120
#define IOCTL_ESCCDRV_RECEIVE_MULTIPLE	0x8300211c
#define IOCTL_ESCCDRV_STOP_LN200		0x83002118
#define IOCTL_ESCCDRV_START_LN200		0x83002114
#define IOCTL_ESCCDRV_GET_GPS_TIME		0x83002110
#define IOCTL_ESCCDRV_SET_GPS_TIME		0x8300210c
#define IOCTL_ESCCDRV_GET_INTERNAL_INFO 0x83002108
#define IOCTL_ESCCDRV_VERIFY_TIMER      0x83002104
#define IOCTL_ESCCDRV_IMMEDIATE_STATUS  0x83002100
#define IOCTL_ESCCDRV_WRITE_REGISTER 0x830020fc
#define IOCTL_ESCCDRV_READ_REGISTER	0x830020f8
#define IOCTL_ESCCDRV_SET_CLOCK		0x830020f4
#define IOCTL_ESCCDRV_SET_DTR		0x830020f0
#define IOCTL_ESCCDRV_GET_DSR		0x830020ec
#define IOCTL_ESCCDRV_CMDR			0x830020e8
#define IOCTL_ESCCDRV_FLUSH_TX		0x830020e4
#define IOCTL_ESCCDRV_FLUSH_RX		0x830020e0
#define IOCTL_ESCCDRV_SET_TX_ADD	0x830020dc
#define IOCTL_ESCCDRV_SET_TX_TYPE	0x830020d8
#define IOCTL_ESCCDRV_STOP_TIMER	0x830020d4
#define IOCTL_ESCCDRV_START_TIMER	0x830020d0
#define IOCTL_ESCCDRV_STATUS		0x830020cc
#define IOCTL_ESCCDRV_SETUP			0x830020c8
#define IOCTL_ESCCDRV_RX_READY		0x830020c4
#define IOCTL_ESCCDRV_TX_ACTIVE		0x830020c0


typedef struct setup{
//used in all
unsigned cmdr;
unsigned mode;
unsigned timr;
unsigned xbcl;
unsigned xbch;
unsigned ccr0;
unsigned ccr1;
unsigned ccr2;
unsigned ccr3;
unsigned ccr4;
unsigned tsax;
unsigned tsar;
unsigned xccr;
unsigned rccr;
unsigned bgr;
unsigned iva;
unsigned ipc;
unsigned imr0;
unsigned imr1;
unsigned pvr;
unsigned pim;
unsigned pcr;
//escc register defines for HDLC/SDLC mode
unsigned xad1;
unsigned xad2;
unsigned rah1;
unsigned rah2;
unsigned ral1;
unsigned ral2;
unsigned rlcr;
unsigned aml;
unsigned amh;
unsigned pre;
//escc async register defines (used in bisync as well)
unsigned xon;
unsigned xoff;
unsigned tcr;
unsigned dafo;
unsigned rfc;
unsigned tic;
unsigned mxn;
unsigned mxf;

//escc bisync register defines
unsigned synl;
unsigned synh;

unsigned n_rbufs;
unsigned n_tbufs;
unsigned n_rfsize_max;
unsigned n_tfsize_max;

} SETUP;

typedef struct clkset{
	ULONG clockbits;
	USHORT numbits;
} CLKSET;

struct regsingle{ //used for ESCC_WRITE_REGISTER ioctl call
	ULONG port;		//offset from base address of register to access
	UCHAR data;		//data to write
};

#define MAX_PATTERN_RECOG 16 //used for bisync pattern recognition 

struct bisync_start_pattern{
	ULONG count;
	UCHAR pattern[MAX_PATTERN_RECOG];
};

//device io control STATUS function return values
#define ST_RX_DONE		0x00000001
#define ST_OVF			0x00000002
#define ST_RFS			0x00000004
#define ST_RX_TIMEOUT	0x00000008
#define ST_RSC			0x00000010
#define ST_PERR			0x00000020
#define ST_PCE			0x00000040
#define ST_FERR			0x00000080
#define ST_SYN			0x00000100
#define ST_DPLLA		0x00000200
#define ST_CDSC			0x00000400
#define ST_RFO			0x00000800
#define ST_EOP			0x00001000
#define ST_BRKD			0x00002000
#define ST_ONLP			0x00004000
#define ST_BRKT			0x00008000
#define	ST_ALLS			0x00010000
#define ST_EXE			0x00020000
#define ST_TIN			0x00040000
#define ST_CTSC			0x00080000
#define ST_XMR			0x00100000
#define ST_TX_DONE		0x00200000
#define ST_DMA_TC		0x00400000
#define ST_DSR1C		0x00800000
#define ST_DSR0C		0x01000000
#define ST_FUBAR_IRQ    0x02000000
//these are also somewhere else in a windows header file.
//
#define STATUS_DEVICE_DOES_NOT_EXIST     0xC00000C0L
#define STATUS_INVALID_PARAMETER         0xC000000DL
#define STATUS_NO_SUCH_DEVICE            0xC000000EL
#define STATUS_CANCELLED                 0xC0000120L

//timetag options
#define TIME_TAG_OFF 0
#define TIME_TAG_RFS 1
#define TIME_TAG_RME 2
#define TIME_TAG_SYN 3

//these are not really necessary unless you need to do something
//special, if so then they are here
//CMDR commands for the ESCC
#define XRES 1
#define XME 2
#define XIF 4
#define HUNT 4
#define XTF 8
#define STIB 16
#define RNR 32
#define RFRD 32
#define RHR 64
#define RMC 128
//STAR codes
#define WFA 1
#define CTS 2
#define CEC 4
#define RLI 8
#define TEC 8
#define RRNR 16
#define SYNC 16
#define FCS 16
#define XRNR 32
#define RFNE 32
#define XFW 64
#define XDOV 128
//ISR0
#define RPF 1
#define RFO 2
#define CDSC 4
#define PLLA 8
#define PCE 16
#define RSC 32
#define RFS 64
#define RME 128
//ISR1
#define XPR 1
#define XMR 2
#define CSC 4
#define TIN 8
#define EXE 16
#define XDU 16
#define ALLS 32
#define AOLP 32
#define RDO 64
#define OLP 64
#define EOP 128
//port offset locations
//hdlc defines
#define STAR 0x20
#define CMDR 0x20
#define RSTA 0x21
#define PRE  0x21
#define MODE 0x22
#define TIMR 0x23
#define XAD1 0x24
#define XAD2 0x25
#define RAH1 0x26
#define RAH2 0x27
#define RAL1 0x28
#define RHCR 0x29
#define RAL2 0x29
#define RBCL 0x2a
#define XBCL 0x2a
#define RBCH 0x2b
#define XBCH 0x2b
#define CCR0 0x2c
#define CCR1 0x2d
#define CCR2 0x2e
#define CCR3 0x2f
#define TSAX 0x30
#define TSAR 0x31
#define XCCR 0x32
#define RCCR 0x33
#define VSTR 0x34
#define BGR  0x34
#define RLCR 0x35
#define AML  0x36
#define AMH  0x37
#define GIS  0x38
#define IVA  0x38
#define IPC  0x39
#define ISR0 0x3a
#define IMR0 0x3a
#define ISR1 0x3b
#define IMR1 0x3b
#define PVR  0x3c
#define PIS  0x3d
#define PIM  0x3d
#define PCR  0x3e
#define CCR4 0x3f
#define FIFO 0x00
//async regs defines
#define XON  0x24
#define XOFF 0x25
#define TCR  0x26
#define DAFO 0x27
#define RFC 0x28
#define TIC 0x35
#define MXN 0x36
#define MXF 0x37
//bisync regs defines
#define SYNL 0x24
#define SYNH 0x25


//end of escc header file