#include "cesccp.h"
#include "conio.h"
#include "stdlib.h"
#include "stdio.h"
#include "bios.h"
#include "dos.h"
#include "string.h"
#include "graph.h"
void atest();
void btest();
void htest();
void cltest();
void ledtree();
void retest();
void ctstest();

void interrupt timer_isr();
extern unsigned delaybase;	
extern unsigned amccb;
unsigned long timertick = 0;
unsigned long tincount = 0;
unsigned long RTCcount = 0;
Cescc esccdrv;
unsigned bdrt;
unsigned long toterr;
void main(int argc, char *argv[])
{          
unsigned i;
bdrt = 0;


do{
_clearscreen(_GCLEARSCREEN);
printf("FASTCOM:ESCC LOOPTST -- LOOPBACK FUNCTIONAL TESTER\r\n");
toterr = 0;
printf("testing HDLC\r\n");
htest();
printf("testing async\r\n");
atest();
printf("testing bisync\r\n");
btest();         
printf("testing clock generator\r\n");
cltest();                                  
printf("receive echo test\r\n");
retest();     
printf("control line test\r\n");
ctstest();
printf("testing board functions\r\n");
ledtree();
if(toterr==0) printf("ESCC Passed test %c\r\n",7);
else printf("ESCC FAILED test \r\n");
printf("do another (y/n)?\r\n");
i = getch();
if((i=='n')||(i=='N'))
	{
	exit(0);
	}

}while(1);

}


void atest()
{
//FASTCOM ESCC ASYNC LOOPTEST

unsigned porta;
unsigned portb;

char buffer[4096];
char buffer2[4096];
struct escc_regs settings;
unsigned i,j,k,lp;
unsigned error;
unsigned long terr;
unsigned long tcnt;
unsigned ports[255];
unsigned maxport;
maxport = esccdrv.get_max_port();
esccdrv.get_user_port_list(ports);

porta = ports[0];
portb = ports[1];

terr  = 0;
tcnt = 0;


/*
esccdrv.set_clock_generator(porta,0x5d2c60,23);
printf("CLOCK = 2M\r\n");
getch();
//2M    0x5d2c60 (23 bits)
esccdrv.set_clock_generator(porta,0x5d2460,23);
printf("CLOCK = 4M\r\n");
getch();
//4M  0x5d2460 (23 bits)
esccdrv.set_clock_generator(porta,0xba1c60,24);
printf("CLOCK = 8M\r\n");
getch();
//8M  0xba1c60  (24 bits)
esccdrv.set_clock_generator(porta,0x5db2d8,23);
printf("CLOCK = 1.544M\r\n");
getch();
//1.544M  0x5db2d8 (23 bits)
esccdrv.set_clock_generator(porta,0x16a990,22);
printf("CLOCK = 2.048M\r\n");
getch();
//2.048M  0x16a990 (22 bits)
//use bitcalc to come up with others pay attention to how many bits are displayed in the
//stuffed bits field (to use in the -nb: field
*/
/*
_clearscreen(_GCLEARSCREEN);
printf("FASTCOM:ESCC LOOPTST -- LOOPBACK FUNCTIONAL TESTER\r\n");
printf("baudrate divisor:%u",bdrt);
printf("\r\n");                 
*/
printf("ASYNC\r\n");
//async settings

settings.mode = 0x08;
settings.timr = 0x1f;
settings.xbcl = 0x00;
settings.xbch = 0x00;
settings.ccr0 = 0x83;   //async
settings.ccr1 = 0x1f;
settings.ccr2 = 0x28;
settings.bgr = bdrt;//0x3b;     //9600
settings.iva = 0;
settings.ipc = 0x03;
settings.imr0 = 0x04;
settings.imr1 = 0x0;
settings.pvr = 0x0;
settings.pim = 0xff;
settings.pcr = 0xe0;
settings.tcr = 0x00;
settings.dafo = 0x00;   //N81
settings.rfc = 0x0c;    //1c stores parity/framing with data every other byte...

j = esccdrv.init_port(portb,OPMODE_ASYNC,&settings,10,10);
//if(j==TRUE)printf("Initializing port 1 OK\n\r");
if(j==FALSE)printf("Initializing port 1 FAILED\n\r");
j = esccdrv.init_port(porta,OPMODE_ASYNC,&settings,10,10);
//if(j==TRUE)printf("Initializing port 0 OK\n\r");
if(j==FALSE)printf("Initializing port 0 FAILED\n\r");
esccdrv.clear_rx_buffer(porta);
esccdrv.clear_rx_buffer(portb);
esccdrv.clear_tx_buffer(porta);
esccdrv.clear_tx_buffer(portb);

for(lp=0;lp<50;lp++)
{
error = 0;

//send a frame of data out port a
printf("\t\t\tTesting port 0\r");
for(j=0;j<1024;j++)buffer[j] = j;     
esccdrv.tx_port(porta,buffer,j);
//printf("sending %u bytes\r\n",j);
j = 0;
/*
while( (j&RX_READY) !=RX_READY )
		{
		j = esccdrv.get_port_status(porta);
		printf ("waiting...%4.4x\r",j);
		if(j!=0) 
			{                                                
			printf("\n\r");
			if((j&XMR_INTERRUPT)==XMR_INTERRUPT) printf("XMR\n\r");
			if((j&PCE_INTERRUPT)==PCE_INTERRUPT) printf("PCE\n\r");
			if((j&EXE_INTERRUPT)==EXE_INTERRUPT) printf("EXE\n\r");
			if((j&RFO_INTERRUPT)==RFO_INTERRUPT) printf("RFO\n\r");
			if((j&CTS_INTERRUPT)==CTS_INTERRUPT) printf("CTS\n\r");
			if((j&RFS_INTERRUPT)==RFS_INTERRUPT) printf("RFS\n\r");
			if((j&RX_BUFFER_OVERFLOW)==RX_BUFFER_OVERFLOW) printf("RX_BUFFER_OVERFLOW\n\r");
			if((j&RSC_INTERRUPT)==RSC_INTERRUPT) printf("RSC\n\r");
	    if((j&TIMER_INTERRUPT)==TIMER_INTERRUPT) printf("TIMER\n\r");
	    if((j&RX_READY)==RX_READY) printf("RX READY\n\r");
	    if((j&PARITY_ERROR)==PARITY_ERROR) printf("PARITY ERROR\n\r");
	    if((j&FRAMING_ERROR)==FRAMING_ERROR) printf("FRAMING ERROR\n\r");
	    if((j&BREAK_DETECTED)==BREAK_DETECTED) printf("BREAK DETECTED\n\r");
	    if((j&BREAK_TERMINATED)==BREAK_TERMINATED) printf("BREAK TERMINATED\n\r");
	    if((j&ALLSENT_INTERRUPT)==ALLSENT_INTERRUPT) printf("ALLSENT INTERRUPT\n\r");
			}
		if(kbhit()!=0) return;
		}
printf("\n\r");
*/

while((j = esccdrv.rx_port(porta,buffer2,4096))==0)printf("waiting...\r");
if(j==1024)
{
for(i=0;i<j;i++)
	{
//      printf("Out:%x  In:%x \r",buffer[i]&0xff,buffer2[i]&0xff);
	if((buffer2[i]&0xff)!=(buffer[i]&0xff))
		{
		//error
		printf("\r\nERROR in LOOPBACK[%x:%x,%x]\r\n",buffer2[i]&0xff,buffer[i]&0xff,i);
		error++;
		printf("\r\n"); 
		}
	}
}
else error++;


//printf("%u bytes received--%u bytes expected\n\r",j,1024);
//if(error==0) printf("Port 0 passed\r\n");
if(error>0) printf("Port 0 failed\r\n");
terr=terr+error;        
toterr = toterr + error;
printf("\t\t\tTesting port 1\r");
error = 0;
//send a frame of data out port b
for(j=0;j<1024;j++)buffer[j] = j;     
esccdrv.tx_port(portb,buffer,j);
//printf("sending %u bytes\r\n",j);
j = 0;
/*
while( (j&RX_READY) !=RX_READY )
		{
		j = esccdrv.get_port_status(portb);
		printf ("waiting...%4.4x\r",j);
		if(j!=0) 
			{                                                
			printf("\n\r");
			if((j&XMR_INTERRUPT)==XMR_INTERRUPT) printf("XMR\n\r");
			if((j&PCE_INTERRUPT)==PCE_INTERRUPT) printf("PCE\n\r");
			if((j&EXE_INTERRUPT)==EXE_INTERRUPT) printf("EXE\n\r");
			if((j&RFO_INTERRUPT)==RFO_INTERRUPT) printf("RFO\n\r");
			if((j&CTS_INTERRUPT)==CTS_INTERRUPT) printf("CTS\n\r");
			if((j&RFS_INTERRUPT)==RFS_INTERRUPT) printf("RFS\n\r");
			if((j&RX_BUFFER_OVERFLOW)==RX_BUFFER_OVERFLOW) printf("RX_BUFFER_OVERFLOW\n\r");
			if((j&RSC_INTERRUPT)==RSC_INTERRUPT) printf("RSC\n\r");
	    if((j&TIMER_INTERRUPT)==TIMER_INTERRUPT) printf("TIMER\n\r");
	    if((j&RX_READY)==RX_READY) printf("RX READY\n\r");
	    if((j&PARITY_ERROR)==PARITY_ERROR) printf("PARITY ERROR\n\r");
	    if((j&FRAMING_ERROR)==FRAMING_ERROR) printf("FRAMING ERROR\n\r");
	    if((j&BREAK_DETECTED)==BREAK_DETECTED) printf("BREAK DETECTED\n\r");
	    if((j&BREAK_TERMINATED)==BREAK_TERMINATED) printf("BREAK TERMINATED\n\r");
	    if((j&ALLSENT_INTERRUPT)==ALLSENT_INTERRUPT) printf("ALLSENT INTERRUPT\n\r");
			}
		if(kbhit()!=0) return;
		}
printf("\n\r");
*/

while((j = esccdrv.rx_port(portb,buffer2,4096))==0)printf("waiting...\r");
if(j==1024)
{
for(i=0;i<j;i++)
	{
//      printf("Out:%x  In:%x \r",buffer[i]&0xff,buffer2[i]&0xff);
	if((buffer2[i]&0xff)!=(buffer[i]&0xff))
		{
		//error
		printf("\r\nERROR in LOOPBACK[%x:%x,%x]\r\n",buffer2[i]&0xff,buffer[i]&0xff,i);
		error++;
printf("\r\n");     

		}
	}
}
else error++;   

//printf("%u bytes received--%u bytes expected\n\r",j,1024);
//if(error==0) printf("Port 1 passed\r\n");
if(error>0) printf("Port 1 failed\r\n");
terr = terr + error;
toterr = toterr + error;
tcnt++;
}

printf("Total Loops:%lu\r\n",tcnt);
printf("Total Errors:%lu\r\n",terr);


}

void btest()
{


unsigned porta;
unsigned portb;

char buffer[4096];
char buffer2[4096];
struct escc_regs settings;
unsigned i,j,k,lp;
unsigned error;
unsigned long terr;
unsigned long tcnt;
unsigned long xr;
unsigned ports[255];
unsigned maxport;
maxport = esccdrv.get_max_port();
esccdrv.get_user_port_list(ports);

porta = ports[0];
portb = ports[1];

terr  = 0;
tcnt = 0;

/*
esccdrv.set_clock_generator(porta,0x5d2c60,23);
printf("CLOCK = 2M\r\n");
getch();
//2M    0x5d2c60 (23 bits)
esccdrv.set_clock_generator(porta,0x5d2460,23);
printf("CLOCK = 4M\r\n");
getch();
//4M  0x5d2460 (23 bits)
esccdrv.set_clock_generator(porta,0xba1c60,24);
printf("CLOCK = 8M\r\n");
getch();
//8M  0xba1c60  (24 bits)
esccdrv.set_clock_generator(porta,0x5db2d8,23);
printf("CLOCK = 1.544M\r\n");
getch();
//1.544M  0x5db2d8 (23 bits)
esccdrv.set_clock_generator(porta,0x16a990,22);
printf("CLOCK = 2.048M\r\n");
getch();
//2.048M  0x16a990 (22 bits)
//use bitcalc to come up with others pay attention to how many bits are displayed in the
//stuffed bits field (to use in the -nb: field
*/

//if(bdrt==0) bdrt = 50;
/*
_clearscreen(_GCLEARSCREEN);
printf("FASTCOM:ESCC LOOPTST -- LOOPBACK FUNCTIONAL TESTER\r\n");
printf("baudrate divisor:%u",bdrt);
printf("\r\n");
*/
printf("BISYNC\r\n");
//bisync settings

settings.mode = 0x38;   //8bit bisync
settings.timr = 0x1f;
settings.synl = 0x00;   //first SYN char
settings.synh = 0x5e;   //second SYN char  
settings.tcr = 0xff;    //reset receive when line is idle (0xff)
settings.dafo = 0x00;   //no parity, 8 bit data
settings.rfc = 0x4d;    //don't save parity, don't store SYN char's 32 byte fifo trigger, TCD enabled
settings.ccr0 = 0x82;   //powerup, NRZ, BISYNC
settings.ccr1 = 0x16;   //txd push-pull (required), itf = 1's, clock mode 7 (baud rate)
settings.ccr2 = 0x38;   //output clock on txclk, use BGR output to generate clock
settings.ccr3 = 0x16;   //no preamble, crc on for all data written, reset crc to ffffh, append crc to end of tx, use CCITT polynom.
settings.bgr = bdrt;
settings.pre = 0x00;    //preamble (if enabled)
settings.iva = 0;
settings.ipc = 0x03;
settings.imr0 = 0x04;
settings.imr1 = 0x0;
settings.pvr = 0x0;
settings.pim = 0xff;
settings.pcr = 0xe0;
settings.xbch = 0x00;

j = esccdrv.init_port(porta,OPMODE_BISYNC,&settings,10,10);
//if(j==TRUE)printf("Initializing port 0 OK\n\r");
if(j==FALSE)printf("Initializing port 0 FAILED\n\r");
esccdrv.set_tx_type(porta,TRANSPARENT_MODE);

j = esccdrv.init_port(portb,OPMODE_BISYNC,&settings,10,10);
//if(j==TRUE)printf("Initializing port 1 OK\n\r");
if(j==FALSE)printf("Initializing port 1 FAILED\n\r");
esccdrv.set_tx_type(portb,TRANSPARENT_MODE);

esccdrv.clear_rx_buffer(porta);
esccdrv.clear_rx_buffer(portb);
esccdrv.clear_tx_buffer(porta);
esccdrv.clear_tx_buffer(portb);

for(lp=0;lp<50;lp++)
{
error = 0;
//send a frame of data out port a
printf("\t\t\tTesting port 0\r");
k = 0;
//0xff is the termination char, so send 0-254 in frame
for(i=0;i<4;i++)for(j=0;j<255;j++)buffer[k++] = j;     
esccdrv.tx_port(porta,buffer,k);
//printf("sending %u bytes\r\n",k);
j = 0;
/*
while( (j&RX_READY) !=RX_READY )
		{
		j = esccdrv.get_port_status(porta);
		printf ("waiting...%4.4x\r",j);
		if(j!=0) 
			{                                                
			printf("\n\r");
			if((j&XMR_INTERRUPT)==XMR_INTERRUPT) printf("XMR\n\r");
			if((j&PCE_INTERRUPT)==PCE_INTERRUPT) printf("PCE\n\r");
			if((j&EXE_INTERRUPT)==EXE_INTERRUPT) printf("EXE\n\r");
			if((j&RFO_INTERRUPT)==RFO_INTERRUPT) printf("RFO\n\r");
			if((j&CTS_INTERRUPT)==CTS_INTERRUPT) printf("CTS\n\r");
			if((j&RFS_INTERRUPT)==RFS_INTERRUPT) printf("RFS\n\r");
			if((j&RX_BUFFER_OVERFLOW)==RX_BUFFER_OVERFLOW) printf("RX_BUFFER_OVERFLOW\n\r");
			if((j&RSC_INTERRUPT)==RSC_INTERRUPT) printf("RSC\n\r");
	    if((j&TIMER_INTERRUPT)==TIMER_INTERRUPT) printf("TIMER\n\r");
	    if((j&RX_READY)==RX_READY) printf("RX READY\n\r");
	    if((j&PARITY_ERROR)==PARITY_ERROR) printf("PARITY ERROR\n\r");
	    if((j&SYN_DETECTED)==SYN_DETECTED) printf("SYN_DETECTED\n\r");
	    if((j&ALLSENT_INTERRUPT)==ALLSENT_INTERRUPT) printf("ALLSENT INTERRUPT\n\r");
			}
		if(kbhit()!=0) return;
		}
printf("\n\r");
*/

while((j = esccdrv.rx_port(porta,buffer2,4096))==0)printf("waiting..\r");
k = 0;
if(j==1023)
{
for(i=0;i<j-3;i++)//last 3 bytes are crc, and termin char
	{
//      printf("Out:%x  In:%x \r",buffer[i]&0xff,buffer2[k]&0xff);
	if((buffer2[i]&0xff)!=(buffer[k]&0xff))
		{
		//error
		printf("\r\nERROR in LOOPBACK[%x:%x,%x]\r\n",buffer2[i]&0xff,buffer[k]&0xff,i);
		error++;
		}
	k++;
	}
}
else error++;
//printf("\r\n");       

//printf("%u bytes received--%u bytes expected\n\r",j,1023);
//if(error==0) printf("Port 0 passed\r\n");
if(error>0) printf("Port 0 failed\r\n");
terr = terr +error;     
toterr = toterr + error;
printf("\t\t\tTesting port 1\r");
error = 0;
//send a frame of data out port b
k = 0;
for(i=0;i<4;i++)for(j=0;j<255;j++)buffer[k++] = j;     
esccdrv.tx_port(portb,buffer,k);
//printf("sending %u bytes\r\n",k);
j = 0;
/*
while( (j&RX_READY) !=RX_READY )
		{
		j = esccdrv.get_port_status(portb);
		printf ("waiting...%4.4x\r",j);
		if(j!=0) 
			{                                                
			printf("\n\r");
			if((j&XMR_INTERRUPT)==XMR_INTERRUPT) printf("XMR\n\r");
			if((j&PCE_INTERRUPT)==PCE_INTERRUPT) printf("PCE\n\r");
			if((j&EXE_INTERRUPT)==EXE_INTERRUPT) printf("EXE\n\r");
			if((j&RFO_INTERRUPT)==RFO_INTERRUPT) printf("RFO\n\r");
			if((j&CTS_INTERRUPT)==CTS_INTERRUPT) printf("CTS\n\r");
			if((j&RFS_INTERRUPT)==RFS_INTERRUPT) printf("RFS\n\r");
			if((j&RX_BUFFER_OVERFLOW)==RX_BUFFER_OVERFLOW) printf("RX_BUFFER_OVERFLOW\n\r");
			if((j&RSC_INTERRUPT)==RSC_INTERRUPT) printf("RSC\n\r");
	    if((j&TIMER_INTERRUPT)==TIMER_INTERRUPT) printf("TIMER\n\r");
	    if((j&RX_READY)==RX_READY) printf("RX READY\n\r");
	    if((j&PARITY_ERROR)==PARITY_ERROR) printf("PARITY ERROR\n\r");
	    if((j&SYN_DETECTED)==SYN_DETECTED) printf("SYN_DETECTED\n\r");
	    if((j&ALLSENT_INTERRUPT)==ALLSENT_INTERRUPT) printf("ALLSENT INTERRUPT\n\r");
			}
		if(kbhit()!=0) return;
		}
printf("\n\r");

*/
while((j = esccdrv.rx_port(portb,buffer2,4096))==0)printf("waiting..\r");;
k = 0;
if(j==1023)
{
for(i=0;i<j-3;i++)
	{
//      printf("Out:%x  In:%x \r",buffer[i]&0xff,buffer2[k]&0xff);
	if((buffer2[i]&0xff)!=(buffer[k]&0xff))
		{
		//error
		printf("\r\nERROR in LOOPBACK[%x:%x,%x]\r\n",buffer2[i]&0xff,buffer[k]&0xff,i);
		error++;
		}                          
	k++;            
	}
}
else error++;
	
//printf("\r\n");
//printf("%u bytes received--%u bytes expected\n\r",j,1023);
//if(error==0) printf("Port 1 passed\r\n");
if(error>0) printf("Port 1 failed\r\n");
terr = terr + error;
toterr = toterr + error;
tcnt++;
}
printf("Total Loops:%lu\r\n",tcnt);
printf("Total Errors:%lu\r\n",terr);

}




void htest()
{

unsigned porta;
unsigned portb;

//hdlc test
char buffer[4096];
char buffer2[4096];
struct escc_regs settings;
unsigned i,j,k,lp;
unsigned error;
unsigned long terr;
unsigned long tcnt;
unsigned ports[255];
unsigned maxport;
maxport = esccdrv.get_max_port();
esccdrv.get_user_port_list(ports);

porta = ports[0];
portb = ports[1];

terr  = 0;
tcnt = 0;



esccdrv.set_clock_generator_307(porta,0x100381);
//set to 18.432MHz

/*
esccdrv.set_clock_generator(porta,0x5d2c60,23);
printf("CLOCK = 2M\r\n");
getch();
//2M    0x5d2c60 (23 bits)
esccdrv.set_clock_generator(porta,0x5d2460,23);
printf("CLOCK = 4M\r\n");
getch();
//4M  0x5d2460 (23 bits)
esccdrv.set_clock_generator(porta,0xba1c60,24);
printf("CLOCK = 8M\r\n");
getch();
//8M  0xba1c60  (24 bits)
esccdrv.set_clock_generator(porta,0x5db2d8,23);
printf("CLOCK = 1.544M\r\n");
getch();
//1.544M  0x5db2d8 (23 bits)
esccdrv.set_clock_generator(porta,0x16a990,22);
printf("CLOCK = 2.048M\r\n");
getch();
//2.048M  0x16a990 (22 bits)
//use bitcalc to come up with others pay attention to how many bits are displayed in the
//stuffed bits field (to use in the -nb: field
*/
/*
_clearscreen(_GCLEARSCREEN);
printf("FASTCOM:ESCC LOOPTST -- LOOPBACK FUNCTIONAL TESTER\r\n");
printf("baudrate divisor:%u",bdrt);
printf("\r\n");
*/
printf("HDLC\r\n");

//HDLC settings

settings.mode = 0x88;
settings.timr = 0x1f;
settings.xbcl = 0x00;
settings.xbch = 0x00;
settings.ccr0 = 0x80;
settings.ccr1 = 0x16;
settings.ccr2 = 0x18;
settings.ccr3 = 0x00;
settings.bgr = bdrt;//0x1d;
settings.iva = 0;
settings.ipc = 0x03;
settings.imr0 = 0x04;
settings.imr1 = 0x0;
settings.pvr = 0x0;
settings.pim = 0xff;
settings.pcr = 0xe0;
settings.xad1 = 0xff;
settings.xad2 = 0xff;
settings.rah1 = 0xff;
settings.rah2 = 0xff;
settings.ral1 = 0xff;
settings.ral2 = 0xff;
settings.rlcr = 0x00;
settings.pre = 0x00;


j = esccdrv.init_port(portb,OPMODE_HDLC,&settings,10,10);
//if(j==TRUE)printf("Initializing port 1 OK\n\r");
if(j==FALSE)printf("Initializing port 1 FAILED\n\r");
esccdrv.set_tx_type(portb,TRANSPARENT_MODE);
j = esccdrv.init_port(porta,OPMODE_HDLC,&settings,10,10);
//if(j==TRUE)printf("Initializing port 0 OK\n\r");
if(j==FALSE)printf("Initializing port 0 FAILED\n\r");
esccdrv.set_tx_type(porta,TRANSPARENT_MODE);
esccdrv.clear_rx_buffer(porta);
esccdrv.clear_rx_buffer(portb);
esccdrv.clear_tx_buffer(porta);
esccdrv.clear_tx_buffer(portb);

for(lp=0;lp<250;lp++)
{

printf("\t\t\tTesting port 0\r");
error = 0;

//send a frame of data out port a
for(j=0;j<1024;j++)buffer[j] = j;     
j = esccdrv.tx_port(porta,buffer,j);
//printf("sending %u bytes\r\n",j);
j = 0;
/*
while( (j&RX_READY) !=RX_READY )
		{
		j = esccdrv.get_port_status(porta);
		printf ("waiting...%4.4x\r",j);
		if(j!=0) 
			{                                                
			printf("\n\r");
			if((j&XMR_INTERRUPT)==XMR_INTERRUPT) printf("XMR\n\r");
			if((j&PCE_INTERRUPT)==PCE_INTERRUPT) printf("PCE\n\r");
			if((j&EXE_INTERRUPT)==EXE_INTERRUPT) printf("EXE\n\r");
			if((j&RFO_INTERRUPT)==RFO_INTERRUPT) printf("RFO\n\r");
			if((j&CTS_INTERRUPT)==CTS_INTERRUPT) printf("CTS\n\r");
			if((j&RFS_INTERRUPT)==RFS_INTERRUPT) printf("RFS\n\r");
			if((j&RX_BUFFER_OVERFLOW)==RX_BUFFER_OVERFLOW) printf("RX_BUFFER_OVERFLOW\n\r");
			if((j&RSC_INTERRUPT)==RSC_INTERRUPT) printf("RSC\n\r");
	    if((j&TIMER_INTERRUPT)==TIMER_INTERRUPT) printf("TIMER\n\r");
	    if((j&RX_READY)==RX_READY) printf("RX READY\n\r");
	    if((j&PARITY_ERROR)==PARITY_ERROR) printf("PARITY ERROR\n\r");
	    if((j&FRAMING_ERROR)==FRAMING_ERROR) printf("FRAMING ERROR\n\r");
	    if((j&BREAK_DETECTED)==BREAK_DETECTED) printf("BREAK DETECTED\n\r");
	    if((j&BREAK_TERMINATED)==BREAK_TERMINATED) printf("BREAK TERMINATED\n\r");
	    if((j&ALLSENT_INTERRUPT)==ALLSENT_INTERRUPT) printf("ALLSENT INTERRUPT\n\r");
			}
		//if(kbhit()!=0) return;
		}
printf("\n\r");

*/
while((j = esccdrv.rx_port(porta,buffer2,4096))==0)printf("waiting...\r");
if(j==1025)
{
for(i=0;i<j-1;i++)
	{
	//printf("Out:%x  In:%x \r",buffer[i]&0xff,buffer2[i]&0xff);
	if((buffer2[i]&0xff)!=(buffer[i]&0xff))
		{
		//error
		printf("\r\nERROR in LOOPBACK[%x:%x,%x]\r\n",buffer2[i]&0xff,buffer[i]&0xff,i);
		error++;
		}
	}
}
else error++;


//printf("%u bytes received--%u bytes expected\n\r",j,1025);
//if(error==0) printf("Port 0 passed\r\n");
if(error>0) printf("Port 0 failed\r\n");
terr = terr + error;    
toterr = toterr + error;
printf("\t\t\tTesting port 1\r");
error = 0;
//send a frame of data out port b
for(j=0;j<1024;j++)buffer[j] = j;     
j = esccdrv.tx_port(portb,buffer,j);
//printf("sending %u bytes\r\n",j);
j = 0;
/*
while( (j&RX_READY) !=RX_READY )
		{
		j = esccdrv.get_port_status(portb);
		printf ("waiting...%4.4x\r",j);
		if(j!=0) 
			{                                                
			printf("\n\r");
			if((j&XMR_INTERRUPT)==XMR_INTERRUPT) printf("XMR\n\r");
			if((j&PCE_INTERRUPT)==PCE_INTERRUPT) printf("PCE\n\r");
			if((j&EXE_INTERRUPT)==EXE_INTERRUPT) printf("EXE\n\r");
			if((j&RFO_INTERRUPT)==RFO_INTERRUPT) printf("RFO\n\r");
			if((j&CTS_INTERRUPT)==CTS_INTERRUPT) printf("CTS\n\r");
			if((j&RFS_INTERRUPT)==RFS_INTERRUPT) printf("RFS\n\r");
			if((j&RX_BUFFER_OVERFLOW)==RX_BUFFER_OVERFLOW) printf("RX_BUFFER_OVERFLOW\n\r");
			if((j&RSC_INTERRUPT)==RSC_INTERRUPT) printf("RSC\n\r");
	    if((j&TIMER_INTERRUPT)==TIMER_INTERRUPT) printf("TIMER\n\r");
	    if((j&RX_READY)==RX_READY) printf("RX READY\n\r");
	    if((j&PARITY_ERROR)==PARITY_ERROR) printf("PARITY ERROR\n\r");
	    if((j&FRAMING_ERROR)==FRAMING_ERROR) printf("FRAMING ERROR\n\r");
	    if((j&BREAK_DETECTED)==BREAK_DETECTED) printf("BREAK DETECTED\n\r");
	    if((j&BREAK_TERMINATED)==BREAK_TERMINATED) printf("BREAK TERMINATED\n\r");
	    if((j&ALLSENT_INTERRUPT)==ALLSENT_INTERRUPT) printf("ALLSENT INTERRUPT\n\r");
			}
	//      if(kbhit()!=0) return;
		}
printf("\n\r");

*/
while((j = esccdrv.rx_port(portb,buffer2,4096))==0)printf("waiting...\r");
if(j==1025)
{
for(i=0;i<j-1;i++)
	{
	//printf("Out:%x  In:%x \r",buffer[i]&0xff,buffer2[i]&0xff);
	if((buffer2[i]&0xff)!=(buffer[i]&0xff))
		{
		//error
		printf("\r\nERROR in LOOPBACK[%x:%x,%x]\r\n",buffer2[i]&0xff,buffer[i]&0xff,i);
		error++;
		}
	}

}
else error++;   
//printf("%u bytes received--%u bytes expected\n\r",j,1025);
//if(error==0) printf("Port 1 passed\r\n");
if(error>0) printf("Port 1 failed\r\n");
terr = terr + error;
toterr = toterr + error;
tcnt++;
}
printf("Total Loops:%lu\r\n",tcnt);
printf("Total Errors:%lu\r\n",terr);

}

void cltest()
{

unsigned porta;
unsigned portb;
void (interrupt *oldvect1)();    //the old interrupt vector temp storage for timer

//hdlc test
char buffer[4096];
char buffer2[4096];
struct escc_regs settings;
unsigned i,j,k,lp;
unsigned error;
unsigned long terr;
unsigned long tcnt;
unsigned long timeout = 0;
unsigned ports[255];
unsigned maxport;
maxport = esccdrv.get_max_port();
esccdrv.get_user_port_list(ports);

porta = ports[0];
portb = ports[1];

terr  = 0;
tcnt = 0;
error = 0;

oldvect1 = _dos_getvect(0x70);                //get the Real Time Clock interrupt vector (IRQ 8)
_dos_setvect(0x70,timer_isr);           //set to our routine
i = _inp(0xa1);                     //Get 8259 PIC interrupt mask
i = i &0xfe;                                            //unmask the IRQ 8 interrupt
_outp(0xa1,i);                                          //update mask


//Set real time clock to interrupt 16 times per second
_outp(0x70,0x0a);       //set real time clock address
_outp(0x71,0x2c);       //set 16 Hz rate
_outp(0x70,0x0b);       //set real time clock address
_outp(0x71,inp(0x71)|0x40);     //enable RTC interrupt






/*
esccdrv.set_clock_generator(porta,0x5d2c60,23);
printf("CLOCK = 2M\r\n");
getch();
//2M    0x5d2c60 (23 bits)
esccdrv.set_clock_generator(porta,0x5d2460,23);
printf("CLOCK = 4M\r\n");
getch();
//4M  0x5d2460 (23 bits)
esccdrv.set_clock_generator(porta,0xba1c60,24);
printf("CLOCK = 8M\r\n");
getch();
//8M  0xba1c60  (24 bits)
esccdrv.set_clock_generator(porta,0x5db2d8,23);
printf("CLOCK = 1.544M\r\n");
getch();
//1.544M  0x5db2d8 (23 bits)
esccdrv.set_clock_generator(porta,0x16a990,22);
printf("CLOCK = 2.048M\r\n");
getch();
//2.048M  0x16a990 (22 bits)
//use bitcalc to come up with others pay attention to how many bits are displayed in the
//stuffed bits field (to use in the -nb: field
*/
/*
_clearscreen(_GCLEARSCREEN);
printf("FASTCOM:ESCC LOOPTST -- LOOPBACK FUNCTIONAL TESTER\r\n");
printf("baudrate divisor:%u",bdrt);
printf("\r\n");
*/
printf("CLOCK TEST\r\n");

//HDLC settings

settings.mode = 0x8a;
settings.timr = 0xFF;
settings.xbcl = 0x00;
settings.xbch = 0x00;
settings.ccr0 = 0x80;
settings.ccr1 = 0x17;
settings.ccr2 = 0x38;
settings.ccr3 = 0x00;
settings.bgr = bdrt;//0x1d;
settings.iva = 0;
settings.ipc = 0x03;
settings.imr0 = 0x04;
settings.imr1 = 0x0;
settings.pvr = 0x0;
settings.pim = 0xff;
settings.pcr = 0xe0;
settings.xad1 = 0xff;
settings.xad2 = 0xff;
settings.rah1 = 0xff;
settings.rah2 = 0xff;
settings.ral1 = 0xff;
settings.ral2 = 0xff;
settings.rlcr = 0x00;
settings.pre = 0x00;


j = esccdrv.init_port(portb,OPMODE_HDLC,&settings,10,10);
//if(j==TRUE)printf("Initializing port 1 OK\n\r");
if(j==FALSE)printf("Initializing port 1 FAILED\n\r");
esccdrv.set_tx_type(portb,TRANSPARENT_MODE);
j = esccdrv.init_port(porta,OPMODE_HDLC,&settings,10,10);
//if(j==TRUE)printf("Initializing port 0 OK\n\r");
if(j==FALSE)printf("Initializing port 0 FAILED\n\r");
esccdrv.set_tx_type(porta,TRANSPARENT_MODE);
//set to 1MHz                    
	esccdrv.set_clock_generator_307(porta,0x1025b1); //6Mhz
	outp(delaybase-8,2);//divide by 6
	

//printf("CLOCK = 1M\r\n");

timeout = 0;
timertick = 0;
esccdrv.start_timer(porta);                    
RTCcount = 0;

while(RTCcount < 8)printf("RT:%lu\r",RTCcount);
//printf("\r\n");
if((timertick>=5)||(timertick<=2))
	{
	//optimal timer value is 61 per second at 1MHz  cleared and updated 16 times per second which gives 3.8 ticks/update
	printf("timertick = %lu, should be in range 3-4\r\n",timertick);
	error++;
	}                                   
printf("Timertick:%lu\r\n",timertick);	
//set to 2MHz
esccdrv.stop_timer(porta);
	esccdrv.set_clock_generator_307(porta,0x123aae);	//12MHz

//printf("CLOCK = 2M\r\n");
timeout = 0;
timertick = 0;
esccdrv.start_timer(porta);                    
RTCcount = 0;
while(RTCcount < 8)printf("RT:%lu\r",RTCcount);
//printf("\r\n");
if((timertick>=9)||(timertick<=5))
	{
	//optimal timer value is 122 per second at 2MHz  cleared and updated 16 times per second which gives 7.6 ticks/update
	printf("timertick = %lu, should be in range 7-8\r\n",timertick);
	error++;              
	}
printf("Timertick:%lu\r\n",timertick);	
esccdrv.stop_timer(porta);
//set to 4M
esccdrv.set_clock_generator_307(porta,0x123ac6);//8MHz
	outp(delaybase-8,0);//divide by 2

//printf("CLOCK = 4M\r\n");
timeout = 0;
timertick = 0;
esccdrv.start_timer(porta);                    
RTCcount = 0;
while(RTCcount < 8)printf("RT:%lu\r",RTCcount);
//printf("\r\n");
if((timertick>=17)||(timertick<=13))
	{
	//optimal timer value is 244 per second at 2MHz  cleared and updated 16 times per second which gives 15.3 ticks/update
	printf("timertick = %lu, should be in range 15-16\r\n",timertick);
	error++;              
	}
printf("Timertick:%lu\r\n",timertick);	
esccdrv.stop_timer(porta);

//if(error==0) printf("Clock Test passed\r\n");
if(error>0) printf("Clock Test failed\r\n");
toterr = toterr + error;

_outp(0x70,0x0b);       //set real time clock address
_outp(0x71,inp(0x71)&0xBF);     //disable RTC interrupt
i = _inp(0xa1);                     //Get 8259 PIC interrupt mask
i = i | 0x01;                                            //mask the IRQ 8 interrupt
_outp(0xa1,i);                                          //update mask

_dos_setvect(0x70,oldvect1);           //set original routine

}


void ledtree()
{

unsigned porta;
unsigned portb;

//hdlc test
char buffer[4096];
char buffer2[4096];
struct escc_regs settings;
unsigned i,j,k,lp;
unsigned error;
unsigned long terr;
unsigned long tcnt;
unsigned ports[255];
unsigned maxport;
maxport = esccdrv.get_max_port();
esccdrv.get_user_port_list(ports);

porta = ports[0];
portb = ports[1];

terr  = 0;
tcnt = 0;


//set to 1MHz                    
	esccdrv.set_clock_generator_307(porta,0x1025b1); //6Mhz

//HDLC settings

settings.mode = 0x88;
settings.timr = 0x1f;
settings.xbcl = 0x00;
settings.xbch = 0x00;
settings.ccr0 = 0x80;
settings.ccr1 = 0x17;
settings.ccr2 = 0x38;
settings.ccr3 = 0x00;
settings.bgr = 0xC7;//0x63;
settings.iva = 0;
settings.ipc = 0x03;
settings.imr0 = 0x04;
settings.imr1 = 0x0;
settings.pvr = 0x0;
settings.pim = 0xff;
settings.pcr = 0xe0;
settings.xad1 = 0xff;
settings.xad2 = 0xff;
settings.rah1 = 0xff;
settings.rah2 = 0xff;
settings.ral1 = 0xff;
settings.ral2 = 0xff;
settings.rlcr = 0x00;
settings.pre = 0x00;


j = esccdrv.init_port(portb,OPMODE_HDLC,&settings,10,10);
//if(j==TRUE)printf("Initializing port 1 OK\n\r");
if(j==FALSE)printf("Initializing port 1 FAILED\n\r");
esccdrv.set_tx_type(portb,TRANSPARENT_MODE);
j = esccdrv.init_port(porta,OPMODE_HDLC,&settings,10,10);
//if(j==TRUE)printf("Initializing port 0 OK\n\r");
if(j==FALSE)printf("Initializing port 0 FAILED\n\r");
esccdrv.set_tx_type(porta,TRANSPARENT_MODE);
esccdrv.clear_rx_buffer(porta);
esccdrv.clear_rx_buffer(portb);
esccdrv.clear_tx_buffer(porta);
esccdrv.clear_tx_buffer(portb);

	printf("Insert LED tree, and press a key\r\n");
	getch();
	//set SD to 422 mode (ON)
	outpw(amccb,0x577);

	printf("port 1\r\n");
	printf("RED ON and YELLOW OFF?\r\n");
	getch();

	printf("YELLOW led blinked?\r\n");
	for(j=0;j<1024;j++)buffer[j] = 0;     
	j = esccdrv.tx_port(porta,buffer,938);//approx 500mS //250mS of blink
	getch();

	printf("Both LED's OFF\r\n");

//set SD to 485 mode (ON)
outpw(amccb,0x575);
	getch();

	printf("RED led blinked, then YELLOW led blinked, then both OFF?\r\n");

	for(j=0;j<281;j++)buffer[j] = 0xff;     
	for(j=281;j<1024;j++)buffer[j] = 0;
	j = esccdrv.tx_port(porta,buffer,938);//approx 500mS //250mS of blink
	getch();


//back to 422 mode
outpw(amccb,0x577);

	printf("port 2\r\n");
	printf("RED ON and YELLOW OFF?\r\n");
	getch();

	printf("YELLOW led blinked?\r\n");
	for(j=0;j<1024;j++)buffer[j] = 0;     
	j = esccdrv.tx_port(portb,buffer,934);
	getch();

	printf("Both LED's OFF\r\n");

//set SD to 485 mode (ON)
outpw(amccb,0x557);

	getch();
	printf("RED led blinked, then YELLOW led blinked, then both OFF?\r\n");
	for(j=0;j<281;j++)buffer[j] = 0xff;     
	for(j=281;j<1024;j++)buffer[j] = 0;
	j = esccdrv.tx_port(portb,buffer,938);//approx 500mS //250mS of blink
	getch();

//back to 422 mode
outpw(amccb,0x577);

	printf("port 1 clock\r\n");
	printf("Both leds ON\r\n");
	getch();
	
//set TT to 485 mode (ON)
outpw(amccb,0x573);
	
	printf("Both leds OFF\r\n");
	getch();
	printf("Both leds blinked?\r\n");
	for(j=0;j<1024;j++)buffer[j] = 0;     
	j = esccdrv.tx_port(porta,buffer,934);
	getch();

//set TT to 422 mode (ON)
outpw(amccb,0x577);
	
	printf("port 2 clock\r\n");
	printf("Both leds ON\r\n");
	getch();
//set TT to 485 mode (ON)
outpw(amccb,0x537);
	
	printf("Both leds OFF\r\n");
	getch();
	printf("Both leds blinked?\r\n");
	for(j=0;j<1024;j++)buffer[j] = 0;     
	j = esccdrv.tx_port(portb,buffer,934);
	getch();
//set TT to 422 mode (ON)
outpw(amccb,0x577);
	
	printf("Testing complete.\n");

}

void retest()
{

unsigned porta;
unsigned portb;

//hdlc test
char buffer[4096];
char buffer2[4096];
struct escc_regs settings;
unsigned i,j,k,lp;
unsigned error;
unsigned long terr;
unsigned long tcnt;
unsigned ports[255];
unsigned maxport;
maxport = esccdrv.get_max_port();
esccdrv.get_user_port_list(ports);

porta = ports[0];
portb = ports[1];

terr  = 0;
tcnt = 0;



esccdrv.set_clock_generator_307(porta,0x100381);
//set to 18.432MHz


//HDLC settings

settings.mode = 0x88;
settings.timr = 0x1f;
settings.xbcl = 0x00;
settings.xbch = 0x00;
settings.ccr0 = 0x80;
settings.ccr1 = 0x16;
settings.ccr2 = 0x18;
settings.ccr3 = 0x00;
settings.bgr = bdrt;//0x1d;
settings.iva = 0;
settings.ipc = 0x03;
settings.imr0 = 0x04;
settings.imr1 = 0x0;
settings.pvr = 0x0;
settings.pim = 0xff;
settings.pcr = 0xe0;
settings.xad1 = 0xff;
settings.xad2 = 0xff;
settings.rah1 = 0xff;
settings.rah2 = 0xff;
settings.ral1 = 0xff;
settings.ral2 = 0xff;
settings.rlcr = 0x00;
settings.pre = 0x00;


j = esccdrv.init_port(portb,OPMODE_HDLC,&settings,10,10);
//if(j==TRUE)printf("Initializing port 1 OK\n\r");
if(j==FALSE)printf("Initializing port 1 FAILED\n\r");
esccdrv.set_tx_type(portb,TRANSPARENT_MODE);
j = esccdrv.init_port(porta,OPMODE_HDLC,&settings,10,10);
//if(j==TRUE)printf("Initializing port 0 OK\n\r");
if(j==FALSE)printf("Initializing port 0 FAILED\n\r");
esccdrv.set_tx_type(porta,TRANSPARENT_MODE);
esccdrv.clear_rx_buffer(porta);
esccdrv.clear_rx_buffer(portb);
esccdrv.clear_tx_buffer(porta);
esccdrv.clear_tx_buffer(portb);
outpw(amccb,0x576);//turn on receive echo cancel (port0)


printf("Testing port 0");
error = 0;

//send a frame of data out port a
for(j=0;j<1024;j++)buffer[j] = j;     
j = esccdrv.tx_port(porta,buffer,16);
j = 0;
//wait for 10mS, and make sure we dind't receive it
for(i=0;i<40;i++)for(j=0;j<1000;j++)inpw(amccb);
j = esccdrv.rx_port(porta,buffer2,4096);
if(j!=0)
	{
	error++;
	}
if(error>0) printf("Port 0 failed\r\n");
else printf("...OK\r\n");
terr = terr + error;    
toterr = toterr + error;
outpw(amccb,0x567);//turn on receive echo cancel (port1)

printf("Testing port 1");
error = 0;                          

//send a frame of data out port b
for(j=0;j<1024;j++)buffer[j] = j;     
j = esccdrv.tx_port(portb,buffer,16);
j = 0;
j = esccdrv.rx_port(portb,buffer2,4096);
if(j!=0)
	{
	error++;
	}
if(error>0) printf("Port 1 failed\r\n");
else printf("...OK\r\n");
terr = terr + error;
toterr = toterr + error;
tcnt++;
outpw(amccb,0x577);//defaults

}

void ctstest()
{

unsigned porta;
unsigned portb;

//hdlc test
char buffer[4096];
char buffer2[4096];
struct escc_regs settings;
unsigned i,j,k,lp;
unsigned error;
unsigned long terr;
unsigned long tcnt;
unsigned ports[255];
unsigned maxport;
unsigned ctrl;
maxport = esccdrv.get_max_port();
esccdrv.get_user_port_list(ports);

porta = ports[0];
portb = ports[1];

terr  = 0;
tcnt = 0;



esccdrv.set_clock_generator_307(porta,0x100381);
//set to 18.432MHz


//HDLC settings

settings.mode = 0x88;
settings.timr = 0x1f;
settings.xbcl = 0x00;
settings.xbch = 0x00;
settings.ccr0 = 0x80;
settings.ccr1 = 0x16;
settings.ccr2 = 0x18;
settings.ccr3 = 0x00;
settings.bgr = bdrt;//0x1d;
settings.iva = 0;
settings.ipc = 0x03;
settings.imr0 = 0x04;
settings.imr1 = 0x0;
settings.pvr = 0x0;
settings.pim = 0xff;
settings.pcr = 0xe0;
settings.xad1 = 0xff;
settings.xad2 = 0xff;
settings.rah1 = 0xff;
settings.rah2 = 0xff;
settings.ral1 = 0xff;
settings.ral2 = 0xff;
settings.rlcr = 0x00;
settings.pre = 0x00;


j = esccdrv.init_port(portb,OPMODE_HDLC,&settings,10,10);
//if(j==TRUE)printf("Initializing port 1 OK\n\r");
if(j==FALSE)printf("Initializing port 1 FAILED\n\r");
esccdrv.set_tx_type(portb,TRANSPARENT_MODE);
j = esccdrv.init_port(porta,OPMODE_HDLC,&settings,10,10);
//if(j==TRUE)printf("Initializing port 0 OK\n\r");
if(j==FALSE)printf("Initializing port 0 FAILED\n\r");
esccdrv.set_tx_type(porta,TRANSPARENT_MODE);
esccdrv.clear_rx_buffer(porta);
esccdrv.clear_rx_buffer(portb);
esccdrv.clear_tx_buffer(porta);
esccdrv.clear_tx_buffer(portb);
outpw(amccb,0x577);//turn on receive echo cancel (port0)

error=0;
printf("Port0: CTS disabled....\tRTS LOW....");
esccdrv.set_control_lines(porta, 0, 0);//dtr rts
ctrl = esccdrv.get_control_lines(porta);
if((ctrl&2)!=2)
	{
	printf("ERROR\r\n");
 	error++;
 	}
else printf("OK\r\n");

printf("Port0: CTS disabled....\tRTS HIGH...");
esccdrv.set_control_lines(porta, 0, 1);//dtr rts
ctrl = esccdrv.get_control_lines(porta);
if((ctrl&2)!=2)
	{
	printf("ERROR\r\n");
 	error++;
 	}
else printf("OK\r\n");

outpw(amccb,0x57F);//turn off CTS disable (port0)

printf("Port0: CTS enabled....\tRTS LOW....");
esccdrv.set_control_lines(porta, 0, 0);//dtr rts
ctrl = esccdrv.get_control_lines(porta);
if((ctrl&2)!=0)
	{
	printf("ERROR\r\n");
 	error++;
 	}
else printf("OK\r\n");

printf("Port0: CTS enabled....\tRTS HIGH...");
esccdrv.set_control_lines(porta, 0, 1);//dtr rts
ctrl = esccdrv.get_control_lines(porta);
if((ctrl&2)!=2)
	{
	printf("ERROR\r\n");
 	error++;
 	}
else printf("OK\r\n");

outpw(amccb,0x577);//defaults


printf("Port1: CTS disabled....\tRTS LOW....");
esccdrv.set_control_lines(portb, 0, 0);//dtr rts
ctrl = esccdrv.get_control_lines(portb);
if((ctrl&2)!=2)
	{
	printf("ERROR\r\n");
 	error++;
 	}
else printf("OK\r\n");

printf("Port1: CTS disabled....\tRTS HIGH...");
esccdrv.set_control_lines(portb, 0, 1);//dtr rts
ctrl = esccdrv.get_control_lines(portb);
if((ctrl&2)!=2)
	{
	printf("ERROR\r\n");
 	error++;
 	}
else printf("OK\r\n");

outpw(amccb,0x5F7);//turn off CTS disable (port1)

printf("Port1: CTS enabled....\tRTS LOW....");
esccdrv.set_control_lines(portb, 0, 0);//dtr rts
ctrl = esccdrv.get_control_lines(portb);
if((ctrl&2)!=0)
	{
	printf("ERROR\r\n");
 	error++;
 	}
else printf("OK\r\n");

printf("Port1: CTS enabled....\tRTS HIGH...");
esccdrv.set_control_lines(portb, 0, 1);//dtr rts
ctrl = esccdrv.get_control_lines(portb);
if((ctrl&2)!=2)
	{
	printf("ERROR\r\n");
 	error++;
 	}
else printf("OK\r\n");

outpw(amccb,0x577);//defaults


//dtr/dsr loop check

printf("Port0: DSR-DTR........\tDTR LOW....");
esccdrv.set_control_lines(porta, 0, 0);//dtr rts
ctrl = esccdrv.get_control_lines(porta);
if((ctrl&0x10)!=0)
	{
	printf("ERROR\r\n");
 	error++;
 	}
else printf("OK\r\n");

printf("Port0: DSR-DTR........\tDTR HIGH...");
esccdrv.set_control_lines(porta, 1, 0);//dtr rts
ctrl = esccdrv.get_control_lines(porta);
if((ctrl&0x10)!=0x10)
	{
	printf("ERROR\r\n");
 	error++;
 	}
else printf("OK\r\n");

printf("Port1: DSR-DTR........\tDTR LOW....");
esccdrv.set_control_lines(portb, 0, 0);//dtr rts
ctrl = esccdrv.get_control_lines(portb);
if((ctrl&0x10)!=0)
	{
	printf("ERROR\r\n");
 	error++;
 	}
else printf("OK\r\n");

printf("Port1: DSR-DTR........\tDTR HIGH...");
esccdrv.set_control_lines(portb, 1, 0);//dtr rts
ctrl = esccdrv.get_control_lines(portb);
if((ctrl&0x10)!=0x10)
	{
	printf("ERROR\r\n");
 	error++;
 	}
else printf("OK\r\n");


//bit flags  X X X DSR DTR DCD CTS RTS
terr = terr + error;
toterr = toterr + error;

}


//timer interrupt service routine
void interrupt timer_isr()      
{
//if the source of the interrupt is the periodic interrupt then update the timertick variable
outp(0x70,0x0c);//set status c

if((inp(0x71)&0x40)==0x40)
	{
	timertick = tincount;
	tincount = 0;
	
	}
RTCcount++;     
//issue end of interrupt
_outp(0x20,0x20);//lower EOI
_outp(0xa0,0x20);//upper EOI
}                          

