#include "cesccp.h"           
#include "PCI.h"
#include "conio.h"
#include "stdio.h"
#include "stdlib.h"
#include "malloc.h"
#include "dos.h"
#include "bios.h"
#include "graph.h"

extern unsigned long tincount;
extern unsigned long inirq;

unsigned junk[50];
unsigned ist;
unsigned x1,x2;
Cescc *t1;	   		//this makes the instance of Chscx class object visible to the ISR

void banner();					

Cescc::Cescc()
{
//printf("in constructor\n\r");
unsigned i,j;
char mech;
unsigned interlevel;
char last_bus;                             
char bus_num;
char funct;
unsigned index;
unsigned long temp;   
unsigned long add1;
unsigned long add2;


t1 = this;
for(i=0;i<MAX_PORTS;i++)
	{
	port_list[i] = 0;
	port_open_list[i]	= 0;                 
	interrupt_list[i]	= 0;
	for(j=0;j<MAX_RBUFS;j++)rxbuffer[i][j] = 0;
	for(j=0;j<MAX_TBUFS;j++)txbuffer[i][j] = 0;
	timer_status[i] = 0;
	current_rxbuf[i] = 0;
	current_txbuf[i] = 0;
	max_rxbuf[i] = 0;
	max_txbuf[i] = 0;
	tx_type[i] = 0x08;
	istxing[i] = 0;	//==1 if a frame is being sent ,==0 if no txing is going on
	port_status[i] = 0;
	eopmode[i] = 0;
 	user_port_list[i] = 0;
 
	}
for(i=0;i<16;i++)
	{
	hooked_irqs[i] = 0;
	old_service_routines[i] = NULL;
	}
	next_port = 0;
	next_irq =0;
	upper_irq = 0;
	max_user_port = 0;

//here we find all escc-pci devices using bios routines...
banner();
if(pci_bios_present(&mech,&interlevel,&last_bus)==SUCCESSFUL)
	{
	printf("PCI BIOS DETECTED Version:%2.2x.%2.2x\r\n",interlevel>>8,interlevel&0xff);
	//if((mech&1)==1) printf("hardware mechanism #1 supported\r\n");
	//if((mech&2)==2) printf("hardware mechanism #2 supported\r\n");
	//printf("last bus #:%u\r\n",last_bus);
	}
else
	{
	printf("PCI BIOS NOT PRESENT\r\n");
	exit(1);
	}       

for(index=0;index<6;index++)
{
if(find_pci_device(0x0001,0x18F7,index,&bus_num,&funct)==SUCCESSFUL)
	{
	printf("Found ESCC-PCI-335--Bus#:%u  Device#:%x  Function:%x\r\n",bus_num,funct>>3,funct&0x07);
	//printf("device#:%x\r\n",funct>>3);
	//printf("function:#%x\r\n",funct&0x07);

	//read config area, show pert stuff
	read_configuration_dword(bus_num,funct,0x10,&add1);
    read_configuration_dword(bus_num,funct,0x14,&add2);
    //if((add2&1)==1) printf("base 2 in I/O space at %lx\r\n",add2&0xfffffffc);
    //if((add2&1)==0) printf("base 2 in MEM space at %lx\r\n",add2&0xfffffff0);
    read_configuration_dword(bus_num,funct,0x18,&temp);

	printf("Matchmaker regs at:%x,  ESCC regs at:%x & %lx\r\n",(unsigned)add1&0xfffc,(unsigned)add2&0xfffc,temp);
    read_configuration_dword(bus_num,funct,0x3c,&temp);
    printf("(IRQ)reg0x3c:%lx\r\n",temp);
//irq = (unsigned)(temp&0x00ff);
//esccport = (unsigned)(add2&0xfffc);
//pciregs = (unsigned)(add1&0xfffc);

user_port_list[max_user_port] = add_port((unsigned)(add2&0xfffc),(unsigned)(temp&0x00ff),(unsigned)(add1&0xfffc));    	//channel 0
max_user_port++;
user_port_list[max_user_port] = add_port((unsigned)(add2&0xfffc)+0x40,(unsigned)(temp&0x0ff),(unsigned)(add1&0xfffc));  //channel 1
max_user_port++;
//getch();
	}
//end of pci location code
}	

}

Cescc::~Cescc()
{
unsigned i,j;
//check if any open ports left if so then unhook unterrupts and kill them free any buffers etc
//printf("in destructor\n\r");
//walk the port list and turn off ints at the uarts
for(i=0;i<next_port;i++)
	{
	if(port_list[i]!=0)
	{
	_outp(port_list[i]+IMR0,0xff);//disable interrupts at escc!!!
	_outp(port_list[i]+IMR1,0xff);
	_outp(port_list[i]+PIM,0xff);
	//if the port is open we need to free all the allocated buffer spaces
	//ffree ignores NULL arguments so we should be safe freeing all of these pointers
	//as the ones that were allocated will not be null, all others should be NULL!!!
	for(j=0;j<max_rxbuf[i];j++)_ffree(rxbuffer[i][j]);
	for(j=0;j<max_txbuf[i];j++)_ffree(txbuffer[i][j]);
		
	}
	}
//walk the hooked IRQ list and replace the ISR's
for(i=0;i<next_irq;i++)
	{
	if(hooked_irqs[i]<8)
		{
		_dos_setvect(hooked_irqs[i]+8,old_service_routines[i]);
		//be nice and mask the IRQ at the PIC...could be bad I suppose... but it is nicer
			j = _inp(0x21);
			if(hooked_irqs[i] ==3) j = j|0x08;
			if(hooked_irqs[i] ==4) j = j|0x10;
			if(hooked_irqs[i] ==5) j = j|0x20;
			if(hooked_irqs[i] ==6) j = j|0x40;
			if(hooked_irqs[i] ==7) j = j|0x80;
			_outp(0x21,j);

		}
	if(hooked_irqs[i]>8)
		{    
		_dos_setvect(hooked_irqs[i]-8+0x70,old_service_routines[i]);
		j = _inp(0xa1);
		if(hooked_irqs[i] ==9)  j = j|0x02;
		if(hooked_irqs[i] ==10) j = j|0x04;
		if(hooked_irqs[i] ==11) j = j|0x08;
		if(hooked_irqs[i] ==12) j = j|0x10;
		if(hooked_irqs[i] ==15) j = j|0x80;
		_outp(0xa1,j);

		}	
	}

}
unsigned Cescc::get_max_port(void)
{
return (max_user_port);
}

void Cescc::get_user_port_list(unsigned *list)
{
unsigned i;
for(i=0;i<max_user_port;i++) list[i] = user_port_list[i];
}


unsigned Cescc::add_port(unsigned base,unsigned irq,unsigned amccbase)
{
unsigned i;
unsigned avail;
avail = next_port;
for(i=0;i<=next_port;i++)
	{
	if(port_list[i]==0)
		{
		avail = i;
		break;
		}
	}

port_list[avail] = base;
interrupt_list[avail] = irq;
amcc_port_list[avail] = amccbase;

//set up the ESCC port register so we can access both channels   
//note this must be the first access to the escc or we can't rely on talking to a specific
//channel (defaults to channel 1, or the upper channel when bit 0 is defined as an input)
_outp(base+PCR,0x60);	//bit 0 = SD				(output)
						//bit 1 = SC				(output)
						//bit 2 = not used			(output)
						//bit 3 = DTR channel 0		(output)
						//bit 4 = DTR channel 1		(output)
						//bit 5 = DSR channel 0		(input)
						//bit 6 = DSR channel 1		(input)
						//bit 7 = not used			(output)
_outp(base+IPC,0x03);	//int pin must be slave mode active high push pull to work.
_disable();
//_outp(amccbase+0x39,0x1f);//set amcc to int on incomming mail box #4 byte 3


//check current interrupts against ones that are hooked and hook new interrupt if necessary
for(i=0;i<16;i++)
	{
	if(hooked_irqs[i]==irq) goto skip_irqsetup;
	}                                          
//block interrupts;
//_asm cli;
_disable();
if(irq<8)
	{
	old_service_routines[next_irq] = _dos_getvect(irq+8);	//get old vector and save
	_dos_setvect(irq+8,escc_isr);		//put in our routine
	
	_outp(base+IMR0,0xff);//disable ints from uart
	_outp(base+IMR1,0xff);//disable ints from uart                                                  
	_outp(base+PIM,0xff);//disable ints from port
	i = _inp(0x21);
	if(irq ==3) i = i&0xf7;
	if(irq ==4) i = i&0xef;
	if(irq ==5) i = i&0xdf;
	if(irq ==6) i = i&0xbf;
	if(irq ==7) i = i&0x7f;
	_outp(0x21,i);
}
if(irq>8)
	{
	old_service_routines[next_irq] = _dos_getvect(irq+0x70 -8);
	_dos_setvect(irq+0x70-8,escc_isr);
	i = _inp(0xa1);
	if(irq ==9)         i = i&0xfd;
	if(irq ==10)        i = i&0xfb;
	if(irq ==11)        i = i&0xf7;
	if(irq ==12)        i = i&0xef;
	if(irq ==15)        i = i&0x7f;
	_outp(0xa1,i);
	upper_irq = 1;
	}
//_asm sti;
hooked_irqs[next_irq] = irq;
next_irq++;
//note we will wait until we get the init_port call before enabling the ints at the uart
//this will prevent bad things from happening like not having a buffer to put data in !!:)	
skip_irqsetup:
_enable();

if(avail==next_port) next_port++;
return (avail);
}
unsigned Cescc::kill_port(unsigned port)
{
//printf("in kill port\n\r");
//here we should set the port list stuff to 0, and unhook the irq

//walk the port list and check the irq to see if any other ports are using it, if not then 
//unhook the irq, and mask it at the pic
unsigned i;
//free the buffers for this port
for(i=0;i<max_rxbuf[port];i++)
	{
	_ffree(rxbuffer[port][i]);
	rxbuffer[port][i] = NULL;	//wipe them since they are now gone
	}
for(i=0;i<max_txbuf[port];i++)
	{
	_ffree(txbuffer[port][i]);
	txbuffer[port][i] = NULL;
	}
//we are done with the base address so kill the links

_outp(port_list[port]+IMR0,0xff); //turn off channel specific interrupts
_outp(port_list[port]+IMR1,0xff);
//should also turn off DSR/DTR interrupts here...since we will no longer have the port address
//after the next line and if it fires we will have a stuck int line.
//need Interrupt masks stored somewhere since we cannot read them back (write only)

port_list[port] = 0;
port_open_list[port] = 0;
port_dmar_list[port] = 0;
port_dmat_list[port] = 0;
interrupt_list[port] = 0;


if(port == (next_port-1)) next_port--; //if it was the last port then we can safely decrement the
									   //next_port specifier
return TRUE;
}

unsigned Cescc::init_port(	unsigned port,
							unsigned opmode,
							struct escc_regs *esccregs,
							unsigned rbufs,
							unsigned tbufs)
{
//set the port (from the list, make sure it is in the list...) to the settings given
//probably a good idea to verify all params before continuing
//set up the registers and reset the 82526 here
if(rbufs<2) return FALSE;
if(tbufs<2) return FALSE;
if(rbufs>MAX_RBUFS) return FALSE;
if(tbufs>MAX_TBUFS) return FALSE;
if((opmode!=OPMODE_HDLC)&&(opmode!=OPMODE_BISYNC)&&(opmode!=OPMODE_ASYNC)) return FALSE;
//allocate the memory for the buffers
unsigned i;
for(i=0;i<max_rxbuf[port];i++)
	{
	_ffree(rxbuffer[port][i]);//just in case this isn't the first call to here
	rxbuffer[port][i]=NULL;
	}
for(i=0;i<max_txbuf[port];i++)
	{
	_ffree(txbuffer[port][i]);//just in case this isn't the first call to here
	txbuffer[port][i]=NULL;
	}

//printf("sizeof(struct buf):%u\n\r",sizeof(struct buf));
//datasize = (unsigned long)rbufs * sizeof(struct buf);
for(i=0;i<rbufs;i++)
	{
	rxbuffer[port][i] = (struct buf far*)_fmalloc(sizeof(struct buf));
	if(rxbuffer[port][i]==NULL) return FALSE;
	}
for(i=0;i<tbufs;i++)
	{
	txbuffer[port][i] = (struct buf far *)_fmalloc(sizeof(struct buf));      
	if(txbuffer[port][i]==NULL) return FALSE;
	}

current_rxbuf[port] = 0;
current_txbuf[port] = 0;
max_rxbuf[port] = rbufs;
max_txbuf[port] = tbufs;

for(i=0;i<rbufs;i++)
		{
		//printf("rxbuf%u:%lp\r\n",i,rxbuffer[port][i]);
		rxbuffer[port][i]->valid = 0;
		rxbuffer[port][i]->no_bytes = 0;
		rxbuffer[port][i]->max = 0;
		}
for(i=0;i<tbufs;i++)
		{
		//printf("txbuf%u:%lp\r\n",i,txbuffer[port][i]);
		txbuffer[port][i]->valid = 0;
		txbuffer[port][i]->no_bytes = 0;
		txbuffer[port][i]->max = 0;
		}

//printf("#buf:%x\n\r",sizeof(struct buf));
//printf("rxbuffer:%lp\n\r",rxbuffer[port]);
//for(i=0;i<rbufs;i++)printf("rxbuffer[%u]:%lp\n\r",i,&rxbuffer[port][i]);

eopmode[port] = opmode;
if(opmode == OPMODE_ASYNC)
{
_outp(port_list[port]+MODE,esccregs->mode);
_outp(port_list[port]+TIMR,esccregs->timr);
_outp(port_list[port]+TCR,esccregs->tcr);
_outp(port_list[port]+DAFO,esccregs->dafo);
_outp(port_list[port]+RFC,esccregs->rfc);
_outp(port_list[port]+XBCL,esccregs->xbcl);
_outp(port_list[port]+XBCH,esccregs->xbch);
_outp(port_list[port]+CCR0,esccregs->ccr0);
_outp(port_list[port]+CCR1,esccregs->ccr1);
_outp(port_list[port]+CCR2,esccregs->ccr2);
_outp(port_list[port]+BGR,esccregs->bgr);
_outp(port_list[port]+PVR,(esccregs->pvr&0xfe)+channel[port]);

}
if(opmode==OPMODE_HDLC)
{
//this is mode specific to hdlc/sdlc...need to do various things for different modes

//copy the regset to the escc in question
//_outp(port_list[port]+register,esccregs->register);
_outp(port_list[port]+MODE,esccregs->mode);
_outp(port_list[port]+TIMR,esccregs->timr);
_outp(port_list[port]+XAD1,esccregs->xad1);
_outp(port_list[port]+XAD2,esccregs->xad2);
_outp(port_list[port]+RAH1,esccregs->rah1);
_outp(port_list[port]+RAH2,esccregs->rah2);
_outp(port_list[port]+RAL1,esccregs->ral1);
_outp(port_list[port]+RAL2,esccregs->ral2);
_outp(port_list[port]+XBCL,esccregs->xbcl);
_outp(port_list[port]+XBCH,esccregs->xbch);
_outp(port_list[port]+CCR0,esccregs->ccr0);
_outp(port_list[port]+CCR1,esccregs->ccr1);
_outp(port_list[port]+CCR2,esccregs->ccr2);
_outp(port_list[port]+CCR3,esccregs->ccr3);
_outp(port_list[port]+BGR,esccregs->bgr);
_outp(port_list[port]+RLCR,esccregs->rlcr);
_outp(port_list[port]+PRE,esccregs->pre);
_outp(port_list[port]+PVR,(esccregs->pvr&0xfe)+channel[port]);



}

if(opmode == OPMODE_BISYNC)
{
_outp(port_list[port]+MODE,esccregs->mode);
_outp(port_list[port]+TIMR,esccregs->timr);
_outp(port_list[port]+XAD1,esccregs->synl);
_outp(port_list[port]+XAD2,esccregs->synh);
_outp(port_list[port]+RAH1,esccregs->tcr);
_outp(port_list[port]+RAH2,esccregs->dafo);
_outp(port_list[port]+RAL1,esccregs->rfc);
_outp(port_list[port]+XBCL,esccregs->xbcl);
_outp(port_list[port]+XBCH,esccregs->xbch);
_outp(port_list[port]+CCR0,esccregs->ccr0);
_outp(port_list[port]+CCR1,esccregs->ccr1);
_outp(port_list[port]+CCR2,esccregs->ccr2);
_outp(port_list[port]+CCR3,esccregs->ccr3);
_outp(port_list[port]+BGR,esccregs->bgr);
_outp(port_list[port]+PRE,esccregs->pre);
_outp(port_list[port]+PVR,(esccregs->pvr&0xfe)+channel[port]);

}
_outp(port_list[port]+CMDR,XRES);//reset transmit
unsigned long timeout_cntr = 0;

while((_inp(port_list[port]+STAR)&CEC)==CEC)
	{
	timeout_cntr++;
	if(timeout_cntr>1000000)
		{
		//bad error condition here must indicate that we are not ready for business
		_outp(port_list[port]+IMR0,0xff);
		_outp(port_list[port]+IMR1,0xff);
		return FALSE;
		}
	}//wait for CEC = 0//need a timeout loop here (only wait so long...now it could be infinite if no txclock _input to hscx)
_outp(port_list[port]+CMDR,RHR);
	timeout_cntr=0;
	while((_inp(port_list[port]+STAR)&CEC)==CEC)
		{
		timeout_cntr++;
		if(timeout_cntr>1000000)
			{
			//bad error condition here must indicate that we are not ready for business
			_outp(port_list[port]+IMR0,0xff);//shut off interrupts 
			_outp(port_list[port]+IMR1,0xff);
			return FALSE;
			}
		}//wait for CEC = 0//need a timeout loop here (only wait so long...now it could be infinite if no txclock _input to hscx)

port_open_list[port] = 1;
_outp(port_list[port]+IMR0,esccregs->imr0);//flame on
_outp(port_list[port]+IMR1,esccregs->imr1);
_outp(port_list[port]+PIM,esccregs->pim);
if(opmode==OPMODE_BISYNC)
	{
	while((_inp(port_list[port]+STAR)&CEC)==CEC);
	 _outp(port_list[port]+CMDR,HUNT);//start receive engine...search for SYN
	}
if(esccregs->cmdr !=0);//could write the command if we wanted to but we won't for now
return TRUE;
}					


unsigned Cescc::rx_port(unsigned port,char far *buf, unsigned num_bytes)
{

//do something like this
//return the number of bytes transfered to buf (one frame worth)
//retval = rxbuffer[port][next_rbuf]->no_bytes
//buf = rxbuffer[port][next_rbuf]->frame
//rxbuffer[port][next_rbuf]->valid = 0;
// next_rbuf++;
// if (next_rbuf > max_rbufs) next_rbuf =0 ;
unsigned i,j;

if((eopmode[port]==OPMODE_HDLC)||(eopmode[port]==OPMODE_BISYNC))
{

i = current_rxbuf[port];
i++;
if(i==max_rxbuf[port]) i = 0;

do
{
if(rxbuffer[port][i]->valid ==1)
	{
	//we got a frame so copy the ->frame to the buf and invalidate it
	if(rxbuffer[port][i]->no_bytes > num_bytes) return 0;
		for(j=0;j<rxbuffer[port][i]->no_bytes;j++)
			buf[j] = (rxbuffer[port][i]->frame[j]);//copy to user buffer
		rxbuffer[port][i]->valid = 0;//invalidate so it can be used again
		return j; //give back the # bytes copied
	}
i++;	//try the next one
if(i==max_rxbuf[port])i = 0;//wrap to 0 if at end of buffers
}while(i!=current_rxbuf[port]);
return 0;	//no received frames so no bytes xfred
}

if(eopmode[port]==OPMODE_ASYNC)
{

i = current_rxbuf[port];
i++;
if(i==max_rxbuf[port]) i = 0;

do
{
if(rxbuffer[port][i]->valid ==1)
	{
	//we got a frame so copy the ->frame to the buf and invalidate it 
	//always assume that async will store both byte and status 
	//such that data is allways multiples of 2
	if((rxbuffer[port][i]->no_bytes) > num_bytes) return 0;
		for(j=0;j<(rxbuffer[port][i]->no_bytes);j++)
			buf[j] = (rxbuffer[port][i]->frame[j]);//copy to user buffer
		rxbuffer[port][i]->valid = 0;//invalidate so it can be used again
		return j; //give back the # bytes copied
	}
i++;	//try the next one
if(i==max_rxbuf[port])i = 0;//wrap to 0 if at end of buffers
}while(i!=current_rxbuf[port]);
return 0;	//no received frames so no bytes xfred
}
return 0;
}
//returns # bytes transfered, 0 if fails


unsigned Cescc::tx_port(unsigned port,char far *buf, unsigned num_bytes)
{
unsigned far *bloc;
if(num_bytes==0) return 0;
if(num_bytes>4096) return 0;
unsigned i,j;
bloc = (unsigned far*)buf;
//all of this is operating mode specific,...need different cases for async, hdlc, bisync
i = current_txbuf[port];
do
{
if(txbuffer[port][i]->valid ==0)
{

if((istxing[port]==0)&&(num_bytes<=32))
	{
	//can complete the send here and now
	//printf("sending =<32\n\r");
	txbuffer[port][i]->valid = 0;
	for(j=0;j<num_bytes;j++)_outp(port_list[port]+FIFO,buf[j]);
while((_inp(port_list[port]+STAR)&CEC)==CEC);
	istxing[port] = 1;
	_outp(port_list[port]+CMDR,tx_type[port]+XME);

	return num_bytes;
	}


//fill the buf and send it
for(j=0;j<num_bytes;j++)txbuffer[port][i]->frame[j] = buf[j];
txbuffer[port][i]->valid = 1;
txbuffer[port][i]->max = num_bytes;
txbuffer[port][i]->no_bytes = 0;
//current_txbuf[port]++;
//if(current_txbuf[port]==max_txbuf[port]) current_txbuf[port] = 0;

if(istxing[port]==1) 
	{
	//printf("\r\ntxing queued\n\r");
	//printf("queued current:%u, in:%u \r\n",current_txbuf[port],i);
	return j;//return number of bytes xfred
	}
else
	{
	//printf("initiating transfer\n\r");
	//_outp(port_list[port]+CMDR,XRES); //will force a tx interrupt and send the frame
	if(num_bytes>32)
		{
		//printf("\r\nsending =32\n\r");
		//printf("sending current:%u, in:%u \r\n",current_txbuf[port],i);
		txbuffer[port][i]->no_bytes = 32;
		//while((_inp(port_list[port]+STAR)&XFW)!=XFW) printf("write block\r\n");
		for(j=0;j<16;j++)_outpw(port_list[port]+FIFO,bloc[j]);
		while((_inp(port_list[port]+STAR)&CEC)==CEC);
		istxing[port]=1;
		_outp(port_list[port]+CMDR,tx_type[port]);
		}
	return num_bytes; //return number of bytes xfrd
	}
}
i++;
if(i==max_txbuf[port]) i = 0;
}while(i!=current_txbuf[port]);
//if here then there are no txbufs avaialable so indicate no bytes xfred
return 0;
}
//returns # bytes transfered,0 if fails

unsigned Cescc::set_control_lines(unsigned port, unsigned dtr, unsigned rts)
{
if(port_open_list!=0)
{                                                                         
//fixup for no channel needed
if(dtr ==1) _outp(port_list[port]+PVR,_inp(port_list[port]+PVR)|(1<<(3+channel[port])));//set DTR
if(channel[port]==0) if(dtr ==0) _outp(port_list[port]+PVR,_inp(port_list[port]+PVR)&0xf7);//clear DTR
if(channel[port]==1) if(dtr ==0) _outp(port_list[port]+PVR,_inp(port_list[port]+PVR)&0xef);
if(rts ==1) _outp(port_list[port]+MODE,_inp(port_list[port]+MODE)|0x04);//set the rts bit in MODE
if(rts ==0) _outp(port_list[port]+MODE,_inp(port_list[port]+MODE)&0xfb);//clear the RTS bit in mode
return TRUE;
}
else return FALSE;
}


unsigned Cescc::get_control_lines(unsigned port)
{
unsigned cts,dsr,dcd,dtr,rts;
unsigned retval;
if(port_open_list!=0)
{
//needs modified for correct registers on escc
dsr = (_inp(port_list[port]+PVR)>>5+channel[port])&0x01;
dtr = (_inp(port_list[port]+PVR)>>3+channel[port])&1;
rts = (_inp(port_list[port]+MODE)>>2)&1;
dcd = (_inp(port_list[port]+VSTR)>>7)&1;
cts = (_inp(port_list[port]+STAR)>>1)&1;
retval = (dsr<<4)+(dtr<<3)+(dcd<<2)+(cts<<1)+rts;
return retval;	//return  = bit flags  X X X DSR DTR DCD CTS RTS
}
else return 0;
}


unsigned Cescc::get_port_status(unsigned port)
{
unsigned st;
if(port_open_list[port]!=0)
{
st = port_status[port];
port_status[port] = 0;
return st;
}
else return 0;
}


unsigned Cescc::clear_rx_buffer(unsigned port)
{
//set all rxbuffer[port][..]->valid = 0 here                             
//and reset the receiver on the 82526
unsigned i;
if(port_open_list[port] != 0)
{
if(rxbuffer[port]!=NULL)
	{
	for(i=0;i<max_rxbuf[port];i++) rxbuffer[port][i]->valid = 0;//wipe all received frames
	current_rxbuf[port] = 0;  
	port_status[port] = port_status[port]&0xfdff;//reset the rxready condition  
	while((_inp(port_list[port]+STAR)&CEC)==CEC);
	_outp(port_list[port]+CMDR,RHR);	//reset the HDLC receiver
	return TRUE;					//success 
	}
}
return FALSE;					//no rxbuffer pointer so port not open (failed)
}

unsigned Cescc::clear_tx_buffer(unsigned port)
{          
unsigned i;
//set all txbuffer[port][..]->valid = 0 here
//and reset the transmitter on the 82526
if(port_open_list[port] != 0)                                             
{
if(txbuffer[port]!=NULL)
	{          
    for(i=0;i<max_txbuf[port];i++) txbuffer[port][i]->valid = 0; //wipe all data frames to be sent
    istxing[port] = 0;	//not sending anymore
    current_txbuf[port] = 0;  
    while((_inp(port_list[port]+STAR)&CEC)==CEC);
    _outp(port_list[port]+CMDR,XRES);//reset the transmitter
	return TRUE;
	}
}
return FALSE; //port not open or buffer not allocated return false (failed)
}


unsigned Cescc::set_tx_type(unsigned port,unsigned type)
{
//quite specific to hdlc/sdlc
if(type==AUTO_MODE) tx_type[port]=0x04;
if(type==TRANSPARENT_MODE) tx_type[port] = 0x08;
return TRUE;
}


unsigned Cescc::set_tx_address(unsigned port,unsigned address)
{
//xad1 is high byte
//xad2 is low byte
if(port_open_list[port] != 0)
{
_outp(port_list[port]+XAD2,address&0xff);
_outp(port_list[port]+XAD1,address>>8);
return TRUE;
}
else return FALSE;
}


unsigned Cescc::set_rx_address1(unsigned port,unsigned address)
{                                                                         
if(port_open_list[port] != 0)
{
_outp(port_list[port]+RAL1,address&0xff);
_outp(port_list[port]+RAH1,address>>8);
return TRUE;
}
else return FALSE;
}


unsigned Cescc::set_rx_address2(unsigned port,unsigned address)
{                                                                         
if(port_open_list[port] != 0)
{
_outp(port_list[port]+RAL2,address&0xff);
_outp(port_list[port]+RAH2,address>>8);
return TRUE;
}
else return FALSE;
}


unsigned Cescc::start_timer(unsigned port)
{                                                                         
if(port_open_list[port] != 0)
{
timer_status[port] = 0;     
while((_inp(port_list[port]+STAR)&CEC)==CEC);
_outp(port_list[port]+CMDR,0x10);//send start timer command
return TRUE;
}
else return FALSE;
}                 

unsigned Cescc::stop_timer(unsigned port)                                 
{
if(port_open_list[port] != 0)
{
_outp(port_list[port]+TIMR,_inp(port_list[port]+TIMR));//writing the timr register stops the timer
return TRUE;
}
else return FALSE;
}                 


unsigned Cescc::is_timer_expired(unsigned port)
{
if(port_open_list[port] != 0)
{
if(timer_status[port]==1)
	{
	timer_status[port] =0;
	port_status[port] = port_status[port]&(~TIMER_INTERRUPT);
	return TRUE;
	}
else return FALSE;
}
return FALSE;
}


unsigned Cescc::wait_for_timer_expired(unsigned port)
{
unsigned long timer_timeout;
timer_timeout = 0;
if(port_open_list[port]!=0)
{
while(timer_status[port]==0)
	{
	timer_timeout++;
	if(timer_timeout>1000000)
		{
		return FALSE;
		}
	}
timer_status[port] = 0;
port_status[port] = port_status[port]&(~TIMER_INTERRUPT);
return TRUE;
}
else return FALSE;
}


void cdecl interrupt far Cescc::escc_isr(void)
{
//this needs complete rewrite for handling of different operating modes, and switching
//between channel 1 and channel 2 of escc...

//make this look something like a cross between the dos hscx.c and the vhscxd.asm code
unsigned isr0;
unsigned isr1;
unsigned pis;
unsigned i; 
unsigned j,k;
unsigned inthit;
unsigned far *bloc;

struct buf far *irxbuf;
struct buf far *itxbuf;

unsigned amccintstat;

_disable();
/*
for(i=0;i<t1->next_port;i++)
	{
	amccintstat = _inp(t1->amcc_port_list[i]+0x3a);
	//check interrupt asserted bit on amcc int status register
	if((amccintstat&0x80)==0x80)
	 {
	 _outp(t1->amcc_port_list[i]+0x3a,0x02);//reset PCI irq (from AMCC) (incomming mailbox #4 byte 3)
	 _inp(t1->amcc_port_list[i]+0x1f);//read MB4B3
	 }
		//we have a escc interrupt here so service it
	}
*/


isr0 = 0;
isr1 = 0;
pis = 0;
j = 0;

//do
//                        
startisr:
inthit = 0;
for(i=0;i<t1->next_port;i++)
	{
	if(t1->port_open_list[i]==0)
		{
		if(t1->port_list[i]!=0)
			{
			
			_outp(t1->port_list[i]+IMR0,0xff);//shut down this port it hasn't been turned on yet
			_outp(t1->port_list[i]+IMR1,0xff);
			goto nextpt;
			}
		}

	isr0 = _inp(t1->port_list[i]+ISR0);
	isr1 = _inp(t1->port_list[i]+ISR1);
	pis = _inp(t1->port_list[i]+PIS);
	
	
	if((isr0+isr1+pis)==0) goto nextpt;
	inthit = 1;


    //here we start doing the isr's
    
    if((isr0&0x80)==0x80)
    	{
		if(t1->eopmode[i]==OPMODE_HDLC)
		{
    	//RME interrupt frame complete
		//last part of a frame is received
		
		//this code is for interrupt mode only
		//get the number of bytes total received
		//then fill the rbuf frame with the rest of the bytes
		k = (unsigned)( (_inp(t1->port_list[i]+RBCH)&0x0f) <<8) +(unsigned)(_inp(t1->port_list[i]+RBCL)&0xff);
		irxbuf = (t1->rxbuffer[i][t1->current_rxbuf[i]]);     
		//this is done as a byte access so that odd frame lengths will be received correctly
		//the fifo can not be accessed both as word and byte in same pool, meaning 
		//if you start doing 16 bit xfrs you must do all 16 bit xfrs...
		//note it might be quicker to just do an extra 16 bit xfr as opposed to doing it byte wise
		//but that only stands a chance of working for rx, txing you must do byte transfers
		//as the number of writes to the fifo determines what gets sent out.
		for(j=irxbuf->no_bytes;j<k;j++) irxbuf->frame[j] = _inp(t1->port_list[i]+FIFO);
		while((_inp(t1->port_list[i]+STAR)&CEC)==CEC);
		_outp(t1->port_list[i]+CMDR,RMC);     //release fifo
		irxbuf->no_bytes = k; //number of bytes received in rbuf
		irxbuf->valid = 1;//validate rbuf
		
		t1->current_rxbuf[i]++;  
		//irxbuf is no longer valid
		if(t1->current_rxbuf[i]==t1->max_rxbuf[i]) t1->current_rxbuf[i]=0;                
		if(t1->rxbuffer[i][t1->current_rxbuf[i]]->valid ==1) 
			{
						   //	an error occured and the 
					       //   main program isn't done with 
					       //   the frame that is about
					       //   to be used for received data
			//set the receive buffers overflowed bit of the status word for this port here
			t1->port_status[i] = t1->port_status[i] | RX_BUFFER_OVERFLOW;
			}
		t1->rxbuffer[i][t1->current_rxbuf[i]]->no_bytes = 0;  //prep buf for use
		t1->rxbuffer[i][t1->current_rxbuf[i]]->valid = 0;    //invalidate it for use
        t1->port_status[i] = t1->port_status[i] | RX_READY;
		
        
        
		}
	    if(t1->eopmode[i]==OPMODE_ASYNC)
	    {
	    //TCD interrupt
//do check for dma..in which case we don't need to get any data here...just need to figure out
//how many bytes are in the buffer        
	
        k = _inp(t1->port_list[i]+RBCL);//get num bytes in fifo
        k = k&0x1f;
        if(k==0) k=32;
        
        //assumes that k is allways a multiple of 2 (status byte included mode)
        irxbuf = (t1->rxbuffer[i][t1->current_rxbuf[i]]);     
        //bloc = (unsigned far *)irxbuf->frame;
        for(j=irxbuf->no_bytes;j<irxbuf->no_bytes+k;j++)  irxbuf->frame[j] = _inp(t1->port_list[i]+FIFO);
        while((_inp(t1->port_list[i]+STAR)&CEC)==CEC);
        _outp(t1->port_list[i]+CMDR,RMC);     //release fifo
		//could move this stuff elsewhere (such that the buffers get more than 32 bytes in em)
		irxbuf->valid = 1;//validate rbuf
		irxbuf->no_bytes = irxbuf->no_bytes + k; //number of bytes received in rbuf
		t1->current_rxbuf[i]++;  
		//irxbuf is no longer valid
		if(t1->current_rxbuf[i]==t1->max_rxbuf[i]) t1->current_rxbuf[i]=0;                
		if(t1->rxbuffer[i][t1->current_rxbuf[i]]->valid ==1) 
			{
						   //	an error occured and the 
					       //   main program isn't done with 
					       //   the frame that is about
					       //   to be used for received data
			//set the receive buffers overflowed bit of the status word for this port here
			t1->port_status[i] = t1->port_status[i] | RX_BUFFER_OVERFLOW;
			}
		t1->rxbuffer[i][t1->current_rxbuf[i]]->no_bytes = 0;  //prep buf for use
		t1->rxbuffer[i][t1->current_rxbuf[i]]->valid = 0;    //invalidate it for use
        t1->port_status[i] = t1->port_status[i] | RX_READY;
        
		}                                                 
		
		if(t1->eopmode[i]==OPMODE_BISYNC)
	    {
	    //TCD interrupt
        
        k = _inp(t1->port_list[i]+RBCL);//get num bytes in fifo
        k = k&0x1f;
        if(k==0) k=32;
        
        //assumes that k is allways a multiple of 2 (status byte included mode)
        irxbuf = (t1->rxbuffer[i][t1->current_rxbuf[i]]);     
        //bloc = (unsigned far *)irxbuf->frame;
        for(j=irxbuf->no_bytes;j<irxbuf->no_bytes+k;j++)  irxbuf->frame[j] = _inp(t1->port_list[i]+FIFO);
        while((_inp(t1->port_list[i]+STAR)&CEC)==CEC);
        _outp(t1->port_list[i]+CMDR,RMC);     //release fifo
		//could move this stuff elsewhere (such that the buffers get more than 32 bytes in em)
		irxbuf->valid = 1;//validate rbuf
		irxbuf->no_bytes = irxbuf->no_bytes + k; //number of bytes received in rbuf
		t1->current_rxbuf[i]++;  
		//irxbuf is no longer valid
		if(t1->current_rxbuf[i]==t1->max_rxbuf[i]) t1->current_rxbuf[i]=0;                
		if(t1->rxbuffer[i][t1->current_rxbuf[i]]->valid ==1) 
			{
						   //	an error occured and the 
					       //   main program isn't done with 
					       //   the frame that is about
					       //   to be used for received data
			//set the receive buffers overflowed bit of the status word for this port here
			t1->port_status[i] = t1->port_status[i] | RX_BUFFER_OVERFLOW;
			}
		t1->rxbuffer[i][t1->current_rxbuf[i]]->no_bytes = 0;  //prep buf for use
		t1->rxbuffer[i][t1->current_rxbuf[i]]->valid = 0;    //invalidate it for use
        t1->port_status[i] = t1->port_status[i] | RX_READY;
		while((_inp(t1->port_list[i]+STAR)&CEC)==CEC);
		_outp(t1->port_list[i]+CMDR,HUNT);     //go back to hunt mode
		}                                                 
		
		
    	
    	}
    if((isr0&0x40)==0x40)
    	{
		if(t1->eopmode[i]==OPMODE_HDLC)
		{			
			//RFS interrupt time to start the coffee
			t1->port_status[i] = t1->port_status[i] + RFS_INTERRUPT;
        }
        if(t1->eopmode[i]==OPMODE_ASYNC)
        {
        //TIME out interrupt
        //I think this would be a good time to pull the data from the fifo, issuing a 
        //RFRD command (0x20) to force the fifo open, the book says that it will force
        //a TCD interrupt which will collect the data
        
        if((_inp(t1->port_list[i]+STAR)&0x20)==0x20)
        	{
        	while((_inp(t1->port_list[i]+STAR)&CEC)==CEC);
        	 _outp(t1->port_list[i]+CMDR,0x20);          
        	}
        else
        	{
        	//here would be a good time to close out the current rxbuf and
        	//start in on a new one...sending rxready...no more data is 
        	//currently coming in so pass what we have on.
        	irxbuf = (t1->rxbuffer[i][t1->current_rxbuf[i]]);     
        	irxbuf->valid = 1;//validate rbuf
			t1->current_rxbuf[i]++;  
			//irxbuf is no longer valid
			if(t1->current_rxbuf[i]==t1->max_rxbuf[i]) t1->current_rxbuf[i]=0;                
			if(t1->rxbuffer[i][t1->current_rxbuf[i]]->valid ==1) 
				{
						   //	an error occured and the 
					       //   main program isn't done with 
					       //   the frame that is about
					       //   to be used for received data
				//set the receive buffers overflowed bit of the status word for this port here
				t1->port_status[i] = t1->port_status[i] | RX_BUFFER_OVERFLOW;
				}
			t1->rxbuffer[i][t1->current_rxbuf[i]]->no_bytes = 0;  //prep buf for use
			t1->rxbuffer[i][t1->current_rxbuf[i]]->valid = 0;    //invalidate it for use
        	t1->port_status[i] = t1->port_status[i] | RX_READY;
            }
        t1->port_status[i] = t1->port_status[i] + REC_TIMEOUT;
        }                               
        //shouldn't happen in bisync mode
    	}
    if((isr0&0x20)==0x20)
    	{
		if(t1->eopmode[i]==OPMODE_HDLC)
		{			
		//RSC interrupt (either got a RR or a RNR from HDLC link in auto mode
		t1->port_status[i] = t1->port_status[i] | RSC_INTERRUPT;
        }
        if(t1->eopmode[i]==OPMODE_ASYNC)
        {
        //parrity error interrupt         
        t1->port_status[i] = t1->port_status[i] + PARITY_ERROR;
        }
    	if(t1->eopmode[i]==OPMODE_BISYNC)
        {
        //parrity error interrupt         
        t1->port_status[i] = t1->port_status[i] + PARITY_ERROR;
        }
    	}
    if((isr0&0x10)==0x10)
    	{
		if(t1->eopmode[i]==OPMODE_HDLC)
			{			
					
			//protocol error (auto mode only)            
			t1->port_status[i] = t1->port_status[i] + PCE_INTERRUPT;
            }
        
    	if(t1->eopmode[i]==OPMODE_ASYNC)
    		{
    		//framing error interrupt                         
    		t1->port_status[i] = t1->port_status[i] + FRAMING_ERROR;
    		} 
    	if(t1->eopmode[i]==OPMODE_BISYNC)
    		{
    		//SYN detected
    		t1->port_status[i] = t1->port_status[i] + SYN_DETECTED;
    		} 
    	}
    if((isr0&0x08)==0x08)
    	{
    	if(t1->eopmode[i]==OPMODE_HDLC)
    		{
    		//DPLL async interrupt (lost clock sync)
    		}
    	if(t1->eopmode[i]==OPMODE_ASYNC)
    		{
    		//DPLL async interrupt (lost clock sync)
    		}
    	if(t1->eopmode[i]==OPMODE_BISYNC)
    		{
    		//DPLL async interrupt (lost clock sync)
    		}
    	}
    if((isr0&CDSC)==CDSC)
    	{
    	if(t1->eopmode[i]==OPMODE_HDLC)
    		{
    		//carrier detect changed state
    		}
    	if(t1->eopmode[i]==OPMODE_ASYNC)
    		{
    		//carrier detect changed state
    		}
    	if(t1->eopmode[i]==OPMODE_BISYNC)
    		{
    		//carrier detect changed state
    		}
    	
    	}
    if((isr0&RFO)==RFO)
    	{
		if(t1->eopmode[i]==OPMODE_HDLC)
    		{
    		//RFO interrupt                              
			t1->port_status[i] = t1->port_status[i] + RFO_INTERRUPT; 
			while((_inp(t1->port_list[i]+STAR)&CEC)==CEC);
			_outp(t1->port_list[i]+CMDR,RHR);

    		}
    	if(t1->eopmode[i]==OPMODE_ASYNC)
    		{
    		//receive overflow                                      
    		t1->port_status[i] = t1->port_status[i] + RFO_INTERRUPT;
    		//must be reset here or will be locked forever
    		}
    	if(t1->eopmode[i]==OPMODE_BISYNC)
    		{
    		//receive overflow                                      
    		t1->port_status[i] = t1->port_status[i] + RFO_INTERRUPT;
    		//must be reset here or will be locked forever
    		}
    				
		
    	}
    if((isr0&RPF)==RPF)
    	{
    	if(t1->eopmode[i]==OPMODE_HDLC)
    	{                                                            
    	
    	//RPF interrupt 32 bytes ready
		//do this for all but DMA (we shouldn't get here in DMA mode)
			irxbuf = (t1->rxbuffer[i][t1->current_rxbuf[i]]);
        	bloc = (unsigned far *)irxbuf->frame;
        	for(j=(irxbuf->no_bytes>>1);j<(irxbuf->no_bytes>>1)+16;j++) bloc[j] = _inpw(t1->port_list[i]+FIFO);
			while((_inp(t1->port_list[i]+STAR)&CEC)==CEC);
			_outp(t1->port_list[i]+CMDR,RMC);     //release fifo
        	irxbuf->no_bytes += 32;
		}
		if(t1->eopmode[i]==OPMODE_ASYNC)
		{
		
		//DATA IS READY                                             
		
        k = _inp(t1->port_list[i]+RBCL);//get num bytes in fifo
        k = k&0x1f;
        if(k==0) k=32;
        //assumes that k is allways a multiple of 2 (status byte included mode)
        irxbuf = (t1->rxbuffer[i][t1->current_rxbuf[i]]);     
        bloc = (unsigned far *)irxbuf->frame;
        for(j=irxbuf->no_bytes>>1;j<(irxbuf->no_bytes+k)>>1;j++)  bloc[j] = _inpw(t1->port_list[i]+FIFO);
        while((_inp(t1->port_list[i]+STAR)&CEC)==CEC);
        _outp(t1->port_list[i]+CMDR,RMC);     //release fifo
		//could move this stuff elsewhere (such that the buffers get more than 32 bytes in em)
		irxbuf->no_bytes = irxbuf->no_bytes + k; //number of bytes received in rbuf
		if(irxbuf->no_bytes >= (FRAME_SIZE - 32))
		{
		irxbuf->valid = 1;//validate rbuf
		t1->current_rxbuf[i]++;  
		//irxbuf is no longer valid
		if(t1->current_rxbuf[i]==t1->max_rxbuf[i]) t1->current_rxbuf[i]=0;                
		if(t1->rxbuffer[i][t1->current_rxbuf[i]]->valid ==1) 
			{
						   //	an error occured and the 
					       //   main program isn't done with 
					       //   the frame that is about
					       //   to be used for received data
			//set the receive buffers overflowed bit of the status word for this port here
			t1->port_status[i] = t1->port_status[i] | RX_BUFFER_OVERFLOW;
			}
		t1->rxbuffer[i][t1->current_rxbuf[i]]->no_bytes = 0;  //prep buf for use
		t1->rxbuffer[i][t1->current_rxbuf[i]]->valid = 0;    //invalidate it for use
        t1->port_status[i] = t1->port_status[i] | RX_READY;
		}
		}    	
    	
    	if(t1->eopmode[i]==OPMODE_BISYNC)
		{
		
		//DATA IS READY                                             
		
        k = _inp(t1->port_list[i]+RBCL);//get num bytes in fifo
        k = k&0x1f;
        if(k==0) k=32;
        
        //assumes that k is allways a multiple of 2 (status byte included mode)
        //this will hold true unless the programmed threshold = 1 in non parity store mode
        //ie this code will not work for threshold level 1, in no parity store mode!!!!!
        irxbuf = (t1->rxbuffer[i][t1->current_rxbuf[i]]);     
        bloc = (unsigned far *)irxbuf->frame;
        for(j=irxbuf->no_bytes>>1;j<(irxbuf->no_bytes+k)>>1;j++)  bloc[j] = _inpw(t1->port_list[i]+FIFO);
        while((_inp(t1->port_list[i]+STAR)&CEC)==CEC);
        _outp(t1->port_list[i]+CMDR,RMC);     //release fifo
		//could move this stuff elsewhere (such that the buffers get more than 32 bytes in em)
		irxbuf->no_bytes = irxbuf->no_bytes + k; //number of bytes received in rbuf
		if(irxbuf->no_bytes >=( FRAME_SIZE - 32))
		{
		irxbuf->valid = 1;//validate rbuf
		t1->current_rxbuf[i]++;  
		//irxbuf is no longer valid
		if(t1->current_rxbuf[i]==t1->max_rxbuf[i]) t1->current_rxbuf[i]=0;                
		if(t1->rxbuffer[i][t1->current_rxbuf[i]]->valid ==1) 
			{
						   //	an error occured and the 
					       //   main program isn't done with 
					       //   the frame that is about
					       //   to be used for received data
			//set the receive buffers overflowed bit of the status word for this port here
			t1->port_status[i] = t1->port_status[i] | RX_BUFFER_OVERFLOW;
			}
		t1->rxbuffer[i][t1->current_rxbuf[i]]->no_bytes = 0;  //prep buf for use
		t1->rxbuffer[i][t1->current_rxbuf[i]]->valid = 0;    //invalidate it for use
        t1->port_status[i] = t1->port_status[i] | RX_READY;
		}
		}    	
    	
    	
    	}
    
    	
    if((isr1&0x80)==0x80)
    	{
    	if(t1->eopmode[i]==OPMODE_HDLC)
    		{
    		//end of poll
    		}            
		if(t1->eopmode[i]==OPMODE_ASYNC)
			{
			//break detect interrupt                                  
			t1->port_status[i] = t1->port_status[i] | BREAK_DETECTED;
			}    		
    	//no function in bisync
    	}
    if((isr1&0x40)==0x40)
    	{               
    	if(t1->eopmode[i]==OPMODE_HDLC)
    		{
    		//on loop interrupt
    		}
    	if(t1->eopmode[i]==OPMODE_ASYNC)
    		{                  
    		//break terminated interrupt
    		t1->port_status[i] = t1->port_status[i] | BREAK_TERMINATED;
    		}                  
        //no function in bisync
    	}
    if((isr1&0x20)==0x20)
    	{               
    	if(t1->eopmode[i]==OPMODE_HDLC)
    		{
    		//all sent interrupt (tx machine is empty)
    		
    		}                                         
    	if(t1->eopmode[i]==OPMODE_ASYNC)
    		{
    		//all characters are sent out the fifo and out the txd pin
    		t1->port_status[i] = t1->port_status[i] | ALLSENT_INTERRUPT;
    		}
    	if(t1->eopmode[i]==OPMODE_BISYNC)
    		{
    		//all characters are sent out the fifo and out the txd pin
    		t1->port_status[i] = t1->port_status[i] | ALLSENT_INTERRUPT;
    		}
    	}
    if((isr1&0x10)==0x10)
    	{
    	if(t1->eopmode[i]==OPMODE_HDLC)
    		{
    		//EXE interupt                               
			t1->port_status[i] = t1->port_status[i] + EXE_INTERRUPT;
			//	_outp(t1->port_list[i]+CMDR,XRES);
			//need something here to reset the dma channel if operating in Extended trasparent mode
			//as this is the normal transmit end message for that mode
            }
        //no function in async mode
        if(t1->eopmode[i]==OPMODE_BISYNC)
    		{
    		//EXE interupt                               
			t1->port_status[i] = t1->port_status[i] + EXE_INTERRUPT;
			}
        
    	}
    if((isr1&TIN)==TIN)
    	{
		if(t1->eopmode[i]==OPMODE_HDLC)
		{
		//Timer  interrupt
		t1->port_status[i] = t1->port_status[i] | TIMER_INTERRUPT;
		t1->timer_status[i] = 1;
		tincount++;
		//_outp(t1->port_list[i]+TIMR,_inp(t1->port_list[i]+TIMR));//stops timer		
        }
        if(t1->eopmode[i]==OPMODE_ASYNC)
        {
        //timer interrupt
        t1->port_status[i] = t1->port_status[i] | TIMER_INTERRUPT;
		t1->timer_status[i] = 1;
		
        }
        if(t1->eopmode[i]==OPMODE_BISYNC)
        {
        //timer interrupt
        t1->port_status[i] = t1->port_status[i] | TIMER_INTERRUPT;
		t1->timer_status[i] = 1;
		
        }
    	}
    if((isr1&CSC)==CSC)
    	{
		if(t1->eopmode[i]==OPMODE_HDLC)
		{			
			//cts status change                                    
			t1->port_status[i] = t1->port_status[i] + CTS_INTERRUPT;
        }
        if(t1->eopmode[i]==OPMODE_ASYNC)
        {
        //CTS changed state
        	t1->port_status[i] = t1->port_status[i] + CTS_INTERRUPT;
        
        }
        if(t1->eopmode[i]==OPMODE_BISYNC)
        {
        //CTS changed state
        	t1->port_status[i] = t1->port_status[i] + CTS_INTERRUPT;
        
        }
    	}
    if((isr1&0x02)==0x02)
    	{
		if(t1->eopmode[i]==OPMODE_HDLC)
		{    		
    		//XMR interrupt
			t1->port_status[i] = t1->port_status[i] + XMR_INTERRUPT;
			//could do an auto resend here ...possibly (indicates transmit message repeat)
		}
		//no function in async
		if(t1->eopmode[i]==OPMODE_BISYNC)
		{    		
    		//XMR interrupt
			t1->port_status[i] = t1->port_status[i] + XMR_INTERRUPT;
			//could do an auto resend here ...possibly (indicates transmit message repeat)
		}
    	}
    if((isr1&XPR)==XPR)
    	{
    	if((t1->eopmode[i]==OPMODE_HDLC)||(t1->eopmode[i]==OPMODE_BISYNC))
    	{
    	//XPR interrupt (time to send the bytes out)            
			//do interrupt xpr here  
	                             
			itxbuf = (t1->txbuffer[i][t1->current_txbuf[i]]);
            bloc = (unsigned far *)itxbuf->frame;
			if(itxbuf->valid==1)
				{
				if((itxbuf->max - itxbuf->no_bytes) > 32)
					{
					//do send 32
					for(j=(itxbuf->no_bytes)>>1;j<(itxbuf->no_bytes>>1)+16;j++)
						_outpw(t1->port_list[i]+FIFO,bloc[j]);	
					while((_inp(t1->port_list[i]+STAR)&CEC)==CEC);						
					_outp(t1->port_list[i]+CMDR,t1->tx_type[i]);
					itxbuf->no_bytes += 32;
					t1->istxing[i] = 1;
					goto donetxingnow;
					}
				else
					{
					//do send <=32 //sending as byte accesses such that odd bytes can go out
					for(j=itxbuf->no_bytes;j<itxbuf->max;j++)
						_outp(t1->port_list[i]+FIFO,itxbuf->frame[j]); 
					while((_inp(t1->port_list[i]+STAR)&CEC)==CEC);
					_outp(t1->port_list[i]+CMDR,t1->tx_type[i]+XME);
					itxbuf->no_bytes = itxbuf->max;
					itxbuf->valid = 0;
					t1->current_txbuf[i]++;
					if(t1->current_txbuf[i]==t1->max_txbuf[i]) t1->current_txbuf[i] = 0;
					//goto try_again_tx;
					}
				}
			else
				{
				t1->istxing[i] = 0; //no valid frames we are done txing
				}
			
			
		
    	}
    	if(t1->eopmode[i]==OPMODE_ASYNC)
    	{
    	//transmit data ready interrupt
    	
    		itxbuf = (t1->txbuffer[i][t1->current_txbuf[i]]);
            bloc = (unsigned far *)itxbuf->frame;
			if(itxbuf->valid==1)
				{
				if((itxbuf->max - itxbuf->no_bytes) > 32)
					{
					//do send 32
					for(j=(itxbuf->no_bytes)>>1;j<(itxbuf->no_bytes>>1)+16;j++)
						_outpw(t1->port_list[i]+FIFO,bloc[j]);
					while((_inp(t1->port_list[i]+STAR)&CEC)==CEC);	
					_outp(t1->port_list[i]+CMDR,0x08);
					itxbuf->no_bytes += 32;
					t1->istxing[i] = 1;
					}
				else
					{
					//do send <=32 //sending as byte accesses such that odd bytes can go out
					for(j=itxbuf->no_bytes;j<itxbuf->max;j++)
						_outp(t1->port_list[i]+FIFO,itxbuf->frame[j]);
					while((_inp(t1->port_list[i]+STAR)&CEC)==CEC);
					_outp(t1->port_list[i]+CMDR,0x08);
					itxbuf->no_bytes = itxbuf->max;
					itxbuf->valid = 0;
					t1->current_txbuf[i]++;
					if(t1->current_txbuf[i]==t1->max_txbuf[i]) t1->current_txbuf[i] = 0;
					//goto try_again_tx;
					}
				}
			else
				{
				t1->istxing[i] = 0; //no valid frames we are done txing
				}
		
    		
		
    	
    	}
    	}
donetxingnow:    	
    if((pis&0x80)==0x80)
    	{
    	//no function
    	}               
	if((pis&0x40)==0x40)
		{
		//DSR channel 1 changed
		}    	               
	if((pis&0x20)==0x20)
		{
		//DSR channel 0 changed
		}
				
	nextpt:
	;
	
	}
//while(inthit==1);
if(inthit==1) goto startisr;
//do eoi's and leave
_outp(0x20,0x20);
if(t1->upper_irq!=0) _outp(0xa0,0x20);
}

void Cescc::set_clock_generator(unsigned port, unsigned long hval,unsigned nmbits)
{
unsigned base;
unsigned curval;
unsigned long tempval;
unsigned i;

base = port_list[port];
curval = 0;
//bit 0 = data
//bit 1 = clock;
outp(base+0x3c,curval);

tempval = STARTWRD;
for(i=0;i<14;i++)
	{
	curval = 0;
	curval = (char)(tempval&0x1);	//set bit
	outp(base+0x3c,curval);
	curval = curval |0x02;		//force rising edge
	outp(base+0x3c,curval);		//clock in data
	curval = curval &0x01;		//force falling edge
	outp(base+0x3c,curval);		//set clock low
	tempval = tempval >> 1;		//get next bit
	}
	
tempval = hval;
for(i=0;i<nmbits;i++)
	{
	curval = 0;
	curval = (char)(tempval&0x1);	//set bit
	outp(base+0x3c,curval);
	curval = curval |0x02;		//force rising edge
	outp(base+0x3c,curval);		//clock in data
	curval = curval &0x01;		//force falling edge
	outp(base+0x3c,curval);		//set clock low
	tempval = tempval >> 1;		//get next bit
	}

tempval = MIDWRD;
for(i=0;i<14;i++)
	{
	curval = 0;
	curval = (char)(tempval&0x1);	//set bit
	outp(base+0x3c,curval);
	curval = curval |0x02;		//force rising edge
	outp(base+0x3c,curval);		//clock in data
	curval = curval &0x01;		//force falling edge
	outp(base+0x3c,curval);		//set clock low
	tempval = tempval >> 1;		//get next bit
	}
//pause for >10ms --should be replaced with a regulation pause routine
for(i=0;i<32000;i++)inp(0x20);

tempval = ENDWRD;
for(i=0;i<14;i++)
	{
	curval = 0;
	curval = (char)(tempval&0x1);	//set bit
	outp(base+0x3c,curval);
	curval = curval |0x02;		//force rising edge
	outp(base+0x3c,curval);		//clock in data
	curval = curval &0x01;		//force falling edge
	outp(base+0x3c,curval);		//set clock low
	tempval = tempval >> 1;		//get next bit
	}

}


void banner()
{
_clearscreen(_GCLEARSCREEN);
printf("ESCC PCI/MATCHMAKER\r\n");
printf("Copyright(C) 1997 Commtech, Inc.\r\n");
}

unsigned amccb;
unsigned delaybase;
void DELAY()
{
unsigned j;
for(j=0;j<200;j++)inp(delaybase);
}


void Cescc::set_clock_generator_307(unsigned port,unsigned long hval)
{
	unsigned long tempValue = 0;
	int i=0;
	unsigned char data=0;
	unsigned char savedval;
	unsigned base;
	base = port_list[port];
	
	savedval = inp(base+0x3c);
	delaybase = base+0x3c;
	amccb = amcc_port_list[port];
	
	outp(base+0x3c,(unsigned char)tempValue);

	tempValue = (hval & 0x00ffffff);
//	printf("tempValue = 0x%X\n",tempValue);


	for(i=0;i<24;i++)
	{
		//data bit set
		if ((tempValue & 0x800000)!=0) 
		{
		data = 1;
//			printf("1");
//		data <<=8;
		}
		else 
		{
			data = 0;
//			printf("0");
		}
		outp(base+0x3c,data);
		DELAY();

		//clock high, data still there
		data |= 0x02;
		outp(base+0x3c,data);
		DELAY();
		//clock low, data still there
		data &= 0x01;
		outp(base+0x3c,data);
		DELAY();

		tempValue<<=1;
	}
//	printf("\n");

	data = 0x4;		//strobe on
	outp(base+0x3c,data);
	DELAY();	
	data = 0x0000;		//all off
	outp(base+0x3c,data);
	DELAY();

	outp(base+0x3c,(unsigned char)(savedval&0xF8));	//force sdta,sclk,sstb to 0

}	//void set_clock_generator_307(unsigned long hval)
