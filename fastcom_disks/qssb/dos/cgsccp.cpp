#include "cgsccp.h"           
#include "amcc.h"
#include "conio.h"
#include "stdio.h"
#include "stdlib.h"
#include "malloc.h"
#include "dos.h"
#include "bios.h"
#include "graph.h"

extern unsigned long tincount;
extern unsigned long inirq;

unsigned junk[50];
unsigned ist;
unsigned x1,x2;
Cescc *t1;	   		//this makes the instance of Chscx class object visible to the ISR

void banner();					

Cescc::Cescc()
{
//printf("in constructor\n\r");
unsigned i,j;
char mech;
unsigned interlevel;
char last_bus;                             
char bus_num;
char funct;
unsigned index;
unsigned long temp;   
unsigned long add1;
unsigned long add2;
unsigned long add3;


t1 = this;
for(i=0;i<MAX_PORTS;i++)
	{
	port_list[i] = 0;
	port_open_list[i]	= 0;                 
	interrupt_list[i]	= 0;
	for(j=0;j<MAX_RBUFS;j++)rxbuffer[i][j] = 0;
	for(j=0;j<MAX_TBUFS;j++)txbuffer[i][j] = 0;
	timer_status[i] = 0;
	current_rxbuf[i] = 0;
	current_txbuf[i] = 0;
	max_rxbuf[i] = 0;
	max_txbuf[i] = 0;
	tx_type[i] = 0x08;
	istxing[i] = 0;	//==1 if a frame is being sent ,==0 if no txing is going on
	port_status[i] = 0;
	eopmode[i] = 0;
 	user_port_list[i] = 0;
 
	}
for(i=0;i<16;i++)
	{
	hooked_irqs[i] = 0;
	old_service_routines[i] = NULL;
	}
	next_port = 0;
	next_irq =0;
	upper_irq = 0;
	max_user_port = 0;

//here we find all escc-pci devices using bios routines...
banner();
if(pci_bios_present(&mech,&interlevel,&last_bus)==SUCCESSFUL)
	{
	printf("PCI BIOS DETECTED Version:%2.2x.%2.2x\r\n",interlevel>>8,interlevel&0xff);
	//if((mech&1)==1) printf("hardware mechanism #1 supported\r\n");
	//if((mech&2)==2) printf("hardware mechanism #2 supported\r\n");
	//printf("last bus #:%u\r\n",last_bus);
	}
else
	{
	printf("PCI BIOS NOT PRESENT\r\n");
	exit(1);
	}       

for(index=0;index<6;index++)
{
if(find_pci_device(0x0011,0x18f7,index,&bus_num,&funct)==SUCCESSFUL)
	{
	printf("Found QSSB--Bus#:%u  Device#:%x  Function:%x\r\n",bus_num,funct>>3,funct&0x07);
	//printf("device#:%x\r\n",funct>>3);
	//printf("function:#%x\r\n",funct&0x07);

	//read config area, show pert stuff
	read_configuration_dword(bus_num,funct,0x10,&add1);
    read_configuration_dword(bus_num,funct,0x14,&add2);
    //if((add2&1)==1) printf("base 2 in I/O space at %lx\r\n",add2&0xfffffffc);
    //if((add2&1)==0) printf("base 2 in MEM space at %lx\r\n",add2&0xfffffff0);
    read_configuration_dword(bus_num,funct,0x18,&add3);

	printf("Matchmaker regs at:%x,  GSCC regs at:%x & %x\r\n",(unsigned)add1&0xfffc,(unsigned)add2&0xfffc,(unsigned)add3&0xfffc);
    read_configuration_dword(bus_num,funct,0x3c,&temp);
    printf("(IRQ)reg0x3c:%lx\r\n",temp);
//irq = (unsigned)(temp&0x00ff);
//esccport = (unsigned)(add2&0xfffc);
//pciregs = (unsigned)(add1&0xfffc);


user_port_list[max_user_port] = add_port((unsigned)(add2&0xfffc),(unsigned)(temp&0x00ff),(unsigned)(add1&0xfffc));    	//channel 0
channel[max_user_port]=0;
max_user_port++;
user_port_list[max_user_port] = add_port((unsigned)(add2&0xfffc),(unsigned)(temp&0x0ff),(unsigned)(add1&0xfffc));  //channel 1
channel[max_user_port]=1;
max_user_port++;
add2+=0x0100;
user_port_list[max_user_port] = add_port((unsigned)(add2&0xfffc),(unsigned)(temp&0x00ff),(unsigned)(add1&0xfffc));    	//channel 0
channel[max_user_port]=0;
max_user_port++;
user_port_list[max_user_port] = add_port((unsigned)(add2&0xfffc),(unsigned)(temp&0x0ff),(unsigned)(add1&0xfffc));  //channel 1
channel[max_user_port]=1;
max_user_port++;

//getch();
	}
//end of pci location code
}	

}

Cescc::~Cescc()
{
unsigned i,j;
//check if any open ports left if so then unhook unterrupts and kill them free any buffers etc
//printf("in destructor\n\r");
//walk the port list and turn off ints at the uarts
for(i=0;i<next_port;i++)
	{
	if(port_list[i]!=0)
	{

_outp(port_list[i]+IMR0,(UCHAR)0xff);
_outp(port_list[i]+IMR1,(UCHAR)0xff);
_outp(port_list[i]+IMR2,(UCHAR)0xff);
_outp(port_list[i]+IMR0+0x50,(UCHAR)0xff);
_outp(port_list[i]+IMR1+0x50,(UCHAR)0xff);
_outp(port_list[i]+IMR2+0x50,(UCHAR)0xff);

	//if the port is open we need to free all the allocated buffer spaces
	//ffree ignores NULL arguments so we should be safe freeing all of these pointers
	//as the ones that were allocated will not be null, all others should be NULL!!!
	for(j=0;j<max_rxbuf[i];j++)_ffree(rxbuffer[i][j]);
	for(j=0;j<max_txbuf[i];j++)_ffree(txbuffer[i][j]);
		
	}
	}
//walk the hooked IRQ list and replace the ISR's
for(i=0;i<next_irq;i++)
	{
	if(hooked_irqs[i]<8)
		{
		_dos_setvect(hooked_irqs[i]+8,old_service_routines[i]);
		//be nice and mask the IRQ at the PIC...could be bad I suppose... but it is nicer
			j = _inp(0x21);
			if(hooked_irqs[i] ==3) j = j|0x08;
			if(hooked_irqs[i] ==4) j = j|0x10;
			if(hooked_irqs[i] ==5) j = j|0x20;
			if(hooked_irqs[i] ==6) j = j|0x40;
			if(hooked_irqs[i] ==7) j = j|0x80;
			_outp(0x21,j);

		}
	if(hooked_irqs[i]>8)
		{    
		_dos_setvect(hooked_irqs[i]-8+0x70,old_service_routines[i]);
		j = _inp(0xa1);
		if(hooked_irqs[i] ==9)  j = j|0x02;
		if(hooked_irqs[i] ==10) j = j|0x04;
		if(hooked_irqs[i] ==11) j = j|0x08;
		if(hooked_irqs[i] ==12) j = j|0x10;
		if(hooked_irqs[i] ==15) j = j|0x80;
		_outp(0xa1,j);

		}	
	}

}
unsigned Cescc::get_max_port(void)
{
return (max_user_port);
}

void Cescc::get_user_port_list(unsigned *list)
{
unsigned i;
for(i=0;i<max_user_port;i++) list[i] = user_port_list[i];
}


unsigned Cescc::add_port(unsigned base,unsigned irq,unsigned amccbase)
{
unsigned i;
unsigned avail;
avail = next_port;
for(i=0;i<=next_port;i++)
	{
	if(port_list[i]==0)
		{
		avail = i;
		break;
		}
	}

port_list[avail] = base;
interrupt_list[avail] = irq;
amcc_port_list[avail] = amccbase;

_outp(base+GMODE,(UCHAR)0x3a);//ipc0:1 both 1 (pushpull/high) osc powerdown and shaper disabled as we have an external clock driving XTAL1 not a crystal.
_outp(base+GPDIRL,(UCHAR)0x06);//bits 9&10 are inputs (DSR0&1) 8 is output (DTRB)
_outp(base+GPDIRH,(UCHAR)0xbf);//bit 6 is output (DTRA) bits 0,1,2 are inputs
_outp(base+GPIML,(UCHAR)0x7);
_outp(base+GPIMH,(UCHAR)0xff);
_outp(base+IMR0,(UCHAR)0xff);
_outp(base+IMR1,(UCHAR)0xff);
_outp(base+IMR2,(UCHAR)0xff);
_outp(base+IMR0+0x50,(UCHAR)0xff);
_outp(base+IMR1+0x50,(UCHAR)0xff);
_outp(base+IMR2+0x50,(UCHAR)0xff);

//block interrupts;
//_asm cli;
_disable();

//check current interrupts against ones that are hooked and hook new interrupt if necessary
for(i=0;i<16;i++)
	{
	if(hooked_irqs[i]==irq) goto skip_irqsetup;
	}                                          
if(irq<8)
	{
	old_service_routines[next_irq] = _dos_getvect(irq+8);	//get old vector and save
	_dos_setvect(irq+8,escc_isr);		//put in our routine
	
	i = _inp(0x21);
	if(irq ==3) i = i&0xf7;
	if(irq ==4) i = i&0xef;
	if(irq ==5) i = i&0xdf;
	if(irq ==6) i = i&0xbf;
	if(irq ==7) i = i&0x7f;
	_outp(0x21,i);
}
if(irq>8)
	{
	old_service_routines[next_irq] = _dos_getvect(irq+0x70 -8);
	_dos_setvect(irq+0x70-8,escc_isr);
	i = _inp(0xa1);
	if(irq ==9)         i = i&0xfd;
	if(irq ==10)        i = i&0xfb;
	if(irq ==11)        i = i&0xf7;
	if(irq ==12)        i = i&0xef;
	if(irq ==15)        i = i&0x7f;
	_outp(0xa1,i);
	upper_irq = 1;
	}
//_asm sti;
hooked_irqs[next_irq] = irq;
next_irq++;
//note we will wait until we get the init_port call before enabling the ints at the uart
//this will prevent bad things from happening like not having a buffer to put data in !!:)	
skip_irqsetup:
_enable();

if(avail==next_port) next_port++;
return (avail);
}
unsigned Cescc::kill_port(unsigned port)
{
//printf("in kill port\n\r");
//here we should set the port list stuff to 0, and unhook the irq

//walk the port list and check the irq to see if any other ports are using it, if not then 
//unhook the irq, and mask it at the pic
unsigned i;
//free the buffers for this port
for(i=0;i<max_rxbuf[port];i++)
	{
	_ffree(rxbuffer[port][i]);
	rxbuffer[port][i] = NULL;	//wipe them since they are now gone
	}
for(i=0;i<max_txbuf[port];i++)
	{
	_ffree(txbuffer[port][i]);
	txbuffer[port][i] = NULL;
	}
//we are done with the base address so kill the links

if(channel[port]==0)
{
_outp(port_list[port]+IMR0,(UCHAR)0xff);
_outp(port_list[port]+IMR1,(UCHAR)0xff);
_outp(port_list[port]+IMR2,(UCHAR)0xff);
}
else if(channel[port]==1)
{
_outp(port_list[port]+IMR0+0x50,(UCHAR)0xff);
_outp(port_list[port]+IMR1+0x50,(UCHAR)0xff);
_outp(port_list[port]+IMR2+0x50,(UCHAR)0xff);
}
//should also turn off DSR/DTR interrupts here...since we will no longer have the port address
//after the next line and if it fires we will have a stuck int line.
//need Interrupt masks stored somewhere since we cannot read them back (write only)

port_list[port] = 0;
port_open_list[port] = 0;
port_dmar_list[port] = 0;
port_dmat_list[port] = 0;
interrupt_list[port] = 0;


if(port == (next_port-1)) next_port--; //if it was the last port then we can safely decrement the
									   //next_port specifier
return TRUE;
}

unsigned Cescc::init_port(	unsigned port,
							unsigned opmode,
							struct serocco_setup *gsccsetup,
							unsigned rbufs,
							unsigned tbufs)
{                                                              
unsigned long timeout_cntr;
//set the port (from the list, make sure it is in the list...) to the settings given
//probably a good idea to verify all params before continuing
//set up the registers and reset the 82526 here
if(rbufs<2) return FALSE;
if(tbufs<2) return FALSE;
if(rbufs>MAX_RBUFS) return FALSE;
if(tbufs>MAX_TBUFS) return FALSE;
if((opmode!=OPMODE_HDLC)&&(opmode!=OPMODE_BISYNC)&&(opmode!=OPMODE_ASYNC)) return FALSE;
//allocate the memory for the buffers
unsigned i;
for(i=0;i<max_rxbuf[port];i++)
	{
	_ffree(rxbuffer[port][i]);//just in case this isn't the first call to here
	rxbuffer[port][i]=NULL;
	}
for(i=0;i<max_txbuf[port];i++)
	{
	_ffree(txbuffer[port][i]);//just in case this isn't the first call to here
	txbuffer[port][i]=NULL;
	}

//printf("sizeof(struct buf):%u\n\r",sizeof(struct buf));
//datasize = (unsigned long)rbufs * sizeof(struct buf);
for(i=0;i<rbufs;i++)
	{
	rxbuffer[port][i] = (struct buf far*)_fmalloc(sizeof(struct buf));
	if(rxbuffer[port][i]==NULL) return FALSE;
	}
for(i=0;i<tbufs;i++)
	{
	txbuffer[port][i] = (struct buf far *)_fmalloc(sizeof(struct buf));      
	if(txbuffer[port][i]==NULL) return FALSE;
	}

current_rxbuf[port] = 0;
current_txbuf[port] = 0;
max_rxbuf[port] = rbufs;
max_txbuf[port] = tbufs;

for(i=0;i<rbufs;i++)
		{
		//printf("rxbuf%u:%lp\r\n",i,rxbuffer[port][i]);
		rxbuffer[port][i]->valid = 0;
		rxbuffer[port][i]->no_bytes = 0;
		rxbuffer[port][i]->max = 0;
		}
for(i=0;i<tbufs;i++)
		{
		//printf("txbuf%u:%lp\r\n",i,txbuffer[port][i]);
		txbuffer[port][i]->valid = 0;
		txbuffer[port][i]->no_bytes = 0;
		txbuffer[port][i]->max = 0;
		}

//printf("#buf:%x\n\r",sizeof(struct buf));
//printf("rxbuffer:%lp\n\r",rxbuffer[port]);
//for(i=0;i<rbufs;i++)printf("rxbuffer[%u]:%lp\n\r",i,&rxbuffer[port][i]);


//printf("initializing\n");
eopmode[port] = opmode;

_outp(port_list[port]+IMR0+(channel[port]*0x50),(UCHAR)0xff);//shut off interrupts 
_outp(port_list[port]+IMR1+(channel[port]*0x50),(UCHAR)0xff);//shut off interrupts 
_outp(port_list[port]+IMR2+(channel[port]*0x50),(UCHAR)0xff);//shut off interrupts 

_outp(port_list[port]+CCR0L+(channel[port]*0x50), (UCHAR)(gsccsetup->ccr0l));
_outp(port_list[port]+CCR0H+(channel[port]*0x50), (UCHAR)(gsccsetup->ccr0h));
_outp(port_list[port]+CCR1L+(channel[port]*0x50), (UCHAR)(gsccsetup->ccr1l));
_outp(port_list[port]+CCR1H+(channel[port]*0x50), (UCHAR)(gsccsetup->ccr1h));
_outp(port_list[port]+CCR2L+(channel[port]*0x50), (UCHAR)(gsccsetup->ccr2l));
_outp(port_list[port]+CCR2H+(channel[port]*0x50), (UCHAR)(gsccsetup->ccr2h));
_outp(port_list[port]+CCR3L+(channel[port]*0x50), (UCHAR)(gsccsetup->ccr3l));
_outp(port_list[port]+CCR3H+(channel[port]*0x50), (UCHAR)(gsccsetup->ccr3h));
_outp(port_list[port]+PREAMB+(channel[port]*0x50), (UCHAR)(gsccsetup->preamb));
_outp(port_list[port]+TOLEN+(channel[port]*0x50),  (UCHAR)(gsccsetup->tolen));
_outp(port_list[port]+ACCM0+(channel[port]*0x50),  (UCHAR)(gsccsetup->accm0));
_outp(port_list[port]+ACCM1+(channel[port]*0x50),  (UCHAR)(gsccsetup->accm1));
_outp(port_list[port]+ACCM2+(channel[port]*0x50),  (UCHAR)(gsccsetup->accm2));
_outp(port_list[port]+ACCM3+(channel[port]*0x50),  (UCHAR)(gsccsetup->accm3));
_outp(port_list[port]+UDAC0+(channel[port]*0x50),  (UCHAR)(gsccsetup->udac0));
_outp(port_list[port]+UDAC1+(channel[port]*0x50),  (UCHAR)(gsccsetup->udac1));
_outp(port_list[port]+UDAC2+(channel[port]*0x50),  (UCHAR)(gsccsetup->udac2));
_outp(port_list[port]+UDAC3+(channel[port]*0x50),  (UCHAR)(gsccsetup->udac3));
_outp(port_list[port]+TTSA0+(channel[port]*0x50),  (UCHAR)(gsccsetup->ttsa0));
_outp(port_list[port]+TTSA1+(channel[port]*0x50),  (UCHAR)(gsccsetup->ttsa1));
_outp(port_list[port]+TTSA2+(channel[port]*0x50),  (UCHAR)(gsccsetup->ttsa2));
_outp(port_list[port]+TTSA3+(channel[port]*0x50),  (UCHAR)(gsccsetup->ttsa3));
_outp(port_list[port]+RTSA0+(channel[port]*0x50),  (UCHAR)(gsccsetup->rtsa0));
_outp(port_list[port]+RTSA1+(channel[port]*0x50),  (UCHAR)(gsccsetup->rtsa1));
_outp(port_list[port]+RTSA2+(channel[port]*0x50),  (UCHAR)(gsccsetup->rtsa2));
_outp(port_list[port]+RTSA3+(channel[port]*0x50),  (UCHAR)(gsccsetup->rtsa3));
_outp(port_list[port]+PCMTX0+(channel[port]*0x50), (UCHAR)(gsccsetup->pcmtx0));
_outp(port_list[port]+PCMTX1+(channel[port]*0x50), (UCHAR)(gsccsetup->pcmtx1));
_outp(port_list[port]+PCMTX2+(channel[port]*0x50), (UCHAR)(gsccsetup->pcmtx2));
_outp(port_list[port]+PCMTX3+(channel[port]*0x50), (UCHAR)(gsccsetup->pcmtx3));
_outp(port_list[port]+PCMRX0+(channel[port]*0x50), (UCHAR)(gsccsetup->pcmrx0));
_outp(port_list[port]+PCMRX1+(channel[port]*0x50), (UCHAR)(gsccsetup->pcmrx1));
_outp(port_list[port]+PCMRX2+(channel[port]*0x50), (UCHAR)(gsccsetup->pcmrx2));
_outp(port_list[port]+PCMRX3+(channel[port]*0x50), (UCHAR)(gsccsetup->pcmrx3));
_outp(port_list[port]+BRRL+(channel[port]*0x50),   (UCHAR)(gsccsetup->brrl));
_outp(port_list[port]+BRRH+(channel[port]*0x50),   (UCHAR)(gsccsetup->brrh));
_outp(port_list[port]+TIMR0+(channel[port]*0x50),  (UCHAR)(gsccsetup->timer0));
_outp(port_list[port]+TIMR1+(channel[port]*0x50),  (UCHAR)(gsccsetup->timer1));
_outp(port_list[port]+TIMR2+(channel[port]*0x50),  (UCHAR)(gsccsetup->timer2));
_outp(port_list[port]+TIMR3+(channel[port]*0x50),  (UCHAR)(gsccsetup->timer3));
_outp(port_list[port]+XAD1+(channel[port]*0x50),   (UCHAR)(gsccsetup->xad1));
_outp(port_list[port]+XAD2+(channel[port]*0x50),   (UCHAR)(gsccsetup->xad2));
_outp(port_list[port]+RAL1+(channel[port]*0x50),   (UCHAR)(gsccsetup->ral1));
_outp(port_list[port]+RAH1+(channel[port]*0x50),   (UCHAR)(gsccsetup->rah1));
_outp(port_list[port]+RAL2+(channel[port]*0x50),   (UCHAR)(gsccsetup->ral2));
_outp(port_list[port]+RAH2+(channel[port]*0x50),   (UCHAR)(gsccsetup->rah2));
_outp(port_list[port]+AMRAL1+(channel[port]*0x50), (UCHAR)(gsccsetup->amral1));
_outp(port_list[port]+AMRAH1+(channel[port]*0x50), (UCHAR)(gsccsetup->amrah1));
_outp(port_list[port]+AMRAL2+(channel[port]*0x50), (UCHAR)(gsccsetup->amral2));
_outp(port_list[port]+AMRAH2+(channel[port]*0x50), (UCHAR)(gsccsetup->amrah2));
_outp(port_list[port]+RLCRL+(channel[port]*0x50),  (UCHAR)(gsccsetup->rlcrl));
_outp(port_list[port]+RLCRH+(channel[port]*0x50),  (UCHAR)(gsccsetup->rlcrh));
_outp(port_list[port]+XON+(channel[port]*0x50),    (UCHAR)(gsccsetup->xon));
_outp(port_list[port]+XOFF+(channel[port]*0x50),   (UCHAR)(gsccsetup->xoff));
_outp(port_list[port]+MXON+(channel[port]*0x50),   (UCHAR)(gsccsetup->mxon));
_outp(port_list[port]+MXOFF+(channel[port]*0x50),  (UCHAR)(gsccsetup->mxoff));
_outp(port_list[port]+TCR+(channel[port]*0x50),    (UCHAR)(gsccsetup->tcr));
//_outp(TICR+(channel[port]*0x50),      (UCHAR)(gsccsetup->.ticr));
_outp(port_list[port]+SYNCL+(channel[port]*0x50),  (UCHAR)(gsccsetup->syncl));
_outp(port_list[port]+SYNCH+(channel[port]*0x50),  (UCHAR)(gsccsetup->synch));

//printf("pre xres\n");

			//add timeouts in these or possible lockup
//sync section reset with timeout (receive) then (transmit)
//SimplDrvKdPrint (("ESCCDRV.SYS: Starting XRES with timeout \n"));  

	_outp(port_list[port]+CMDRL+(channel[port]*0x50),(UCHAR)XRES);//reset transmit
	timeout_cntr=0;
	while( (_inp(port_list[port]+STARL+(channel[port]*0x50))&CEC)==CEC)
		{
		timeout_cntr++;
		if(timeout_cntr>1000000)
			{
			//failing on transmit reset
			return FALSE;
			}
		}//wait for CEC = 0//need a timeout loop here (only wait so long...now it could be infinite if no txclock input to ESCC)
//SimplDrvKdPrint (("ESCCDRV.SYS: Starting RHR with timeout \n"));  
//printf("pre rres\n");
	_outp(port_list[port]+CMDRH+(channel[port]*0x50),(UCHAR)RRES);//reset receive
	timeout_cntr=0;
	while((_inp(port_list[port]+STARL+(channel[port]*0x50))&CEC)==CEC)
		{
		timeout_cntr++;
		if(timeout_cntr>1000000)
			{
			return FALSE;
			}
		}//wait for CEC = 0//need a timeout loop here (only wait so long...now it could be infinite if no txclock input to ESCC)

port_open_list[port] = 1;
//printf("pre unmasked\n");


_outp(port_list[port]+IMR0+(channel[port]*0x50),(UCHAR)(gsccsetup->imr0));
_outp(port_list[port]+IMR1+(channel[port]*0x50),(UCHAR)(gsccsetup->imr1));
_outp(port_list[port]+IMR2+(channel[port]*0x50),(UCHAR)(gsccsetup->imr2));

//printf("unmasked\n");
//opmode = _inp(port_list[port]+CCR0H+(channel[port]*0x50))&0x03;

if(opmode==OPMODE_BISYNC)
	{
	_outp(port_list[port]+CMDRH+(channel[port]*0x50),(UCHAR)HUNT);//start receive engine...search for SYN
	//should wait for CEC done here....
	}
return TRUE;
}					


unsigned Cescc::rx_port(unsigned port,char far *buf, unsigned num_bytes)
{

//do something like this
//return the number of bytes transfered to buf (one frame worth)
//retval = rxbuffer[port][next_rbuf]->no_bytes
//buf = rxbuffer[port][next_rbuf]->frame
//rxbuffer[port][next_rbuf]->valid = 0;
// next_rbuf++;
// if (next_rbuf > max_rbufs) next_rbuf =0 ;
unsigned i,j;

if((eopmode[port]==OPMODE_HDLC)||(eopmode[port]==OPMODE_BISYNC))
{

i = current_rxbuf[port];
i++;
if(i==max_rxbuf[port]) i = 0;

do
{
if(rxbuffer[port][i]->valid ==1)
	{
	//we got a frame so copy the ->frame to the buf and invalidate it
	if(rxbuffer[port][i]->no_bytes > num_bytes) return 0;
		for(j=0;j<rxbuffer[port][i]->no_bytes;j++)
			buf[j] = (rxbuffer[port][i]->frame[j]);//copy to user buffer
		rxbuffer[port][i]->valid = 0;//invalidate so it can be used again
		return j; //give back the # bytes copied
	}
i++;	//try the next one
if(i==max_rxbuf[port])i = 0;//wrap to 0 if at end of buffers
}while(i!=current_rxbuf[port]);
return 0;	//no received frames so no bytes xfred
}

if(eopmode[port]==OPMODE_ASYNC)
{

i = current_rxbuf[port];
i++;
if(i==max_rxbuf[port]) i = 0;

do
{
if(rxbuffer[port][i]->valid ==1)
	{
	//we got a frame so copy the ->frame to the buf and invalidate it 
	//always assume that async will store both byte and status 
	//such that data is allways multiples of 2
	if((rxbuffer[port][i]->no_bytes) > num_bytes) return 0;
		for(j=0;j<(rxbuffer[port][i]->no_bytes);j++)
			buf[j] = (rxbuffer[port][i]->frame[j]);//copy to user buffer
		rxbuffer[port][i]->valid = 0;//invalidate so it can be used again
		return j; //give back the # bytes copied
	}
i++;	//try the next one
if(i==max_rxbuf[port])i = 0;//wrap to 0 if at end of buffers
}while(i!=current_rxbuf[port]);
return 0;	//no received frames so no bytes xfred
}
return 0;
}
//returns # bytes transfered, 0 if fails


unsigned Cescc::tx_port(unsigned port,char far *buf, unsigned num_bytes)
{
unsigned far *bloc;
if(num_bytes==0) return 0;
if(num_bytes>4096) return 0;
unsigned i,j;
bloc = (unsigned far*)buf;
//all of this is operating mode specific,...need different cases for async, hdlc, bisync
i = current_txbuf[port];
do
{
if(txbuffer[port][i]->valid ==0)
{

if((istxing[port]==0)&&(num_bytes<=32))
	{
	//can complete the send here and now
	//printf("sending =<32\n\r");
	txbuffer[port][i]->valid = 0;
	for(j=0;j<num_bytes;j++)_outp(port_list[port]+FIFO+(channel[port]*0x50),buf[j]);
while((_inp(port_list[port]+STARL+(channel[port]*0x50))&CEC)==CEC);
	istxing[port] = 1;
	if(eopmode[port]!=OPMODE_ASYNC)	_outp(port_list[port]+CMDRL+(channel[port]*0x50),tx_type[port]+XME);
	else 	_outp(port_list[port]+CMDRL+(channel[port]*0x50),tx_type[port]);

	return num_bytes;
	}


//fill the buf and send it
for(j=0;j<num_bytes;j++)txbuffer[port][i]->frame[j] = buf[j];
txbuffer[port][i]->valid = 1;
txbuffer[port][i]->max = num_bytes;
txbuffer[port][i]->no_bytes = 0;
//current_txbuf[port]++;
//if(current_txbuf[port]==max_txbuf[port]) current_txbuf[port] = 0;

if(istxing[port]==1) 
	{
	//printf("\r\ntxing queued\n\r");
	//printf("queued current:%u, in:%u \r\n",current_txbuf[port],i);
	return j;//return number of bytes xfred
	}
else
	{
	//printf("initiating transfer\n\r");
	//_outp(port_list[port]+CMDR,XRES); //will force a tx interrupt and send the frame
	if(num_bytes>32)
		{
		//printf("\r\nsending =32\n\r");
		//printf("sending current:%u, in:%u \r\n",current_txbuf[port],i);
		txbuffer[port][i]->no_bytes = 32;
		//while((_inp(port_list[port]+STAR)&XFW)!=XFW) printf("write block\r\n");
		for(j=0;j<16;j++)_outpw(port_list[port]+FIFO+(channel[port]*0x50),bloc[j]);
		while((_inp(port_list[port]+STARL+(channel[port]*0x50))&CEC)==CEC);
		istxing[port]=1;
		_outp(port_list[port]+CMDRL+(channel[port]*0x50),tx_type[port]);
		}
	return num_bytes; //return number of bytes xfrd
	}
}
i++;
if(i==max_txbuf[port]) i = 0;
}while(i!=current_txbuf[port]);
//if here then there are no txbufs avaialable so indicate no bytes xfred
return 0;
}
//returns # bytes transfered,0 if fails

unsigned Cescc::set_control_lines(unsigned port, unsigned dtr, unsigned rts)
{
/* needs converted
if(port_open_list!=0)
{                                                                         
//fixup for no channel needed
if(dtr ==1) _outp(port_list[port]+PVR,_inp(port_list[port]+PVR)|(1<<(3+channel[port])));//set DTR
if(channel[port]==0) if(dtr ==0) _outp(port_list[port]+PVR,_inp(port_list[port]+PVR)&0xf7);//clear DTR
if(channel[port]==1) if(dtr ==0) _outp(port_list[port]+PVR,_inp(port_list[port]+PVR)&0xef);
if(rts ==1) _outp(port_list[port]+MODE,_inp(port_list[port]+MODE)|0x04);//set the rts bit in MODE
if(rts ==0) _outp(port_list[port]+MODE,_inp(port_list[port]+MODE)&0xfb);//clear the RTS bit in mode
return TRUE;
}
else return FALSE;
*/
	return FALSE;
}


unsigned Cescc::get_control_lines(unsigned port)
{
unsigned cts,dsr,dcd,dtr,rts;
unsigned retval;
/* needs converted
if(port_open_list!=0)
{
//needs modified for correct registers on escc
dsr = (_inp(port_list[port]+PVR)>>5+channel[port])&0x01;
dtr = (_inp(port_list[port]+PVR)>>3+channel[port])&1;
rts = (_inp(port_list[port]+MODE)>>2)&1;
dcd = (_inp(port_list[port]+VSTR)>>7)&1;
cts = (_inp(port_list[port]+STAR)>>1)&1;
retval = (dsr<<4)+(dtr<<3)+(dcd<<2)+(cts<<1)+rts;
return retval;	//return  = bit flags  X X X DSR DTR DCD CTS RTS
}
else return 0;
*/
return 0;
}


unsigned long Cescc::get_port_status(unsigned port)
{
unsigned long st;
if(port_open_list[port]!=0)
{
st = port_status[port];
port_status[port] = 0;
return st;
}
else return 0;
}


unsigned Cescc::clear_rx_buffer(unsigned port)
{
//set all rxbuffer[port][..]->valid = 0 here                             
//and reset the receiver on the 82526
unsigned i;
if(port_open_list[port] != 0)
{
if(rxbuffer[port]!=NULL)
	{
	for(i=0;i<max_rxbuf[port];i++) rxbuffer[port][i]->valid = 0;//wipe all received frames
	current_rxbuf[port] = 0;  
	port_status[port] = port_status[port]&0xfdff;//reset the rxready condition  
	while((_inp(port_list[port]+STARL+(channel[port]*0x50))&CEC)==CEC);
	_outp(port_list[port]+CMDRH+(channel[port]*0x50),RRES);	//reset the HDLC receiver
	return TRUE;					//success 
	}
}
return FALSE;					//no rxbuffer pointer so port not open (failed)
}

unsigned Cescc::clear_tx_buffer(unsigned port)
{          
unsigned i;
//set all txbuffer[port][..]->valid = 0 here
//and reset the transmitter on the 82526
if(port_open_list[port] != 0)                                             
{
if(txbuffer[port]!=NULL)
	{          
    for(i=0;i<max_txbuf[port];i++) txbuffer[port][i]->valid = 0; //wipe all data frames to be sent
    istxing[port] = 0;	//not sending anymore
    current_txbuf[port] = 0;  
    while((_inp(port_list[port]+STARL+(channel[port]*0x50))&CEC)==CEC);
    _outp(port_list[port]+CMDRL+(channel[port]*0x50),XRES);//reset the transmitter
	return TRUE;
	}
}
return FALSE; //port not open or buffer not allocated return false (failed)
}


unsigned Cescc::set_tx_type(unsigned port,unsigned type)
{
//quite specific to hdlc/sdlc
if(type==AUTO_MODE) tx_type[port]=0x04;
if(type==TRANSPARENT_MODE) tx_type[port] = 0x08;
return TRUE;
}


unsigned Cescc::set_tx_address(unsigned port,unsigned address)
{
//xad1 is high byte
//xad2 is low byte
	/*
if(port_open_list[port] != 0)
{
_outp(port_list[port]+XAD2,address&0xff);
_outp(port_list[port]+XAD1,address>>8);
return TRUE;
}
else return FALSE;
*/
	return FALSE;
}


unsigned Cescc::set_rx_address1(unsigned port,unsigned address)
{
	/*
if(port_open_list[port] != 0)
{
_outp(port_list[port]+RAL1,address&0xff);
_outp(port_list[port]+RAH1,address>>8);
return TRUE;
}
else return FALSE;
*/
	return FALSE;
}


unsigned Cescc::set_rx_address2(unsigned port,unsigned address)
{                
	/*
if(port_open_list[port] != 0)
{
_outp(port_list[port]+RAL2,address&0xff);
_outp(port_list[port]+RAH2,address>>8);
return TRUE;
}
else return FALSE;
*/
	return FALSE;
}


unsigned Cescc::start_timer(unsigned port)
{                
	
if(port_open_list[port] != 0)
{
timer_status[port] = 0;     
while((_inp(port_list[port]+STARL+(channel[port]*0x50))&CEC)==CEC);
_outp(port_list[port]+CMDRL+(channel[port]*0x50),0x80);//send start timer command
return TRUE;
}
else return FALSE;

	return FALSE;
}                 

unsigned Cescc::stop_timer(unsigned port)                                 
{

if(port_open_list[port] != 0)
{
while((_inp(port_list[port]+STARL+(channel[port]*0x50))&CEC)==CEC);
_outp(port_list[port]+CMDRL+(channel[port]*0x50),0x40);//send stop timer command
return TRUE;
}
else return FALSE;

	return FALSE;
}                 


unsigned Cescc::is_timer_expired(unsigned port)
{
if(port_open_list[port] != 0)
{
if(timer_status[port]==1)
	{
	timer_status[port] =0;
	port_status[port] = port_status[port]&(~TIN_INTERRUPT);
	return TRUE;
	}
else return FALSE;
}
return FALSE;
}


unsigned Cescc::wait_for_timer_expired(unsigned port)
{
unsigned long timer_timeout;
timer_timeout = 0;
if(port_open_list[port]!=0)
{
while(timer_status[port]==0)
	{
	timer_timeout++;
	if(timer_timeout>1000000)
		{
		return FALSE;
		}
	}
timer_status[port] = 0;
port_status[port] = port_status[port]&(~TIN_INTERRUPT);
return TRUE;
}
else return FALSE;
}


void cdecl interrupt far Cescc::escc_isr(void)
{
//this needs complete rewrite for handling of different operating modes, and switching
//between channel 1 and channel 2 of escc...

//make this look something like a cross between the dos hscx.c and the vhscxd.asm code
unsigned isr0;
unsigned isr1;
unsigned isr2;
unsigned pis;
unsigned i; 
unsigned j,k;
unsigned inthit;
unsigned far *bloc;

struct buf far *irxbuf;
struct buf far *itxbuf;

unsigned amccintstat;

//_disable();

isr0 = 0;
isr1 = 0;
pis = 0;
j = 0;

//do
//                        
startisr:
inthit = 0;
for(i=0;i<t1->next_port;i++)
	{
	if(t1->port_open_list[i]==0)
		{
		if(t1->port_list[i]!=0)
			{
			_outp(t1->port_list[i]+IMR0+(t1->channel[i]*0x50),(UCHAR)0xff);//shut off interrupts 
			_outp(t1->port_list[i]+IMR1+(t1->channel[i]*0x50),(UCHAR)0xff);//shut off interrupts 
			_outp(t1->port_list[i]+IMR2+(t1->channel[i]*0x50),(UCHAR)0xff);//shut off interrupts 
			goto nextpt;
			}
		}

isr0 = _inp(t1->port_list[i]+ISR0+(t1->channel[i]*0x50));//get irq status
isr1 = _inp(t1->port_list[i]+ISR1+(t1->channel[i]*0x50));//get irq status
isr2 = _inp(t1->port_list[i]+ISR2+(t1->channel[i]*0x50));//get irq status
pis  = _inp(t1->port_list[i]+GPISL);//port interrupt status

	
	
	if((isr0+isr1+isr2+pis)==0) goto nextpt;
	inthit = 1;


    //here we start doing the isr's
//um we need to convert this!    
    if((isr0&0x80)==0x80)
    	{
		if(t1->eopmode[i]==OPMODE_HDLC) t1->port_status[i] = t1->port_status[i] + RDO_INTERRUPT;
		}
    if((isr0&0x40)==0x40)
    	{
		t1->port_status[i] = t1->port_status[i] + RFO_INTERRUPT;
		}

	if((isr0&0x20)==0x20)
		{
		//PCE/FERR/SCD
		if(t1->eopmode[i]==OPMODE_HDLC)
			{
			t1->port_status[i] = t1->port_status[i] + PCE_INTERRUPT;
			}
		else if(t1->eopmode[i]==OPMODE_ASYNC)
			{
			t1->port_status[i] = t1->port_status[i] + FERR_INTERRUPT;
			}
		else if(t1->eopmode[i]==OPMODE_BISYNC)
			{
			t1->port_status[i] = t1->port_status[i] + SYN_INTERRUPT;
			}
		}

	if((isr0&0x10)==0x10)
		{
		//RSC/PERR
		if(t1->eopmode[i]==OPMODE_HDLC)
			{
			t1->port_status[i] = t1->port_status[i] + RSC_INTERRUPT;
			}
		else
			{
			t1->port_status[i] = t1->port_status[i] + PERR_INTERRUPT;
			}
		}

	if((isr0&0x08)==0x08)
		{                         
		unsigned ccr3val;    
		unsigned cnt;
		unsigned cnt2;
		ccr3val = _inp(t1->port_list[i]+CCR3H+(t1->channel[i]*0x50));
		//RPF
		//SimplDrvKdPrint (("RPF\n"));

	if(t1->eopmode[i]==OPMODE_HDLC)
		{
		//get trigger value
		if((ccr3val&0x3)==0x00) cnt=32;
		if((ccr3val&0x3)==0x01) cnt=16;
		if((ccr3val&0x3)==0x02) cnt=4;
		if((ccr3val&0x3)==0x03) cnt=2;
		}
	if( (t1->eopmode[i]==OPMODE_ASYNC)||(t1->eopmode[i]==OPMODE_BISYNC))
		{
		//DATA IS READY                                             
//###########this is the "normal code section for receiving async/bisync data
		//get trigger value from ccr3h 1:0
		if((ccr3val&0x08)==0x08)				
			{//RFDF=1 (data+parity/status)
			if((ccr3val&0x3)==0x00)	cnt = 2;
			if((ccr3val&0x3)==0x01)	cnt = 4;
			if((ccr3val&0x3)==0x02)	cnt = 16;
			if((ccr3val&0x3)==0x03)	cnt = 32;
			}
		else
			{//RFDF=0 (data only)
			if((ccr3val&0x3)==0x00)	cnt = 1;
			if((ccr3val&0x3)==0x01)	cnt = 4;
			if((ccr3val&0x3)==0x02)	cnt = 16;
			if((ccr3val&0x3)==0x03)	cnt = 32;
			}
		}
	cnt2=cnt/2;
	if(cnt2==0) cnt2=1;

			irxbuf = (t1->rxbuffer[i][t1->current_rxbuf[i]]);
        	bloc = (unsigned far *)irxbuf->frame;
        	for(j=(irxbuf->no_bytes>>1);j<(irxbuf->no_bytes>>1)+cnt2;j++) bloc[j] = _inpw(t1->port_list[i]+FIFO+(t1->channel[i]*0x50));
			while((_inp(t1->port_list[i]+STARL+(t1->channel[i]*0x50))&CEC)==CEC);
			_outp(t1->port_list[i]+CMDRH+(t1->channel[i]*0x50),RMC);     //release fifo
        	irxbuf->no_bytes += cnt;
        

		if(irxbuf->no_bytes >= (FRAME_SIZE - cnt))
		{
		irxbuf->valid = 1;//validate rbuf
		t1->current_rxbuf[i]++;  
		//irxbuf is no longer valid
		if(t1->current_rxbuf[i]==t1->max_rxbuf[i]) t1->current_rxbuf[i]=0;                
		if(t1->rxbuffer[i][t1->current_rxbuf[i]]->valid ==1) 
			{
						   //	an error occured and the 
					       //   main program isn't done with 
					       //   the frame that is about
					       //   to be used for received data
			//set the receive buffers overflowed bit of the status word for this port here
			t1->port_status[i] = t1->port_status[i] | RX_BUFFER_OVERFLOW;
			}
		t1->rxbuffer[i][t1->current_rxbuf[i]]->no_bytes = 0;  //prep buf for use
		t1->rxbuffer[i][t1->current_rxbuf[i]]->valid = 0;    //invalidate it for use
        t1->port_status[i] = t1->port_status[i] | RX_READY;
		}
	}


	if((isr0&0x04)==0x04)
	{
	//RME/TCD
	//SimplDrvKdPrint (("RME\n"));
		k = _inpw(t1->port_list[i]+RBCL+(t1->channel[i]*0x1a))&0x0fff;

		irxbuf = (t1->rxbuffer[i][t1->current_rxbuf[i]]);     
		for(j=irxbuf->no_bytes;j<k;j++) irxbuf->frame[j] = _inp(t1->port_list[i]+FIFO+(t1->channel[i]*0x50));
		while((_inp(t1->port_list[i]+STARL+(t1->channel[i]*0x50))&CEC)==CEC);
		_outp(t1->port_list[i]+CMDRH+(t1->channel[i]*0x50),RMC);     //release fifo
		irxbuf->no_bytes = k; //number of bytes received in rbuf
		irxbuf->valid = 1;//validate rbuf
		t1->current_rxbuf[i]++;  
		//irxbuf is no longer valid
		if(t1->current_rxbuf[i]==t1->max_rxbuf[i]) t1->current_rxbuf[i]=0;                
		if(t1->rxbuffer[i][t1->current_rxbuf[i]]->valid ==1) 
			{
						   //	an error occured and the 
					       //   main program isn't done with 
					       //   the frame that is about
					       //   to be used for received data
			//set the receive buffers overflowed bit of the status word for this port here
			t1->port_status[i] = t1->port_status[i] | RX_BUFFER_OVERFLOW;
			}
		t1->rxbuffer[i][t1->current_rxbuf[i]]->no_bytes = 0;  //prep buf for use
		t1->rxbuffer[i][t1->current_rxbuf[i]]->valid = 0;    //invalidate it for use
        t1->port_status[i] = t1->port_status[i] | RX_READY;

	if(t1->eopmode[i]==OPMODE_BISYNC)
		{  	
		while((_inp(t1->port_list[i]+STARL+(t1->channel[i]*0x50))&CEC)==CEC);
		_outp(t1->port_list[i]+CMDRH+(t1->channel[i]*0x50),HUNT);     //release fifo
		}
	}
	
	if((isr0&0x02)==0x02)
		{
		//RFS/TIME
		if(t1->eopmode[i]==OPMODE_HDLC)
		{			
			//RFS interrupt time to start the coffee
			t1->port_status[i] = t1->port_status[i] + RFS_INTERRUPT;
        }
		else
			{
			if((_inp(t1->port_list[i]+STARH+(t1->channel[i]*0x50))&0x40)==0x40) 
				{
				_outp(t1->port_list[i]+CMDRH+(t1->channel[i]*0x50),(UCHAR)0x02); //this will force a RME interrupt
				}
			else
				{
				//just finish the frame
				irxbuf = (t1->rxbuffer[i][t1->current_rxbuf[i]]);     
				irxbuf->valid = 1;//validate rbuf
				t1->current_rxbuf[i]++;  
				//irxbuf is no longer valid
				if(t1->current_rxbuf[i]==t1->max_rxbuf[i]) t1->current_rxbuf[i]=0;                
				if(t1->rxbuffer[i][t1->current_rxbuf[i]]->valid ==1) 
					{
								   //	an error occured and the 
								   //   main program isn't done with 
								   //   the frame that is about
								   //   to be used for received data
					//set the receive buffers overflowed bit of the status word for this port here
					t1->port_status[i] = t1->port_status[i] | RX_BUFFER_OVERFLOW;
					}
				t1->rxbuffer[i][t1->current_rxbuf[i]]->no_bytes = 0;  //prep buf for use
				t1->rxbuffer[i][t1->current_rxbuf[i]]->valid = 0;    //invalidate it for use
				t1->port_status[i] = t1->port_status[i] | RX_READY;
				}

			}
		}

   
    	
    if((isr1&0x80)==0x80)
    	{
			t1->port_status[i] = t1->port_status[i] | TIN_INTERRUPT;
			tincount++;
    	}
    if((isr1&0x40)==0x40)
    	{               
    		t1->port_status[i] = t1->port_status[i] | CTSC_INTERRUPT;
    	}
    if((isr1&0x20)==0x20)
    	{   
		//XMR/XOFF/XMR
		
    	if(t1->eopmode[i]==OPMODE_HDLC)
    		{
    		t1->port_status[i] = t1->port_status[i] | XMR_INTERRUPT;
    		}                                         
    	if(t1->eopmode[i]==OPMODE_ASYNC)
    		{
    		t1->port_status[i] = t1->port_status[i] | XOFF_INTERRUPT;
    		}
    	if(t1->eopmode[i]==OPMODE_BISYNC)
    		{
    		t1->port_status[i] = t1->port_status[i] | XMR_INTERRUPT;
    		}
    	}
    if((isr1&0x10)==0x10)
    	{
		//XPR
    	//XPR interrupt (time to send the bytes out)            
			//do interrupt xpr here  
	                             
			itxbuf = (t1->txbuffer[i][t1->current_txbuf[i]]);
            bloc = (unsigned far *)itxbuf->frame;
			if(itxbuf->valid==1)
				{
				if((itxbuf->max - itxbuf->no_bytes) > 32)
					{
					//do send 32
					for(j=(itxbuf->no_bytes)>>1;j<(itxbuf->no_bytes>>1)+16;j++)
						_outpw(t1->port_list[i]+FIFO+(t1->channel[i]*0x50),bloc[j]);	
					while((_inp(t1->port_list[i]+STARL+(t1->channel[i]*0x50))&CEC)==CEC);						
					_outp(t1->port_list[i]+CMDRL+(t1->channel[i]*0x50),t1->tx_type[i]);
					itxbuf->no_bytes += 32;
					t1->istxing[i] = 1;
					goto donetxingnow;
					}
				else
					{
					//do send <=32 //sending as byte accesses such that odd bytes can go out
					for(j=itxbuf->no_bytes;j<itxbuf->max;j++)
						_outp(t1->port_list[i]+FIFO+(t1->channel[i]*0x50),itxbuf->frame[j]); 
					while((_inp(t1->port_list[i]+STARL+(t1->channel[i]*0x50))&CEC)==CEC);
					if(t1->eopmode[i]!=OPMODE_ASYNC) _outp(t1->port_list[i]+CMDRL+(t1->channel[i]*0x50),t1->tx_type[i]+XME);
					else _outp(t1->port_list[i]+CMDRL+(t1->channel[i]*0x50),t1->tx_type[i]);
					itxbuf->no_bytes = itxbuf->max;
					itxbuf->valid = 0;
					t1->current_txbuf[i]++;
					if(t1->current_txbuf[i]==t1->max_txbuf[i]) t1->current_txbuf[i] = 0;
					//goto try_again_tx;
					}
				}
			else
				{
				t1->istxing[i] = 0; //no valid frames we are done txing
				}
donetxingnow:;
    	}
if((isr1&0x08)==0x08)
	{
	//ALLS
	t1->port_status[i] = t1->port_status[i] | ALLS_INTERRUPT;
	}
if((isr1&0x04)==0x04)
{
//XDU/XON/XDU
	if(t1->eopmode[i]==OPMODE_ASYNC) t1->port_status[i] = t1->port_status[i] | XON_INTERRUPT;
	else t1->port_status[i] = t1->port_status[i] | XDU_INTERRUPT;
}
if((isr1&0x02)==0x02)
{
//SUEX/BRK
	if(t1->eopmode[i]==OPMODE_ASYNC) t1->port_status[i] = t1->port_status[i] | BREAK_DETECTED;
}
if((isr1&0x01)==0x01)
{
//BRKT
	if(t1->eopmode[i]==OPMODE_ASYNC) t1->port_status[i] = t1->port_status[i] | BREAK_TERMINATED;
}
if((isr2&0x02)==0x02)
{
//PLLA
t1->port_status[i] = t1->port_status[i] | PLLA_INTERRUPT;
}
if((isr2&0x01)==0x01)
{
//CDSC
t1->port_status[i] = t1->port_status[i] | CDSC_INTERRUPT;
}
if((pis&0x02)==0x02)
{
t1->port_status[i] = t1->port_status[i] | DSR0C_INTERRUPT;
}
if((pis&0x04)==0x04)
{
t1->port_status[i] = t1->port_status[i] | DSR1C_INTERRUPT;
}
			
	nextpt:
	;
	
	}
//while(inthit==1);
if(inthit==1) goto startisr;
//do eoi's and leave
_outp(0x20,0x20);
if(t1->upper_irq!=0) _outp(0xa0,0x20);
}


void DELAY()
{                            
int i;
for(i=0;i<4;i++) _inp(0x20);
}

void Cescc::setics307clock(unsigned port, unsigned long hval)
{
	unsigned long tempValue = 0;
	int i=0;
	unsigned char data=0;
	unsigned char savedval;
	unsigned base;
	base = amcc_port_list[port]+2+4;	
	
	_outp(base,(unsigned char)tempValue);

	tempValue = (hval & 0x00ffffff);
//	printf("tempValue = 0x%X\n",tempValue);


	for(i=0;i<24;i++)
	{
		//data bit set
		if ((tempValue & 0x800000)!=0) 
		{
		data = 1;
//			printf("1");
//		data <<=8;
		}
		else 
		{
			data = 0;
//			printf("0");
		}
		_outp(base,data);
		DELAY();

		//clock high, data still there
		data |= 0x02;
		_outp(base,data);
		DELAY();
		//clock low, data still there
		data &= 0x01;
		_outp(base,data);
		DELAY();

		tempValue<<=1;
	}
//	printf("\n");

	data = 0x4;		//strobe on
	_outp(base,data);
	DELAY();	
	data = 0x0000;		//all off
	_outp(base,data);
	DELAY();
}	//void set_clock_generator_307(unsigned long hval)

void Cescc::set_clock_generator(unsigned port, unsigned long hval,unsigned nmbits)
{
unsigned base;
unsigned curval;
unsigned long tempval;
unsigned i;

base = port_list[port];
curval = 0;
//bit 0 = data
//bit 1 = clock;
outp(base+0x3c,curval);

tempval = STARTWRD;
for(i=0;i<14;i++)
	{
	curval = 0;
	curval = (char)(tempval&0x1);	//set bit
	outp(base+0x3c,curval);
	curval = curval |0x02;		//force rising edge
	outp(base+0x3c,curval);		//clock in data
	curval = curval &0x01;		//force falling edge
	outp(base+0x3c,curval);		//set clock low
	tempval = tempval >> 1;		//get next bit
	}
	
tempval = hval;
for(i=0;i<nmbits;i++)
	{
	curval = 0;
	curval = (char)(tempval&0x1);	//set bit
	outp(base+0x3c,curval);
	curval = curval |0x02;		//force rising edge
	outp(base+0x3c,curval);		//clock in data
	curval = curval &0x01;		//force falling edge
	outp(base+0x3c,curval);		//set clock low
	tempval = tempval >> 1;		//get next bit
	}

tempval = MIDWRD;
for(i=0;i<14;i++)
	{
	curval = 0;
	curval = (char)(tempval&0x1);	//set bit
	outp(base+0x3c,curval);
	curval = curval |0x02;		//force rising edge
	outp(base+0x3c,curval);		//clock in data
	curval = curval &0x01;		//force falling edge
	outp(base+0x3c,curval);		//set clock low
	tempval = tempval >> 1;		//get next bit
	}
//pause for >10ms --should be replaced with a regulation pause routine
for(i=0;i<32000;i++)inp(0x20);

tempval = ENDWRD;
for(i=0;i<14;i++)
	{
	curval = 0;
	curval = (char)(tempval&0x1);	//set bit
	outp(base+0x3c,curval);
	curval = curval |0x02;		//force rising edge
	outp(base+0x3c,curval);		//clock in data
	curval = curval &0x01;		//force falling edge
	outp(base+0x3c,curval);		//set clock low
	tempval = tempval >> 1;		//get next bit
	}

}


void banner()
{
_clearscreen(_GCLEARSCREEN);
printf("QSSB PCI/MATCHMAKER\r\n");
printf("Copyright(C) 2007 Commtech, Inc.\r\n");
}
