#define FRAME_SIZE 4096         //maximum size of a received frame 
#define MAX_RBUFS 25
#define MAX_TBUFS 25
#define MAX_PORTS 6

//SEROCCO defines
//STARL defines
#define XREPE	0x80
#define TEC		0x20
#define CEC		0x10
#define FCS		0x08
#define XDOV	0x04
#define XFW		0x02
#define CTS		0x01
//STARH defines
#define RFNE	0x40
#define CD		0x20
#define RLI		0x10
#define SYNC	0x10
#define DPLA	0x08
#define WFA		0x04
#define XRNR	0x02
#define RRNR	0x01
//CMDRL defines
#define STI		0x80
#define TRES	0x40
#define XIF		0x20
#define TXON    0x20
#define XRES	0x10
#define XF		0x08
#define XME		0x04
#define XREP	0x02
#define TXOFF   0x01
//CMDRH defines
#define RMC		0x80
#define RNR		0x40
#define	RSUC	0x08
#define HUNT	0x08
#define RFRD	0x02
#define RRES	0x01
//CCR0L defines
#define VIS		0x80
#define PSD		0x40
#define BCR		0x20
#define TOE		0x10
#define SSEL	0x08
#define CM		0x07
//CCR0H defines
#define PU		0x80
#define SC		0x70
#define SM		0x03
//CCR1L defines
#define CRL		0x80
#define C32		0x40
#define SOC		0x30
#define SFLG	0x08
#define DIV		0x04
#define ODS		0x02
//CCR1H defines
#define ICD		0x40
#define RTS		0x10
#define FRTS	0x08
#define FCTS	0x04
#define CAS		0x02
#define TSCM	0x01
//CCR2L defines
#define MDS		0xc0
#define ADM		0x20
#define NRM		0x10
#define PPPM	0x0c
#define TLPO	0x02
#define TLP		0x01
#define SLEN	0x08
#define BISNC	0x04
//CCR2H defines
#define MCS		0x80
#define EPT		0x40
#define NPRE	0x30
#define ITF		0x08
#define OIN		0x02
#define XCRC	0x01
#define FLON	0x01
#define CAPP	0x02
#define CRCM	0x01
//CCR3L defines
#define ELC		0x80
#define AFX		0x40
#define CSF		0x20
#define SUET	0x10
#define RAC		0x08
#define ESS7	0x01
#define TCDE	0x80
#define SLOAD	0x40
#define CHL		0x30
#define DXS		0x04
#define XBRK	0x02
#define STOP	0x01
//CCR3H defines
#define DRCRC	0x40
#define PAR		0xc0
#define RCRC	0x20
#define PARE	0x20
#define RADD	0x10
#define DPS		0x10
#define RFDF	0x08
#define RFTH	0x03
//BRRL defines
#define BRN		0x3f
//BRRH defines
#define BRM		0x0f
//TIMER3 defines
#define SRC		0x80
#define TMD		0x10
#define CNT		0x07
//IMR0 defines
#define RDO		0x80
#define RFO		0x40
#define PCE		0x20
#define FERR	0x20
#define SCD		0x20
#define RSC		0x10
#define PERR	0x10
#define RPF		0x08
#define RME		0x04
#define TCD		0x04
#define RFS		0x02
#define TIMEO	0x02
#define FLEX	0x01
//IMR1 defines
#define TIN		0x80
#define CSC		0x40
#define XMR		0x20
#define XOFFI	0x20
#define XPR		0x10
#define ALLS	0x08
#define XDU		0x04
#define XONI	0x04
#define SUEX	0x02
#define BRK		0x02
#define BRKT	0x01
//IMR2 defines
#define PLLA	0x02
#define CDSC	0x01


#define GCMDR  0x00
#define GMODE  0x01
//2 is reserved
#define GSTAR  0x03
#define GPDIRL  0x04
#define GPDIRH  0x05
#define GPDATL  0x06
#define GPDATH  0x07
#define GPIML  0x08
#define GPIMH  0x09
#define GPISL  0x0a
#define GPISH  0x0b
#define DCMDR  0x0c
//d is reserved
#define DISR   0x0e
#define DIMR   0x0f


//from here down it is accessed as  x+(0x50*dev->channel)
//ie the second channel starts at 0x60 and runs up parallel to the first.
#define FIFO   0x10
//0x11 is high byte of fifo
#define STARL  0x12
#define STARH  0x13
#define CMDRL  0x14
#define CMDRH  0x15
#define CCR0L  0x16
#define CCR0H  0x17
#define CCR1L  0x18
#define CCR1H  0x19
#define CCR2L  0x1a
#define CCR2H  0x1b
#define CCR3L  0x1c
#define CCR3H  0x1d
#define PREAMB  0x1e
#define TOLEN  0x1f
#define ACCM0  0x20
#define ACCM1  0x21
#define ACCM2  0x22
#define ACCM3  0x23
#define UDAC0  0x24
#define UDAC1  0x25
#define UDAC2  0x26
#define UDAC3  0x27
#define TTSA0  0x28
#define TTSA1  0x29
#define TTSA2  0x2a
#define TTSA3  0x2b
#define RTSA0  0x2c
#define RTSA1  0x2d
#define RTSA2  0x2e
#define RTSA3  0x2f
#define PCMTX0  0x30
#define PCMTX1  0x31
#define PCMTX2  0x32
#define PCMTX3  0x33
#define PCMRX0  0x34
#define PCMRX1  0x35
#define PCMRX2  0x36
#define PCMRX3  0x37
#define BRRL  0x38
#define BRRH  0x39
#define TIMR0  0x3a
#define TIMR1  0x3b
#define TIMR2  0x3c
#define TIMR3  0x3d
#define XAD1  0x3e
#define XAD2  0x3f
#define RAL1  0x40
#define RAH1  0x41
#define RAL2  0x42
#define RAH2  0x43
#define AMRAL1  0x44
#define AMRAH1  0x45
#define AMRAL2  0x46
#define AMRAH2  0x47
#define RLCRL   0x48
#define RLCRH   0x49
#define XON  0x4a
#define XOFF  0x4b
#define MXON  0x4c
#define MXOFF  0x4d
#define TCR  0x4e
#define TICR  0x4f
#define ISR0  0x50
#define ISR1  0x51
#define ISR2  0x52
//53 is reserved
#define IMR0  0x54
#define IMR1  0x55
#define IMR2  0x56
//57 is reserved
#define RSTA  0x58
//59 is reserved
#define SYNCL  0x5a
#define SYNCH  0x5b
#define XBCL  0xb8
#define XBCH  0xb9
#define RMBSL  0xc4
#define RMBSH  0xc5
#define RBCL  0xc6
#define RBCH  0xc7
#define VER0  0xec
#define VER1  0xed
#define VER2  0xee
#define VER3  0xef

			
//status function defines

#define RDO_INTERRUPT		0x000001
#define RFO_INTERRUPT		0x000002
#define PCE_INTERRUPT		0x000004
#define FERR_INTERRUPT		0x000008
#define SYN_INTERRUPT		0x000010
#define RSC_INTERRUPT		0x000020
#define PERR_INTERRUPT		0x000040
#define RX_BUFFER_OVERFLOW	0x000080
#define RX_READY			0x000100
#define RFS_INTERRUPT		0x000200
#define TIN_INTERRUPT		0x000400
#define CTSC_INTERRUPT		0x000800
#define XMR_INTERRUPT		0x001000
#define XOFF_INTERRUPT		0x002000
#define ALLS_INTERRUPT		0x008000
#define XON_INTERRUPT		0x010000
#define XDU_INTERRUPT		0x020000
#define BREAK_DETECTED		0x040000
#define BREAK_TERMINATED	0x080000
#define PLLA_INTERRUPT		0x100000
#define CDSC_INTERRUPT		0x200000
#define DSR0C_INTERRUPT		0x400000
#define DSR1C_INTERRUPT		0x800000


#define UCHAR unsigned char
#define TRUE 1                                                                  
#define FALSE 0

#define CHANNEL0 0
#define CHANNEL1 1

#define OPMODE_HDLC 	0
#define OPMODE_ASYNC 	1
#define OPMODE_BISYNC 	2

#define AUTO_MODE 0
#define TRANSPARENT_MODE 1

//defines for 2053b
#define STARTWRD 0x1e05
#define MIDWRD   0x1e04
#define ENDWRD   0x1e00


struct serocco_setup{
unsigned gcmdr;
unsigned gmode;
//unsigned reserved1;
//unsigned gstar;
unsigned gpdirl;
unsigned gpdirh;
unsigned gpdatl;
unsigned gpdath;
unsigned gpiml;
unsigned gpimh;
//unsigned gpisl;
//unsigned gpish;
unsigned dcmdr;
//unsigned reserved2;
//unsigned disr;
unsigned dimr;
//unsigned fifo;
//unsigned starl;
//unsigned starh;
unsigned cmdrl;
unsigned cmdrh;
unsigned ccr0l;
unsigned ccr0h;
unsigned ccr1l;
unsigned ccr1h;
unsigned ccr2l;
unsigned ccr2h;
unsigned ccr3l;
unsigned ccr3h;
unsigned preamb;
unsigned tolen;
unsigned accm0;
unsigned accm1;
unsigned accm2;
unsigned accm3;
unsigned udac0;
unsigned udac1;
unsigned udac2;
unsigned udac3;
unsigned ttsa0;
unsigned ttsa1;
unsigned ttsa2;
unsigned ttsa3;
unsigned rtsa0;
unsigned rtsa1;
unsigned rtsa2;
unsigned rtsa3;
unsigned pcmtx0;
unsigned pcmtx1;
unsigned pcmtx2;
unsigned pcmtx3;
unsigned pcmrx0;
unsigned pcmrx1;
unsigned pcmrx2;
unsigned pcmrx3;
unsigned brrl;
unsigned brrh;
unsigned timer0;
unsigned timer1;
unsigned timer2;
unsigned timer3;
unsigned xad1;
unsigned xad2;
unsigned ral1;
unsigned rah1;
unsigned ral2;
unsigned rah2;
unsigned amral1;
unsigned amrah1;
unsigned amral2;
unsigned amrah2;
unsigned rlcrl;
unsigned rlcrh;
unsigned xon;
unsigned xoff;
unsigned mxon;
unsigned mxoff;
unsigned tcr;
unsigned ticr;
//unsigned isr0;
//unsigned isr1;
//unsigned isr2;
unsigned imr0;
unsigned imr1;
unsigned imr2;
//unsigned reserved3;
//unsigned rsta;
//unsigned reserved4;
unsigned syncl;
unsigned synch;

unsigned n_rbufs;
unsigned n_tbufs;
unsigned n_rfsize_max;
unsigned n_tfsize_max;

};

struct buf{
unsigned valid;         //indicator 1 = frame[] has data, 0 = frame[] has ???
unsigned no_bytes;      //number of bytes in frame[]
unsigned max;           //maximum number of bytes to send/receive
char frame[FRAME_SIZE];  //data array for received/transmitted data
				
};


class __far Cescc
{
protected:
//protected variables

unsigned port_list[MAX_PORTS];						//base address list
unsigned amcc_port_list[MAX_PORTS];					//base address of amcc registers
unsigned port_open_list[MAX_PORTS];                 	//port has been inited list
unsigned interrupt_list[MAX_PORTS];					//ports associated interrupt level (hardware)
unsigned port_dmat_list[MAX_PORTS];
unsigned port_dmar_list[MAX_PORTS];
unsigned hooked_irqs[16];						//list of irq vectors that are hooked (by number)
void (interrupt far *old_service_routines[16])();//hooked interrupt service vectors previous routines
unsigned next_port;								//holds the next port to be used (added)
unsigned next_irq;								//holds the next irq to be added
unsigned upper_irq;								//flag for ISR to send upper EOI if irq >8 is being used
unsigned current_rxbuf[MAX_PORTS];
unsigned current_txbuf[MAX_PORTS];
unsigned max_rxbuf[MAX_PORTS];
unsigned max_txbuf[MAX_PORTS];
unsigned timer_status[MAX_PORTS];
unsigned tx_type[MAX_PORTS];
unsigned istxing[MAX_PORTS];	//==1 if a frame is being sent ,==0 if no txing is going on
unsigned long  port_status[MAX_PORTS];
unsigned channel[MAX_PORTS];
unsigned eopmode[MAX_PORTS];
unsigned user_port_list[MAX_PORTS];
unsigned max_user_port;
struct buf far *rxbuffer[MAX_PORTS][MAX_RBUFS];							//array of pointers for receive buffering
struct buf far *txbuffer[MAX_PORTS][MAX_TBUFS];							//array of pointers for transmitt buffering

public:
//public varaiables
Cescc(); 
~Cescc();

// Operations
public:
//user callable functions
unsigned get_max_port(void);
void get_user_port_list(unsigned *list);
void set_clock_generator(unsigned port, unsigned long hval,unsigned nmbits);
void setics307clock(unsigned port, unsigned long hval);
unsigned add_port(unsigned base, unsigned irq,unsigned amccbase);//return port# (index into port..arrays)
unsigned kill_port(unsigned port);//true ==success
unsigned init_port(	unsigned port,
					unsigned opmode,
					struct serocco_setup *gsccsetup,
					unsigned rbufs,
					unsigned tbufs);//true ==success
unsigned rx_port(unsigned port,char far *buf, unsigned num_bytes);//returns # bytes transfered, 0 if fails
unsigned tx_port(unsigned port, char far *buf, unsigned num_bytes);//returns # bytes transfered,0 if fails
unsigned set_control_lines(unsigned port,unsigned dtr, unsigned rts);//sets or clears dtr/rts 1 = set 0 = clear
unsigned get_control_lines(unsigned port);//return  = bit flags  X X X DSR DTR DCD CTS RTS 
unsigned long get_port_status(unsigned port);//returns status flag consisting of one or more of the following
													//XMR_INTERRUPT   0x0001
													//EXE_INTERRUPT   0x0002
													//PCE_INTERRUPT   0x0004
													//RFO_INTERRUPT   0x0008
													//CTS_INTERRUPT   0x0010
													//RFS_INTERRUPT   0x0020
													//RX_BUFFER_OVERFLOW 0x0040
													//RSC_INTERRUPT   0x0080
													//TIMER_INTERRUPT 0x0100
													//RX_READY        0x0200

unsigned clear_rx_buffer(unsigned port); //False if port not open
unsigned clear_tx_buffer(unsigned port); //False if port not open
unsigned start_timer(unsigned port); //False if port not open
unsigned is_timer_expired(unsigned port); //False if port not open
unsigned wait_for_timer_expired(unsigned port); //False if port not open
unsigned stop_timer(unsigned port);//Fals if port not open

//hdlc/sdlc specific
unsigned set_tx_type(unsigned port,unsigned type); //False if port not open
unsigned set_tx_address(unsigned port , unsigned address); //False if port not open
unsigned set_rx_address1(unsigned port,unsigned address); //False if port not open
unsigned set_rx_address2(unsigned port,unsigned address); //False if port not open


// Implementation
protected:
//class defined functions	
void cdecl interrupt far escc_isr(void);
};

