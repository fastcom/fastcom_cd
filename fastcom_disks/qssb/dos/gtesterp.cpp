#include "cgsccp.h"
#include "conio.h"
#include "stdlib.h"
#include "stdio.h"
#include "bios.h"
#include "dos.h"
#include "string.h"
#include "graph.h"

void atest();
void btest();
void htest();
void cltest();
void interrupt timer_isr();
	
unsigned long timertick = 0;
unsigned long tincount = 0;
unsigned long RTCcount = 0;
Cescc esccdrv;
unsigned bdrt;
unsigned long toterr;
void main(int argc, char *argv[])
{          
unsigned i;
bdrt = 0;


do{
//_clearscreen(_GCLEARSCREEN);
printf("FASTCOM:QSSB LOOPTST -- LOOPBACK FUNCTIONAL TESTER\r\n");
toterr = 0;
printf("testing HDLC\r\n");
htest();
printf("testing async\r\n");
atest();
printf("testing bisync\r\n");
btest();         
printf("testing clock generator\r\n");
cltest();

if(toterr==0) printf("QSSB Passed test %c\r\n",7);
else printf("QSSB FAILED test \r\n");
printf("do another (y/n)?\r\n");
i = getch();
if((i=='n')||(i=='N'))
	{
	exit(0);
	}

}while(1);

}


void atest()
{
//FASTCOM ESCC ASYNC LOOPTEST



char buffer[4096];
char buffer2[4096];
struct serocco_setup settings;
unsigned i,j,k,lp;
unsigned bt;
unsigned error;
unsigned long terr;
unsigned long tcnt;
unsigned ports[255];
unsigned maxport;
maxport = esccdrv.get_max_port();
esccdrv.get_user_port_list(ports);


terr  = 0;
tcnt = 0;


printf("ASYNC\r\n");
//async settings



			settings.gcmdr   =0x0000;
			settings.gmode   =0x003a;
			settings.gpdirl  =0x0006;
			settings.gpdirh  =0x00bf;
			settings.gpdatl  =0x0000;
			settings.gpdath  =0x0000;
			settings.gpiml   =0x0007;
			settings.gpimh   =0x00ff;
			settings.dcmdr   =0x0000;
			settings.dimr    =0x0000;
			settings.cmdrl   =0x0000;
			settings.cmdrh   =0x0000;
			settings.ccr0l   =0x003f;
			settings.ccr0h   =0x0083;
			settings.ccr1l   =0x0002;
			settings.ccr1h   =0x0000;
			settings.ccr2l   =0x0000;
			settings.ccr2h   =0x0000;
			settings.ccr3l   =0x0008;
			settings.ccr3h   =0x0003;
			settings.preamb  =0x0000;
			settings.tolen   =0x0080;
			settings.accm0   =0x0000;
			settings.accm1   =0x0000;
			settings.accm2   =0x0000;
			settings.accm3   =0x0000;
			settings.udac0   =0x0000;
			settings.udac1   =0x0000;
			settings.udac2   =0x0000;
			settings.udac3   =0x0000;
			settings.ttsa0   =0x0000;
			settings.ttsa1   =0x0000;
			settings.ttsa2   =0x0000;
			settings.ttsa3   =0x0000;
			settings.rtsa0   =0x0000;
			settings.rtsa1   =0x0000;
			settings.rtsa2   =0x0000;
			settings.rtsa3   =0x0000;
			settings.pcmtx0  =0x0000;
			settings.pcmtx1  =0x0000;
			settings.pcmtx2  =0x0000;
			settings.pcmtx3  =0x0000;
			settings.pcmrx0  =0x0000;
			settings.pcmrx1  =0x0000;
			settings.pcmrx2  =0x0000;
			settings.pcmrx3  =0x0000;
			settings.brrl    =0x0002;//983000/16/((2+1)*2) ~ 102k
			settings.brrh    =0x0001;
			settings.timer0  =0x00ff;
			settings.timer1  =0x0007;
			settings.timer2  =0x0000;
			settings.timer3  =0x0000;
			settings.xad1    =0x0000;
			settings.xad2    =0x0000;
			settings.ral1    =0x0000;
			settings.rah1    =0x0000;
			settings.ral2    =0x0000;
			settings.rah2    =0x0000;
			settings.amral1  =0x0000;
			settings.amrah1  =0x0000;
			settings.amral2  =0x0000;
			settings.amrah2  =0x0000;
			settings.rlcrl   =0x0000;
			settings.rlcrh   =0x0000;
			settings.xon     =0x0000;
			settings.xoff    =0x0000;
			settings.mxon    =0x0000;
			settings.mxoff   =0x0000;
			settings.tcr     =0x0000;
			settings.ticr    =0x0000;
			settings.imr0    =0x0081;
			settings.imr1    =0x0000;
			settings.imr2    =0x00fd;
			settings.syncl   =0x0000;
			settings.synch   =0x0000;


for(i=0;i<4;i++)
{
j = esccdrv.init_port(ports[i],OPMODE_ASYNC,&settings,10,10);
//if(j==TRUE)printf("Initializing port 1 OK\n\r");
if(j==FALSE)printf("Initializing port %d FAILED\n\r",i);
esccdrv.clear_rx_buffer(ports[i]);
esccdrv.clear_tx_buffer(ports[i]);
}

for(lp=0;lp<50;lp++)
{
printf("\t\t\tTesting port :");
for(i=0;i<4;i++)
{                           
printf("%d",i);

error = 0;

//send a frame of data out port a

for(j=0;j<1024;j++)buffer[j] = j;     
esccdrv.tx_port(ports[i],buffer,j);
//printf("sending %u bytes\r\n",j);
j = 0;
/*
while( (j&RX_READY) !=RX_READY )
		{
		j = esccdrv.get_port_status(porta);
		printf ("waiting...%4.4x\r",j);
		if(j!=0) 
			{                                                
			printf("\n\r");
			if((j&XMR_INTERRUPT)==XMR_INTERRUPT) printf("XMR\n\r");
			if((j&PCE_INTERRUPT)==PCE_INTERRUPT) printf("PCE\n\r");
			if((j&EXE_INTERRUPT)==EXE_INTERRUPT) printf("EXE\n\r");
			if((j&RFO_INTERRUPT)==RFO_INTERRUPT) printf("RFO\n\r");
			if((j&CTS_INTERRUPT)==CTS_INTERRUPT) printf("CTS\n\r");
			if((j&RFS_INTERRUPT)==RFS_INTERRUPT) printf("RFS\n\r");
			if((j&RX_BUFFER_OVERFLOW)==RX_BUFFER_OVERFLOW) printf("RX_BUFFER_OVERFLOW\n\r");
			if((j&RSC_INTERRUPT)==RSC_INTERRUPT) printf("RSC\n\r");
	    if((j&TIMER_INTERRUPT)==TIMER_INTERRUPT) printf("TIMER\n\r");
	    if((j&RX_READY)==RX_READY) printf("RX READY\n\r");
	    if((j&PARITY_ERROR)==PARITY_ERROR) printf("PARITY ERROR\n\r");
	    if((j&FRAMING_ERROR)==FRAMING_ERROR) printf("FRAMING ERROR\n\r");
	    if((j&BREAK_DETECTED)==BREAK_DETECTED) printf("BREAK DETECTED\n\r");
	    if((j&BREAK_TERMINATED)==BREAK_TERMINATED) printf("BREAK TERMINATED\n\r");
	    if((j&ALLSENT_INTERRUPT)==ALLSENT_INTERRUPT) printf("ALLSENT INTERRUPT\n\r");
			}
		if(kbhit()!=0) return;
		}
printf("\n\r");
*/

while((j = esccdrv.rx_port(ports[i],buffer2,4096))==0);
if(j==1024)
{
for(bt=0;bt<j;bt++)
	{
//      printf("Out:%x  In:%x \r",buffer[i]&0xff,buffer2[i]&0xff);
	if((buffer2[bt]&0xff)!=(buffer[bt]&0xff))
		{
		//error
		printf("\r\nERROR in LOOPBACK[%x:%x,%x]\r\n",buffer2[bt]&0xff,buffer[bt]&0xff,bt);
		error++;
		printf("\r\n"); 
		}
	}
}
else error++;


//printf("%u bytes received--%u bytes expected\n\r",j,1024);
//if(error==0) printf("Port 0 passed\r\n");
if(error>0) printf("Port %d failed\r\n",i);
terr=terr+error;        
toterr = toterr + error;

}
printf("\r\t\t\tTesting port :     \r");
}
printf("\nTotal Loops:%lu\r\n",tcnt);
printf("Total Errors:%lu\r\n",terr);

}
void btest()
{



char buffer[4096];
char buffer2[4096];
struct serocco_setup settings;
unsigned i,j,k,lp;
unsigned bt;
unsigned error;
unsigned long terr;
unsigned long tcnt;
unsigned long xr;
unsigned ports[255];
unsigned maxport;
maxport = esccdrv.get_max_port();
esccdrv.get_user_port_list(ports);


terr  = 0;
tcnt = 0;

printf("BISYNC\r\n");
//bisync settings

			settings.gcmdr   =0x0000;
			settings.gmode   =0x003a;
			settings.gpdirl  =0x0006;
			settings.gpdirh  =0x00bf;
			settings.gpdatl  =0x0000;
			settings.gpdath  =0x0000;
			settings.gpiml   =0x0007;
			settings.gpimh   =0x00ff;
			settings.dcmdr   =0x0000;
			settings.dimr    =0x0000;
			settings.cmdrl   =0x0000;
			settings.cmdrh   =0x0000;
			settings.ccr0l   =0x0018;
			settings.ccr0h   =0x0082;
			settings.ccr1l   =0x0002;
			settings.ccr1h   =0x0010;
			settings.ccr2l   =0x000c;
			settings.ccr2h   =0x0000;
			settings.ccr3l   =0x0088;
			settings.ccr3h   =0x0013;
			settings.preamb  =0x0000;
			settings.tolen   =0x0000;
			settings.accm0   =0x0000;
			settings.accm1   =0x0000;
			settings.accm2   =0x0000;
			settings.accm3   =0x0000;
			settings.udac0   =0x0000;
			settings.udac1   =0x0000;
			settings.udac2   =0x0000;
			settings.udac3   =0x0000;
			settings.ttsa0   =0x0000;
			settings.ttsa1   =0x0000;
			settings.ttsa2   =0x0000;
			settings.ttsa3   =0x0000;
			settings.rtsa0   =0x0000;
			settings.rtsa1   =0x0000;
			settings.rtsa2   =0x0000;
			settings.rtsa3   =0x0000;
			settings.pcmtx0  =0x0000;
			settings.pcmtx1  =0x0000;
			settings.pcmtx2  =0x0000;
			settings.pcmtx3  =0x0000;
			settings.pcmrx0  =0x0000;
			settings.pcmrx1  =0x0000;
			settings.pcmrx2  =0x0000;
			settings.pcmrx3  =0x0000;
			settings.brrl    =0x000f;//9830000/(15+1)*2^2 ~153.5k
			settings.brrh    =0x0002;
			settings.timer0  =0x00ff;
			settings.timer1  =0x0007;
			settings.timer2  =0x0000;
			settings.timer3  =0x0000;
			settings.xad1    =0x0000;
			settings.xad2    =0x0000;
			settings.ral1    =0x0000;
			settings.rah1    =0x0000;
			settings.ral2    =0x0000;
			settings.rah2    =0x0000;
			settings.amral1  =0x0000;
			settings.amrah1  =0x0000;
			settings.amral2  =0x0000;
			settings.amrah2  =0x0000;
			settings.rlcrl   =0x0000;
			settings.rlcrh   =0x0000;
			settings.xon     =0x0000;
			settings.xoff    =0x0000;
			settings.mxon    =0x0000;
			settings.mxoff   =0x0000;
			settings.tcr     =0x00ff;
			settings.ticr    =0x0000;
			settings.imr0    =0x0083;
			settings.imr1    =0x0003;
			settings.imr2    =0x00fd;
			settings.syncl   =0x0000;
			settings.synch   =0x005e;

for(i=0;i<4;i++)
{
j = esccdrv.init_port(ports[i],OPMODE_BISYNC,&settings,10,10);
//if(j==TRUE)printf("Initializing port 0 OK\n\r");
if(j==FALSE)printf("Initializing port %d FAILED\n\r",i);
esccdrv.set_tx_type(ports[i],TRANSPARENT_MODE);
esccdrv.clear_rx_buffer(ports[i]);
esccdrv.clear_tx_buffer(ports[i]);
}

for(lp=0;lp<50;lp++)
{
printf("\t\t\tTesting port :");
for(i=0;i<4;i++)
{
printf("%d",i);
error = 0;
//send a frame of data out port a

k = 0;
//0xff is the termination char, so send 0-254 in frame
for(bt=0;bt<4;bt++)for(j=0;j<255;j++)buffer[k++] = j;     
esccdrv.tx_port(ports[i],buffer,k);
//printf("sending %u bytes\r\n",k);
j = 0;
/*
while( (j&RX_READY) !=RX_READY )
		{
		j = esccdrv.get_port_status(porta);
		printf ("waiting...%4.4x\r",j);
		if(j!=0) 
			{                                                
			printf("\n\r");
			if((j&XMR_INTERRUPT)==XMR_INTERRUPT) printf("XMR\n\r");
			if((j&PCE_INTERRUPT)==PCE_INTERRUPT) printf("PCE\n\r");
			if((j&EXE_INTERRUPT)==EXE_INTERRUPT) printf("EXE\n\r");
			if((j&RFO_INTERRUPT)==RFO_INTERRUPT) printf("RFO\n\r");
			if((j&CTS_INTERRUPT)==CTS_INTERRUPT) printf("CTS\n\r");
			if((j&RFS_INTERRUPT)==RFS_INTERRUPT) printf("RFS\n\r");
			if((j&RX_BUFFER_OVERFLOW)==RX_BUFFER_OVERFLOW) printf("RX_BUFFER_OVERFLOW\n\r");
			if((j&RSC_INTERRUPT)==RSC_INTERRUPT) printf("RSC\n\r");
	    if((j&TIMER_INTERRUPT)==TIMER_INTERRUPT) printf("TIMER\n\r");
	    if((j&RX_READY)==RX_READY) printf("RX READY\n\r");
	    if((j&PARITY_ERROR)==PARITY_ERROR) printf("PARITY ERROR\n\r");
	    if((j&SYN_DETECTED)==SYN_DETECTED) printf("SYN_DETECTED\n\r");
	    if((j&ALLSENT_INTERRUPT)==ALLSENT_INTERRUPT) printf("ALLSENT INTERRUPT\n\r");
			}
		if(kbhit()!=0) return;
		}
printf("\n\r");
*/

while((j = esccdrv.rx_port(ports[i],buffer2,4096))==0);
k = 0;
if(j==1021)
{
for(bt=0;bt<j-1;bt++)//last 3 bytes are crc, and termin char
	{
//      printf("Out:%x  In:%x \r",buffer[i]&0xff,buffer2[k]&0xff);
	if((buffer2[bt]&0xff)!=(buffer[k]&0xff))
		{
		//error
		printf("\r\nERROR in LOOPBACK[%x:%x,%x]\r\n",buffer2[bt]&0xff,buffer[k]&0xff,bt);
		error++;
		}
	k++;
	}
}
else error++;
//printf("\r\n");       

//printf("%u bytes received--%u bytes expected\n\r",j,1023);
//if(error==0) printf("Port 0 passed\r\n");
if(error>0) printf("Port %d failed\r\n",i);
terr = terr +error;     
toterr = toterr + error;
}
printf("\r\t\t\tTesting port :     \r");
}
printf("\nTotal Loops:%lu\r\n",tcnt);
printf("Total Errors:%lu\r\n",terr);

}




void htest()
{


//hdlc test
char buffer[4096];
char buffer2[4096];
struct serocco_setup settings;
unsigned i,j,k,lp;
unsigned bt;
unsigned error;
unsigned long terr;
unsigned long tcnt;
unsigned ports[255];
unsigned maxport;
maxport = esccdrv.get_max_port();
esccdrv.get_user_port_list(ports);


terr  = 0;
tcnt = 0;



printf("HDLC\r\n");

//HDLC settings

			settings.gcmdr   =0x0000;
			settings.gmode   =0x003a;
			settings.gpdirl  =0x0006;
			settings.gpdirh  =0x00bf;
			settings.gpdatl  =0x0000;
			settings.gpdath  =0x0000;
			settings.gpiml   =0x0007;
			settings.gpimh   =0x00ff;
			settings.dcmdr   =0x0000;
			settings.dimr    =0x0000;
			settings.cmdrl   =0x0000;
			settings.cmdrh   =0x0000;
			settings.ccr0l   =0x0018;
			settings.ccr0h   =0x0080;
			settings.ccr1l   =0x0002;
			settings.ccr1h   =0x0000;
			settings.ccr2l   =0x0080;
			settings.ccr2h   =0x0000;
			settings.ccr3l   =0x0008;
			settings.ccr3h   =0x0000;
			settings.preamb  =0x0000;
			settings.tolen   =0x0000;
			settings.accm0   =0x0000;
			settings.accm1   =0x0000;
			settings.accm2   =0x0000;
			settings.accm3   =0x0000;
			settings.udac0   =0x0000;
			settings.udac1   =0x0000;
			settings.udac2   =0x0000;
			settings.udac3   =0x0000;
			settings.ttsa0   =0x0000;
			settings.ttsa1   =0x0000;
			settings.ttsa2   =0x0000;
			settings.ttsa3   =0x0000;
			settings.rtsa0   =0x0000;
			settings.rtsa1   =0x0000;
			settings.rtsa2   =0x0000;
			settings.rtsa3   =0x0000;
			settings.pcmtx0  =0x0000;
			settings.pcmtx1  =0x0000;
			settings.pcmtx2  =0x0000;
			settings.pcmtx3  =0x0000;
			settings.pcmrx0  =0x0000;
			settings.pcmrx1  =0x0000;
			settings.pcmrx2  =0x0000;
			settings.pcmrx3  =0x0000;
			settings.brrl    =0x000f;
			settings.brrh    =0x0002;
			settings.timer0  =0x00ff;
			settings.timer1  =0x003f;
			settings.timer2  =0x0000;
			settings.timer3  =0x0000;
			settings.xad1    =0x00ff;
			settings.xad2    =0x00ff;
			settings.ral1    =0x00ff;
			settings.rah1    =0x00ff;
			settings.ral2    =0x00ff;
			settings.rah2    =0x00ff;
			settings.amral1  =0x0000;
			settings.amrah1  =0x0000;
			settings.amral2  =0x0000;
			settings.amrah2  =0x0000;
			settings.rlcrl   =0x0000;
			settings.rlcrh   =0x0000;
			settings.xon     =0x0000;
			settings.xoff    =0x0000;
			settings.mxon    =0x0000;
			settings.mxoff   =0x0000;
			settings.tcr     =0x0000;
			settings.ticr    =0x0000;
			settings.imr0    =0x0001;
			settings.imr1    =0x0003;
			settings.imr2    =0x00fd;
			settings.syncl   =0x0000;
			settings.synch   =0x0000;


for(i=0;i<4;i++)
{
j = esccdrv.init_port(ports[i],OPMODE_HDLC,&settings,10,10);
//if(j==TRUE)printf("Initializing port 1 OK\n\r");
if(j==FALSE)printf("Initializing port %d FAILED\n\r",i);
esccdrv.set_tx_type(ports[i],TRANSPARENT_MODE);
esccdrv.clear_rx_buffer(ports[i]);
esccdrv.clear_tx_buffer(ports[i]);
}

for(lp=0;lp<50;lp++)
{
printf("\t\t\tTesting port :");
for(i=0;i<4;i++)
{
printf("%d",i);


error = 0;

//send a frame of data out port a
for(j=0;j<1024;j++)buffer[j] = j;     
j = esccdrv.tx_port(ports[i],buffer,j);
//printf("sending %u bytes\r\n",j);
j = 0;
/*
while( (j&RX_READY) !=RX_READY )
		{
		j = esccdrv.get_port_status(porta);
		printf ("waiting...%4.4x\r",j);
		if(j!=0) 
			{                                                
			printf("\n\r");
			if((j&XMR_INTERRUPT)==XMR_INTERRUPT) printf("XMR\n\r");
			if((j&PCE_INTERRUPT)==PCE_INTERRUPT) printf("PCE\n\r");
			if((j&EXE_INTERRUPT)==EXE_INTERRUPT) printf("EXE\n\r");
			if((j&RFO_INTERRUPT)==RFO_INTERRUPT) printf("RFO\n\r");
			if((j&CTS_INTERRUPT)==CTS_INTERRUPT) printf("CTS\n\r");
			if((j&RFS_INTERRUPT)==RFS_INTERRUPT) printf("RFS\n\r");
			if((j&RX_BUFFER_OVERFLOW)==RX_BUFFER_OVERFLOW) printf("RX_BUFFER_OVERFLOW\n\r");
			if((j&RSC_INTERRUPT)==RSC_INTERRUPT) printf("RSC\n\r");
	    if((j&TIMER_INTERRUPT)==TIMER_INTERRUPT) printf("TIMER\n\r");
	    if((j&RX_READY)==RX_READY) printf("RX READY\n\r");
	    if((j&PARITY_ERROR)==PARITY_ERROR) printf("PARITY ERROR\n\r");
	    if((j&FRAMING_ERROR)==FRAMING_ERROR) printf("FRAMING ERROR\n\r");
	    if((j&BREAK_DETECTED)==BREAK_DETECTED) printf("BREAK DETECTED\n\r");
	    if((j&BREAK_TERMINATED)==BREAK_TERMINATED) printf("BREAK TERMINATED\n\r");
	    if((j&ALLSENT_INTERRUPT)==ALLSENT_INTERRUPT) printf("ALLSENT INTERRUPT\n\r");
			}
		//if(kbhit()!=0) return;
		}
printf("\n\r");

*/
while((j = esccdrv.rx_port(ports[i],buffer2,4096))==0);
if(j==1025)
{
for(bt=0;bt<j-1;bt++)
	{
	//printf("Out:%x  In:%x \r",buffer[i]&0xff,buffer2[i]&0xff);
	if((buffer2[bt]&0xff)!=(buffer[bt]&0xff))
		{
		//error
		printf("\r\nERROR in LOOPBACK[%x:%x,%x]\r\n",buffer2[bt]&0xff,buffer[bt]&0xff,bt);
		error++;
		}
	}
}
else
{
printf("sent:%d, received:%d\n",1024,j);
 error++;
}

//printf("%u bytes received--%u bytes expected\n\r",j,1025);
//if(error==0) printf("Port 0 passed\r\n");
if(error>0) printf("Port %d failed\r\n",i);
terr = terr + error;    
toterr = toterr + error;
}
printf("\r\t\t\tTesting port :     \r");
}
printf("\nTotal Loops:%lu\r\n",tcnt);
printf("Total Errors:%lu\r\n",terr);
}

void cltest()
{

unsigned porta;
unsigned portb;
void (interrupt *oldvect1)();    //the old interrupt vector temp storage for timer

//hdlc test
char buffer[4096];
char buffer2[4096];
struct serocco_setup settings;
unsigned i,j,k,lp;
unsigned error;
unsigned long terr;
unsigned long tcnt;
unsigned long timeout = 0;
unsigned ports[255];
unsigned maxport;
maxport = esccdrv.get_max_port();
esccdrv.get_user_port_list(ports);

porta = ports[0];
portb = ports[1];

terr  = 0;
tcnt = 0;
error = 0;

oldvect1 = _dos_getvect(0x70);                //get the Real Time Clock interrupt vector (IRQ 8)
_dos_setvect(0x70,timer_isr);           //set to our routine
i = _inp(0xa1);                     //Get 8259 PIC interrupt mask
i = i &0xfe;                                            //unmask the IRQ 8 interrupt
_outp(0xa1,i);                                          //update mask


//Set real time clock to interrupt 16 times per second
_outp(0x70,0x0a);       //set real time clock address
_outp(0x71,0x2c);       //set 16 Hz rate
_outp(0x70,0x0b);       //set real time clock address
_outp(0x71,inp(0x71)|0x40);     //enable RTC interrupt






/*
esccdrv.set_clock_generator(porta,0x5d2c60,23);
printf("CLOCK = 2M\r\n");
getch();
//2M    0x5d2c60 (23 bits)
esccdrv.set_clock_generator(porta,0x5d2460,23);
printf("CLOCK = 4M\r\n");
getch();
//4M  0x5d2460 (23 bits)
esccdrv.set_clock_generator(porta,0xba1c60,24);
printf("CLOCK = 8M\r\n");
getch();
//8M  0xba1c60  (24 bits)
esccdrv.set_clock_generator(porta,0x5db2d8,23);
printf("CLOCK = 1.544M\r\n");
getch();
//1.544M  0x5db2d8 (23 bits)
esccdrv.set_clock_generator(porta,0x16a990,22);
printf("CLOCK = 2.048M\r\n");
getch();
//2.048M  0x16a990 (22 bits)
//use bitcalc to come up with others pay attention to how many bits are displayed in the
//stuffed bits field (to use in the -nb: field
*/
/*
_clearscreen(_GCLEARSCREEN);
printf("FASTCOM:ESCC LOOPTST -- LOOPBACK FUNCTIONAL TESTER\r\n");
printf("baudrate divisor:%u",bdrt);
printf("\r\n");
*/
printf("CLOCK TEST\r\n");

//HDLC settings

			settings.gcmdr   =0x0000;
			settings.gmode   =0x003a;
			settings.gpdirl  =0x0006;
			settings.gpdirh  =0x00bf;
			settings.gpdatl  =0x0000;
			settings.gpdath  =0x0000;
			settings.gpiml   =0x0007;
			settings.gpimh   =0x00ff;
			settings.dcmdr   =0x0000;
			settings.dimr    =0x0000;
			settings.cmdrl   =0x0000;
			settings.cmdrh   =0x0000;
			settings.ccr0l   =0x0018;
			settings.ccr0h   =0x0080;
			settings.ccr1l   =0x0002;
			settings.ccr1h   =0x0000;
			settings.ccr2l   =0x0080;
			settings.ccr2h   =0x0000;
			settings.ccr3l   =0x0008;
			settings.ccr3h   =0x0000;
			settings.preamb  =0x0000;
			settings.tolen   =0x0000;
			settings.accm0   =0x0000;
			settings.accm1   =0x0000;
			settings.accm2   =0x0000;
			settings.accm3   =0x0000;
			settings.udac0   =0x0000;
			settings.udac1   =0x0000;
			settings.udac2   =0x0000;
			settings.udac3   =0x0000;
			settings.ttsa0   =0x0000;
			settings.ttsa1   =0x0000;
			settings.ttsa2   =0x0000;
			settings.ttsa3   =0x0000;
			settings.rtsa0   =0x0000;
			settings.rtsa1   =0x0000;
			settings.rtsa2   =0x0000;
			settings.rtsa3   =0x0000;
			settings.pcmtx0  =0x0000;
			settings.pcmtx1  =0x0000;
			settings.pcmtx2  =0x0000;
			settings.pcmtx3  =0x0000;
			settings.pcmrx0  =0x0000;
			settings.pcmrx1  =0x0000;
			settings.pcmrx2  =0x0000;
			settings.pcmrx3  =0x0000;
			settings.brrl    =0x0017;
			settings.brrh    =0x0001;
			settings.timer0  =0x00ff;
			settings.timer1  =0x0001;
			settings.timer2  =0x0000;
			settings.timer3  =0x0007;
			settings.xad1    =0x00ff;
			settings.xad2    =0x00ff;
			settings.ral1    =0x00ff;
			settings.rah1    =0x00ff;
			settings.ral2    =0x00ff;
			settings.rah2    =0x00ff;
			settings.amral1  =0x0000;
			settings.amrah1  =0x0000;
			settings.amral2  =0x0000;
			settings.amrah2  =0x0000;
			settings.rlcrl   =0x0000;
			settings.rlcrh   =0x0000;
			settings.xon     =0x0000;
			settings.xoff    =0x0000;
			settings.mxon    =0x0000;
			settings.mxoff   =0x0000;
			settings.tcr     =0x0000;
			settings.ticr    =0x0000;
			settings.imr0    =0x0001;
			settings.imr1    =0x0003;
			settings.imr2    =0x00fd;
			settings.syncl   =0x0000;
			settings.synch   =0x0000;
for(i=0;i<4;i++)
{
j = esccdrv.init_port(ports[i],OPMODE_HDLC,&settings,10,10);
//if(j==TRUE)printf("Initializing port 1 OK\n\r");
if(j==FALSE)printf("Initializing port %d FAILED\n\r",i);
esccdrv.set_tx_type(ports[i],TRANSPARENT_MODE);
}

//clock is always 9.830 MHz, on the QSSB
//printf("CLOCK = 1M\r\n");
//M=1,N=23, div=48, rate = 204791bps ~4.88uS/clock
//timer fires every 512 clocks ~.0025seconds, or 400/sec, updated every 16hz that gives a timercount value of 25
timeout = 0;
timertick = 0;
esccdrv.start_timer(porta);                    
RTCcount = 0;

while(RTCcount < 8)printf("RT:%lu\r",RTCcount);
//printf("\r\n");
if((timertick>26)||(timertick<24))
	{
	printf("timertick = %lu, should be in range 24-26\r\n",timertick);
	error++;
	}                                   
printf("Timertick:%lu\r\n",timertick);	
esccdrv.stop_timer(porta);

	settings.brrl    =0x0017;
	settings.brrh    =0x0002;
for(i=0;i<4;i++)
{
j = esccdrv.init_port(ports[i],OPMODE_HDLC,&settings,10,10);
//if(j==TRUE)printf("Initializing port 1 OK\n\r");
if(j==FALSE)printf("Initializing port %d FAILED\n\r",i);
esccdrv.set_tx_type(ports[i],TRANSPARENT_MODE);
}
//M=2,N=23, div=90, rate = 109222bps ~9.155uS/clock
//timer fires every 512 clocks ~.0047seconds, or 213/sec, updated every 16hz that gives a timercount value of 13
	

timeout = 0;
timertick = 0;
esccdrv.start_timer(porta);                    
RTCcount = 0;
while(RTCcount < 8)printf("RT:%lu\r",RTCcount);
//printf("\r\n");
if((timertick>14)||(timertick<12))
	{
	
	printf("timertick = %lu, should be in range 12-14\r\n",timertick);
	error++;              
	}
printf("Timertick:%lu\r\n",timertick);	
esccdrv.stop_timer(porta);

	settings.brrl    =0x0017;
	settings.brrh    =0x0003;
for(i=0;i<4;i++)
{
j = esccdrv.init_port(ports[i],OPMODE_HDLC,&settings,10,10);
//if(j==TRUE)printf("Initializing port 1 OK\n\r");
if(j==FALSE)printf("Initializing port %d FAILED\n\r",i);
esccdrv.set_tx_type(ports[i],TRANSPARENT_MODE);
}
//M=3,N=23, div=192, rate = 51197.9bps ~19.53uS/clock
//timer fires every 512 clocks ~.010seconds, or 100/sec, updated every 16hz that gives a timercount value of 6.24



timeout = 0;
timertick = 0;
esccdrv.start_timer(porta);                    
RTCcount = 0;
while(RTCcount < 8)printf("RT:%lu\r",RTCcount);
//printf("\r\n");
if((timertick>7)||(timertick<5))
	{
	
	printf("timertick = %lu, should be in range 5-7\r\n",timertick);
	error++;              
	}
printf("Timertick:%lu\r\n",timertick);	
esccdrv.stop_timer(porta);

//if(error==0) printf("Clock Test passed\r\n");
if(error>0) printf("Clock Test failed\r\n");
toterr = toterr + error;

_outp(0x70,0x0b);       //set real time clock address
_outp(0x71,inp(0x71)&0xBF);     //disable RTC interrupt
i = _inp(0xa1);                     //Get 8259 PIC interrupt mask
i = i | 0x01;                                            //mask the IRQ 8 interrupt
_outp(0xa1,i);                                          //update mask

_dos_setvect(0x70,oldvect1);           //set original routine

}


//timer interrupt service routine
void interrupt timer_isr()      
{
//if the source of the interrupt is the periodic interrupt then update the timertick variable
outp(0x70,0x0c);//set status c

if((inp(0x71)&0x40)==0x40)
	{
	timertick = tincount;
	tincount = 0;
	
	}
RTCcount++;     
//issue end of interrupt
_outp(0x20,0x20);//lower EOI
_outp(0xa0,0x20);//upper EOI
}                          
