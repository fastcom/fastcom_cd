The IOCTL called in this example program to enable time tagging only exists in the
Windows 2000 and XP driver.  It is not available in NT.  See the timetag.c example
for an explanation of the use of this function.