Attribute VB_Name = "Module1"

Public Type ESCCSETUP
 cmdr As Long
 mode As Long
 timr As Long
 xbcl As Long
 xbch As Long
 ccr0 As Long
 ccr1 As Long
 ccr2 As Long
 ccr3 As Long
 ccr4 As Long
 tsax As Long
 tsar As Long
 xccr As Long
 rccr As Long
 bgr As Long
 iva As Long
 ipc As Long
 imr0 As Long
 imr1 As Long
 pvr As Long
 pim As Long
 pcr As Long
 xad1 As Long
 xad2 As Long
 rah1 As Long
 rah2 As Long
 ral1 As Long
 ral2 As Long
 rlcr As Long
 aml As Long
 amh As Long
 pre As Long
 xon As Long
 xoff As Long
 tcr As Long
 dafo As Long
 rfc As Long
 tic As Long
 mxn As Long
 mxf As Long
 synl As Long
 synh As Long
 n_rbufs As Long
 n_tbufs As Long
 n_rfsize_max As Long
 n_tfsize_max As Long
End Type
Public Declare Function ESCCToolBox_Create Lib "escctoolbox.dll" Alias "#8" (ByVal port As Long) As Long
Public Declare Function ESCCToolBox_Destroy Lib "escctoolbox.dll" Alias "#9" (ByVal port As Long) As Long
Public Declare Function ESCCToolBox_Set_Clock Lib "escctoolbox.dll" Alias "#24" (ByVal port As Long, ByVal frequency As Long) As Long
Public Declare Function ESCCToolBox_Get_Clock Lib "escctoolbox.dll" Alias "#12" (ByVal port As Long) As Long
Public Declare Function ESCCToolBox_Read_Frame Lib "escctoolbox.dll" Alias "#18" (ByVal port As Long, ByRef rbuf As Byte, ByVal szrbuf As Long, ByRef bytesreturned As Long, ByVal timeout As Long) As Long
Public Declare Function ESCCToolBox_Write_Frame Lib "escctoolbox.dll" Alias "#30" (ByVal port As Long, ByRef tbuf As Byte, ByVal numbytes As Long, ByVal timeout As Long) As Long
Public Declare Function ESCCToolBox_Get_Status Lib "escctoolbox.dll" Alias "#14" (ByVal port As Long, ByRef status As Long, ByVal mask As Long, ByVal timeout As Long) As Long
Public Declare Function ESCCToolBox_Get_IStatus Lib "escctoolbox.dll" Alias "#13" (ByVal port As Long, ByRef status As Long) As Long
Public Declare Function ESCCToolBox_Flush_RX Lib "escctoolbox.dll" Alias "#10" (ByVal port As Long) As Long
Public Declare Function ESCCToolBox_Flush_TX Lib "escctoolbox.dll" Alias "#11" (ByVal port As Long) As Long
Public Declare Function ESCCToolBox_Read_Register Lib "escctoolbox.dll" Alias "#19" (ByVal port As Long, ByVal regno As Long) As Long
Public Declare Function ESCCToolBox_Write_Register Lib "escctoolbox.dll" Alias "#31" (ByVal port As Long, ByVal regno As Long, ByVal value As Long) As Long
Public Declare Function ESCCToolBox_Configure Lib "escctoolbox.dll" Alias "#7" (ByVal port As Long, ByRef settings As ESCCSETUP) As Long
Public Declare Function ESCCToolBox_Set_Txclk_TT Lib "escctoolbox.dll" Alias "#29" (ByVal port As Long, ByVal onoff As Long) As Long
Public Declare Function ESCCToolBox_Set_Txclk_ST Lib "escctoolbox.dll" Alias "#28" (ByVal port As Long, ByVal onoff As Long) As Long
Public Declare Function ESCCToolBox_Set_SD_485 Lib "escctoolbox.dll" Alias "#26" (ByVal port As Long, ByVal onoff As Long) As Long
Public Declare Function ESCCToolBox_Set_TT_485 Lib "escctoolbox.dll" Alias "#27" (ByVal port As Long, ByVal onoff As Long) As Long
Public Declare Function ESCCToolBox_Set_RD_Echo_Cancel Lib "escctoolbox.dll" Alias "#25" (ByVal port As Long, ByVal onoff As Long) As Long


Rem these still need converted
Rem ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Bisync_Start_Pattern_Enable(DWORD port,DWORD onoff);
Rem ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Bisync_Cutoff_Enable(DWORD port,DWORD onoff);
Rem ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_LSB2MSB_Convert_Enable(DWORD port,DWORD onoff);
Rem ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Bisync_End_Pattern_Enable(DWORD port,DWORD onoff);
Rem ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Bisync_Start_Pattern(DWORD port,struct bisync_start_pattern pattern);
Rem ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Bisync_Size_Cutoff(DWORD port,DWORD size);
Rem ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Bisync_End_Pattern(DWORD port,struct bisync_start_pattern pattern);

Rem ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Postamble_Enable(DWORD port,DWORD onoff);
Rem ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Postamble_Delay(DWORD port,DWORD delay);

