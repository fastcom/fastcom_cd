/* $Id$ */
/*
Copyright(c) 2006,2007 Commtech, Inc.
setfeatures.c -- user mode function to set current board specific features configuration using the escctoolbox.dll

To use the escctoolbox.DLL, you must do three things:

1. #include escctoolbox.h in your source code
2. Add the file escctoolbox.lib to your linker's Output/Library modules.  To do this in
   Visual Studio, Click on Settings in the Project menu.  In the Project settings dialog
   click on the Link tab.  In the Category drop down, choose General.  In the 
   Output/library modules box, add the path to escctoolbox.lib.  It is easiest to copy the 
   file to your working directory and just add "escctoolbox.lib"
3. The escctoolbox.dll file must reside in the same directory as your compiled program .exe

usage:
 getfeatures port 

 The port can be any valid escc port (0,1,2...)

*/

#include "windows.h"
#include "..\escctoolbox.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"


int main(int argc, char *argv[])
{
	unsigned long port;
	long ret;
	ULONG rxecho;
	ULONG sd485;
	ULONG tt485;
	ULONG ctsdisable;
	ULONG sttxclk;
	ULONG tttxclk;
	ULONG busmode;

	if(argc!=9)
	{
		printf("usage:%s port rxecho sd485 tt485 ctsdisable sttxclk tttxclk busmode\r\n",argv[0]);
		exit(1);
	}
	port = atoi(argv[1]);
	rxecho = atol(argv[2]);
	sd485 = atol(argv[3]);
	tt485 = atol(argv[4]);
	ctsdisable = atol(argv[5]);
	sttxclk = atol(argv[6]);
	tttxclk = atol(argv[7]);
	busmode = atol(argv[8]);
		
	if((ret = ESCCToolBox_Create(port))<0)
	{
		printf("create:%d\n",ret);
		exit(1);
	}
	if((ret= ESCCToolBox_Set_Features(port,rxecho,sd485,tt485,ctsdisable,sttxclk,tttxclk,busmode))<0)
	{
		printf("error in setfeatures:%d\n",ret);
	}
	else
	{
	if(rxecho==1)     printf("Receive Echo Cancel: ON\n");
	else              printf("Receive Echo Cancel: OFF\n");
	if(ctsdisable==1) printf("CTS Disable        : ON\n");
	else              printf("CTS Disable        : OFF\n");
	if(sd485==1)      printf("SD is RS-485\n");
	else              printf("SD is RS-422\n");
	if(tt485==1)      printf("TT is RS-485\n");
	else              printf("TT is RS-422\n");
	if(sttxclk==1)    printf("TXCLK is an input (ST)\n");
	else              printf("ST not connected\n");
	if(tttxclk==1)    printf("TXCLK is an output (TT)\n");
	else              printf("TT not connected\n");
	if(busmode==1)    printf("BUS mode 1\n");
	else if(busmode==2) printf("BUS mode 2\n");
	else              printf("Normal RS-422/RS-485 Mode\n");

	}
	
	ESCCToolBox_Destroy(port);
}
/* $Id$ */