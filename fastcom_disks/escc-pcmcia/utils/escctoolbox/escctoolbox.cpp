// escctoolbox.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "stdlib.h"
#include "string.h"
#include "stdio.h"
#include "escctoolbox.h"

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
			break;
		case DLL_THREAD_ATTACH:
			break;
		case DLL_THREAD_DETACH:
			break;
		case DLL_PROCESS_DETACH:
			break;
    }
    return TRUE;
}


ULONG refcount = 0;

readcallbackfn readcallbacks[MAXPORTS]= {0};
void (__stdcall *writecallbacks[MAXPORTS])(DWORD port,char * tbuf,DWORD *bytestowrite)= {NULL};
void (__stdcall *statuscallbacks[MAXPORTS])(DWORD port,DWORD status)= {NULL};

static HANDLE hESCC[MAXPORTS] = {NULL};
BOOL connected[MAXPORTS] = {0};
DWORD statusmask[MAXPORTS] = {0};
DWORD readsize[MAXPORTS] = {0};
DWORD maxwritesize[MAXPORTS] = {0};
HANDLE hReadThread[MAXPORTS] = {NULL};
HANDLE hWriteThread[MAXPORTS] = {NULL};
HANDLE hStatusThread[MAXPORTS] = {NULL};
static DWORD refcnt[MAXPORTS] = {0};
HMODULE mymod[MAXPORTS]= {NULL};




DWORD FAR PASCAL StatusProc( DWORD port );
DWORD FAR PASCAL ReadProc( DWORD port );
DWORD FAR PASCAL WriteProc( DWORD port );



// Function name	: ESCCToolBox_Create
// Description	    : Opens handle to the ESCC port specified in the port variable
// Return type		: DWORD --return status
// Argument         : DWORD port --port number ESCC#
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Create(DWORD port)
{
	char devname[25];
	ULONG onoff;
	ULONG temp;
	
	if(port>MAXPORTS) return -1;	//exceeded number of allowable ports
	if(hESCC[port]!=NULL) return -2;	//port already open
	
	sprintf(devname,"\\\\.\\ESCC%d",port);
	
    hESCC[port] = CreateFile (devname,
		GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL,
		OPEN_ALWAYS,
		FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
		NULL
		);
	
    if (hESCC[port] == INVALID_HANDLE_VALUE)
    {
		//	MessageBox(NULL,"create failed\n","",MB_OK);
		return -3;	//something bad happened
	}
	//else MessageBox(NULL,"Create success\n","",MB_OK);
	onoff=0;
	DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_BLOCK_MULTIPLE_IO,&onoff,sizeof(ULONG),NULL,0,&temp,NULL);
	//sets the driver to allow a particular port to be opened in more than one handle
	
	return 0;
	
}


// Function name	: ESCCToolBox_Destroy
// Description	    : Stops and closes the status, write and read threads and closes
//					  handle to specified port
// Return type		: DWORD --return status
// Argument         : DWORD port --port number i.e. ESCC#
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Destroy(DWORD port)
{
	DWORD ret;
	if(port>MAXPORTS) return -1;
	if(hESCC[port]==NULL) return -2;
	
	
	connected[port]=0;	//will cause any threads running on this port to stop

	if(hReadThread[port]!=NULL)
	{
		DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_CANCEL_RX,NULL,0,NULL,0,&ret,NULL);
		//causes a blocked ReadFile to return canceled
		Sleep(0);
		CloseHandle(hReadThread[port]);
	}
	if(hWriteThread[port]!=NULL)
	{
		DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_CANCEL_TX,NULL,0,NULL,0,&ret,NULL);
		//causes a blocked WriteFile to return canceled
		Sleep(0);
		CloseHandle(hWriteThread[port]);
	}
	if(hStatusThread[port]!=NULL)
	{
		DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_CANCEL_STATUS,NULL,0,NULL,0,&ret,NULL);
		//causes a blocked status request to return canceled
		Sleep(0);
		CloseHandle(hStatusThread[port]);
	}
	CloseHandle(hESCC[port]);
	hReadThread[port]=NULL;
	hWriteThread[port]=NULL;
	hStatusThread[port]=NULL;
	hESCC[port]=NULL;
	return 0;
	
}


// Function name	: ESCCToolBox_Set_Clock
// Description	    : Sets the programmable clock generator (ICS307) that supplies
//					  the OSC clock input to the 82532
// Return type		: DWORD --return status
// Argument         : DWORD port --port number i.e. ESCC#
// Argument         : DWORD frequency --desired clock generator frequency in Hz
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Set_Clock(DWORD port,DWORD frequency)
{
	DWORD temp;
	ULONG actualf;
	ULONG passval[2];
	
	if(port>MAXPORTS) return -1;
	if(hESCC[port]==NULL) return -2;
	
	passval[0] = frequency;
	if(	DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_SET_FREQ,&frequency,sizeof(ULONG),&actualf,sizeof(ULONG),&temp,NULL)!=0)
	{
		return actualf;
	}
	else return -3;
	return -4;
}


// Function name	: ESCCToolBox_Get_Clock
// Description	    : Returns the frequency of the clock generator
// Return type		: DWORD --return status
// Argument         : DWORD port --port number i.e. ESCC#
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Get_Clock(DWORD port)
{
	DWORD res;
	ULONG passval;
	
	if(port>MAXPORTS) return -1;
	if(hESCC[port]==NULL) return -2;
	if(DeviceIoControl(hESCC[port], IOCTL_ESCCDRV_GET_FREQ,NULL,0,&passval,sizeof(ULONG),&res,NULL)!=0)
	{
		return (DWORD)(passval);
	}
	else return -3;
}


// Function name	: ESCCToolBox_Read_Frame
// Description	    : Standard read routing that does not callback
// Return type		: ESCCTOOLBOX_API DWORD 
// Argument         : DWORD port --ESCC# port number
// Argument         : char * rbuf --pointer to user variable where data is stored
// Argument         : DWORD szrbuf --NumBytesToRead
// Argument         : DWORD *retbytes --NumbBytesRead
// Argument         : DWORD timeout --number of miliseconds for WaitForSingleObject
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Read_Frame(DWORD port,char * rbuf,DWORD szrbuf,DWORD *retbytes,DWORD timeout)
{
	ULONG t;
	OVERLAPPED  rq;
	int j;
	ULONG temp;
	
	if(port>MAXPORTS) return -1;
	if(hESCC[port]==NULL) return -2;
	
	memset( &rq, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
	rq.hEvent = CreateEvent( NULL,TRUE,FALSE,NULL );
	if (rq.hEvent == NULL)
	{
		return -3;
	}
	
	t = ReadFile(hESCC[port],rbuf,szrbuf,retbytes,&rq);
	if(t==FALSE)  
	{
		t=GetLastError();
		if(t==ERROR_IO_PENDING)
		{
			do
			{
				j = WaitForSingleObject( rq.hEvent, timeout );
				if(j==WAIT_TIMEOUT)
				{
					DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_CANCEL_RX,NULL,0,NULL,0,&temp,NULL);
					retbytes[0] = 0;
					CloseHandle(rq.hEvent);
					return -4;
				}
				if(j==WAIT_ABANDONED)
				{
					retbytes[0] = 0;
					CloseHandle(rq.hEvent);
					return -5;
				}
			} while(j!=WAIT_OBJECT_0);
			GetOverlappedResult(hESCC[port],&rq,retbytes,TRUE);
		}
		else 
		{
			//		char buf[256];
			//		sprintf(buf,"%8.8x error:%8.8x\n",hESCC[port],ret);
			//		MessageBox(NULL,buf,"ERROR",MB_OK);
			
			retbytes[0] = 0;
			CloseHandle(rq.hEvent);
			return -6;
		}
	}
	CloseHandle(rq.hEvent);
	return 0;
	
	
}


// Function name	: ESCCToolBox_Write_Frame
// Description	    : 
// Return type		: DWORD --return status
// Argument         : DWORD port --port number i.e. ESCC#
// Argument         : char * tbuf --pointer to user's data
// Argument         : DWORD numbytes
// Argument         : DWORD timeout
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Write_Frame(DWORD port,char * tbuf,DWORD numbytes,DWORD timeout)
{
	OVERLAPPED  wq;
	BOOL t;
	DWORD nobyteswritten;
	DWORD j;
	DWORD ret;
	DWORD temp;
	
	if(port>MAXPORTS) return -1;
	if(hESCC[port]==NULL) return -2;
	
	memset( &wq, 0, sizeof( OVERLAPPED ) );
	wq.hEvent = CreateEvent( NULL,TRUE,FALSE,NULL);
	if (wq.hEvent == NULL)
	{
		return -3;
	}
	
	t = WriteFile(hESCC[port],tbuf,numbytes,&nobyteswritten,&wq);
	if(t==FALSE)  
	{
		ret=GetLastError();
		if(ret==ERROR_IO_PENDING)
		{
			do
			{
				j = WaitForSingleObject( wq.hEvent, timeout );
				if(j==WAIT_TIMEOUT)
				{
					DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_CANCEL_TX,NULL,0,NULL,0,&temp,NULL);
					CloseHandle(wq.hEvent);
					return -4;
				}
				if(j==WAIT_ABANDONED)
				{
					CloseHandle(wq.hEvent);
					return -5;
				}
			} while(j!=WAIT_OBJECT_0);
			GetOverlappedResult(hESCC[port],&wq,&nobyteswritten,TRUE);
		}
		else
		{
			//		char buf[256];
			//		sprintf(buf,"%8.8x error:%8.8x\n",hESCC[port],ret);
			//		MessageBox(NULL,buf,"ERROR",MB_OK);
			CloseHandle(wq.hEvent);
			return -6;
		}
	}
	CloseHandle(wq.hEvent);
	return 0;
	
}


ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Get_Status(DWORD port,DWORD *status,DWORD mask,DWORD timeout)
{
	ULONG t;
	ULONG temp;
	OVERLAPPED  st;
	DWORD j;

status[0]=0;
if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;

memset( &st, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
st.hEvent = CreateEvent( NULL,TRUE,FALSE,NULL);
if (st.hEvent == NULL)
	{
	return -3;
	}


t=	DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_STATUS,&mask,sizeof(ULONG),status,sizeof(ULONG),&temp,&st);
if(t==FALSE)  
	{
	t=GetLastError();
	if(t==ERROR_IO_PENDING)
		{
		do
			{
			j = WaitForSingleObject( st.hEvent, timeout );
			if(j==WAIT_TIMEOUT)
				{
                DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_CANCEL_STATUS,NULL,0,NULL,0,&temp,NULL);
				CloseHandle(st.hEvent);
				return -4;
				}
			if(j==WAIT_ABANDONED)
				{
				CloseHandle(st.hEvent);
				return -5;
				}
			} while(j!=WAIT_OBJECT_0);
		GetOverlappedResult(hESCC[port],&st,&temp,TRUE);		
		}
	else
		{
//		char buf[256];
//		sprintf(buf,"%8.8x error:%8.8x\n",hESCC[port],ret);
//		MessageBox(NULL,buf,"ERROR",MB_OK);

		CloseHandle(st.hEvent);
		return -6;
		}
	}
CloseHandle(st.hEvent);
return 0;
}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Get_IStatus(DWORD port,DWORD *status)
{
BOOL t;
DWORD temp;
DWORD mask = 0xFFFFFFFF;

status[0]=0;
if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;

t = DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_IMMEDIATE_STATUS,&mask,sizeof(ULONG),status,sizeof(ULONG),&temp,NULL);
if(t) return 0;
else return -3;
}


/*
Doesn't work

// Function name	: ESCCToolBox_Register_Read_Callback
// Description	    : Starts a read thread on port with a read size of szread that calls
//					  the user's function fnptr when the ReadFile completes.
// Return type		: DWORD --return status
// Argument         : DWORD port --port number i.e. ESCC#
// Argument         : readcallbackfn fnptr --pointer to user function to be called by read thread after ReadFile completes
// Argument         : DWORD szread --size of ReadFile's nNumberOfBytesToRead 
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Register_Read_Callback(DWORD port,readcallbackfn fnptr,DWORD szread)
{
	
	HANDLE	hreadThread ;	//handle to read thread
	DWORD	dwThreadID ;		//temp Thread ID storage

	if(port>MAXPORTS) return -1;
	if(hESCC[port]==NULL) return -2;

	readsize[port] = szread;
	readcallbacks[port] =  fnptr;
	connected[port]|=1;

	//create read thread here.
	hreadThread =	CreateThread( (LPSECURITY_ATTRIBUTES) NULL,
				    0, 
					(LPTHREAD_START_ROUTINE) ReadProc,
					(LPVOID) port,
					0, &dwThreadID );
	
	if(hreadThread==NULL)
	{
		readcallbacks[port] = NULL;
		connected[port]&=0xFFFFFFFE;
		return -3;
	}

	hReadThread[port] = hreadThread;
	
	return 0;
}
*/
/*	Doesn't Work

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Register_Write_Callback(DWORD port,void (__stdcall *callbackfunction)(DWORD port,char * tbuf,DWORD *bytestowrite),DWORD szmaxwrite)
{
HANDLE            hwriteThread ;	//handle to write thread
DWORD         dwThreadID ;		//temp Thread ID storage

if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;
maxwritesize[port] = szmaxwrite;
writecallbacks[port] = callbackfunction;
connected[port]|=2;
//create write thread here


hwriteThread =     CreateThread( (LPSECURITY_ATTRIBUTES) NULL,
				    0, 
				    (LPTHREAD_START_ROUTINE) WriteProc,
				    (LPVOID) port,
				    0, &dwThreadID );

if(hwriteThread==NULL)
	{
	writecallbacks[port] = NULL;
	connected[port]&=0xFFFFFFFD;
	return -3;
	}
hWriteThread[port] = hwriteThread;
return 0;
}
*/

/* Doesn't work

// Function name	: ESCCToolBox_Register_Status_Callback
// Description	    : 
// Return type		: DWORD --return status
// Argument         : DWORD port --port number i.e. ESCC#
// Argument         : DWORD mask --status messages to be masked off
// Argument         : __stdcall *callbackfunction
// Argument         : DWORD status
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Register_Status_Callback(DWORD port,DWORD mask,void (__stdcall *callbackfunction)(DWORD port,DWORD status))
{
	HANDLE            hstatusThread ;	//handle to status thread
	DWORD         dwThreadID ;		//temp Thread ID storage
	
	if(port>MAXPORTS) return -1;
	if(hESCC[port]==NULL) return -2;
	statusmask[port] = mask;
	statuscallbacks[port] = callbackfunction;
	connected[port]|=4;
	//create status thread here
	
	hstatusThread =     CreateThread( (LPSECURITY_ATTRIBUTES) NULL,
				    0, 
					(LPTHREAD_START_ROUTINE) StatusProc,
					(LPVOID) port,
					0, &dwThreadID );
	
	if(hstatusThread==NULL)
	{
		statuscallbacks[port] = NULL;
		connected[port]&=0xFFFFFFFB;
		return -3;
	}
	hStatusThread[port] = hstatusThread;
	return 0;
}
*/

// Function name	: ESCCToolBox_Flush_RX
// Description	    : Command issues a reset receiver on the 82532, zeros all receive
//					  buffers and will issue a CANCEL_RX
// Return type		: DWORD --return status
// Argument         : DWORD port --port number i.e. ESCC#
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Flush_RX(DWORD port)
{
	DWORD temp;
	if(port>MAXPORTS) return -1;
	if(hESCC[port]==NULL) return -2;
	
	DeviceIoControl(hESCC[port],
		IOCTL_ESCCDRV_FLUSH_RX,                          /* Device IOCONTROL */
		NULL,								/* write data */
		0,						/* write size */
		NULL,								/* read data */
		0,									/* read size */
		&temp,								/* Returned Size */
		NULL); /* overlap */
	return 0;
}


// Function name	: ESCCToolBox_Flush_TX
// Description	    : Command issues a reset transmit on the 82532, zeros all transmit
//					  buffers and will issue a CANCEL_TX
// Return type		: DWORD --return status
// Argument         : DWORD port --port number i.e. ESCC#
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Flush_TX(DWORD port)
{
	DWORD temp;
	if(port>MAXPORTS) return -1;
	if(hESCC[port]==NULL) return -2;
	DeviceIoControl(hESCC[port],
		IOCTL_ESCCDRV_FLUSH_TX,                          /* Device IOCONTROL */
		NULL,								/* write data */
		0,						/* write size */
		NULL,								/* read data */
		0,									/* read size */
		&temp,								/* Returned Size */
		NULL); /* overlap */
return 0;
}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Read_Register(DWORD port,DWORD regno)
{
DWORD val,temp;
BOOL t;
if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;

	t = DeviceIoControl(hESCC[port],
                        IOCTL_ESCCDRV_READ_REGISTER,                             /* Device IOCONTROL */
			&regno,								/* write data */
			sizeof(unsigned long ),						/* write size */
			&val,								/* read data */
			sizeof(unsigned long),									/* read size */
			&temp,								/* Returned Size */
			NULL); /* overlap */
        if(temp!=0)  return val;
        else 
			{	
			//-1/-2/-3 are all valid returns...
			return -3;
			}

}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Write_Register(DWORD port,DWORD regno,DWORD value)
{
BOOL t;
DWORD passval[2];
DWORD val,temp;
if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;
passval[0] = regno;
passval[1] = value;
	t = DeviceIoControl(hESCC[port],
                        IOCTL_ESCCDRV_WRITE_REGISTER,                            /* Device IOCONTROL */
			&passval,								/* write data */
			2*sizeof(unsigned long ),						/* write size */
			&val,								/* read data */
			sizeof(unsigned long),									/* read size */
			&temp,								/* Returned Size */
			NULL); /* overlap */
        if(t)  return 0;
        else return -3;

}


// Function name	: ESCCToolBox_Configure
// Description	    : 
// Return type		: DWORD --return status
// Argument         : DWORD port --port number i.e. ESCC#
// Argument         : SETUP *settings --pointer to the ESCC settings structure
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Configure(DWORD port,SETUP *settings)
{
	DWORD ret;
	BOOL t;
	
	if(port>MAXPORTS) return -1;
	if(hESCC[port]==NULL) return -2;
	
	t =	DeviceIoControl(hESCC[port],
		IOCTL_ESCCDRV_SETUP,         /* Device IOCONTROL */
		settings,							/* write data */
		sizeof(SETUP)				,	/* write size */
		NULL,								/* read data */
		0,									/* read size */
		&ret,								/* Returned Size */
		NULL);								/* overlap */
	if(t) return 0;
	else 
	{
		//char buf[512];
		//	sprintf(buf,"%8.8x\n%8.8x\n%8.8x",settings->ccr0,settings->ccr1,settings->ccr2);
		//	MessageBox(NULL,buf,"CONFIGERR",MB_OK);
		return -3;
	}
}

// Function name	: ESCCToolBox_Get_Setup
// Description	    : Returns the current setup/configuration
// Return type		: struct setup 
// Argument         : DWORD port --port number i.e. ESCC#
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Get_Setup(DWORD port,SETUP *settings)
{
	DWORD res;
	
	if(port>MAXPORTS) return -1;
	if(hESCC[port]==NULL) return -2;
	if(DeviceIoControl(hESCC[port], IOCTL_ESCCDRV_GET_SETUP,NULL,0,settings,sizeof(SETUP),&res,NULL)!=0)
	{
		return (DWORD)(0);
	}
	else return -3;
}



ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Bisync_Start_Pattern_Enable(DWORD port,DWORD onoff)
{
ULONG temp;

if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;

if(DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_BISYNC_START_PATTERN_MATCH_EN,&onoff,sizeof(ULONG),NULL,0,&temp,NULL)) return 0;
else return -3;
}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Bisync_Cutoff_Enable(DWORD port,DWORD onoff)
{
ULONG temp;

if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;

if(DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_BISYNC_SIZE_CUTOFF_EN,&onoff,sizeof(ULONG),NULL,0,&temp,NULL)) return 0;
else return -3;
}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_LSB2MSB_Convert_Enable(DWORD port,DWORD onoff)\
{
ULONG temp;

if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;

if(DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_LSB2MSB_CONVERT_EN,&onoff,sizeof(ULONG),NULL,0,&temp,NULL)) return 0;
else return -3;
}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Bisync_End_Pattern_Enable(DWORD port,DWORD onoff)
{
ULONG temp;

if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;

if(DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_BISYNC_END_PATTERN_MATCH_EN,&onoff,sizeof(ULONG),NULL,0,&temp,NULL)) return 0;
else return -3;
}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Bisync_Start_Pattern(DWORD port,struct bisync_start_pattern pattern)
{
ULONG temp;
if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;
if(DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_SET_BISYNC_START_PATTERN,&pattern,sizeof(struct bisync_start_pattern),NULL,0,&temp,NULL)) return 0;
else return -3;

}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Bisync_Size_Cutoff(DWORD port,DWORD size)
{
ULONG temp;
if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;
if(DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_SET_BISYNC_SIZE_CUTOFF_SIZE,&size,sizeof(ULONG),NULL,0,&temp,NULL)) return 0;
else return -3;

}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Bisync_End_Pattern(DWORD port,struct bisync_start_pattern pattern)
{
ULONG temp;
if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;
if(DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_SET_BISYNC_END_PATTERN,&pattern,sizeof(struct bisync_start_pattern),NULL,0,&temp,NULL)) return 0;
else return -3;
}


ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Postamble_Enable(DWORD port,DWORD onoff)
{
ULONG temp;

if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;

if(DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_POSTAMBLE_EN,&onoff,sizeof(ULONG),NULL,0,&temp,NULL)) return 0;
else return -3;
}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Postamble_Delay(DWORD port,DWORD delay)
{
ULONG temp;
if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;
if(DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_POSTAMBLE_DELAY,&delay,sizeof(ULONG),NULL,0,&temp,NULL)) return 0;
else return -3;

}


// Function name	: ESCCToolBox_Set_RD_Echo_Cancel
// Description	    : Receive echo cancel is useful in some 2-wire 485 networks where 
//					  it may be useful for the receiver to be disabled during transmits 
//					  so that a station does not receive what it transmits.
// Return type		: DWORD --return status
// Argument         : DWORD port --port number i.e. ESCC#
// Argument         : DWORD onoff = 1 enable RxEcho Cancel
//								  = 0 disable RxEcho Cancel
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Set_RD_Echo_Cancel(DWORD port,DWORD onoff)
{
	ULONG temp;
	
	if(port>MAXPORTS) return -1;
	if(hESCC[port]==NULL) return -2;

	if(DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_RX_ECHO_CANCEL,&onoff,sizeof(ULONG),NULL,0,&temp,NULL))
	{
		return 0;
	}
	else return -3;
}


// Function name	: ESCCToolBox_Set_Features
// Description	    : Function to control board specific features (rd echo cancel,sd485 control/tt485 control
// Return type		: DWORD --return status
// Argument         : DWORD port --port number i.e. ESCC#
//rxecho; rx echo cancel control, 1==echo cancel ON, 0== OFF
//sd485; SD 485 control, 1== SD is RS-485, 0== SD is RS-422
//tt485 TT 485 control, 1== TT is RS-485, 0== TT is RS-422
//ctsdisable; CTS disable control, 1== CTS is disabled, 0== CTS used from connector
//sttxclk; 1== ST connected to txclk pin, 0== ST disconnected.
//tttxclk; 1== TT connected to txclk pin, 0== TT disconnected.
//busmode; 1== BUS mode operation, 0 == RS-422/485 operation, 2 == special bus operation, inverted & gated

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Set_Features(DWORD port,DWORD rxecho,DWORD sd485,DWORD tt485,DWORD ctsdisable,DWORD sttxclk,DWORD tttxclk,DWORD busmode)
{
	ULONG temp;
	ULONG desreg;
	if(port>MAXPORTS) return -1;
	if(hESCC[port]==NULL) return -2;

	desreg = 0;
	if(rxecho     == 0)		desreg|=0x01;
	if(sd485      == 0)		desreg|=0x02;
	if(tt485      == 0)		desreg|=0x04;
	if(ctsdisable == 0)	    desreg|=0x08;
	if(sttxclk    == 0)		desreg|=0x10;
	if(tttxclk    == 0)		desreg|=0x20;
	if(busmode    == 1)     desreg|=0x10000;
	if(busmode    == 2)     desreg|=0x50000;
	
	if(DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_SET_FEATURES,&desreg,sizeof(ULONG),NULL,0,&temp,NULL))
	{
		return 0;
	}
	else return -3;
}

// Function name	: ESCCToolBox_Get_Features
// Description	    : Function to control board specific features (rd echo cancel,sd485 control/tt485 control
// Return type		: DWORD --return status
// Argument         : DWORD port --port number i.e. ESCC#
//&rxecho; rx echo cancel control, 1==echo cancel ON, 0== OFF
//&sd485; SD 485 control, 1== SD is RS-485, 0== SD is RS-422
//&tt485 TT 485 control, 1== TT is RS-485, 0== TT is RS-422
//&ctsdisable; CTS disable control, 1== CTS is disabled, 0== CTS used from connector
//&sttxclk; 1== ST connected to txclk pin, 0== ST disconnected.
//&tttxclk; 1== TT connected to txclk pin, 0== TT disconnected.
//&busmode; 1== BUS mode operation, 0 == RS-422/485 operation, 2 == special bus operation, inverted & gated

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Get_Features(DWORD port,DWORD *rxecho,DWORD *sd485,DWORD *tt485,DWORD *ctsdisable,DWORD *sttxclk,DWORD *tttxclk,DWORD *busmode)
{
	ULONG temp;
	ULONG desreg;
	if(port>MAXPORTS) return -1;
	if(hESCC[port]==NULL) return -2;

	if(DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_GET_FEATURES,NULL,0,&desreg,sizeof(ULONG),&temp,NULL))
	{
	if((desreg&1)==1)		 rxecho[0]=0;
	else					 rxecho[0]=1;
	if((desreg&2)==2)        sd485[0]=0;
	else					 sd485[0]=1;
	if((desreg&4)==4)        tt485[0]=0;
	else					 tt485[0]=1;
	if((desreg&8)==8)		 ctsdisable[0]=0;
	else					 ctsdisable[0]=1;
	if((desreg&0x10)==0x10)  sttxclk[0]=0;
	else					 sttxclk[0]=1;
	if((desreg&0x20)==0x20)  tttxclk[0]=0;
	else					 tttxclk[0]=1;
	if((desreg&0x10000)==0x10000) busmode[0]=1;
	else if((desreg&0x50000)==0x50000) busmode[0]=2;
	else busmode[0]=0;
	return 0;
	}
	else return -3;
}




DWORD FAR PASCAL StatusProc( DWORD port )
{
DWORD k;
BOOL t;
DWORD returnsize;
OVERLAPPED  os ;
DWORD status;

				
memset( &os, 0, sizeof( OVERLAPPED ) );


os.hEvent = CreateEvent( NULL,TRUE,FALSE,NULL );
if (os.hEvent == NULL)
   {
   return -3;
   }
do
	{

	t = DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_STATUS,&statusmask[port],sizeof(ULONG),&status,sizeof(ULONG),&returnsize,&os);
	if(t==FALSE)
		{
		if (GetLastError() == ERROR_IO_PENDING)
			{
			do
				{
				k = WaitForSingleObject( os.hEvent, 5000 );//5 second timeout
				if(k==WAIT_TIMEOUT)
					{
					}
				if(k==WAIT_ABANDONED)
					{
					}
				}while((k!=WAIT_OBJECT_0)&&((connected[port]&4)!=0));//exit if we get signaled or if the main thread quits
				
			if((connected[port]&4)!=0)
				{
				GetOverlappedResult(hESCC[port],&os,&returnsize,TRUE); 
				t=TRUE;
				}
			}
		else
			{
			//actual error, what to do, what to do
			}
		}
	//if we get here then either it was true immediatly, so we return, or we waited and fell through and have the overlapped complete, either way signal it
	if((connected[port]&4)!=0)                   //if not connected then j is invalid
		{
		statuscallbacks[port](port,status);
		}
	}while((connected[port]&4)!=0);              //keep making requests until we want to terminate
CloseHandle( os.hEvent ) ;              //we are terminating so close the event
return 0;                          //done
}


DWORD FAR PASCAL ReadProc( DWORD port )
{
DWORD j;		
BOOL t;			
char *data;	
DWORD nobytestoread;
DWORD nobytesread;	
OVERLAPPED  os ;	

memset( &os, 0, sizeof( OVERLAPPED ) );

os.hEvent = CreateEvent( NULL,TRUE,FALSE,NULL);
if (os.hEvent == NULL)
   {
   return -3 ;
   }

data = (char *)malloc(readsize[port]);

if(data==NULL) return -4;

nobytestoread = readsize[port];

do
	{
	t = ReadFile(hESCC[port],data,nobytestoread,&nobytesread,&os);
	if(t==FALSE)
		{
		if (GetLastError() == ERROR_IO_PENDING)
			{
			do
				{
				j = WaitForSingleObject( os.hEvent, 5000 );//5 second timeout
				if(j==WAIT_TIMEOUT)
					{
					//timeout processing
					}
				if(j==WAIT_ABANDONED)
					{
					//??
					}
				}while((j!=WAIT_OBJECT_0)&&((connected[port]&1)!=0));//stay here until we get signaled or the main thread exits
			if((connected[port]&1)!=0)
					{                                                       
					GetOverlappedResult(hESCC[port],&os,&nobytesread,TRUE); //here to get the actual nobytesread!!!
					t=TRUE;
					}
			}
		else
			{
			//actual read error here...what to do, what to do
			//MessageBox(NULL,"READ ERROR","",MB_OK);
			}
		}
	//if we get here then either it was true immediatly, so we return, or we waited and fell through and have the overlapped complete, either way signal it
	if((connected[port]&1)!=0)
		{
		readcallbacks[port](port,data,nobytesread);
		}
	}while((connected[port]&1)!=0);              //do until we want to terminate
	CloseHandle( os.hEvent ) ;      //done with event
	free(data);
	return 0;                   //outta here
}

DWORD FAR PASCAL WriteProc( DWORD port )
{
OVERLAPPED  wq;
BOOL t;
DWORD nobyteswritten;
DWORD nobytestowrite;
DWORD j;
DWORD ret;
char *data;


memset( &wq, 0, sizeof( OVERLAPPED ) );
wq.hEvent = CreateEvent( NULL,TRUE,FALSE,NULL);
if (wq.hEvent == NULL)
	{
	return -3;
	}
data = (char *)malloc(maxwritesize[port]);
if(data==NULL) return -4;

do
{

if((connected[port]&2)!=0)                   
	{
	writecallbacks[port](port,data,&nobytestowrite);
	}
if(nobytestowrite>0)
{
t = WriteFile(hESCC[port],data,nobytestowrite,&nobyteswritten,&wq);
if(t==FALSE)  
	{
	ret=GetLastError();
	if(ret==ERROR_IO_PENDING)
		{
		do
			{
			j = WaitForSingleObject( wq.hEvent, 5000 );
			if(j==WAIT_TIMEOUT)
				{
				//timeout processing
				}
				if(j==WAIT_ABANDONED)
				{
				//??
				}
			} while((j!=WAIT_OBJECT_0)&&((connected[port]&2)!=0));
		GetOverlappedResult(hESCC[port],&wq,&nobyteswritten,TRUE);
		}
	else
		{
		//write error...what to do, what to do
		}
	}
//returned true, so its done or queued
}
}while((connected[port]&2)!=0);
CloseHandle(wq.hEvent);
free(data);
return 0;
}
