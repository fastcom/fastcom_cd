/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
postamble.c -- user mode function to enable/disable postamble generation using the 
escctoolbox.dll

To use the escctoolbox.DLL, you must do three things:

1. #include escctoolbox.h in your source code
2. Add the file escctoolbox.lib to your linker's Output/Library modules.  To do this in
   Visual Studio, Click on Settings in the Project menu.  In the Project settings dialog
   click on the Link tab.  In the Category drop down, choose General.  In the 
   Output/library modules box, add the path to escctoolbox.lib.  It is easiest to copy the 
   file to your working directory and just add "escctoolbox.lib"
3. The escctoolbox.dll file must reside in the same directory as your compiled program .exe


usage:
 postamble port delay

 The port can be any valid escc port (0,1) 
 
*/

#include "windows.h"
#include "..\escctoolbox.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"


int main(int argc, char *argv[])
{
unsigned long port;
DWORD ret;
DWORD delay;
DWORD onoff =1;

if(argc<3)
	{
	printf("usage:%s port delay [off]\r\n",argv[0]);
	exit(1);
	}
port = atoi(argv[1]);
delay = atol(argv[2]);
if(argc>3) onoff=0;
if((ret = ESCCToolBox_Create(port))<0)
	{
	printf("create:%d\n",ret);
	exit(1);
	}
if((ret =ESCCToolBox_Postamble_Enable(port,onoff))<0)
	{
	printf("error in Postamble Enable:%d\n",ret);
	}
else 
	{
	if(onoff==1)
		{
		if((ret =ESCCToolBox_Postamble_Delay(port,delay))<0)
			{
			printf("error in Postamble Delay:%d\n",ret);
			}
		else printf("postamble delay enabled and set to %lu\n",delay);
		}
	else printf("postamble delay disabled\n");
	}

ESCCToolBox_Destroy(port);
}
/* $Id$ */