/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.

The port can be any valid escc port (0,1,2...) 

*/

#include "windows.h"
#include "..\escctoolbox.h"
#include "stdio.h"
#include "stdlib.h"


void main(int argc,char *argv[])
{
	int ret;
	ULONG port;
	char data[4096];
	ULONG retbytes;
	unsigned int i;

	if(argc<2)
	{
		printf("usage:%s port\r\n",argv[0]);
		exit(1);
	}
	port = atoi(argv[1]);
	if((ret = ESCCToolBox_Create(port))<0) 
	{
		printf("create:%d\n",ret);
		exit(0);
	}


	if((ret = ESCCToolBox_Read_Frame(port,&data[0],4096,&retbytes,1000))<0)
		{
		printf("readframe returned :%d\r\n",ret);
		}
	else
	{
		printf("read returned %d bytes\r\n",retbytes);
		for(i=0;i<retbytes;i++) printf("%x:",data[i]&0xff);
		printf("\r\n");
	}
	//port is the port number
	//esccreadcb is your function that will execute after the read completes
	//4096 is nNumberOfBytesToRead in ReadFile.  You can leave this at 4096 and it will only return the data that is present
	ESCCToolBox_Destroy(port);
}


/* $Id$ */