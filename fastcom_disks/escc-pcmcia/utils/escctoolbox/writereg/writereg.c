/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
writereg.c -- user mode function to write to 82532 register using the escctoolbox.dll
To use the escctoolbox.DLL, you must do three things:

1. #include escctoolbox.h in your source code
2. Add the file escctoolbox.lib to your linker's Output/Library modules.  To do this in
   Visual Studio, Click on Settings in the Project menu.  In the Project settings dialog
   click on the Link tab.  In the Category drop down, choose General.  In the 
   Output/library modules box, add the path to escctoolbox.lib.  It is easiest to copy the 
   file to your working directory and just add "escctoolbox.lib"
3. The escctoolbox.dll file must reside in the same directory as your compiled program .exe

usage:
 writereg port register value

 The port can be any valid escc port (0,1) 
 
*/

#include "windows.h"
#include "..\escctoolbox.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"


int main(int argc, char *argv[])
{
unsigned long port;
DWORD ret;
DWORD reg;
DWORD val;

if(argc<4)
	{
	printf("usage:%s port register value\r\n",argv[0]);
	exit(1);
	}
port = atoi(argv[1]);
sscanf(argv[2],"%x",&reg);
sscanf(argv[3],"%x",&val);

if((ret = ESCCToolBox_Create(port))<0)
	{
	printf("create:%d\n",ret);
	exit(1);
	}
if((ret =ESCCToolBox_Write_Register(port,reg,val))<0)
	{
	printf("Error writing register:%d",ret);
	}
else  printf("reg:%x <= %x\n",reg,val);

ESCCToolBox_Destroy(port);
}
/* $Id$ */