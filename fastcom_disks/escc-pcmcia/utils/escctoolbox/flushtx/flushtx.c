/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
flushtx.c -- user mode function flush ESCC transmitter/buffers using the escctoolbox.dll

Description	    : Command issues a reset transmit on the 82532, zeros all transmit
				  buffers and will issue a CANCEL_TX
  
To use the escctoolbox.DLL, you must do three things:

1. #include escctoolbox.h in your source code
2. Add the file escctoolbox.lib to your linker's Output/Library modules.  To do this in
   Visual Studio, Click on Settings in the Project menu.  In the Project settings dialog
   click on the Link tab.  In the Category drop down, choose General.  In the 
   Output/library modules box, add the path to escctoolbox.lib.  It is easiest to copy the 
   file to your working directory and just add "escctoolbox.lib"
3. The escctoolbox.dll file must reside in the same directory as your compiled program .exe

usage:
 flushtx port 

 The port can be any valid escc port (0,1,2...)

*/

#include "windows.h"
#include "..\escctoolbox.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"


int main(int argc, char *argv[])
{
	unsigned long port;
	DWORD ret;
	
	if(argc<2)
	{
		printf("usage:%s port \r\n",argv[0]);
		exit(1);
	}
	port = atoi(argv[1]);
	
	if((ret = ESCCToolBox_Create(port))<0)
	{
		printf("create:%d\n",ret);
		exit(1);
	}
	if((ret =ESCCToolBox_Flush_TX(port))<0)
	{
		printf("error in flushtx:%d\n",ret);
	}
	else printf("TX flushed\r\n");
	
	ESCCToolBox_Destroy(port);
}
/* $Id$ */