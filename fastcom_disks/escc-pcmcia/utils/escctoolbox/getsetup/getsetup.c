/* $Id$ */
/*
Copyright(c) 2006, Commtech, Inc.
getsetup.c -- user mode function to get current configuration using the escctoolbox.dll

To use the escctoolbox.DLL, you must do three things:

1. #include escctoolbox.h in your source code
2. Add the file escctoolbox.lib to your linker's Output/Library modules.  To do this in
   Visual Studio, Click on Settings in the Project menu.  In the Project settings dialog
   click on the Link tab.  In the Category drop down, choose General.  In the 
   Output/library modules box, add the path to escctoolbox.lib.  It is easiest to copy the 
   file to your working directory and just add "escctoolbox.lib"
3. The escctoolbox.dll file must reside in the same directory as your compiled program .exe

usage:
 getsetup port 

 The port can be any valid escc port (0,1,2...)

*/

#include "windows.h"
#include "..\escctoolbox.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"


int main(int argc, char *argv[])
{
	unsigned long port;
	DWORD ret;
	SETUP settings;

	if(argc!=2)
	{
		printf("usage:%s port\r\n",argv[0]);
		exit(1);
	}
	port = atoi(argv[1]);
	
	if((ret = ESCCToolBox_Create(port))<0)
	{
		printf("create:%d\n",ret);
		exit(1);
	}
	if((ret =ESCCToolBox_Get_Setup(port,&settings))<0)
	{
		printf("error in getsetup:%d\n",ret);
	}
	else
	{
	printf("cmdr:%2.2x\n",settings.cmdr);
	printf("mode:%2.2x\n",settings.mode);
	printf("timr:%2.2x\n",settings.timr);
	printf("xbcl:%2.2x\n",settings.xbcl);
	printf("xbch:%2.2x\n",settings.xbch);
	printf("ccr0:%2.2x\n",settings.ccr0);
	printf("ccr1:%2.2x\n",settings.ccr1);
	printf("ccr2:%2.2x\n",settings.ccr2);
	printf("ccr3:%2.2x\n",settings.ccr3);
	printf("ccr4:%2.2x\n",settings.ccr4);
	printf("tsax:%2.2x\n",settings.tsax);
	printf("tsar:%2.2x\n",settings.tsar);
	printf("xccr:%2.2x\n",settings.xccr);
	printf("rccr:%2.2x\n",settings.rccr);
	printf("bgr :%2.2x\n",settings.bgr);
	printf("iva :%2.2x\n",settings.iva);
	printf("ipc :%2.2x\n",settings.ipc);
	printf("imr0:%2.2x\n",settings.imr0);
	printf("imr1:%2.2x\n",settings.imr1);
	printf("pvr :%2.2x\n",settings.pvr);
	printf("pim :%2.2x\n",settings.pim);
	printf("pcr :%2.2x\n",settings.pcr);
	printf("xad1:%2.2x\n",settings.xad1);
	printf("xad2:%2.2x\n",settings.xad2);
	printf("rah1:%2.2x\n",settings.rah1);
	printf("rah2:%2.2x\n",settings.rah2);
	printf("ral1:%2.2x\n",settings.ral1);
	printf("ral2:%2.2x\n",settings.ral2);
	printf("rlcr:%2.2x\n",settings.rlcr);
	printf("aml :%2.2x\n",settings.aml);
	printf("amh :%2.2x\n",settings.amh);
	printf("pre :%2.2x\n",settings.pre);
	printf("xon :%2.2x\n",settings.xon);
	printf("xoff:%2.2x\n",settings.xoff);
	printf("tcr :%2.2x\n",settings.tcr);
	printf("dafo:%2.2x\n",settings.dafo);
	printf("rfc :%2.2x\n",settings.rfc);
	printf("mxn :%2.2x\n",settings.mxn);
	printf("mxf :%2.2x\n",settings.mxf);
	printf("synl:%2.2x\n",settings.synl);
	printf("synh:%2.2x\n",settings.synh);
	printf("nrbufs    :%d\n",settings.n_rbufs);
	printf("nrfsizemax:%d\n",settings.n_rfsize_max);
	printf("ntbufs    :%d\n",settings.n_tbufs);
	printf("ntfsizemax:%d\n",settings.n_tfsize_max);
	}
	
	ESCCToolBox_Destroy(port);
}
/* $Id$ */