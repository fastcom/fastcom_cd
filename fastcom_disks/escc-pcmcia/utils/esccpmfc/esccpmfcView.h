// esccpmfcView.h : interface of the CEsccpmfcView class
//
/////////////////////////////////////////////////////////////////////////////

class CEsccpmfcView : public CView
{
protected: // create from serialization only
	CEsccpmfcView();
	DECLARE_DYNCREATE(CEsccpmfcView)

// Attributes
public:
	CEsccpmfcDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEsccpmfcView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void CalcWindowRect(LPRECT lpClientRect, UINT nAdjustType = adjustBorder);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CEsccpmfcView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
LRESULT OnReadUpdate(WPARAM wParam, LPARAM lParam);
LRESULT OnStatusUpdate(WPARAM wParam, LPARAM lParam);
	//{{AFX_MSG(CEsccpmfcView)
	afx_msg void OnAddescc();
	afx_msg void OnChannelset();
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnDisconnect();
	afx_msg void OnSetclock();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDtrdsr();
	afx_msg void OnWrreg();
	afx_msg void OnRdreg();
	afx_msg void OnFlushrx();
	afx_msg void OnFlushtx();
	afx_msg void OnStarttime();
	afx_msg void OnStoptime();
	afx_msg void OnSettxtype();
	afx_msg void OnWrcmd();
	afx_msg void OnSettxadd();
	afx_msg void OnSetrxadd();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in esccpmfcView.cpp
inline CEsccpmfcDoc* CEsccpmfcView::GetDocument()
   { return (CEsccpmfcDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////
