// TXsettype.cpp : implementation file
//

#include "stdafx.h"
#include "esccpmfc.h"
#include "TXsettype.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTXsettype dialog


CTXsettype::CTXsettype(CWnd* pParent /*=NULL*/)
	: CDialog(CTXsettype::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTXsettype)
	m_choice = -1;
	//}}AFX_DATA_INIT
}


void CTXsettype::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTXsettype)
	DDX_Radio(pDX, IDC_XTF, m_choice);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTXsettype, CDialog)
	//{{AFX_MSG_MAP(CTXsettype)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTXsettype message handlers
