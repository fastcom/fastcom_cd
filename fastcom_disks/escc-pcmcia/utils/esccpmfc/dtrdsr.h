// dtrdsr.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// dtrdsr dialog

class dtrdsr : public CDialog
{
// Construction
public:
	dtrdsr(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(dtrdsr)
	enum { IDD = IDD_DSRDTR };
	BOOL	m_dsr;
	BOOL	m_dtr;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(dtrdsr)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(dtrdsr)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
