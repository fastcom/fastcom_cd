/*++

Copyright (c) 1998,1999 Commtech, Inc Wichita ,KS

Module Name:

    tronly.c

Abstract:

    A simple Win32 app that uses the esccdrv device to transmit only
	--modified to send a file specified from the command line as raw bits
Environment:

    user mode only

Notes:

    
Revision History:

    6/8/98             started
	4/15/99   --mod for file out as raw data
	10/27/99  --mod for bitrate specified on command line
--*/



#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "..\esccptest.h"


VOID
main(int argc, char *argv[]
    )
/*++

Routine Description:

Arguments:

Return Value:

--*/
{
	HANDLE hDevice;                 //handle to the escc driver/device
	HANDLE fin;//handle for input filename
	DWORD nobyteswritten;   //number of bytes that the driver returns as written to the device
	struct setup esccsetup; //setup structure for initializing the escc registers (see escctest.h)
	char data[4096];                //character array for data storage (passing to and from the driver)
	DWORD nobytestowrite;   //the number of bytes to write to the driver
	ULONG j;                                //temp vars
	DWORD returnsize;               //temp vars
	BOOL t;                                 //temp vars
	OVERLAPPED  os ;                                //overlapped structure for use in the transmit routine
	DWORD nobytestoread;
	DWORD nobytesread;
	DWORD idle;
	DWORD totbytesr;
	DWORD totbytest;
	double bitrate;
	double desiredclock;
	unsigned N;
	DWORD waittime;
	
	if(argc<2)
	{
		printf("Usage:\r\n");
		printf("sendit filetosend [bitrate]\r\n");
		exit(1);
	}
	if(argc==3)
	{
		bitrate = atof(argv[2]);
		if(bitrate==0.0) bitrate = 1000000.0; //if err use 1M
	}
	else bitrate = 1000000.0;//if not specified use 1M
	
	//printf("file %s, rate:%12.2f\r\n",argv[1],bitrate);
	
	fin = CreateFile(argv[1],GENERIC_READ,0,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
	if(fin==INVALID_HANDLE_VALUE)
	{
		printf("can not open specified file %s\r\n",argv[1]);
		exit(1);
	}
	
	
	memset( &os, 0, sizeof( OVERLAPPED ) ) ;        //wipe the overlapped struct
	
	// create I/O event used for overlapped write
	
	os.hEvent = CreateEvent( NULL,    // no security
		TRUE,    // explicit reset req
		FALSE,   // initial event reset
		NULL ) ; // no name
	if (os.hEvent == NULL)
	{
		MessageBox( NULL, "Failed to create event for thread!", "main Error!",
			MB_ICONEXCLAMATION | MB_OK ) ;
		CloseHandle(fin);
		return; 
	}
	
	
	
	//this will start up the driver, it also gives us a handle to send
	//and receive data from the driver
	//note that it is created with the FILE_FLAG_OVERLAPPED set
	//if it is not then you must make sequential calls to the driver
	//(ie wait until one call has returned before making another call)
	//the read/write and status functions are set up to use overlapped, they 
	//will return immediatly with a ERROR_IO_PENDING or immediatly with the required data
	//
	
	//default to using port 0 (cable #1)
    hDevice = CreateFile ("\\\\.\\ESCC0",
		GENERIC_READ | GENERIC_WRITE,
		0,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
		NULL
		);
	
    if (hDevice == INVALID_HANDLE_VALUE)
    {
		//for some reason the driver won't load or isn't loaded
		printf ("Can't get a handle to esccdrv\n");
		CloseHandle(fin);
		CloseHandle(os.hEvent);
		exit(1);
		//abort and leave here!!!
	}
	//determine clock frequency (based on clock mode 7b);
	if(bitrate >= 6000000.0)
	{
		//use clock generator directly
		esccsetup.ccr2 = 0x18;//(BDF=0, forces inputclock/1 as ouput of bgr) 
		esccsetup.bgr = 0x00;//not used
		(ULONG)desiredclock = bitrate;
		
	}
	else
	{
		//must divide down to get rate
		esccsetup.ccr2 = 0x38;//(BDF=1, forces inputclock/((N+1)*2) as ouput of bgr) 
		N = (((unsigned)(6000000.0/bitrate)+2)/2)-1;//force inclock to be >400k
		(ULONG)desiredclock = bitrate*((N+1)*2);//calcluate what inclock needs to be to get rate using N
		//make sure that N is attainable;
		//for simplicity limit N to 0-255, could actually go to 1023 by modifying CCR2 bits 7 and 8
		if(N>255)
		{
			printf("can't get to %12.2f easily\r\n",bitrate);
			//be nice and close handles here
			CloseHandle (hDevice);// stops the escc from interrupting (ie shuts it down)
			CloseHandle(os.hEvent);
			CloseHandle(fin);
			exit(1);
		}
		esccsetup.bgr = N;
		//      printf("N:%u,Desired clock:%12.2f\r\n",N,desiredclock);
		
	}
	//this IOCTL function demonstrates how to set the clock generator on 
	//the escc card
	//this function will return TRUE unless an invalid parameter is given
	//
	if(	DeviceIoControl(hDevice,IOCTL_ESCCDRV_SET_CLOCK_FREQ,&desiredclock,sizeof(ULONG),NULL,0,&returnsize,NULL))
	{
		printf("Actual Frequency Set to: %d\n",desiredclock);
	}
	else printf("Problem Setting clock frequency.\n");/* check owners manual */       
	
	
	//make to use HDLC settings here
	esccsetup.mode = 0xc0;//Extended transparent mode 0 receiver inactive
	esccsetup.timr = 0x1f;//not used
	esccsetup.xbcl = 0x00;//not used
	esccsetup.xbch = 0x00;//not using DMA note DMAR must = DMAT = 0 in ntinstall
	esccsetup.ccr0 = 0x80;//power up, NRZ, HDLC mode, no master clock
	esccsetup.ccr1 = 0x17;//clock mode 7b uses txclk as output using BGR
	esccsetup.ccr3 = 0x00;//(preamble and other features that are turned off or not used)
	esccsetup.iva = 0;//must be
	esccsetup.ipc = 0x03;//must be
	esccsetup.imr0 = 0x04;//could mask more of the receive irq's but they won't happen anyway
	esccsetup.imr1 = 0x00;//must have XPR, should keep XDU/EXE and ALLS on as well
	esccsetup.pvr = 0x0;
	esccsetup.pim = 0xff;
	esccsetup.pcr = 0xe0;
	esccsetup.xad1 = 0xff;//must be for extended transparent mode
	esccsetup.xad2 = 0xff;//must be for extended transparent mode
	esccsetup.rah1 = 0xff;//must be for extended transparent mode
	esccsetup.rah2 = 0xff;//must be for extended transparent mode
	esccsetup.ral1 = 0xff;//must be for extended transparent mode
	esccsetup.ral2 = 0xff;//must be for extended transparent mode
	esccsetup.rlcr = 0x00;//not used
	esccsetup.pre = 0x00;//if you setup the preamble...this is it
	esccsetup.ccr4 = 0x00;//no ebgr or master clk/4.
	
	esccsetup.n_rbufs = 2;//number of receive buffers
	esccsetup.n_tbufs = 20;//number of transmit buffers
	esccsetup.n_rfsize_max = 4096;//receive buffer size
	esccsetup.n_tfsize_max = 4096;//transmit buffer size
	
	
	//when called this IOCTL will set the registers of the escc and
	//reset the TX and RX machines (issuing a separate XRES and RHR command)
	//if the resets time out the function will return FALSE, otherwise it will
	//return TRUE
	t = DeviceIoControl(hDevice,IOCTL_ESCCDRV_SETUP,&esccsetup,sizeof(struct setup),NULL,0,&returnsize,NULL);
	if(t==TRUE)printf("SETTINGS SUCCESSFUL:%lu\n\r",returnsize);
	if(t==FALSE)
	{
		printf("SETTINGS FAILED:%lu\n\r",returnsize);
		CloseHandle(fin);
		exit(1);
	}
	nobytestoread = 4096;//read from file (requested)
	nobytestowrite = 4096;  //this is the number of bytes to send to the driver to send as a single frame (can be from 1 to 4096) (requested)
	nobytesread = 0; //what we get from the file (actual)
	nobyteswritten = 0;//what is sent out the device (actual)
	totbytesr = 0;//total bytes received (from file)
	totbytest = 0;//total bytes transmitted (out device)
	idle = 0;//idle counter (#times that writefile returns TRUE)
	do
	{
		t= ReadFile(fin,data,nobytestoread,&nobytesread,NULL);//get data from the input file
		totbytesr += nobytesread;//inc the byte counter
		
		nobytestowrite = nobytesread;//set the bytes to write in case the read comes up short
		t = WriteFile(hDevice,data,nobytestowrite,&nobyteswritten,&os);//send the frame
		if(t==FALSE)                    //if it returned FALSE then the IO request is queued (waiting for previous frame to get done sending)
		{                       //and we must wait until the os.event gets signaled before we try any more sending
			if (GetLastError() == ERROR_IO_PENDING)  //IO_PENDING is the indication that the request was queued, if not this than a parameter error occured
			{
				// wait for a second for this transmission to start
				//you should allways end up here other than on the first write.
				//if you do not get here then the data will not go out back to back
				//(there will be idle time between frames)
				//The first writefile should return true (indicating that the frame is started on the transmitter)
				//all writefile calls after that first one should return false/iopending
				//indicating that the frame is queued for transmit when the previous frame is complete.
				
				//the basics of the above are still true, only with the latest driver the
				//pending will only happen after all n_tbufs frames are pending 
				//for instance since there are 20 tbuffers defined (n_tbufs =20) for this
				//application, the 21st WriteFile (presuming that all 21 writefiles will occur before 4096 bytes are physically transmitted out of the ESCC)
				//will be the writefile that will pend.
				
				do
				{
					j = WaitForSingleObject( os.hEvent, (DWORD)(((double)(8*(4096+32))/bitrate)*(double)1000)+10);//bitrate dependent timeout, this number corresponds to the time to send 1 frame  +32 byte times
					if(j==WAIT_TIMEOUT)
					{
						printf("timeout\r\n");
						//this will execute every xx second that all of the tbuffers are full (queued)
						//the timeout value should be set depending on the bitrate used
						//it should be longer than the ammount of time it takes to send 1 of your frames
						
						//if this happens then something is wrong with the driver, or 
						//some event happened that prevented the card from finishing the write.
						//(be careful to make the timeout longer than the ammount of time that it
						//takes to send a frame :)
						
						//you could put a counter in here and if the 
						//driver takes an inordinate ammout of time
						//to complete, you could issue a flush TX command
						//and break out of this loop
						
						//printf("Transmitter Locked up...resetting tx\r\n");
						DeviceIoControl(hDevice,IOCTL_ESCCDRV_FLUSH_TX,NULL,0,NULL,0,&returnsize,NULL);
						//uncomment the next line if you are using the new driver
						/*
						DeviceIoControl(hDevice,IOCTL_ESCCDRV_IMMEDIATE_STATUS,NULL,0,&k,sizeof(DWORD),&returnsize,NULL);
						if(k!=0) printf("\r\nSTATUS: %lx\r\n",k);
						*/
						//the most common problem will be EXE/XDU or a data underrun, if that occurs then
						//the PC is not keeping up with the interrupt rate necessary to keep the 82532 fifo filled
						
					}
					if(j==WAIT_ABANDONED)
					{
						printf("abandoned\r\n");
						
					}
					
				}while(j!=WAIT_OBJECT_0);
			}
		}
		else
		{
			//write file returned true
			//this means that there was some idle pattern going out before this frame:
			//if all goes well this should only print 1 time when the program is started.
			idle++;
			//printf("idle detected\r\n");
			
			//with the new driver this isn't exactly true anymore, if you get here it means
			//that the driver did not have to wait for a transmit buffer, and could queue it immediately
			//if all goes well you should get idle == 20, but it is no longer possible to
			//say that if idle is >20 that there was idle between frames, as it is possible
			//that there are still frames queued (say 18 or 19) when the write happens, which would get here as there is an open buffer, 
			//but the data would still go out back to back because there are still frames to send from.
			
		}
		//GetOverlappedResult(hDevice,&os,&nobyteswritten,TRUE);//if you are speed challanged (ie the idle keeps being greater than 1) try removing this
		//      totbytest += nobyteswritten;    //as it is only here to keep track of how many bytes are sent.
		
	}while(nobytesread!=0);
	printf("filesend complete, waiting for end of tx...\r\n");
	if((totbytesr*8)<(esccsetup.n_tbufs*4096*8)) waittime = (DWORD)((double)totbytesr*(double)(8)/bitrate*(double)1000)+10;
	else waittime = (DWORD)((double)esccsetup.n_tbufs*(double)(4096*8)/bitrate*(double)1000)+10;
	Sleep(waittime);//make sure you wait until the last write(s) complete their sends, wait time should be greater than n_tbufs*(framesize*8)*(1/bitrate) )
	printf("DONE\r\n");
	CloseHandle (hDevice);// stops the escc from interrupting (ie shuts it down)
	CloseHandle(os.hEvent);//finished with the event
	CloseHandle(fin);//finished with the input file
	printf("Idle:%u\r\n",idle);
	printf("bytes in:%lu\r\n",totbytesr);
	//      printf("bytes out:%lu\r\n",totbytest);
	printf("exiting program\n\r");          //exit message
	
}                                               //done

