/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
setclock.c -- user mode function to set the OSC input frequency to the 20534

usage:
 setclock port frequency

 The port can be any valid ESCC port (0,1) and it doesn't matter which
 port you select,  There is only 1 OSC input that is used by all 2 channels.

 The frequency can be any in the range 6000000 to 33333333


*/


#include <windows.h>
#include <stdio.h>

#include "..\esccptest.h"



int main(int argc, char * argv[])
{
	//clkset clock;
	char nbuf[80];
	int port;
	HANDLE hDevice; 
	ULONG  desfreq;
	unsigned long temp;
	
	if(argc<3) {
		printf("usage: setclock port frequency\n");
		exit(1);
	}
	
	port = atoi(argv[1]);
	desfreq = atol(argv[2]);
	
	sprintf(nbuf,"\\\\.\\ESCC%u",port);
	
	printf("Opening: %s\n",nbuf);
	
	if((hDevice = CreateFile (
		nbuf, 
		GENERIC_READ | GENERIC_WRITE,
		0,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
		NULL)) == INVALID_HANDLE_VALUE ) 
	{
		printf("Can't get a handle to esccdrv @ %s\n",nbuf);
		exit(1);
	}
	
	printf("Desired Frequency: %d\n",desfreq);
	
	if(	DeviceIoControl(hDevice,IOCTL_ESCCDRV_SET_CLOCK_FREQ,&desfreq,sizeof(ULONG),NULL,0,&temp,NULL))
	{
		printf("Actual Frequency Set to: %d\n",desfreq);
	}
	else printf("Problem Setting clock frequency.\n");/* check owners manual */       
	
	
	CloseHandle(hDevice);
	return 0;
}




/* $Id$ */