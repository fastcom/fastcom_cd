VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   3675
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6120
   LinkTopic       =   "Form1"
   ScaleHeight     =   3675
   ScaleWidth      =   6120
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Receivedata 
      Caption         =   "Receive data"
      Height          =   375
      Left            =   2880
      TabIndex        =   11
      Top             =   2160
      Width           =   1455
   End
   Begin VB.CommandButton sendit 
      Caption         =   "Send data"
      Height          =   375
      Left            =   2880
      TabIndex        =   10
      Top             =   720
      Width           =   1455
   End
   Begin VB.CommandButton Command1 
      Caption         =   "stop/exit"
      Height          =   492
      Left            =   4560
      TabIndex        =   9
      Top             =   1680
      Width           =   1452
   End
   Begin VB.TextBox status 
      Height          =   372
      Left            =   1080
      TabIndex        =   7
      Top             =   3240
      Width           =   4932
   End
   Begin VB.CommandButton Start 
      Caption         =   "Start"
      Height          =   492
      Left            =   4560
      TabIndex        =   6
      Top             =   1080
      Width           =   1452
   End
   Begin VB.TextBox Port 
      Height          =   372
      Left            =   3960
      TabIndex        =   2
      Text            =   "0"
      Top             =   240
      Width           =   1092
   End
   Begin VB.TextBox txdata 
      Height          =   972
      Left            =   600
      MultiLine       =   -1  'True
      TabIndex        =   1
      Top             =   720
      Width           =   2172
   End
   Begin VB.TextBox rxdata 
      Height          =   972
      Left            =   600
      MultiLine       =   -1  'True
      TabIndex        =   0
      Top             =   2160
      Width           =   2172
   End
   Begin VB.Label Label4 
      Caption         =   "Status"
      Height          =   252
      Left            =   0
      TabIndex        =   8
      Top             =   3240
      Width           =   852
   End
   Begin VB.Label Label3 
      Caption         =   "RX"
      Height          =   252
      Left            =   120
      TabIndex        =   5
      Top             =   1920
      Width           =   372
   End
   Begin VB.Label Label2 
      Caption         =   "TX"
      Height          =   252
      Left            =   120
      TabIndex        =   4
      Top             =   360
      Width           =   372
   End
   Begin VB.Label Label1 
      Caption         =   "Port"
      Height          =   252
      Left            =   3240
      TabIndex        =   3
      Top             =   240
      Width           =   492
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim portnum As Long


Private Sub Command1_Click()
Dim ret As Long
ret = ESCCToolBox_Destroy(portnum)
End Sub

Private Sub Form_Load()
portnum = 0
End Sub

Private Sub Form_Terminate()
ret = ESCCToolBox_Destroy(portnum)
End Sub

Private Sub Port_Change()
portnum = Port.Text

End Sub

Private Sub Receivedata_Click()
Dim ret As Long
Dim data(4096) As Byte
Dim retbytes As Long
Dim szrbuf As Long
Dim timeout As Long
Dim i As Long
Dim s As String

szrbuf = 4096
timeout = 1000

ret = ESCCToolBox_Read_Frame(portnum, data(1), szrbuf, retbytes, timeout)
If ret = 0 Then
status.Text = "RX OK"
s = retbytes
Form1.rxdata.Text = "received " + s + " bytes "
For i = 1 To retbytes Step 1
s = data(i)
Form1.rxdata.Text = Form1.rxdata.Text + ":" + s
Next
Else
Form1.rxdata.Text = "no bytes received"
End If

End Sub

Private Sub sendit_Click()
Dim ret As Long
Dim i As Long
Dim data(1024) As Byte
Dim timeout As Long


For i = 1 To Len(txdata) Step 1
data(i) = Asc(Mid(txdata, i, 1))
Next
timeout = 1000
ret = ESCCToolBox_Write_Frame(portnum, data(1), Len(txdata.Text), timeout)
If (ret = 0) Then
status.Text = "TX OK"
End If
If (ret < 0) Then
status.Text = "TX Failed" + Str(ret)
End If

End Sub

Private Sub Start_Click()
Dim settings As ESCCSETUP
Dim ret As Long
ret = ESCCToolBox_Create(portnum)
If (ret < 0) Then
status.Text = "Create Error: " + Str(ret)
MsgBox ("ERR")
End If
ret = ESCCToolBox_Set_Clock(portnum / 4, 6000000)
If (ret < 0) Then
status.Text = "SetClock Error: " + Str(ret)
MsgBox ("ERR")
End If

settings.cmdr = &H0
settings.mode = &H88
settings.timr = &H1F
settings.xbcl = &H0
settings.xbch = &H0
settings.ccr0 = &H80
settings.ccr1 = &H16
settings.ccr2 = &H38
settings.ccr3 = &H0
settings.ccr4 = &H0
settings.tsax = &H0
settings.tsar = &H0
settings.xccr = &H0
settings.rccr = &H0
settings.bgr = &H0
settings.iva = &H0
settings.ipc = &H3
settings.imr0 = &H4
settings.imr1 = &H0
settings.pvr = &H0
settings.pim = &HFF
settings.pcr = &HE0
settings.xad1 = &HFF
settings.xad2 = &HFF
settings.rah1 = &HFF
settings.rah2 = &HFF
settings.ral1 = &HFF
settings.ral2 = &HFF
settings.rlcr = &H0
settings.aml = &H0
settings.amh = &H0
settings.pre = &H0
settings.xon = &H0
settings.xoff = &H0
settings.tcr = &H0
settings.dafo = &H0
settings.rfc = &H0
settings.tic = &H0
settings.mxn = &H0
settings.mxf = &H0
settings.synl = &H0
settings.synh = &H0
settings.n_rbufs = &HA
settings.n_tbufs = &HA
settings.n_rfsize_max = &H1000
settings.n_tfsize_max = &H1000
        



ret = ESCCToolBox_Configure(portnum, settings)
If (ret < 0) Then
status.Text = "Configure Error: " + Str(ret)
MsgBox ("ERR")
End If

ret = ESCCToolBox_Flush_RX(portnum)
ret = ESCCToolBox_Flush_TX(portnum)


Rem This doesn't work, I think it has something to do with the threading model of vb, but for whatever reason
Rem calling back into this project from the dll doesn't work, it works from a C/C++ project but not here
Rem If you know enough about VB to fix this please let me know what you did to make it work!
Rem ret = ESCCToolBox_Register_Read_Callback(portnum, AddressOf ReadCallbackFunction, 1024, rdata(1))
Rem If (ret < 0) Then
Rem status.Text = "Unable to register read callback: " + Str(ret)
Rem MsgBox ("ERR")
Rem End If


End Sub

