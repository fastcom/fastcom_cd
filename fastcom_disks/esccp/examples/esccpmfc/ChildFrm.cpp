// ChildFrm.cpp : implementation of the CChildFrame class
//

#include "stdafx.h"
#include "esccpmfc.h"

#include "ChildFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChildFrame

IMPLEMENT_DYNCREATE(CChildFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CChildFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CChildFrame)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChildFrame construction/destruction

CChildFrame::CChildFrame()
{
	// TODO: add member initialization code here
	
}

CChildFrame::~CChildFrame()
{
}

BOOL CChildFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CMDIChildWnd::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CChildFrame diagnostics

#ifdef _DEBUG
void CChildFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CChildFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CChildFrame message handlers

void CChildFrame::OnSize(UINT nType, int cx, int cy) 
{
	//CMDIChildWnd::OnSize(nType, cx, cy);
WINDOWPLACEMENT wndpl;	
	// TODO: Add your message handler code here
GetWindowPlacement(&wndpl);
ULONG flag = 0;

	if((wndpl.rcNormalPosition.right - wndpl.rcNormalPosition.left) <  518)
		{
		cx = 518;
		wndpl.rcNormalPosition.right = wndpl.rcNormalPosition.left + 518;  
		SetWindowPlacement(&wndpl);
		//SetWindowPos(NULL,0,0,cx,cy,SWP_NOMOVE|SWP_NOZORDER);
		flag = 1;
		}
	if((wndpl.rcNormalPosition.bottom - wndpl.rcNormalPosition.top) < 281)
		{
		cy = 281; //minimum window size
		wndpl.rcNormalPosition.bottom = wndpl.rcNormalPosition.top + 281;
		SetWindowPlacement(&wndpl);
		//SetWindowPos(NULL,0,0,cx,cy,SWP_NOMOVE|SWP_NOZORDER);
		flag = 1;
		}
if(flag==1)SetWindowPos(NULL,0,0,cx,cy,SWP_NOMOVE|SWP_NOZORDER);
else CMDIChildWnd::OnSize(nType, cx, cy);	
}
