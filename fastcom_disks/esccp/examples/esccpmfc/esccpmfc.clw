; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=Csetdlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "esccpmfc.h"
LastPage=0

ClassCount=14
Class1=CEsccpmfcApp
Class2=CEsccpmfcDoc
Class3=CEsccpmfcView
Class4=CMainFrame

ResourceCount=12
Resource1=IDD_GETSETREG
Resource2=IDD_ADDRESS
Resource3=IDD_CLOCKSET
Class5=CAboutDlg
Class6=CChildFrame
Class7=CDevno
Resource4=IDD_ABOUTBOX
Resource5=IDD_TXSETTYPE
Class8=Csetdlg
Resource6=IDD_DSRDTR
Class9=CClockSet
Resource7=IDR_MAINFRAME
Class10=dtrdsr
Resource8=IDR_ESCCPMTYPE
Class11=CGetSetReg
Resource9=IDD_ASYNCSET
Resource10=IDD_SETDLG
Class12=CTXsettype
Resource11=IDD_DEVNO
Class13=CGetAdd
Class14=CGetCmd
Resource12=IDD_GETCMD

[CLS:CEsccpmfcApp]
Type=0
HeaderFile=esccpmfc.h
ImplementationFile=esccpmfc.cpp
Filter=N

[CLS:CEsccpmfcDoc]
Type=0
HeaderFile=esccpmfcDoc.h
ImplementationFile=esccpmfcDoc.cpp
Filter=N
LastObject=CEsccpmfcDoc

[CLS:CEsccpmfcView]
Type=0
HeaderFile=esccpmfcView.h
ImplementationFile=esccpmfcView.cpp
Filter=C
LastObject=CEsccpmfcView
BaseClass=CView
VirtualFilter=VWC

[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T
BaseClass=CMDIFrameWnd
VirtualFilter=fWC
LastObject=CMainFrame


[CLS:CChildFrame]
Type=0
HeaderFile=ChildFrm.h
ImplementationFile=ChildFrm.cpp
Filter=M
LastObject=CChildFrame
BaseClass=CMDIChildWnd
VirtualFilter=mfWC

[CLS:CAboutDlg]
Type=0
HeaderFile=esccpmfc.cpp
ImplementationFile=esccpmfc.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=6
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889
Control5=IDC_STATIC,static,1342308352
Control6=IDC_STATIC,static,1342308352

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_MRU_FILE1
Command4=ID_APP_EXIT
Command5=ID_APP_ABOUT
CommandCount=5

[MNU:IDR_ESCCPMTYPE]
Type=1
Class=CEsccpmfcView
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_CLOSE
Command4=ID_FILE_SAVE
Command5=ID_FILE_SAVE_AS
Command6=ID_FILE_MRU_FILE1
Command7=ID_APP_EXIT
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=IDM_ADDESCC
Command13=IDM_CHANNELSET
Command14=IDM_DISCONNECT
Command15=IDM_SETCLOCK
Command16=IDM_DTRDSR
Command17=IDM_WRREG
Command18=IDM_RDREG
Command19=IDM_WRCMD
Command20=IDM_FLUSHTX
Command21=IDM_FLUSHRX
Command22=IDM_SETTXADD
Command23=IDM_SETRXADD
Command24=IDM_STARTTIME
Command25=IDM_STOPTIME
Command26=IDM_SETTXTYPE
Command27=ID_WINDOW_NEW
Command28=ID_WINDOW_CASCADE
Command29=ID_WINDOW_TILE_HORZ
Command30=ID_WINDOW_ARRANGE
Command31=ID_APP_ABOUT
CommandCount=31

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_UNDO
Command5=ID_EDIT_CUT
Command6=ID_EDIT_COPY
Command7=ID_EDIT_PASTE
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_NEXT_PANE
Command13=ID_PREV_PANE
CommandCount=13

[DLG:IDD_DEVNO]
Type=1
Class=CDevno
ControlCount=3
Control1=IDC_EDIT1,edit,1350631552
Control2=IDOK,button,1342242817
Control3=IDC_STATIC,static,1342308352

[CLS:CDevno]
Type=0
HeaderFile=Devno.h
ImplementationFile=Devno.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CDevno

[DLG:IDD_SETDLG]
Type=1
Class=Csetdlg
ControlCount=89
Control1=IDC_HDLC,button,1342373897
Control2=IDC_BISYNC,button,1342242825
Control3=IDC_ASYNC,button,1342242825
Control4=IDC_MODE,edit,1350762624
Control5=IDC_TIMR,edit,1350762624
Control6=IDC_XBCL,edit,1350762624
Control7=IDC_XBCH,edit,1350762624
Control8=IDC_CCR0,edit,1350762624
Control9=IDC_CCR1,edit,1350762624
Control10=IDC_CCR2,edit,1350762624
Control11=IDC_CCR3,edit,1350762624
Control12=IDC_BGR,edit,1350762624
Control13=IDC_IVA,edit,1350762624
Control14=IDC_IPC,edit,1350762624
Control15=IDC_IMR0,edit,1350762624
Control16=IDC_IMR1,edit,1350762624
Control17=IDC_PVR,edit,1350762624
Control18=IDC_PIM,edit,1350762624
Control19=IDC_PCR,edit,1350762624
Control20=IDC_XAD1,edit,1350762624
Control21=IDC_XAD2,edit,1350762624
Control22=IDC_RAH1,edit,1350762624
Control23=IDC_RAL1,edit,1350762624
Control24=IDC_RAH2,edit,1350762624
Control25=IDC_RAL2,edit,1350762624
Control26=IDC_RLCR,edit,1350762624
Control27=IDC_PRE,edit,1350762624
Control28=IDC_TCR,edit,1350762624
Control29=IDC_DAFO,edit,1350762624
Control30=IDC_RFC,edit,1350762624
Control31=IDC_SYNL,edit,1350762624
Control32=IDC_SYNH,edit,1350762624
Control33=IDOK,button,1342242817
Control34=IDCANCEL,button,1342242816
Control35=IDC_STATIC,static,1342308352
Control36=IDC_STATIC,static,1342308352
Control37=IDC_STATIC,static,1342308352
Control38=IDC_STATIC,static,1342308352
Control39=IDC_STATIC,static,1342308352
Control40=IDC_STATIC,static,1342308352
Control41=IDC_STATIC,static,1342308352
Control42=IDC_STATIC,static,1342308352
Control43=IDC_STATIC,static,1342308352
Control44=IDC_STATIC,static,1342308352
Control45=IDC_STATIC,static,1342308352
Control46=IDC_STATIC,static,1342308352
Control47=IDC_STATIC,static,1342308352
Control48=IDC_STATIC,static,1342308352
Control49=IDC_STATIC,static,1342308352
Control50=IDC_STATIC,static,1342308352
Control51=IDC_STATIC,static,1342308352
Control52=IDC_STATIC,static,1342308352
Control53=IDC_STATIC,static,1342308352
Control54=IDC_STATIC,static,1342308352
Control55=IDC_STATIC,static,1342308352
Control56=IDC_STATIC,static,1342308352
Control57=IDC_STATIC,static,1342308352
Control58=IDC_STATIC,static,1342308352
Control59=IDC_STATIC,static,1342308352
Control60=IDC_STATIC,static,1342308352
Control61=IDC_STATIC,static,1342308352
Control62=IDC_STATIC,static,1342308352
Control63=IDC_STATIC,static,1342308352
Control64=IDC_CCR4,edit,1350762624
Control65=IDC_STATIC,static,1342308352
Control66=IDC_TSAX,edit,1350762624
Control67=IDC_STATIC,static,1342308352
Control68=IDC_TSAR,edit,1350762624
Control69=IDC_STATIC,static,1342308352
Control70=IDC_XCCR,edit,1350762624
Control71=IDC_STATIC,static,1342308352
Control72=IDC_RCCR,edit,1350762624
Control73=IDC_STATIC,static,1342308352
Control74=IDC_AML,edit,1350762624
Control75=IDC_STATIC,static,1342308352
Control76=IDC_AMH,edit,1350762624
Control77=IDC_STATIC,static,1342308352
Control78=IDC_MXN,edit,1350762624
Control79=IDC_STATIC,static,1342308352
Control80=IDC_MXF,edit,1350762624
Control81=IDC_STATIC,static,1342308352
Control82=IDC_NRBUFS,edit,1350631552
Control83=IDC_RBUFSIZE,edit,1350631552
Control84=IDC_NTBUFS,edit,1350631552
Control85=IDC_TBUFSIZE,edit,1350631552
Control86=IDC_STATIC,static,1342308352
Control87=IDC_STATIC,static,1342308352
Control88=IDC_STATIC,static,1342308352
Control89=IDC_STATIC,static,1342308352

[DLG:IDD_ASYNCSET]
Type=1
Class=?
ControlCount=19
Control1=IDC_BAUDS,combobox,1344339970
Control2=IDC_PE,button,1342373897
Control3=IDC_PO,button,1342242825
Control4=IDC_PN,button,1342242825
Control5=IDC_PM,button,1342242825
Control6=IDC_PS,button,1342242825
Control7=IDC_B5,button,1342373897
Control8=IDC_B6,button,1342242825
Control9=IDC_B7,button,1342242825
Control10=IDC_8,button,1342242825
Control11=IDC_ST1,button,1342373897
Control12=IDC_ST2,button,1342242825
Control13=IDC_PARITYSAVE,button,1342242819
Control14=IDOK,button,1342242817
Control15=IDCANCEL,button,1342242816
Control16=IDC_STATIC,button,1342177287
Control17=IDC_STATIC,button,1342177287
Control18=IDC_STATIC,button,1342177287
Control19=IDC_STATIC,static,1342308352

[CLS:Csetdlg]
Type=0
HeaderFile=setdlg.h
ImplementationFile=setdlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=IDC_EDIT3

[DLG:IDD_CLOCKSET]
Type=1
Class=CClockSet
ControlCount=6
Control1=IDC_NUMBITS,edit,1350631552
Control2=IDC_CLOCKBITS,edit,1350631552
Control3=IDOK,button,1342242817
Control4=IDCANCEL,button,1342242816
Control5=IDC_STATIC,static,1342308352
Control6=IDC_STATIC,static,1342308352

[CLS:CClockSet]
Type=0
HeaderFile=ClockSet.h
ImplementationFile=ClockSet.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CClockSet

[DLG:IDD_DSRDTR]
Type=1
Class=dtrdsr
ControlCount=4
Control1=IDC_DTR,button,1342242819
Control2=IDC_DSR,button,1342242819
Control3=IDOK,button,1342242817
Control4=IDCANCEL,button,1342242816

[CLS:dtrdsr]
Type=0
HeaderFile=dtrdsr.h
ImplementationFile=dtrdsr.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=dtrdsr

[DLG:IDD_GETSETREG]
Type=1
Class=CGetSetReg
ControlCount=6
Control1=IDC_REGS,combobox,1344339970
Control2=IDC_REGVAL,edit,1350631552
Control3=IDOK,button,1342242817
Control4=IDCANCEL,button,1342242816
Control5=IDC_STATIC,static,1342308352
Control6=IDC_STATIC,static,1342308352

[CLS:CGetSetReg]
Type=0
HeaderFile=GetSetReg.h
ImplementationFile=GetSetReg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=IDOK

[DLG:IDD_TXSETTYPE]
Type=1
Class=CTXsettype
ControlCount=4
Control1=IDC_XTF,button,1342308361
Control2=IDC_XIF,button,1342177289
Control3=IDOK,button,1342242817
Control4=IDCANCEL,button,1342242816

[CLS:CTXsettype]
Type=0
HeaderFile=TXsettype.h
ImplementationFile=TXsettype.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CTXsettype

[DLG:IDD_ADDRESS]
Type=1
Class=CGetAdd
ControlCount=4
Control1=IDC_EDIT1,edit,1350631552
Control2=IDOK,button,1342242817
Control3=IDCANCEL,button,1342242816
Control4=IDC_STATIC,static,1342308352

[CLS:CGetAdd]
Type=0
HeaderFile=GetAdd.h
ImplementationFile=GetAdd.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CGetAdd

[DLG:IDD_GETCMD]
Type=1
Class=CGetCmd
ControlCount=4
Control1=IDC_EDIT1,edit,1350631552
Control2=IDOK,button,1342242817
Control3=IDCANCEL,button,1342242816
Control4=IDC_STATIC,static,1342308352

[CLS:CGetCmd]
Type=0
HeaderFile=GetCmd.h
ImplementationFile=GetCmd.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC

