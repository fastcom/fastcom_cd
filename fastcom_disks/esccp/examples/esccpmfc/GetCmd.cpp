// GetCmd.cpp : implementation file
//

#include "stdafx.h"
#include "esccpmfc.h"
#include "GetCmd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGetCmd dialog


CGetCmd::CGetCmd(CWnd* pParent /*=NULL*/)
	: CDialog(CGetCmd::IDD, pParent)
{
	//{{AFX_DATA_INIT(CGetCmd)
	m_cmd = _T("");
	//}}AFX_DATA_INIT
}


void CGetCmd::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGetCmd)
	DDX_Text(pDX, IDC_EDIT1, m_cmd);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CGetCmd, CDialog)
	//{{AFX_MSG_MAP(CGetCmd)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGetCmd message handlers
