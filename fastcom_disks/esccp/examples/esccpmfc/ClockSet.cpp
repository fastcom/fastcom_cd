// ClockSet.cpp : implementation file
//

#include "stdafx.h"
#include "esccpmfc.h"
#include "ClockSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CClockSet dialog


CClockSet::CClockSet(CWnd* pParent /*=NULL*/)
	: CDialog(CClockSet::IDD, pParent)
{
	//{{AFX_DATA_INIT(CClockSet)
	m_clockbits = _T("");
	m_numbits = _T("");
	//}}AFX_DATA_INIT
}


void CClockSet::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CClockSet)
	DDX_Text(pDX, IDC_CLOCKBITS, m_clockbits);
	DDX_Text(pDX, IDC_NUMBITS, m_numbits);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CClockSet, CDialog)
	//{{AFX_MSG_MAP(CClockSet)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CClockSet message handlers
