// GetAdd.cpp : implementation file
//

#include "stdafx.h"
#include "esccpmfc.h"
#include "GetAdd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGetAdd dialog


CGetAdd::CGetAdd(CWnd* pParent /*=NULL*/)
	: CDialog(CGetAdd::IDD, pParent)
{
	//{{AFX_DATA_INIT(CGetAdd)
	m_add = _T("");
	//}}AFX_DATA_INIT
}


void CGetAdd::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGetAdd)
	DDX_Text(pDX, IDC_EDIT1, m_add);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CGetAdd, CDialog)
	//{{AFX_MSG_MAP(CGetAdd)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGetAdd message handlers
