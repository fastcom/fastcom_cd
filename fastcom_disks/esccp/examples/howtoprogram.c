//$Id$
/*++

Copyright (c) 2003 Commtech, Inc Wichita ,KS

Module Name:

    howto.c

Abstract:

    A simple Win32 app that demonstrates the program interface to the esccdrv device
	This program does not do anything useful, it is merely meant to show how to program
	the card.

Environment:

    user mode only

Notes:

    
Revision History:

	11/19/2003	Created		MDS
--*/
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>

#include "esccptest.h"

VOID main(IN int argc,IN char *argv[])
{

	HANDLE hDevice;	//handle to the escc driver/device

	OVERLAPPED  txoverstruct;	//overlapped structure for use in the transmit routine

	OVERLAPPED  rxoverstruct;	//overlapped structure for use in the receive routine

	char devicename[16] = {0};	//char arrar to store the device name string

	ULONG numbytestoread = 0;	//number of bytes to request on a read

	ULONG numbytesread = 0;	//number of bytes actually read

	ULONG numbytestowrite = 0;	//number of bytes to write

	ULONG numbyteswritten = 0;	//number of bytes actually written

	CLKSET clock;	//this is used to pass programming data to the clock generator

	ULONG returnsize = 0;	//temporary variable used in DeviceIoControl() calls

	BOOL retvalue = 0;	//value returned from functions

	SETUP esccsettings;	//sturcture that holds the 82532 register settings

	struct regsingle regs;	//sturcture used to access single registers of 82532

	ULONG datain = 0;	//input to IOCTLs

	ULONG dataout = 0;	//output from IOCTLs

	ULONG i=0;	//loop variable

	char txdata[4096];	//transmit data

	char rxdata[4096];	//receive data

	BISYNCSTART bisyncstart;	//data to pass to IOCTL_ESCCDRV_SET_BISYNC_START_PATTERN


/********************** Setup Program Environment **********************/
	memset( &bisyncstart, 0, sizeof( BISYNCSTART ) );//wipe structure

	memset( &txoverstruct, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct

	txoverstruct.hEvent = CreateEvent( NULL,    // no security
					TRUE,    // explicit reset req
					FALSE,   // initial event reset
					NULL ) ; // no name
	if (txoverstruct.hEvent == NULL)
	{
      MessageBox( NULL, "Failed to create event for thread!", "main Error!", MB_ICONEXCLAMATION | MB_OK ) ;
      return;
	}

	memset( &rxoverstruct, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct

	rxoverstruct.hEvent = CreateEvent( NULL,    // no security
					TRUE,    // explicit reset req
					FALSE,   // initial event reset
					NULL ) ; // no name
	if (rxoverstruct.hEvent == NULL)
	{
      MessageBox( NULL, "Failed to create event for thread!", "main Error!", MB_ICONEXCLAMATION | MB_OK ) ;
      return;
	}

/***************************** Setup Device ****************************/

	/*
		This should allways be 4096, it is the maximum frame size returned from the 
		driver at one time.
	*/
	numbytestoread = 4096;

	/*	
		This is the number of bytes to send to the driver to send as a single frame 
		(can be from 1 to 4096).
	*/
	numbytestowrite = 256;

	/*
		This is the name of the device.
		It will always be \\\\.\\ESCC# where # is the port number of the board
	*/
	sprintf(devicename,"\\\\.\\ESCC0");

	/*
	This will start up the driver, it also gives us a handle to send and receive data 
	from the driver.
	Note that it is created with the FILE_FLAG_OVERLAPPED set if it is not, then you must 
	make sequential calls to the driver (ie wait until one call has returned before making 
	another call).
	The read/write and status functions are set up to use overlapped, they will return 
	immediatly with a ERROR_IO_PENDING or immediatly with the required data

	*/
    hDevice = CreateFile (devicename,
			  GENERIC_READ | GENERIC_WRITE,
			  0,
			  NULL,
			  OPEN_EXISTING,
			  FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
			  NULL
			  );
    if (hDevice == INVALID_HANDLE_VALUE)
    {
		//for some reason the driver won't load or isn't loaded
		printf ("Can't get a handle to esccdrv\n");
		exit(1);
		//abort and leave here!!!
	}


	/*
	Install and use the Bitcalc program found on the Fastcom CD to come up with 
	your desired frequency.  The total number of bits displayed in the "Binary word (bit 
	stuffed) field" = clock.numbits.  The Hex word (stuffed) = clock.clockbits.
	The frequency that you set here is divided by the divisor from the BGR register to 
	determine your datarate.
	*/
	clock.clockbits = 0x1ce920; //set to 1.8432 MHz
	clock.numbits = 24;



	/*
	This IOCTL function sets the clock generator on the escc card
	INPUT CLKSET structure
	OUTPUT NULL
	This function will return TRUE unless an invalid parameter is given.
	*/
	retvalue = DeviceIoControl(hDevice,IOCTL_ESCCDRV_SET_CLOCK,&clock,sizeof(CLKSET),NULL,0,&returnsize,NULL);



	/*
	These settings will set HDLC as well as various other options.
	You should refer to the manual for the SAB82532 (which can be found on the Fastcom CD
	in the data_sheets directory) so that you can create a set of register values for
	your particular application.
	*/
	esccsettings.mode = 0x88;
	esccsettings.timr = 0x1f;
	esccsettings.xbcl = 0x00;
	esccsettings.xbch = 0x00;
	esccsettings.ccr0 = 0x80;
	esccsettings.ccr1 = 0x16; 
	esccsettings.ccr2 = 0x38; 
	esccsettings.ccr3 = 0x00;
	esccsettings.bgr = 0x00;
	esccsettings.iva = 0;
	esccsettings.ipc = 0x03;
	esccsettings.imr0 = 0x04;	//disable cdsc
	esccsettings.imr1 = 0x0;
	esccsettings.pvr = 0x0;
	esccsettings.pim = 0xff;
	esccsettings.pcr = 0xe0;
	esccsettings.xad1 = 0xff;
	esccsettings.xad2 = 0xff;
	esccsettings.rah1 = 0xff;
	esccsettings.rah2 = 0xff;
	esccsettings.ral1 = 0xff;
	esccsettings.ral2 = 0xff;
	esccsettings.rlcr = 0x00;
	esccsettings.pre = 0x00;
	esccsettings.ccr4 = 0x00;


	/*
	This is where you specify the number and size of the software transmit and 
	receive buffers.
	*/
	esccsettings.n_rbufs = 10;		//min 2 max 1500
	esccsettings.n_rfsize_max = 4096;	//min 64 max 4096
	esccsettings.n_tbufs = 10;	//min 2 max 1500
	esccsettings.n_tfsize_max = 4096;	//min64 max 4096



	/*
	This IOCTL will set the registers of the 82532 to the settings of esccsettings struct
	and reset the TX and RX machines (issuing a separate XRES and RHR commands).
	If the resets time out the function will return FALSE, otherwise it will eturn TRUE.
	This IOCTL must be called to setup the 82532 registers before any reads or writes
	can be issued.
	INPUT SETUP struct
	OUTPUT NULL
	*/
	retvalue = DeviceIoControl(hDevice,IOCTL_ESCCDRV_SETUP,&esccsettings,sizeof(SETUP),NULL,0,&returnsize,NULL);
	if(retvalue==TRUE)printf("SETTINGS SUCCESSFUL:%lu\n\r",returnsize);
	else printf("SETTINGS FAILED:%lu\n\r",returnsize);



	/*
	Specifies additional BISYNC start pattern matching.  After the first two SYN characters 
	have been detected, up to the first sixteen bytes received after the SYN are compared
	to the additional pattern.  If they match, the data is forwarded on to receive buffers
	and data is continued to clock in until a stop condition occurs.  If they do not match,
	the data is flushed out and another HUNT command is issued.

	If you have enabled the bit to pass your SYN characters on as data, then the first 
	bytes of the pattern must match the SYN characters.	
	INPUT BSYNCSTART structure
	OUTPUT NULL

	In this example the data will be checked to see if the first 3 bytes are 0x2E B4 F2.
	If they are, we have achieved sync and all is well otherwise issue HUNT and start over
	*/
	bisyncstart.count = 3;
	bisyncstart.pattern[0] = 0x2E;
	bisyncstart.pattern[1] = 0xB4;
	bisyncstart.pattern[2] = 0xF2;
	DeviceIoControl(hDevice,IOCTL_ESCCDRV_SET_BISYNC_START_PATTERN,&bisyncstart,sizeof(BISYNCSTART),NULL,0,&returnsize,NULL);



	/*
	Enable the pattern matching described in the above command
	datain = 1 will enable/turn on the START_PATTERN feature.
	datain = 0 will disable/turn off the START_PATTERN feature.
	INPUT ULONG
	OUTPUT NULL
	*/
	DeviceIoControl(hDevice,IOCTL_ESCCDRV_BISYNC_START_PATTERN_MATCH_EN,&datain,sizeof(ULONG),NULL,0,&returnsize,NULL);



	/*
	Specifies additional BISYNC end pattern matching.  Up to the last sixteen bytes of 
	received data are compared to the stop pattern.  If they match it is almost the same
	as receiving a TCR character.  No more data after the pattern is handed on to the 
	receive buffer and a HUNT command is issued.
	The usual method is to enable TCD on a 0xFF pattern, and have the receiver re-enter
	HUNT whenever the 0xFF is detected.  This causes problems if you have 8 bit binary
	data, as whenever a 0xFF is in the data, the frame will end abruptly.
	This is ment to be used for detecting the end of standard bisync frames ie:
		DLE ETX x x 0xFF (0x10,0x03,?,?,0xff)


	INPUT BSYNCSTART structure
	OUTPUT NULL

	In this example the data will be checked to see if the first 3 bytes are 0x2E B4 F2.
	If they are, we have achieved sync and all is well otherwise issue HUNT and start over
	*/
	bisyncstart.count = 3;
	bisyncstart.pattern[0] = 0x2E;
	bisyncstart.pattern[1] = 0xB4;
	bisyncstart.pattern[2] = 0xF2;
	DeviceIoControl(hDevice,IOCTL_ESCCDRV_SET_BISYNC_END_PATTERN,&bisyncstart,sizeof(BISYNCSTART),NULL,0,&returnsize,NULL);



	/*
	Enable the pattern matching described in the above command
	datain = 1 will enable/turn on the END_PATTERN feature.
	datain = 0 will disable/turn off the END_PATTERN feature.
	INPUT ULONG
	OUTPUT NULL
	*/
	DeviceIoControl(hDevice,IOCTL_ESCCDRV_BISYNC_END_PATTERN_MATCH_EN,&datain,sizeof(ULONG),NULL,0,&returnsize,NULL);



	/*
	This function sets the point at which a ReadFile() will return in a continuously 
	received data stream.  (in other words,	once the initial SYNL/SYNH pair has been 
	detected by the 82532, it will simply clocks data into the fifo at one bit per clock.
	This function allows you to define a frame end that is not on the buffer size 
	boundary.  Note that the effects of using this are different from just setting 
	n_rfsize_max = 600.  If you set n_rfsize_max = 600 you will NOT get ReadFile() 
	functions returning with the correct data at 600 byte chunks.  More than likely
	you would get 576 bytes back, and an overflow indication.
	This ioctl function correctly handles storage of partial frames 
	(framesizes that are not integer multiples of 32).
	datain can range from a minimum of your pattern length, to a maximum of 4096 bytes)
	(note that you must set n_rfsize_max to be greater than or equal to the size you set
	or you will get an overflow before your desired size is reached)

	INPUT ULONG
	OUTPUT NULL
	*/
	datain = 600;
	DeviceIoControl(hDevice,IOCTL_ESCCDRV_SET_BISYNC_SIZE_CUTOFF_SIZE,&datain,sizeof(ULONG),NULL,0,&returnsize,NULL);



	/*
	Enable the pattern size cutoff described in the above command
	datain = 1 will enable/turn on the SIZE_CUTOFF feature.
	datain = 0 will disable/turn off the SIZE_CUTOFF feature.
	INPUT ULONG
	OUTPUT NULL
	*/
	DeviceIoControl(hDevice,IOCTL_ESCCDRV_BISYNC_SIZE_CUTOFF_EN,&datain,sizeof(ULONG),NULL,0,&returnsize,NULL);



	/*
	This function will enable a conversion of the entire contents of the frame data 
	from LSB first to MSB first.  The 82532 ONLY receives LSB first.  This means that 
	any hardware pattern detections (ie SYNL/SYNH) must match a LSB first received data.
	There are situations where the data on the line is organized as MSB first data.  
	This function can be used to convert the frame contents to their correct values.  
	The converstion occurs just prior to the data being returned to the user (completed 
	ReadFile() call), so any pattern matching must be done on the data assuming that 
	it is received LSB first--adjust your patterns accordingly.
	There is not much of an advantage to doing this converstion in kernel space, other 
	than being a handy feature, the code could just as easily be executed in user space 
	after the readfile is completed. (see the lsb2msb.c file).
	datain = 1 will enable/turn on the size cutoff feature.
	datain = 0 will disable/turn off the size cutoff feature.
	INPUT ULONG
	OUTPUT NULL
	*/
	DeviceIoControl(hDevice,IOCTL_ESCCDRV_LSB2MSB_CONVERT_EN,&datain,sizeof(ULONG),NULL,0,&returnsize,NULL);

/***************************** Read and Write data ****************************/
	/*
	Keep in mind that all printf() commands slow down your operation.
	I have put some in here just to help illustrate what is going on.

	Also, this code segment isn't very useful by itself.  It might be nicer to put
	the read and write in separate threads so both can happen at the same time.
	*/

	for(i=0;i<numbytestowrite;i++) txdata[i] = (char)i; //fill the data up with something

	do
	{
		/*
		Start a write request by calling ReadFile() with the esccdevice handle
		If it returns true then we wrote a frame.
		If it returns false and ERROR_IO_PENDING then there are no buffers 
		available so we wait until the overlapped struct gets signaled.
		*/
		retvalue = WriteFile(hDevice,&txdata,numbytestowrite,&numbyteswritten,&txoverstruct);

		printf("WRITEFILE esccdrv attempting to write %lu bytes\n",numbyteswritten);

		if(retvalue==TRUE) printf("TX returned TRUE\n\r"); //if returned true then the IO request was started (txing has begun)
		else if(retvalue==FALSE)
		{
			if (GetLastError() == ERROR_IO_PENDING)  //IO_PENDING is the indication that the request was queued, if not this than a parameter error occured
			{
			// wait for a second for this transmission to complete
				do
				{
					i = WaitForSingleObject( txoverstruct.hEvent, 1000 );//1 second timeout
					if(i==WAIT_TIMEOUT)
					{
						/*
						this will execute every 1 second
						you could put a counter in here and if the 
						driver takes an inordinate ammout of time
						to complete, you could issue a flush TX, or cancel Tx command
						and break out of this loop
						*/
					}
					if(i==WAIT_ABANDONED)
					{
						//don't know if this will ever happen
					}
					
				}while(i!=WAIT_OBJECT_0);	//write has occured
			}
			else
			{
				printf("\r\na Write ERROR occured\n");
			}
		}
		/*
		Start a read request by calling ReadFile() with the esccdevice handle
		If it returns true then we received a frame.
		If it returns false and ERROR_IO_PENDING then there are no receive frames 
		available so we wait until the overlapped struct gets signaled.
		*/
		retvalue = ReadFile(hDevice,&rxdata,numbytestoread,&numbytesread,&rxoverstruct);
		if(retvalue ==TRUE)
		{
			//we have some data here so do something with it ... display it?
			//display our newly received frame
			printf("received %u bytes:\n\r",numbytesread);
			for(i=0;i<numbytesread;i++)printf("%c",rxdata[i]);
			printf("\n\r");
		}
		else if (GetLastError() == ERROR_IO_PENDING)
		{
			// wait for a receive frame to come in, note it will wait forever
			do
				{
				i = WaitForSingleObject( rxoverstruct.hEvent, 1000 );//1 second timeout
				if(i==WAIT_TIMEOUT)
				{
					/*
					this will execute every 1 second
					you could put a counter in here and if the 
					driver takes an inordinate ammout of time
					to complete, you could issue a flush RX or cancel Rx command
					and break out of this loop
					*/
				}
				if(i==WAIT_ABANDONED)
					{
					}
				
				}while((i!=WAIT_OBJECT_0));
			
				GetOverlappedResult(hDevice,&rxoverstruct,&numbytesread,TRUE); //here to get the actual nobytesread!!!
				printf("received %u bytes:\n\r",numbytesread);    //display the number of bytes received
				for(i=0;i<numbytesread;i++)printf("%c",rxdata[i]);  //display the buffer
				printf("\n");
				retvalue=TRUE;
		}
		else
		{
			printf("\r\na read error has occured\n");
		}
				
	}while(!_kbhit());


/***************************** Device IOCTL's ****************************/
	/*
	The following 2 IOCTL functions are used to read or write individual registers of the 
	82532.
	Note that this should be used sparingly as it forces a transition to the DIRQL 
	of the ESCCP ISR to execute, it is here mainly to do quick changes without having 
	to reinitialize	the entire card, (changing addresses, baud rates, etc)
	*/
	datain = VSTR; //register to read: Version Status Register
	/*
	INPUT (datain) is a ULONG.  It is the offset of the register from the base address.
	You can use the number or the defines in the header file.
	OUTPUT data from that register to another ULONG (dataout).
	*/
	retvalue = DeviceIoControl(hDevice,IOCTL_ESCCDRV_READ_REGISTER,&datain,sizeof(ULONG),&dataout,sizeof(ULONG),&returnsize,NULL);
	if(retvalue) printf("ESCC 82532 version status:%x\r\n",dataout);
	else printf("Error reading\n");



	/*
	Set the transmit address 1 to 0x01
	*/
	regs.port = XAD1;//register to write: Transmit Address 1
	regs.data = 0x01;//byte to be written
	/*
	INPUT (regs) is a regsingle struct.  
	regs.port is the offset of the register from the base address.  You can use the 
	number or the defines in the header file.
	regs.data is the byte to be written to the specified register
	OUTPUT NULL
	*/
	retvalue = DeviceIoControl(hDevice,IOCTL_ESCCDRV_WRITE_REGISTER,&regs,sizeof(struct regsingle),NULL,0,&returnsize,NULL);
	if (retvalue) printf("write successful\n");
	else printf("error on write\n");



	/*
	This IOCTL is used to set the transmit type.  The options are to send transparent 
	frames or information frames (determines if tx is initiated with a XTF command or a 
	XIF command).
	This is only usefull in HDLC mode, it is ignored in ASYNC, and should allways be 
	set to its default (XTF) for BISYNC.
	0 == XTF is used to transmit frames
	1 == XIF is used to transmit frames
	This should only be set when the escc is not transmitting, it will return FALSE if 
	a frame is being transmitted
	This command should only be used to send XIF frames if the escc is setup in AUTO mode
	On startup the tx_type defaults to XTF (so this call isn't really necessary except 
	to show that it exists)
	INPUT ULONG
	OUTPUT NULL
	*/
	datain = 0;
	retvalue = DeviceIoControl(hDevice,IOCTL_ESCCDRV_SET_TX_TYPE,&datain,sizeof(ULONG),NULL,0,&returnsize,NULL);
	if (retvalue) printf("write successful\n");
	else printf("error on write\n");



	/*
	This IOCTL function is for information only as it serves no real purpose.
	It will however, return the number of completed but unread READ buffers being held 
	in the driver.
	This function will return TRUE unless an invalid parameter is given.
	dataout = the number of receive buffers that are ready to be read by ReadFile
	INPUT NULL
	OUTPUT ULONG
	*/
	retvalue = DeviceIoControl(hDevice,IOCTL_ESCCDRV_RX_READY,NULL,0,&dataout,sizeof(ULONG),&returnsize,NULL);
	if (retvalue) 	printf("# receive buffers ready:%lu\n",dataout);
	else printf("error on read\n");



	/*
	This IOCTL is used to change the state of the DTR line
	if datain = 0 is passed the DTR goes low
	if datain = 1 is passed thn DTR goes high
	clear dtr here
	INPUT ULONG
	OUTPUT NULL
	*/
	datain = 0;
	retvalue = DeviceIoControl(hDevice,IOCTL_ESCCDRV_SET_DTR,&datain,sizeof(ULONG),NULL,0,&returnsize,NULL);
	if (retvalue) printf("write successful");
	else printf("error on write\n");



	/*
	This IOCTL is used to check the state of the DTR/DSR lines
	on return the value will be:
	bit 0 = DTR state
	bit 1 = DSR state 
	all others should be masked.
	INPUT NULL
	OUTPUT ULONG
	*/
	dataout = 0; 
	retvalue = DeviceIoControl(hDevice,IOCTL_ESCCDRV_GET_DSR,NULL,0,&dataout,sizeof(ULONG),&returnsize,NULL);
	if (retvalue)
	{
		dataout &=  0x03;
		if((dataout & 1) == 1) printf("DTR SET\r\n");
		else printf("DTR not SET\r\n");
		if((dataout & 2) == 2) printf("DSR SET\r\n");
		else printf("DSR not SET\r\n");
	}
	else printf("error on read\n");



	/*
	Starts the escc timer, notice no parameters passed, if in external mode.
	This should force the status thread to become active with an ST_TIN response 
	at a time specified by the value of k (BGR) and TIMR in the escc registers.
	This function will allways return TRUE.
	INPUT NULL
	OUTPUT NULL
	*/
	retvalue = DeviceIoControl(hDevice,IOCTL_ESCCDRV_START_TIMER,NULL,0,NULL,0,&returnsize,NULL);



	/*
	Stops the escc timer, notice no parameters passed, if in external mode.
	This should prevent a ST_TIN response if the timer was previously started (as above).
	This function will allways return TRUE.
	INPUT NULL
	OUTPUT NULL
	*/
	retvalue = DeviceIoControl(hDevice,IOCTL_ESCCDRV_STOP_TIMER,NULL,0,NULL,0,&returnsize,NULL);



	/*
	Flush the receive buffers.  This will clear all of the internal receive buffers 
	and issue a RHR command to the ESCC.  It will allways return TRUE unless the RHR 
	command times out.
	INPUT NULL
	OUTPUT NULL
	*/
	retvalue = DeviceIoControl(hDevice,IOCTL_ESCCDRV_FLUSH_RX,NULL,0,NULL,0,&returnsize,NULL);
	if (retvalue) printf("RX flushed\r\n");
	else printf("Ooops\n");



	/*
	Flush the transmit buffers.  This will clear all of the internal transmit buffers 
	and issue a XRES command to the ESCC.  It will allways return TRUE unless the XRES 
	command times out.
	INPUT NULL
	OUTPUT NULL
	*/
	retvalue = DeviceIoControl(hDevice,IOCTL_ESCCDRV_FLUSH_TX,NULL,0,NULL,0,&returnsize,NULL);
	if (retvalue) printf("TX flushed\r\n");
	else printf("Ooops\n");



	/*
	Issue a command directly to the 82532 CMDR register.
	You can use the defines in the header file or write byte values directly
	This example issues a HUNT command which is only good in Bisync mode, but it is a 
	good example regardless.
	INPUT ULONG
	OUTPUT NULL
	*/
	datain = HUNT;
	retvalue = DeviceIoControl(hDevice,IOCTL_ESCCDRV_CMDR,&datain,sizeof(ULONG),NULL,0,&returnsize,NULL);
	if (retvalue) printf("HUNT command issued\n\r");
	else printf("Uh oh\n");



	/*
	Sets the Transmit address 1 and 2
	INPUT ULONG
	OUTPUT NULL
	XAD1 = ULONG shifted right 8 bits and ANDed with 0xFC
	XAD2 = ULONG ANDed with 0xFF
	example:
		datain = 0x0000AABB;
		XAD1 = 0xAA;
		XAD2 = 0xBB;
	*/
	datain = 0x1516;
	DeviceIoControl(hDevice,IOCTL_ESCCDRV_SET_TX_ADD,&datain,sizeof(ULONG),NULL,0,&returnsize,NULL);



	/*
	Gets the status info from the driver.
	This IOCTL will not return until the status changes if the escc device was opened 
	with the non-overlapped io.
	Since we opened with overlapped io it will return immeadiately either with the 
	status (if the status bits were set before the call) and the return value will be 
	TRUE or the return value will be FALSE with ERROR_IO_PENDING or a device busy error 
	message.
	The device busy will be returned if a call is made to IOCTL_ESCCDRV_STATUS with an 
	outstanding call in progress.
	The return buffer will hold the current status upon completion of the IO request
	Use Get
	INPUT ULONG
	OUTPUT ULONG

	For a better example of how to use this, refer to the StatProc() function in esccptest.c

	*/
	datain = 0xFFDFFFFE; //(mask out rxdone/txdone messages)
	retvalue = DeviceIoControl(hDevice,IOCTL_ESCCDRV_STATUS,&datain,sizeof(ULONG),&dataout,sizeof(ULONG),&returnsize,&txoverstruct);



	/*
	Queries and returns the same values as IOCTL_ESCCDRV_STATUS, but this one does not
	block.  It returns the status right now and will never wait for an event.
	INPUT ULONG
	OUTPUT ULONG
	*/
	retvalue = DeviceIoControl(hDevice,IOCTL_ESCCDRV_IMMEDIATE_STATUS,&datain,sizeof(ULONG),&dataout,sizeof(ULONG),&returnsize,NULL);
	if(dataout!=0) printf("\r\nSTATUS: %lx\n",dataout);



	/*
	Causes a blocked ReadFile() to unblock with status canceled.  Does not lose data that
	was in the receive FIFO.  Where flushing will reset everything.
	A little bit nicer than flushing the receiver
	INPUT NULL
	OUTPUT NULL
	*/
	DeviceIoControl(hDevice,IOCTL_ESCCDRV_CANCEL_RX,NULL,0,NULL,0,&returnsize,NULL);



	/*
	Causes a blocked WriteFile() to unblock with status canceled.  Does not lose data that 
	was in the transmit FIFO.  Where flushing will reset everything.
	A little bit nicer than flushing the transmitter
	INPUT NULL
	OUTPUT NULL
	*/
	DeviceIoControl(hDevice,IOCTL_ESCCDRV_CANCEL_TX,NULL,0,NULL,0,&returnsize,NULL);



	/*
	Causes a blocked IOCTL_ESCCDRV_STATUS to unblock with status canceled.  Does not lose
	data that was in the status queue.
	INPUT NULL
	OUTPUT NULL
	*/
	DeviceIoControl(hDevice,IOCTL_ESCCDRV_CANCEL_STATUS,NULL,0,NULL,0,&returnsize,NULL);



	/*
	When we want to exit the program there is still the possibility that a frame is 
	being transmitted so we spin in this IOCTL function until the driver reports that 
	it is not transmitting.  
	Technically you could omit this if you are leaving for good (ie are done computing 
	for the day, and are about to shutdown) but it might cause problems if the escc 
	device is started again without powering down; misc errors will occur when the 
	device is re-opened as it was halted in mid transmitting.  EXE interrupts are most likely.
	Will return 0 in output buffer (i) if not active
	Will return 1 in output buffer (i) if active
	INPUT NULL
	OUTPUT ULONG
	*/
	dataout = 0;
	do
	{
		retvalue = DeviceIoControl(hDevice,IOCTL_ESCCDRV_TX_ACTIVE,NULL,0,&dataout,sizeof(ULONG),&returnsize,NULL);
	}while(dataout==1);//keep requesting until not active

	//carefull not to close the device while transmitting or receiving data
	CloseHandle (hDevice);// stops the escc from interrupting (ie shuts it down)

	printf("exiting program\n\r");          //exit message

	/*
	Rx Echo Cancel is useful in 2 wire RS-485 network when you do not want to receive 
	what is transmitted.  This feature will diable the receiver during transmit on a per 
	port basis.  Because it is implemented at the driver level, it will be much more 
	accurate than polling the ALLS status bit.
	datain = 1 receive echo cancel ON
    datain = 0 receive echo cancel OFF
	INPUT ULONG
	OUTPUT NULL
	*/
	datain = 1;
	DeviceIoControl(hDevice,IOCTL_ESCCDRV_RX_ECHO_CANCEL,&datain,sizeof(ULONG),NULL,0,&returnsize,NULL);


}
//end of main
