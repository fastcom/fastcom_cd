/* $Id$ */
/*
Copyright(C) 2002, Commtech, Inc.
read.c -- a user mode program to read bytes from a channel and stuff them to a file

  usage:
  read port size outfile
  
port from SFC to ESCC 4/1/2004
	
*/


#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "..\esccptest.h"

int main(int argc, char *argv[])
{
	HANDLE rDevice;
	FILE *fout;
	ULONG t;
	DWORD nobytesread;
	char *rdata;
	OVERLAPPED  rq;
	int j,error;
	ULONG totalsent;
	ULONG totalread;
	ULONG totalerror;
	ULONG loop;
    char devname[25];
	struct setup esccsetup; //setup structure for initializing the escc registers (see esccptest.h)

	if(argc<3)
	{
		printf("usage:\n");
		printf("read port outfile\n");
		exit(1);
	}
	fout=fopen(argv[2],"wb");
	if(fout==NULL)
	{
		printf("cannot open output file %s\n",argv[2]);
		exit(1);
	}
	
	sprintf(devname,"\\\\.\\ESCC%d",atoi(argv[1]));
	printf("devicename:%s\n",devname);
	
	memset( &rq, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
	rq.hEvent = CreateEvent( NULL,    // no security
		TRUE,    // explicit reset req
		FALSE,   // initial event reset
		NULL ) ; // no name
	if (rq.hEvent == NULL)
	{
		//MessageBox( NULL, "Failed to create event for thread!", "main Error!",
		//	MB_ICONEXCLAMATION | MB_OK ) ;
		printf("Failed to create event for thread!\n");
		fclose(fout);
		exit(1);
	}
	rDevice = CreateFile (devname,
		GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL,
		OPEN_ALWAYS,
		FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
		NULL
		);
	
    if (rDevice == INVALID_HANDLE_VALUE)
    {
		//for some reason the driver won't load or isn't loaded
		printf ("Can't get a handle to esccdrv\n");
		fclose(fout);
		CloseHandle(rq.hEvent);
		exit(1);
		//abort and leave here!!!
	}
	//allocate memory for read/write
	rdata = (char*)malloc(4096);
	if(rdata==NULL)
	{
		printf("cannot allocate memory for data area\n");
		fclose(fout);
		CloseHandle(rDevice);
		CloseHandle(rq.hEvent);
		exit(1);
	}

	esccsetup.mode = 0xe0;//Extended transparent mode 1 receiver active
	esccsetup.timr = 0x00;//not used
	esccsetup.xbcl = 0x00;//not used
	esccsetup.xbch = 0x00;//not using DMA note DMAR must = DMAT = 0 in ntinstall
	esccsetup.ccr0 = 0x80;//power up, NRZ, HDLC mode, no master clock
	esccsetup.ccr1 = 0x10;//clock mode 7b uses txclk as output using BGR
	esccsetup.ccr2 = 0x18;
	esccsetup.ccr3 = 0x00;//(preamble and other features that are turned off or not used)
	esccsetup.bgr = 0x00;
	esccsetup.iva = 0;//must be
	esccsetup.ipc = 0x03;//must be
	esccsetup.imr0 = 0x04;//could mask more of the receive irq's but they won't happen anyway
	esccsetup.imr1 = 0x00;//must have XPR, should keep XDU/EXE and ALLS on as well
	esccsetup.pvr = 0x0;
	esccsetup.pim = 0xff;
	esccsetup.pcr = 0xe0;
	esccsetup.xad1 = 0xff;//must be for extended transparent mode
	esccsetup.xad2 = 0xff;//must be for extended transparent mode
	esccsetup.rah1 = 0xff;//must be for extended transparent mode
	esccsetup.rah2 = 0xff;//must be for extended transparent mode
	esccsetup.ral1 = 0xff;//must be for extended transparent mode
	esccsetup.ral2 = 0xff;//must be for extended transparent mode
	esccsetup.rlcr = 0x00;//not used
	esccsetup.pre = 0x00;//if you setup the preamble...this is it
	esccsetup.ccr4 = 0x00;//no ebgr or master clk/4.
	
	esccsetup.n_rbufs = 10;//number of receive buffers
	esccsetup.n_tbufs = 10;//number of transmit buffers
	esccsetup.n_rfsize_max = 4096;//receive buffer size
	esccsetup.n_tfsize_max = 4096;//transmit buffer size
	
	
	//when called this IOCTL will set the registers of the escc and
	//reset the TX and RX machines (issuing a separate XRES and RHR command)
	//if the resets time out the function will return FALSE, otherwise it will
	//return TRUE
	t = DeviceIoControl(rDevice,IOCTL_ESCCDRV_SETUP,&esccsetup,sizeof(struct setup),NULL,0,&t,NULL);
	if(t==TRUE)printf("SETTINGS SUCCESSFUL:%lu\n\r",t);
	if(t==FALSE)
	{
		printf("SETTINGS FAILED:%lu\n\r",t);
		CloseHandle(rDevice);
		exit(1);
	}

	/* Flush the RX Descriptors so not as to have any complete descriptors in their
	* the first read in hdlc will get those left over frames and this test program
	* would not be of any use. */
	//printf("flush rx\n");
	DeviceIoControl(rDevice,IOCTL_ESCCDRV_FLUSH_RX,NULL,0,NULL,0,&t,NULL);
	
	totalerror=0;
	totalsent=0;
	totalread=0;
	loop=0;
	
	while(!kbhit())
	{
		
		error=0;
		t = ReadFile(rDevice,&rdata[0],4096,&nobytesread,&rq);
		if(t==FALSE)  
		{
			//	printf("read blocked\n");
			t=GetLastError();
			if(t==ERROR_IO_PENDING)
			{
				do
				{
					j = WaitForSingleObject( rq.hEvent, 5000 );//5 second timeout -- must be greater than size*8*(1/bitrate)*1000
					//			printf("after wait:%lx\n",j);
					if(j==WAIT_TIMEOUT)
					{
						if(kbhit()) goto done;
						printf("Reciever Locked up... Resetting RX.\r\n");
						//DeviceIoControl(rDevice,IOCTL_ESCCDRV_FLUSH_TX,NULL,0,NULL,0,&returnsize,NULL);
					}
					if(j==WAIT_ABANDONED)
					{
						printf("Reciever Abandoned.\r\n");
						goto close;
					}
				} while(j!=WAIT_OBJECT_0);
				GetOverlappedResult(rDevice,&rq,&nobytesread,TRUE);
				//		printf("postblock read:%d\n",nobytesread);
			}
			else printf("READ ERROR: #%x\n",t);
		}
		//      else printf("read returned true:%d\n",nobytesread);
		
		printf("[%d]READ %d\n\n",t,nobytesread);
		if(nobytesread!=4096)
		{
			printf("received:%lu, expected 4096\n",nobytesread);
		}
		if(nobytesread!=0)
		{
			totalsent+=fwrite(rdata,1,4096,fout);
		}
		//printf("Found: %d errors\n",error);
		loop++;
		totalread+=nobytesread;
	}
done:
	getch();
	printf("Read  %lu bytes\n",totalread);
	printf("Wrote %lu bytes\n",totalsent);
	printf("Count %lu\n",loop);
	
	
close:
	free(rdata);
	fclose(fout);
	CloseHandle(rDevice);
	CloseHandle(rq.hEvent);
	return 0;
}
/* $Id$ */