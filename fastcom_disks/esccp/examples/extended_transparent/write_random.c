/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
write_random.c -- a user mode program to write a random bitstream (with counter) to a port

  usage:
  write_random port size [savefile]
  
	port from SFC to ESCC 3/30/2004
	size is the blocksize to use for writes
	savefile is the optional file to dump the data to as well as pumping it out the port
	
*/


#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "..\esccptest.h"

int main(int argc, char *argv[])
{
	HANDLE wDevice;/* Handle for the ESCC port */
	ULONG t;
	DWORD nobyteswritten;
	char *tdata;
	ULONG size;
	OVERLAPPED  wq;
	int j,x,error,tosend;
	ULONG totalsent;
	ULONG totalread;
	ULONG totalerror;
	ULONG loop;
    char devname[25];
	FILE *fout=NULL;
	struct setup esccsetup; //setup structure for initializing the escc registers (see esccptest.h)
	struct clkset clk;

	
	if(argc<3)
	{
		printf("usage:\n");
		printf("write_random port blocksize [savefile]\n");
		exit(1);
	}
	if(argc>3)
	{
		fout = fopen(argv[3],"wb");
		if(fout==NULL)
		{
			printf("cannot open save file\n");
			exit(1);
		}
	}
	srand( (unsigned)time( NULL ) );
	size = atol(argv[2]);
	if(size==0)
	{
		printf("block size must be nonzero\n");
		exit(1);
	}
    sprintf(devname,"\\\\.\\ESCC%d",atoi(argv[1]));
	printf("devicename:%s\n",devname);
	printf("blocksize:%lu\n",size);
	
	memset( &wq, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
	wq.hEvent = CreateEvent( NULL,    // no security
		TRUE,    // explicit reset req
		FALSE,   // initial event reset
		NULL ) ; // no name
	if (wq.hEvent == NULL)
	{
		//MessageBox( NULL, "Failed to create event for thread!", "main Error!",
		//	MB_ICONEXCLAMATION | MB_OK ) ;
		printf("Failed to create event for thread!\n");
		return 1; 
	}
	//	printf("write overlapped event created\n");
	wDevice = CreateFile (devname,
		GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL,
		OPEN_ALWAYS,
		FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
		NULL
		);
    if (wDevice == INVALID_HANDLE_VALUE)
    {
		//for some reason the driver won't load or isn't loaded
		printf ("Can't get a handle to esccdrv\n");
		exit(1);
		//abort and leave here!!!
	}
	//printf("esccdrv handle created\n");
	tdata = (char*)malloc(size+1);
	if(tdata==NULL)
	{
		printf("unable to allocate data buffer\n");
		exit(1);
	}

	//Set clock generator to 18.432MHz
	
	clk.clockbits = 0x21d110;
	clk.numbits = 23;
	if(DeviceIoControl(wDevice,IOCTL_ESCCDRV_SET_CLOCK,&clk,sizeof(struct clkset),NULL,0,&t,NULL))
	{
		printf("Actual Frequency Set to: 18.432MHz \n");
	}

	esccsetup.mode = 0xc0;//Extended transparent mode 1 receiver inactive
	esccsetup.timr = 0x00;//not used
	esccsetup.xbcl = 0x00;//not used
	esccsetup.xbch = 0x00;//not using DMA note DMAR must = DMAT = 0 in ntinstall
	esccsetup.ccr0 = 0x80;//power up, NRZ, HDLC mode, no master clock
	esccsetup.ccr1 = 0x17;//clock mode 7b uses txclk as output using BGR
	esccsetup.ccr2 = 0x38;
	esccsetup.ccr3 = 0x00;//(preamble and other features that are turned off or not used)
	esccsetup.bgr = 0xbf;//set baud to 9600
	esccsetup.iva = 0;//must be
	esccsetup.ipc = 0x03;//must be
	esccsetup.imr0 = 0x04;//could mask more of the receive irq's but they won't happen anyway
	esccsetup.imr1 = 0x00;//must have XPR, should keep XDU/EXE and ALLS on as well
	esccsetup.pvr = 0x0;
	esccsetup.pim = 0xff;
	esccsetup.pcr = 0xe0;
	esccsetup.xad1 = 0xff;//must be for extended transparent mode
	esccsetup.xad2 = 0xff;//must be for extended transparent mode
	esccsetup.rah1 = 0xff;//must be for extended transparent mode
	esccsetup.rah2 = 0xff;//must be for extended transparent mode
	esccsetup.ral1 = 0xff;//must be for extended transparent mode
	esccsetup.ral2 = 0xff;//must be for extended transparent mode
	esccsetup.rlcr = 0x00;//not used
	esccsetup.pre = 0x00;//if you setup the preamble...this is it
	esccsetup.ccr4 = 0x00;//no ebgr or master clk/4.
	
	esccsetup.n_rbufs = 10;//number of receive buffers
	esccsetup.n_tbufs = 100;//number of transmit buffers
	esccsetup.n_rfsize_max = 4096;//receive buffer size
	esccsetup.n_tfsize_max = 4096;//transmit buffer size
	
	
	//when called this IOCTL will set the registers of the escc and
	//reset the TX and RX machines (issuing a separate XRES and RHR command)
	//if the resets time out the function will return FALSE, otherwise it will
	//return TRUE
	t = DeviceIoControl(wDevice,IOCTL_ESCCDRV_SETUP,&esccsetup,sizeof(struct setup),NULL,0,&t,NULL);
	if(t==TRUE)printf("SETTINGS SUCCESSFUL:%lu\n\r",t);
	if(t==FALSE)
	{
		printf("SETTINGS FAILED:%lu\n\r",t);
		CloseHandle(wDevice);
		exit(1);
	}
	
	DeviceIoControl(wDevice,IOCTL_ESCCDRV_FLUSH_TX,NULL,0,NULL,0,&t,NULL);
	
	totalerror=0;
	totalsent=0;
	totalread=0;
	loop=0;
	
	while(!kbhit())
	{
		error=0;
		
		// generate a random length 1 - 4095 bytes
		tosend=size;
		// generate a random string of our random length.
		for(x=0;x<tosend;x++) 
			tdata[x]=(UCHAR)(rand());
		sprintf(&tdata[10],"%c%c%8.8lu%c%c",0xff,0x00,loop,0x00,0xff);//put our frame counter in ascii in the data
		//printf("pre-write\n");
		t = WriteFile(wDevice,&tdata[0],tosend,&nobyteswritten,&wq);
		
//				Sleep(1000);	
		
		if(t==FALSE)  
		{
			t=GetLastError();
			
			
			if(t==ERROR_IO_PENDING)
			{
				do
				{
					j = WaitForSingleObject( wq.hEvent, 1000 );//5 second timeout -- must be larger than size*8*(1/bitrate)*1000
					if(j==WAIT_TIMEOUT)
					{
						printf("Transmitter Locked up... Resetting TX.\r\n");
						//DeviceIoControl(wDevice,IOCTL_ESCCDRV_FLUSH_TX,NULL,0,NULL,0,&returnsize,NULL);
					}
					if(j==WAIT_ABANDONED)
					{
						printf("Recieve Abandoned.\r\n");
						goto close;
					}
				} while(j!=WAIT_OBJECT_0);
				GetOverlappedResult(wDevice,&wq,&nobyteswritten,TRUE);
			}
			else printf("WRITE ERROR: #%d\n",t);
		}
		if(nobyteswritten!=size)
		{
			printf("unexpected actual:%lu, sent:%lu\n",nobyteswritten,size); 
		}
		loop++;
		totalsent+=nobyteswritten;
		if(fout!=NULL) fwrite(tdata,1,tosend,fout);
	}
	getch();
	printf("Wrote %lu bytes\n",totalsent);
	printf("count %lu\n",loop);
	
close:
	free(tdata);
	CloseHandle(wq.hEvent);
	CloseHandle (wDevice);
	if(fout!=NULL) fclose(fout);
	return 0;
	}
	/* $Id$ */