/*++

Copyright (c) 1998,1999 Commtech, Inc Wichita ,KS

Module Name:

    reconly.c

Abstract:

    A simple console app that will do receive only.
	Bare bones receive HDLC application.
	
	reconly prog modified to:
	take bitrate and line encoding from command line 
	then display SDLC frame information as frames are received
	
Environment:

    user mode only

Notes:

    
Revision History:

    8/25/98             started
	10/27/99                        mod for bitrate/line encoding from cmd line
	12/23/02	remove bgr=0 from HDLC settings; would overide previous value of N
--*/



#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "..\..\esccptest.h"

void decode_sdlc(char *buffer,unsigned size);
BOOL calculate_bits(double reffreq,double desired_freq,double *m_actual_clock,CLKSET *clk);

void main(int argc, char *argv[])
{
HANDLE hDevice;                 //handle to the escc driver/device
struct clkset clk;
struct setup esccsetup; //setup structure for initializing the escc registers (see esccptest.h)
char data[4096];                //character array for data storage (passing to and from the driver)
DWORD nobytestoread;    //the number of bytes to read from the driver
DWORD nobytesread;
ULONG i,j,k;                            //temp vars
DWORD returnsize;               //temp vars
ULONG timeout;
BOOL t;                                 //temp vars
OVERLAPPED  os;                         //overlapped structure for use in the receive routine
DWORD debug;
double referenceclock;
double desiredclock;
double actualclock;
double bitrate;
DWORD N;

if(argc<3)
{
printf("Usage:\r\n");
printf("sdlcsnoop bitrate [NRZ|NRZI|FM0|FM1|MAN]\r\n");
exit(1);
}
bitrate = atof(argv[1]);
if(bitrate==0.0) bitrate = 1000000.0; //if err use 1M
printf("SDLC SNOOPER (C)1999 Commtech, Inc.\r\n");

esccsetup.ccr0 = 0x80;//default to NRZ.
if( (strcmp(argv[2],"NRZ")==0)||(strcmp(argv[2],"nrz")==0) )
	{
	esccsetup.ccr0 = 0x80;
	printf("Using NRZ line encoding\r\n");
	}
if( (strcmp(argv[2],"NRZI")==0)||(strcmp(argv[2],"nrzi")==0) )
	{
	esccsetup.ccr0 = 0x88;
	printf("Using NRZI line encoding\r\n");
	}
if( (strcmp(argv[2],"FM0")==0)||(strcmp(argv[2],"fm0")==0) ) 
	{
	esccsetup.ccr0 = 0x90;
	printf("Using FM0 line encoding\r\n");
	}
if( (strcmp(argv[2],"FM1")==0)||(strcmp(argv[2],"fm1")==0) )
	{
	esccsetup.ccr0 = 0x94;
	printf("Using FM1 line encoding\r\n");
	}
if( (strcmp(argv[2],"MAN")==0)||(strcmp(argv[2],"man")==0) )
	{
	esccsetup.ccr0 = 0x98;
	printf("Using MANCHESTER line encoding\r\n");
	}


referenceclock= 18432000.0;
//determine clock frequency (based on clock mode 6b, recovering clock using DPLL);(if you have an external clock then you must modify this, use mode 4 or 0 and skip all this bitrate stuff from the command line)
desiredclock = bitrate * 16.0;
if(desiredclock >= 391000.0)
	{
	//use clock generator directly
	esccsetup.ccr2 = 0x18;//(BDF=0, forces inputclock/1 as ouput of bgr) 
	esccsetup.bgr = 0x00;//not used
	desiredclock = bitrate*16.0;
	calculate_bits(referenceclock,desiredclock,&actualclock,&clk);
	printf("data rate used:%12.2f\r\n",actualclock/16.0);
	}
else
	{
	//must divide down to get rate
	esccsetup.ccr2 = 0x38;//(BDF=1, forces inputclock/((N+1)*2) as ouput of bgr) 
	N = (((unsigned)(400000.0/(bitrate*16.0))+2)/2)-1;//force inclock to be >400k
	desiredclock = bitrate*((N+1)*2)*16.0;//calcluate what inclock needs to be to get rate using N
	//make sure that N is attainable;
	//for simplicity limit N to 0-255, could actually go to 1023 by modifying CCR2 bits 7 and 8
	if(N>255)
		{
		printf("can't get to %12.2f easily\r\n",bitrate*16.0);
		//be nice and close handles here
		exit(1);
		}
	esccsetup.bgr = N;
//      printf("N:%u,Desired clock:%12.2f\r\n",N,desiredclock);
	calculate_bits(referenceclock,desiredclock,&actualclock,&clk);
	printf("data rate used:%12.2f\r\n",(actualclock/((N+1)*2))/16.0);
	}


debug = 0;//set to 1 to get escc status messages with data


memset( &os, 0, sizeof( OVERLAPPED ) ) ;        //wipe the overlapped struct

// create I/O event used for overlapped structure

os.hEvent = CreateEvent( NULL,    // no security
			    TRUE,    // explicit reset req
			    FALSE,   // initial event reset
			    NULL ) ; // no name
if (os.hEvent == NULL)
   {
      MessageBox( NULL, "Failed to create event for thread!", "main Error!",
		  MB_ICONEXCLAMATION | MB_OK ) ;
      return; 
   }


nobytestoread = 4096;    //this should allways be 4096, it is the maximum frame size returned from the driver at one time

    
//this will start up the driver, it also gives us a handle to send
//and receive data from the driver
//note that it is created with the FILE_FLAG_OVERLAPPED set
//if it is not then you must make sequential calls to the driver
//(ie wait until one call has returned before making another call)
//the read/write and status functions are set up to use overlapped, they 
//will return immediatly with a ERROR_IO_PENDING or immediatly with the required data
//


    hDevice = CreateFile ("\\\\.\\ESCC0",
			  GENERIC_READ | GENERIC_WRITE,
			  0,
			  NULL,
			  OPEN_EXISTING,
			  FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
			  NULL
			  );

    if (hDevice == INVALID_HANDLE_VALUE)
    {
		//for some reason the driver won't load or isn't loaded
	printf ("Can't get a handle to esccdrv\n");
		exit(1);
		//abort and leave here!!!
	}
	
//this IOCTL function demonstrates how to set the clock generator on 
//the escc card
//this function will return TRUE unless an invalid parameter is given
//
//clk is generated above
t = DeviceIoControl(hDevice,IOCTL_ESCCDRV_SET_CLOCK,&clk,sizeof(struct clkset),NULL,0,&returnsize,NULL);

	
	
	//to initialize the escc to esccsetup parameters
	//this IOCTL function must be called to setup the escc internal 
	//registers prior to calling any of the read/write functions
	//for information reguarding what each register does refer to 
	//the SIEMENS 82532 data sheet.
	//

		//make to use HDLC settings here
//note ccr0 and ccr2 are set above.
		esccsetup.mode = 0x88;//transparent mode 0 receiver active
		esccsetup.timr = 0x1f;//not used
		esccsetup.xbcl = 0x00;//not used
		esccsetup.xbch = 0x00;//not using DMA note DMAR must = DMAT = 0 in ntinstall
		esccsetup.ccr1 = 0x16;//clock mode 6b uses txclk as output using BGR/16. input clock = 1MHz (dpll recovered from RD), output = (16E6/1)/16 = 1.0 Mbps output, idle = flags
		esccsetup.ccr3 = 0x00;
		esccsetup.iva = 0;
		esccsetup.ipc = 0x03;
		esccsetup.imr0 = 0x04;
		esccsetup.imr1 = 0x00;
		esccsetup.pvr = 0x0;
		esccsetup.pim = 0xff;
		esccsetup.pcr = 0xe0;
		esccsetup.xad1 = 0xff;
		esccsetup.xad2 = 0xff;
		esccsetup.rah1 = 0xff;
		esccsetup.rah2 = 0xff;
		esccsetup.ral1 = 0xff;
		esccsetup.ral2 = 0xff;
		esccsetup.rlcr = 0x00;
		esccsetup.pre = 0x00;
		esccsetup.ccr4 = 0x00;
		
		esccsetup.n_rbufs = 20;//20 receive buffers
		esccsetup.n_tbufs = 2;//2 transmit buffers
		esccsetup.n_rfsize_max = 4096;//size of receive buffers (max)
		esccsetup.n_tfsize_max = 4096;//size of transmit buffers (max)


	//when called this IOCTL will set the registers of the escc and
	//reset the TX and RX machines (issuing a separate XRES and RHR command)
	//if the resets time out the function will return FALSE, otherwise it will
	//return TRUE
	t = DeviceIoControl(hDevice,IOCTL_ESCCDRV_SETUP,&esccsetup,sizeof(struct setup),NULL,0,&returnsize,NULL);
	if(t==TRUE)printf("SETTINGS SUCCESSFUL:%lu\n\r",returnsize);
	if(t==FALSE)
		{
		printf("SETTINGS FAILED:%lu\n\r",returnsize);
		exit(1);
		}

start:
do
	{

	//start a read request by calling ReadFile() with the esccdevice handle
	//if it returns true then we received a frame 
	//if it returns false and ERROR_IO_PENDING then there are no
	//receive frames available so we wait until the overlapped struct
	//gets signaled. (indicating that a frame has been received)
	timeout = 0;
	t = ReadFile(hDevice,&data,nobytestoread,&nobytesread,&os);
	//if this returns true then your system is not reading the frames from the 
	//driver as fast as they are coming in.
	//if it returns false then the system is processing the frames quick enough
	
	if(t==FALSE)
		{
		if (GetLastError() == ERROR_IO_PENDING)
			{
			// wait for a receive frame to come in
			do
				{
				j = WaitForSingleObject( os.hEvent, 1000 );//1 second timeout
				if(j==WAIT_TIMEOUT)
					{
					//this will execute every 1 second that a frame is not received
					//you could put a counter in here and if the 
					//driver takes an inordinate ammout of time
					//to complete, you could issue a flush RX command
					//and break out of this loop
					timeout++;
					if(timeout > 60) 
						{
						printf("no data received for 1 minute...aborting\r\n");
						goto quitprog;
						}
					if(kbhit())
						{
						if(getch()==27)
							{
							printf("exiting on keypress\r\n");
							goto quitprog;
							}
						}
					}
				if(j==WAIT_ABANDONED)
					{
					}
				
				}while(j!=WAIT_OBJECT_0);//stay here until we get signaled
			
				
			}
		}                                                     
		GetOverlappedResult(hDevice,&os,&nobytesread,TRUE); //here to get the actual nobytesread!!!
		if(nobytesread>=1)
			{
			for(i=0;i<40;i++)printf("-");
			printf("\r\n");
			decode_sdlc(data,nobytesread);
			for(i=0;i<40;i++)printf("-");
			printf("\r\n");
			}
		else
			{
			for(i=0;i<40;i++)printf("-");
			printf("\r\n");
			printf("received 0 byte frame\r\n");
			for(i=0;i<40;i++)printf("-");
			printf("\r\n");
			}
		if(debug==1)
			{
			DeviceIoControl(hDevice,IOCTL_ESCCDRV_IMMEDIATE_STATUS,NULL,0,&k,sizeof(DWORD),&returnsize,NULL);
			if((k&~ST_RX_DONE)!=0) printf("\r\nSTATUS: %lx\r\n",k);
				//you could decode the returned here 
				//they are the ST_XXXXXX defines in the .h file
				//basically if you get a ST_OVF or a ST_RFO then you are having
				//problems getting the data through the system
				//(the RFO is a hardware overflow)
				//the OVF is a software (driver buffers) overflow.
			}
	}while(!kbhit());         
	j = getch();
if(j!=27) goto start;
quitprog:
CloseHandle (os.hEvent);//done with the event
CloseHandle (hDevice);// stops the escc from interrupting (ie shuts it down)
}                                               //done


//function that decodes SDLC frames
void decode_sdlc(char *buffer,unsigned size)
{
unsigned j,i;

j = size;

if(j>=2)
{
printf("address:0x%x\r\n",buffer[0]&0xff);
printf("control:0x%x\r\n",buffer[1]&0xff);              
if((buffer[1]&0x01)==0)
	{
	printf("I Frame, NR = %u, NS = %u p/f = %u\r\n",(buffer[1]&0xe0)>>5,(buffer[1]&0x0e)>>1,(buffer[1]&0x10)>>4);
	}     
if((buffer[1]&0x0f)==0x01)
	{
	printf("S-Frame-RR: NR = %u, p/f = %u\r\n",(buffer[1]&0xe0)>>5,(buffer[1]&0x10)>>4);
	}       
if((buffer[1]&0x0f)==0x05)
	{
	printf("S-Frame-RNR: NR = %u, p/f = %u\r\n",(buffer[1]&0xe0)>>5,(buffer[1]&0x10)>>4);
	}       
if((buffer[1]&0x0f)==0x09)
	{
	printf("S-Frame-REJ: NR = %u, p/f = %u\r\n",(buffer[1]&0xe0)>>5,(buffer[1]&0x10)>>4);
	}       
if((buffer[1]&0xff)==0x03) printf("Nonsequenced-NSI p/f = 0\r\n");
if((buffer[1]&0xff)==0x13) printf("Nonsequenced-NSI p/f = 1\r\n");
if((buffer[1]&0xff)==0x07) printf("Nonsequenced-RQI\r\n");
if((buffer[1]&0xff)==0x17) printf("Nonsequenced-SIM\r\n");
if((buffer[1]&0xff)==0x93) printf("Nonsequenced-SNRM\r\n");
if((buffer[1]&0xff)==0x0F) printf("Nonsequenced-ROL\r\n");
if((buffer[1]&0xff)==0x53) printf("Nonsequenced-DISC\r\n");
if((buffer[1]&0xff)==0x63) printf("Nonsequenced-NSA\r\n");
if((buffer[1]&0xff)==0x87) printf("Nonsequenced-CMDR\r\n");
if((buffer[1]&0xff)==0x33) printf("Nonsequenced-ORP\r\n");



printf("data:\r\n");
for(i=2;i<j-1;i++) printf("0x%x,",buffer[i]&0xff);
printf("\r\n"); 
}
if(j>=1)
{
printf("RSTA:0x%x\r\n",buffer[j-1]&0xff);
if((buffer[j-1]&0x20)==0x20) printf("CRC passed\r\n");
else printf("CRC failed\r\n");
if((buffer[j-1]&0x40)==0x40) printf("Receive Data Overflow (hardware)\r\n");
if((buffer[j-1]&0x80)==0x00) printf("Invalid frame\r\n");
if((buffer[j-1]&0x10)==0x10) printf("Receive Frame Abort indicated\r\n");
printf("\r\n");
}
}

//function to calcluate input clock params.
BOOL calculate_bits(double reffreq,double desired_freq,double *m_actual_clock,CLKSET *clk)
{
DWORD P;
DWORD Pprime;
DWORD Q;
DWORD Qprime;
DWORD M;
DWORD I;
DWORD D;
double fvco;
double desired_ratio;
double actual_ratio;
DWORD bestP;
DWORD bestQ;
DWORD bestM;
DWORD bestI;
double best_actual_ratio;
DWORD Progword;
DWORD Stuffedword;
DWORD bit1;
DWORD bit2;
DWORD bit3;
DWORD i,j;
char buf[256];
unsigned long rangelo;
unsigned long rangehi;
double best_diff;

D = 0x00000000;//from bitcalc, but the datasheet says to make this 1...
bestP = 0;
bestQ = 0;
best_actual_ratio = 1000000.0;//hopefully we can do better than this...
best_diff = 1000000.0;
rangelo = (unsigned long)floor(reffreq/1000000.0) +1;
rangehi = (unsigned long)floor(reffreq/200000.0);
if(rangelo <3) rangelo = 3;
if(rangehi>129) rangehi = 129;
sprintf(buf,"%u(%f), %u(%f)",rangelo,(reffreq/1000000.0) +1.0,rangehi,(reffreq/200000.0));
//MessageBox(buf,"rl(fl),rh(fh)",MB_OK);
for(i=0;i<=7;i++)
{
M = i;
fvco = desired_freq * (pow(2,i));
//sprintf(buf,"%f",fvco);
//MessageBox(NULL,buf,"Fvco",MB_OK);

if(fvco<80000000.0) I = 0x00000000;
if(fvco>=80000000.0) I = 0x00000008;
if((fvco>50000000.0)&&(fvco<150000000.0))
{
sprintf(buf,"%f",fvco);
//MessageBox(NULL,buf,"Fvco",MB_OK);
desired_ratio = fvco/(2.0 * reffreq);
sprintf(buf,"%f",desired_ratio);
//MessageBox(NULL,buf,"desired",MB_OK);

for(P=4;P<=130;P++)for(Q=rangelo;Q<=rangehi;Q++)
{

actual_ratio = (double)P/(double)Q;
if(actual_ratio==desired_ratio) 
	{
	sprintf(buf,"%u,%u",P,Q);
	//MessageBox(NULL,buf,"Direct Hit",MB_OK);
	bestP = P;
	bestQ = Q;
	bestM = M;
	bestI = I;
	best_actual_ratio = actual_ratio;
	goto donecalc;
	}
else 
	{
	if(fabs(desired_ratio - actual_ratio)<(best_diff)) 
		{
		best_diff = fabs(desired_ratio - actual_ratio);
		best_actual_ratio = actual_ratio;
		bestP = P;
		bestQ = Q;
		bestM = M;
		bestI = I;
		sprintf(buf,"desired:%f,actual:%f, best%f P%u,Q%u,fvco:%f,M:%u",desired_ratio,actual_ratio,best_diff,bestP,bestQ,fvco,M);
		//MessageBox(buf,"ratiocalc",MB_OK);
		}
	}       
}
}
}
donecalc:
if((bestP!=0)&&(bestQ!=0))
	{
	//here bestP BestQ are good to go.
	I = bestI;
	M = bestM;
	P = bestP;
	Q = bestQ;
	Pprime = bestP - 3;
	Qprime = bestQ - 2;
	sprintf(buf,"P':%u, Q':%u, M:%u, I:%u",Pprime,Qprime,M,I);
	//MessageBox(buf,"P,Q,M,I",MB_OK);
	Progword = 0;
	Progword =  (Pprime<<15) | (D<<14) | (M<<11) | (Qprime<<4) | I;
	sprintf(buf,"%lx",Progword);
//      MessageBox(buf,"Progword",MB_OK);
	bit1 = 0;
	bit2 = 0;
	bit3 = 0;
	Stuffedword = 0;
	i = 0;
	j = 0;
	bit1 = ((Progword>>i)&1);
	Stuffedword |=  (bit1<<j);
	i++;
	j++;
	bit2 = ((Progword>>i)&1);
	Stuffedword |=  (bit2<<j);
	i++;
	j++;
	bit3 = ((Progword>>i)&1);
	Stuffedword |=  (bit3<<j);
	j++;
	i++;
	while(i<=22)
		{
		if((bit1==1)&&(bit2==1)&&(bit3==1))
			{
			//force a 0 in the stuffed word;
			j++;
			bit3 = 0;
			sprintf(buf,"i,j : %u,%u",i,j);
//                      MessageBox(buf,"Stuffing",MB_OK);
			}
		bit1 = bit2;
		bit2 = bit3;
		bit3 = ((Progword>>i)&1);
		Stuffedword |=  (bit3<<j);
		i++;
		j++;
		}
	sprintf(buf,"SW:%lx ,numbits:%u",Stuffedword,j);

clk->clockbits = Stuffedword;
clk->numbits = (USHORT)j-1;
*m_actual_clock = ((2.0 * reffreq) * ((double)P/(double)Q)) / pow(2,M);
//      MessageBox(buf,"stuffedword, numbits",MB_OK);
return TRUE;
	}
else
{
	printf("\r\nError in ICD calculation\r\n");
	return FALSE;

}
}
