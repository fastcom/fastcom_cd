These example programs will compile and run in Windows NT, 2000, and XP.

If you are using any other operating system, you can use these programs as 
reference, but they will not compile for you.

For basic programming using the ESCC-PCI, see howtoprogram.c

Note, NT users must compile their programs using the esccptestNT.h header file, 2K and XP users must compile their programs using the esccptest.h header file.