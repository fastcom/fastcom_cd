/*++

Copyright (c) 1998,1999 Commtech, Inc Wichita ,KS

Module Name:

    tronly.c

Abstract:

    A simple Win32 app that uses the esccdrv device to transmit only
	--modified to generate and send SDLC frames
	designed to generate frames to be received by the SDLCSNOOP example program
	(with a SD+(cable#2) ->RD+(cable#1), SD-(cable#2) ->RD-(cable#1) connection.)
	(this is a SD->RD loop from ESCC1 to ESCC0).

Environment:

    user mode only

Notes:

    
Revision History:

    6/8/98             started
	10/27/99  --mod for SDLC frame send
--*/



#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "..\..\esccptest.h"


BOOL calculate_bits(double reffreq,double desired_freq,double *m_actual_clock,CLKSET *clk);

VOID
main(int argc, char *argv[]
    )
/*++

Routine Description:

Arguments:

Return Value:

--*/
{
HANDLE hDevice;                 //handle to the escc driver/device
DWORD nobyteswritten;   //number of bytes that the driver returns as written to the device
struct setup esccsetup; //setup structure for initializing the escc registers (see esccptest.h)
struct clkset clk;
char buffer[4096];                //character array for data storage (passing to and from the driver)
ULONG j;                                //temp vars
unsigned i;
unsigned key;
DWORD k;
DWORD returnsize;               //temp vars
BOOL t;                                 //temp vars
OVERLAPPED  os ;                                //overlapped structure for use in the transmit routine
double bitrate;
double desiredclock;
double referenceclock;
double actualclock;
unsigned N;
unsigned ns,nr,pf;
unsigned address;

if(argc<2)
{
printf("Usage:\r\n");
printf("sendit [bitrate] [NRZ|NRZI|FM0|FM1|MAN]\r\n");
exit(1);
}

bitrate = atof(argv[1]);
if(bitrate==0.0) bitrate = 1000000.0; //if err use 1M

esccsetup.ccr0 = 0x80;//default to NRZ.
if( (strcmp(argv[2],"NRZ")==0)||(strcmp(argv[2],"nrz")==0) )
	{
	esccsetup.ccr0 = 0x80;
	printf("Using NRZ line encoding\r\n");
	}
if( (strcmp(argv[2],"NRZI")==0)||(strcmp(argv[2],"nrzi")==0) )
	{
	esccsetup.ccr0 = 0x88;
	printf("Using NRZI line encoding\r\n");
	}
if( (strcmp(argv[2],"FM0")==0)||(strcmp(argv[2],"fm0")==0) ) 
	{
	esccsetup.ccr0 = 0x90;
	printf("Using FM0 line encoding\r\n");
	}
if( (strcmp(argv[2],"FM1")==0)||(strcmp(argv[2],"fm1")==0) )
	{
	esccsetup.ccr0 = 0x94;
	printf("Using FM1 line encoding\r\n");
	}
if( (strcmp(argv[2],"MAN")==0)||(strcmp(argv[2],"man")==0) )
	{
	esccsetup.ccr0 = 0x98;
	printf("Using MANCHESTER line encoding\r\n");
	}




memset( &os, 0, sizeof( OVERLAPPED ) ) ;        //wipe the overlapped struct

// create I/O event used for overlapped write

os.hEvent = CreateEvent( NULL,    // no security
			    TRUE,    // explicit reset req
			    FALSE,   // initial event reset
			    NULL ) ; // no name
if (os.hEvent == NULL)
   {
      MessageBox( NULL, "Failed to create event for thread!", "main Error!",
		  MB_ICONEXCLAMATION | MB_OK ) ;
      return; 
   }



//this will start up the driver, it also gives us a handle to send
//and receive data from the driver
//note that it is created with the FILE_FLAG_OVERLAPPED set
//if it is not then you must make sequential calls to the driver
//(ie wait until one call has returned before making another call)
//the read/write and status functions are set up to use overlapped, they 
//will return immediatly with a ERROR_IO_PENDING or immediatly with the required data
//

//default to using port 1 (cable #2)
    hDevice = CreateFile ("\\\\.\\ESCC1",
			  GENERIC_READ | GENERIC_WRITE,
			  0,
			  NULL,
			  OPEN_EXISTING,
			  FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
			  NULL
			  );

    if (hDevice == INVALID_HANDLE_VALUE)
    {
		//for some reason the driver won't load or isn't loaded
	printf ("Can't get a handle to esccdrv\n");
	CloseHandle(os.hEvent);
		exit(1);
		//abort and leave here!!!
	}
referenceclock= 18432000.0;

//this can be done a couple of different ways, but since the main purpose of this 
//program is to be able to generate SDLC frames on channel 1 so we can route them 
//to channel 0 and receive(and display) them using the SDLCSNOOP example, we must
//keep the same clock generator value between the two programs.
//if you are running this standalone on its own board, then you could use the 
//mode 7b routine.
//this is the matching routine from SDLCSNOOP (presuming that you give both programs
//the same data rate they should generate the same clock value):
//also note that the limit is about 2Mbps for mode 6, whereas you can 
//use mode 7b to get the full range (to 10M).
//determine clock frequency (based on clock mode 6b, recovering clock using DPLL);(if you have an external clock then you must modify this, use mode 4 or 0 and skip all this bitrate stuff from the command line)
desiredclock = bitrate * 16.0;
if(desiredclock >= 391000.0)
	{
	//use clock generator directly
	esccsetup.ccr2 = 0x18;//(BDF=0, forces inputclock/1 as ouput of bgr) 
	esccsetup.bgr = 0x00;//not used
	desiredclock = bitrate*16.0;
	calculate_bits(referenceclock,desiredclock,&actualclock,&clk);
	printf("data rate used:%12.2f\r\n",actualclock/16.0);
	}
else
	{
	//must divide down to get rate
	esccsetup.ccr2 = 0x38;//(BDF=1, forces inputclock/((N+1)*2) as ouput of bgr) 
	N = (((unsigned)(400000.0/(bitrate*16.0))+2)/2)-1;//force inclock to be >400k
	desiredclock = bitrate*((N+1)*2)*16.0;//calcluate what inclock needs to be to get rate using N
	//make sure that N is attainable;
	//for simplicity limit N to 0-255, could actually go to 1023 by modifying CCR2 bits 7 and 8
	if(N>255)
		{
		printf("can't get to %12.2f easily\r\n",bitrate*16.0);
		//be nice and close handles here
		exit(1);
		}
	esccsetup.bgr = N;
//      printf("N:%u,Desired clock:%12.2f\r\n",N,desiredclock);
	calculate_bits(referenceclock,desiredclock,&actualclock,&clk);
	printf("data rate used:%12.2f\r\n",(actualclock/((N+1)*2))/16.0);
	}


//
/*
//determine clock frequency (based on clock mode 7b);
if(bitrate >= 391000.0)
	{
	//use clock generator directly
	esccsetup.ccr2 = 0x18;//(BDF=0, forces inputclock/1 as ouput of bgr) 
	esccsetup.bgr = 0x00;//not used
	desiredclock = bitrate;
	calculate_bits(referenceclock,desiredclock,&actualclock,&clk);
	printf("data rate used:%12.2f\r\n",actualclock);

	}
else
	{
	//must divide down to get rate
	esccsetup.ccr2 = 0x38;//(BDF=1, forces inputclock/((N+1)*2) as ouput of bgr) 
	N = (((unsigned)(400000.0/bitrate)+2)/2)-1;//force inclock to be >400k
	desiredclock = bitrate*((N+1)*2);//calcluate what inclock needs to be to get rate using N
	//make sure that N is attainable;
	//for simplicity limit N to 0-255, could actually go to 1023 by modifying CCR2 bits 7 and 8
	if(N>255)
		{
		printf("can't get to %12.2f easily\r\n",bitrate);
		//be nice and close handles here
		CloseHandle (hDevice);// stops the escc from interrupting (ie shuts it down)
		CloseHandle(os.hEvent);
		exit(1);
		}
	esccsetup.bgr = N;
//      printf("N:%u,Desired clock:%12.2f\r\n",N,desiredclock);
	calculate_bits(referenceclock,desiredclock,&actualclock,&clk);
	printf("data rate used:%12.2f\r\n",actualclock/((N+1)*2));

	}
*/
//this IOCTL function demonstrates how to set the clock generator on 
//the escc card
//this function will return TRUE unless an invalid parameter is given
//

t = DeviceIoControl(hDevice,IOCTL_ESCCDRV_SET_CLOCK,&clk,sizeof(struct clkset),NULL,0,&returnsize,NULL);
	
	
		//make to use HDLC settings here
		esccsetup.mode = 0x80;//transparent mode 0 receiver inactive
		esccsetup.timr = 0x1f;//not used
		esccsetup.xbcl = 0x00;//not used
		esccsetup.xbch = 0x00;//not using DMA note DMAR must = DMAT = 0 in ntinstall
		esccsetup.ccr1 = 0x16;//clock mode 7b uses txclk as output using BGR
		esccsetup.ccr3 = 0x00;//(preamble and other features that are turned off or not used)
		esccsetup.iva = 0;//must be
		esccsetup.ipc = 0x03;//must be
		esccsetup.imr0 = 0x04;//could mask more of the receive irq's but they won't happen anyway
		esccsetup.imr1 = 0x00;//must have XPR, should keep XDU/EXE and ALLS on as well
		esccsetup.pvr = 0x0;
		esccsetup.pim = 0xff;
		esccsetup.pcr = 0xe0;
		esccsetup.xad1 = 0xff;//must be for extended transparent mode
		esccsetup.xad2 = 0xff;//must be for extended transparent mode
		esccsetup.rah1 = 0xff;//must be for extended transparent mode
		esccsetup.rah2 = 0xff;//must be for extended transparent mode
		esccsetup.ral1 = 0xff;//must be for extended transparent mode
		esccsetup.ral2 = 0xff;//must be for extended transparent mode
		esccsetup.rlcr = 0x00;//not used
		esccsetup.pre = 0x00;//if you setup the preamble...this is it
		esccsetup.ccr4 = 0x00;//no ebgr or master clk/4.

		esccsetup.n_rbufs = 2;//number of receive buffers
		esccsetup.n_tbufs = 20;//number of transmit buffers
		esccsetup.n_rfsize_max = 4096;//receive buffer size
		esccsetup.n_tfsize_max = 4096;//transmit buffer size


	//when called this IOCTL will set the registers of the escc and
	//reset the TX and RX machines (issuing a separate XRES and RHR command)
	//if the resets time out the function will return FALSE, otherwise it will
	//return TRUE
	t = DeviceIoControl(hDevice,IOCTL_ESCCDRV_SETUP,&esccsetup,sizeof(struct setup),NULL,0,&returnsize,NULL);
	if(t==TRUE)printf("SETTINGS SUCCESSFUL:%lu\n\r",returnsize);
	if(t==FALSE)
		{
		printf("SETTINGS FAILED:%lu\n\r",returnsize);
		CloseHandle(hDevice);
		CloseHandle(os.hEvent);
		exit(1);
		}

do
{
//get info and build SDLC frame
printf("send I, S, N(U) frame?\r\n");
i = getch();
key = i;
if((i=='i')||(i=='I'))
	{
	printf("enter NS (0-7):");
	scanf("%u",&ns);        
	printf("\r\n");
	printf("enter NR (0-7):");
	scanf("%u",&nr);        
	printf("\r\n");
	printf("enter p/f (0,1):");
	scanf("%u",&pf);        
	printf("\r\n");
	ns = ns &0x07;//mask off unused.
	nr = nr &0x07;//mask off unused.
	pf = pf &0x01;//mask off unused.
	printf("enter address (HEX):");
	scanf("%x",&address);
	printf("\r\n");
	buffer[0] = address;//set address field
	buffer[1] = (nr<<5)|(pf<<4)|(ns<<1);//build control field               
	printf("enter info:([esc] to send)\r\n");
	k  = 2;
	do
		{
		j = getche();
		if(j!=27)buffer[k++]= (char)j;
		}while(j!=27);
	printf("\r\n");
	goto sendtheframe;
	}
if((i=='s')||(i=='S'))
	{
	printf("1. RR\r\n");
	printf("2. RNR\r\n");
	printf("3. REJ\r\n");
	j = getch();

	printf("enter NR (0-7):");
	scanf("%u",&nr);        
	printf("\r\n");
	printf("enter p/f (0,1):");
	scanf("%u",&pf);        
	printf("\r\n");
	nr = nr &0x07;//mask off unused.
	pf = pf &0x01;//mask off unused.
	printf("enter address (HEX):");
	scanf("%x",&address);
	printf("\r\n");
	buffer[0] = address;//set address field
		
	if(j =='1')
	{
	buffer[1] = (nr<<5)|(pf<<4)|1;
	}
	if(j=='2')
	{
	buffer[1] = (nr<<5)|(pf<<4)|5;
	}
	if(j=='3')
	{
	buffer[1] = (nr<<5)|(pf<<4)|9;
	}                            
	k = 2;
	goto sendtheframe;
	}
if((i=='n')||(i=='N')||(i=='u')||(i=='U'))
	{
	printf("1. NSI\r\n");
	printf("2. RQI\r\n");
	printf("3. SIM\r\n");
	printf("4. SNRM\r\n");
	printf("5. ROL\r\n");
	printf("6. DISC\r\n");
	printf("7. NSA\r\n");
	printf("8. CMDR\r\n");
	printf("9. ORP\r\n");
	j = getch();

	printf("enter address (HEX):");
	scanf("%x",&address);
	printf("\r\n");
	buffer[0] = address;//set address field

	if(j=='1')
		{
		printf("enter p/f (0,1):");
		scanf("%u",&pf);        
		printf("\r\n");
		pf = pf &0x01;//mask off unused.
		buffer[1] = (pf<<4)|3;
		printf("enter info:([esc] to send)\r\n");
		k  = 2;
		do
			{
			j = getche();
			if(j!=27)buffer[k++]= (char)j;
			}while(j!=27);
		printf("\r\n");
		goto sendtheframe;
		}
	if(j=='2')
		{
		buffer[1] = 0x07;
		k = 2;
		goto sendtheframe;
		}               
	if(j=='3')
		{
		buffer[1] = 0x17;
		k = 2;
		goto sendtheframe;
		}               
	if(j=='4')
		{
		buffer[1] = (char)0x93;
		k = 2;
		goto sendtheframe;
		}                       
	if(j=='5')
		{
		buffer[1] = 0x0f;
		k = 2;
		goto sendtheframe;
		}                       
	if(j=='6')
		{
		buffer[1] = 0x53;
		k = 2;
		goto sendtheframe;
		}                       
	if(j=='7')
		{
		buffer[1] = 0x63;
		k = 2;
		goto sendtheframe;
		}                       
	if(j=='8')
		{
		buffer[1] = (char)0x87;
		k = 2;
		goto sendtheframe;
		}       
	if(j=='9')
		{
		buffer[1] = 0x33;
		k = 2;
		goto sendtheframe;
		}               
	}

sendtheframe:
//send the frame
	t = WriteFile(hDevice,buffer,k,&nobyteswritten,&os);//send the frame
	if(t==FALSE)                    //if it returned FALSE then the IO request is queued (waiting for previous frame to get done sending)
		{                       //and we must wait until the os.event gets signaled before we try any more sending
		if (GetLastError() == ERROR_IO_PENDING)  //IO_PENDING is the indication that the request was queued, if not this than a parameter error occured
			{
			do
				{
				j = WaitForSingleObject( os.hEvent, (DWORD)(((double)(8*(4096+32))/bitrate)*(double)1000));//bitrate dependent timeout, this number corresponds to the time to send 1 frame  +32 byte times
				if(j==WAIT_TIMEOUT)
					{
					printf("timeout\r\n");
					DeviceIoControl(hDevice,IOCTL_ESCCDRV_FLUSH_TX,NULL,0,NULL,0,&returnsize,NULL);
					//the most common problem will be EXE/XDU or a data underrun, if that occurs then
					//the PC is not keeping up with the interrupt rate necessary to keep the 82532 fifo filled
					}
				}while(j!=WAIT_OBJECT_0);
			}
		}
}while(key!=27);

	CloseHandle (hDevice);// stops the escc from interrupting (ie shuts it down)
	CloseHandle(os.hEvent);//finished with the event
	printf("exiting program\n\r");          //exit message
}                                               //done


//function to calcluate input clock params.
BOOL calculate_bits(double reffreq,double desired_freq,double *m_actual_clock,CLKSET *clk)
{
DWORD P;
DWORD Pprime;
DWORD Q;
DWORD Qprime;
DWORD M;
DWORD I;
DWORD D;
double fvco;
double desired_ratio;
double actual_ratio;
DWORD bestP;
DWORD bestQ;
DWORD bestM;
DWORD bestI;
double best_actual_ratio;
DWORD Progword;
DWORD Stuffedword;
DWORD bit1;
DWORD bit2;
DWORD bit3;
DWORD i,j;
char buf[256];
unsigned long rangelo;
unsigned long rangehi;
double best_diff;

D = 0x00000000;//from bitcalc, but the datasheet says to make this 1...
bestP = 0;
bestQ = 0;
best_actual_ratio = 1000000.0;//hopefully we can do better than this...
best_diff = 1000000.0;
rangelo = (unsigned long)floor(reffreq/1000000.0) +1;
rangehi = (unsigned long)floor(reffreq/200000.0);
if(rangelo <3) rangelo = 3;
if(rangehi>129) rangehi = 129;
sprintf(buf,"%u(%f), %u(%f)",rangelo,(reffreq/1000000.0) +1.0,rangehi,(reffreq/200000.0));
//MessageBox(buf,"rl(fl),rh(fh)",MB_OK);
for(i=0;i<=7;i++)
{
M = i;
fvco = desired_freq * (pow(2,i));
//sprintf(buf,"%f",fvco);
//MessageBox(NULL,buf,"Fvco",MB_OK);

if(fvco<80000000.0) I = 0x00000000;
if(fvco>=80000000.0) I = 0x00000008;
if((fvco>50000000.0)&&(fvco<150000000.0))
{
sprintf(buf,"%f",fvco);
//MessageBox(NULL,buf,"Fvco",MB_OK);
desired_ratio = fvco/(2.0 * reffreq);
sprintf(buf,"%f",desired_ratio);
//MessageBox(NULL,buf,"desired",MB_OK);

for(P=4;P<=130;P++)for(Q=rangelo;Q<=rangehi;Q++)
{

actual_ratio = (double)P/(double)Q;
if(actual_ratio==desired_ratio) 
	{
	sprintf(buf,"%u,%u",P,Q);
	//MessageBox(NULL,buf,"Direct Hit",MB_OK);
	bestP = P;
	bestQ = Q;
	bestM = M;
	bestI = I;
	best_actual_ratio = actual_ratio;
	goto donecalc;
	}
else 
	{
	if(fabs(desired_ratio - actual_ratio)<(best_diff)) 
		{
		best_diff = fabs(desired_ratio - actual_ratio);
		best_actual_ratio = actual_ratio;
		bestP = P;
		bestQ = Q;
		bestM = M;
		bestI = I;
		sprintf(buf,"desired:%f,actual:%f, best%f P%u,Q%u,fvco:%f,M:%u",desired_ratio,actual_ratio,best_diff,bestP,bestQ,fvco,M);
		//MessageBox(buf,"ratiocalc",MB_OK);
		}
	}       
}
}
}
donecalc:
if((bestP!=0)&&(bestQ!=0))
	{
	//here bestP BestQ are good to go.
	I = bestI;
	M = bestM;
	P = bestP;
	Q = bestQ;
	Pprime = bestP - 3;
	Qprime = bestQ - 2;
	sprintf(buf,"P':%u, Q':%u, M:%u, I:%u",Pprime,Qprime,M,I);
	//MessageBox(buf,"P,Q,M,I",MB_OK);
	Progword = 0;
	Progword =  (Pprime<<15) | (D<<14) | (M<<11) | (Qprime<<4) | I;
	sprintf(buf,"%lx",Progword);
//      MessageBox(buf,"Progword",MB_OK);
	bit1 = 0;
	bit2 = 0;
	bit3 = 0;
	Stuffedword = 0;
	i = 0;
	j = 0;
	bit1 = ((Progword>>i)&1);
	Stuffedword |=  (bit1<<j);
	i++;
	j++;
	bit2 = ((Progword>>i)&1);
	Stuffedword |=  (bit2<<j);
	i++;
	j++;
	bit3 = ((Progword>>i)&1);
	Stuffedword |=  (bit3<<j);
	j++;
	i++;
	while(i<=22)
		{
		if((bit1==1)&&(bit2==1)&&(bit3==1))
			{
			//force a 0 in the stuffed word;
			j++;
			bit3 = 0;
			sprintf(buf,"i,j : %u,%u",i,j);
//                      MessageBox(buf,"Stuffing",MB_OK);
			}
		bit1 = bit2;
		bit2 = bit3;
		bit3 = ((Progword>>i)&1);
		Stuffedword |=  (bit3<<j);
		i++;
		j++;
		}
	sprintf(buf,"SW:%lx ,numbits:%u",Stuffedword,j);

clk->clockbits = Stuffedword;
clk->numbits = (USHORT)j-1;
*m_actual_clock = ((2.0 * reffreq) * ((double)P/(double)Q)) / pow(2,M);
//      MessageBox(buf,"stuffedword, numbits",MB_OK);
return TRUE;
	}
else
{
	printf("\r\nError in ICD calculation\r\n");
	return FALSE;

}
}
