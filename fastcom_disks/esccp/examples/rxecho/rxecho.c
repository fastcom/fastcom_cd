//$Id$
/*
Demonstrates how to enable Rx Echo Cancel feature.  Rx Echo Cancel is useful in 2 wire
RS-485 network when you do not want to receive what is transmitted.  This feature will 
diable the receiver during transmit on a per port basis.  Because it is implemented at 
the driver level, it will be much more accurate than polling the ALLS status bit.
*/
#include "windows.h"
#include "stdlib.h"
#include "stdio.h"
#include "..\esccptest.h"

void main(int argc,char *argv[])
{
HANDLE hDevice;                 //handle to the escc driver/device
char devname[64];
unsigned port;
ULONG t;
ULONG returnsize;

if(argc<3)
{
printf("usage:\r\n");
printf("%s port [1|0]\r\n",argv[0]);
exit(1);
}

port = atoi(argv[1]);
t = atol(argv[2]);

sprintf(devname,"\\\\.\\ESCC%d",port);

    hDevice = CreateFile (devname,
			  GENERIC_READ | GENERIC_WRITE,
			  0,
			  NULL,
			  OPEN_EXISTING,
			  FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
			  NULL
			  );

    if (hDevice == INVALID_HANDLE_VALUE)
    {
		//for some reason the driver won't load or isn't loaded
	printf ("Can't get a handle to %s\n",devname);
		exit(1);
		//abort and leave here!!!
	}

DeviceIoControl(hDevice,IOCTL_ESCCDRV_RX_ECHO_CANCEL,&t,sizeof(ULONG),NULL,0,&returnsize,NULL);

if(t==1) printf("receive echo cancel ON\r\n");
else printf("receive echo cancel OFF\r\n");

CloseHandle (hDevice);// stops the escc from interrupting (ie shuts it down)

//done
}