//Copyright(C) 1998, Commtech, INC. Wichita, KS.
/****************************************************************************
*
* PROGRAM: ESCC_RECONLY (reconly.c)
*
* PURPOSE: Simple console application for calling ESCCDRV  VxD
*		   Bare bones receive application
*
* FUNCTIONS:
*  main() - 
*
* SPECIAL INSTRUCTIONS:
*
****************************************************************************/
#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "esccdrv.h"

void decode_stat(unsigned long stat);
void decode_err(DWORD err);

int main(int argc, char *argv[])
{

    HANDLE      hESCC = 0;
    DWORD       cbBytesReturned;
    DWORD       dwErrorCode;
	DWORD		Port;
	DWORD i,k;
	SETUP settings;
	REGSINGLE esccreg;
	CLK clock;
	unsigned long stat;
	char buffer[4096];
	OVERLAPPED osr;
	DWORD olbytesread;
	unsigned key;
	DWORD tocount;
	DWORD framecount;
	DWORD badcount;

	DWORD validcount;
	DWORD crccount;
	DWORD rdocount;
	DWORD statcount;
if(argc>=2)
{
Port= atoi(argv[1]);
}
else Port = 0;

memset( &osr, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct

// create I/O event used for overlapped read

osr.hEvent = CreateEvent( NULL,    // no security
			    TRUE,    // manual  reset req
			    FALSE,   // initial event reset
			    NULL ) ; // no name
if (osr.hEvent == NULL)
   {
      printf("Failed to create event\n");
      return(1); 
   }

    // Dynamically load and prepare to call esccdrv.vxd
    // The CREATE_NEW flag is not necessary
	// The FILE_FLAGE_OVERLAPPED is only necessary if you want to use overlapped i/o (the driver will work both ways)
    hESCC = CreateFile("\\\\.\\ESCCPDRV.VXD", 0,0,0,
                        OPEN_EXISTING, FILE_FLAG_OVERLAPPED, 0);

	//unless the file is not there this will allways return successfully
    if ( hESCC == INVALID_HANDLE_VALUE )
    {
        dwErrorCode = GetLastError();
        if ( dwErrorCode == ERROR_NOT_SUPPORTED )
        {
            printf("Unable to open VxD, \n device does not support DeviceIOCTL\n");
        }
        else
        {
            printf("Unable to open VxD, Error code: %lx\n", dwErrorCode);
        }
    }
    else
    {
	printf("obtained handle to esccdrv\n");
	//got a handle to the vxd so it must be loaded.
if(DeviceIoControl(hESCC, ESCC_GET_MAX_PORT,(LPVOID)NULL,0,&i, sizeof(DWORD),&cbBytesReturned, NULL))
	{
	//returns true and i = maximum usable port # (starting at 0)
	//or ERROR_INVALID_PARAMETER if a DWORD is not given for the output buffer
	//or ERROR_NO_PORTS_DEFINED if no ports are known to the driver
	if(cbBytesReturned!=sizeof(DWORD))printf("bad returnsize:%lu\n",cbBytesReturned);
	printf("#ports active:%lu\n",i+1);//remember 0 based return value...
	}
else
	{
	dwErrorCode = GetLastError();
	decode_err(dwErrorCode);
	goto end;	
	}
if(Port>i)
{
printf("Port specified is out of range\r\n");
return(1);
}

printf("Set ICD2053b\n");		
//setup the icd2053b		
clock.clockbits = 0x5d1460;//settings for 16.000 MHz
clock.numbits = 23;
clock.port = Port;
if ( DeviceIoControl(hESCC,ESCC_SET_CLOCK,&clock,sizeof(CLK),(LPVOID)NULL,0,&cbBytesReturned, NULL) )
	//returns true if clock generator is set 
	//or ERROR_INVALID_PARAMETER if a CLK struct is not passed
	//or ERROR_BAD_PORT_NUMBER if the port given is out of range
	{
	if(cbBytesReturned!=0)printf("bad returnsize:%lu\n",cbBytesReturned);
	else printf("ICD2053B set to 16MHz\n");
	}
	else
	{
	dwErrorCode = GetLastError();
	decode_err(dwErrorCode);
	goto end;
	}

printf("starting settings...");
settings.port = Port;
settings.txtype = XTF;
settings.mode = 0x88;//transparent mode 0 receiver active
settings.timr = 0x1f;//not used in this example
settings.xbcl = 0x00;//not used in this example
settings.xbch = 0x00;//DMA bit must be 0
settings.ccr0 = 0x80;//Power up NRZ, HDLC mode
settings.ccr1 = 0x16;//clock mode 6 (b) (clock recovery on rx, bgr/16 on tx)
settings.ccr2 = 0x38;//use bgr = inclock/(N+1)*2
settings.ccr3 = 0x00;//no special handling
settings.ccr4 = 0x00;//not master mode, not ebgr mode
settings.tsax = 0x00;//not used in this example
settings.tsar = 0x00;//not used in this example
settings.xccr = 0x00;//not used in this example
settings.rccr = 0x00;//not used in this example
settings.bgr = 0x01;// bitrate will be (16E6/(1+1)*2)/16 = 250kbps
settings.iva = 0x00;//not used in this example
settings.ipc = 0x03;//must be 3
settings.imr0 = 0x04;//disable CDSC int
settings.imr1 = 0x00;//could disable these as well, but not for now
settings.pvr = 0x00;//not used reccommend that you write 0
settings.pim = 0xff;//not used in this example
settings.pcr = 0xe0;//must be set to 0xe0
settings.xad1 = 0xff;//not used in this example
settings.xad2 = 0xff;//not used in this example
settings.rah1 = 0xff;//not used in this example
settings.rah2 = 0xff;//not used in this example
settings.ral1 = 0xff;//not used in this example
settings.ral2 = 0xff;//not used in this example
settings.rlcr = 0x00;//not used in this example
settings.aml = 0x00;//not used in this example
settings.amh = 0x00;//not used in this example
settings.pre = 0x00;//not used in this example
settings.n_rbufs = 100;//number of receive buffers to allocate
settings.n_tbufs = 2;//number of transmit buffers to allocate
settings.n_rfsize_max = 4096;//size of each receive buffer (max received frame size)
settings.n_tfsize_max = 4096;//size of each transmit buffer (max transmitted frame size)

if ( DeviceIoControl(hESCC, ESCC_INITIALIZE,(LPVOID)&settings, sizeof(SETUP),(LPVOID)NULL, 0,&cbBytesReturned, NULL) )
	//returns true if settings successfull
	//returns ERROR_INVALID_PARAMETER if SETUP struct not passed
	//returns ERROR_BAD_PORT_NUMBER if port is out of range
	//returns ERROR_CEC_TIMEOUT if the command executing bit times out(ie no core clock)
	{
    if(cbBytesReturned!=0)printf("bad returnsize:%lu\n",cbBytesReturned);
	else printf("...INIT OK\n");
	}
else
	{
    dwErrorCode = GetLastError();
	printf("INIT FAILED\nINIT returned:%li\n",dwErrorCode);
	decode_err(dwErrorCode);
	goto end;
	}

start:
framecount=0;
badcount=0;
validcount = 0;
crccount = 0;
rdocount = 0;
statcount=0;
do
{
//printf("Waiting for Received Frame..\n\r");
tocount = 0;
if ( DeviceIoControl(hESCC, ESCC_READ,&Port, sizeof(DWORD),buffer, sizeof(buffer),&cbBytesReturned, &osr) )
	{
	//if here then the the frame was buffered, as it returned immediatly
	olbytesread = cbBytesReturned;
	}
else
	{
	dwErrorCode = GetLastError();
	//decode_err(dwErrorCode);
	//io still pending
	if(dwErrorCode==ERROR_IO_PENDING)
		{
		//if here a frame has not been received yet, so we wait 
		do
			{
			k = WaitForSingleObject(osr.hEvent,2000);//2 second timeout could increase or decrease depending on expected frame sizes
			if(k==WAIT_TIMEOUT)
				{
				//this code will execute every 2 seconds (or every timeout period if you change it)
				//as long as a frame has not been received.
				if(kbhit())
					{
					getch();
					goto end;
					}
				printf("RTO\r"); 
				tocount++;
				if(tocount>30) //(if wait is longer than 1min then abort);
					{
					//this flush will force the received frame to complete
					//with 0 bytes and allow the program to drop out
					//flush rx
					printf("\r\nflushing rx\n");
					if ( DeviceIoControl(hESCC, ESCC_FLUSH_RX,&Port,sizeof(DWORD),(LPVOID)NULL, 0,&cbBytesReturned, NULL) )
					//returns true if receive buffers are flushed or
					//ERROR_INVALID_PARAMETER if a DWORD is not passed
					//ERROR_BAD_PORT_NUMBER if the port is out of range
					//ERROR_CEC_TIMEOUT if there is a command executing
						{
						if(cbBytesReturned!=0)printf("bad returnsize:%lu\n",cbBytesReturned);
						printf("RX flushed\n");
						goto end;
						}
					else
						{
						dwErrorCode = GetLastError();
						decode_err(dwErrorCode);
						goto end;
						}

					}
				}
			if(k==WAIT_OBJECT_0)
				{
				//when a frame is received this code executes
				//printf("Received Frame:\n");
				}
			}while(k!=WAIT_OBJECT_0);//continue waiting until a frame is received
	//here a frame has been read (either immediatly or a waited for frame)
	//the GetOverlappedResult is really only necessary for the waited frame but
	//it won't hurt anything.
		GetOverlappedResult(hESCC,&osr,&olbytesread,TRUE);//to get the actual number of bytes in the frame

		}
	}

if(olbytesread!=0)
{
	
		for(i=0;i<olbytesread-1;i++) if(buffer[i]!=(i%10 +0x30)) badcount++;
		if(olbytesread!=0)//check the RSTA byte
			{
			if(buffer[olbytesread-1]&0x80)
				{
				//test for valid frame OK
				}
			else
				{
				//invalid frame indicated
				validcount++;
				}
			
			if(buffer[olbytesread-1]&0x20)
				{
				//test for crc OK
				}
			else 
				{
				//crc test failed
				crccount++;
				}
			if(buffer[olbytesread-1]&0x40)
				{
				//recieve data overflow (hardware) indicated
				rdocount++;
				}
			}
		printf("count:%lu, bad:%lu V:%lu C:%lu R:%lu S:%lu\r",framecount,badcount,validcount,crccount,rdocount,statcount);		

		framecount++;

/*
		for(i=0;i<cbBytesReturned;i++) printf("%c",buffer[i]);
		printf("\n");
		printf("Read #%lu returned :%lu bytes ---  ",framecount,cbBytesReturned);
		if(cbBytesReturned!=0)
		{
			if(buffer[cbBytesReturned-1]&0x80)printf("Valid ;");
			else printf("Invalid ;");
			printf("CRC..");
			if(buffer[cbBytesReturned-1]&0x20)printf("OK ;");
			else printf("Failed ;");
			if(buffer[cbBytesReturned-1]&0x40)printf("RDO ;");
		}
		printf("\n");
		framecount++;

*/
}
else
{
printf("\r\nframe error, returned 0, probably flushed/canceled\r\n");
}

//check status
if(DeviceIoControl(hESCC, ESCC_STATUS,&Port,sizeof(DWORD),&stat,sizeof(unsigned long),&cbBytesReturned, NULL) )
	//returns true with unsigned long status or (reads and clears the internal status value)
	//ERROR_INVALID_PARAMETER if DWORD and unsigned long not passed
	//ERROR_BAD_PORT_NUMBER if the port is out of range
	{
	if(cbBytesReturned!=sizeof(unsigned long))printf("bad returnsize:%lu\n",cbBytesReturned);
//	decode_stat(stat);
	if((stat&ST_OVF)==ST_OVF) statcount++;
	if((stat&ST_RFO)==ST_RFO) statcount++;
	if((stat&ST_EXE)==ST_EXE) statcount++;
	}
else
	{
    dwErrorCode = GetLastError();
	decode_err(dwErrorCode);
	}

}while((!kbhit())&&(tocount<=30));
if(tocount<=30)
{
key = getch();
if(key!=27) goto start;
}
else printf("\r\nno data received for 1 minute exiting program\r\n");

end:
printf("\r\nexiting prog\n");
        // Dynamically UNLOAD the C Virtual Device sample.
		CloseHandle(osr.hEvent);
        CloseHandle(hESCC);

}
}

void decode_stat(unsigned long stat)
{
if(stat==0)return;
printf("STATUS DECODE:\n");
if(stat&ST_RX_DONE) printf("Receive Done\n");
if(stat&ST_OVF) printf("RX BUFFERS overflow\n");
if(stat&ST_RFS) printf("Receive Frame Start\n");
if(stat&ST_RX_TIMEOUT) printf("RX timeout\n");
if(stat&ST_RSC) printf("Receive Status Change\n");
if(stat&ST_PERR) printf("Parity Error\n");
if(stat&ST_PCE) printf("Protocol Error\n");
if(stat&ST_FERR) printf("Framing Error\n");
if(stat&ST_SYN) printf("SYN detect\n");
if(stat&ST_DPLLA) printf("DPLL asyncronous\n");
if(stat&ST_CDSC) printf("CD changed state\n");
if(stat&ST_RFO) printf("Receive Frame Overflow\n");
if(stat&ST_EOP) printf("End of Poll\n");
if(stat&ST_BRKD) printf("Break Detected\n");
if(stat&ST_ONLP) printf("On Loop\n");
if(stat&ST_BRKT) printf("Break Terminated\n");
if(stat&ST_ALLS) printf("All Sent\n");
if(stat&ST_EXE) printf("Transmit Underrun\n");
if(stat&ST_TIN) printf("Timer Expired\n");
if(stat&ST_CTSC) printf("CTS changed state\n");
if(stat&ST_XMR) printf("Transmit Message Repeat\n");
if(stat&ST_TX_DONE) printf("Transmit Done\n");
if(stat&ST_DMA_TC) printf("DMA TC reached\n");
if(stat&ST_DSR0C) printf("DSR0 changed state\n");
if(stat&ST_DSR1C) printf("DSR1 changed state\n");
if(stat&TX_FUBAR) printf("TX is FUBAR\n");
}

void decode_err(DWORD err)
{
if(err==ERROR_SUCCESS) return;
switch (err)
{
case ERROR_IO_PENDING:
	printf("The I/O operation is pending\n");
	break;
case ERROR_INVALID_PARAMETER:
	printf("The parameter given was invalid.\n");
	break;
case ERROR_BAD_PORT_NUMBER:
	printf("Invalid port number.\n");
	break;
case ERROR_NO_DATA_RECEIVED:
	printf("No received frames ready.\n");
	break;
case ERROR_USER_BUFFER_TOO_SMALL:
	printf("Received frame is larger than user buffer.\n");
	break;
case ERROR_TRANSMIT_FRAME_TOO_LARGE:
	printf("Tx frame must be in range 1 -> 4096 bytes\n");
	break;
case ERROR_TRANSMIT_BUFFERS_FULL:
	printf("Tx buffers full; try again later.\n");
	break;
case ERROR_TRANSMITTER_LOCKED_UP:
	printf("Transmitter is in locked up state; call ESCC_FLUSH_TX and try again\n");
	break;
case ERROR_CEC_TIMEOUT:
	printf("There is a command executing.\n");
	printf("The two most likely causes:\n");
	printf("1.  No clock source for the core(no txclk in non master mode)\n");
	printf("2.  Very slow baud rate in non master mode\n");
	break;
case ERROR_MAX_BOARDS_INSTALLED:
	printf("Maximum boards allready in use\n");
	break;
case ERROR_IRQ_IN_USE:
	printf("Unable to virtualize the irq specified\n");
	break;
case ERROR_NO_PORTS_DEFINED:
	printf("The driver does not have any ports defined\n");
	break;
case ERROR_BOARD_EXISTS:
	printf("The board allready exists\n");
	break;
case ERROR_TOO_MANY_RBUFS:
	printf("You requested too many receive buffers\n");
	break;
case ERROR_TOO_MANY_TBUFS:
	printf("You requested too many transmit buffers\n");
	break;
case ERROR_MAX_FRAME_SIZE:
	printf("The maximum frame size of 4k was exceeded\n");
	break;
case ERROR_MEM_ALLOCATION:
	printf("While allocating memory an error occured\n");
	break;
case ERROR_MIN_FRAME_SIZE:
	printf("the minimum frame size of 32 bytes was violated\n");
	break;


}
}