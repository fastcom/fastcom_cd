//Copyright (C) 1999 Commtech, Inc.
//
//Win32 Console application to run a loopback test on multiple consecutive
//serial ports using the standard win32 COM api.
//
//command line:
//looptest X Y Z
//X is the number of ports to include in the loop
//Y is the starting COM port #
//Z is the bitrate to use (115200, 57600, 38400, 19200, 9600, etc)
//This program will send 0-0xff to each port, and expect to get it back
//
#include "windows.h"
#include "wtypes.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "conio.h"
#include "math.h"


void main(int argc, char *argv[])
{
	
	HANDLE port[25];
	DWORD i,j;
	DWORD numwr,numrd;
	DWORD ret;
	unsigned long loopcount;
	unsigned long errors;
	DCB mdcb;
	unsigned maxports;
	unsigned startport;
	char tbuffer[1024];
	char rbuffer[1024];
	char buf[256];
	unsigned long bitrate;
	
	OVERLAPPED osr[25];
	OVERLAPPED ost[25];
	COMMTIMEOUTS cto;
	if(argc>=4)
	{
		bitrate = atol(argv[3]);
	}
	else bitrate = 115200;
	
	if(argc>=3)
	{
		maxports = atoi(argv[1]);
		startport = atoi(argv[2]);
		if(maxports==0)return;
		if(startport==0) return;
	}
	else
	{
		printf("USAGE:\r\n");
		printf("looptest X Y Z\r\n");
		printf("X is the number of ports to include in the loop\r\n");
		printf("Y is the starting COM port #\r\n");
		printf("Z is the bitrate to use (115200, 57600, 38400, 19200, 9600, etc)\r\n");
		exit(1);
	}
	
	printf("looptest:\r\n");
	printf("starting port:%u\r\n",startport);
	printf("ending port:%u\r\n",startport+maxports);
	printf("bitrate:%lu\r\n",bitrate);
	
	for(i=0;i<maxports;i++)
	{
		memset( &osr[i], 0, sizeof( OVERLAPPED ) ) ;    //wipe the overlapped struct
		memset( &ost[i], 0, sizeof( OVERLAPPED ) ) ;    //wipe the overlapped struct
		
		// create I/O event used for overlapped write
		
		ost[i].hEvent = CreateEvent( NULL,    // no security
			TRUE,    // explicit reset req
			FALSE,   // initial event reset
			NULL ) ; // no name
		if (ost[i].hEvent == NULL)
		{
			printf("Failed to create event for thread!\r\n");
			return; 
		}
		
		// create I/O event used for overlapped read
		
		osr[i].hEvent = CreateEvent( NULL,    // no security
			TRUE,    // explicit reset req
			FALSE,   // initial event reset
			NULL ) ; // no name
		if (osr[i].hEvent == NULL)
		{
			printf("Failed to create event for thread!\r\n");
			return; 
		}
		
		sprintf(buf,"\\\\.\\COM%u",i+startport);
		port[i] = CreateFile (buf,
			GENERIC_READ | GENERIC_WRITE,
			0,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
			NULL
			);
		
		if(port[i]== INVALID_HANDLE_VALUE)
		{
			//fubar here
			printf("cannot open handle to COM%u\r\n",i+5);
			return;
		}
	}
	for(i=0;i<maxports;i++)
	{
		GetCommState(port[i],&mdcb);
		mdcb.BaudRate = bitrate;
		mdcb.fBinary = 1;
		mdcb.fParity = 0;
		mdcb.fOutxCtsFlow = 0;
		mdcb.fOutxDsrFlow = 0;
		mdcb.fOutX = 0;
		mdcb.fInX = 0;
		mdcb.fRtsControl = 0;
		mdcb.ByteSize = 8;
		mdcb.Parity = 0;
		mdcb.StopBits = 0;
		SetCommState(port[i],&mdcb);
		SetupComm(port[i],1024,1024);
		PurgeComm(port[i],PURGE_TXCLEAR|PURGE_RXCLEAR);
		
		
		cto.ReadIntervalTimeout = 40;
		cto.ReadTotalTimeoutMultiplier = 4;
		cto.ReadTotalTimeoutConstant = 360;
		cto.WriteTotalTimeoutMultiplier = 4;
		cto.WriteTotalTimeoutConstant = 360;
		SetCommTimeouts(port[i],&cto);
	}
	
	for(i=0;i<256;i++) tbuffer[i] = (char)i;
	loopcount = 0;
	errors = 0;
	
	do
	{
		for(i=0;i<maxports;i++)
		{
			ret = WriteFile(port[i],tbuffer,256,&numwr,&ost[i]);
			if(ret==FALSE)
			{
				if(GetLastError()==ERROR_IO_PENDING)
				{
					//printf("COM%u write pending\r\n",i+5);
					/*
					j = WaitForSingleObject(ost[i].hEvent,250);
					if(j==WAIT_TIMEOUT)
					{
					printf("COM%u write timeout\r\n",i+5);
					errors++;                                       
					}
					//if(j==WAIT_OBJECT_0) printf("COM%u write successful\r\n",i+5);
					*/                      
				}
				else
				{
					printf("\r\na Write ERROR occured on COM%u\r\n",i+startport);
					errors++;
				}
			}
		}
		
		
		for(i=0;i<maxports;i++)
		{
			memset( rbuffer, 0, 256 ) ;     //wipe the rxbuffer
			ret = ReadFile(port[i],rbuffer,256,&numrd,&osr[i]);
			if(ret ==TRUE)
			{
				//printf("com%u received %u bytes:\n\r",i+5,numrd);    //display the number of bytes received
				if(numrd!=256) 
				{
					printf("\r\nerror in bytes received, count off %u != 256\r\n",numrd);
					errors++;
				}
				for(j=0;j<numrd;j++)
				{
					if(tbuffer[j]!=rbuffer[j])
					{
						errors++;
						printf("\r\ndata mismatch on com%u, %x != %x\r\n",i+startport,rbuffer[j],tbuffer[j]);
					}
				}
				
			}
				else if((ret==FALSE)&&(GetLastError()==ERROR_IO_PENDING))
				{
					j = WaitForSingleObject( osr[i].hEvent, 250 );//250 ms timeout
					if(j==WAIT_TIMEOUT)
					{
						errors++;
						printf("\r\nCOM%u Read timeout\r\n",i+startport);
					}
					if(j==WAIT_ABANDONED)
					{
						errors++;
						printf("\r\nCOM%u Read abandoned\r\n",i+startport);
					}
					if(j==WAIT_OBJECT_0)
					{
						GetOverlappedResult(port[i],&osr[i],&numrd,TRUE); //here to get the actual nobytesread!!!
						//printf("com%u received %u bytes:\n\r",i+5,numrd);    //display the number of bytes received
						if(numrd!=256) 
						{
							printf("\r\nerror in bytes received, count off %u != 256\r\n",numrd);
							errors++;
						}
						for(j=0;j<numrd;j++)
						{
							if(tbuffer[j]!=rbuffer[j])
							{
								errors++;
								printf("\r\ndata mismatch on com%u, %x != %x\r\n",i+startport,rbuffer[j],tbuffer[j]);
							}
						}
					}
				}
			else
			{
				printf("\r\na read error has occured on COM%u\r\n",i+startport);
			}
		}
		loopcount++;
		printf("Loop:%lu, Errors:%lu\r",loopcount,errors);
		
		
	}while(!_kbhit());
	
	_getch();
	printf("\r\n\r\nTotal Loops:%lu\r\n",loopcount);
	printf("Total Bytes per port:%lu\r\n",loopcount*256);
	printf("Total Bytes :%lu\r\n",loopcount*256*maxports);
	printf("Total Errors:%lu\r\n",errors);
	
}
