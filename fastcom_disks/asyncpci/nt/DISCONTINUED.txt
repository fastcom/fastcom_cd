Support for the NT driver for the async-pci series has been discontinued.

The driver works as of the date on the serialp.sys file.  

However, any new driver features that you may find in the Windows 
2000 or xp drivers have not been applied to the NT driver.

If you have any questions, please contact us: techsupport@commtech-fastcom.com