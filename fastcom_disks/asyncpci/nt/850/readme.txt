Exar 16C850 utilities:

Usage:
850util -i                                      -> install driver
850util -r                                      -> remove driver 
850util -s                                      -> start driver  
850util -t                                      -> stop driver    
850util -ar base rxtrigger      -> set rxtrigger registry setting
850util -at base txtrigger      -> set txtrigger registry setting
850util -aa base auto485        -> set enable/disable auto485 registry setting
850util -d  base                        -> delete port settings from registry
850util -ir base rxtrigger      -> immediate set rx trigger
850util -it base txtrigger      -> immediate set tx trigger
850util -ia base auto485    -> immediate enable/disable auto 485

Note that if you change the triggers via ir or it that it does not enable
those trigger values until you call the -ia function.
The correlary to this is that if you set the registry entries to specify
the triggers on startup, you must also add the auto485 registry entry to
activate those trigger levels.
In fact it is probably a good idea to have all of the entries in the registry,
for example if you have a single channel board that has its address switch set to
0x280 hex (positions 5 and 7 OFF and 1,2,3,4,6,8 ON the following sequence should
setup the system to, on subsequent reboots initialze the card to take advantage of the
850 features.

850util -i
(this installs the driver)

850util -ar 280 64
(this sets the receive trigger level to 64 bytes)

850util -at 280 64
(this sets the transmit trigger level to 64 bytes)

850util -aa 280 1
(this sets the card to use automatic RS-485 transmitter control)

reboot the system here and the new settings will take effect.


