All of this assumes that you have the board installed via the normal manner,
and have executed the serialgt program and set the port settings prior to
following these directions.
(for your specific needs the enable 485,disable cts, and rx echo cancel boxes
should be checked, and the auto 485 and enable source selections should be
unchecked)
Enabling auto485 in the serialgt application WILL NOT WORK with the board
modified for 16C950's, please leave this box UNCHECKED!
Checking the 'enable source' box will make the automatic 485 mode setup below
inoperable as well, as it switches 485 control to the RTS signal line. So
leave the enable source box UNCHECKED!


To obtain 4.0Mbps:

set clock generator to 60.0MHz.  The serialgt program will not let you
select values that are above 1.5Mbps (24MHz).  So you will have to edit
the clock generator setting in the registry manually.  To do this on NT4
open the registry editor (regedt32.exe) and expand
HKEY_LOCAL_MACHINE
SYSTEM
CurrentControlSet
Services
serial (or serialp if you installed the driver separatly from the stock serial.sys)
FASTCOM
FC4222P_X_0 (the X is the board #, if it is the first board then it will be 1)

Edit the ICD_Clockbits value to be (hex) 0x280310
Edit the ICD_Numbits value to be (hex) 0x16

make note of the COM# associated with this port

While you are in the registry, expand out
HKEY_LOCAL_MACHINE
SYSTEM
CurrentControlSet
Services
serial (or serialp if you installed the driver separatly from the stock serial.sys)
parameters

look in the SerialPX keys to find the matching COM# from above and then 
make note of the PortAddress value

You will need this number and that number+8 to pass to the 950util program.
for my particular system this number = DC00, so substitue your number
everywhere you see DC00 below. (the +8 value for me is DC08)

execute:
950util -i
950util -as DC00 15
950util -ar DC00 64
950util -at DC00 64
950util -aa DC00 1
950util -as DC08 15
950util -ar DC08 64
950util -at DC08 64
950util -aa DC08 1


On the next boot, the clock generator will be set to 60MHz, and the sample
rate will be set to 15 samples per bit, the trigger levels will be set to 64 bytes
for both transmit and receive, the automatic 485 control mode will be enabled.
(and assuming you did the serialgt settings correctly, the cts signal will be
disabled, and the 485 mode will be enabled, and receive echo canceling will be
enabled (you will not receive what you transmit)).
When you set your port to 115200bps you will yield an effective bitrate of
(60000000/15)/1 = 4.0Mbps.


It is a very good idea to install multiple boards one at a time such that you
can be sure which keys relate to which cards.

This procedure is similar for windows 2000, only the registry keys are in different
locations.  I believe that the key that contains the clock settings on W2K
will be under HKEY_LOCAL_MACHINE\SYSETM\CurrentControlSet\Services\fcapci\Fastcom\FC4222PCI_xxxxxx
The values will be
Clockbits
Numbits

Then to find the address(s) to pass to 950util use device manager
(ControlPanel->System->Hardware tab->device manager
expand multiport serial adapters
select Fastcom:422/2-PCI, properties, resources tab
The number we are interested in is the first number in the second io range
For the example above it would list it as 'I/O range DC00 - DC0F' and in
general it will the range that is XXX0 - XXXF.


cg
3/1/2002

