Oxford 16C950 utilities:

Usage:
950util -i                                      -> install driver
950util -r                                      -> remove driver 
950util -s                                      -> start driver  
950util -t                                      -> stop driver    
950util -as base samples        -> set sample rate registry setting
950util -ar base rxtrigger      -> set rxtrigger registry setting
950util -at base txtrigger      -> set txtrigger registry setting
950util -aa base auto485        -> set enable/disable auto485 registry setting
950util -d  base                        -> delete port settings from registry
950util -is base samples        -> immediate set sample rate
950util -ir base rxtrigger      -> immediate set rx trigger
950util -it base txtrigger      -> immediate set tx trigger
950util -ia base auto485    -> immediate enable/disable auto 485

Note that if you change the triggers via ir or it that it does not enable
those trigger values until you call the -ia function.
The correlary to this is that if you set the registry entries to specify
the triggers on startup, you must also add the auto485 registry entry to
activate those trigger levels.
In fact it is probably a good idea to have all of the entries in the registry,
for example if you have a single channel board that has its address switch set to
0x280 hex (positions 5 and 7 OFF and 1,2,3,4,6,8 ON the following sequence should
setup the system to, on subsequent reboots initialze the card to take advantage of the
950 features.

950util -i
(this installs the driver)

950util -as 280 15
(this sets the sample rate to 15 samples, or a max bitrate of 4Mbps)

950util -ar 280 64
(this sets the receive trigger level to 64 bytes)

950util -at 280 64
(this sets the transmit trigger level to 64 bytes)

950util -aa 280 1
(this sets the card to use automatic RS-485 transmitter control)

reboot the system here and the new settings will take effect.


