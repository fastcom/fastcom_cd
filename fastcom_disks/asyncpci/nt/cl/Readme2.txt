!The information on fcset in the previous readme is out of date.  The new
functions that were added modified the operation of the fcset example.!

'on the fly' operation in windows NT4.0 for FASTCOM async serial PCI cards.
to install the necessary drivers execute:

fcpinstall -i
850util -i

once this is done you can run the fcset utility, or write your own code 
that uses the functions in the fcputil.h/.dll to access all of the functions
that are available in serialgt.
The only function that you can't change is the "transmit write size", 
that value is the TXFIFO registry entry for the serial driver/subkey, and 
it is only loaded by the serial driver on boot/driver load.

The rest of the options are transparent to the driver.  It is probably a 
good idea to have the port closed when changing options, but other than 
that it should be a changeable "on the fly" operation.

!Important!
//note the state variables are not kept in the dll if the dll is unloaded, 
//as a result of this if you execute fcset multiple times, say
//the first time executing -enable485, and the next time with -rxechocancel
//the second call will "turn off" the enable485 option
//the only way around this is to do "both" durring the same instance of the dll.
//so if you write a function that calls both functions before program exit it will
//work correctly.
//the values affected by this are -enable485,-ctsdisable,-rxechocancel,and -485source
//also you should set the trigger values before the 485 function, just as in the 850utils docs
//so call all of the functions in one program in this order:
/*
set_max_baud(unsigned com_number, double max_bitrate);
set_4x(unsigned com_number, BOOLEAN state);
set_txtrigger(unsigned com_number, ULONG value);
set_rxtrigger(unsigned com_number, ULONG value);
set_auto485(unsigned com_number, BOOLEAN state);
set_enable485(unsigned com_number, BOOLEAN state);
set_ctsdisable(unsigned com_number, BOOLEAN state);
set_rxechocancel(unsigned com_number, BOOLEAN state);
set_485source(unsigned com_number, BOOLEAN state);
*/
Also the max baud function does not take into consideration the 4x function
(ie the 115.2k rate will either be the max_bitrate as set, or max_bitrate/4)



cg
11/13/2002

files required:
850util.exe
fcpinstall.exe
850utils.sys
fcpclockset.sys
fcputil.dll
fcputil.lib
fcputil.h
[fcset.c]
[fcset.exe]

