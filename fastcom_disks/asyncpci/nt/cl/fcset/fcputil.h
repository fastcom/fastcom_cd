
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the FCPUTIL_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// FCPUTIL_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef FCPUTIL_EXPORTS
#define FCPUTIL_API __declspec(dllexport)
#else
#define FCPUTIL_API __declspec(dllimport)
#endif

FCPUTIL_API double set_max_baud(unsigned com_number, double max_bitrate);
FCPUTIL_API BOOLEAN set_4x(unsigned com_number, BOOLEAN state);
FCPUTIL_API BOOLEAN set_auto485(unsigned com_number, BOOLEAN state);
FCPUTIL_API BOOLEAN set_enable485(unsigned com_number, BOOLEAN state);
FCPUTIL_API BOOLEAN set_ctsdisable(unsigned com_number, BOOLEAN state);
FCPUTIL_API BOOLEAN set_rxechocancel(unsigned com_number, BOOLEAN state);
FCPUTIL_API BOOLEAN set_485source(unsigned com_number, BOOLEAN state);
FCPUTIL_API BOOLEAN set_txtrigger(unsigned com_number, ULONG value);
FCPUTIL_API BOOLEAN set_rxtrigger(unsigned com_number, ULONG value);
