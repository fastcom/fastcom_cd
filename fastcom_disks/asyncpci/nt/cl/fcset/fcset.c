#include "windows.h"
#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "string.h"
#include "fcputil.h"

void main(int argc, char * argv[])
{
	unsigned comport;
	double bitrate;
	double actual;
	unsigned long val;
	if(argc<3)
	{
		printf("usage:\r\n");
		printf("fcset comport -baud maxbaud\n");
		printf("fcset comport -4x [1|0]\n");
		printf("1==enable divide by 4, 0==divide by 1\n\n");
		printf("fcset comport -auto485 [1|0]\n");
		printf("1==enable automatic RS485 control, 0 == standard (RS422) operation\n\n");
		printf("fcset comport -enable485 [1|0]\n");
		printf("1== turn on RS-485, 0== RS-422\n\n");
		printf("fcset comport -ctsdisable [1|0]\n");
		printf("1==disable CTS signal, 0==CTS from connector\n\n");
		printf("fcset comport -rxechocancel [1|0]\n");
		printf("1==disable receiver while transmitting, 0==normal operation (receiver allways on)\n\n");
		printf("fcset comport -485source [1|0]\n");
		printf("1==485 controlled by RTS, 0 = 485 controlled by OUT1\n\n");
		printf("fcset comport -txtrig txtrigger\n");
		printf("fcset comport -rxtrig rxtrigger\n");
		exit(1);
	}
	//note the state variables are not kept in the dll if the dll is unloaded, 
	//as a result of this if you execute fcset multiple times, say
	//the first time executing -enable485, and the next time with -rxechocancel
	//the second call will "turn off" the enable485 option
	//the only way around this is to do "both" durring the same instance of the dll.
	//so if you write a function that calls both functions before program exit it will
	//work correctly.
	//the values affected by this are -enable485,-ctsdisable,-rxechocancel,and -485source
	//also you should set the trigger values before the 485 function, just as in the 850utils docs
	//so call all of the functions in one program in this order:
	/*
	double set_max_baud(unsigned com_number, double max_bitrate);
	set_4x(unsigned com_number, BOOLEAN state);
	set_txtrigger(unsigned com_number, ULONG value);
	set_rxtrigger(unsigned com_number, ULONG value);
	set_auto485(unsigned com_number, BOOLEAN state);
	set_enable485(unsigned com_number, BOOLEAN state);
	set_ctsdisable(unsigned com_number, BOOLEAN state);
	set_rxechocancel(unsigned com_number, BOOLEAN state);
	set_485source(unsigned com_number, BOOLEAN state);
	*/
	
	
	sscanf(argv[1],"%u",&comport);
	
	if(strncmp(argv[2],"-baud",5)==0)
	{
		
		bitrate = atof(argv[3]);
		//printf("rate=%f\r\n",bitrate);
		//printf("rate=%s\r\n",argv[2]);
		actual = set_max_baud(comport,bitrate);
		if(actual==0) 
		{
			printf("failed to set com%u to %f\r\n",comport,bitrate);
		}
		else printf("set com%u to %f bps(max)\r\n",comport,actual);
	}
	
	if(strncmp(argv[2],"-4x",3)==0)
	{
		val = atoi(argv[3]);
		set_4x(comport,(UCHAR)val);
		if(val) printf("enabled divide by 4 on com:%u\n",comport);
		else printf("enabled divide by 1 on com%u\n",comport);
	}
	
	if(strncmp(argv[2],"-auto485",8)==0)
	{
		val = atoi(argv[3]);
		set_auto485(comport,(UCHAR)val);
		if(val) printf("enabled automatic RS485 control on com:%u\n",comport);
		else printf("disabled automatic RS485 control on com%u\n",comport);
	}
	
	if(strncmp(argv[2],"-enable485",10)==0)
	{
		val = atoi(argv[3]);
		set_enable485(comport,(UCHAR)val);
		if(val) printf("enabled RS485 control on com:%u\n",comport);
		else printf("disabled RS485 control on com%u\n",comport);
	}
	
	if(strncmp(argv[2],"-ctsdisable",11)==0)
	{
		val = atoi(argv[3]);
		set_ctsdisable(comport,(UCHAR)val);
		if(val) printf("Disabled CTS signal on com:%u\n",comport);
		else printf("Enabled CTS signal on com%u\n",comport);
	}
	
	if(strncmp(argv[2],"-rxechocancel",13)==0)
	{
		val = atoi(argv[3]);
		set_rxechocancel(comport,(UCHAR)val);
		if(val) printf("Receive Echo cancel ON for com:%u\n",comport);
		else printf("Receive Echo cancel OFF for com%u\n",comport);
	}
	
	if(strncmp(argv[2],"-485source",10)==0)
	{
		val = atoi(argv[3]);
		set_rxechocancel(comport,(UCHAR)val);
		if(val) printf("485 control by RTS on com:%u\n",comport);
		else printf("485 control by OUT1 on com%u\n",comport);
	}
	
	
	if(strncmp(argv[2],"-txtrig",7)==0)
	{
		val = atoi(argv[3]);
		set_txtrigger(comport,val);
		printf("txtrigger on com:%u set to %d\n",comport,val);
	}
	
	if(strncmp(argv[2],"-rxtrig",7)==0)
	{
		val = atoi(argv[3]);
		set_rxtrigger(comport,val);
		printf("rxtrigger on com:%u set to %d\n",comport,val);
	}
	
}