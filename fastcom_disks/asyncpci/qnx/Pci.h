#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <i86.h>
#include <string.h>
#include <sys/seginfo.h>
#include <sys/osinfo.h>
#include <sys/pci.h>
#include <sys/inline.h>
#include <math.h>

typedef struct clkset{
	unsigned long clockbits;
	unsigned numbits;
} CLKSET;
typedef unsigned USHORT;
typedef unsigned long DWORD;
typedef unsigned long ULONG ;
typedef unsigned BOOL;

#define TRUE 1
#define FALSE 0

#define  FC2328P   (ULONG)1
#define  FC2324P   (ULONG)2
#define  FCIG2322P (ULONG)3
#define  FCIG4221P (ULONG)4
#define  FC4222P   (ULONG)5
#define  FC4224P   (ULONG)6

//defines for 2053b
#define STARTWRD 0x1e05
#define MIDWRD   0x1e04
#define ENDWRD   0x1e00

ULONG portspertype[10] = {0,
						8,
						4,
						2,
						1,
						2,
						4,
						0,
						0,
						0
						};


get_addresses(unsigned busnum, unsigned devfuncnum);
void get_add();
void set_clock_generator(unsigned port, ULONG hval,ULONG nmbits);
BOOL calculate_bits(double reffreq,double desired_freq,double *m_actual_clock,CLKSET *clk);

unsigned long amcc_base[10];
unsigned long uart_base[10];
unsigned long board_base[10];
unsigned board_irq[10];
unsigned long validtype[10];

unsigned board_count = 0;
void main(int argc, char *argv[])
{
CLKSET clk;
unsigned long txtrigger;
unsigned long rxtrigger;
unsigned long auto485;
unsigned long dividemode;
double reffreq= 18432000.0;
double desiredfreq;
double actual_clock;
long int i,j;
unsigned long board;
unsigned port;
unsigned long settings;

//printf("code starting\r\n");
for(i=0;i<10;i++)
	{
	amcc_base[i]=0;
	uart_base[i]=0;
	board_base[i]=0;
	board_irq[i]=0;
	validtype[i]=0;
	}
board_count=0;

get_add();
//printf("got addresses, boardcount:%d\r\n",board_count);

if(board_count==0)
	{
	printf("no Fastcom PCI cards found in system \r\n");
	exit(1);
	}

if(argc<2)
	{
	printf("usage:\r\n");
	printf("fcpci -t board port txtrigger\r\n");
	printf("fcpci -r board port rxtrigger\r\n");
	printf("fcpci -a board port [1|0]\r\n");
	printf("fcpci -d board port [1|0]\r\n");
	printf("fcpci -c board top_frequency\r\n");
	printf("fcpci -s board settings\r\n");
	printf("fcpci -l \r\n");
	printf("\r\nnote both port and board are zero based (ie 0,1,2,3...)\r\n");
	printf("\r\n");
	printf("-t set transmit trigger level to txtrigger (range 0 to 128)\r\n");
	printf("-r set receive trigger level to rxtrigger (range 0 to 128)\r\n");
	printf("-a 1== turn on auto 485 mode, 0==turn off auto 485 mode \r\n");
	printf("-d 1== turn on divide by 1 mode, 0 = turn on divide by 4 mode \r\n");
	printf("-c set input clock frequency (16 X max baud rate)(range 391000. to 24000000.)\r\n");
	printf("-l list cards and board numbers\r\n");
	printf("-s set extra board settings\r\n");
	printf("   enter settings as a hex value\r\n");
	printf("   extra settings for 422 cards are 4 bits per port defined as\r\n");
	printf("   bit0: 1= cts disable, 0 = cts from connector\r\n");
	printf("   bit1: 1= enable RS485 driver enables, 0 =  force driver allways on (422mode)\r\n");
	printf("   bit2: 1= cancel rx while tx, 0 = receiver allways on\r\n");
	printf("   bit3: 1= 485 control = RTS pin, 0=485 control=OUT1 pin\r\n");
	printf("         (use 0 for automatic 485, 1 for software control\r\n");
	printf("   bit4: == bit0 of channel 1...etc\r\n");
	printf("   default settings is settings=0x00000000 which decodes to \r\n");
	printf("   cts from connector, 422 mode, receive allways on, 485 control is OUT1\r\n");
	printf("\r\n");
	printf("for fully automatic RS-485 control you must run a fcpci -a on the\r\n");
	printf("port and then also do a fcpci -s on the port with bit 1 set for that port\r\n");
	printf("the settings bit3 for the port should be 0, this will allow the \r\n");
	printf("UART to directly control the on/off state of the transmitter when\r\n");
	printf("it has data to send\r\n");


	exit(1);
	}
if(argv[1][0]!='-')
	{
	printf("invalid option--run fcpci with no parameters for usage\r\n");
	exit(1);
	}

if(argv[1][1]=='l')
{
for(i=0;i<board_count;i++)
	{
	if(validtype[i]==FC2328P) printf("board:%d--Fastcom:232//8-PCI @ 0x%x,irq:%d\r\n",i,uart_base[i],board_irq[i]);
	if(validtype[i]==FC2324P) printf("board:%d--Fastcom:232//4-PCI @ 0x%x,irq:%d\r\n",i,uart_base[i],board_irq[i]);
	if(validtype[i]==FCIG2322P) printf("board:%d--Fastcom:IG232//2-PCI @ 0x%x,irq:%d\r\n",i,uart_base[i],board_irq[i]);
	if(validtype[i]==FCIG4221P) printf("board:%d--Fastcom:IG422//1-PCI @ 0x%x,irq:%d\r\n",i,uart_base[i],board_irq[i]);
	if(validtype[i]==FC4222P) printf("board:%d--Fastcom:422//2-PCI @ 0x%x,irq:%d\r\n",i,uart_base[i],board_irq[i]);
	if(validtype[i]==FC4224P) printf("board:%d--Fastcom:422//4-PCI @ 0x%x,irq:%d\r\n",i,uart_base[i],board_irq[i]);
	}
exit(0);
}

if(argc<3)
{
printf("not enough parameters\r\n");
exit(1);
}
board = atoi(argv[2]);
if(board > board_count)
	{
	printf("board specified not in system, valid range:%d->%d\r\n",0,board_count);
	exit(1);
	}

outp(amcc_base[board]+0x39,0x20);//turn on irq


if((amcc_base[board]==0)||(uart_base[board]==0)||(board_base[board]==0))
	{
	printf("board not found\r\n");
	exit(1);
	}
if(argv[1][1]=='t')
	{
if(argc<5)
{
printf("not enough parameters to set txtrigger\r\n");
exit(1);
}
	txtrigger = atol(argv[4]);
	port = atol(argv[3]);
	//set transmit trigger:
	//printf("Set Startech to TX trigger table D mode\r\n");
	outp(uart_base[board]+(port*8)+3,0xBF);//set to get to Extended regs
	outp(uart_base[board]+(port*8)+2,0x10);//set the EFR bit (enable extendeds);
	outp(uart_base[board]+(port*8)+1,(char)((inp(uart_base[board]+(port*8)+1)&(char)0xcf)|(char)0xb0));//set to tx tab d (not killing other modes)
	outp(uart_base[board]+(port*8),(char)txtrigger);//get user trig setting and set here 
	outp(uart_base[board]+(port*8)+2,0x00);//clear the EFR bit (Latch extendeds);
	outp(uart_base[board]+(port*8)+3,0x03);//set to get to Normal regs
	printf("set port %d to a transmit trigger level of %d\r\n",port,txtrigger&0xff);
	}
if(argv[1][1]=='r')
	{
if(argc<5)
{
printf("not enough parameters to set rxtrigger\r\n");
exit(1);
}
	rxtrigger = atol(argv[4]);
	port = atol(argv[3]);
	//set receive trigger
	//printf("Set Startech to RX trigger table D mode\r\n");
	outp(uart_base[board]+(port*8)+3,0xBF);//set to get to Extended regs
	outp(uart_base[board]+(port*8)+2,0x10);//set the EFR bit (enable extendeds);
	outp(uart_base[board]+(port*8)+1,(char)((inp(uart_base[board]+(port*8)+1)&(char)0x4f)|(char)0x30));//set to rx tab d (not killing other modes)
	outp(uart_base[board]+(port*8),(char)rxtrigger);//get user trig setting and set here
	outp(uart_base[board]+(port*8)+2,0x00);//clear the EFR bit (Latch extendeds);
	outp(uart_base[board]+(port*8)+3,0x03);//set to get to Normal regs
	printf("set port %d to a receive trigger level of %d\r\n",port,rxtrigger&0xff);
	}
if(argv[1][1]=='a')
	{
if(argc<5)
{
printf("not enough parameters to set auto485 mode\r\n");
exit(1);
}
	auto485 = atol(argv[4]);
	port = atol(argv[3]);
	if(auto485==1)
		{
		//printf("Set Startech to Auto 485 mode\r\n");
		outp(uart_base[board]+(port*8)+3,0xBF);//set to get to Extended regs
		outp(uart_base[board]+(port*8)+2,0x10);//set the EFR bit (enable extendeds);
		outp(uart_base[board]+(port*8)+1,(char)((inp(uart_base[board]+(port*8)+1)&(char)0xf7)|(char)0x8));//set to auto 485 mode
		outp(uart_base[board]+(port*8)+2,0x00);//clear the EFR bit (Latch extendeds);
		outp(uart_base[board]+(port*8)+3,0x03);//set to get to Normal regs
		printf("set port %d to automatic 485 mode\r\n",port);
		}
	else
		{
		//printf("Set Startech to normal (non auto 485) mode\r\n");
		outp(uart_base[board]+(port*8)+3,0xBF);//set to get to Extended regs
		outp(uart_base[board]+(port*8)+2,0x10);//set the EFR bit (enable extendeds);
		outp(uart_base[board]+(port*8)+1,(char)((inp(uart_base[board]+(port*8)+1)&(char)0xf7)));//set to not auto 485 mode
		outp(uart_base[board]+(port*8)+2,0x00);//clear the EFR bit (Latch extendeds);
		outp(uart_base[board]+(port*8)+3,0x03);//set to get to Normal regs
		printf("set port %d to normal (422) mode\r\n",port);
		}

	}
if(argv[1][1]=='d')
	{
if(argc<5)
{
printf("not enough parameters to set clock divide by 1|4 mode\r\n");
exit(1);
}

	dividemode = atol(argv[4]);
	port = atol(argv[3]);
	if(dividemode==1)
		{
		//printf("Set to div by 1 mode\n");
		outp(uart_base[board]+(port*8)+3,0xBF);//set to get to Extended regs
		outp(uart_base[board]+(port*8)+2,0x10);//set the EFR bit (enable extendeds);
		outp(uart_base[board]+(port*8)+3,0x03);//set to get to Normal regs
		outp(uart_base[board]+(port*8)+4,0x00);//set to /1 mode (will give 4x baud rates with a 7.3728MHz inclock
		outp(uart_base[board]+(port*8)+3,0xBF);//set to get to Extended regs
		outp(uart_base[board]+(port*8)+2,0x00);//clear the EFR bit (Latch extendeds);
		outp(uart_base[board]+(port*8)+3,0x03);//set to get to Normal regs
		printf("set port %d to divide by 1 mode\r\n",port);
		}
	else
		{
		//printf("set to div by 4 mode\n");
		outp(uart_base[board]+(port*8)+3,0xBF);//set to get to Extended regs
		outp(uart_base[board]+(port*8)+2,0x10);//set the EFR bit (enable extendeds);
		outp(uart_base[board]+(port*8)+3,0x03);//set to get to Normal regs
		outp(uart_base[board]+(port*8)+4,0x80);//set to /4 mode (will give 1x baud rates with a 7.3728MHz inclock
		outp(uart_base[board]+(port*8)+3,0xBF);//set to get to Extended regs
		outp(uart_base[board]+(port*8)+2,0x00);//clear the EFR bit (Latch extendeds);
		outp(uart_base[board]+(port*8)+3,0x03);//set to get to Normal regs
		printf("set port %d to divide by 4 mode \r\n",port);
		}
	}
if(argv[1][1]=='c')
	{
if(argc<4)
{
printf("not enough parameters to set top frequency for board\r\n");
exit(1);
}

	desiredfreq = atof(argv[3]);
if(desiredfreq==0) 
{
printf("invalid frequency specified\r\n");
exit(1);
}
printf("ref:%f, desired:%f\r\n",reffreq,desiredfreq);
	calculate_bits(reffreq,desiredfreq,(double*)&actual_clock,(CLKSET*)&clk);
//printf("clockbits:%lx , numbits:%d\r\n",clk.clockbits,clk.numbits);

	if((clk.clockbits!=0)&&(clk.numbits!=0))
		{
		if(validtype[board]==FC4222P) set_clock_generator(board_base[board]+1,clk.clockbits,clk.numbits);			
		if(validtype[board]==FC4224P) set_clock_generator(board_base[board]+2,clk.clockbits,clk.numbits);			
		if(validtype[board]==FCIG4221P) set_clock_generator(board_base[board]+1,clk.clockbits,clk.numbits);			
		if(validtype[board]==FCIG2322P) set_clock_generator(board_base[board],clk.clockbits,clk.numbits);			
		if(validtype[board]==FC2324P) set_clock_generator(board_base[board],clk.clockbits,clk.numbits);			
		if(validtype[board]==FC2328P) set_clock_generator(board_base[board],clk.clockbits,clk.numbits);			
		printf("set clock to %f\r\n",actual_clock);
		}
	}
if(argv[1][1]=='s')
	{
if(argc<4)
{
printf("not enough parameters to set board settings\r\n");
exit(1);
}

sscanf(argv[3],"%x",&settings);

	if(validtype[board]==FC4222P) outp(board_base[board],settings);
	if(validtype[board]==FC4224P) 
		{
		outp(board_base[board],settings);
		outp(board_base[board]+1,(settings>>8));
		}
	if(validtype[board]==FCIG4221P) outp(board_base[board],settings);
	}
printf("to start up driver use the following line:\r\n");
printf("/bin/Dev.ser 3f8,4 2f8,3 ");
for(i=0;i<=board_count;i++)for(j=0;j<portspertype[validtype[i]];j++) printf("%x,%d ",uart_base[i]+(8*j),board_irq[i]);
printf(" &\r\n");
}




//function to calcluate input clock params.
BOOL calculate_bits(double reffreq,double desired_freq,double *m_actual_clock,CLKSET *clk)
{
DWORD P;
DWORD Pprime;
DWORD Q;
DWORD Qprime;
DWORD M;
DWORD I;
DWORD D;
double fvco;
double desired_ratio;
double actual_ratio;
DWORD bestP;
DWORD bestQ;
DWORD bestM;
DWORD bestI;
double best_actual_ratio;
DWORD Progword;
DWORD Stuffedword;
DWORD bit1;
DWORD bit2;
DWORD bit3;
DWORD i,j;
char buf[256];
unsigned long rangelo;
unsigned long rangehi;
double best_diff;

D = 0x00000000;//from bitcalc, but the datasheet says to make this 1...
bestP = 0;
bestQ = 0;
best_actual_ratio = 1000000.0;//hopefully we can do better than this...
best_diff = 1000000.0;
rangelo = (unsigned long)floor(reffreq/1000000.0) +1;
rangehi = (unsigned long)floor(reffreq/200000.0);
if(rangelo <3) rangelo = 3;
if(rangehi>129) rangehi = 129;
//sprintf(buf,"%u(%f), %u(%f)",rangelo,(reffreq/1000000.0) +1.0,rangehi,(reffreq/200000.0));
//MessageBox(buf,"rl(fl),rh(fh)",MB_OK);
for(i=0;i<=7;i++)
{
M = i;
fvco = desired_freq * (pow(2,i));
//sprintf(buf,"%f",fvco);
//MessageBox(NULL,buf,"Fvco",MB_OK);

if(fvco<80000000.0) I = 0x00000000;
if(fvco>=80000000.0) I = 0x00000008;
if((fvco>50000000.0)&&(fvco<150000000.0))
{
//sprintf(buf,"%f",fvco);
//MessageBox(NULL,buf,"Fvco",MB_OK);
desired_ratio = fvco/(2.0 * reffreq);
//sprintf(buf,"%f",desired_ratio);
//MessageBox(NULL,buf,"desired",MB_OK);

for(P=4;P<=130;P++)for(Q=rangelo;Q<=rangehi;Q++)
{

actual_ratio = (double)P/(double)Q;
if(actual_ratio==desired_ratio) 
	{
	//sprintf(buf,"%u,%u",P,Q);
	//MessageBox(NULL,buf,"Direct Hit",MB_OK);
	bestP = P;
	bestQ = Q;
	bestM = M;
	bestI = I;
	best_actual_ratio = actual_ratio;
	goto donecalc;
	}
else 
	{
	if(fabs(desired_ratio - actual_ratio)<(best_diff)) 
		{
		best_diff = fabs(desired_ratio - actual_ratio);
		best_actual_ratio = actual_ratio;
		bestP = P;
		bestQ = Q;
		bestM = M;
		bestI = I;
		//sprintf(buf,"desired:%f,actual:%f, best%f P%u,Q%u,fvco:%f,M:%u",desired_ratio,actual_ratio,best_diff,bestP,bestQ,fvco,M);
		//MessageBox(buf,"ratiocalc",MB_OK);
		}
	}       
}
}
}
donecalc:
if((bestP!=0)&&(bestQ!=0))
	{
	//here bestP BestQ are good to go.
	I = bestI;
	M = bestM;
	P = bestP;
	Q = bestQ;
	Pprime = bestP - 3;
	Qprime = bestQ - 2;
	//sprintf(buf,"P':%u, Q':%u, M:%u, I:%u",Pprime,Qprime,M,I);
	//MessageBox(buf,"P,Q,M,I",MB_OK);
	Progword = 0;
	Progword =  (Pprime<<15) | (D<<14) | (M<<11) | (Qprime<<4) | I;
	//sprintf(buf,"%lx",Progword);
//      MessageBox(buf,"Progword",MB_OK);
	bit1 = 0;
	bit2 = 0;
	bit3 = 0;
	Stuffedword = 0;
	i = 0;
	j = 0;
	bit1 = ((Progword>>i)&1);
	Stuffedword |=  (bit1<<j);
	i++;
	j++;
	bit2 = ((Progword>>i)&1);
	Stuffedword |=  (bit2<<j);
	i++;
	j++;
	bit3 = ((Progword>>i)&1);
	Stuffedword |=  (bit3<<j);
	j++;
	i++;
	while(i<=22)
		{
		if((bit1==1)&&(bit2==1)&&(bit3==1))
			{
			//force a 0 in the stuffed word;
			j++;
			bit3 = 0;
			//sprintf(buf,"i,j : %u,%u",i,j);
//                      MessageBox(buf,"Stuffing",MB_OK);
			}
		bit1 = bit2;
		bit2 = bit3;
		bit3 = ((Progword>>i)&1);
		Stuffedword |=  (bit3<<j);
		i++;
		j++;
		}
	//sprintf(buf,"SW:%lx ,numbits:%u",Stuffedword,j);

clk->clockbits = Stuffedword;
clk->numbits = (USHORT)j-1;
*m_actual_clock = ((2.0 * reffreq) * ((double)P/(double)Q)) / pow(2,M);
//      MessageBox(buf,"stuffedword, numbits",MB_OK);
return TRUE;
	}
else
{
	printf("\r\nError in ICD calculation\r\n");
	return FALSE;

}
}


void set_clock_generator(unsigned port, ULONG hval,ULONG nmbits)
{

ULONG curval;
ULONG tempval;
ULONG i;


curval = 0;
//bit 0 = data
//bit 1 = clock;
outp(port,(char)curval);
//SimplDrvKdPrint(("6"));

tempval = STARTWRD;
for(i=0;i<14;i++)
	{
	curval = 0;
	curval = (char)(tempval&0x1);   //set bit
	outp(port,(char)curval);
	curval = curval |0x02;          //force rising edge
	outp(port,(char)curval);              //clock in data
	curval = curval &0x01;          //force falling edge
	outp(port,(char)curval);              //set clock low
	tempval = tempval >> 1;         //get next bit
	}
//SimplDrvKdPrint(("7"));	
tempval = hval;
for(i=0;i<nmbits;i++)
	{
	curval = 0;
	curval = (char)(tempval&0x1);   //set bit
	outp(port,(char)curval);
	curval = curval |0x02;          //force rising edge
	outp(port,(char)curval);              //clock in data
	curval = curval &0x01;          //force falling edge
	outp(port,(char)curval);              //set clock low
	tempval = tempval >> 1;         //get next bit
	}
//SimplDrvKdPrint(("8"));
tempval = MIDWRD;
for(i=0;i<14;i++)
	{
	curval = 0;
	curval = (char)(tempval&0x1);   //set bit
	outp(port,(char)curval);
	curval = curval |0x02;          //force rising edge
	outp(port,(char)curval);              //clock in data
	curval = curval &0x01;          //force falling edge
	outp(port,(char)curval);              //set clock low
	tempval = tempval >> 1;         //get next bit
	}
//SimplDrvKdPrint(("9"));
//pause for >10ms --should be replaced with a regulation pause routine
for(i=0;i<50000;i++)outp(port,(char)curval);//might need to change for NT
//SimplDrvKdPrint(("10"));
tempval = ENDWRD;
for(i=0;i<14;i++)
	{
	curval = 0;
	curval = (char)(tempval&0x1);   //set bit
	outp(port,(char)curval);
	curval = curval |0x02;          //force rising edge
	outp(port,(char)curval);              //clock in data
	curval = curval &0x01;          //force falling edge
	outp(port,(char)curval);              //set clock low
	tempval = tempval >> 1;         //get next bit
	}
//SimplDrvKdPrint(("11"));
}

//cut directly from show_pci.c
unsigned long pci_io_mem_length(unsigned busnum, unsigned devfuncnum, unsigned pos, unsigned type)
{
	unsigned long	cur_addr, size, tmp, shift;
	
	_CA_PCI_Read_Config_DWord(busnum, devfuncnum, pos, 1, (char *)&cur_addr);
	tmp = 0xffffffff;
	_CA_PCI_Write_Config_DWord(busnum, devfuncnum, pos, 1, (char *)&tmp);
	_CA_PCI_Read_Config_DWord(busnum, devfuncnum, pos, 1, (char *)&tmp);

	if(type)
	{
		if(tmp & 0x1)
			tmp &=0xfffffffc;
		else
			tmp &=0xfffffff0;
	}
	else
		tmp &=0xfffff800;

	for(shift=0;shift<32;shift++)
	{
		size = ((unsigned long)1)<<shift;
		if((tmp>>shift) & 0x1) 
			break;
	}
	
	_CA_PCI_Write_Config_DWord(busnum, devfuncnum, pos, 1, (char *)&cur_addr);
	return size;
}
//hacked show_pci.c functions...
get_addresses(unsigned busnum, unsigned devfuncnum)
{
	int							i;
	struct _pci_config_regs		creg;
	unsigned long len;

	_CA_PCI_Read_Config_DWord(busnum, devfuncnum, 0, 16, (char *)&creg);

	if( creg.Vendor_ID == 0xffff )		return( 0 );
	if( creg.Vendor_ID != 0x10E8) return(0);
	if( creg.Device_ID == 0x8291) validtype[board_count] = FC2328P;
	if( creg.Device_ID == 0x82CA) validtype[board_count] = FC2324P;
	if( creg.Device_ID == 0x82C7) validtype[board_count] = FCIG2322P;
	if( creg.Device_ID == 0x82C6) validtype[board_count] = FCIG4221P;
	if( creg.Device_ID == 0x82C5) validtype[board_count] = FC4222P;
	if( creg.Device_ID == 0x82C4) validtype[board_count] = FC4224P;
	
	if(validtype[board_count]==0) return(0);

	switch(creg.Header_Type & 0x7f)
	{
		case 0:
			for(i = 0; i < 6; i++)
			{
				if(creg.Base_Address_Regs[i] == 0)	break;
				if(PCI_IS_IO(creg.Base_Address_Regs[i]))
				{
					len =pci_io_mem_length(busnum, devfuncnum, 0x10+(i*4), 1);
					if((i==0)&&(len==128)) amcc_base[board_count] = PCI_IO_ADDR(creg.Base_Address_Regs[i]);
					if((i==1)) uart_base[board_count] = PCI_IO_ADDR(creg.Base_Address_Regs[i]);
					if((i==2)&&(len==4)) board_base[board_count] = PCI_IO_ADDR(creg.Base_Address_Regs[i]);
				}
			}
			if(creg.Interrupt_Line <= 0xf)
				{
				//printf("Interrupt line = %d\n", creg.Interrupt_Line);
				board_irq[board_count] = creg.Interrupt_Line;
				}
			else if(creg.Interrupt_Line == 0xff) board_irq[board_count] = 0;
				//printf("Interrupt line = no connection\n");
			else board_irq[board_count] = 0;
				//printf("Interrupt line = reserved\n");
			break;

		default:
			break;
	
	}
	if(validtype[board_count]!=0) board_count++;

	return 0;
}
void get_add()
{
	unsigned lastbus, busnum, device, function, num_fcns;
	unsigned header_type, device_id, version, hardware;
	struct _osinfo info;

	if(qnx_osinfo(0,(struct _osinfo*) &info) == -1)
	{
		printf("NO BIOS32 found\n");
		exit(0);
	}

	if(!(info.sflags & _PSF_PCI_BIOS))		// we have a PCI BIOS
	{
		printf("NO BIOS32 found\n");
		exit(0);
	}

	if(_CA_PCI_BIOS_Present((unsigned*)&lastbus,(unsigned*) &version,(unsigned*) &hardware) != PCI_SUCCESS)
	{
		printf("NO BIOS32 found\n");
		exit(0);
	}


	if(lastbus > 0x40)			// KLUDGE FOR AMI BIOS it reports 66 buses
		lastbus = 0;

	for(busnum = 0; busnum <= lastbus; busnum++)
	{
		for(device = 0; device < 32; device++)
		{
			_CA_PCI_Read_Config_Word(busnum, device << 3, offsetof(struct _pci_config_regs, Device_ID),
									1, (char *)&device_id);
			if(device_id == 0xffff)
				continue;
			_CA_PCI_Read_Config_Byte(busnum, device << 3, offsetof(struct _pci_config_regs, Header_Type),
									1, (char *)&header_type);
			if(header_type & 0x80) {
				num_fcns = 8;
			} else {
				num_fcns = 1;
			}
			for(function = 0; function < num_fcns; function++)
			{
			get_addresses(busnum, (device << 3) | function);
			}
		}
	}
	
}