#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "string.h"
#include "fcputil.h"

void main(int argc, char * argv[])
{
unsigned comport;
double bitrate;
double actual;
if(argc<3)
{
printf("usage:\r\n");
printf("fcset comport maxbaud");
exit(1);
}

sscanf(argv[1],"%u",&comport);
bitrate = atof(argv[2]);
//printf("rate=%f\r\n",bitrate);
//printf("rate=%s\r\n",argv[2]);
actual = set_max_baud(comport,bitrate);
if(actual==0) 
	{
	printf("failed to set com%u to %f\r\n",comport,bitrate);
	}
else printf("set com%u to %f bps(max)\r\n",comport,actual);

}