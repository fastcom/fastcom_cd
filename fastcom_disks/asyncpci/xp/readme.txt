This is the driver package for the Fastcom:xxx-PCI async cards.
It consists of:

fastcom.inf - the inf installer file for the physical card
fcports.inf - the inf file for the individual ports
fcapci.sys  - the driver file for the cards
fcfilt.sys  - an upper filter driver for the regular serial driver stack
serialp.sys - a modified ddk serial driver allowing IRQ sharing among other fixes
fcports.dll - control panel property page provider (replaces serialgt.exe)

You should be able to point the "found new hardware wizard" to this 
location and have it recognize the Fastcom card that is installed.
Then point it back to this location for (possibly each) individual
port install.

cg
2/19/03


