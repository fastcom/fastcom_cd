//dio24.h Copyright(C)1999 Commtech, Inc.
//
//

#define IOCTL_READ_REGISTER 1
#define IOCTL_WRITE_REGISTER 2
#define IOCTL_GET_MAX_BOARD 3

#define ERROR_BAD_PORT_NUMBER			-2
#define ERROR_NO_PORTS_DEFINED			-11

typedef struct dioregs{
unsigned board;//board # to write to in multiboard install
unsigned reg;//register (0-3) (a,b,c,ctrl)
unsigned val;//value to write to register
} DIOREG;


#define REG_A		0
#define REG_B		1
#define REG_C		2
#define REG_CTRL	3

