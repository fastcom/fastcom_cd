//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by diotest.rc
//
#define IDD_DIOTEST_DIALOG              102
#define IDR_MAINFRAME                   128
#define IDC_PA0                         1000
#define IDC_PA1                         1001
#define IDC_PA2                         1002
#define IDC_PA3                         1003
#define IDC_PA4                         1004
#define IDC_PA5                         1005
#define IDC_PA6                         1006
#define IDC_PA7                         1007
#define IDC_PB0                         1008
#define IDC_PB1                         1009
#define IDC_PB2                         1010
#define IDC_PB3                         1011
#define IDC_PB4                         1012
#define IDC_PB5                         1013
#define IDC_PB6                         1014
#define IDC_PB7                         1015
#define IDC_PC0                         1016
#define IDC_PC1                         1017
#define IDC_PC2                         1018
#define IDC_PC3                         1019
#define IDC_PC4                         1020
#define IDC_PC5                         1021
#define IDC_PC6                         1022
#define IDC_PC7                         1023
#define IDC_ADIR                        1024
#define IDC_BDIR                        1025
#define IDC_CLDIR                       1026
#define IDC_CHDIR                       1027
#define IDC_UPDATE                      1029
#define IDC_BOARDNUM                    1030
#define IDC_MAXBOARD                    1031

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1032
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
