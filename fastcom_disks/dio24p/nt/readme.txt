Windows NT Fastcom:DIO24H-PCI device driver info.
The functions that the driver supports are:



 hDIO = CreateFile("\\\\.\\DIO24P0", 0,0,0,
                        OPEN_EXISTING, FILE_FLAG_DELETE_ON_CLOSE, 0);

This function is used to open the device.  It either returns a valid handle 
to the device or INVALID_HANDLE_VALUE.

There are two DeviceIoControl() functions.  One to read a register on the
dio board, one to write a register.
The structure used to pass the register and value is:
typedef struct dioregs{
unsigned reg;//register (0-3) (a,b,c,ctrl)
char val;//value to write to register
} DIOREG;

The format to write a register is:

dreg.reg = REG_CTRL;
dreg.val = 0xff;//set to all input
DeviceIoControl(hDIO, IOCTL_WRITE_REGISTER,&dreg, sizeof(DIOREG),(LPVOID)NULL,0,&cbBytesReturned, NULL);
This will allways return true unless you pass an invalid parameter (ie not a struct dioregs) (returns ERROR_INVALID_PARAMETER).


Reading a register is:

dreg.reg = REG_A;
DeviceIoControl(hDIO, IOCTL_READ_REGISTER,&dreg, sizeof(DIOREG),&a,sizeof(DWORD), &cbBytesReturned, NULL);

This will allways return true unless you pass an invalid parameter (ie not a struct dioregs) (returns ERROR_INVALID_PARAMETER).
The value of the register specified will be in a (a ULONG or DWORD).  The function allways returns a DWORD, but only the lower 8 bits are valid.


device history
2-26-99 initial release
