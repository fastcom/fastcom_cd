// simple.c : simple dio24p program
//

#include "windows.h"
#include "stdlib.h"
#include "string.h"
#include "stdio.h"
#include "dio24.h"

//globals


int main(int argc,char *argv[])
{
HANDLE hDIO;
char devname[256];
DIOREG dregs;
DWORD cbBytesReturned;
DWORD pv;

if(argc<3)
{
printf("usage:\n%s board port register [value(hex)]\r\n",argv[0]);
exit(1);
}

	sprintf(devname,"\\\\.\\DIO24P%d",atoi(argv[1]));
//only create it if we haven't previously

    hDIO = CreateFile(devname, GENERIC_READ|GENERIC_WRITE,FILE_SHARE_READ|FILE_SHARE_WRITE,0,OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
    if ( hDIO == INVALID_HANDLE_VALUE )
    {
	printf("cannot open %s\r\n",devname);
	exit(1);
	}
if(argc<4)
{
	//we are doing a read (no value specified)
dregs.reg = atoi(argv[2]);
dregs.val = 0;
pv = 0;
DeviceIoControl(hDIO, IOCTL_READ_REGISTER,&dregs, sizeof(DIOREG),&pv,sizeof(DWORD),&cbBytesReturned, NULL);
printf("in from port %d ==> %2.2x\r\n",atoi(argv[2]),pv&0xff);
}
else
{
//value specified, so do a write
//value is assumed hex
dregs.reg = atoi(argv[2]);
sscanf(argv[3],"%x",&dregs.val);
DeviceIoControl(hDIO, IOCTL_WRITE_REGISTER,&dregs, sizeof(DIOREG),(LPVOID)NULL,0,&cbBytesReturned, NULL);
printf("out to port %d <== %2.2x\r\n",atoi(argv[2]),dregs.val&0xff);
}
CloseHandle(hDIO);
return TRUE;
}



