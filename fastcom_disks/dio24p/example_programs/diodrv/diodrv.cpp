// diodrv.cpp : Defines the entry point for the DLL application.
//
//simple dll that exports the functions necessary to interface to the DIO24 from
//anything (ie VB)
//functions are :
//
// CreateDio()			-- opens dio device (activates driver)
// CloseDio()			-- closes dio device 
// ReadDioReg(reg)		-- gets the contents of an i/o address (presumably from the DIO24H registers)
// WriteDioReg(reg,val)	-- writes to an i/o address (again presumably to the DIO24H registers) 
//
#include "stdafx.h"
#include "stdlib.h"
#include "string.h"
#include "stdio.h"
#include "diodrv.h"
#include "dio24.h"
#define MAXBOARDS 10

//globals
ULONG refcount = 0;
HANDLE hDIO[MAXBOARDS];

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
	int i;
	for(i=0;i<MAXBOARDS;i++) hDIO[i]=NULL;

    switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
			break;
		case DLL_THREAD_ATTACH:
			refcount++;
			break;
		case DLL_THREAD_DETACH:
			refcount--;
			if((refcount==0)&&(hDIO!=NULL))
				{	
				for(i=0;i<MAXBOARDS;i++)
					{
					if(hDIO[i]!=NULL) CloseHandle(hDIO[i]);
					}
				}
			break;
		case DLL_PROCESS_DETACH:
			break;
    }
    return TRUE;
}



DIODRV_API DWORD __stdcall CreateDio(DWORD boardnum)
{
if(hDIO[boardnum]==NULL)
{
	char devname[256];
	sprintf(devname,"\\\\.\\DIO24P%d",boardnum);
//only create it if we haven't previously

    hDIO[boardnum] = CreateFile(devname, GENERIC_READ|GENERIC_WRITE,FILE_SHARE_READ|FILE_SHARE_WRITE,0,
                        OPEN_EXISTING, 0, 0);
    if ( hDIO[boardnum] == INVALID_HANDLE_VALUE )
    {
	hDIO[boardnum] = NULL;
	//	MessageBox(NULL,"Cannot Open DIO24H","ERROR",MB_OK);
    return (-1);//failed return value
	}
}
return 0;//success return value
}

DIODRV_API DWORD __stdcall ReadDioReg(DWORD boardnum,DWORD port)
{
DIOREG dregs;
DWORD cbBytesReturned;
DWORD pv;
dregs.reg = port;
dregs.val = 0;
pv = 0;
DeviceIoControl(hDIO[boardnum], IOCTL_READ_REGISTER,&dregs, sizeof(DIOREG),&pv,sizeof(DWORD),&cbBytesReturned, NULL);
return pv;

}

DIODRV_API void __stdcall WriteDioReg(DWORD boardnum,DWORD port, DWORD value)
{
DIOREG dregs;
DWORD cbBytesReturned;
dregs.reg = port;
dregs.val = (char)value;
DeviceIoControl(hDIO[boardnum], IOCTL_WRITE_REGISTER,&dregs, sizeof(DIOREG),(LPVOID)NULL,0,&cbBytesReturned, NULL);
}

DIODRV_API void __stdcall CloseDio(DWORD boardnum)
{
if(hDIO[boardnum]!=NULL)
	{
	CloseHandle(hDIO[boardnum]);
	hDIO[boardnum] = NULL;
	}
}

