
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the DIODRV_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// DIODRV_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef DIODRV_EXPORTS
#define DIODRV_API __declspec(dllexport)
#else
#define DIODRV_API __declspec(dllimport)
#endif

DIODRV_API DWORD __stdcall CreateDio(DWORD boardnum);
DIODRV_API DWORD __stdcall ReadDioReg(DWORD boardnum,DWORD port);
DIODRV_API void __stdcall WriteDioReg(DWORD boardnum,DWORD port, DWORD value);
DIODRV_API void __stdcall CloseDio(DWORD boardnum);
