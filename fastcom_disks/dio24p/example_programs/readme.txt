These example programs show how to use the card in Windows NT, 2000, & XP.  Examples for other operating systems are in their respective directories.

There is a diotest written in C and a diotest written in VB.  Both have the same functionality and both will work in NT, 2000, & XP

The C version uses DeviceIoControls directly and the VB program uses a DLL to call the appropriate IO functions.

We have supplied you with the source to the DLL as well, in the diodrv directory.
