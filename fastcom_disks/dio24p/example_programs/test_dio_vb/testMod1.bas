Attribute VB_Name = "Module1"
Public Const REG_A =  0
Public Const REG_B =  1
Public Const REG_C =  2
Public Const REG_CTRL = 3

Public Declare Function CreateDio Lib "diodrv.dll" Alias "#2" (ByVal boardnum As Long) As Long
Public Declare Function ReadDioReg Lib "diodrv.dll" Alias "#3" (ByVal boardnum As Long,ByVal port As Long) As Long
Public Declare Function WriteDioReg Lib "diodrv.dll" Alias "#4" (ByVal boardnum As Long,ByVal port As Long, ByVal value As Long) As Long
Public Declare Function CloseDio Lib "diodrv.dll" Alias "#1" (ByVal boardnum As Long) As Long

