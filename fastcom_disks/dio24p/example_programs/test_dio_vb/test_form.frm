VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "DIO24H Test Program"
   ClientHeight    =   3084
   ClientLeft      =   2952
   ClientTop       =   2712
   ClientWidth     =   6024
   LinkTopic       =   "Form1"
   ScaleHeight     =   3084
   ScaleWidth      =   6024
   Begin VB.TextBox board 
      Height          =   288
      Left            =   5280
      TabIndex        =   35
      Text            =   "Text1"
      Top             =   2400
      Width           =   612
   End
   Begin VB.CommandButton SETALL 
      Caption         =   "Set All"
      Height          =   372
      Left            =   3720
      TabIndex        =   34
      Top             =   2640
      Width           =   732
   End
   Begin VB.CommandButton Clear 
      Caption         =   "Clear"
      Height          =   372
      Left            =   3720
      TabIndex        =   33
      Top             =   2160
      Width           =   732
   End
   Begin VB.CommandButton STOP 
      Caption         =   "Stop"
      Height          =   252
      Left            =   960
      TabIndex        =   31
      Top             =   2520
      Width           =   612
   End
   Begin VB.CheckBox PC 
      Caption         =   "PC7"
      Height          =   192
      Index           =   7
      Left            =   2280
      TabIndex        =   30
      Top             =   1920
      Width           =   612
   End
   Begin VB.CheckBox PC 
      Caption         =   "PC6"
      Height          =   192
      Index           =   6
      Left            =   2280
      TabIndex        =   29
      Top             =   1680
      Width           =   612
   End
   Begin VB.CheckBox PC 
      Caption         =   "PC5"
      Height          =   192
      Index           =   5
      Left            =   2280
      TabIndex        =   28
      Top             =   1440
      Width           =   612
   End
   Begin VB.CheckBox PC 
      Caption         =   "PC4"
      Height          =   192
      Index           =   4
      Left            =   2280
      TabIndex        =   27
      Top             =   1200
      Width           =   612
   End
   Begin VB.CheckBox PC 
      Caption         =   "PC3"
      Height          =   192
      Index           =   3
      Left            =   2280
      TabIndex        =   26
      Top             =   960
      Width           =   612
   End
   Begin VB.CheckBox PC 
      Caption         =   "PC2"
      Height          =   192
      Index           =   2
      Left            =   2280
      TabIndex        =   25
      Top             =   720
      Width           =   612
   End
   Begin VB.CheckBox PC 
      Caption         =   "PC1"
      Height          =   192
      Index           =   1
      Left            =   2280
      TabIndex        =   24
      Top             =   480
      Width           =   612
   End
   Begin VB.CheckBox PC 
      Caption         =   "PC0"
      Height          =   192
      Index           =   0
      Left            =   2280
      TabIndex        =   23
      Top             =   240
      Width           =   612
   End
   Begin VB.CheckBox PB 
      Caption         =   "PB7"
      Height          =   192
      Index           =   7
      Left            =   1320
      TabIndex        =   22
      Top             =   1920
      Width           =   612
   End
   Begin VB.CheckBox PB 
      Caption         =   "PB6"
      Height          =   192
      Index           =   6
      Left            =   1320
      TabIndex        =   21
      Top             =   1680
      Width           =   612
   End
   Begin VB.CheckBox PB 
      Caption         =   "PB5"
      Height          =   192
      Index           =   5
      Left            =   1320
      TabIndex        =   20
      Top             =   1440
      Width           =   612
   End
   Begin VB.CheckBox PB 
      Caption         =   "PB4"
      Height          =   192
      Index           =   4
      Left            =   1320
      TabIndex        =   19
      Top             =   1200
      Width           =   612
   End
   Begin VB.CheckBox PB 
      Caption         =   "PB3"
      Height          =   192
      Index           =   3
      Left            =   1320
      TabIndex        =   18
      Top             =   960
      Width           =   612
   End
   Begin VB.CheckBox PB 
      Caption         =   "PB2"
      Height          =   192
      Index           =   2
      Left            =   1320
      TabIndex        =   17
      Top             =   720
      Width           =   612
   End
   Begin VB.CheckBox PB 
      Caption         =   "PB1"
      Height          =   192
      Index           =   1
      Left            =   1320
      TabIndex        =   16
      Top             =   480
      Width           =   612
   End
   Begin VB.CheckBox PB 
      Caption         =   "PB0"
      Height          =   192
      Index           =   0
      Left            =   1320
      TabIndex        =   15
      Top             =   240
      Width           =   612
   End
   Begin VB.CheckBox PA 
      Caption         =   "PA0"
      Height          =   192
      Index           =   0
      Left            =   360
      TabIndex        =   14
      Top             =   240
      Width           =   612
   End
   Begin VB.CheckBox PA 
      Caption         =   "PA7"
      Height          =   192
      Index           =   7
      Left            =   360
      TabIndex        =   13
      Top             =   1920
      Width           =   612
   End
   Begin VB.CheckBox PA 
      Caption         =   "PA6"
      Height          =   192
      Index           =   6
      Left            =   360
      TabIndex        =   12
      Top             =   1680
      Width           =   612
   End
   Begin VB.CheckBox PA 
      Caption         =   "PA5"
      Height          =   192
      Index           =   5
      Left            =   360
      TabIndex        =   11
      Top             =   1440
      Width           =   612
   End
   Begin VB.CheckBox PA 
      Caption         =   "PA4"
      Height          =   192
      Index           =   4
      Left            =   360
      TabIndex        =   10
      Top             =   1200
      Width           =   612
   End
   Begin VB.CheckBox PA 
      Caption         =   "PA3"
      Height          =   192
      Index           =   3
      Left            =   360
      TabIndex        =   9
      Top             =   960
      Width           =   612
   End
   Begin VB.CheckBox PA 
      Caption         =   "PA2"
      Height          =   192
      Index           =   2
      Left            =   360
      TabIndex        =   8
      Top             =   720
      Width           =   612
   End
   Begin VB.CheckBox PA 
      Caption         =   "PA1"
      Height          =   192
      Index           =   1
      Left            =   360
      TabIndex        =   7
      Top             =   480
      Width           =   612
   End
   Begin VB.CommandButton Start 
      Caption         =   "Start"
      Height          =   252
      Left            =   240
      TabIndex        =   6
      Top             =   2520
      Width           =   612
   End
   Begin VB.CommandButton EXIT 
      Caption         =   "Exit"
      Height          =   372
      Left            =   4800
      TabIndex        =   5
      Top             =   1680
      Width           =   852
   End
   Begin VB.CheckBox PCLDIR 
      Caption         =   "Port C Low Direction"
      Height          =   192
      Left            =   3720
      TabIndex        =   4
      Top             =   1080
      Width           =   1812
   End
   Begin VB.CheckBox PCHDIR 
      Caption         =   "Port C High Direction"
      Height          =   192
      Left            =   3720
      TabIndex        =   3
      Top             =   840
      Width           =   1812
   End
   Begin VB.CheckBox PBDIR 
      Caption         =   "Port B Direction"
      Height          =   192
      Left            =   3720
      TabIndex        =   2
      Top             =   600
      Width           =   1812
   End
   Begin VB.CheckBox PADIR 
      Caption         =   "Port A Direction"
      Height          =   192
      Left            =   3720
      TabIndex        =   1
      Top             =   360
      Width           =   1812
   End
   Begin VB.CommandButton Refresh 
      Caption         =   "Refresh"
      Height          =   372
      Left            =   3720
      TabIndex        =   0
      Top             =   1680
      Width           =   732
   End
   Begin VB.Label Label1 
      Caption         =   "Board"
      Height          =   252
      Left            =   4680
      TabIndex        =   36
      Top             =   2520
      Width           =   492
   End
   Begin VB.Label DIOStatus 
      Caption         =   "DIO Inactive"
      Height          =   252
      Left            =   1800
      TabIndex        =   32
      Top             =   2520
      Width           =   972
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim hDIO As Long
Dim ctrl_val As Long
Dim boardnumber As Long

Private Sub Clear_Click()
Dim ret As Long
ret = WriteDioReg(boardnumber, REG_A, 0)
ret = WriteDioReg(boardnumber, REG_B, 0)
ret = WriteDioReg(boardnumber, REG_C, 0)
End Sub

Private Sub EXIT_Click()
Dim ret As Long
ret = CloseDio(boardnumber)

Unload Me

End Sub

Private Sub Form_Load()
boardnumber = 0
board.Text = "0"
End Sub

Private Sub PA_Click(Index As Integer)
Dim a As Long
Dim mask As Long
Dim ret As Long

a = ReadDioReg(boardnumber, REG_A)
mask = &HFF
mask = mask - (2 ^ Index)
If PA(Index).value = 1 Then
    a = (a And mask) Or (2 ^ Index)
Else
    a = a And mask
End If
ret = WriteDioReg(boardnumber, REG_A, a)
End Sub

Private Sub PADIR_Click()
Dim ret As Long

If PADIR.value = vbChecked Then
    PADIR.Caption = "Port A Output"
    'set PA to output here
ctrl_val = (ctrl_val And &HEF)
ret = WriteDioReg(boardnumber, REG_CTRL, ctrl_val)
Else
    PADIR.Caption = "Port A Input"
    'Set PA to input here
ctrl_val = (ctrl_val And &HEF) Or &H10
ret = WriteDioReg(boardnumber, REG_CTRL, ctrl_val)
End If

End Sub

Private Sub PB_Click(Index As Integer)
Dim b As Long
Dim ret As Long
Dim mask As Long

b = ReadDioReg(boardnumber, REG_B)
mask = &HFF
mask = mask - (2 ^ Index)
If PB(Index).value = 1 Then
    b = (b And mask) Or (2 ^ Index)
Else
    b = b And mask
End If
ret = WriteDioReg(boardnumber, REG_B, b)

End Sub

Private Sub PBDIR_Click()
Dim ret As Long

If PBDIR.value = vbChecked Then
    PBDIR.Caption = "Port B Output"
    'set PB to output here
ctrl_val = (ctrl_val And &HFD)
ret = WriteDioReg(boardnumber, REG_CTRL, ctrl_val)

Else
    PBDIR.Caption = "Port B Input"
    'Set PB to input here
ctrl_val = (ctrl_val And &HFD) Or &H2
ret = WriteDioReg(boardnumber, REG_CTRL, ctrl_val)
    
End If

End Sub

Private Sub PC_Click(Index As Integer)
Dim c As Long
Dim ret As Long
Dim mask As Long

c = ReadDioReg(boardnumber, REG_C)
mask = &HFF
mask = mask - (2 ^ Index)
If PC(Index).value = 1 Then
    c = (c And mask) Or (2 ^ Index)
Else
    c = c And mask
End If
ret = WriteDioReg(boardnumber, REG_C, c)


End Sub

Private Sub PCHDIR_Click()
Dim ret As Long

If PCHDIR.value = vbChecked Then
    PCHDIR.Caption = "Port C High Output"
    'set PCH to output here
ctrl_val = (ctrl_val And &HF7)
ret = WriteDioReg(boardnumber, REG_CTRL, ctrl_val)
    
Else
    PCHDIR.Caption = "Port C High Input"
    'Set PCH to input here
ctrl_val = (ctrl_val And &HF7) Or &H8
ret = WriteDioReg(boardnumber, REG_CTROL, ctrl_val)
    
End If

End Sub

Private Sub PCLDIR_Click()
Dim ret As Long

If PCLDIR.value = vbChecked Then
    PCLDIR.Caption = "Port C Low Output"
    'set PCL to output here
ctrl_val = (ctrl_val And &HFE)
ret = WriteDioReg(boardnumber, REG_CTRL, ctrl_val)
    
Else
    PCLDIR.Caption = "Port C Low Input"
    'Set PCL to input here
ctrl_val = (ctrl_val And &HFE) Or &H1
ret = WriteDioReg(boardnumber, REG_CTRL, ctrl_val)
    
End If

End Sub

Private Sub Refresh_Click()
Dim a As Long
Dim b As Long
Dim c As Long
Dim ret As Long
Dim i As Long

a = ReadDioReg(boardnumber, REG_A)
b = ReadDioReg(boardnumber, REG_B)
c = ReadDioReg(boardnumber, REG_C)
'go through each a,b and c and set the checkboxes
'for pa0(0).value etc

i = 1
j = 0
Do
    If (a And i) = i Then
        PA(j).value = 1
    Else
        PA(j).value = 0
    End If
    
    If (b And i) = i Then
        PB(j).value = 1
    Else
        PB(j).value = 0
    End If
    
    If (c And i) = i Then
        PC(j).value = 1
    Else
        PC(j).value = 0
    End If
    
    
    j = j + 1
    i = i * 2
Loop While j < 8


End Sub

Private Sub SETALL_Click()
Dim ret As Long
ret = WriteDioReg(boardnumber, REG_A, &HFF)
ret = WriteDioReg(boardnumber, REG_B, &HFF)
ret = WriteDioReg(boardnumber, REG_C, &HFF)

End Sub

Private Sub Start_Click()
boardnumber = board.Text
MsgBox ("board: " + Str(boardnumber))
If (CreateDio(boardnumber) = -1) Then
MsgBox ("cannot open DIO24")
Else
DIOStatus.Caption = "DIO Started"
End If
End Sub

Private Sub STOP_Click()
Dim ret As Long
ret = CloseDio(boardnumber)
DIOStatus.Caption = "DIO Stopped"
End Sub
