//dio24.h Copyright(C)1999 Commtech, Inc.
//
//

#define IOCTL_READ_REGISTER 0x830020c0
#define IOCTL_WRITE_REGISTER 0x830020c4


typedef struct dioregs{
unsigned reg;//register (0-3) (a,b,c,ctrl)
char val;//value to write to register
} DIOREG;


#define REG_A		0
#define REG_B		1
#define REG_C		2
#define REG_CTRL	3

