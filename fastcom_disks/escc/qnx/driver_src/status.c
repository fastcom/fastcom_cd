#include "esccdrv.h"

void our_status(pid_t pid, io_msg *msg, Escc_Dev *dev)
{
unsigned long flags;
unsigned long status;
struct _mxfer_entry mx[2];

//do status here
			
if(dev->port_initialized)
	 {
	status = 0;
	flags = pswget();
	disable();
	status = dev->status;//get status from driver info area
	dev->status = 0;//clear it
	restore(flags);//enable interrupts

	if(status==0)
		{
		if(dev->nonblocking==1)
			{
			//if nonblocking==1 then we return if there are no frames to be had
#ifdef ENABLE_DEBUG_MESSAGES
			DBGMSG("Status would block");
#endif
			msg->qioctl_reply.status = EAGAIN;
			msg->qioctl_reply.ret_val = 0;
			Reply(pid,msg,sizeof(msg->qioctl_reply));
			}
		else
			{
			//nonblocking =0 so block (actually just setup the info we need 
			//the next time round after the isr gets a status to return,
			//such that the proxy can setup the info and we can finish the call)
			dev->s_pid = pid;
			dev->status_pending=1;
			}
		}//end of while not pending status
	else
		{
		//nonzero status so return it
		msg->qioctl_reply.status = EOK;
		msg->qioctl_reply.ret_val =  sizeof(status);
		_setmx(mx+0,msg,sizeof(msg->qioctl_reply)-1);
		_setmx(mx+1,&status,sizeof(status));
		Replymx(pid,2,mx);
	#ifdef ENABLE_DEBUG_MESSAGES
		sprintf(errbuf,"STATUS:%lx",status);
		DBGMSG(errbuf);
	#endif
		}
	}//end if initialized
else
	{
#ifdef ENABLE_DEBUG_MESSAGES
	DBGMSG("Status with port not initialized");
#endif
	msg->qioctl_reply.status = EBUSY;
	msg->qioctl_reply.ret_val =  0;
	Reply(pid,msg,sizeof(msg->qioctl_reply));
	}
return;
}