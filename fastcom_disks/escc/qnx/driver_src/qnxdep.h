#include <sys/fd.h>
#include <sys/prfx.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <sys/io_msg.h>
#include <sys/sys_msg.h>
#include <sys/fsys_msg.h>
#include <errno.h>
#include <sys/kernel.h>
#include <sys/sendmx.h>
#include <string.h>
#include <semaphore.h>
#include <malloc.h>
#include <sys/psinfo.h>
#include <sys/irqinfo.h>
#include <sys/inline.h>

void serviceRequests();
int init_module(void);
void cleanup_module(void);


typedef union {
	msg_t                               type;
	struct _io_open                     open;
	struct _io_close                    close;
	struct _io_read                     read;
	struct _io_write                    write;
	struct _io_lseek                    lseek;
	struct _io_config                   config;
	struct _io_dup                      dup;
	struct _io_fstat                    fstat;
	struct _io_chmod                    chmod;
	struct _io_chown                    chown;
	struct _io_utime                    utime;
	struct _io_flags                    flags;
	struct _io_lock                     lock;
	struct _io_readdir                  readdir;
	struct _io_rewinddir                rewinddir;
	struct _io_ioctl                    ioctl;
	struct _io_qioctl                   qioctl;
	struct _io_select                   select;
	struct _select_set				    select_set;
	struct _io_open_reply				open_reply;
	struct _io_close_reply              close_reply;
	struct _io_read_reply               read_reply;
	struct _io_write_reply              write_reply;
	struct _io_lseek_reply              lseek_reply;
	struct _io_config_reply             config_reply;
	struct _io_dup_reply                dup_reply;
	struct _io_fstat_reply              fstat_reply;
	struct _io_chmod_reply              chmod_reply;
	struct _io_chown_reply              chown_reply;
	struct _io_utime_reply              utime_reply;
	struct _io_flags_reply              flags_reply;
	struct _io_readdir_reply            readdir_reply;
	struct _io_rewinddir_reply          rewinddir_reply;
	struct _io_ioctl_reply              ioctl_reply;
	struct _io_qioctl_reply             qioctl_reply;
	struct _io_select_reply             select_reply;
} io_msg;
