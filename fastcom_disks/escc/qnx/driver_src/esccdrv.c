/******************************************************
 *
 * Copyright (C) 2000 Commtech, Inc. Wichita KS
 *
 * esccdrv.c -- init stuff for escc-isa module
 *
 * Tested with Linux version 2.2 12
 * hacked to qnx 4.25 12/8/00
 ******************************************************/

/* $Id$ */

#include "esccdrv.h"        /* local definitions */
//#include "fcntl.h"
volatile unsigned long ihit=0;

//these are globals for the driver
int escc_nr_devs = 2;				//number of escc ports to handle 
Escc_Dev *escc_devices;				// allocated in init_module, holds all board/channel pertinent info
sem_t *boardlock;					//keeps accesses to both channels of a single board in line
int *used_board_numbers;//used to determine which devices are part of the same board

void *fd_ctrl;

int devno; //our device number, allocated in init_module
pid_t isr_proxy;
char errbuf[256];


void main(int argc, char *argv[])
{
unsigned i;
if(argc>1) escc_nr_devs = atoi(argv[1]);
if(escc_nr_devs <2) escc_nr_devs = 2;

if(init_module() == -1)
	{
	DBGMSG("initialization failed");
	exit(EXIT_FAILURE);
	}

sprintf(errbuf,"ESCCDRV started, supporting %u ports\n",escc_nr_devs);
DBGMSG(errbuf);

serviceRequests();
}


int init_module(void)
{
    int result, i;

//it would appear that this is the equivalent of the acquisition of a 
//UNIX Major #, and that the next available(dynamic) is the default
//linux equiv: result = register_chrdev(escc_major, "escc", &escc_fops);
if((devno = qnx_device_attach()) == -1)
	{
	DBGMSG("ERROR cannot qnx_device_attach");
	perror(NULL);
	exit(EXIT_FAILURE);
	}
//this is the rough equivalent of the mknod /dev/escc only there is no mknod in qnx
//so this is the way that we tell the os to redirect system messages to our driver
//any operation that acts on "/escc/xx..." will end up in our message handler
if(qnx_prefix_attach("/escc",NULL,devno)==-1)
	{
	DBGMSG("ERROR cannot qnx_prefix_attach");
	perror(NULL);
	exit(EXIT_FAILURE);
	}
if( (fd_ctrl = __init_fd(getpid())) == NULL)
	{
	DBGMSG("ERROR cannot __init_fd");
	perror(NULL);
	exit(EXIT_FAILURE);
	}
isr_proxy = qnx_proxy_attach(0,NULL,0,-1);
#ifdef ENABLE_DEBUG_MESSAGES
sprintf(errbuf,"isrproxy pid:%u",isr_proxy);
DBGMSG(errbuf);
#endif
//allocate memory for our per instance data (per port values/settings etc)
escc_devices = malloc(escc_nr_devs * sizeof (Escc_Dev));
if (!escc_devices) 
	{
	DBGMSG("ERROR mem allocation (Escc_Dev)");
    result = -ENOMEM;
    goto fail_malloc;
	}
//zero it and init values that need initing
memset((char*)escc_devices, 0, escc_nr_devs * sizeof (Escc_Dev));
for (i=0; i < escc_nr_devs; i++) 
	{
	escc_devices[i].is_transmitting = 0;
	escc_devices[i].tx_type = XTF;
	
	//set per instance data here
	}
//allocate space for semaphores to prevent access across ports on the same board
//at the same time...registered port select necessitates this
boardlock = malloc(escc_nr_devs * sizeof (sem_t));
if (!boardlock) 
	{
	DBGMSG("ERROR mem allocation (boardlock)");
    result = -ENOMEM;
    goto fail_malloc1;
	}
for(i=0;i<escc_nr_devs;i++)
	{
	sem_init(&boardlock[i],0,1);//init process specific (this one only)
	}
used_board_numbers = malloc(escc_nr_devs);//actually will only use escc_nr_devs/2 
if(!used_board_numbers)
	{
	DBGMSG("ERROR mem alloc (used_board_numbers)");
    result = -ENOMEM;
    goto fail_malloc2;
	}
for(i=0;i<escc_nr_devs;i++)used_board_numbers[i] = 0;

return 0; /* succeed */
fail_malloc2:
	free(boardlock);
fail_malloc1:
	free((void*)escc_devices);
fail_malloc: 
    return -1;
}

void cleanup_module(void)
{
Escc_Dev *dev;
int i;    
for(i=0;i<escc_nr_devs;i++)
	{
	dev = &escc_devices[i];
	//free allocated memory (if any)
	if((dev->escc_rbuf!=NULL)&&(dev->escc_rbuf[0].frame !=NULL)) free(dev->escc_rbuf[0].frame);
	if((dev->escc_tbuf!=NULL)&&(dev->escc_tbuf[0].frame !=NULL)) free(dev->escc_tbuf[0].frame);
	if(dev->escc_rbuf!=NULL) free(dev->escc_rbuf);
	if(dev->escc_tbuf!=NULL) free(dev->escc_tbuf);
	sem_destroy(&boardlock[i]);//if fails with EBUSY then there is someone blocked on this sem
	}
//any cleanup then
free((void*)escc_devices);
free(boardlock);

}


void serviceRequests()
{
pid_t pid;
int replyParms;
io_msg msg;
io_msg *pmsg;
int pmsg_allocated_flag;
//struct _mxfer_entry mx[2];
int i;
int j;
Escc_Dev *dev; /* device information */
struct _psinfo3 info;

unsigned long timeoutvalue;
unsigned long flags;
unsigned chanstor;
unsigned port;
board_settings board_switches;
regsingle regs;
clkset clock;
unsigned long passval;
char *tempbuf;
int nfds;

while(1)
	{
	pid = Receive(0,&msg,sizeof(msg));

#ifdef ENABLE_DEBUG_MESSAGES
	sprintf(errbuf,"Received a message of type 0x%04x ,pid:%u ,ihit:%lu\n",msg.type,pid,ihit);
	DBGMSG(errbuf);
#endif

if(pid==isr_proxy)
{
//scan all devices for interrupt indication flags
for(i=0;i<escc_nr_devs;i++)
	{
	if(escc_devices[i].rq == 1)
		{
#ifdef ENABLE_DEBUG_MESSAGES
		sprintf(errbuf,"read proxy device:%d\n",i);
		DBGMSG(errbuf);
#endif
		dev = &escc_devices[i];
		dev->rq = 0;
		if(dev->read_pending==1)
			{
#ifdef ENABLE_DEBUG_MESSAGES
			sprintf(errbuf,"read pending set, %u",dev->received_frames_pending);
			DBGMSG(errbuf);
#endif		
			pid = dev->r_pid; //get our read pid 
			dev->r_pid = 0;   //clear it
			msg.read.nbytes = dev->r_size;//get the requested number of bytes
			dev->read_pending=0;//clear the pending flag
			our_read(pid,&msg,dev);//service the read 
			}

		if(dev->rselect_pending==1)
			{
			if(dev->rselect_pid_proxy != 0)
				{
				Trigger(dev->rselect_pid_proxy);
#ifdef ENABLE_DEBUG_MESSAGES
				DBGMSG("receive select proxy triggered");
#endif
				}
			dev->rselect_pid_proxy = 0;
			dev->rselect_pending = 0;
			}

		}//end .rq flag check
	if(escc_devices[i].wq == 1) 
		{
#ifdef ENABLE_DEBUG_MESSAGES
		sprintf(errbuf,"write proxy device:%d\n",i);
		DBGMSG(errbuf);
#endif
		dev = &escc_devices[i];
		dev->wq = 0;
		if(dev->write_pending==1)
			{
#ifdef ENABLE_DEBUG_MESSAGES
			sprintf(errbuf,"write pending set, %u",dev->transmit_frames_available);
			DBGMSG(errbuf);
#endif
			pid = dev->w_pid;//get the pid of the write call
			dev->w_pid = 0;//clear it for next time
			msg.write.nbytes = dev->w_size;//get the requested #bytes
			dev->write_pending=0;//clear the pending flag
			our_write(pid,&msg,dev);//service the write
			}
//trigger for select insert here
		if(dev->tselect_pending==1)
			{
			if(dev->tselect_pid_proxy != 0)
				{
				Trigger(dev->tselect_pid_proxy);
#ifdef ENABLE_DEBUG_MESSAGES
				DBGMSG("transmit select proxy triggered");
#endif
				}
			dev->tselect_pid_proxy = 0;
			dev->tselect_pending = 0;
			}

		}//end .wq flag check
	if(escc_devices[i].sq == 1)
		{
#ifdef ENABLE_DEBUG_MESSAGES
		sprintf(errbuf,"status proxy device:%d\n",i);
		DBGMSG(errbuf);
#endif
		dev = &escc_devices[i];

		dev->sq =0;
		if(dev->status_pending==1)
			{
			dev->status_pending=0;
			pid = dev->s_pid;//get the pid of the status call
			dev->s_pid = 0;//clear it for next time
			//insert status return code here
			our_status(pid,&msg,dev);
			}
//trigger for select insert here
		if(dev->sselect_pending==1)
			{
			if(dev->sselect_pid_proxy != 0)
				{
				Trigger(dev->sselect_pid_proxy);
#ifdef ENABLE_DEBUG_MESSAGES
				DBGMSG("status select proxy triggered");
#endif
				}
			dev->sselect_pid_proxy = 0;
			dev->sselect_pending = 0;
			}

		}//end .sq flag check
	}//end for(i=0;i<escc_nr_devs;i++)
}//end isr proxy code
else
{//is a "normal" (not isr proxy) message
	switch(msg.type)
	{

	case _IO_OPEN:
#ifdef ENABLE_DEBUG_MESSAGES
		DBGMSG("OPEN");
#endif
		i = atoi(msg.open.path);//path is assumed to be a number (ie /escc/0, or /escc/1)
sprintf(errbuf,"esccdrv-port:%d\n",i);
DBGMSG(errbuf);
		if(i>escc_nr_devs) //cannot open a port greater than we have, let them know
			{
#ifdef ENABLE_DEBUG_MESSAGES
			DBGMSG("port out of range");
#endif
			msg.open_reply.status =  ENOSYS;
			Reply(pid,&msg,sizeof(msg.open_reply));
			break;
			}
		if(i==0)
			{
			if(msg.open.path[0]!='0')//atoi defaults to 0, so verify that the path is indeed '/escc/0' if not give them an error
				{
#ifdef ENABLE_DEBUG_MESSAGES
				DBGMSG("invalid port/path");
#endif
				msg.open_reply.status =  ENOENT;
				Reply(pid,&msg,sizeof(msg.open_reply));
				break;
				}
			}
	    dev = &escc_devices[i];//grab pointer to our device structure
#ifdef ENABLE_DEBUG_MESSAGES
sprintf(errbuf,"index:%u\n",i);
DBGMSG(errbuf);
#endif
//get info on user to validate the call

		__get_pid_info(pid,&info,fd_ctrl);
		
//if the user is not the current owner then fail the open
		if(dev->escc_u_count && (dev->escc_u_owner!=info.euid))
			{
#ifdef ENABLE_DEBUG_MESSAGES
			sprintf(errbuf,"Open request not from current owner, current:%u, request:%u\n",dev->escc_u_owner,info.euid);
			DBGMSG(errbuf);
#endif
			msg.open_reply.status = EBUSY;
			Reply(pid,&msg,sizeof(msg.open_reply));
			break;
			}
//if allready open and not the same
//user then fail with busy
		if(dev->escc_u_count == 0)
			{
			//port not open yet, so open and stash the user info for future open check above
			dev->escc_u_owner = info.euid;
			}   							 
		dev->escc_u_count++;//usage count to know when to de-allocate on close
		
		
		//associate the device index with the pid and fd using mystic qnx functions:
		if(qnx_fd_attach(pid,msg.open.fd,0,0,0,0,i) == -1)
			{
#ifdef ENABLE_DEBUG_MESSAGES
			sprintf(errbuf,"ERROR cannot fd_attach, pid:%u,fd:%u,index:%u",pid,msg.open.fd,i);
			DBGMSG(errbuf);
#endif
			perror(NULL);
			msg.open_reply.status = EBADF;
			Reply(pid,&msg,sizeof(msg.open_reply));
			break;
			}

//the logic and compare value on this could be fubar
//the idea is to generate a flag such that we know if the device was opened
//as a blocking or nonblocking manner. (we will treat reads/writes and someday ioctl status functions 
//differently in blocking/nonblocking modes.)
if((msg.open.oflag&0x80)==0x80)
{
	dev->nonblocking = 1;
#ifdef ENABLE_DEBUG_MESSAGES
	DBGMSG("NONBLOCK=1");
#endif
}
else 
{
	dev->nonblocking = 0;
#ifdef ENABLE_DEBUG_MESSAGES
DBGMSG("NONBLOCK=0");
#endif
}
//device is now open, so indicate it.  Note that nothing usefull can take place until
//the add board and initialize ioctl functions have been executed
		msg.open_reply.status = EOK;
		Reply(pid,&msg,sizeof(msg.open_reply));
		break;
	case _IO_READ:
#ifdef ENABLE_DEBUG_MESSAGES
		sprintf(errbuf,"READ %u bytes requested",msg.read.nbytes);
		DBGMSG(errbuf);
#endif
		if((i = __get_fd(pid,msg.read.fd,fd_ctrl))==-1)
			{
#ifdef ENABLE_DEBUG_MESSAGES
			sprintf(errbuf,"ERROR cannot __get_fd, pid:%u fd:%u",pid,msg.read.fd);
			DBGMSG(errbuf);
#endif
			msg.read_reply.status = EBADF;
			msg.read_reply.nbytes = 0;
			Reply(pid,&msg,sizeof(msg.read_reply));
			break;
			}
			dev = &escc_devices[i];
			our_read(pid,&msg,dev);
		break;
	case _IO_WRITE:
#ifdef ENABLE_DEBUG_MESSAGES
		sprintf(errbuf,"WRITE %u bytes requested",msg.write.nbytes);
		DBGMSG(errbuf);
#endif
		if((i = __get_fd(pid,msg.write.fd,fd_ctrl))==-1)
			{
#ifdef ENABLE_DEBUG_MESSAGES
			sprintf(errbuf,"ERROR cannot __get_fd, pid:%u fd:%u",pid,msg.write.fd);
			DBGMSG(errbuf);
#endif
			msg.write_reply.status = EBADF;
			msg.write_reply.nbytes = 0;
			Reply(pid,&msg,sizeof(msg.write_reply));
			break;
			}
			dev = &escc_devices[i];
			our_write(pid,&msg,dev);
		break;
	case _IO_QIOCTL:
#ifdef ENABLE_DEBUG_MESSAGES
		sprintf(errbuf,"QIOCTL REQUEST#:%x , size:%u",msg.qioctl.request,msg.qioctl.nbytes);
		DBGMSG(errbuf);
#endif
		if((i = __get_fd(pid,msg.qioctl.fd,fd_ctrl))==-1)
			{
#ifdef ENABLE_DEBUG_MESSAGES
			sprintf(errbuf,"ERROR cannot __get_fd, pid:%u fd:%u",pid,msg.qioctl.fd);
			DBGMSG(errbuf);
#endif
			msg.qioctl_reply.status = EBADF;
			Reply(pid,&msg,sizeof(msg.qioctl_reply));
			break;
			}
			dev = &escc_devices[i];
		if(msg.qioctl.request > ESCC_IOC_MAXNR)
			{
#ifdef ENABLE_DEBUG_MESSAGES
			sprintf(errbuf,"ERROR ioctl request out of range >(%u)",ESCC_IOC_MAXNR);
			DBGMSG(errbuf);
#endif
			msg.qioctl_reply.status = EINVAL;
			Reply(pid,&msg,sizeof(msg.qioctl_reply));
			break;
			}
		our_ioctl(pid,&msg,dev);
//		msg.qioctl_reply.status = EOK;
//		Reply(pid,&msg,sizeof(msg.qioctl_reply));
		break;
	case _IO_IOCTL:
#ifdef ENABLE_DEBUG_MESSAGES
		DBGMSG("IOCTL");
#endif
		if((i = __get_fd(pid,msg.ioctl.fd,fd_ctrl))==-1)
			{
			sprintf(errbuf,"ERROR cannot __get_fd, pid:%u fd:%u",pid,msg.ioctl.fd);
			DBGMSG(errbuf);
			msg.ioctl_reply.status = EBADF;
			Reply(pid,&msg,sizeof(msg.ioctl_reply));
			break;
			}
			dev = &escc_devices[i];

		msg.ioctl_reply.status = ENOSYS;//don't allow ioctl messages, use qioctl instead
		Reply(pid,&msg,sizeof(msg.ioctl_reply));
		break;
	case _IO_CLOSE:
#ifdef ENABLE_DEBUG_MESSAGES
		DBGMSG("CLOSE");
#endif
		if((i = __get_fd(pid,msg.close.fd,fd_ctrl))==-1)
			{
#ifdef ENABLE_DEBUG_MESSAGES
			sprintf(errbuf,"ERROR cannot __get_fd, pid:%u fd:%u",pid,msg.close.fd);
			DBGMSG(errbuf);
#endif
			msg.close_reply.status = EBADF;
			Reply(pid,&msg,sizeof(msg.close_reply));
			break;
			}
			dev = &escc_devices[i];

		dev->escc_u_count--;//decrement usage count
#ifdef ENABLE_DEBUG_MESSAGES
		sprintf(errbuf,"usage count:%u",dev->escc_u_count);
		DBGMSG(errbuf);
#endif
		if(dev->escc_u_count==0)
			{
			//this was the last open handle being closed so we are finished with
			//the device for now
#ifdef ENABLE_DEBUG_MESSAGES
			DBGMSG("usage count==0");
#endif
			//unhook/free anything here, and release device to next user
			dev->escc_u_owner = 0; //no owner

			if(dev->irq_hooked==1)
				{
#ifdef ENABLE_DEBUG_MESSAGES
				DBGMSG("detach irq");
#endif
				qnx_hint_detach(dev->intID);
				dev->irq_hooked=0;
				}
#ifdef ENABLE_DEBUG_MESSAGES
			DBGMSG("unbind fd_attach index");
#endif
			qnx_fd_attach(pid,msg.close.fd,0,0,0,0,0);//unbind fd/pid/index association
			dev->w_pid = 0;
			dev->r_pid = 0;
			dev->s_pid = 0;
			dev->write_pending =0;
			dev->read_pending = 0;
			dev->status_pending =0;
			dev->rq =0;
			dev->wq =0;
			dev->sq =0;
			dev->base = 0;
			dev->irq = 0;
			dev->rpf = 0;
			dev->xpr = 0;
			dev->rme = 0;
			dev->port_initialized =0;
			flushall();//this forces output to log files (via the DBGMSG's) to get written to disk on the close
			//could be a good idea to free all of the allocated buffers (rbufs and tbufs)
			//that were allocated in the initialize call here.
			//they will be de-allocated/re-allocated on the next init, but if
			//you kill the driver then it could be a mem leak.
#ifdef ENABLE_DEBUG_MESSAGES
			DBGMSG("free buffers");
#endif
			//free buffers if they allready exist
			if((dev->escc_rbuf!=NULL)&&(dev->escc_rbuf[0].frame !=NULL)) free(dev->escc_rbuf[0].frame);
			if((dev->escc_tbuf!=NULL)&&(dev->escc_tbuf[0].frame !=NULL)) free(dev->escc_tbuf[0].frame);
			if(dev->escc_rbuf!=NULL) free(dev->escc_rbuf);
			if(dev->escc_tbuf!=NULL) free(dev->escc_tbuf);
			dev->escc_rbuf=NULL;
			dev->escc_tbuf=NULL;
			}	
		msg.close_reply.status = EOK;
		Reply(pid,&msg,sizeof(msg.close_reply));
		break;
	case _IO_SELECT:
#ifdef ENABLE_DEBUG_MESSAGES
		DBGMSG("SELECT");
#endif

//printf("proxy:%u\n",msg.select.proxy);//proxy to trigger to finsih?
//printf("pid:%u\n",msg.select.pid);
//printf("number of fds:%u\n",msg.select.nfds);//should be width?
//presumably we can get to the file descriptor and flags for the selct through
//msg.io_select.set[0].flag
//msg.io_select.set[0].fd
//note that if the number of fd's is large, the size of our 
//received message will not hold them all, and we might have to 
//re-read the message to get all of them.
//this will happen if sizeof(msg.select)+msg.select.nfds*sizeof(struct _select_set) > sizeof(msg)
		if(sizeof(msg.select)+ (msg.select.nfds*sizeof(struct _select_set)) > sizeof(msg))
			{
			//allocate a message large enough to take it
			//then re-read message into new message buffer
			//for now just print an error message

			printf("message not big enough in select\n");

			pmsg = malloc( sizeof(msg.select)+ (msg.select.nfds*sizeof(struct _select_set)) );
			if(pmsg==NULL)
				{
				msg.select_reply.status = ENOMEM;
				Reply(pid,&msg,sizeof(msg.select_reply));
				break;
				}
			pmsg_allocated_flag = 1;

			if(Readmsg(pid,0,pmsg, sizeof(msg.select)+ (msg.select.nfds*sizeof(struct _select_set)) )==-1)
				{

				printf("error reading message in select\n");

				free(pmsg);
				pmsg_allocated_flag = 0;
				msg.select_reply.status = ENOMEM;
				Reply(pid,&msg,sizeof(msg.select_reply));
				break;
				}
			}
		else
			{
			pmsg = &msg;
			pmsg_allocated_flag = 0;
			}
//printf("1st fd:%u\n",msg.select.set[0].fd);
//printf("1st flag:%x\n",msg.select.set[0].flag);

//note that the sizeof(msg.select_reply) does not include space for the select_set array
//and you will get an endless series of _SEL_POLL messages if you don't return a valid sized 
//reply frame (been there done that).


		if(msg.select.mode == _SEL_ARM)
			{
#ifdef ENABLE_DEBUG_MESSAGES
			printf("SEL_ARM\n");
#endif
			nfds = 0;
			//printf("nfds = %u\n",msg.select.nfds);
			for(j=0;j<msg.select.nfds;j++)
				{
				if((i = __get_fd(pid,pmsg->select.set[j].fd,fd_ctrl))==-1)
					{
					//could be normal for a select call, an entry in the select list might be for another device,
					//simply skip it and go to the next fd in the list
#ifdef ENABLE_DEBUG_MESSAGES
					
					sprintf(errbuf,"ERROR cannot __get_fd, pid:%u fd:%u",pid,msg.select.set[j].fd);
					DBGMSG(errbuf);
#endif
					/*
					msg.select_reply.status = EBADF;
					Reply(pid,&msg,sizeof(msg.select_reply));
					break;
					*/
					}
				else
					{
					dev = &escc_devices[i];
					
					//check to see if we have a finished condition
					if(((pmsg->select.set[j].flag&_SEL_INPUT)==_SEL_INPUT)&&(atomic_read(dev->received_frames_pending)!=0))
						{
						//printf("finishing in input\n");
						pmsg->select.set[j].flag |= (_SEL_IS_INPUT | _SEL_POLLED);//means read would not block if called
						}
					if(((pmsg->select.set[j].flag&_SEL_OUTPUT)==_SEL_OUTPUT)&&(atomic_read(dev->transmit_frames_available)!=0))	
						{
						//printf("finishing in output\n");
						pmsg->select.set[j].flag |= (_SEL_IS_OUTPUT | _SEL_POLLED);//means write would not block if called
						}
					if(((pmsg->select.set[j].flag&_SEL_EXCEPT)==_SEL_EXCEPT)&&(dev->status != 0))	
						{
						//printf("finishing in status\n");
						pmsg->select.set[j].flag |= (_SEL_IS_EXCEPT | _SEL_POLLED);//means status would not block if called
						}

					if((pmsg->select.set[j].flag & (_SEL_IS_INPUT | _SEL_IS_OUTPUT | _SEL_IS_EXCEPT)) != 0)
						{
						//printf("finishing up\n");
						//yes we are done with the select here, so indicate it
						nfds++;//note is number of fd's that have data to finish the select call
						pmsg->select.set[j].flag &= ~_SEL_ARMED;
						//printf("flag:%x\n",pmsg->select.set[j].flag);
						dev->rselect_pid_proxy = 0;
						dev->tselect_pid_proxy = 0;
						dev->sselect_pid_proxy = 0;
						dev->select_pid = 0;
						dev->select_fd = 0;
						dev->rselect_pending = 0;
						dev->tselect_pending = 0;
						dev->sselect_pending = 0;
						}
					else
						{
						//printf("select trigger set\n");
						//nothing to return, so setup the trigger
						dev->select_pid = pid;
						dev->select_fd = msg.select.set[j].fd;
						if(((pmsg->select.set[j].flag&_SEL_INPUT)==_SEL_INPUT))
							{
							dev->rselect_pending = 1;
							dev->rselect_pid_proxy = msg.select.proxy;
							}
						if(((pmsg->select.set[j].flag&_SEL_OUTPUT)==_SEL_OUTPUT))
							{
							dev->tselect_pending = 1;
							dev->tselect_pid_proxy = msg.select.proxy;
							}
						if(((pmsg->select.set[j].flag&_SEL_EXCEPT)==_SEL_EXCEPT))
							{
							dev->sselect_pending = 1;
							dev->sselect_pid_proxy = msg.select.proxy;
							}
						pmsg->select.set[j].flag |= (_SEL_ARMED | _SEL_POLLED) ;
						}
					}
				}

			pmsg->select_reply.status = EOK;
			for(i=0;i<7;i++) pmsg->select_reply.zero[i] = 0;
			pmsg->select_reply.nfds = nfds;
			Reply(pid,pmsg,sizeof(msg.select_reply)+(j*sizeof(struct _select_set)));//j == msg.selecct.nfds here
			}
		if(msg.select.mode == _SEL_POLL)
			{
#ifdef ENABLE_DEBUG_MESSAGES
			printf("SEL_POLL\n");
#endif
			nfds = 0;
			for(j=0;j<msg.select.nfds;j++)
				{
				if((i = __get_fd(pid,pmsg->select.set[j].fd,fd_ctrl))==-1)
					{
					//again a normal condition, just skip
#ifdef ENABLE_DEBUG_MESSAGES
					sprintf(errbuf,"ERROR cannot __get_fd, pid:%u fd:%u",pid,msg.select.set[j].fd);
					DBGMSG(errbuf);
#endif
					/*
					msg.select_reply.status = EBADF;
					Reply(pid,&msg,sizeof(msg.select_reply));
					break;
					*/
					}
				else
					{
					dev = &escc_devices[i];
					

					if(((pmsg->select.set[j].flag&_SEL_INPUT)==_SEL_INPUT)&&(atomic_read(dev->received_frames_pending)!=0))		pmsg->select.set[j].flag |= _SEL_IS_INPUT | _SEL_POLLED;//means read would not block if called
					if(((pmsg->select.set[j].flag&_SEL_OUTPUT)==_SEL_OUTPUT)&&(atomic_read(dev->transmit_frames_available)!=0))	pmsg->select.set[j].flag |= _SEL_IS_OUTPUT | _SEL_POLLED;//means write would not block if called
					if(((pmsg->select.set[j].flag&_SEL_EXCEPT)==_SEL_EXCEPT)&&(dev->status != 0))									pmsg->select.set[j].flag |= _SEL_IS_EXCEPT | _SEL_POLLED;//means status would not block if called

					if(pmsg->select.set[j].flag & (_SEL_IS_INPUT | _SEL_IS_OUTPUT | _SEL_IS_EXCEPT) != 0)
						{
						nfds++;
						}

					//unset the trigger for this device
					pmsg->select.set[j].flag &= ~_SEL_ARMED;
					dev->rselect_pid_proxy = 0;
					dev->tselect_pid_proxy = 0;
					dev->sselect_pid_proxy = 0;
					dev->select_pid = 0;
					dev->select_fd = 0;
					dev->rselect_pending = 0;
					dev->tselect_pending = 0;
					dev->sselect_pending = 0;
					}
				}
			
			//this poll will finish the select call - yes
			


			pmsg->select_reply.status = EOK;
			for(i=0;i<7;i++) pmsg->select_reply.zero[i] = 0;
			pmsg->select_reply.nfds = nfds;
			Reply(pid,pmsg,sizeof(msg.select_reply)+(j*sizeof(struct _select_set)));

			}
		if(pmsg_allocated_flag == 1)
			{
			free(pmsg);
			pmsg_allocated_flag = 0;
			}


		break;
	default:
#ifdef ENABLE_DEBUG_MESSAGES
		DBGMSG("DEFAULT MSG HANDLER");
#endif
		msg.open_reply.status = ENOSYS;
		Reply(pid,&msg,sizeof(msg.open_reply));
		break;
	}//end switch message type
}//end non isr proxy (normal messages)
	}//end while(1)
}//end servicerequests()
