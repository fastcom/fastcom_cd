/******************************************************
 *
 * Copyright (C) 2000 Commtech, Inc. Wichita KS
 *
 * esccdrv.h -- header file for escc-isa module
 *
 * Tested with Linux version 2.2 12
 ******************************************************/

/* $Id */

#include "qnxdep.h"
//uncomment this for debug print messages
//#define ESCC_DEBUG

//this will make the WAIT_WITH_TIMEOUT macro actually check the status of the 82532
//undefining this will basically make WAIT_WITH_TIMEOUT do nothing (no checking of CEC before writing to CMDR)
#define WAIT_FOR_CEC

//this will turn off the receiver when a frame is transmitting effectively
//canceling the echo on a 2 wire 485 net
//#ifdef RS485_RX_ECHO_CANCEL

//this will force the RTS line on when a frame is transmitted, it should not be
//necessary as the default function of the 82532 is to frame the outgoing frame
//with RTS
//#ifdef FORCE_485_RTS_CONTROL


//#define ENABLE_DEBUG_MESSAGES

/*
 * Macros to help debugging
 */

#define MAX_TIMEOUT_VALUE 1000000  //max value for WAIT_WITH_TIMEOUT

#define TRUE 1
#define FALSE 0
typedef unsigned int atomic_t;

/*
 */
//structure for buffered frames
struct buf{
unsigned valid;         //indicator 1 = frame[] has data, 0 = frame[] has ???
unsigned no_bytes;      //number of bytes in frame[]
unsigned max;           //maximum number of bytes to send/receive
unsigned char *frame;  //data array for received/transmitted data
};


typedef struct escc_setup{
//used in all
unsigned cmdr;
unsigned mode;
unsigned timr;
unsigned xbcl;
unsigned xbch;
unsigned ccr0;
unsigned ccr1;
unsigned ccr2;
unsigned ccr3;
unsigned ccr4;
unsigned tsax;
unsigned tsar;
unsigned xccr;
unsigned rccr;
unsigned bgr;
unsigned iva;
unsigned ipc;
unsigned imr0;
unsigned imr1;
unsigned pvr;
unsigned pim;
unsigned pcr;
//escc register defines for HDLC/SDLC mode
unsigned xad1;
unsigned xad2;
unsigned rah1;
unsigned rah2;
unsigned ral1;
unsigned ral2;
unsigned rlcr;
unsigned aml;
unsigned amh;
unsigned pre;
//escc async register defines (used in bisync as well)
unsigned xon;
unsigned xoff;
unsigned tcr;
unsigned dafo;
unsigned rfc;
unsigned tic;
unsigned mxn;
unsigned mxf;

//escc bisync register defines
unsigned synl;
unsigned synh;

unsigned n_rbufs;
unsigned n_tbufs;
unsigned n_rfsize_max;
unsigned n_tfsize_max;

}setup;

typedef struct escc_clkset{
	unsigned long clockbits;
	unsigned  numbits;
}clkset;

typedef struct escc_regsingle{
	unsigned long port;		//offset from base address of register to access
	unsigned char data;		//data to write
}regsingle;

typedef struct board_settings{
	unsigned base;
	unsigned irq;
	unsigned channel;
	unsigned dmar;
	unsigned dmat;
} board_settings;

typedef struct Escc_Dev {
//82532 register settings
setup settings;
//icd2053b settings
clkset clock;
//board switches
unsigned base;
unsigned irq;
unsigned channel;
unsigned dmar;
unsigned dmat;
unsigned board;//holds board number (ch0 and  ch1 have same board number)
               //this is used to lock access to the board if 
               //both channels access at the same time
               //in conjunction with the global atomic_t boardlock[] and 
               //the per device wboardlock wait queue 
unsigned dev_no;//only needed to check against previous in addboard
//add other per port info here
unsigned tx_type;
//state flags
unsigned irq_hooked;
unsigned port_initialized;

//interrupt status flags
volatile unsigned long status;

//wait queues
//struct wait_queue *rq;//these need to be replaced with message passing primitives
//struct wait_queue *wq;//there are no wait queues in qnx
//struct wait_queue *sq;//possibly do a read/reply message?
volatile unsigned rq;//flag that indicate that the irq service requested a read finished
volatile unsigned wq;//flag that indicate that the irq service requested a write finished
volatile unsigned sq;//flag that indicate that the irq service requested a status finished
//buffering params

struct buf *escc_rbuf;
struct buf *escc_tbuf;
volatile unsigned current_rxbuf;
volatile unsigned current_txbuf;
volatile unsigned is_transmitting;
volatile atomic_t received_frames_pending;
volatile atomic_t transmit_frames_available;

unsigned long escc_u_count;
uid_t escc_u_owner;

int intID;
int nonblocking;
pid_t w_pid;//pid of blocked write
pid_t r_pid;//pid of blocked read
pid_t s_pid;//pid of blocked status
unsigned long w_size;
unsigned long r_size;
unsigned write_pending;
unsigned read_pending;
unsigned status_pending;

pid_t select_pid;
pid_t rselect_pid_proxy;
pid_t tselect_pid_proxy;
pid_t sselect_pid_proxy;
unsigned rselect_pending;
unsigned tselect_pending;
unsigned sselect_pending;
int select_fd;
volatile unsigned long rpf;
volatile unsigned long rme;
volatile unsigned long xpr;
} Escc_Dev;


#define ESCC_INIT				1
#define ESCC_READ_REGISTER		2
#define ESCC_WRITE_REGISTER		3
#define ESCC_STATUS				4
#define ESCC_ADD_BOARD			5
#define ESCC_TX_ACTIVE			6
#define ESCC_RX_READY			7
#define ESCC_SETUP				ESCC_INIT
#define ESCC_START_TIMER		8
#define ESCC_STOP_TIMER			9
#define ESCC_SET_TX_TYPE		10
#define ESCC_SET_TX_ADDRESS		11
#define ESCC_FLUSH_RX			12
#define ESCC_FLUSH_TX			13
#define ESCC_CMDR				14
#define ESCC_GET_DSR			15
#define ESCC_SET_DTR			16
#define ESCC_SET_CLOCK			17
#define ESCC_IMMEDIATE_STATUS	18
#define ESCC_REMOVE_BOARD		19
#define ESCC_SET_FS6131         20
#define ESCC_IOC_MAXNR          20

//CMDR commands
#define XRES 1
#define XME 2
#define XIF 4
#define HUNT 4
#define XTF 8
#define STIB 16
#define RNR 32
#define RFRD 32
#define RHR 64
#define RMC 128
//STAR codes
#define WFA 1

#define CTS 2
#define CEC 4
#define RLI 8
#define TEC 8
#define RRNR 16
#define SYNC 16
#define FCS 16
#define XRNR 32
#define RFNE 32
#define XFW 64
#define XDOV 128
//ISR0
#define RPF 1
#define RFO 2
#define CDSC 4
#define PLLA 8
#define PCE 16
#define RSC 32
#define RFS 64
#define RME 128
//ISR1
#define XPR 1
#define XMR 2
#define CSC 4
#define TIN 8
#define EXE 16
#define XDU 16
#define ALLS 32
#define AOLP 32
#define RDO 64
#define OLP 64
#define EOP 128

//port addresses
//hdlc defines
#define STAR port
#define CMDR port
#define RSTA port+1
#define PRE port+1
#define MODE port+2
#define TIMR port+3
#define XAD1 port+4
#define XAD2 port+5
#define RAH1 port+6
#define RAH2 port+7
#define RAL1 port+8
#define RHCR port+9
#define RAL2 port+9
#define RBCL port+10
#define XBCL port+10
#define RBCH port+11
#define XBCH port+11
#define CCR0 port+12
#define CCR1 port+13
#define CCR2 port+14
#define CCR3 port+15
#define TSAX port+16
#define TSAR port+17
#define XCCR port+18
#define RCCR port+19
#define VSTR port+20
#define BGR port+20
#define RLCR port+21
#define AML port+22
#define AMH port+23
#define GIS port+24
#define IVA port+24
#define IPC port+25
#define ISR0 port+26
#define IMR0 port+26
#define ISR1 port+27
#define IMR1 port+27
#define PVR port+28
#define PIS port+29
#define PIM port+29
#define PCR port+30
#define CCR4 port+31
#define FIFO port+32
//async defines
#define XON port+4
#define XOFF port+5
#define TCR port+6
#define DAFO port+7
#define RFC port+8
#define TIC port+21
#define MXN port+22
#define MXF port+23
//bisync defines
#define SYNL port+4
#define SYNH port+5

//defines for 2053b
#define STARTWRD 0x1e05
#define MIDWRD   0x1e04
#define ENDWRD   0x1e00


#define OPMODE_HDLC		0
#define OPMODE_ASYNC		3
#define OPMODE_BISYNC		2
#define OPMODE_SDLC_LOOP	1
//device io control STATUS function return values
#define ST_RX_DONE		0x00000001
#define ST_OVF			0x00000002
#define ST_RFS			0x00000004
#define ST_RX_TIMEOUT	0x00000008
#define ST_RSC			0x00000010
#define ST_PERR			0x00000020
#define ST_PCE			0x00000040
#define ST_FERR			0x00000080
#define ST_SYN			0x00000100
#define ST_DPLLA		0x00000200
#define ST_CDSC			0x00000400
#define ST_RFO			0x00000800
#define ST_EOP			0x00001000
#define ST_BRKD			0x00002000
#define ST_ONLP			0x00004000
#define ST_BRKT			0x00008000
#define	ST_ALLS			0x00010000
#define ST_EXE			0x00020000
#define ST_TIN			0x00040000
#define ST_CTSC			0x00080000
#define ST_XMR			0x00100000
#define ST_TX_DONE		0x00200000
#define ST_DMA_TC		0x00400000
#define ST_DSR1C		0x00800000
#define ST_DSR0C		0x01000000
#define ST_FUBAR_IRQ    0x02000000

//our debug message, give file and line # along with message
//#define DBGMSG(c) fprintf(stderr,"%s:%u --%s \n",__FILE__,__LINE__,c)
#define DBGMSG(c) printf("%s:%u --%s \n",__FILE__,__LINE__,c)


#ifdef WAIT_FOR_CEC
#define WAIT_WITH_TIMEOUT				\
do							\
	{						\
	unsigned long timeout_val;				\
    timeout_val = 0;					\
	do						\
		{					\
		timeout_val++;				\
		if((inb(STAR)&CEC)==0x00) break;        \
		}while(timeout_val<MAX_TIMEOUT_VALUE);		\
		if(timeout_val>=MAX_TIMEOUT_VALUE) DBGMSG("WAIT_WITH_TIMEOUT--TIMED OUT");   \
	}while(0)
#else
 #define WAIT_WITH_TIMEOUT
#endif

pid_t far escc_irq(void);
void our_read(pid_t pid, io_msg *msg, Escc_Dev *dev);
void our_write(pid_t pid, io_msg *msg, Escc_Dev *dev);
void our_ioctl(pid_t pid, io_msg *msg, Escc_Dev *dev);
void our_status(pid_t pid, io_msg *msg, Escc_Dev *dev);

void set_fs6131(unsigned port,char data[10]);

extern int escc_nr_devs;
extern Escc_Dev *escc_devices; 
extern sem_t *boardlock;
extern pid_t isr_proxy;
extern char errbuf[256];
extern int *used_board_numbers;
extern int devno;

//these could stand to be made truely atomic...
#define atomic_inc(a) a++
#define atomic_dec(a) a--
#define atomic_set(a,b) a = b
#define atomic_read(a) a
//no string bytewide in/out definitions in qnx...hacked from their word wide definitions
extern caddr_t outsb(caddr_t buffer,unsigned count, unsigned port);
extern caddr_t insb(caddr_t buffer, unsigned count, unsigned port);
#pragma aux insb = "push ds" "cld" "pop es" "rep insb" "mov eax,edi"\
	__parm [edi] [ecx] [edx] __modify [es ecx edi] __value [eax];
#pragma aux outsb = "cld" "rep outsb" "mov eax,edi"\
	__parm [esi] [ecx] [edx] __modify [ecx esi] __value [eax];
