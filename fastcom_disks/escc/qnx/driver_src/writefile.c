/******************************************************
 *
 * Copyright (C) 2000, 2001 Commtech, Inc. Wichita KS
 *
 * open_init.c -- example code to open and configure the escc card/port
 *
 * qnx 4.25 1/4/01
 ******************************************************/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <ioctl.h>
#include <sys/qioctl.h>
#include <string.h>
#include "esccdrv_user.h"

void decode_stat(unsigned long stat);

void main(int argc, char *argv[])
{
int fd;
char data[4096];
char sbuf[255];
char rbuf[10];
unsigned i;
setup settings;
board_settings bset;
clkset clk;
regsingle reg;
unsigned val;
unsigned long status;
unsigned writesize;
FILE *fin;
int ret;
char devname[40];
int channel_number;
unsigned long brate;
int N;
FILE *fin;

if(argc<3)
{
printf("usage:\n");
printf("%s port filetosend\n",argv[0]);
exit(1);
}
channel_number = 0;//defaults to 0
if(argc>1)
{
sprintf(devname,"/escc/%u",atoi(argv[1]));
channel_number = atoi(argv[1]);//only works for single board install...ie channel # 2 doesn't make any sense.
}
else sprintf(devname,"/escc/0");

//open file given on command line from user
fin = fopen(argv[2],"rb");
if(fin==NULL)
{
printf("cannot open input file %s\n",argv[2]);
exit(1);
}



printf("device:%s\n",devname);
fd = open(devname,O_RDWR);//|O_NONBLOCK);
if(fd == -1) perror(NULL);

//function to set the address/irq that the board is configured to
//it lets the driver know where to access the card
bset.base = 0x280; //base address switch on the board
bset.irq = 5; //interrupt switch on the board
bset.channel = channel_number; //channel number (port) 0 or 1
bset.dmar = 0;//not used
bset.dmat = 0;//not used
if(qnx_ioctl(fd,ESCC_ADD_BOARD,&bset,sizeof(board_settings),rbuf,sizeof(rbuf))==-1)perror(NULL);

//this is the function that configures the individual port registers
//on the 82532, it also sets up the buffering parameters.
memset(&settings,0,sizeof(setup));
settings.ccr0 = 0x80;
settings.ccr1 = 0x17;
settings.ccr2 = 0xB8;//rate = 18.432E6/((n+1)*(2^m))


settings.ccr3 = 0x00;
settings.ccr4 = 0x40;//ebgr active
settings.bgr = 0x1D;//rate = 2400bps = 18.432E6/((29+1)*(2^8))
settings.mode = 0xc0;//extended transparent mode 0 (transmit only)
settings.n_rbufs = 2;//not receiving
settings.n_tbufs = 25;//
settings.n_rfsize_max = 4096;
settings.n_tfsize_max = 4096;

settings.timr = 0x1f;
settings.xbcl = 0x00;
settings.xbch = 0x00;
settings.iva = 0;
settings.ipc = 0x03;
settings.imr0 = 0x44;//disable receive frame start and CDSC interrupts
settings.imr1 = 0x0;
settings.pvr = 0x0;
settings.pim = 0xff;
settings.pcr = 0xe0;
settings.xad1 = 0xff;
settings.xad2 = 0xff;
settings.rah1 = 0xff;
settings.rah2 = 0xff;
settings.ral1 = 0xff;
settings.ral2 = 0xff;
settings.rlcr = 0x00;
settings.pre = 0x00;

printf("settings:%u\n",sizeof(setup));
if(qnx_ioctl(fd,ESCC_SETUP,&settings,sizeof(setup),rbuf,sizeof(rbuf))==-1)perror(NULL);


//open user file and send data

while(!feof(fin))//do this loop while there is still data to read from the user file
{
//printf("top of loop\n");

i = fread(data,1,1024,fin);//get data from user file
if(i!=0)
	{
	if((ret = write(fd,data,i))==-1)//write data out escc port (note assumes that open_init is used to open the port in HDLC mode with 4k buffers)
		{
		if(errno!=EAGAIN)perror(NULL);
		printf("write error\n");
		}
	}
}
status =0;
while((status&ST_EXE)!=ST_EXE)
	{
	if(qnx_ioctl(fd,ESCC_STATUS,NULL,0,&status,sizeof(status))==-1)perror(NULL);
	}
fclose(fin);
close(fd);
}


void decode_stat(unsigned long stat)
{
if(stat==0)return;
printf("STATUS DECODE:\n");
if(stat&ST_RX_DONE) printf("Receive Done\n");
if(stat&ST_OVF) printf("RX BUFFERS overflow\n");
if(stat&ST_RFS) printf("Receive Frame Start\n");
if(stat&ST_RX_TIMEOUT) printf("RX timeout\n");
if(stat&ST_RSC) printf("Receive Status Change\n");
if(stat&ST_PERR) printf("Parity Error\n");
if(stat&ST_PCE) printf("Protocol Error\n");
if(stat&ST_FERR) printf("Framing Error\n");
if(stat&ST_SYN) printf("SYN detect\n");
if(stat&ST_DPLLA) printf("DPLL asyncronous\n");
if(stat&ST_CDSC) printf("CD changed state\n");
if(stat&ST_RFO) printf("Receive Frame Overflow\n");
if(stat&ST_EOP) printf("End of Poll\n");
if(stat&ST_BRKD) printf("Break Detected\n");
if(stat&ST_ONLP) printf("On Loop\n");
if(stat&ST_BRKT) printf("Break Terminated\n");
if(stat&ST_ALLS) printf("All Sent\n");
if(stat&ST_EXE) printf("Transmit Underrun\n");
if(stat&ST_TIN) printf("Timer Expired\n");
if(stat&ST_CTSC) printf("CTS changed state\n");
if(stat&ST_XMR) printf("Transmit Message Repeat\n");
if(stat&ST_TX_DONE) printf("Transmit Done\n");
if(stat&ST_DMA_TC) printf("DMA TC reached\n");
if(stat&ST_DSR0C) printf("DSR0 changed state\n");
if(stat&ST_DSR1C) printf("DSR1 changed state\n");
	}
