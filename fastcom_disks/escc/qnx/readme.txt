This code will compile/run on QNX 4.25 only

To use this QNX code, simply execute

./esccdrv &

then execute the loop program as:

./loop 0

This will execute a simple loopback sending and receiving frames.
The loop.c code incorporates the code from open_init.c and looptest.c 
and adds the clock generator setting function for the FS6131 (sets 
it to 16MHz).  Of course executing this sequence it is assumed that 
you have a loopback adapter plugged into the escc port, (the way it is 
currently coded, you must have a minimum of RD->SD connected).

You can modify the driver source found in the driver_src directory
as you see necessary.  You would then need to execute

./ccescc

to compile your sourcecode into a new driver and then load the new
driver by executing

./esccdrv &

again.

cg
1/8/2004