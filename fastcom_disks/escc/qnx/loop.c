/******************************************************
 *
 * Copyright (C) 2000, 2001 Commtech, Inc. Wichita KS
 *
 * open_init.c -- example code to open and configure the escc card/port
 *
 * qnx 4.25 1/4/01
 ******************************************************/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <ioctl.h>
#include <sys/qioctl.h>
#include <string.h>
#include "esccdrv_user.h"

void decode_stat(unsigned long stat);

static char fs6131_16M[10] = {0x00,0x10,0x10,0x14,0x7D,0x80,0x21,0xFE,0xd0};
void main(int argc, char *argv[])
{
int fd;
char data[4096];
char sbuf[255];
char rbuf[10];
unsigned i;
setup settings;
board_settings bset;
clkset clk;
regsingle reg;
unsigned val;
unsigned long status;
unsigned writesize;
FILE *fin;
int ret;
char devname[40];
int channel_number;
unsigned long brate;
int N;
unsigned long txcount=0;

if(argc<2)
{
printf("usage:\n");
printf("open_init port\n");
exit(1);
}
channel_number = 0;//defaults to 0
if(argc>1)
{
sprintf(devname,"/escc/%u",atoi(argv[1]));
channel_number = atoi(argv[1]);//only works for single board install...ie channel # 2 doesn't make any sense.
}
else sprintf(devname,"/escc/0");


printf("device:%s\n",devname);
fd = open(devname,O_RDWR);//|O_NONBLOCK);
if(fd == -1) perror(NULL);

//function to set the address/irq that the board is configured to
//it lets the driver know where to access the card
bset.base = 0x280; //base address switch on the board
bset.irq = 5; //interrupt switch on the board
bset.channel = channel_number; //channel number (port) 0 or 1
bset.dmar = 0;//not used
bset.dmat = 0;//not used
if(qnx_ioctl(fd,ESCC_ADD_BOARD,&bset,sizeof(board_settings),rbuf,sizeof(rbuf))==-1)perror(NULL);

if(0)
{//ESCC-104
clk.clockbits = 0x5d1460;//set to 16MHz
clk.numbits = 23;
if(qnx_ioctl(fd,ESCC_SET_CLOCK,&clk,sizeof(clkset),rbuf,sizeof(rbuf))==-1)perror(NULL);
}
else
{//ESCC-104-ET
if(qnx_ioctl(fd,ESCC_SET_FS6131,&fs6131_16M,10*sizeof(char),rbuf,sizeof(rbuf))==-1)perror(NULL);
}

//this is the function that configures the individual port registers
//on the 82532, it also sets up the buffering parameters.
memset(&settings,0,sizeof(setup));
settings.ccr0 = 0x80;
settings.ccr1 = 0x16;
settings.ccr2 = 0x18;
settings.ccr3 = 0x00;
settings.ccr4 = 0x00;
settings.bgr =  0x07;
settings.mode = 0x88;
settings.n_rbufs = 10;
settings.n_tbufs = 10;
settings.n_rfsize_max = 4096;
settings.n_tfsize_max = 4096;

settings.timr = 0x1f;
settings.xbcl = 0x00;
settings.xbch = 0x00;
settings.iva = 0;
settings.ipc = 0x03;
settings.imr0 = 0x44;//disable receive frame start and CDSC interrupts
settings.imr1 = 0x0;
settings.pvr = 0x0;
settings.pim = 0xff;
settings.pcr = 0xe0;
settings.xad1 = 0xff;
settings.xad2 = 0xff;
settings.rah1 = 0xff;
settings.rah2 = 0xff;
settings.ral1 = 0xff;
settings.ral2 = 0xff;
settings.rlcr = 0x00;
settings.pre = 0x00;

printf("settings:%u\n",sizeof(setup));
if(qnx_ioctl(fd,ESCC_SETUP,&settings,sizeof(setup),rbuf,sizeof(rbuf))==-1)perror(NULL);

txcount = 0;
while(!kbhit()) //continue while no keyboard activity (will exit on keypress)
{
for(i=0;i<257;i++) data[i] = i;//fill our outgoing data struct with 0-ff
writesize = 256;//send 256 bytes of data
if((ret = write(fd,data,writesize))==-1)//send it out the escc port
	{
	perror(NULL);
	printf("write failed:%u\n",ret);
	}
if(ret!=256) printf("ERROR,write returned:%u\n",ret);//it should send all 256 bytes unless you opened the port with a buffer less than 256 bytes. (it is assumed here that the open_init program opened and configured the port with a 4k buffer size)

if(((ret = read(fd,data,4096)))==-1)//issue the read function call, should return with 257 bytes, our 256 looped back data bytes and a HDLC receive status byte (contents of the RSTA register will be the last byte received)
	{
	perror(NULL);
	printf("read failed:%u\n",ret);
	}
if(ret!=257)printf("read size problem:%u!=257\n",ret);//should be 256 data bytes and one status byte (again assuming that open_init opened/configured this port to be in HDLC mode)
for(i=0;i<ret-1;i++) //check the data bytes
{
	if(i!=data[i])
	{
	printf("error in received data at %u!=%u\n",i,data[i]);
	}
}
printf("Loopcount:%lu\n",txcount);//display the cycle we are on
txcount++;//increment the cycle counter

}
getch();//user pressed a key to break out of the loop get the key
printf("paused...\n");
getch();
//pull/clear the status from the driver 
if(qnx_ioctl(fd,ESCC_IMMEDIATE_STATUS,NULL,0,&status,sizeof(status))==-1)perror(NULL);
//display the status values
decode_stat(status);



close(fd);
}


void decode_stat(unsigned long stat)
{
if(stat==0)return;
printf("STATUS DECODE:\n");
if(stat&ST_RX_DONE) printf("Receive Done\n");
if(stat&ST_OVF) printf("RX BUFFERS overflow\n");
if(stat&ST_RFS) printf("Receive Frame Start\n");
if(stat&ST_RX_TIMEOUT) printf("RX timeout\n");
if(stat&ST_RSC) printf("Receive Status Change\n");
if(stat&ST_PERR) printf("Parity Error\n");
if(stat&ST_PCE) printf("Protocol Error\n");
if(stat&ST_FERR) printf("Framing Error\n");
if(stat&ST_SYN) printf("SYN detect\n");
if(stat&ST_DPLLA) printf("DPLL asyncronous\n");
if(stat&ST_CDSC) printf("CD changed state\n");
if(stat&ST_RFO) printf("Receive Frame Overflow\n");
if(stat&ST_EOP) printf("End of Poll\n");
if(stat&ST_BRKD) printf("Break Detected\n");
if(stat&ST_ONLP) printf("On Loop\n");
if(stat&ST_BRKT) printf("Break Terminated\n");
if(stat&ST_ALLS) printf("All Sent\n");
if(stat&ST_EXE) printf("Transmit Underrun\n");
if(stat&ST_TIN) printf("Timer Expired\n");
if(stat&ST_CTSC) printf("CTS changed state\n");
if(stat&ST_XMR) printf("Transmit Message Repeat\n");
if(stat&ST_TX_DONE) printf("Transmit Done\n");
if(stat&ST_DMA_TC) printf("DMA TC reached\n");
if(stat&ST_DSR0C) printf("DSR0 changed state\n");
if(stat&ST_DSR1C) printf("DSR1 changed state\n");
}
