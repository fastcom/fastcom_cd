This program sets the clock on the extended temperature board.

Do not use this board if you do not have an ESCC-104-ET.

If you are unsure, check the back side of the board, if you see a 

chip labeled FS6131-01, then you have an extended temperature board.