/* $Id$ */
/*
	Copyright(C) 2002,2003 Commtech, Inc.

	set_clock.c --  a user program to set the current clock generator
			setting for the industrial temp Fastcom:ESCC-104-ET card
			(using the FS6131-1 instead of the icd2053b)
			This code actually twiggles the clock/data lines to program the FS6131-1

usage:
int set_fs6131(int port, char CData[10])

port is the physical i/o port to write to
CData is the 10 byte programming values for the FS6131-1

*/

#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "escctest.h"


int set_fs6131(int port, char CData[10])
{
	HANDLE hDevice; 
	ULONG temp;
	char devname[128];
	sprintf(devname,"\\\\.\\ESCC%d",port);
	if((hDevice = CreateFile (
			devname, 
			GENERIC_READ | GENERIC_WRITE,
			0,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
			NULL)) == INVALID_HANDLE_VALUE ) 
		{
        printf("Can't get a handle to esccdrv @ %s\n",devname);
		exit(1);
		}
	printf("Opened the %s Successfully!\n",devname);

    DeviceIoControl(hDevice,IOCTL_ESCCDRV_SET_FS6131,&CData[0],10*sizeof(char),NULL,0,&temp,NULL);

    CloseHandle(hDevice);
	return 0;
}
/* $Id$ */
