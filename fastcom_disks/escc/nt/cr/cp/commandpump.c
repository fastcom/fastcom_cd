/*
Copyright (C) 1999 Commtech, Inc. Wichita, KS.

Module name:
  
	commandpump.c

Abstract:

	Win32 console program that demonstrates how to connect to the named
	pipe created using the cr.c /cr.exe example program.
	essentially opens the \\.\pipe\commandpipe named pipe
	and sends 6 byte messages to the pipe to be sent out the ESCC.

Environment:

    user mode only, compiled using Visual C++ ver 6.0

Notes:

    
Revision History:

    6/11/1999             started

*/

#include "windows.h"
#include "stdio.h"
#include "conio.h"


void main()
{
HANDLE	commandpipeh;	//handle for named pipe
char	command[10];	//data array for command to send
DWORD	i,j,rep;		//temp vars		
DWORD	byteswritten;	//holds number of bytes written out pipe
DWORD	framecount;		//frame counter

framecount=0;			//start frame count at 0
//create the named pipe (open it actually)
commandpipeh = CreateFile("\\\\.\\pipe\\commandpipe",GENERIC_WRITE,0,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
if(commandpipeh==INVALID_HANDLE_VALUE)
	{
	printf("cannot open command pipe\r\n");
	exit(1);
	}
//display what you can do.
printf("enter the number corresponding the the number of commands to send\r\n");
printf("1		-> 30\r\n");
printf("2		-> 60\r\n");
printf("3		-> 90\r\n");
printf("4		-> 120\r\n");
printf("[esc]	-> exit\r\n");

i = getch(); //get user input
if(i=='1') rep = 30;	//set rep (repitition) rate to user selection
if(i=='2') rep = 60;
if(i=='3') rep = 90;
if(i=='4') rep = 120;
if(i==27) return;

do
	{
	for(j=0;j<rep;j++)
		{
		//fill the command with what you want to send 
		//(here I am just filling it with the frame count
		// so I can see that the data is making the full rounds).
		sprintf(command,"%6.6u",framecount++);
		//send the data down the pipe
		WriteFile(commandpipeh,&command,6,&byteswritten,NULL);
		}
	i = getch();//get user input
	if(i=='1') rep = 30;//set rep (repitition) rate to user selection
	if(i=='2') rep = 60;
	if(i=='3') rep = 90;
	if(i=='4') rep = 120;
	}while(i!=27);
//done with the pipe
CloseHandle(commandpipeh);
//done
}