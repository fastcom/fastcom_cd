/*++

Copyright (c) 1999 Commtech, Inc Wichita ,KS

Module Name:

    cr.c

Abstract:

    A simple Win32 app that uses the esccdrv device to create a pair of 
	named pipes to send commands and receive responses through the ESCC
	the pipes created are:
	\\.\pipe\commandpipe
	\\.\pipe\responsepipe
	
	the command pipe is expecting command messages 6 bytes in length,
	and will pull 1 message out of the pipe and send it out the ESCC
	every 10mS.
	
	The response pipe will relay the responses that are received by the
	ESCC.  (They are expected to be 8 byte messages). 

    the other example programs cp and rp are examples of how to deal 
	with the other ends of the pipes.
	the cp (command pump) program opens the commandpipe and sends
	the 6 byte messages that get sent out of the ESCC.
	the rp (response pump) program opens the responsepipe and 
	displays the data being received along with a received frame count.

Environment:

    user mode only, compiled using Visual C++ ver 6.0

Notes:

    
Revision History:

    6/11/1999             started
--*/



#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include "escctest.h"


DWORD FAR PASCAL StatProc( LPVOID lpData );//thread routine that handles status returns from the escc driver
DWORD FAR PASCAL ReadProc( LPVOID lpData );//thread routine that handles read calls to the escc driver
		
BOOL connected;//flag to let threads know when to dump (exit)

VOID
main(IN int   argc,IN char *argv[] )
{
HANDLE		hDevice;	//handle to the escc driver/device
struct setup esccsetup;	//setup structure for initializing the escc registers (see escctest.h)
struct clkset clk;		//struct used to set clock generator
ULONG		j,k;		//temp vars
DWORD		returnsize;	//temp vars
BOOL		t;			//temp vars
HANDLE      hstatThread;//handle to status thread 
HANDLE      hreadThread;//handle to read thread
DWORD       statID;		//status thread ID storage
DWORD       readID;		//read thread ID storage
DWORD       dwThreadID ;//temp Thread ID storage
LPVOID		phdev;		//pointer to device Handle
char		devname[80];//holds device name
unsigned	devno;		//holds device number



    
devno = 0;//default to 0
if(argc>1) devno = atoi(argv[1]);//if specified on command line take it to what they give us

sprintf(devname,"\\\\.\\ESCC%u",devno);//create device name
    hDevice = CreateFile (devname,
			  GENERIC_READ | GENERIC_WRITE,
			  0,
			  NULL,
			  OPEN_EXISTING,
			  FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
			  NULL
			  );//create handle to escc device

    if (hDevice == INVALID_HANDLE_VALUE)
    {
		//for some reason the driver won't load or isn't loaded
	printf ("Can't get a handle to escc%u\n",devno);
		exit(1);
		//abort and leave here!!!
	}
	//we got our handle and are ready to go
		printf("Created esccdrv--ESCC%u\n\r",devno);


clk.clockbits = 0x5d61c0; //set to 1.2288 MHz
clk.numbits = 24;
t = DeviceIoControl(hDevice,IOCTL_ESCCDRV_SET_CLOCK,&clk,sizeof(struct clkset),NULL,0,&returnsize,NULL);

	printf("resetting\n\r");
		//make to use async settings here
		esccsetup.mode = 0x0a;
		esccsetup.timr = 0xf7;
		esccsetup.xbcl = 0x00;
		esccsetup.xbch = 0x00;
		esccsetup.ccr0 = 0xc3;   //async
		esccsetup.ccr1 = 0x1f; //bit clock rate =16, clock mode 7
		esccsetup.ccr2 = 0x18;
		esccsetup.bgr = 0x00;	
		esccsetup.iva = 0;
		esccsetup.ipc = 0x03;
		esccsetup.imr0 = 0x04;
		esccsetup.imr1 = 0x0;
		esccsetup.pvr = 0x0;
		esccsetup.pim = 0xff;
		esccsetup.pcr = 0xe0;
		esccsetup.tcr = 0x00;
		esccsetup.dafo = 0x0C;	//O81
		esccsetup.rfc = 0x4c;	

		esccsetup.n_rbufs = 30;//number of internal receive buffers
		esccsetup.n_tbufs = 30;//number of internal transmit buffers (note we allways only use 1 (in this program) and the min is 2)
		esccsetup.n_rfsize_max = 64;//size of each internal receive buffer
		esccsetup.n_tfsize_max = 64;//size of each internal transmit buffer


	t = DeviceIoControl(hDevice,IOCTL_ESCCDRV_SETUP,&esccsetup,sizeof(struct setup),NULL,0,&returnsize,NULL);
	if(t==TRUE)printf("SETTINGS SUCCESSFUL:%lu\n\r",returnsize);
	if(t==FALSE)printf("SETTINGS FAILED:%lu\n\r",returnsize);


	
//Two threads will be created a dedicated thread to check on
//the status of the escc and a dedicated thread to do READ function calls
//startup support threads, the main thread (this one) will just hang out until the user presses [esc]
connected = TRUE;//flag to support threads that they should stay active (exit when false)
phdev = &hDevice;//used to pass the device handle to the threads so they can work with the ESCC

hreadThread =     CreateThread( (LPSECURITY_ATTRIBUTES) NULL,
				    0, 
				    (LPTHREAD_START_ROUTINE) ReadProc,
				    (LPVOID) phdev,
				    0, &dwThreadID );

if(hreadThread==NULL)
	{
	printf("cannot start Data read thread\n\r");
	CloseHandle(hDevice);
	exit(1);
	}
readID=dwThreadID;

hstatThread =     CreateThread( (LPSECURITY_ATTRIBUTES) NULL,
				    0, 
				    (LPTHREAD_START_ROUTINE) StatProc,
				    (LPVOID) phdev,
				    0, &dwThreadID );

if(hstatThread==NULL)
	{
	printf("cannot start status thread\n\r");
	CloseHandle(hDevice);
	exit(1);
	}
statID=dwThreadID;



	
	
	//This starts the escc timer (notice no parameters passed)
	//this function will start the timer (if in external mode)
	//this should force the status thread to become active with
	//a ST_TIN response at a time specified by the value of k and TIMR in the escc registers
	//this function will allways return TRUE 
     t = DeviceIoControl(hDevice,IOCTL_ESCCDRV_START_TIMER,NULL,0,NULL,0,&returnsize,NULL);
	
	//this will forever start sending messages at 100Hz...at least until we hit a key
    
	
//now we enter the main loop of this thread        
//all we are going to do is wait for a keyhit and when we 
//if the [ESC] key is pressed the program will terminate
do
	{
//this is a holdover from the loopback example program.
//I left it here because it might be usefull to force a flushrx or flushtx
//by the operator if something goes haywire.
//the decoded keys are t,r,i,p,[esc]
//t flushes the transmit queue
//r flushes the receive queue
//i starts the timer
//p stops the timer
//[esc] exits the program

	printf("waiting for a key\n\r");
kht:        
	j = getch();                            //wait for keyhit
	if((j&0xff)=='r')
		{
		//if the user presses r on the keyboard, we will flush the receive buffers.
		//this will clear all of the internal receive buffers and issue a RHR command
		//to the ESCC.  It will allways return TRUE unless the RHR command times out
		DeviceIoControl(hDevice,IOCTL_ESCCDRV_FLUSH_RX,NULL,0,NULL,0,&returnsize,NULL);
		printf("RX flushed\r\n");
		goto kht;
		}
	if((j&0xff)=='t')
		{
		//if the user presses t on the keyboard, we will flush the transmit buffers.
		//this will clear all of the internal transmit buffers and issue a XRES command
		//to the ESCC.  It will allways return TRUE unless the XRES command times out
		DeviceIoControl(hDevice,IOCTL_ESCCDRV_FLUSH_TX,NULL,0,NULL,0,&returnsize,NULL);
		printf("TX flushed\r\n");
		goto kht;
		}
	if((j&0xff)=='i')
		{
		//alternate way of issuing timer commands, via the IOCTL_ESCCDRV_CMDR interface,
		//this will take the value passed and send it directly to the escc CMDR register.
		//It should allways return TRUE
		//if the user presses i the this timer command will be issued
		k = 0x10;
		DeviceIoControl(hDevice,IOCTL_ESCCDRV_CMDR,&k,sizeof(DWORD),NULL,0,&returnsize,NULL);
		printf("timer command issued\n\r");
		goto kht;
		}
	if((j&0xff)=='p')
		{
      	//if the user presses p then stop the timer.
		DeviceIoControl(hDevice,IOCTL_ESCCDRV_STOP_TIMER,NULL,0,NULL,0,&returnsize,NULL);
		printf("timer stopped\n\r");
		goto kht;
		}
	}while((j&0xff)!=27);         //keep getting keys and sending frames until esc is pressed


connected = FALSE;      //indicate to threads that we are leaving
Sleep(5000); //give the worker threads a few seconds to terminate
CloseHandle (hDevice);// stops the escc from interrupting (ie shuts it down)
CloseHandle (hreadThread);                //done with this thread
CloseHandle (hstatThread);                //done with this thread
printf("exiting program\n\r");          //exit message
}                                               //done

							 

DWORD FAR PASCAL StatProc( LPHANDLE lpData )
{
HANDLE		hdev;			//device handle to esccdrv
DWORD		i,j,k;			//temp storage
BOOL		t;				//temp storage
DWORD		returnsize;		//temp storage
DWORD		statusmask;		//used to mask the status returns
OVERLAPPED  os ;			//overlapped struct for driver signaling
OVERLAPPED	osw;			//overlapped struct for the write to ESCC
DWORD		writesize;		//number of bytes to write used for WriteFile to ESCC
DWORD		byteswritten;	//holds the result of the WriteFile() 
char		command[10];    //the current command, used to queue the commands and receive them from the pipe
HANDLE		inpipeh;		//handle to named pipe to get commands from
DWORD		bytesread;		//number of bytes received from the pipe
DWORD		bytesavail;		//used in the peeknamedpipe call
DWORD		bytesleft;		//used in the peeknamedpipe call

hdev = (LPHANDLE)lpData[0];     //the escc device handle is passed in to this thread


//create the named pipe used to get commands from
//note that in this version of the program the named pipe is the 
//buffer for the messages, so if you want to buffer more messages, just
//give a bigger value for the buffer size for thepipe (the 4096 values below).
inpipeh = CreateNamedPipe("\\\\.\\pipe\\commandpipe",
						  PIPE_ACCESS_INBOUND,
						  PIPE_TYPE_MESSAGE|PIPE_READMODE_MESSAGE|PIPE_WAIT,
						  1,4096,4096,500,NULL);
if(inpipeh==INVALID_HANDLE_VALUE)
	{
	printf("cannot create command pipe\r\n");
	printf("ERROR:%x",GetLastError());
	return (FALSE);
	}

memset( &os, 0, sizeof( OVERLAPPED ) ) ; //wipe the overlapped structure

// create I/O event used for overlapped status

os.hEvent = CreateEvent( NULL,    // no security
			    TRUE,    // explicit reset req
			    FALSE,   // initial event reset
			    NULL ) ; // no name
if (os.hEvent == NULL)
   {
	printf("Failed to create event for thread!\r\n");
    return ( FALSE ) ;
   }

memset( &osw, 0, sizeof( OVERLAPPED ) ) ; //wipe the overlapped structure

// create I/O event used for overlapped write

osw.hEvent = CreateEvent( NULL,    // no security
			    TRUE,    // explicit reset req
			    FALSE,   // initial event reset
			    NULL ) ; // no name
if (osw.hEvent == NULL)
	{
    printf("Failed to create event for thread!\r\n");
    CloseHandle(os.hEvent);
    return ( FALSE ) ;
	}


//wait for named pipe to be connected (ie there is a command pump going at
//the other end of the pipe).
//if you want the connection to be dynamic you can put this further down
//in the status loop, you just need to make sure you have an open pipe
//before you start to try to pull messages from it.
//the benefit of putting it further down would be that you would not have
//to exit and restart the program to re-connect the pipe in the event that
//it breaks (the command pump application is closed).
//the benefit of leaving it here is it is simpler.
if(ConnectNamedPipe(inpipeh,NULL)==0)
	{
	//pipe failed
	printf("command pipe connect failed\r\n");
	goto done;
	}

printf("status thread started\n\r");    //entry message
statusmask= 0xffffffff - ST_RX_DONE - ST_TX_DONE; //create mask so that we do not receive rxdone or txdone status indicators
do
	{

	//gets the error status info from the driver
	//This IOCTL will not return until the status changes if the escc device
	//was opened with the non-overlapped io.
	//since we opened with overlapped io it will return immed either with
	//the status (if the status bits were set before the call) and the return value will be TRUE
	//or the return value will be FALSE with ERROR_IO_PENDING or a device busy error message
	//the device busy will be returned if a call is made to IOCTL_ESCCDRV_STATUS with an 
	//outstanding call in progress.
	//the return buffer will hold the current status upon completion of the IO request

	t = DeviceIoControl(hdev,IOCTL_ESCCDRV_STATUS,&statusmask,sizeof(ULONG),&j,sizeof(ULONG),&returnsize,&os);
//if it returns true then you are not calling it fast enough to keep up with
//the 100 Hz ST_TIN rate.

	if(t==FALSE)
		{
		if (GetLastError() == ERROR_IO_PENDING) //overlapped io indicates that the IRP isn't done being processed yet (status hasn't changed)
			{
			// wait for the status to change
			//note that this will block indefinitly unless you give a value
			//different than INFINITE to the waitforsingleobject() call
			//if the status never changes the call to closehandle() on the 
			//esccdevice will cause the cancel routine to cancel the io request
			//and the wait will complete (and if everything goes right the connected 
			//indicator will be false and the thread will terminate)
			do
				{
				k = WaitForSingleObject( os.hEvent, 1000 );//1 second timeout
				if(k==WAIT_TIMEOUT)
					{
					//printf("STATUS:TIMEOUT\r\n");
					//this will execute every 1 second
					//if you want do do some periodic processing here would
					//be a good place to put it...
					}
				if(k==WAIT_ABANDONED)
					{
					//printf("STATUS:ABANDONED\r\n");
					}
				
				}while((k!=WAIT_OBJECT_0)&&(connected));//exit if we get signaled or if the main thread quits
				
				//printf("STATUS:SIGNALED:=%lx\r\n",j);
			}	
		}
	if(connected)                   //if not connected then j is invalid
		{
		if((j&ST_RX_DONE)==ST_RX_DONE) printf("STATUS, RX_DONE\r\n");//this one is masked
		if((j&ST_OVF)==ST_OVF) printf("STATUS, BUFFERS overflowed\n\r");//if you get this you should increase the number of buffers allocated in the setup ioctl, and or get a faster processor/streemline the system.
																		//as the processor is not calling readfile() frequently enough to pull the frames from the internal driver buffers 
//		if((j&ST_RFS)==ST_RFS) printf("STATUS, Receive Frame Start\r\n");
//		if((j&ST_RX_TIMEOUT)==ST_RX_TIMEOUT) printf("STATUS, Receive Timeout\r\n");
//		if((j&ST_RSC)==ST_RSC) printf("STATUS, Receive Status Change\r\n");
//		if((j&ST_PERR)==ST_PERR) printf("STATUS, Parity Error\r\n");//if this error happens you should check the parity settings (make sure it is matched to the other side)
																	//if it still occurs, check the number of data bits/number of stop bits
																	//if it still occurs, check the bitrate (again match the other side)
																	//if it still occurs, check your cabling for loose connections.
																	
//		if((j&ST_PCE)==ST_PCE) printf("STATUS, Protocol Error\r\n");
//		if((j&ST_FERR)==ST_FERR) printf("STATUS, Framing Error\r\n");//if this error happens you should check your bitrate (match the other side)
																	 //if it still occurs, check the number of data bits/number of stop bits
																	 //if it still occurs, check your cabling
																	 //
//		if((j&ST_SYN)==ST_SYN) printf("STATUS, SYN detected\r\n");
//		if((j&ST_DPLLA)==ST_DPLLA) printf("STATUS, DPLL Asynchronous\r\n");
//		if((j&ST_CDSC)==ST_CDSC) printf("STATUS, Carrier Detect Change State\r\n");
		if((j&ST_RFO)==ST_RFO) printf("STATUS, Receive Frame Overflow(HARDWARE)\r\n");//if this happens something is very wrong, as the processor has not gotten around to getting data from the fifo in 64 character times.

//		if((j&ST_EOP)==ST_EOP) printf("STATUS, End of Poll\r\n");
//		if((j&ST_BRKD)==ST_BRKD) printf("STATUS, Break Detected\r\n");//do break processing here
//		if((j&ST_ONLP)==ST_ONLP) printf("STATUS, On Loop\r\n");
//		if((j&ST_BRKT)==ST_BRKT) printf("STATUS, Break Terminated\r\n");//do end of break processing here
//		if((j&ST_ALLS)==ST_ALLS) printf("STATUS, All Sent\r\n");
//		if((j&ST_EXE)==ST_EXE) printf("STATUS, Transmit Underrun\r\n");
//		if((j&ST_CTSC)==ST_CTSC) printf("STATUS, CTS Changed State\r\n");
//		if((j&ST_XMR)==ST_XMR) printf("STATUS, Transmit Message Repeat\r\n");
//		if((j&ST_TX_DONE)==ST_TX_DONE) printf("STATUS, TX Done\r\n");//this is masked
//		if((j&ST_DMA_TC)==ST_DMA_TC) printf("STATUS, DMA TC reached\r\n");
//		if((j&ST_DSR1C)==ST_DSR1C) printf("STATUS, Channel 1 DSR Changed\r\n");
//		if((j&ST_DSR0C)==ST_DSR0C) printf("STATUS, Channel 0 DSR Changed\r\n");
		if((j&ST_TIN)==ST_TIN)
			{
		//printf("TIN\r\n");//this is the timer status return
		//lets check the pipe to see if a message is there
			t = PeekNamedPipe(inpipeh,NULL,0,NULL,&bytesavail,&bytesleft);
			if(t==FALSE)
				{
				//printf("peek error:%x\r\n",GetLastError());
				//if the pipe is broken then wait for it to connect again
				//(there is no sense in continuing processing status messages
				// if we do not have a command/message source)
				//the status values will just build up and overwrite themselves while
				//we wait.
				i = GetLastError();
				if(i==ERROR_BROKEN_PIPE)
					{
					printf("disconnect/reconnect command pipe\r\n");
					DisconnectNamedPipe(inpipeh);			//force the pipe closed
					ConnectNamedPipe(inpipeh,NULL);			//wait for a new one
					}
				}
			if(bytesavail>0)//if bigger than 0 then there is at least one message in the pipe
				{
				t = ReadFile(inpipeh,&command,6,&bytesread,NULL);//get the message
				writesize = bytesread;
				t = WriteFile(hdev,command,writesize,&byteswritten,&osw);//send the frame
					//if(t==TRUE); //if returned true then the IO request was started (txing has begun)
					//this routine should allways return true, as the time to send the frame is much less than the frame rate
					//the frames should be completely sent before the next one gets started
					if(t==FALSE)                    //if it returned FALSE then the IO request is queued (waiting for previous frame to get done sending)
						{                       //and we must wait until the os.event gets signaled before we try any more sending
						printf("returned FALSE\n\r");
						// I would treat this as a major error /problem, flush the tx,rx and start over.
						DeviceIoControl(hdev,IOCTL_ESCCDRV_FLUSH_TX,NULL,0,NULL,0,&returnsize,NULL);
						DeviceIoControl(hdev,IOCTL_ESCCDRV_FLUSH_RX,NULL,0,NULL,0,&returnsize,NULL);
						}               
				}
			}
		}
	}while(connected);              //keep making requests until we want to terminate
done:
DisconnectNamedPipe(inpipeh);			//force the pipe closed
CloseHandle( os.hEvent ) ;              //we are terminating so close the event
CloseHandle( osw.hEvent ) ;              //we are terminating so close the event
CloseHandle(inpipeh);					//we are done with the pipe
printf("exiting status thread\n\r");    //exit message
return(FALSE);                          //done
}


DWORD FAR PASCAL ReadProc( LPHANDLE lpData )
{
HANDLE		hdev;			//handle to the esccdrv device
DWORD		i,j;			//temp
BOOL		t;				//temp
char		data[4096];		//data storage for data from the driver (note you can trim it down to 64 bytes)
DWORD		nobytestoread;	//the number of bytes that can be put in data[] (max)
DWORD		nobytesread;	//the number of bytes that the driver put in data[]
OVERLAPPED  os ;			//overlapped struct for overlapped I/O
DWORD		fcount;			//received frame(message) count
HANDLE		outpipeh;		//named pipe to send responses to
DWORD		nobyteswritten; //holds number of bytes written in writefile call

fcount = 1;	//start frame count at 1.

//create the named pipe used to get commands from
//the same goes for this pipe as the command pipe, the buffering can be adjusted by changing
//the 4096 values below.
outpipeh = CreateNamedPipe("\\\\.\\pipe\\responsepipe",
						  PIPE_ACCESS_OUTBOUND,
						  PIPE_TYPE_MESSAGE|PIPE_READMODE_MESSAGE|PIPE_WAIT,
						  1,4096,4096,500,NULL);
if(outpipeh==INVALID_HANDLE_VALUE)
	{
	printf("cannot create Response pipe\r\n");
	printf("ERROR:%x",GetLastError());
	return (FALSE);
	}


memset( &os, 0, sizeof( OVERLAPPED ) ) ;        //wipe the overlapped struct

// create I/O event used for overlapped read

os.hEvent = CreateEvent( NULL,    // no security
			    TRUE,    // explicit reset req
			    FALSE,   // initial event reset
			    NULL ) ; // no name
if (os.hEvent == NULL)
	{
	printf("Failed to create event for thread!\r\n");
    return ( FALSE ) ;
	}

hdev = (LPHANDLE)lpData[0];     //we get the escc device handle passed to us

nobytestoread = 4096;		//(this can be trimed to 64 bytes if you trim the data[] array)

//this will wait until the response client opens the pipe
if(ConnectNamedPipe(outpipeh,NULL)==0)
	{
	//pipe failed
	printf("response pipe connect failed\r\n");
	goto done;
	}

printf("read thread started\n\r");     //entry message

do
	{
	//start a read request by calling ReadFile() with the esccdevice handle
	//if it returns true then we received a frame 
	//if it returns false and ERROR_IO_PENDING then there are no
	//receive frames available so we wait until the overlapped struct
	//gets signaled.

	t = ReadFile(hdev,&data,nobytestoread,&nobytesread,&os);
	
	if(t==FALSE)
		{
		if (GetLastError() == ERROR_IO_PENDING)
			{
			// wait for a receive frame to come in, note it will wait forever
			do
				{
				j = WaitForSingleObject( os.hEvent, 1000 );//1 second timeout
				if(j==WAIT_TIMEOUT)
					{
					//this will execute every 1 second
					//you could put a counter in here and if the 
					//driver takes an inordinate ammout of time
					//to complete, you could issue a flush RX command
					//and break out of this loop
					
					//check to see if the other end of the pipe is still there
					if(ConnectNamedPipe(outpipeh,NULL)==0)
						{
						i = GetLastError();
						//printf("connect error-read:%x\r\n",i);
						if((i==ERROR_NO_DATA)||(i==ERROR_BROKEN_PIPE))
							{
							printf("disconnect/reconnect response pipe(timeout)\r\n");
							DisconnectNamedPipe(outpipeh);			//force the pipe closed
							ConnectNamedPipe(outpipeh,NULL);			//wait for a new one
							DeviceIoControl(hdev,IOCTL_ESCCDRV_FLUSH_RX,NULL,0,NULL,0,&i,NULL);//wipe (old) responses in queue (and break out of this wait, (it will return/complete with 0 bytes)
							}
						}

					}
				if(j==WAIT_ABANDONED)
					{
					}
				
				}while((j!=WAIT_OBJECT_0)&&(connected));//stay here until we get signaled or the main thread exits
			}
		}
			
	if(connected)
		{                                                       
		GetOverlappedResult(hdev,&os,&nobytesread,TRUE); //here to get the actual nobytesread!!!
		//if we get here then data holds nobytesread bytes, so send
		//that out the response pipe.
if(nobytesread>0)
{
		if(WriteFile(outpipeh,&data,nobytesread,&nobyteswritten,NULL)==FALSE)
			{
			i = GetLastError();
			//printf("connect error-read:%x\r\n",i);
			if(i==ERROR_BROKEN_PIPE)
				{
				printf("disconnect/reconnect response pipe(write)\r\n");
				DisconnectNamedPipe(outpipeh);			//force the pipe closed
				ConnectNamedPipe(outpipeh,NULL);			//wait for a new one
				DeviceIoControl(hdev,IOCTL_ESCCDRV_FLUSH_RX,NULL,0,NULL,0,&i,NULL);//wipe (old) responses in queue
				}
			}
//		printf("received %u bytes:\n\r",nobytesread);    //display the number of bytes received
//		for(i=0;i<nobytesread;i++)printf("%x:",data[i]&0xff);  //display the buffer
//		printf("::%u\n\r",fcount++);
//		t=TRUE;
}
		}
	}while(connected);              //do until we want to terminate
done:
	DisconnectNamedPipe(outpipeh);			//force the pipe closed
	CloseHandle( os.hEvent ) ;      //done with event
	CloseHandle(outpipeh);			//done with pipe

	printf("exiting read thread\n\r");      //exit messge
	return(TRUE);                   //outta here
	
}
