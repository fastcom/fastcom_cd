//Copyright (c) 1995,1997 Commtech, Inc. Wichtia, KS
// esccdrv.h  -- header file for esccdrv 
//
// The ESCC device driver IOCTLs
//
#define IOCTL_ESCCDRV_SET_FS6131		0x83002140
#define IOCTL_ESCCDRV_IMMEDIATE_STATUS	0x83002100
#define IOCTL_ESCCDRV_WRITE_REGISTER	0x830020fc
#define IOCTL_ESCCDRV_READ_REGISTER     0x830020f8
#define IOCTL_ESCCDRV_SET_CLOCK         0x830020f4
#define IOCTL_ESCCDRV_SET_DTR           0x830020f0
#define IOCTL_ESCCDRV_GET_DSR           0x830020ec
#define IOCTL_ESCCDRV_CMDR				0x830020e8
#define IOCTL_ESCCDRV_FLUSH_TX          0x830020e4
#define IOCTL_ESCCDRV_FLUSH_RX          0x830020e0
#define IOCTL_ESCCDRV_SET_TX_ADD        0x830020dc
#define IOCTL_ESCCDRV_SET_TX_TYPE       0x830020d8
#define IOCTL_ESCCDRV_STOP_TIMER        0x830020d4
#define IOCTL_ESCCDRV_START_TIMER       0x830020d0
#define IOCTL_ESCCDRV_STATUS            0x830020cc
#define IOCTL_ESCCDRV_SETUP				0x830020c8
#define IOCTL_ESCCDRV_RX_READY          0x830020c4
#define IOCTL_ESCCDRV_TX_ACTIVE         0x830020c0



struct setup{
//used in all
unsigned cmdr;
unsigned mode;
unsigned timr;
unsigned xbcl;
unsigned xbch;
unsigned ccr0;
unsigned ccr1;
unsigned ccr2;
unsigned ccr3;
unsigned ccr4;
unsigned tsax;
unsigned tsar;
unsigned xccr;
unsigned rccr;
unsigned bgr;
unsigned iva;
unsigned ipc;
unsigned imr0;
unsigned imr1;
unsigned pvr;
unsigned pim;
unsigned pcr;
//escc register defines for HDLC/SDLC mode
unsigned xad1;
unsigned xad2;
unsigned rah1;
unsigned rah2;
unsigned ral1;
unsigned ral2;
unsigned rlcr;
unsigned aml;
unsigned amh;
unsigned pre;
//escc async register defines (used in bisync as well)
unsigned xon;
unsigned xoff;
unsigned tcr;
unsigned dafo;
unsigned rfc;
unsigned tic;
unsigned mxn;
unsigned mxf;

//escc bisync register defines
unsigned synl;
unsigned synh;

unsigned n_rbufs;
unsigned n_tbufs;
unsigned n_rfsize_max;
unsigned n_tfsize_max;

};



struct clkset{
	ULONG clockbits;
	USHORT numbits;
};

struct regsingle{ //used for ESCC_WRITE_REGISTER ioctl call
	ULONG port;             //offset from base address of register to access
	UCHAR data;             //data to write
};


//device io control STATUS function return values
#define ST_RX_DONE              0x00000001
#define ST_OVF                  0x00000002
#define ST_RFS                  0x00000004
#define ST_RX_TIMEOUT   0x00000008
#define ST_RSC                  0x00000010
#define ST_PERR                 0x00000020
#define ST_PCE                  0x00000040
#define ST_FERR                 0x00000080
#define ST_SYN                  0x00000100
#define ST_DPLLA                0x00000200
#define ST_CDSC                 0x00000400
#define ST_RFO                  0x00000800
#define ST_EOP                  0x00001000
#define ST_BRKD                 0x00002000
#define ST_ONLP                 0x00004000
#define ST_BRKT                 0x00008000
#define ST_ALLS                 0x00010000
#define ST_EXE                  0x00020000
#define ST_TIN                  0x00040000
#define ST_CTSC                 0x00080000
#define ST_XMR                  0x00100000
#define ST_TX_DONE              0x00200000
#define ST_DMA_TC               0x00400000
#define ST_DSR1C                0x00800000
#define ST_DSR0C                0x01000000

//these are also somewhere else in a windows header file.
//
#define STATUS_DEVICE_DOES_NOT_EXIST     0xC00000C0L
#define STATUS_INVALID_PARAMETER         0xC000000DL
#define STATUS_NO_SUCH_DEVICE            0xC000000EL
#define STATUS_CANCELLED                 0xC0000120L

//these are not really necessary unless you need to do something
//special, if so then they are here
//CMDR commands for the ESCC
#define XRES 1
#define XME 2
#define XIF 4
#define HUNT 4
#define XTF 8
#define STIB 16
#define RNR 32
#define RFRD 32
#define RHR 64
#define RMC 128
//STAR codes
#define WFA 1
#define CTS 2
#define CEC 4
#define RLI 8
#define TEC 8
#define RRNR 16
#define SYNC 16
#define FCS 16
#define XRNR 32
#define RFNE 32
#define XFW 64
#define XDOV 128
//ISR0
#define RPF 1
#define RFO 2
#define CDSC 4
#define PLLA 8
#define PCE 16
#define RSC 32
#define RFS 64
#define RME 128
//ISR1
#define XPR 1
#define XMR 2
#define CSC 4
#define TIN 8
#define EXE 16
#define XDU 16
#define ALLS 32
#define AOLP 32
#define RDO 64
#define OLP 64
#define EOP 128

//port addresses
//hdlc defines
#define STAR 0
#define CMDR 0
#define RSTA 1
#define PRE 1
#define MODE 2
#define TIMR 3
#define XAD1 4
#define XAD2 5
#define RAH1 6
#define RAH2 7
#define RAL1 8
#define RHCR 9
#define RAL2 9
#define RBCL 10
#define XBCL 10
#define RBCH 11
#define XBCH 11
#define CCR0 12
#define CCR1 13
#define CCR2 14
#define CCR3 15
#define TSAX 16
#define TSAR 17
#define XCCR 18
#define RCCR 19
#define VSTR 20
#define BGR 20
#define RLCR 21
#define AML 22
#define AMH 23
#define GIS 24
#define IVA 24
#define IPC 25
#define ISR0 26
#define IMR0 26
#define ISR1 27
#define IMR1 27
#define PVR 28
#define PIS 29
#define PIM 29
#define PCR 30
#define CCR4 31
#define FIFO 32
//async defines
#define XON 4
#define XOFF 5
#define TCR 6
#define DAFO 7
#define RFC 8
#define TIC 21
#define MXN 22
#define MXF 23
//bisync defines
#define SYNL 4
#define SYNH 5

//mode register defines
#define AUTO_MODE					0x00
#define NON_AUTO_MODE				0x40
#define TRANSPARENT_MODE			0x80
#define EXTENDED_TRANSPARENT_MODE	0xc0

#define ADDRESS_MODE_0				0x00
#define ADDRESS_MODE_1				0x20

#define TIMER_INTERNAL				0x10
#define TIMER_EXTERNAL				0x00

#define RECEIVER_ACTIVE				0x08
#define RECEIVER_INACTIVE			0x00

#define RTS_ACTIVE					0x04
#define RTS_AUTO					0x00

#define TIMER_HIRES					0x02
#define TIMER_LORES					0x00

#define TESTLOOP_ON					0x01
#define TESTLOOP_OFF				0x00

//ccr0 defines
#define POWERUP						0x80
#define POWERDOWN					0x00

#define MASTERCLOCK_ON				0x40
#define MASTERCLOCK_OFF				0x00

#define NRZ							0x00
#define NRZI						0x08
#define FM0							0x10
#define FM1							0x14
#define MANCHESTER					0x18

#define HDLC						0x00
#define SDLCLOOP					0x01
#define BISYNC						0x02
#define ASYNC						0x03

//ccr1 defines
#define CLOCKMODE0					0x10
#define CLOCKMODE1					0x11
#define CLOCKMODE2					0x12
#define CLOCKMODE3					0x13
#define CLOCKMODE4					0x14
#define CLOCKMODE5					0x15
#define CLOCKMODE6					0x16
#define CLOCKMODE7					0x17
#define TIMEFILL_FLAG				0x08
#define TIMEFILL_HIGH				0x00

//ccr2 defines
#define BDF_0						0x00
#define BDF_1						0x20

#define SSEL_0						0x00
#define SSEL_1						0x10

#define TOE_0						0x00
#define TOE_1						0x08

#define CCITT						0x00
#define CRC32						0x02

#define NONINVERTED					0x00
#define INVERTED					0x01

//ccr3 defines
#define PREAMBLE_0					0x00
#define PREAMBLE_1					0x20
#define PREAMBLE_2					0x60
#define PREAMBLE_4					0xA0
#define PREAMBLE_8					0xE0

#define SAVE_RADD					0x10

#define CRCRESET_FFFF				0x00
#define CRCRESET_0000				0x08

#define SAVE_CRC					0x04
#define SEND_CRC					0x02
#define DPLL_EXTEND					0x01

//ccr4 defines
#define MASTER_CLOCK_DIVIDE_BY_FOUR	0x80
#define EXTENDED_BGR				0x40
#define CD_INVERT					0x10
//do not use other than 32 unless you have a special driver
//the stock driver assumes that a rpf will generate 32 bytes (in HDLC modes).
#define THRESHOLD_32				0x00
#define THRESHOLD_16				0x01
#define THRESHOLD_4					0x02
#define THRESHOLD_2					0x03


//end of escc header file
