/*
Copyright (C) 1999 Commtech, Inc. Wichita, KS.

Module name:
  
	responsepump.c

Abstract:

	Win32 console program that demonstrates how to connect to the named
	pipe created using the cr.c /cr.exe example program.
	essentially opens the \\.\pipe\responsepipe named pipe
	and displays messages that are received.

Environment:

    user mode only, compiled using Visual C++ ver 6.0

Notes:

    
Revision History:

    6/11/1999             started

*/
#include "windows.h"
#include "stdio.h"
#include "conio.h"


void main()
{
HANDLE	responsepipeh;	//handle for named pipe
char	response[256];	//char array to hold data from pipe
DWORD	i;				//temp var
DWORD	bytesread;		//holds number of bytes returned from pipe
DWORD	rframecount;	//frame counter

rframecount=1;			//start frame count at 1
//create the named pipe.
responsepipeh = CreateFile("\\\\.\\pipe\\responsepipe",GENERIC_READ,0,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
if(responsepipeh==INVALID_HANDLE_VALUE)
	{
	printf("cannot open response pipe\r\n");
	exit(1);
	}
//main loop, read from pipe and exit on keypress
do
	{
	//read a message from the pipe
	if(ReadFile(responsepipeh,&response,8,&bytesread,NULL))
		{
		//print the data received to the screen with teh frame count
		for(i=0;i<bytesread;i++) printf("%c",response[i]&0xff);
		printf(":%u\r\n",rframecount++);
		}
	else
		{
		//error reading from the pipe
		printf("error in read from pipe\r\n");
		}
	}while(!kbhit());//keep doing until the user hits a key
getch();//clear the key
CloseHandle(responsepipeh);//done with the pipe
//done
}