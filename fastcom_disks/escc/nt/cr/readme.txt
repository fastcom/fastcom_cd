cr\cr.c  command/response multithreaded program that interacts through named pipes and uses the ESCC device.
cp\commandpump.c  opens named pipe and sends commands.
rp\responsepump.c opens named pipe and displays (receives) responses.

This example is being included because it has an interesting feature, 
sending frames at a timed interval.  The method that
is used is much more consistant than doing something like:
do
{
WriteFile(hDevice,...);
Sleep(10);
}

If you look at the output that is generated using the above loop, you will notice
that the frames do not consistantly go out one every 10mS.  The reson for this
is due to the way that NT timeslices.  NT operates on a 10mS timeslice, so 
using Sleep() for values less than 50-100mS will yield less than reliable timing.
As a Sleep(0) just gives up the current timeslice, and Sleep(1-10) will be
dependent on when the other threads in the system give up their timeslices 
(and of course the prioritys of the threads).  The value that you give to 
Sleep is just the ammount of time that the scheduler waits before trying
to reschedule the thread, it is not a "this thread will run again in" kind of thing.
But once it is up for execution it must compete with the rest of the "runnable"
threads in the system.  The more threads you have running the less predictable 
small Sleep values become.


The method used to generate the consistant 10mS frame output of this example
is to use the timer resource of the 82532 in such a way as to generate an interrupt
at the period that we would like to transmit.
The 82532 is set up in master clock mode (because the timer runs on the master clock)
the inclk is set to 1.2288MHz).  You can use non masterclock mode but you must
keep in mind that the timer rate will be connected to the bitrate 
(as the timer clock source is the transmit clock source in non master clock mode).
The timer is then set to 0xF7 which corresponds to a periodic timer expriring every 10mS:
t1 = 512 * (23+1)*1/1.2288E6 = .01 S
Once this timer is started (with the IOCTL_ESCCDRV_START_TIMER) the timer
will expire and send notification via the status IOCTL as ST_TIN, and will
continue to do so every 10mS until the escc is reset or the stop timer command is issued.
To get the output we just block on a status ioctl until a ST_TIN arrives, then
when we get the ST_TIN the status thread unblocks and we can transmit a frame
using WriteFile().
If you want to get fancy you can up the priority of the status thread, such that
when it gets unblocked it has priority to execute.  This may help in systems
that have a large number of "standard" priority threads running.

This concept works well until you get down into the single milisecond range.
At that point, the time it takes to enter and leave the driver will be large enough 
in comparison to the timout value that the consistancy will break down. 
(In other words a ST_TIN will occur before the WriteFile call is finished executing,
thus the status function never blocks, and the interval will be not as constant).
You can extend the concept to the sub milisecond level, but you must execute
the frame writes at the driver level (in the interrupt service routine at the point
where the ST_TIN originates, thus bypassing the transitions from user to kernel mode).
Unfortunately to do this requires that you know what is to be sent when the ST_TIN
occurs(hard coded transmission from the driver), or that you can transfer more
than one frame to the drivers transmit queue at a time (something like a WriteFile
that transfers multiple frames to the driver at a time (which does not currently exist)), 
For if you are transfering one frame at a time to the driver you will incur 
the driver entry/exit penalty for each frame, and be right back where you 
started in user mode.
As a general rule as PC's become faster the driver entry/exit penalty will go 
down, allowing this kind of timing to extend further into the milisecond/submilisecond range
before breaking down.
for example on a 100MHz K5 system the driver entry/exit time is about 380uS (ranges from 330-600uS)
on a Dual Pentium Pro 200MHz system that time is reduced to about 180uS (ranges from 73 to 180uS).
Eventually driver entry/exit time will not be as much of a concern as interrupt latency,
which will produce the limit of this technique.

If you use this technique to time the sending of frames, make sure that the 
data that you are sending will be completely sent in the interval that you 
are using. 
(#bytes*bitsperbyte)/bitrate < interval

For the cr program it is transmitting async data as Odd,8,1.  This
corresponds to 11 bits per byte and (6*11)/76800 = 0.9mS < 10mS.

The transmitter in this case should be finished transmitting well before
the next frame is to go out.
If you are using HDLC data make sure and add in the time for the flags, CRC bytes,
and possibly address bytes.



cg
11/1/99

