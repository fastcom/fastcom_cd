// setdlg.cpp : implementation file
//

#include "stdafx.h"
#include "esccpmfc.h"
#include "setdlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Csetdlg dialog


Csetdlg::Csetdlg(CWnd* pParent /*=NULL*/)
	: CDialog(Csetdlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(Csetdlg)
	m_bgr = _T("");
	m_ccr0 = _T("");
	m_ccr1 = _T("");
	m_ccr2 = _T("");
	m_ccr3 = _T("");
	m_dafo = _T("");
	m_imr0 = _T("");
	m_imr1 = _T("");
	m_ipc = _T("");
	m_iva = _T("");
	m_mode = _T("");
	m_pcr = _T("");
	m_pim = _T("");
	m_pre = _T("");
	m_pvr = _T("");
	m_rah1 = _T("");
	m_rah2 = _T("");
	m_ral1 = _T("");
	m_ral2 = _T("");
	m_rbufs = _T("");
	m_rlcr = _T("");
	m_synl = _T("");
	m_synh = _T("");
	m_tbufs = _T("");
	m_tcr = _T("");
	m_timr = _T("");
	m_xad1 = _T("");
	m_xad2 = _T("");
	m_xbch = _T("");
	m_xbcl = _T("");
	m_type = -1;
	m_rfc = _T("");
	m_ccr4 = _T("");
	m_amh = _T("");
	m_aml = _T("");
	m_mxf = _T("");
	m_mxn = _T("");
	m_rccr = _T("");
	m_tsar = _T("");
	m_tsax = _T("");
	m_xccr = _T("");
	m_tbufsize = _T("");
	m_rbufsize = _T("");
	m_nrbufs = _T("");
	m_ntbufs = _T("");
	//}}AFX_DATA_INIT
}


void Csetdlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Csetdlg)
	DDX_Text(pDX, IDC_BGR, m_bgr);
	DDX_Text(pDX, IDC_CCR0, m_ccr0);
	DDX_Text(pDX, IDC_CCR1, m_ccr1);
	DDX_Text(pDX, IDC_CCR2, m_ccr2);
	DDX_Text(pDX, IDC_CCR3, m_ccr3);
	DDX_Text(pDX, IDC_DAFO, m_dafo);
	DDX_Text(pDX, IDC_IMR0, m_imr0);
	DDX_Text(pDX, IDC_IMR1, m_imr1);
	DDX_Text(pDX, IDC_IPC, m_ipc);
	DDX_Text(pDX, IDC_IVA, m_iva);
	DDX_Text(pDX, IDC_MODE, m_mode);
	DDX_Text(pDX, IDC_PCR, m_pcr);
	DDX_Text(pDX, IDC_PIM, m_pim);
	DDX_Text(pDX, IDC_PRE, m_pre);
	DDX_Text(pDX, IDC_PVR, m_pvr);
	DDX_Text(pDX, IDC_RAH1, m_rah1);
	DDX_Text(pDX, IDC_RAH2, m_rah2);
	DDX_Text(pDX, IDC_RAL1, m_ral1);
	DDX_Text(pDX, IDC_RAL2, m_ral2);
	DDX_Text(pDX, IDC_RLCR, m_rlcr);
	DDX_Text(pDX, IDC_SYNH, m_synl);
	DDX_Text(pDX, IDC_SYNL, m_synh);
	DDX_Text(pDX, IDC_TCR, m_tcr);
	DDX_Text(pDX, IDC_TIMR, m_timr);
	DDX_Text(pDX, IDC_XAD1, m_xad1);
	DDX_Text(pDX, IDC_XAD2, m_xad2);
	DDX_Text(pDX, IDC_XBCH, m_xbch);
	DDX_Text(pDX, IDC_XBCL, m_xbcl);
	DDX_Radio(pDX, IDC_HDLC, m_type);
	DDX_Text(pDX, IDC_RFC, m_rfc);
	DDX_Text(pDX, IDC_CCR4, m_ccr4);
	DDX_Text(pDX, IDC_AMH, m_amh);
	DDX_Text(pDX, IDC_AML, m_aml);
	DDX_Text(pDX, IDC_MXF, m_mxf);
	DDX_Text(pDX, IDC_MXN, m_mxn);
	DDX_Text(pDX, IDC_RCCR, m_rccr);
	DDX_Text(pDX, IDC_TSAR, m_tsar);
	DDX_Text(pDX, IDC_TSAX, m_tsax);
	DDX_Text(pDX, IDC_XCCR, m_xccr);
	DDX_Text(pDX, IDC_TBUFSIZE, m_tbufsize);
	DDX_Text(pDX, IDC_RBUFSIZE, m_rbufsize);
	DDX_Text(pDX, IDC_NRBUFS, m_nrbufs);
	DDX_Text(pDX, IDC_NTBUFS, m_ntbufs);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Csetdlg, CDialog)
	//{{AFX_MSG_MAP(Csetdlg)
	ON_BN_CLICKED(IDC_ASYNC, OnAsync)
	ON_BN_CLICKED(IDC_BISYNC, OnBisync)
	ON_BN_CLICKED(IDC_HDLC, OnHdlc)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Csetdlg message handlers

BOOL Csetdlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	//start with hdlc setup parameters

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void Csetdlg::OnAsync() 
{
	// TODO: Add your control notification handler code here
char buf[80];

sprintf(buf,"%x",0x08);
SetDlgItemText(IDC_MODE,buf);
sprintf(buf,"%x",0x1f);
SetDlgItemText(IDC_TIMR,buf);
sprintf(buf,"%x",0x00);
SetDlgItemText(IDC_XBCH,buf);
sprintf(buf,"%x",0x00);
SetDlgItemText(IDC_XBCL,buf);
sprintf(buf,"%x",0x83);
SetDlgItemText(IDC_CCR0,buf);
sprintf(buf,"%x",0x1f);
SetDlgItemText(IDC_CCR1,buf);
sprintf(buf,"%x",0x38);
SetDlgItemText(IDC_CCR2,buf);
sprintf(buf,"%x",0x3b);
SetDlgItemText(IDC_BGR,buf);
sprintf(buf,"%x",0x00);
SetDlgItemText(IDC_IVA,buf);
sprintf(buf,"%x",0x03);
SetDlgItemText(IDC_IPC,buf);
sprintf(buf,"%x",0x04);
SetDlgItemText(IDC_IMR0,buf);
sprintf(buf,"%x",0x00);
SetDlgItemText(IDC_IMR1,buf);
sprintf(buf,"%x",0x00);
SetDlgItemText(IDC_PVR,buf);
sprintf(buf,"%x",0xFF);
SetDlgItemText(IDC_PIM,buf);
sprintf(buf,"%x",0xe0);
SetDlgItemText(IDC_PCR,buf);
sprintf(buf,"%x",0x00);
SetDlgItemText(IDC_TCR,buf);
sprintf(buf,"%x",0x00);
SetDlgItemText(IDC_DAFO,buf);
sprintf(buf,"%x",0x0c);
SetDlgItemText(IDC_RFC,buf);
sprintf(buf,"");
SetDlgItemText(IDC_SYNL,buf);
sprintf(buf,"");
SetDlgItemText(IDC_SYNH,buf);

}

void Csetdlg::OnBisync() 
{
	// TODO: Add your control notification handler code here
char buf[80];
sprintf(buf,"%x",0x3c);
SetDlgItemText(IDC_MODE,buf);
sprintf(buf,"%x",0x1f);
SetDlgItemText(IDC_TIMR,buf);
sprintf(buf,"%x",0x00);
SetDlgItemText(IDC_SYNL,buf);
sprintf(buf,"%x",0x5e);
SetDlgItemText(IDC_SYNH,buf);
sprintf(buf,"%x",0xff);
SetDlgItemText(IDC_TCR,buf);
sprintf(buf,"%x",0x00);
SetDlgItemText(IDC_DAFO,buf);
sprintf(buf,"%x",0x4d);
SetDlgItemText(IDC_RFC,buf);
sprintf(buf,"%x",0x82);
SetDlgItemText(IDC_CCR0,buf);
sprintf(buf,"%x",0x16);
SetDlgItemText(IDC_CCR1,buf);
sprintf(buf,"%x",0x38);
SetDlgItemText(IDC_CCR2,buf);
sprintf(buf,"%x",0x00);
SetDlgItemText(IDC_CCR3,buf);
sprintf(buf,"%x",0x00);
SetDlgItemText(IDC_BGR,buf);
sprintf(buf,"%x",0x00);
SetDlgItemText(IDC_PRE,buf);
sprintf(buf,"%x",0x00);
SetDlgItemText(IDC_IVA,buf);
sprintf(buf,"%x",0x03);
SetDlgItemText(IDC_IPC,buf);
sprintf(buf,"%x",0x04);
SetDlgItemText(IDC_IMR0,buf);
sprintf(buf,"%x",0x00);
SetDlgItemText(IDC_IMR1,buf);
sprintf(buf,"%x",0x00);
SetDlgItemText(IDC_PVR,buf);
sprintf(buf,"%x",0xff);
SetDlgItemText(IDC_PIM,buf);
sprintf(buf,"%x",0xe0);
SetDlgItemText(IDC_PCR,buf);
sprintf(buf,"%x",0x00);
SetDlgItemText(IDC_XBCH,buf);
}

void Csetdlg::OnHdlc() 
{
	// TODO: Add your control notification handler code here
char buf[80];

sprintf(buf,"%x",0x88);
SetDlgItemText(IDC_MODE,buf);
sprintf(buf,"%x",0x1f);
SetDlgItemText(IDC_TIMR,buf);
sprintf(buf,"%x",0x00);
SetDlgItemText(IDC_XBCL,buf);
sprintf(buf,"%x",0x00);
SetDlgItemText(IDC_XBCH,buf);
sprintf(buf,"%x",0x80);
SetDlgItemText(IDC_CCR0,buf);
sprintf(buf,"%x",0x16);
SetDlgItemText(IDC_CCR1,buf);
sprintf(buf,"%x",0x38);
SetDlgItemText(IDC_CCR2,buf);
sprintf(buf,"%x",0x00);
SetDlgItemText(IDC_CCR3,buf);
sprintf(buf,"%x",0x00);
SetDlgItemText(IDC_BGR,buf);
sprintf(buf,"%x",0x00);
SetDlgItemText(IDC_IVA,buf);
sprintf(buf,"%x",0x03);
SetDlgItemText(IDC_IPC,buf);
sprintf(buf,"%x",0x04);
SetDlgItemText(IDC_IMR0,buf);
sprintf(buf,"%x",0x00);
SetDlgItemText(IDC_IMR1,buf);
sprintf(buf,"%x",0x00);
SetDlgItemText(IDC_PVR,buf);
sprintf(buf,"%x",0xff);
SetDlgItemText(IDC_PIM,buf);
sprintf(buf,"%x",0xe0);
SetDlgItemText(IDC_PCR,buf);
sprintf(buf,"%x",0xff);
SetDlgItemText(IDC_XAD1,buf);
sprintf(buf,"%x",0xff);
SetDlgItemText(IDC_XAD2,buf);
sprintf(buf,"%x",0xff);
SetDlgItemText(IDC_RAH1,buf);
sprintf(buf,"%x",0xff);
SetDlgItemText(IDC_RAH2,buf);
sprintf(buf,"%x",0xff);
SetDlgItemText(IDC_RAL1,buf);
sprintf(buf,"%x",0xff);
SetDlgItemText(IDC_RAL2,buf);
sprintf(buf,"%x",0x00);
SetDlgItemText(IDC_RLCR,buf);
sprintf(buf,"%x",0x00);
SetDlgItemText(IDC_PRE,buf);
sprintf(buf,"%x",0x00);
SetDlgItemText(IDC_CCR4,buf);

sprintf(buf,"");
SetDlgItemText(IDC_SYNL,buf);
sprintf(buf,"");
SetDlgItemText(IDC_SYNH,buf);
sprintf(buf,"");
SetDlgItemText(IDC_TCR,buf);
sprintf(buf,"");
SetDlgItemText(IDC_DAFO,buf);
sprintf(buf,"");
SetDlgItemText(IDC_RFC,buf);


}
