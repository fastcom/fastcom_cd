//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by esccpmfc.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_ESCCPMTYPE                  129
#define IDD_DEVNO                       130
#define IDD_SETDLG                      131
#define IDD_ASYNCSET                    132
#define IDD_CLOCKSET                    134
#define IDD_DSRDTR                      135
#define IDD_GETSETREG                   136
#define IDD_TXSETTYPE                   138
#define IDD_ADDRESS                     139
#define IDD_GETCMD                      140
#define IDC_EDIT1                       1000
#define IDC_MODE                        1000
#define IDC_TIMR                        1001
#define IDC_NUMBITS                     1001
#define IDC_XBCL                        1002
#define IDC_CLOCKBITS                   1002
#define IDC_XBCH                        1003
#define IDC_DTR                         1003
#define IDC_CCR0                        1004
#define IDC_DSR                         1004
#define IDC_CCR1                        1005
#define IDC_REGS                        1005
#define IDC_CCR2                        1006
#define IDC_REGVAL                      1006
#define IDC_CCR3                        1007
#define IDC_XTF                         1007
#define IDC_BGR                         1008
#define IDC_XIF                         1008
#define IDC_IVA                         1009
#define IDC_IPC                         1010
#define IDC_IMR0                        1011
#define IDC_IMR1                        1012
#define IDC_PVR                         1013
#define IDC_PIM                         1014
#define IDC_PCR                         1015
#define IDC_XAD1                        1016
#define IDC_XAD2                        1017
#define IDC_RAH1                        1018
#define IDC_RAL1                        1019
#define IDC_RAH2                        1020
#define IDC_RAL2                        1021
#define IDC_RLCR                        1022
#define IDC_PRE                         1023
#define IDC_TCR                         1024
#define IDC_DAFO                        1025
#define IDC_RFC                         1026
#define IDC_SYNL                        1027
#define IDC_SYNH                        1028
#define IDC_HDLC                        1029
#define IDC_BISYNC                      1030
#define IDC_ASYNC                       1031
#define IDC_TSAX                        1032
#define IDC_TSAR                        1033
#define IDC_XCCR                        1034
#define IDC_CCR4                        1035
#define IDC_PE                          1036
#define IDC_RCCR                        1036
#define IDC_BAUDS                       1037
#define IDC_AML                         1037
#define IDC_PO                          1038
#define IDC_AMH                         1038
#define IDC_PN                          1039
#define IDC_MXN                         1039
#define IDC_PM                          1040
#define IDC_MXF                         1040
#define IDC_PS                          1041
#define IDC_NRBUFS                      1041
#define IDC_B5                          1042
#define IDC_RBUFSIZE                    1042
#define IDC_B6                          1043
#define IDC_NTBUFS                      1043
#define IDC_B7                          1044
#define IDC_TBUFSIZE                    1044
#define IDC_8                           1045
#define IDC_ST1                         1046
#define IDC_ST2                         1047
#define IDC_PARITYSAVE                  1048
#define IDM_ADDESCC                     32771
#define IDM_CHANNELSET                  32772
#define IDM_DISCONNECT                  32773
#define IDM_SETCLOCK                    32774
#define IDM_DTRDSR                      32775
#define IDM_WRREG                       32777
#define IDM_RDREG                       32778
#define IDM_WRCMD                       32779
#define IDM_FLUSHTX                     32780
#define IDM_FLUSHRX                     32781
#define IDM_SETTXADD                    32782
#define IDM_SETRXADD                    32783
#define IDM_STARTTIME                   32784
#define IDM_STOPTIME                    32785
#define IDM_SETTXTYPE                   32786

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        141
#define _APS_NEXT_COMMAND_VALUE         32787
#define _APS_NEXT_CONTROL_VALUE         1043
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
