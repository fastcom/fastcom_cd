# Microsoft Developer Studio Generated NMAKE File, Format Version 4.20
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

!IF "$(CFG)" == ""
CFG=esccpmfc - Win32 Debug
!MESSAGE No configuration specified.  Defaulting to esccpmfc - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "esccpmfc - Win32 Release" && "$(CFG)" !=\
 "esccpmfc - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE on this makefile
!MESSAGE by defining the macro CFG on the command line.  For example:
!MESSAGE 
!MESSAGE NMAKE /f "esccpmfc.mak" CFG="esccpmfc - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "esccpmfc - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "esccpmfc - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 
################################################################################
# Begin Project
# PROP Target_Last_Scanned "esccpmfc - Win32 Release"
CPP=cl.exe
RSC=rc.exe
MTL=mktyplib.exe

!IF  "$(CFG)" == "esccpmfc - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "temp"
# PROP Target_Dir ""
OUTDIR=.\Release
INTDIR=.\temp

ALL : "$(OUTDIR)\esccpmfc.exe" ".\temp\esccpmfc.pch"

CLEAN : 
	-@erase "$(INTDIR)\ChildFrm.obj"
	-@erase "$(INTDIR)\ClockSet.obj"
	-@erase "$(INTDIR)\Devno.obj"
	-@erase "$(INTDIR)\dtrdsr.obj"
	-@erase "$(INTDIR)\esccpmfc.obj"
	-@erase "$(INTDIR)\esccpmfc.pch"
	-@erase "$(INTDIR)\esccpmfc.res"
	-@erase "$(INTDIR)\esccpmfcDoc.obj"
	-@erase "$(INTDIR)\esccpmfcView.obj"
	-@erase "$(INTDIR)\GetAdd.obj"
	-@erase "$(INTDIR)\GetCmd.obj"
	-@erase "$(INTDIR)\GetSetReg.obj"
	-@erase "$(INTDIR)\MainFrm.obj"
	-@erase "$(INTDIR)\setdlg.obj"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\TXsettype.obj"
	-@erase "$(OUTDIR)\esccpmfc.exe"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /c
# ADD CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /c
# SUBTRACT CPP /YX /Yc /Yu
CPP_PROJ=/nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D\
 "_MBCS" /Fo"$(INTDIR)/" /c 
CPP_OBJS=.\temp/
CPP_SBRS=.\.
# ADD BASE MTL /nologo /D "NDEBUG" /win32
# ADD MTL /nologo /D "NDEBUG" /win32
MTL_PROJ=/nologo /D "NDEBUG" /win32 
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG"
RSC_PROJ=/l 0x409 /fo"$(INTDIR)/esccpmfc.res" /d "NDEBUG" 
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/esccpmfc.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 /nologo /subsystem:windows /machine:I386
LINK32_FLAGS=/nologo /subsystem:windows /incremental:no\
 /pdb:"$(OUTDIR)/esccpmfc.pdb" /machine:I386 /out:"$(OUTDIR)/esccpmfc.exe" 
LINK32_OBJS= \
	"$(INTDIR)\ChildFrm.obj" \
	"$(INTDIR)\ClockSet.obj" \
	"$(INTDIR)\Devno.obj" \
	"$(INTDIR)\dtrdsr.obj" \
	"$(INTDIR)\esccpmfc.obj" \
	"$(INTDIR)\esccpmfc.res" \
	"$(INTDIR)\esccpmfcDoc.obj" \
	"$(INTDIR)\esccpmfcView.obj" \
	"$(INTDIR)\GetAdd.obj" \
	"$(INTDIR)\GetCmd.obj" \
	"$(INTDIR)\GetSetReg.obj" \
	"$(INTDIR)\MainFrm.obj" \
	"$(INTDIR)\setdlg.obj" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\TXsettype.obj"

"$(OUTDIR)\esccpmfc.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "esccpmfc - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "temp"
# PROP Target_Dir ""
OUTDIR=.\Debug
INTDIR=.\temp

ALL : "$(OUTDIR)\esccpmfc.exe" ".\temp\esccpmfc.pch"

CLEAN : 
	-@erase "$(INTDIR)\ChildFrm.obj"
	-@erase "$(INTDIR)\ClockSet.obj"
	-@erase "$(INTDIR)\Devno.obj"
	-@erase "$(INTDIR)\dtrdsr.obj"
	-@erase "$(INTDIR)\esccpmfc.obj"
	-@erase "$(INTDIR)\esccpmfc.pch"
	-@erase "$(INTDIR)\esccpmfc.res"
	-@erase "$(INTDIR)\esccpmfcDoc.obj"
	-@erase "$(INTDIR)\esccpmfcView.obj"
	-@erase "$(INTDIR)\GetAdd.obj"
	-@erase "$(INTDIR)\GetCmd.obj"
	-@erase "$(INTDIR)\GetSetReg.obj"
	-@erase "$(INTDIR)\MainFrm.obj"
	-@erase "$(INTDIR)\setdlg.obj"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\TXsettype.obj"
	-@erase "$(INTDIR)\vc40.idb"
	-@erase "$(INTDIR)\vc40.pdb"
	-@erase "$(OUTDIR)\esccpmfc.exe"
	-@erase "$(OUTDIR)\esccpmfc.ilk"
	-@erase "$(OUTDIR)\esccpmfc.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /c
# SUBTRACT CPP /YX /Yc /Yu
CPP_PROJ=/nologo /MTd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS"\
 /D "_MBCS" /Fo"$(INTDIR)/" /Fd"$(INTDIR)/" /c 
CPP_OBJS=.\temp/
CPP_SBRS=.\.
# ADD BASE MTL /nologo /D "_DEBUG" /win32
# ADD MTL /nologo /D "_DEBUG" /win32
MTL_PROJ=/nologo /D "_DEBUG" /win32 
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG"
RSC_PROJ=/l 0x409 /fo"$(INTDIR)/esccpmfc.res" /d "_DEBUG" 
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/esccpmfc.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386
LINK32_FLAGS=/nologo /subsystem:windows /incremental:yes\
 /pdb:"$(OUTDIR)/esccpmfc.pdb" /debug /machine:I386\
 /out:"$(OUTDIR)/esccpmfc.exe" 
LINK32_OBJS= \
	"$(INTDIR)\ChildFrm.obj" \
	"$(INTDIR)\ClockSet.obj" \
	"$(INTDIR)\Devno.obj" \
	"$(INTDIR)\dtrdsr.obj" \
	"$(INTDIR)\esccpmfc.obj" \
	"$(INTDIR)\esccpmfc.res" \
	"$(INTDIR)\esccpmfcDoc.obj" \
	"$(INTDIR)\esccpmfcView.obj" \
	"$(INTDIR)\GetAdd.obj" \
	"$(INTDIR)\GetCmd.obj" \
	"$(INTDIR)\GetSetReg.obj" \
	"$(INTDIR)\MainFrm.obj" \
	"$(INTDIR)\setdlg.obj" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\TXsettype.obj"

"$(OUTDIR)\esccpmfc.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(CPP_OBJS)}.obj:
   $(CPP) $(CPP_PROJ) $<  

.cpp{$(CPP_OBJS)}.obj:
   $(CPP) $(CPP_PROJ) $<  

.cxx{$(CPP_OBJS)}.obj:
   $(CPP) $(CPP_PROJ) $<  

.c{$(CPP_SBRS)}.sbr:
   $(CPP) $(CPP_PROJ) $<  

.cpp{$(CPP_SBRS)}.sbr:
   $(CPP) $(CPP_PROJ) $<  

.cxx{$(CPP_SBRS)}.sbr:
   $(CPP) $(CPP_PROJ) $<  

################################################################################
# Begin Target

# Name "esccpmfc - Win32 Release"
# Name "esccpmfc - Win32 Debug"

!IF  "$(CFG)" == "esccpmfc - Win32 Release"

!ELSEIF  "$(CFG)" == "esccpmfc - Win32 Debug"

!ENDIF 

################################################################################
# Begin Source File

SOURCE=.\ReadMe.txt

!IF  "$(CFG)" == "esccpmfc - Win32 Release"

!ELSEIF  "$(CFG)" == "esccpmfc - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\esccpmfc.cpp
DEP_CPP_ESCCP=\
	".\ChildFrm.h"\
	".\esccpmfc.h"\
	".\esccpmfcDoc.h"\
	".\esccpmfcView.h"\
	".\escctest.h"\
	".\MainFrm.h"\
	".\StdAfx.h"\
	

"$(INTDIR)\esccpmfc.obj" : $(SOURCE) $(DEP_CPP_ESCCP) "$(INTDIR)"\
 ".\Release\esccpmfc.pch"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\StdAfx.cpp
DEP_CPP_STDAF=\
	".\StdAfx.h"\
	

!IF  "$(CFG)" == "esccpmfc - Win32 Release"

# ADD CPP /Yc"stdafx.h"

BuildCmds= \
	$(CPP) /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS"\
 /Fp"$(INTDIR)/esccpmfc.pch" /Yc"stdafx.h" /Fo"$(INTDIR)/" /c $(SOURCE) \
	

"$(INTDIR)\StdAfx.obj" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"
   $(BuildCmds)

"$(INTDIR)\esccpmfc.pch" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"
   $(BuildCmds)

!ELSEIF  "$(CFG)" == "esccpmfc - Win32 Debug"

# ADD CPP /Yc"stdafx.h"

BuildCmds= \
	$(CPP) /nologo /MTd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS"\
 /D "_MBCS" /Fp"$(INTDIR)/esccpmfc.pch" /Yc"stdafx.h" /Fo"$(INTDIR)/"\
 /Fd"$(INTDIR)/" /c $(SOURCE) \
	

"$(INTDIR)\StdAfx.obj" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"
   $(BuildCmds)

"$(INTDIR)\esccpmfc.pch" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"
   $(BuildCmds)

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\MainFrm.cpp
DEP_CPP_MAINF=\
	".\esccpmfc.h"\
	".\MainFrm.h"\
	".\StdAfx.h"\
	

"$(INTDIR)\MainFrm.obj" : $(SOURCE) $(DEP_CPP_MAINF) "$(INTDIR)"\
 ".\Release\esccpmfc.pch"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\ChildFrm.cpp
DEP_CPP_CHILD=\
	".\ChildFrm.h"\
	".\esccpmfc.h"\
	".\StdAfx.h"\
	

"$(INTDIR)\ChildFrm.obj" : $(SOURCE) $(DEP_CPP_CHILD) "$(INTDIR)"\
 ".\Release\esccpmfc.pch"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\esccpmfcDoc.cpp
DEP_CPP_ESCCPM=\
	".\esccpmfc.h"\
	".\esccpmfcDoc.h"\
	".\escctest.h"\
	".\StdAfx.h"\
	

"$(INTDIR)\esccpmfcDoc.obj" : $(SOURCE) $(DEP_CPP_ESCCPM) "$(INTDIR)"\
 ".\Release\esccpmfc.pch"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\esccpmfcView.cpp
DEP_CPP_ESCCPMF=\
	".\ClockSet.h"\
	".\Devno.h"\
	".\dtrdsr.h"\
	".\esccpmfc.h"\
	".\esccpmfcDoc.h"\
	".\esccpmfcView.h"\
	".\escctest.h"\
	".\GetAdd.h"\
	".\GetCmd.h"\
	".\GetSetReg.h"\
	".\setdlg.h"\
	".\StdAfx.h"\
	".\TXsettype.h"\
	

"$(INTDIR)\esccpmfcView.obj" : $(SOURCE) $(DEP_CPP_ESCCPMF) "$(INTDIR)"\
 ".\Release\esccpmfc.pch"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\esccpmfc.rc
DEP_RSC_ESCCPMFC=\
	".\res\esccpmfc.ico"\
	".\res\esccpmfc.rc2"\
	".\res\esccpmfcDoc.ico"\
	

"$(INTDIR)\esccpmfc.res" : $(SOURCE) $(DEP_RSC_ESCCPMFC) "$(INTDIR)"
   $(RSC) $(RSC_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\Devno.cpp
DEP_CPP_DEVNO=\
	".\Devno.h"\
	".\esccpmfc.h"\
	".\StdAfx.h"\
	

"$(INTDIR)\Devno.obj" : $(SOURCE) $(DEP_CPP_DEVNO) "$(INTDIR)"\
 ".\Release\esccpmfc.pch"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\setdlg.cpp
DEP_CPP_SETDL=\
	".\esccpmfc.h"\
	".\setdlg.h"\
	".\StdAfx.h"\
	

"$(INTDIR)\setdlg.obj" : $(SOURCE) $(DEP_CPP_SETDL) "$(INTDIR)"\
 ".\Release\esccpmfc.pch"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\ClockSet.cpp
DEP_CPP_CLOCK=\
	".\ClockSet.h"\
	".\esccpmfc.h"\
	".\StdAfx.h"\
	

"$(INTDIR)\ClockSet.obj" : $(SOURCE) $(DEP_CPP_CLOCK) "$(INTDIR)"\
 ".\Release\esccpmfc.pch"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\dtrdsr.cpp
DEP_CPP_DTRDS=\
	".\dtrdsr.h"\
	".\esccpmfc.h"\
	".\StdAfx.h"\
	

"$(INTDIR)\dtrdsr.obj" : $(SOURCE) $(DEP_CPP_DTRDS) "$(INTDIR)"\
 ".\Release\esccpmfc.pch"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\GetSetReg.cpp
DEP_CPP_GETSE=\
	".\esccpmfc.h"\
	".\GetSetReg.h"\
	".\StdAfx.h"\
	

"$(INTDIR)\GetSetReg.obj" : $(SOURCE) $(DEP_CPP_GETSE) "$(INTDIR)"\
 ".\Release\esccpmfc.pch"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\TXsettype.cpp
DEP_CPP_TXSET=\
	".\esccpmfc.h"\
	".\StdAfx.h"\
	".\TXsettype.h"\
	

"$(INTDIR)\TXsettype.obj" : $(SOURCE) $(DEP_CPP_TXSET) "$(INTDIR)"\
 ".\Release\esccpmfc.pch"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\GetAdd.cpp
DEP_CPP_GETAD=\
	".\esccpmfc.h"\
	".\GetAdd.h"\
	".\StdAfx.h"\
	

"$(INTDIR)\GetAdd.obj" : $(SOURCE) $(DEP_CPP_GETAD) "$(INTDIR)"\
 ".\Release\esccpmfc.pch"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\GetCmd.cpp
DEP_CPP_GETCM=\
	".\esccpmfc.h"\
	".\GetCmd.h"\
	".\StdAfx.h"\
	

"$(INTDIR)\GetCmd.obj" : $(SOURCE) $(DEP_CPP_GETCM) "$(INTDIR)"\
 ".\Release\esccpmfc.pch"


# End Source File
# End Target
# End Project
################################################################################
