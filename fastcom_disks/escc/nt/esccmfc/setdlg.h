// setdlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// Csetdlg dialog

class Csetdlg : public CDialog
{
// Construction
public:
	Csetdlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(Csetdlg)
	enum { IDD = IDD_SETDLG };
	CString	m_bgr;
	CString	m_ccr0;
	CString	m_ccr1;
	CString	m_ccr2;
	CString	m_ccr3;
	CString	m_dafo;
	CString	m_imr0;
	CString	m_imr1;
	CString	m_ipc;
	CString	m_iva;
	CString	m_mode;
	CString	m_pcr;
	CString	m_pim;
	CString	m_pre;
	CString	m_pvr;
	CString	m_rah1;
	CString	m_rah2;
	CString	m_ral1;
	CString	m_ral2;
	CString	m_rbufs;
	CString	m_rlcr;
	CString	m_synl;
	CString	m_synh;
	CString	m_tbufs;
	CString	m_tcr;
	CString	m_timr;
	CString	m_xad1;
	CString	m_xad2;
	CString	m_xbch;
	CString	m_xbcl;
	int		m_type;
	CString	m_rfc;
	CString	m_ccr4;
	CString	m_amh;
	CString	m_aml;
	CString	m_mxf;
	CString	m_mxn;
	CString	m_rccr;
	CString	m_tsar;
	CString	m_tsax;
	CString	m_xccr;
	CString	m_tbufsize;
	CString	m_rbufsize;
	CString	m_nrbufs;
	CString	m_ntbufs;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Csetdlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Csetdlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnAsync();
	afx_msg void OnBisync();
	afx_msg void OnHdlc();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
