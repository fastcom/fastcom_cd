//Copyright (C) 1998 Commtech, Inc.
/*
* escc_rate_set.c/.exe
* a simple example showing how to set up the Fastcom:ESCC to
* a specific bit rate.
*
* Author: Carl George
* Date:   11/10/98
*
* OS: WinNT/W95 (win32 console)
* Compiler: VC/C++ 4.2
*
*
*/
#include "windows.h"
#include "math.h"
#include "stdlib.h"
#include "stdio.h"

#include "esccdrv.h"
#include "rateset.h"

void main(int argc, char *argv[])
{
unsigned port;
double bitrate;
unsigned mode;
CLKSET clk;
SETUP set;

port = 0;
mode = HDLC;
bitrate = 1000000.0;
if (argc<4)
	{
	printf("USAGE: escc_rate_set port mode bitrate [ccr0] [ccr1] [ccr2] [ccr4]\r\n");
	exit(1);
	}
port = atoi(argv[1]);
if((argv[2][1]=='h')||(argv[2][1]=='H')) mode = HDLC;
if((argv[2][1]=='a')||(argv[2][1]=='A')) mode = ASYNC;
if((argv[2][1]=='b')||(argv[2][1]=='B')) mode = BISYNC;
bitrate = atof(argv[3]);
if(argc==8)
{
 sscanf(argv[4],"%x",&set.ccr0);
 sscanf(argv[5],"%x",&set.ccr1);
 sscanf(argv[6],"%x",&set.ccr2);
 sscanf(argv[7],"%x",&set.ccr4);
}
else
{
set.ccr0 = 0x80;//hdlc
set.ccr1 = 0x17;//mode 7
set.ccr2 = 0x18;//(7b) bdf = 0
set.ccr4 = 0x00;//no ebgr
}
clk.numbits = 0;
clk.clockbits = 0;
printf("port:%u\r\n",port);
if(mode==ASYNC)printf("ASYNC mode\r\n");
if(mode==BISYNC)printf("BISYNC mode\r\n");
if(mode==HDLC) printf("HDLC mode\r\n");
printf("desired bitrate:%10.1f\r\n",bitrate);
printf("ccr0:%x\r\n",set.ccr0);
printf("ccr1:%x\r\n",set.ccr1);
printf("ccr2:%x\r\n",set.ccr2);
printf("ccr4:%x\r\n",set.ccr4);
printf("\r\nCalculating clocks...\r\n");

if(figure_settings(bitrate,&set,&clk))
	{
	printf("clock determination successful:\r\n");
	printf("clockbits:%x,numbits:%u\r\n",clk.clockbits,clk.numbits);
	printf("ccr0:%x\r\n",set.ccr0);
	printf("ccr1:%x\r\n",set.ccr1);
	printf("ccr2:%x\r\n",set.ccr2);
	printf("ccr4:%x\r\n",set.ccr4);
	printf("bgr:%x\r\n",set.bgr);
	
	exit(0);
	}
else
	{
	printf("cannot determine clock settings from setup given\r\n");
	exit(1);
	}
}

BOOL print_actual(double bitrate,double actualfreq, SETUP *settings)
{
double actualrate;
unsigned N;
unsigned n,m;
//given the bitrate and user settings calculate the clk (ICD settings) and update
//the bgr in user settings.
//determine the needed reference frequency by how the user has the settings
N = settings->bgr + ((settings->ccr2&0xc0)<<2);

if((((settings->ccr0)&HDLC)==HDLC)||(((settings->ccr0)&HDLC)==BISYNC))
	{
	//ok in hdlc(or bisync) mode can be one of:
//	rate = inclk/(N+1)*2, = (inclk/(N+1)*2)/16, = inclk/(n+1)*2^m, or =(inclk/(n+1)*2^m)/16
	//if clk mode 4 then rate = inclk
	//if clk mode 5 then rate = n/a ?
	//if clk mode 0b,7b then is either #1 or #3
	//if clk mode 6a,6b,7a then is either #2 or #4
	//if clk mode 0a,1,2a,2b,3a,3b then the rate does not depend on the osc/inclk it depends
	//on the external clock being supplied at either rxclk/txclk.
	//#3/#4 are selected by CCR4 ebgr bit.
if((settings->ccr1&0x07)==5) return FALSE;//mode 5
if((settings->ccr1&0x07)==1) return FALSE;//mode 1
if((settings->ccr1&0x07)==2) return FALSE;//mode 2a/2b
if((settings->ccr1&0x07)==3) return FALSE;//mode 3a/3b
if(((settings->ccr1&0x07)==0)&&((settings->ccr2&0x10)==0x00)) return FALSE;//mode 0a

if((settings->ccr1&0x07)==4) 
	{
	//mode 4
	actualrate = actualfreq;
	printf("desired rate:%10.1f, actual rate:%10.1f\r\n",bitrate,actualrate);
	return TRUE;
	}
if((((settings->ccr1&0x07)==0)&&((settings->ccr2&0x10)==0x10))||(((settings->ccr1&0x07)==7)&&((settings->ccr2&0x10)==0x10)))
	{
	//mode 0b/7b
	//check bdf
	if((settings->ccr2&0x20)==0x00)
		{
		//bdf bit = 0
		//rate = inclk/1
		//inclk = rate;
		actualrate = actualfreq;
		printf("desired rate:%10.1f, actual rate:%10.1f\r\n",bitrate,actualrate);
		return TRUE;
		}
	else
		{
		//bdf bit = 1
		//check ebgr;
		if((settings->ccr4&0x40)==0x00)
			{
			//not using ebgr
			//rate = inclk/(N+1)*2
			//inclk = rate * (N+1)*2
			actualrate = actualfreq  / (((double)(N+1)*2.0));
			printf("desired rate:%10.1f, actual rate:%10.1f\r\n",bitrate,actualrate);
			return TRUE;
			}
		else
			{
			m = N>>6;
			n = N & 0x3f;

			//using ebgr
			//rate = inclk/(n+1)*2^m
			actualrate = actualfreq  / ((double)(n+1)* pow(2,m));
			printf("desired rate:%10.1f, actual rate:%10.1f\r\n",bitrate,actualrate);
			return TRUE;
			}
		}
	}
if( ( ((settings->ccr1&0x07) == 7) && ((settings->ccr2&0x10)==0x00)) || ((settings->ccr1&0x07)==6) )
	{
	//mode 7a,6a/6b
	//check bdf
	if((settings->ccr2&0x20)==0x00)
		{
		//bdf bit = 0
		//rate = inclk/16
		//inclk = rate * 16;
		actualrate = actualfreq / 16.0;
		printf("desired rate:%10.1f, actual rate:%10.1f\r\n",bitrate,actualrate);
		return TRUE;
		}
	else
		{
		//bdf bit = 1
		//check ebgr;
		if((settings->ccr4&0x40)==0x00)
			{
			//rate = (inclk/(N+1)*2)/16
			//inclk = rate * (N+1)*2 * 16

			actualrate = (actualfreq / ((double)(N+1)*2.0))/16.0;
			printf("desired rate:%10.1f, actual rate:%10.1f\r\n",bitrate,actualrate);
			return TRUE;
			}
		else
			{
			n = N&0x3f;
			m = N>>6;
			//rate = inclk/(n+1)*2^m
			actualrate = (actualfreq / ((double)(n+1)* pow(2,m)))/16.0;
			printf("desired rate:%10.1f, actual rate:%10.1f\r\n",bitrate,actualrate);
			return TRUE;
			}
		}
	}


	}
if(((settings->ccr0)&HDLC)==ASYNC)
	{
	//ok in hdlc mode can be one of:
//	rate = inclk/(N+1)*2, = (inclk/(N+1)*2)/16, = inclk/(n+1)*2^m, or =(inclk/(n+1)*2^m)/16
// the only really viable clock modes are 4 and 7b
	//all other modes either require external clocks or they use a dpll scheme to recover
	//a clock.
	//if BCR = 0 then #1 or #3 is used (isosync)
	//if BCR = 1 then #2 or #4 is used (true async)
	//#3/#4 are selected by CCR4 ebgr bit.
	}
}

BOOL figure_settings(double bitrate, SETUP *settings,CLKSET *clk)
{
double reffreq;
double desiredfreq;
double actualfreq;
unsigned N;
unsigned n,m;
//given the bitrate and user settings calculate the clk (ICD settings) and update
//the bgr in user settings.
//determine the needed reference frequency by how the user has the settings
N = 0;

if((((settings->ccr0)&HDLC)==HDLC)||(((settings->ccr0)&HDLC)==BISYNC))
	{
	//ok in hdlc(or bisync) mode can be one of:
//	rate = inclk/(N+1)*2, = (inclk/(N+1)*2)/16, = inclk/(n+1)*2^m, or =(inclk/(n+1)*2^m)/16
	//if clk mode 4 then rate = inclk
	//if clk mode 5 then rate = n/a ?
	//if clk mode 0b,7b then is either #1 or #3
	//if clk mode 6a,6b,7a then is either #2 or #4
	//if clk mode 0a,1,2a,2b,3a,3b then the rate does not depend on the osc/inclk it depends
	//on the external clock being supplied at either rxclk/txclk.
	//#3/#4 are selected by CCR4 ebgr bit.
if((settings->ccr1&0x07)==5) return FALSE;//mode 5
if((settings->ccr1&0x07)==1) return FALSE;//mode 1
if((settings->ccr1&0x07)==2) return FALSE;//mode 2a/2b
if((settings->ccr1&0x07)==3) return FALSE;//mode 3a/3b
if(((settings->ccr1&0x07)==0)&&((settings->ccr2&0x10)==0x00)) return FALSE;//mode 0a

if((settings->ccr1&0x07)==4) 
	{
	//mode 4
	desiredfreq = bitrate;
	if(desiredfreq < 391000.0) return FALSE; //ICD2053b lower limit
	if(desiredfreq > 10000000.0) return FALSE;//SAB82532 upper limit
	//here should be good to go figure clkbits:
	}
if((((settings->ccr1&0x07)==0)&&((settings->ccr2&0x10)==0x10))||(((settings->ccr1&0x07)==7)&&((settings->ccr2&0x10)==0x10)))
	{
	printf("mode 7b\r\n");
	//mode 0b/7b
	//check bdf
	if((settings->ccr2&0x20)==0x00)
		{
		printf("rate = inclk/1\r\n");
		//bdf bit = 0
		//rate = inclk/1
		//inclk = rate;
		desiredfreq = bitrate;
		if(desiredfreq < 391000.0) return FALSE; //ICD2053b lower limit
		if(desiredfreq > 10000000.0) return FALSE;//SAB82532 upper limit
			//instead of returning FALSE here we could force bdf=1 and use the next section
		//should be good to go if we get here

		}
	else
		{
		//bdf bit = 1
		//check ebgr;
		if((settings->ccr4&0x40)==0x00)
			{
			//not using ebgr
			//rate = inclk/(N+1)*2
			//inclk = rate * (N+1)*2
			for(N=0;N<1023;N++)
				{
				desiredfreq = bitrate * ((double)(N+1)*2.0);
				
				if( ((N&0x3f)!=0) || (N == 0))//glitch in 82532 rev 3.2
				{
					if((desiredfreq> 391000.0)&&(desiredfreq<33333333.3))
						goto done;
					}
				}
			//if we get here (through the loop) we never found a freq
			//that we can use
			return FALSE;
done:
;			//here we have the minimum N that will get the bitrate specified
			//at the desiredfreq.
			}
		else
			{
			//using ebgr
			//rate = inclk/(n+1)*2^m
			for(m=1;m<16;m++)for(n=0;n<64;n++)
				{
				//m = 0 is generally bad, but it will work for some values
				//it is removed for convienence.
				desiredfreq = bitrate * ((double)(n+1)* pow(2,m));
				if((desiredfreq> 391000.0)&&(desiredfreq<33333333.3))
					goto done2;
				}
			//if we get here (through the loop) we never found a freq
			//that we can use
			return FALSE;
done2:
			N = m<<6 + n;
			//here we have the minimum N that will get the bitrate specified
			//at the desiredfreq.

			}
		}
	}
if( ( ((settings->ccr1&0x07) == 7) && ((settings->ccr2&0x10)==0x00)) || ((settings->ccr1&0x07)==6) )
	{
	//mode 7a,6a/6b
	//check bdf
	if((settings->ccr2&0x20)==0x00)
		{
		//bdf bit = 0
		//rate = inclk/16
		//inclk = rate * 16;
		desiredfreq = bitrate * 16.0;
		if(desiredfreq < 391000.0) return FALSE; //ICD2053b lower limit
		if(desiredfreq > 33333333.3) return FALSE;//SAB82532 upper limit
			//instead of returning FALSE here we could force bdf=1 and use the next section
		//should be good to go if we get here
		}
	else
		{
		//bdf bit = 1
		//check ebgr;
		if((settings->ccr4&0x40)==0x00)
			{
			//rate = (inclk/(N+1)*2)/16
			//inclk = rate * (N+1)*2 * 16
			for(N=0;N<1023;N++)
				{
				desiredfreq = bitrate * ((double)(N+1)*2.0)	*16.0;			
				if( ((N&0x3f)!=0) || (N == 0))//glitch in 82532 rev 3.2
					{
					if((desiredfreq> 391000.0)&&(desiredfreq<33333333.3))
						goto done3;
					}
				}
			//if we get here (through the loop) we never found a freq
			//that we can use
			return FALSE;
done3:
;			//here we have the minimum N that will get the bitrate specified
			//at the desiredfreq.
			}
		else
			{
			//rate = inclk/(n+1)*2^m
			for(m=1;m<16;m++)for(n=0;n<64;n++)
				{
				//m = 0 is generally bad, but it will work for some values
				//it is removed for convienence.
				desiredfreq = bitrate * ((double)(n+1)* pow(2,m)) * 16.0;
				if((desiredfreq> 391000.0)&&(desiredfreq<33333333.3))
					goto done4;
				}
			//if we get here (through the loop) we never found a freq
			//that we can use
			return FALSE;
done4:
			N = m<<6 + n;
			//here we have the minimum N that will get the bitrate specified
			//at the desiredfreq.

			}
		}

	}


	}
if(((settings->ccr0)&HDLC)==ASYNC)
	{
	//ok in hdlc mode can be one of:
//	rate = inclk/(N+1)*2, = (inclk/(N+1)*2)/16, = inclk/(n+1)*2^m, or =(inclk/(n+1)*2^m)/16
// the only really viable clock modes are 4 and 7b
	//all other modes either require external clocks or they use a dpll scheme to recover
	//a clock.
	//if BCR = 0 then #1 or #3 is used (isosync)
	//if BCR = 1 then #2 or #4 is used (true async)
	//#3/#4 are selected by CCR4 ebgr bit.
	}

//then call calculate_bits to determine if it is OK and the programming word
reffreq = 18432000.0;
//desiredfreq calculate above
//actualclock and clk are returned
if(	calculate_bits(reffreq,desiredfreq,&actualfreq,clk))
	{
	//success. check the actual then run with it
	//if the actual is off by more than you want then adjust the bgr/desired freq and
	//try again
	printf("Desired clock:%10.1f actualclock:%10.1f\r\n",desiredfreq,actualfreq);
	//printf("clk:%x,%u\r\n",clk->clockbits,clk->numbits);
	//printf("N:%x\r\n",N);

	settings->bgr = N&0xff;
	settings->ccr2 = (settings->ccr2&0x3f)|((N&0x300)>>2);
	if((settings->ccr0&0x40)==0x40)
		{
		//is master clock mode
		//so inclock must be adjusted or ccr4 must be set to /4
		//(ie master clock must never be more than 10MHz)
		if(actualfreq>10000000.0)
			{
			settings->ccr4 = settings->ccr4 | 0x80; 
			//inclock is greater than 10MHz so master clock is inclk/4.
			}
		}
	else
		{
		settings->ccr4 = settings->ccr4 & 0x7f; //force to default (not used anyway)
		}
	print_actual(bitrate,actualfreq,settings);
	}
}


BOOL calculate_bits(double reffreq,double desired_freq,double *m_actual_clock,CLKSET *clk)
{
DWORD P;
DWORD Pprime;
DWORD Q;
DWORD Qprime;
DWORD M;
DWORD I;
DWORD D;
double fvco;
double desired_ratio;
double actual_ratio;
DWORD bestP;
DWORD bestQ;
DWORD bestM;
DWORD bestI;
double best_actual_ratio;
DWORD Progword;
DWORD Stuffedword;
DWORD bit1;
DWORD bit2;
DWORD bit3;
DWORD i,j;
char buf[256];
unsigned long rangelo;
unsigned long rangehi;
double best_diff;

D = 0x00000000;//from bitcalc, but the datasheet says to make this 1...
bestP = 0;
bestQ = 0;
best_actual_ratio = 1000000.0;//hopefully we can do better than this...
best_diff = 1000000.0;
rangelo = (unsigned long)floor(reffreq/1000000.0) +1;
rangehi = (unsigned long)floor(reffreq/200000.0);
if(rangelo <3) rangelo = 3;
if(rangehi>129) rangehi = 129;
sprintf(buf,"%u(%f), %u(%f)",rangelo,(reffreq/1000000.0) +1.0,rangehi,(reffreq/200000.0));
//MessageBox(buf,"rl(fl),rh(fh)",MB_OK);
for(i=0;i<=7;i++)
{
M = i;
fvco = desired_freq * (pow(2,i));
//sprintf(buf,"%f",fvco);
//MessageBox(NULL,buf,"Fvco",MB_OK);

if(fvco<80000000.0) I = 0x00000000;
if(fvco>=80000000.0) I = 0x00000008;
if((fvco>50000000.0)&&(fvco<150000000.0))
{
sprintf(buf,"%f",fvco);
//MessageBox(NULL,buf,"Fvco",MB_OK);
desired_ratio = fvco/(2.0 * reffreq);
sprintf(buf,"%f",desired_ratio);
//MessageBox(NULL,buf,"desired",MB_OK);

for(P=4;P<=130;P++)for(Q=rangelo;Q<=rangehi;Q++)
{

actual_ratio = (double)P/(double)Q;
if(actual_ratio==desired_ratio) 
	{
	sprintf(buf,"%u,%u",P,Q);
	//MessageBox(NULL,buf,"Direct Hit",MB_OK);
	bestP = P;
	bestQ = Q;
	bestM = M;
	bestI = I;
	best_actual_ratio = actual_ratio;
	goto donecalc;
	}
else 
	{
	if(fabs(desired_ratio - actual_ratio)<(best_diff)) 
		{
		best_diff = fabs(desired_ratio - actual_ratio);
		best_actual_ratio = actual_ratio;
		bestP = P;
		bestQ = Q;
		bestM = M;
		bestI = I;
		sprintf(buf,"desired:%f,actual:%f, best%f P%u,Q%u,fvco:%f,M:%u",desired_ratio,actual_ratio,best_diff,bestP,bestQ,fvco,M);
		//MessageBox(buf,"ratiocalc",MB_OK);
		}
	}	
}
}
}
donecalc:
if((bestP!=0)&&(bestQ!=0))
	{
	//here bestP BestQ are good to go.
	I = bestI;
	M = bestM;
	P = bestP;
	Q = bestQ;
	Pprime = bestP - 3;
	Qprime = bestQ - 2;
	sprintf(buf,"P':%u, Q':%u, M:%u, I:%u",Pprime,Qprime,M,I);
	//MessageBox(buf,"P,Q,M,I",MB_OK);
	Progword = 0;
	Progword =  (Pprime<<15) | (D<<14) | (M<<11) | (Qprime<<4) | I;
	sprintf(buf,"%lx",Progword);
//	MessageBox(buf,"Progword",MB_OK);
	bit1 = 0;
	bit2 = 0;
	bit3 = 0;
	Stuffedword = 0;
	i = 0;
	j = 0;
	bit1 = ((Progword>>i)&1);
	Stuffedword |=  (bit1<<j);
	i++;
	j++;
	bit2 = ((Progword>>i)&1);
	Stuffedword |=  (bit2<<j);
	i++;
	j++;
	bit3 = ((Progword>>i)&1);
	Stuffedword |=  (bit3<<j);
	j++;
	i++;
	while(i<=22)
		{
		if((bit1==1)&&(bit2==1)&&(bit3==1))
			{
			//force a 0 in the stuffed word;
			j++;
			bit3 = 0;
			sprintf(buf,"i,j : %u,%u",i,j);
//			MessageBox(buf,"Stuffing",MB_OK);
			}
		bit1 = bit2;
		bit2 = bit3;
		bit3 = ((Progword>>i)&1);
		Stuffedword |=  (bit3<<j);
		i++;
		j++;
		}
	sprintf(buf,"SW:%lx ,numbits:%u",Stuffedword,j);

clk->clockbits = Stuffedword;
clk->numbits = (USHORT)j-1;
*m_actual_clock = ((2.0 * reffreq) * ((double)P/(double)Q)) / pow(2,M);
//	MessageBox(buf,"stuffedword, numbits",MB_OK);
return TRUE;
	}
else
{
	printf("\r\nError in ICD calculation\r\n");
	return FALSE;

}
}
