//Copyright (C) 1998 Commtech, Inc.
//rateset.h

#define HDLC   0
#define ASYNC  3
#define BISYNC 2


BOOL figure_settings(double bitrate, SETUP *setttings,CLKSET *clk);
BOOL calculate_bits(double reffreq,double desired_freq,double *m_actual_clock,CLKSET *clk);
BOOL print_actual(double bitrate,double actualfreq, SETUP *settings);
