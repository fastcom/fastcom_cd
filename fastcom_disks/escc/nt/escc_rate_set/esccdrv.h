//Copyright (c) 1995,1997 Commtech, Inc. Wichtia, KS
// esccdrv.h  -- header file for esccdrv 
//
// The ESCC device driver IOCTLs
//
#define IOCTL_ESCCDRV_SET_FS6131		0x83002140
#define IOCTL_ESCCDRV_IMMEDIATE_STATUS	0x83002100
#define IOCTL_ESCCDRV_WRITE_REGISTER	0x830020fc
#define IOCTL_ESCCDRV_READ_REGISTER     0x830020f8
#define IOCTL_ESCCDRV_SET_CLOCK         0x830020f4
#define IOCTL_ESCCDRV_SET_DTR           0x830020f0
#define IOCTL_ESCCDRV_GET_DSR           0x830020ec
#define IOCTL_ESCCDRV_CMDR				0x830020e8
#define IOCTL_ESCCDRV_FLUSH_TX          0x830020e4
#define IOCTL_ESCCDRV_FLUSH_RX          0x830020e0
#define IOCTL_ESCCDRV_SET_TX_ADD        0x830020dc
#define IOCTL_ESCCDRV_SET_TX_TYPE       0x830020d8
#define IOCTL_ESCCDRV_STOP_TIMER        0x830020d4
#define IOCTL_ESCCDRV_START_TIMER       0x830020d0
#define IOCTL_ESCCDRV_STATUS            0x830020cc
#define IOCTL_ESCCDRV_SETUP				0x830020c8
#define IOCTL_ESCCDRV_RX_READY          0x830020c4
#define IOCTL_ESCCDRV_TX_ACTIVE         0x830020c0


typedef struct setup{
//used in all
unsigned cmdr;
unsigned mode;
unsigned timr;
unsigned xbcl;
unsigned xbch;
unsigned ccr0;
unsigned ccr1;
unsigned ccr2;
unsigned ccr3;
unsigned ccr4;
unsigned tsax;
unsigned tsar;
unsigned xccr;
unsigned rccr;
unsigned bgr;
unsigned iva;
unsigned ipc;
unsigned imr0;
unsigned imr1;
unsigned pvr;
unsigned pim;
unsigned pcr;
//escc register defines for HDLC/SDLC mode
unsigned xad1;
unsigned xad2;
unsigned rah1;
unsigned rah2;
unsigned ral1;
unsigned ral2;
unsigned rlcr;
unsigned aml;
unsigned amh;
unsigned pre;
//escc async register defines (used in bisync as well)
unsigned xon;
unsigned xoff;
unsigned tcr;   //used in bisync too
unsigned dafo;  //used in bisync too
unsigned rfc;   //used in bisync too
unsigned tic;
unsigned mxn;
unsigned mxf;

//escc bisync register defines
unsigned synl;
unsigned synh;
} SETUP;



typedef struct clkset{
	ULONG clockbits;
	USHORT numbits;
} CLKSET;

struct regsingle{ //used for ESCC_WRITE_REGISTER ioctl call
	ULONG port;             //offset from base address of register to access
	UCHAR data;             //data to write
};


//device io control STATUS function return values
#define ST_RX_DONE		0x00000001	//A block of data has been moved from the card to the buffers; Not for user code, Used in driver code ONLY
#define ST_OVF			0x00000002	//the software buffers were full and then another frame came in
#define ST_RFS			0x00000004	//IMR0:RFS (HDLC)
#define ST_RX_TIMEOUT	0x00000008	//IMR0:TIME (ASYNC)
#define ST_RSC			0x00000010	//IMR0:RSC (HDLC)
#define ST_PERR			0x00000020	//IMR0:PERR (ASYNC, BISYNC)
#define ST_PCE			0x00000040	//IMR0:PCE (HDLC)
#define ST_FERR			0x00000080	//IMR0:FERR (ASYNC)
#define ST_SYN			0x00000100	//IMR0:SDC (BISYNC)
#define ST_DPLLA		0x00000200	//IMR0:PLLA (HDLC, ASYNC, BISYNC)
#define ST_CDSC			0x00000400	//IMR0:CDSC (HDLC, ASYNC, BISYNC)
#define ST_RFO			0x00000800	//IMR0:RFO (HDLC, ASYNC, BISYNC)
#define ST_EOP			0x00001000	//IMR1:EOP (HDLC)
#define ST_BRKD			0x00002000	//IMR1:BRK (ASYNC)
#define ST_ONLP			0x00004000	//IMR1:OLP (HDLC) same as RDO
#define ST_RDO			0x00004000	//IMR1:RDO (HDLC) same as OLP
#define ST_BRKT			0x00008000	//IMR1:BRKT (ASYNC)
#define	ST_AOLP			0x00010000	//IMR1:AOLP (HDLC) same as ALLS
#define	ST_ALLS			0x00010000	//IMR1:ALLS (HDLC, ASYNC, BISYNC) same as AOLP
#define ST_XDU			0x00020000	//IMR1:XDU (HDLC, BISYNC) same as EXE
#define ST_EXE			0x00020000	//IMR1:EXE (HDLC) same as XDU
#define ST_TIN			0x00040000	//IMR1:TIN (HDLC, ASYNC, BISYNC)
#define ST_CTSC			0x00080000	//IMR1:CSC (HDLC, ASYNC, BISYNC)
#define ST_XMR			0x00100000	//IMR1:XMR (HDLC, BISYNC)
#define ST_TX_DONE		0x00200000	//A a block of data has been moved from the buffers to the card; Not for user code, Used in driver code ONLY
#define ST_DMA_TC		0x00400000	//DMA Signal Count has occurred (this isn't used)
#define ST_DSR1C		0x00800000	//State change of DSR1 if enabled in PIM
#define ST_DSR0C		0x01000000	//State change of DSR0 if enabled in PIM
#define ST_FUBAR_IRQ    0x02000000	//Very rare and bad, detection of IRQ latchup.  If you get this reboot

//these are also somewhere else in a windows header file.
//
#define STATUS_DEVICE_DOES_NOT_EXIST     0xC00000C0L
#define STATUS_INVALID_PARAMETER         0xC000000DL
#define STATUS_NO_SUCH_DEVICE            0xC000000EL
#define STATUS_CANCELLED                 0xC0000120L

//these are not really necessary unless you need to do something
//special, if so then they are here
//CMDR commands for the ESCC
#define XRES 1
#define XME 2
#define XIF 4
#define HUNT 4
#define XTF 8
#define STIB 16
#define RNR 32
#define RFRD 32
#define RHR 64
#define RMC 128
//STAR codes
#define WFA 1
#define CTS 2
#define CEC 4
#define RLI 8
#define TEC 8
#define RRNR 16
#define SYNC 16
#define FCS 16
#define XRNR 32
#define RFNE 32
#define XFW 64
#define XDOV 128
//ISR0
#define RPF 1
#define RFO 2
#define CDSC 4
#define PLLA 8
#define PCE 16
#define RSC 32
#define RFS 64
#define RME 128
//ISR1
#define XPR 1
#define XMR 2
#define CSC 4
#define TIN 8
#define EXE 16
#define XDU 16
#define ALLS 32
#define AOLP 32
#define RDO 64
#define OLP 64
#define EOP 128

//port addresses
//hdlc defines
#define STAR 0
#define CMDR 0
#define RSTA 1
#define PRE 1
#define MODE 2
#define TIMR 3
#define XAD1 4
#define XAD2 5
#define RAH1 6
#define RAH2 7
#define RAL1 8
#define RHCR 9
#define RAL2 9
#define RBCL 10
#define XBCL 10
#define RBCH 11
#define XBCH 11
#define CCR0 12
#define CCR1 13
#define CCR2 14
#define CCR3 15
#define TSAX 16
#define TSAR 17
#define XCCR 18
#define RCCR 19
#define VSTR 20
#define BGR 20
#define RLCR 21
#define AML 22
#define AMH 23
#define GIS 24
#define IVA 24
#define IPC 25
#define ISR0 26
#define IMR0 26
#define ISR1 27
#define IMR1 27
#define PVR 28
#define PIS 29
#define PIM 29
#define PCR 30
#define CCR4 31
#define FIFO 32
//async defines
#define XON 4
#define XOFF 5
#define TCR 6
#define DAFO 7
#define RFC 8
#define TIC 21
#define MXN 22
#define MXF 23
//bisync defines
#define SYNL 4
#define SYNH 5


//end of escc header file
