//Copyright (C) 1998 Commtech, INC. Wichita, KS
/****************************************************************************
*
* PROGRAM: ESCCTEST.C
*
* PURPOSE: Simple console application for calling ESCCDRV  VxD
*
* FUNCTIONS:
*  main() - 
*
* SPECIAL INSTRUCTIONS:
*
****************************************************************************/
#include <conio.h>
#include <stdio.h>
#include <windows.h>
#include "esccdrv.h"

struct txbuffer
{
	DWORD port;
	char buffer[4096];
};

void decode_stat(unsigned long stat);
void decode_err(DWORD err);
int main()
{
    HANDLE      hESCC = 0;
    DWORD       cbBytesReturned;
    DWORD       dwErrorCode;
	DWORD		Port;
	DWORD i,k;
	SETUP settings;
	REGSINGLE esccreg;
	CLK clock;
	struct board_switches boardsw;
	unsigned long stat;
	char buffer[4096];
	struct txbuffer tbuf;
	DWORD bytestosend;
	OVERLAPPED osr;
	DWORD olbytesread;
	OVERLAPPED osw;
	DWORD olbyteswrite;
	unsigned key;

memset( &osr, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct

// create I/O event used for overlapped read

osr.hEvent = CreateEvent( NULL,    // no security
			    TRUE,    // manual  reset req
			    FALSE,   // initial event reset
			    NULL ) ; // no name
if (osr.hEvent == NULL)
   {
      printf("Failed to create event\n");
      return(1); 
   }
memset( &osw, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct

// create I/O event used for overlapped write

osw.hEvent = CreateEvent( NULL,    // no security
			    TRUE,    // manual  reset req
			    FALSE,   // initial event reset
			    NULL ) ; // no name
if (osw.hEvent == NULL)
   {
      printf("Failed to create event\n");
      return(1); 
   }


    // Dynamically load and prepare to call CVXDSAMP
    // The CREATE_NEW flag is not necessary
    hESCC = CreateFile("\\\\.\\ESCCDRV.VXD", 0,0,0,
                        CREATE_NEW, FILE_FLAG_OVERLAPPED, 0);

	//unless the file is not there this will allways return successfully
    if ( hESCC == INVALID_HANDLE_VALUE )
    {
        dwErrorCode = GetLastError();
        if ( dwErrorCode == ERROR_NOT_SUPPORTED )
        {
            printf("Unable to open VxD, \n device does not support DeviceIOCTL\n");
        }
        else
        {
            printf("Unable to open VxD, Error code: %lx\n", dwErrorCode);
        }
    }
    else
    {
	printf("obtained handle to esccdrv\n");
	//got a handle to the vxd so it must be loaded.
	//lets start by telling it where the board is:
printf("try to add port\n");
boardsw.base  = 0x280;	//match the base address switch setting here
boardsw.irq   = 5;		//match the irq switch setting here
boardsw.dmat1 = 0;		//channel 0 dma transmit (0 if no dma)
boardsw.dmar1 = 0;		//channel 0 dma receive  (0 if no dma)
boardsw.dmat2 = 0;		//channel 1 dma transmit (0 if no dma)
boardsw.dmar2 = 0;		//channel 1 dma receive  (0 if no dma)
        if ( DeviceIoControl(hESCC, ESCC_ADD_BOARD,
                &boardsw, sizeof(struct board_switches),
				(LPVOID)NULL, 0,
                &cbBytesReturned, NULL) )
        {
			printf("port added\n");
        }
        else
        {
            dwErrorCode = GetLastError();
			decode_err(dwErrorCode);
        }
	//now verify that the ports were added
        if(DeviceIoControl(hESCC, ESCC_GET_MAX_PORT,
                (LPVOID)NULL,0,
				&i, sizeof(DWORD),
                &cbBytesReturned, NULL))
			{
			//returns true and i = maximum usable port # (starting at 0)
			//or ERROR_INVALID_PARAMETER if a DWORD is not given for the output buffer
			//or ERROR_NO_PORTS_DEFINED if no ports are known to the driver
			if(cbBytesReturned!=sizeof(DWORD))printf("bad returnsize:%lu\n",cbBytesReturned);
			printf("#ports active:%lu\n",i+1);//remember 0 based return value...
			}
		else
			{
		    dwErrorCode = GetLastError();
			decode_err(dwErrorCode);
			goto end;	
			}

printf("Set ICD2053b\n");		
//setup the icd2053b		
		clock.clockbits = 0x5d1460;//settings for 16.000 MHz
		clock.numbits = 23;
		clock.port = 0;
        if ( DeviceIoControl(hESCC,ESCC_SET_CLOCK,&clock,sizeof(CLK),(LPVOID)NULL,0,&cbBytesReturned, NULL) )
		//returns true if clock generator is set 
		//or ERROR_INVALID_PARAMETER if a CLK struct is not passed
		//or ERROR_BAD_PORT_NUMBER if the port given is out of range
        {
			if(cbBytesReturned!=0)printf("bad returnsize:%lu\n",cbBytesReturned);
			else printf("ICD2053B set to 16MHz\n");
        }
        else
        {
            dwErrorCode = GetLastError();
			decode_err(dwErrorCode);
			goto end;
        }

printf("starting settings...");
settings.port = 0;
settings.txtype = XTF;
settings.mode = 0x88;
settings.timr = 0x1f;
settings.xbcl = 0x00;
settings.xbch = 0x00;
settings.ccr0 = 0x80;
settings.ccr1 = 0x17;
settings.ccr2 = 0xf8;
settings.ccr3 = 0x00;
settings.ccr4 = 0x00;
settings.tsax = 0x00;
settings.tsar = 0x00;
settings.xccr = 0x00;
settings.rccr = 0x00;
settings.bgr = 0xff;//slow down to 7812bps
settings.iva = 0x00;
settings.ipc = 0x03;
settings.imr0 = 0x04;
settings.imr1 = 0x00;
settings.pvr = 0x00;
settings.pim = 0xff;
settings.pcr = 0xe0;
settings.xad1 = 0xff;
settings.xad2 = 0xff;
settings.rah1 = 0xff;
settings.rah2 = 0xff;
settings.ral1 = 0xff;
settings.ral2 = 0xff;
settings.rlcr = 0x00;
settings.aml = 0x00;
settings.amh = 0x00;
settings.pre = 0x00;
settings.n_rbufs = 10;
settings.n_tbufs = 2;
settings.n_rfsize_max = 4096;
settings.n_tfsize_max = 4096;

        if ( DeviceIoControl(hESCC, ESCC_INITIALIZE,(LPVOID)&settings, sizeof(SETUP),(LPVOID)NULL, 0,&cbBytesReturned, NULL) )
			//returns true if settings successfull
			//returns ERROR_INVALID_PARAMETER if SETUP struct not passed
			//returns ERROR_BAD_PORT_NUMBER if port is out of range
			//returns ERROR_CEC_TIMEOUT if the command executing bit times out(ie no core clock)
			{
            if(cbBytesReturned!=0)printf("bad returnsize:%lu\n",cbBytesReturned);
			else printf("...INIT OK\n");
			}
        else
		    {
            dwErrorCode = GetLastError();
			printf("INIT FAILED\nINIT returned:%li\n",dwErrorCode);
			decode_err(dwErrorCode);
			goto end;
			}

//port now initialized so lets play with it
printf("Getting VSTR\n");		
		esccreg.port = 0;
		esccreg.reg_offset = VSTR;

        if ( DeviceIoControl(hESCC, ESCC_READ_REGISTER,&esccreg, sizeof(REGSINGLE),&i, sizeof(DWORD),&cbBytesReturned, NULL) )
        //returns true with the contents of the register specified 
		//or ERROR_INVALID_PARAMETER if a REGSINGLE struct and DWORD are not passed
		//ERROR_BAD_PORT_NUMBER if the port is out fo range
		{
			if(cbBytesReturned!=(sizeof(DWORD)))printf("bad returnsize:%lu\n",cbBytesReturned);
            printf("VSTR contents:%lx\n",i);
        }
        else
        {
            dwErrorCode = GetLastError();
			decode_err(dwErrorCode);
			goto end;
        }

printf("WRITE REG (CMDR->XRES)\n");		
		esccreg.port = 0;
		esccreg.reg_offset = CMDR;
		esccreg.reg_value = XRES;

        if ( DeviceIoControl(hESCC, ESCC_WRITE_REGISTER,&esccreg,sizeof(REGSINGLE),(LPVOID)NULL, 0,&cbBytesReturned, NULL) )
        //returns true if value written to port
		//or ERROR_INVALID_PARAMETER if REGSINGLE struct not passed
		//or ERROR_BAD_PORT_NUMBER if port is out of range
		//or ERROR_CEC_TIMEOUT if a write to the CMDR and there is a command executing timeout condition
		{
			if(cbBytesReturned!=0)printf("bad returnsize:%lu\n",cbBytesReturned);
			else printf("Write Reg successful\n");
		}
        else
        {
            dwErrorCode = GetLastError();
			decode_err(dwErrorCode);
			goto end;
        }

printf("check status\n");
//check status
		Port = 0;
        if(DeviceIoControl(hESCC, ESCC_STATUS,
                &Port,sizeof(DWORD),
				&stat, sizeof(unsigned long),
                &cbBytesReturned, NULL) )
		//returns true with unsigned long status or (reads and clears the internal status value)
		//ERROR_INVALID_PARAMETER if DWORD and unsigned long not passed
		//ERROR_BAD_PORT_NUMBER if the port is out of range
		{
		if(cbBytesReturned!=sizeof(unsigned long))printf("bad returnsize:%lu\n",cbBytesReturned);
		decode_stat(stat);
		}
		else
			{
            dwErrorCode = GetLastError();
			decode_err(dwErrorCode);
			goto end;
			}

//flush rx
printf("flushing rx\n");
		Port = 0;
        if ( DeviceIoControl(hESCC, ESCC_FLUSH_RX,&Port,sizeof(DWORD),(LPVOID)NULL, 0,&cbBytesReturned, NULL) )
		//returns true if receive buffers are flushed or
		//ERROR_INVALID_PARAMETER if a DWORD is not passed
		//ERROR_BAD_PORT_NUMBER if the port is out of range
		//ERROR_CEC_TIMEOUT if there is a command executing
        {
		if(cbBytesReturned!=0)printf("bad returnsize:%lu\n",cbBytesReturned);
        printf("RX flushed\n");
		}
        else
        {
            dwErrorCode = GetLastError();
			decode_err(dwErrorCode);
			goto end;
        }
//flush tx
printf("flushing tx\n");
		Port = 0;
        if ( DeviceIoControl(hESCC, ESCC_FLUSH_TX,&Port,sizeof(DWORD),(LPVOID)NULL, 0,&cbBytesReturned, NULL) )
		//returns true if transmit buffers are flushed or
		//ERROR_INVALID_PARAMETER if a DWORD is not passed
		//ERROR_BAD_PORT_NUMBER if the port is out of range
		//ERROR_CEC_TIMEOUT if there is a command executing
		{
		if(cbBytesReturned!=0)printf("bad returnsize:%lu\n",cbBytesReturned);
        printf("TX flushed\n");
		}
        else
        {
            dwErrorCode = GetLastError();
			decode_err(dwErrorCode);
			goto end;
        }

do
{
//read	

printf("read frame\n");
		Port = 0;
        if ( DeviceIoControl(hESCC, ESCC_READ,&Port, sizeof(DWORD),buffer, sizeof(buffer),&cbBytesReturned, &osr) )
		//returns true if a frame is received and copied to the buffer (in HDLC mode the last byte is the contents of the RSTA register)
        //cbBytesReturned will be the number of bytes copied to the buffer
		//or ERROR_INVALID_PARAMETER if not a DWORD and char array passed
		//or ERROR_BAD_PORT_NUMBER if the port is out of range
		//or ERROR_NO_DATA_RECEIVED if there are not any valid receive buffers waiting in the driver
		//or ERROR_USER_BUFFER_TOO_SMALL if the received frame is bigger than the buffer passed.
		{
			printf("Read returned :%lu bytes\n",cbBytesReturned);
			//display the returned buffer here
			for(i=0;i<cbBytesReturned;i++) printf("%c",buffer[i]);
			printf("\n");
        }
        else
        {
            dwErrorCode = GetLastError();
			decode_err(dwErrorCode);
			//printf("overlapped:%lx\n",osr.Internal);
        }
//write
		printf("write frame #1\n");
		tbuf.port = 0;
		for(i=0;i<4096;i++) tbuf.buffer[i] = (char)((i%10)+0x30);//sample data frame contanes 0-9 repeating
		bytestosend = sizeof(DWORD)+25;//the dword is the port#, the # after is the number of bytes to send in the frame
        if ( DeviceIoControl(hESCC, ESCC_WRITE,&tbuf, bytestosend,(LPVOID)NULL,0,&cbBytesReturned, &osw) )
			//returns true if a frame is written or queued
			//ERROR_INVALID_PARAMETER if the bytestosend is not at least a sizeof(DWORD)
			//ERROR_BAD_PORT_NUMBER if the port is out of range
			//ERROR_TRANSMIT_FRAME_TOO_LARGE if bytestosend is > sizeof(DWORD)+4096
			//ERROR_TRANSMIT_BUFFERS_FULL if all of the internal buffers are used and queued for transmit
			//ERROR_TRANSMITTER_LOCKED_UP if the driver thinks that it is not transmitting but a valid transmit buffer is found.(requires a flush to transmit anything)
			//ERROR_CEC_TIMEOUT if a command is executing
			{
			if(cbBytesReturned!=0)printf("bad returnsize:%lu\n",cbBytesReturned);	
			printf("Write successful\n");
			}
        else
			{
            dwErrorCode = GetLastError();
			decode_err(dwErrorCode);
	        if(dwErrorCode==ERROR_IO_PENDING)
				{
				do//should not be here, as it is the first write to the device.
					{
					k = WaitForSingleObject(osw.hEvent,2000);
					if(k==WAIT_TIMEOUT) printf("Write Timeout\n");
					if(k==WAIT_OBJECT_0)
						{
						printf("WAIT_OBJECT_0\n");
						GetOverlappedResult(hESCC,&osw,&olbyteswrite,TRUE);
						}
					}while(k!=WAIT_OBJECT_0);

				}

	        }

//io still pending

do
{
k = WaitForSingleObject(osr.hEvent,2000);
if(k==WAIT_TIMEOUT) printf("Read Timeout\n");
if(k==WAIT_OBJECT_0)
	{
	printf("WAIT_OBJECT_0\n");
	GetOverlappedResult(hESCC,&osr,&olbytesread,TRUE);
	printf("post GET_OVERLAPPED_RESULT\n");
		for(i=0;i<olbytesread;i++) printf("%c",buffer[i]);
		printf("\n");
		printf("Read returned :%lu bytes ---  ",olbytesread);
		if(olbytesread!=0)
		{
		if(buffer[olbytesread-1]&0x80)printf("Valid ;");
		else printf("Invalid ;");
		printf("CRC..");
		if(buffer[olbytesread-1]&0x20)printf("OK ;");
		else printf("Failed ;");
		if(buffer[olbytesread-1]&0x40)printf("RDO ;");
		}
		printf("\n");
	}
}while(k!=WAIT_OBJECT_0);

//write
		printf("write frame #1\n");
		tbuf.port = 0;
		for(i=0;i<4096;i++) tbuf.buffer[i] = (char)((i%10)+0x30);//sample data frame contanes 0-9 repeating
		bytestosend = sizeof(DWORD)+4000;//the dword is the port#, the # after is the number of bytes to send in the frame
        if ( DeviceIoControl(hESCC, ESCC_WRITE,&tbuf, bytestosend,(LPVOID)NULL,0,&cbBytesReturned, &osw) )
			{
			if(cbBytesReturned!=0)printf("bad returnsize:%lu\n",cbBytesReturned);	
			printf("Write #1successful\n");
			}
        else
			{
            dwErrorCode = GetLastError();
			decode_err(dwErrorCode);
	        if(dwErrorCode==ERROR_IO_PENDING)
				{
				do//should spin here until write #3 starts (or gets queued)
					{
					k = WaitForSingleObject(osw.hEvent,2000);
					if(k==WAIT_TIMEOUT) printf("Write Timeout\n");
					if(k==WAIT_OBJECT_0)
						{
						printf("WAIT_OBJECT_0\n");
						GetOverlappedResult(hESCC,&osw,&olbyteswrite,TRUE);
						}
					}while(k!=WAIT_OBJECT_0);

				}

			}
//write
		printf("write frame #2 \n");
		tbuf.port = 0;
		for(i=0;i<4096;i++) tbuf.buffer[i] = (char)((i%10)+0x30);//sample data frame contanes 0-9 repeating
		bytestosend = sizeof(DWORD)+3000;//the dword is the port#, the # after is the number of bytes to send in the frame
        if ( DeviceIoControl(hESCC, ESCC_WRITE,&tbuf, bytestosend,(LPVOID)NULL,0,&cbBytesReturned, &osw) )
			{
			if(cbBytesReturned!=0)printf("bad returnsize:%lu\n",cbBytesReturned);	
			printf("Write #2 successful\n");
			}
        else
			{
            dwErrorCode = GetLastError();
			decode_err(dwErrorCode);
	        if(dwErrorCode==ERROR_IO_PENDING)
				{
				do//should spin here until write #3 starts (or gets queued)
					{
					k = WaitForSingleObject(osw.hEvent,2000);
					if(k==WAIT_TIMEOUT) printf("Write Timeout\n");
					if(k==WAIT_OBJECT_0)
						{
						printf("WAIT_OBJECT_0\n");
						GetOverlappedResult(hESCC,&osw,&olbyteswrite,TRUE);
						}
					}while(k!=WAIT_OBJECT_0);

				}

	        }

//write
		printf("write frame #3 --should pend (no more tbufs)\n");
		tbuf.port = 0;
		for(i=0;i<4096;i++) tbuf.buffer[i] = (char)((i%10)+0x30);//sample data frame contanes 0-9 repeating
		bytestosend = sizeof(DWORD)+2000;//the dword is the port#, the # after is the number of bytes to send in the frame
        if ( DeviceIoControl(hESCC, ESCC_WRITE,&tbuf, bytestosend,(LPVOID)NULL,0,&cbBytesReturned, &osw) )
			{
			if(cbBytesReturned!=0)printf("bad returnsize:%lu\n",cbBytesReturned);	
			printf("Write #3 successful\n");
			}
        else
			{
            dwErrorCode = GetLastError();
			decode_err(dwErrorCode);
/*this wait is skipped to show how the next write would fail
			if(dwErrorCode==ERROR_IO_PENDING)
				{
				do//should spin here until write #3 starts (or gets queued)
					{
					k = WaitForSingleObject(osw.hEvent,2000);
					if(k==WAIT_TIMEOUT) printf("Write Timeout\n");
					if(k==WAIT_OBJECT_0)
						{
						printf("WAIT_OBJECT_0\n");
						GetOverlappedResult(hESCC,&osw,&olbyteswrite,TRUE);
						}
					}while(k!=WAIT_OBJECT_0);

				}
*/
			}

//write
		printf("write frame #4 --should fail (bufs full)\n");
		tbuf.port = 0;
		for(i=0;i<4096;i++) tbuf.buffer[i] = (char)((i%10)+0x30);//sample data frame contanes 0-9 repeating
		bytestosend = sizeof(DWORD)+1000;//the dword is the port#, the # after is the number of bytes to send in the frame
        if ( DeviceIoControl(hESCC, ESCC_WRITE,&tbuf, bytestosend,(LPVOID)NULL,0,&cbBytesReturned, &osw) )
			{
			if(cbBytesReturned!=0)printf("bad returnsize:%lu\n",cbBytesReturned);	
			printf("Write #4 successful\n");
			}
        else
			{
            dwErrorCode = GetLastError();
			decode_err(dwErrorCode);
	        if(dwErrorCode==ERROR_IO_PENDING)
				{
				do//should spin here until write #3 starts (or gets queued)
					{
					k = WaitForSingleObject(osw.hEvent,2000);
					if(k==WAIT_TIMEOUT) printf("Write Timeout\n");
					if(k==WAIT_OBJECT_0)
						{
						printf("WAIT_OBJECT_0\n");
						GetOverlappedResult(hESCC,&osw,&olbyteswrite,TRUE);
						}
					}while(k!=WAIT_OBJECT_0);

				}
			}

		

printf("read frame (should get 4001 bytes)\n");//to get write # 1
		Port = 0;
        if ( DeviceIoControl(hESCC, ESCC_READ,&Port, sizeof(DWORD),buffer, sizeof(buffer),&cbBytesReturned, &osr) )
        {
			for(i=0;i<cbBytesReturned;i++) printf("%c",buffer[i]);
			printf("\n");
			printf("Read returned :%lu bytes ---  ",cbBytesReturned);
			if(cbBytesReturned!=0)
			{
			if(buffer[cbBytesReturned-1]&0x80)printf("Valid ;");
			else printf("Invalid ;");
			printf("CRC..");
			if(buffer[cbBytesReturned-1]&0x20)printf("OK ;");
			else printf("Failed ;");
			if(buffer[cbBytesReturned-1]&0x40)printf("RDO ;");
			}
			printf("\n");
        }
        else
        {
            dwErrorCode = GetLastError();
			decode_err(dwErrorCode);
			//io still pending
			if(dwErrorCode==ERROR_IO_PENDING)
			{
			do
				{
				k = WaitForSingleObject(osr.hEvent,2000);
				if(k==WAIT_TIMEOUT) printf("Read Timeout\n");
				if(k==WAIT_OBJECT_0)
					{
					printf("WAIT_OBJECT_0\n");
					GetOverlappedResult(hESCC,&osr,&olbytesread,TRUE);
					printf("post GET_OVERLAPPED_RESULT\n");
						for(i=0;i<olbytesread;i++) printf("%c",buffer[i]);
						printf("\n");
						printf("Read returned :%lu bytes ---  ",olbytesread);
						if(olbytesread!=0)
						{
						if(buffer[olbytesread-1]&0x80)printf("Valid ;");
						else printf("Invalid ;");
						printf("CRC..");
						if(buffer[olbytesread-1]&0x20)printf("OK ;");
						else printf("Failed ;");
						if(buffer[olbytesread-1]&0x40)printf("RDO ;");
						}
						printf("\n");
					}
				}while(k!=WAIT_OBJECT_0);
			}
        }

printf("check status\n");
//check status
		Port = 0;
        if(DeviceIoControl(hESCC, ESCC_STATUS,
                &Port,sizeof(DWORD),
				&stat, sizeof(unsigned long),
                &cbBytesReturned, NULL) )
		//returns true with unsigned long status or (reads and clears the internal status value)
		//ERROR_INVALID_PARAMETER if DWORD and unsigned long not passed
		//ERROR_BAD_PORT_NUMBER if the port is out of range
		{
		if(cbBytesReturned!=sizeof(unsigned long))printf("bad returnsize:%lu\n",cbBytesReturned);
		decode_stat(stat);
		}
		else
			{
            dwErrorCode = GetLastError();
			decode_err(dwErrorCode);
			}

printf("read frame(should get 3001 bytes)\n");//to get write #2
		Port = 0;
        if ( DeviceIoControl(hESCC, ESCC_READ,&Port, sizeof(DWORD),buffer, sizeof(buffer),&cbBytesReturned, &osr) )
        {
			for(i=0;i<cbBytesReturned;i++) printf("%c",buffer[i]);
			printf("\n");
			printf("Read returned :%lu bytes  ---  ",cbBytesReturned);
			if(cbBytesReturned!=0)
			{
			if(buffer[cbBytesReturned-1]&0x80)printf("Valid ;");
			else printf("Invalid ;");
			printf("CRC..");
			if(buffer[cbBytesReturned-1]&0x20)printf("OK ;");
			else printf("Failed ;");
			if(buffer[cbBytesReturned-1]&0x40)printf("RDO ;");
			}
			printf("\n");
        }
        else
        {
            dwErrorCode = GetLastError();
			decode_err(dwErrorCode);
			//io still pending
			if(dwErrorCode==ERROR_IO_PENDING)
			{
			do
				{
				k = WaitForSingleObject(osr.hEvent,2000);
				if(k==WAIT_TIMEOUT) printf("Read Timeout\n");
				if(k==WAIT_OBJECT_0)
					{
					printf("WAIT_OBJECT_0\n");
					GetOverlappedResult(hESCC,&osr,&olbytesread,TRUE);
					printf("post GET_OVERLAPPED_RESULT\n");
						for(i=0;i<olbytesread;i++) printf("%c",buffer[i]);
						printf("\n");
						printf("Read returned :%lu bytes ---  ",olbytesread);
						if(olbytesread!=0)
						{
						if(buffer[olbytesread-1]&0x80)printf("Valid ;");
						else printf("Invalid ;");
						printf("CRC..");
						if(buffer[olbytesread-1]&0x20)printf("OK ;");
						else printf("Failed ;");
						if(buffer[olbytesread-1]&0x40)printf("RDO ;");
						}
						printf("\n");
					}
				}while(k!=WAIT_OBJECT_0);
			}

		}

printf("check status\n");
//check status
		Port = 0;
        if(DeviceIoControl(hESCC, ESCC_STATUS,
                &Port,sizeof(DWORD),
				&stat, sizeof(unsigned long),
                &cbBytesReturned, NULL) )
		//returns true with unsigned long status or (reads and clears the internal status value)
		//ERROR_INVALID_PARAMETER if DWORD and unsigned long not passed
		//ERROR_BAD_PORT_NUMBER if the port is out of range
		{
		if(cbBytesReturned!=sizeof(unsigned long))printf("bad returnsize:%lu\n",cbBytesReturned);
		decode_stat(stat);
		}
		else
			{
            dwErrorCode = GetLastError();
			decode_err(dwErrorCode);
			}

printf("read frame(should get 2001 bytes)\n");//to get write #3
		Port = 0;
        if ( DeviceIoControl(hESCC, ESCC_READ,&Port, sizeof(DWORD),buffer, sizeof(buffer),&cbBytesReturned, &osr) )
        {
			for(i=0;i<cbBytesReturned;i++) printf("%c",buffer[i]);
			printf("\n");
			printf("Read returned :%lu bytes  ---  ",cbBytesReturned);
			if(cbBytesReturned!=0)
			{
			if(buffer[cbBytesReturned-1]&0x80)printf("Valid ;");
			else printf("Invalid ;");
			printf("CRC..");
			if(buffer[cbBytesReturned-1]&0x20)printf("OK ;");
			else printf("Failed ;");
			if(buffer[cbBytesReturned-1]&0x40)printf("RDO ;");
			}
			printf("\n");
        }
        else
        {
            dwErrorCode = GetLastError();
			decode_err(dwErrorCode);
			//io still pending
			if(dwErrorCode==ERROR_IO_PENDING)
			{
			do
				{
				k = WaitForSingleObject(osr.hEvent,2000);
				if(k==WAIT_TIMEOUT) printf("Read Timeout\n");
				if(k==WAIT_OBJECT_0)
					{
					printf("WAIT_OBJECT_0\n");
					GetOverlappedResult(hESCC,&osr,&olbytesread,TRUE);
					printf("post GET_OVERLAPPED_RESULT\n");
						for(i=0;i<olbytesread;i++) printf("%c",buffer[i]);
						printf("\n");
						printf("Read returned :%lu bytes ---  ",olbytesread);
						if(olbytesread!=0)
						{
						if(buffer[olbytesread-1]&0x80)printf("Valid ;");
						else printf("Invalid ;");
						printf("CRC..");
						if(buffer[olbytesread-1]&0x20)printf("OK ;");
						else printf("Failed ;");
						if(buffer[olbytesread-1]&0x40)printf("RDO ;");
						}
						printf("\n");
					}
				}while(k!=WAIT_OBJECT_0);
			}

		}

printf("check status\n");
//check status
		Port = 0;
        if(DeviceIoControl(hESCC, ESCC_STATUS,
                &Port,sizeof(DWORD),
				&stat, sizeof(unsigned long),
                &cbBytesReturned, NULL) )
		//returns true with unsigned long status or (reads and clears the internal status value)
		//ERROR_INVALID_PARAMETER if DWORD and unsigned long not passed
		//ERROR_BAD_PORT_NUMBER if the port is out of range
		{
		if(cbBytesReturned!=sizeof(unsigned long))printf("bad returnsize:%lu\n",cbBytesReturned);
		decode_stat(stat);
		}
		else
			{
            dwErrorCode = GetLastError();
			decode_err(dwErrorCode);
			}


printf("Press a key to send again, [esc] to exit\n");
key = getch();
}while(key!=27);

end:
printf("exiting prog\n");
        // Dynamically UNLOAD the C Virtual Device sample.
		CloseHandle(osr.hEvent);
		CloseHandle(osw.hEvent);
        CloseHandle(hESCC);
    }
    return(0);
}

void decode_stat(unsigned long stat)
{
if(stat==0)return;
printf("STATUS DECODE:\n");
if(stat&ST_RX_DONE) printf("Receive Done\n");
if(stat&ST_OVF) printf("RX BUFFERS overflow\n");
if(stat&ST_RFS) printf("Receive Frame Start\n");
if(stat&ST_RX_TIMEOUT) printf("RX timeout\n");
if(stat&ST_RSC) printf("Receive Status Change\n");
if(stat&ST_PERR) printf("Parity Error\n");
if(stat&ST_PCE) printf("Protocol Error\n");
if(stat&ST_FERR) printf("Framing Error\n");
if(stat&ST_SYN) printf("SYN detect\n");
if(stat&ST_DPLLA) printf("DPLL asyncronous\n");
if(stat&ST_CDSC) printf("CD changed state\n");
if(stat&ST_RFO) printf("Receive Frame Overflow\n");
if(stat&ST_EOP) printf("End of Poll\n");
if(stat&ST_BRKD) printf("Break Detected\n");
if(stat&ST_ONLP) printf("On Loop\n");
if(stat&ST_BRKT) printf("Break Terminated\n");
if(stat&ST_ALLS) printf("All Sent\n");
if(stat&ST_EXE) printf("Transmit Underrun\n");
if(stat&ST_TIN) printf("Timer Expired\n");
if(stat&ST_CTSC) printf("CTS changed state\n");
if(stat&ST_XMR) printf("Transmit Message Repeat\n");
if(stat&ST_TX_DONE) printf("Transmit Done\n");
if(stat&ST_DMA_TC) printf("DMA TC reached\n");
if(stat&ST_DSR0C) printf("DSR0 changed state\n");
if(stat&ST_DSR1C) printf("DSR1 changed state\n");
if(stat&TX_FUBAR) printf("TX is FUBAR\n");
}

void decode_err(DWORD err)
{
if(err==ERROR_SUCCESS) return;
switch (err)
{
case ERROR_IO_PENDING:
	printf("The I/O operation is pending\n");
	break;
case ERROR_INVALID_PARAMETER:
	printf("The parameter given was invalid.\n");
	break;
case ERROR_BAD_PORT_NUMBER:
	printf("Invalid port number.\n");
	break;
case ERROR_NO_DATA_RECEIVED:
	printf("No received frames ready.\n");
	break;
case ERROR_USER_BUFFER_TOO_SMALL:
	printf("Received frame is larger than user buffer.\n");
	break;
case ERROR_TRANSMIT_FRAME_TOO_LARGE:
	printf("Tx frame must be in range 1 -> 4096 bytes\n");
	break;
case ERROR_TRANSMIT_BUFFERS_FULL:
	printf("Tx buffers full; try again later.\n");
	break;
case ERROR_TRANSMITTER_LOCKED_UP:
	printf("Transmitter is in locked up state; call ESCC_FLUSH_TX and try again\n");
	break;
case ERROR_CEC_TIMEOUT:
	printf("There is a command executing.\n");
	printf("The two most likely causes:\n");
	printf("1.  No clock source for the core(no txclk in non master mode)\n");
	printf("2.  Very slow baud rate in non master mode\n");
	break;
case ERROR_MAX_BOARDS_INSTALLED:
	printf("Maximum boards allready in use\n");
	break;
case ERROR_IRQ_IN_USE:
	printf("Unable to virtualize the irq specified\n");
	break;
case ERROR_NO_PORTS_DEFINED:
	printf("The driver does not have any ports defined\n");
	break;
case ERROR_BOARD_EXISTS:
	printf("The board allready exists\n");
	break;
case ERROR_TOO_MANY_RBUFS:
	printf("You requested too many receive buffers\n");
	break;
case ERROR_TOO_MANY_TBUFS:
	printf("You requested too many transmit buffers\n");
	break;
case ERROR_MAX_FRAME_SIZE:
	printf("The maximum frame size of 4k was exceeded\n");
	break;
case ERROR_MEM_ALLOCATION:
	printf("While allocating memory an error occured\n");
	break;
case ERROR_MIN_FRAME_SIZE:
	printf("the minimum frame size of 32 bytes was violated\n");
	break;

}
}
