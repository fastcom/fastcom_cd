/*
SDLCMON.cpp --a simple sdlc monitor
This program uses the Fastcom:ESCC card to display SLDC frames.
It assumes that the RT+/- lines and the RD+/- lines are connected to a SDLC data source.
It receives sdlc frames from port 0 and displays them to the screen until a key is pressed

Author: Carl George
Date  : 7/7/99
Compiler: Visual C/C++ v 1.52c

*/
#include "cescc.h"
#include "conio.h"
#include "stdlib.h"
#include "stdio.h"
#include "bios.h"
#include "dos.h"
#include "string.h"
#include "graph.h"


void main(int argc, char *argv[])
{          
Cescc esccdrv;
char buffer[4096];
struct escc_regs settings;
unsigned i,j,k;
unsigned porta;
unsigned portb;


_clearscreen(_GCLEARSCREEN);
printf("FASTCOM:ESCC SDLC MONITOR\r\n");
printf("\r\n");

porta = esccdrv.add_port(0x280,0,5,0,0); //interrupt mode
//porta = esccdrv.add_port(0x280,0,5,1,7); //using dma
portb = esccdrv.add_port(0x280,1,5,0,0);

esccdrv.set_clock_generator(porta,0xba1c60,24);
printf("CLOCK = 8M\r\n");

//HDLC settings

settings.mode = 0x88;//transparent mode 0
settings.timr = 0x1f;//not used
settings.xbcl = 0x00;//not used
settings.xbch = 0x00;//not used (change to 0x80 if using DMA)
settings.ccr0 = 0x80;//power up, NRZ, HDLC
settings.ccr1 = 0x10;//clock mode 0, idle = '1'
settings.ccr2 = 0x38;//clock mode 0b (the b part), BDF = 1 (not inclk/1), txclk is output
settings.ccr3 = 0x00;//not used
settings.bgr =  0x07; //rate = inclock/(N+1)*2 , 8E6/(7+1)*2 = 500000
settings.iva = 0;//forced
settings.ipc = 0x03;//forced
settings.imr0 = 0x04;//unless you have a physical connection on DCD
settings.imr1 = 0x0;//
settings.pvr = 0x0;//
settings.pim = 0xff;//
settings.pcr = 0xe0;//forced
settings.xad1 = 0xff;//not used
settings.xad2 = 0xff;//not used
settings.rah1 = 0xff;//not used
settings.rah2 = 0xff;//not used
settings.ral1 = 0xff;//not used
settings.ral2 = 0xff;//not used
settings.rlcr = 0x00;//not used, can be used to abort frames received larger than specified
settings.pre = 0x00;//not used


j = esccdrv.init_port(portb,OPMODE_HDLC,&settings,10,10);
if(j==TRUE)printf("Initializing port 1 OK\n\r");
if(j==FALSE)printf("Initializing port 1 FAILED\n\r");
esccdrv.set_tx_type(portb,TRANSPARENT_MODE);
j = esccdrv.init_port(porta,OPMODE_HDLC,&settings,10,10);
if(j==TRUE)printf("Initializing port 0 OK\n\r");
if(j==FALSE)printf("Initializing port 0 FAILED\n\r");
esccdrv.set_tx_type(porta,TRANSPARENT_MODE);

esccdrv.clear_rx_buffer(porta);
esccdrv.clear_tx_buffer(porta);


do
{
/*  
//unremark this if you want to have 82532 status messages interspersed
j = 0;
while( (j&RX_READY) !=RX_READY )
		{
		j = esccdrv.get_port_status(porta);
		printf ("waiting...%4.4x\r",j);
		if(j!=0) 
			{                                                
			printf("\n\r");
			if((j&XMR_INTERRUPT)==XMR_INTERRUPT) printf("XMR\n\r");
			if((j&PCE_INTERRUPT)==PCE_INTERRUPT) printf("PCE\n\r");
			if((j&EXE_INTERRUPT)==EXE_INTERRUPT) printf("EXE\n\r");
			if((j&RFO_INTERRUPT)==RFO_INTERRUPT) printf("RFO\n\r");
			if((j&CTS_INTERRUPT)==CTS_INTERRUPT) printf("CTS\n\r");
			if((j&RFS_INTERRUPT)==RFS_INTERRUPT) printf("RFS\n\r");
			if((j&RX_BUFFER_OVERFLOW)==RX_BUFFER_OVERFLOW) printf("RX_BUFFER_OVERFLOW\n\r");
			if((j&RSC_INTERRUPT)==RSC_INTERRUPT) printf("RSC\n\r");
            if((j&TIMER_INTERRUPT)==TIMER_INTERRUPT) printf("TIMER\n\r");
            if((j&RX_READY)==RX_READY) printf("RX READY\n\r");
            if((j&PARITY_ERROR)==PARITY_ERROR) printf("PARITY ERROR\n\r");
            if((j&FRAMING_ERROR)==FRAMING_ERROR) printf("FRAMING ERROR\n\r");
            if((j&BREAK_DETECTED)==BREAK_DETECTED) printf("BREAK DETECTED\n\r");
            if((j&BREAK_TERMINATED)==BREAK_TERMINATED) printf("BREAK TERMINATED\n\r");
            if((j&ALLSENT_INTERRUPT)==ALLSENT_INTERRUPT) printf("ALLSENT INTERRUPT\n\r");
			}
		//if(kbhit()!=0) return;
		}
printf("\n\r");

*/
while((j = esccdrv.rx_port(porta,buffer,4096))==0);//add a printf("\r"); here if you want to be able to break out.
if(j>=2)
{
printf("address:0x%x\r\n",buffer[0]&0xff);
printf("control:0x%x\r\n",buffer[1]&0xff);              
if((buffer[1]&0x01)==0)
	{
	printf("I Frame, NR = %u, NS = %u p/f = %u\r\n",(buffer[1]&0xe0)>>5,(buffer[1]&0x0e)>>1,(buffer[1]&0x10)>>4);
	}     
if((buffer[1]&0x0f)==0x01)
	{
	printf("S-Frame-RR: NR = %u, p/f = %u\r\n",(buffer[1]&0xe0)>>5,(buffer[1]&0x10)>>4);
	}	
if((buffer[1]&0x0f)==0x05)
	{
	printf("S-Frame-RNR: NR = %u, p/f = %u\r\n",(buffer[1]&0xe0)>>5,(buffer[1]&0x10)>>4);
	}	
if((buffer[1]&0x0f)==0x09)
	{
	printf("S-Frame-REJ: NR = %u, p/f = %u\r\n",(buffer[1]&0xe0)>>5,(buffer[1]&0x10)>>4);
	}	
if((buffer[1]&0xff)==0x03) printf("Nonsequenced-NSI p/f = 0\r\n");
if((buffer[1]&0xff)==0x13) printf("Nonsequenced-NSI p/f = 1\r\n");
if((buffer[1]&0xff)==0x07) printf("Nonsequenced-RQI\r\n");
if((buffer[1]&0xff)==0x17) printf("Nonsequenced-SIM\r\n");
if((buffer[1]&0xff)==0x93) printf("Nonsequenced-SNRM\r\n");
if((buffer[1]&0xff)==0x0F) printf("Nonsequenced-ROL\r\n");
if((buffer[1]&0xff)==0x53) printf("Nonsequenced-DISC\r\n");
if((buffer[1]&0xff)==0x63) printf("Nonsequenced-NSA\r\n");
if((buffer[1]&0xff)==0x87) printf("Nonsequenced-CMDR\r\n");
if((buffer[1]&0xff)==0x33) printf("Nonsequenced-ORP\r\n");



printf("data:\r\n");
for(i=2;i<j-1;i++) printf("0x%x,",buffer[i]&0xff);
printf("\r\n");	
}
printf("RSTA:0x%x\r\n",buffer[j-1]&0xff);
if((buffer[j-1]&0x20)==0x20) printf("CRC passed\r\n");
else printf("CRC failed\r\n");
if((buffer[j-1]&0x40)==0x40) printf("Receive Data Overflow (hardware)\r\n");
if((buffer[j-1]&0x80)==0x00) printf("Invalid frame\r\n");
if((buffer[j-1]&0x10)==0x10) printf("Receive Frame Abort indicated\r\n");
printf("\r\n");

}while(!kbhit());
getch();
}

