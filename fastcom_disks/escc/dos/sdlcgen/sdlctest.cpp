/*
SDLCTEST.cpp --a simple sdlc frame generator
This program uses the Fastcom:ESCC card to send SLDC frames.
It assumes that the TT+/- lines and the SD+/- lines will be used to inject SDLC frames.

Author: Carl George
Date  : 7/7/99
Compiler: Visual C/C++ v 1.52c

*/
#include "cescc.h"
#include "conio.h"
#include "stdlib.h"
#include "stdio.h"
#include "bios.h"
#include "dos.h"
#include "string.h"
#include "graph.h"


void main(int argc, char *argv[])
{          
Cescc esccdrv;
char buffer[4096];
struct escc_regs settings;
unsigned i,j,k;
unsigned porta;
unsigned portb;     
unsigned ns,nr,pf;
unsigned control,address;

_clearscreen(_GCLEARSCREEN);
printf("FASTCOM:ESCC SDLC MONITOR\r\n");
printf("\r\n");

porta = esccdrv.add_port(0x280,0,5,0,0); //interrupt mode
//porta = esccdrv.add_port(0x280,0,5,1,7); //using dma
portb = esccdrv.add_port(0x280,1,5,0,0);

esccdrv.set_clock_generator(porta,0xba1c60,24);
printf("CLOCK = 8M\r\n");

//HDLC settings

settings.mode = 0x88;//transparent mode 0
settings.timr = 0x1f;//not used
settings.xbcl = 0x00;//not used
settings.xbch = 0x00;//not used (change to 0x80 if using DMA)
settings.ccr0 = 0x80;//power up, NRZ, HDLC
settings.ccr1 = 0x10;//clock mode 0, idle = '1'
settings.ccr2 = 0x38;//clock mode 0b (the b part), BDF = 1 (not inclk/1), txclk is output
settings.ccr3 = 0x00;//not used
settings.bgr =  0x07; //rate = inclock/(N+1)*2 , 8E6/(7+1)*2 = 500000
settings.iva = 0;//forced
settings.ipc = 0x03;//forced
settings.imr0 = 0x04;//unless you have a physical connection on DCD
settings.imr1 = 0x0;//
settings.pvr = 0x0;//
settings.pim = 0xff;//
settings.pcr = 0xe0;//forced
settings.xad1 = 0xff;//not used
settings.xad2 = 0xff;//not used
settings.rah1 = 0xff;//not used
settings.rah2 = 0xff;//not used
settings.ral1 = 0xff;//not used
settings.ral2 = 0xff;//not used
settings.rlcr = 0x00;//not used, can be used to abort frames received larger than specified
settings.pre = 0x00;//not used


j = esccdrv.init_port(portb,OPMODE_HDLC,&settings,10,10);
if(j==TRUE)printf("Initializing port 1 OK\n\r");
if(j==FALSE)printf("Initializing port 1 FAILED\n\r");
esccdrv.set_tx_type(portb,TRANSPARENT_MODE);
j = esccdrv.init_port(porta,OPMODE_HDLC,&settings,10,10);
if(j==TRUE)printf("Initializing port 0 OK\n\r");
if(j==FALSE)printf("Initializing port 0 FAILED\n\r");
esccdrv.set_tx_type(porta,TRANSPARENT_MODE);

esccdrv.clear_rx_buffer(porta);
esccdrv.clear_tx_buffer(porta);


do
{
printf("send I, S, N(U) frame?\r\n");
i = getch();
if((i=='i')||(i=='I'))
	{
	printf("enter NS:");
	scanf("%u",&ns);	
	printf("\r\n");
	printf("enter NR:");
	scanf("%u",&nr);	
	printf("\r\n");
	printf("enter p/f:");
	scanf("%u",&pf);	
	printf("\r\n");
	ns = ns &0x07;//mask off unused.
	nr = nr &0x07;//mask off unused.
	pf = pf &0x01;//mask off unused.
	printf("enter address:");
	scanf("%x",&address);
	printf("\r\n");
	buffer[0] = address;//set address field
	buffer[1] = (nr<<5)|(pf<<4)|(ns<<1);//build control field		
	printf("enter info:([esc] to send)\r\n");
	k  = 2;
	do
		{
		j = getche();
		if(j!=27)buffer[k++]= j;
		}while(j!=27);
	esccdrv.tx_port(porta,buffer,k);
	}
if((i=='s')||(i=='S'))
	{
	printf("1. RR\r\n");
	printf("2. RNR\r\n");
	printf("3. REJ\r\n");
	j = getch();

	printf("enter NR:");
	scanf("%u",&nr);	
	printf("\r\n");
	printf("enter p/f:");
	scanf("%u",&pf);	
	printf("\r\n");
	nr = nr &0x07;//mask off unused.
	pf = pf &0x01;//mask off unused.
	printf("enter address:");
	scanf("%x",&address);
	printf("\r\n");
	buffer[0] = address;//set address field
		
	if(j =='1')
	{
	buffer[1] = (nr<<5)|(pf<<4)|1;
	}
	if(j=='2')
	{
	buffer[1] = (nr<<5)|(pf<<4)|5;
	}
	if(j=='3')
	{
	buffer[1] = (nr<<5)|(pf<<4)|9;
	}                            
	esccdrv.tx_port(porta,buffer,2);
	}
if((i=='n')||(i=='N')||(i=='u')||(i=='U'))
	{
	printf("1. NSI\r\n");
	printf("2. RQI\r\n");
	printf("3. SIM\r\n");
	printf("4. SNRM\r\n");
	printf("5. ROL\r\n");
	printf("6. DISC\r\n");
	printf("7. NSA\r\n");
	printf("8. CMDR\r\n");
	printf("9. ORP\r\n");
	j = getch();

	printf("enter address:");
	scanf("%x",&address);
	printf("\r\n");
	buffer[0] = address;//set address field

	if(j=='1')
		{
		printf("enter p/f:");
		scanf("%u",&pf);	
		printf("\r\n");
		pf = pf &0x01;//mask off unused.
		buffer[1] = (pf<<4)|3;
		printf("enter info:([esc] to send)\r\n");
		k  = 2;
		do
			{
			j = getche();
			if(j!=27)buffer[k++]= j;
			}while(j!=27);
		printf("\r\n");
		esccdrv.tx_port(porta,buffer,k);
		}
	if(j=='2')
		{
		buffer[1] = 0x07;
		esccdrv.tx_port(porta,buffer,2);
		}		
	if(j=='3')
		{
		buffer[1] = 0x17;
		esccdrv.tx_port(porta,buffer,2);
		}		
	if(j=='4')
		{
		buffer[1] = 0x93;
		esccdrv.tx_port(porta,buffer,2);
		}		        
	if(j=='5')
		{
		buffer[1] = 0x0f;
		esccdrv.tx_port(porta,buffer,2);
		}		        
	if(j=='6')
		{
		buffer[1] = 0x53;
		esccdrv.tx_port(porta,buffer,2);
		}		        
	if(j=='7')
		{
		buffer[1] = 0x63;
		esccdrv.tx_port(porta,buffer,2);
		}		        
	if(j=='8')
		{
		buffer[1] = 0x87;
		esccdrv.tx_port(porta,buffer,2);
		}	
	if(j=='9')
		{
		buffer[1] = 0x33;
		esccdrv.tx_port(porta,buffer,2);
		}		
	}
}while(i!=27);		
}

