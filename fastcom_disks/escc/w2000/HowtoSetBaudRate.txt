So that you understand what is going on when you want to set a particular 
baud rate, I will explain how the board works.

The board has a programmable clock generator that can be set to any 
frequency between 391K and 33MHz (391K is the minimum of the clock 
chip and 33MHz is the max of the controller input).  This frequency 
can either be used directly to attain a given baud rate, or it can be 
fed into the internal baud rate generator of the 82532 and divided down 
to get a different baud rate.

For the purposes of this document, we will set a baud rate of 256 Kbits/s.

The rate of 256K will need to come out of the baud rate generator.  To 
do this you must calculate a clock rate that can be divided evenly down 
to 256K.  I happen to know that if use a clock rate of 18.432 MHz and 
divide it by 72, you will get 256K as your baud.  As a bonus, this clock 
rate can also be easily divided down to all of the "standard" baud rates 
as well.

So you will have to set the clock to 18.432 MHz and the divisor to 72 in 
order to get the desired 256K baud rate.

To set the clock you can use the code in Escc\nt\escc_rate_set as an example.  
The GUI program from Cypress that can be found on the Fastcom CD in the 
bitcalc directory can be used to find the correct clkset values.  Or, the 
calculate_bits function will do the calculations on your desired frequency 
and fill in the clkset structure correctly. Or, you can just directly use 
the appropriate clkset values if you know them:

clock.clockbits = 0x21D110;	//these values set 18.432 MHz
clock.numbits = 23;

You then use the IOCTL_ESCCDRV_SET_CLOCK (Windows) or ESCC_SET_CLOCK (Linux) 
ioctl calls to send the programming data into the clock chip.

Once you have set the clock, you must set the divisor, this is done by 
setting the BGR register to 35.  The divisor, (k = (N + 1) * 2), where N 
is the values BR9:BR0 (0 to 1023).  So 35 will get you a divisor of 72.  
You can refer to pg 144 of the 82532 manual for a more detailed description.

If you are unable to get you clock value using the standard BGR mode, you 
can use enhanced mode as described on pg 144.  Enhanced mode can be enabled 
in CCR4 setting bit EBRG = 1.  The equation for enhanced mode is 
k = (N + 1) * 2^M, where N is the values BR5:BR0 (0 to 63), and M is 
the values BR9:BR6.