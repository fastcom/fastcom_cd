// escctoolbox.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "stdlib.h"
#include "string.h"
#include "stdio.h"
#include "escctoolbox.h"

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
			break;
		case DLL_THREAD_ATTACH:
			break;
		case DLL_THREAD_DETACH:
			break;
		case DLL_PROCESS_DETACH:
			break;
    }
    return TRUE;
}


ULONG refcount = 0;

readcallbackfn readcallbacks[MAXPORTS]= {0};
void (__stdcall *writecallbacks[MAXPORTS])(DWORD port,char * tbuf,DWORD *bytestowrite)= {NULL};
void (__stdcall *statuscallbacks[MAXPORTS])(DWORD port,DWORD status)= {NULL};

static HANDLE hESCC[MAXPORTS] = {NULL};
BOOL connected[MAXPORTS] = {0};
DWORD statusmask[MAXPORTS] = {0};
DWORD readsize[MAXPORTS] = {0};
DWORD maxwritesize[MAXPORTS] = {0};
HANDLE hReadThread[MAXPORTS] = {NULL};
HANDLE hWriteThread[MAXPORTS] = {NULL};
HANDLE hStatusThread[MAXPORTS] = {NULL};
static DWORD refcnt[MAXPORTS] = {0};
HMODULE mymod[MAXPORTS]= {NULL};




DWORD FAR PASCAL StatusProc( DWORD port );
DWORD FAR PASCAL ReadProc( DWORD port );
DWORD FAR PASCAL WriteProc( DWORD port );


ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Create(DWORD port)
{
char devname[25];
ULONG onoff;
ULONG temp;

if(port>MAXPORTS) return -1;
if(hESCC[port]!=NULL) return -2;
sprintf(devname,"\\\\.\\ESCC%d",port);
        hESCC[port] = CreateFile (devname,
			  GENERIC_READ | GENERIC_WRITE,
			  FILE_SHARE_READ | FILE_SHARE_WRITE,
			  NULL,
			  OPEN_ALWAYS,
			  FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
			  NULL
			  );

    if (hESCC[port] == INVALID_HANDLE_VALUE)
    {
//	MessageBox(NULL,"create failed\n","",MB_OK);
	return -3;
	}
onoff=0;//has to be...
DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_BLOCK_MULTIPLE_IO,&onoff,sizeof(ULONG),NULL,0,&temp,NULL);

return 0;

}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Destroy(DWORD port)
{
DWORD ret;
if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;


	connected[port]=0;
	if(hReadThread[port]!=NULL)
		{
		DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_CANCEL_RX,NULL,0,NULL,0,&ret,NULL);
		Sleep(0);
		CloseHandle(hReadThread[port]);
		}
	if(hWriteThread[port]!=NULL)
		{
		DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_CANCEL_TX,NULL,0,NULL,0,&ret,NULL);
		Sleep(0);
		CloseHandle(hWriteThread[port]);
		}
	if(hStatusThread[port]!=NULL)
		{
		DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_CANCEL_STATUS,NULL,0,NULL,0,&ret,NULL);
		Sleep(0);
		CloseHandle(hStatusThread[port]);
		}
	CloseHandle(hESCC[port]);
	hReadThread[port]=NULL;
	hWriteThread[port]=NULL;
	hStatusThread[port]=NULL;
	hESCC[port]=NULL;
return 0;

}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Set_Clock(DWORD port,DWORD frequency,DWORD clocktype)
{
DWORD temp;
ULONG actualf;
ULONG passval[2];

if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;

passval[0] = frequency;
if(	DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_SET_FREQ,&frequency,sizeof(ULONG),&actualf,sizeof(ULONG),&temp,NULL)!=0)
	{
	return actualf;
	}
else return -3;
return -4;
}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Get_Clock(DWORD port)
{
DWORD res;
ULONG passval;

if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;
if(DeviceIoControl(hESCC[port], IOCTL_ESCCDRV_GET_FREQ,NULL,0,&passval,sizeof(ULONG),&res,NULL)!=0)
	{
	return (DWORD)(passval);
	}
else return -3;
}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Read_Frame(DWORD port,char * rbuf,DWORD szrbuf,DWORD *retbytes,DWORD timeout)
{
	ULONG t;
	OVERLAPPED  rq;
	int j;
	ULONG temp;

if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;

memset( &rq, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
rq.hEvent = CreateEvent( NULL,TRUE,FALSE,NULL );
if (rq.hEvent == NULL)
	{
	return -3;
	}

t = ReadFile(hESCC[port],rbuf,szrbuf,retbytes,&rq);
if(t==FALSE)  
	{
	t=GetLastError();
	if(t==ERROR_IO_PENDING)
		{
		do
			{
			j = WaitForSingleObject( rq.hEvent, timeout );
			if(j==WAIT_TIMEOUT)
				{
                DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_CANCEL_RX,NULL,0,NULL,0,&temp,NULL);
				retbytes[0] = 0;
				CloseHandle(rq.hEvent);
				return -4;
				}
				if(j==WAIT_ABANDONED)
				{
				retbytes[0] = 0;
				CloseHandle(rq.hEvent);
				return -5;
				}
			} while(j!=WAIT_OBJECT_0);
		GetOverlappedResult(hESCC[port],&rq,retbytes,TRUE);
		}
	else 
		{
//		char buf[256];
//		sprintf(buf,"%8.8x error:%8.8x\n",hSFC[port],ret);
//		MessageBox(NULL,buf,"ERROR",MB_OK);

		retbytes[0] = 0;
		CloseHandle(rq.hEvent);
		return -6;
		}
	}
CloseHandle(rq.hEvent);
return 0;


}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Write_Frame(DWORD port,char * tbuf,DWORD numbytes,DWORD timeout)
{
OVERLAPPED  wq;
BOOL t;
DWORD nobyteswritten;
DWORD j;
DWORD ret;
DWORD temp;

if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;

memset( &wq, 0, sizeof( OVERLAPPED ) );
wq.hEvent = CreateEvent( NULL,TRUE,FALSE,NULL);
if (wq.hEvent == NULL)
	{
	return -3;
	}

t = WriteFile(hESCC[port],tbuf,numbytes,&nobyteswritten,&wq);
if(t==FALSE)  
	{
	ret=GetLastError();
	if(ret==ERROR_IO_PENDING)
		{
		do
			{
			j = WaitForSingleObject( wq.hEvent, timeout );
			if(j==WAIT_TIMEOUT)
				{
                DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_CANCEL_TX,NULL,0,NULL,0,&temp,NULL);
				CloseHandle(wq.hEvent);
				return -4;
				}
				if(j==WAIT_ABANDONED)
				{
				CloseHandle(wq.hEvent);
				return -5;
				}
			} while(j!=WAIT_OBJECT_0);
		GetOverlappedResult(hESCC[port],&wq,&nobyteswritten,TRUE);
		}
	else
		{
//		char buf[256];
//		sprintf(buf,"%8.8x error:%8.8x\n",hSFC[port],ret);
//		MessageBox(NULL,buf,"ERROR",MB_OK);
		CloseHandle(wq.hEvent);
		return -6;
		}
	}
CloseHandle(wq.hEvent);
return 0;

}


ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Get_Status(DWORD port,DWORD *status,DWORD mask,DWORD timeout)
{
	ULONG t;
	ULONG temp;
	OVERLAPPED  st;
	DWORD j;

status[0]=0;
if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;

memset( &st, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
st.hEvent = CreateEvent( NULL,TRUE,FALSE,NULL);
if (st.hEvent == NULL)
	{
	return -3;
	}


t=	DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_STATUS,&mask,sizeof(ULONG),status,sizeof(ULONG),&temp,&st);
if(t==FALSE)  
	{
	t=GetLastError();
	if(t==ERROR_IO_PENDING)
		{
		do
			{
			j = WaitForSingleObject( st.hEvent, timeout );
			if(j==WAIT_TIMEOUT)
				{
                DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_CANCEL_STATUS,NULL,0,NULL,0,&temp,NULL);
				CloseHandle(st.hEvent);
				return -4;
				}
			if(j==WAIT_ABANDONED)
				{
				CloseHandle(st.hEvent);
				return -5;
				}
			} while(j!=WAIT_OBJECT_0);
		GetOverlappedResult(hESCC[port],&st,&temp,TRUE);		
		}
	else
		{
//		char buf[256];
//		sprintf(buf,"%8.8x error:%8.8x\n",hSFC[port],ret);
//		MessageBox(NULL,buf,"ERROR",MB_OK);

		CloseHandle(st.hEvent);
		return -6;
		}
	}
CloseHandle(st.hEvent);
return 0;
}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Get_IStatus(DWORD port,DWORD *status)
{
BOOL t;
DWORD temp;
DWORD mask = 0xFFFFFFFF;

status[0]=0;
if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;

t = DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_STATUS,&mask,sizeof(ULONG),status,sizeof(ULONG),&temp,NULL);
if(t) return 0;
else return -3;
}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Register_Read_Callback(DWORD port,readcallbackfn fnptr,DWORD szread)
{

HANDLE            hreadThread ;	//handle to read thread
DWORD         dwThreadID ;		//temp Thread ID storage
if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;
readsize[port] = szread;
readcallbacks[port] =  fnptr;
connected[port]|=1;
//create read thread here.



hreadThread =     CreateThread( (LPSECURITY_ATTRIBUTES) NULL,
				    0, 
				    (LPTHREAD_START_ROUTINE) ReadProc,
				    (LPVOID) port,
				    0, &dwThreadID );

if(hreadThread==NULL)
	{
	readcallbacks[port] = NULL;
	connected[port]&=0xFFFFFFFE;
	return -3;
	}
hReadThread[port] = hreadThread;

return 0;
}


ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Register_Write_Callback(DWORD port,void (__stdcall *callbackfunction)(DWORD port,char * tbuf,DWORD *bytestowrite),DWORD szmaxwrite)
{
HANDLE            hwriteThread ;	//handle to write thread
DWORD         dwThreadID ;		//temp Thread ID storage

if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;
maxwritesize[port] = szmaxwrite;
writecallbacks[port] = callbackfunction;
connected[port]|=2;
//create write thread here


hwriteThread =     CreateThread( (LPSECURITY_ATTRIBUTES) NULL,
				    0, 
				    (LPTHREAD_START_ROUTINE) WriteProc,
				    (LPVOID) port,
				    0, &dwThreadID );

if(hwriteThread==NULL)
	{
	writecallbacks[port] = NULL;
	connected[port]&=0xFFFFFFFD;
	return -3;
	}
hWriteThread[port] = hwriteThread;
return 0;
}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Register_Status_Callback(DWORD port,DWORD mask,void (__stdcall *callbackfunction)(DWORD port,DWORD status))
{
HANDLE            hstatusThread ;	//handle to status thread
DWORD         dwThreadID ;		//temp Thread ID storage

if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;
statusmask[port] = mask;
statuscallbacks[port] = callbackfunction;
connected[port]|=4;
//create status thread here

hstatusThread =     CreateThread( (LPSECURITY_ATTRIBUTES) NULL,
				    0, 
				    (LPTHREAD_START_ROUTINE) StatusProc,
				    (LPVOID) port,
				    0, &dwThreadID );

if(hstatusThread==NULL)
	{
	statuscallbacks[port] = NULL;
	connected[port]&=0xFFFFFFFB;
	return -3;
	}
hStatusThread[port] = hstatusThread;
return 0;
}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Flush_RX(DWORD port)
{
DWORD temp;
if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;

DeviceIoControl(hESCC[port],
                        IOCTL_ESCCDRV_FLUSH_RX,                          /* Device IOCONTROL */
			NULL,								/* write data */
			0,						/* write size */
			NULL,								/* read data */
			0,									/* read size */
			&temp,								/* Returned Size */
			NULL); /* overlap */
return 0;
}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Flush_TX(DWORD port)
{
DWORD temp;
if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;
DeviceIoControl(hESCC[port],
                        IOCTL_ESCCDRV_FLUSH_TX,                          /* Device IOCONTROL */
			NULL,								/* write data */
			0,						/* write size */
			NULL,								/* read data */
			0,									/* read size */
			&temp,								/* Returned Size */
			NULL); /* overlap */
return 0;
}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Read_Register(DWORD port,DWORD regno)
{
DWORD val,temp;
BOOL t;
if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;

	t = DeviceIoControl(hESCC[port],
                        IOCTL_ESCCDRV_READ_REGISTER,                             /* Device IOCONTROL */
			&regno,								/* write data */
			sizeof(unsigned long ),						/* write size */
			&val,								/* read data */
			sizeof(unsigned long),									/* read size */
			&temp,								/* Returned Size */
			NULL); /* overlap */
        if(temp!=0)  return val;
        else 
			{	
			//-1/-2/-3 are all valid returns...
			return -3;
			}

}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Write_Register(DWORD port,DWORD regno,DWORD value)
{
BOOL t;
DWORD passval[2];
DWORD val,temp;
if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;
passval[0] = regno;
passval[1] = value;
	t = DeviceIoControl(hESCC[port],
                        IOCTL_ESCCDRV_WRITE_REGISTER,                            /* Device IOCONTROL */
			&passval,								/* write data */
			2*sizeof(unsigned long ),						/* write size */
			&val,								/* read data */
			sizeof(unsigned long),									/* read size */
			&temp,								/* Returned Size */
			NULL); /* overlap */
        if(t)  return 0;
        else return -3;

}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Configure(DWORD port,SETUP *settings)
{
DWORD ret;
BOOL t;

if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;
//MessageBox(NULL,"In Configure\n","",MB_OK);
t =	DeviceIoControl(hESCC[port],
                        IOCTL_ESCCDRV_SETUP,         /* Device IOCONTROL */
			settings,							/* write data */
			sizeof(SETUP)				,	/* write size */
			NULL,								/* read data */
			0,									/* read size */
			&ret,								/* Returned Size */
			NULL);								/* overlap */
if(t) return 0;
else {
//char buf[512];
//	sprintf(buf,"%8.8x\n%8.8x\n%8.8x",settings->ccr0,settings->ccr1,settings->ccr2);
//	MessageBox(NULL,buf,"CONFIGERR",MB_OK);
	return -3;
}
}


ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Set_Txclk_TT(DWORD port,DWORD onoff)
{
ULONG desreg;
ULONG temp;

if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;

if(DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_GET_FEATURES,NULL,0,&desreg,sizeof(ULONG),&temp,NULL))
	{
	if(onoff==1) desreg &= 0x1f; //turn off TT bit (enables it)
	else desreg |=0x20;          //turn on TT bit (disables it)
	DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_SET_FEATURES,&desreg,sizeof(ULONG),NULL,0,&temp,NULL);
	return 0;
	}
else return -3;
}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Set_Txclk_ST(DWORD port,DWORD onoff)
{
ULONG desreg;
ULONG temp;

if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;

if(DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_GET_FEATURES,NULL,0,&desreg,sizeof(ULONG),&temp,NULL))
	{
	if(onoff==1) desreg &= 0x2f; //turn off ST bit (enables it)
	else desreg |=0x10;          //turn on ST bit (disables it)
	DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_SET_FEATURES,&desreg,sizeof(ULONG),NULL,0,&temp,NULL);
	return 0;
	}
else return -3;
}


ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Set_SD_485(DWORD port,DWORD onoff)
{
ULONG desreg;
ULONG temp;

if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;

if(DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_GET_FEATURES,NULL,0,&desreg,sizeof(ULONG),&temp,NULL))
	{
	if(onoff==1) desreg &= 0x3d; //turn off SD485 bit (enables it)
	else desreg |=0x2;          //turn on SD485 bit (disables it)
	DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_SET_FEATURES,&desreg,sizeof(ULONG),NULL,0,&temp,NULL);
	return 0;
	}
else return -3;
}


ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Set_TT_485(DWORD port,DWORD onoff)
{
ULONG desreg;
ULONG temp;

if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;

if(DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_GET_FEATURES,NULL,0,&desreg,sizeof(ULONG),&temp,NULL))
	{
	if(onoff==1) desreg &= 0x3b; //turn off TT485 bit (enables it)
	else desreg |=0x4;          //turn on TT485 bit (disables it)
	DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_SET_FEATURES,&desreg,sizeof(ULONG),NULL,0,&temp,NULL);
	return 0;
	}
else return -3;
}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Set_RD_Echo_Cancel(DWORD port,DWORD onoff)
{
ULONG desreg;
ULONG temp;

if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;

if(DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_GET_FEATURES,NULL,0,&desreg,sizeof(ULONG),&temp,NULL))
	{
	if(onoff==1) desreg &= 0x3e; //turn off rxechocancel bit (enables it)
	else desreg |=0x1;           //turn on rxechocancel bit (disables it)
	DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_SET_FEATURES,&desreg,sizeof(ULONG),NULL,0,&temp,NULL);
	return 0;
	}
else return -3;
}


ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Set_CTS_Disable(DWORD port,DWORD onoff)
{
ULONG desreg;
ULONG temp;

if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;

if(DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_GET_FEATURES,NULL,0,&desreg,sizeof(ULONG),&temp,NULL))
	{
	if(onoff==1) desreg &= 0x37; //turn off ctsdisable bit (enables it)
	else desreg |=0x8;          //turn on ctsdisable bit (disables it)
	DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_SET_FEATURES,&desreg,sizeof(ULONG),NULL,0,&temp,NULL);
	return 0;
	}
else return -3;
}



ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Bisync_Start_Pattern_Enable(DWORD port,DWORD onoff)
{
ULONG temp;

if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;

if(DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_BISYNC_START_PATTERN_MATCH_EN,&onoff,sizeof(ULONG),NULL,0,&temp,NULL)) return 0;
else return -3;
}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Bisync_Cutoff_Enable(DWORD port,DWORD onoff)
{
ULONG temp;

if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;

if(DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_BISYNC_SIZE_CUTOFF_EN,&onoff,sizeof(ULONG),NULL,0,&temp,NULL)) return 0;
else return -3;
}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_LSB2MSB_Convert_Enable(DWORD port,DWORD onoff)\
{
ULONG temp;

if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;

if(DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_LSB2MSB_CONVERT_EN,&onoff,sizeof(ULONG),NULL,0,&temp,NULL)) return 0;
else return -3;
}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Bisync_End_Pattern_Enable(DWORD port,DWORD onoff)
{
ULONG temp;

if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;

if(DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_BISYNC_END_PATTERN_MATCH_EN,&onoff,sizeof(ULONG),NULL,0,&temp,NULL)) return 0;
else return -3;
}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Bisync_Start_Pattern(DWORD port,struct bisync_start_pattern pattern)
{
ULONG temp;
if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;
if(DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_SET_BISYNC_START_PATTERN,&pattern,sizeof(struct bisync_start_pattern),NULL,0,&temp,NULL)) return 0;
else return -3;

}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Bisync_Size_Cutoff(DWORD port,DWORD size)
{
ULONG temp;
if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;
if(DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_SET_BISYNC_SIZE_CUTOFF_SIZE,&size,sizeof(ULONG),NULL,0,&temp,NULL)) return 0;
else return -3;

}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Bisync_End_Pattern(DWORD port,struct bisync_start_pattern pattern)
{
ULONG temp;
if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;
if(DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_SET_BISYNC_END_PATTERN,&pattern,sizeof(struct bisync_start_pattern),NULL,0,&temp,NULL)) return 0;
else return -3;
}


ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Postamble_Enable(DWORD port,DWORD onoff)
{
ULONG temp;

if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;

if(DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_POSTAMBLE_EN,&onoff,sizeof(ULONG),NULL,0,&temp,NULL)) return 0;
else return -3;
}

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Postamble_Delay(DWORD port,DWORD delay)
{
ULONG temp;
if(port>MAXPORTS) return -1;
if(hESCC[port]==NULL) return -2;
if(DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_POSTAMBLE_DELAY,&delay,sizeof(ULONG),NULL,0,&temp,NULL)) return 0;
else return -3;

}



DWORD FAR PASCAL StatusProc( DWORD port )
{
DWORD k;
BOOL t;
DWORD returnsize;
OVERLAPPED  os ;
DWORD status;

				
memset( &os, 0, sizeof( OVERLAPPED ) );


os.hEvent = CreateEvent( NULL,TRUE,FALSE,NULL );
if (os.hEvent == NULL)
   {
   return -3;
   }
do
	{

	t = DeviceIoControl(hESCC[port],IOCTL_ESCCDRV_STATUS,&statusmask[port],sizeof(ULONG),&status,sizeof(ULONG),&returnsize,&os);
	if(t==FALSE)
		{
		if (GetLastError() == ERROR_IO_PENDING)
			{
			do
				{
				k = WaitForSingleObject( os.hEvent, 5000 );//5 second timeout
				if(k==WAIT_TIMEOUT)
					{
					}
				if(k==WAIT_ABANDONED)
					{
					}
				}while((k!=WAIT_OBJECT_0)&&((connected[port]&4)!=0));//exit if we get signaled or if the main thread quits
				
			if((connected[port]&4)!=0)
				{
				GetOverlappedResult(hESCC[port],&os,&returnsize,TRUE); 
				t=TRUE;
				}
			}
		else
			{
			//actual error, what to do, what to do
			}
		}
	//if we get here then either it was true immediatly, so we return, or we waited and fell through and have the overlapped complete, either way signal it
	if((connected[port]&4)!=0)                   //if not connected then j is invalid
		{
		statuscallbacks[port](port,status);
		}
	}while((connected[port]&4)!=0);              //keep making requests until we want to terminate
CloseHandle( os.hEvent ) ;              //we are terminating so close the event
return 0;                          //done
}


DWORD FAR PASCAL ReadProc( DWORD port )
{
DWORD j;		
BOOL t;			
char *data;	
DWORD nobytestoread;
DWORD nobytesread;	
OVERLAPPED  os ;	

memset( &os, 0, sizeof( OVERLAPPED ) );

os.hEvent = CreateEvent( NULL,TRUE,FALSE,NULL);
if (os.hEvent == NULL)
   {
   return -3 ;
   }

data = (char *)malloc(readsize[port]);

if(data==NULL) return -4;

nobytestoread = readsize[port];

do
	{
	t = ReadFile(hESCC[port],data,nobytestoread,&nobytesread,&os);
	if(t==FALSE)
		{
		if (GetLastError() == ERROR_IO_PENDING)
			{
			do
				{
				j = WaitForSingleObject( os.hEvent, 5000 );//5 second timeout
				if(j==WAIT_TIMEOUT)
					{
					//timeout processing
					}
				if(j==WAIT_ABANDONED)
					{
					//??
					}
				}while((j!=WAIT_OBJECT_0)&&((connected[port]&1)!=0));//stay here until we get signaled or the main thread exits
			if((connected[port]&1)!=0)
					{                                                       
					GetOverlappedResult(hESCC[port],&os,&nobytesread,TRUE); //here to get the actual nobytesread!!!
					t=TRUE;
					}
			}
		else
			{
			//actual read error here...what to do, what to do
			//MessageBox(NULL,"READ ERROR","",MB_OK);
			}
		}
	//if we get here then either it was true immediatly, so we return, or we waited and fell through and have the overlapped complete, either way signal it
	if((connected[port]&1)!=0)
		{
		readcallbacks[port](port,data,nobytesread);
		}
	}while((connected[port]&1)!=0);              //do until we want to terminate
	CloseHandle( os.hEvent ) ;      //done with event
	free(data);
	return 0;                   //outta here
}

DWORD FAR PASCAL WriteProc( DWORD port )
{
OVERLAPPED  wq;
BOOL t;
DWORD nobyteswritten;
DWORD nobytestowrite;
DWORD j;
DWORD ret;
char *data;


memset( &wq, 0, sizeof( OVERLAPPED ) );
wq.hEvent = CreateEvent( NULL,TRUE,FALSE,NULL);
if (wq.hEvent == NULL)
	{
	return -3;
	}
data = (char *)malloc(maxwritesize[port]);
if(data==NULL) return -4;

do
{

if((connected[port]&2)!=0)                   
	{
	writecallbacks[port](port,data,&nobytestowrite);
	}

t = WriteFile(hESCC[port],data,nobytestowrite,&nobyteswritten,&wq);
if(t==FALSE)  
	{
	ret=GetLastError();
	if(ret==ERROR_IO_PENDING)
		{
		do
			{
			j = WaitForSingleObject( wq.hEvent, 5000 );
			if(j==WAIT_TIMEOUT)
				{
				//timeout processing
				}
				if(j==WAIT_ABANDONED)
				{
				//??
				}
			} while((j!=WAIT_OBJECT_0)&&((connected[port]&2)!=0));
		GetOverlappedResult(hESCC[port],&wq,&nobyteswritten,TRUE);
		}
	else
		{
		//write error...what to do, what to do
		}
	}
//returned true, so its done or queued
}while((connected[port]&2)!=0);
CloseHandle(wq.hEvent);
free(data);
return 0;
}
