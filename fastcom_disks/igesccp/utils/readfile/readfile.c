/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
readfile.c -- user mode function to read frames from a escc port and stuff them in a file

usage:
 readfile port file [hdlc]

 The port can be any valid escc port (0,1) 
 if hdlc is present the last byte of every frame is dropped from the file (RSTA byte);
 
*/

#include "windows.h"
#include "..\escctoolbox.h"
#include "stdio.h"
#include "stdlib.h"

void __stdcall esccreadcb(DWORD port,char * rbuf,DWORD retbytes);
FILE *fout;
ULONG frame;
ULONG hdlc;

void main(int argc,char *argv[])
{
int ret;
ULONG port;
frame=0;
hdlc=0;
fout=NULL;

if(argc<2)
	{
	printf("usage:%s port file [hdlc]\r\n",argv[0]);
	exit(1);
	}
port = atoi(argv[1]);
if(argc>2) hdlc=1;
fout = fopen(argv[2],"wb");
if(fout==NULL)
{
printf("cannot open output file:%s\r\n",argv[2]);
exit(1);
}
if((ret = ESCCToolBox_Create(port))<0) 
	{
	printf("create:%d\n",ret);
	exit(0);
	}
ESCCToolBox_Flush_RX(port);
ESCCToolBox_Register_Read_Callback(port,esccreadcb,4096);
printf("paused...any key to exit\n");
getchar();
ESCCToolBox_Destroy(port);
fclose(fout);
}

void __stdcall esccreadcb(DWORD port,char * rbuf,DWORD retbytes)
{
ULONG i;
if(retbytes>0)
	{
	if(hdlc==0)
		{
		i=fwrite(rbuf,1,retbytes,fout);
		if(i!=retbytes) printf("file write error %d!=%d\r\n",i,retbytes);
		}
	else
		{
		i=fwrite(rbuf,1,retbytes-1,fout);
		if(i!=(retbytes-1)) printf("file write error %d!=%d\r\n",i,retbytes);
		}
	if((rbuf[retbytes-1]&0x10)==0x10) printf("Receive Abort error in frame	:%d\r\n",frame);
	if((rbuf[retbytes-1]&0x20)!=0x20) printf("CRC error in frame			:%d\r\n",frame);
	if((rbuf[retbytes-1]&0x40)==0x40) printf("RDO error in frame			:%d\r\n",frame);
	if((rbuf[retbytes-1]&0x80)!=0x80) printf("Invalid frame error in frame	:%d\r\n",frame);
	frame++;
	}
else printf("received 0 byte frame\r\n");
}
/* $Id$ */