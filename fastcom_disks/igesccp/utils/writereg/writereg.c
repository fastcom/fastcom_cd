/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
writereg.c -- user mode function to write to 82532 register

usage:
 writereg port register value

 The port can be any valid escc port (0,1) 
 
*/

#include "windows.h"
#include "..\escctoolbox.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"


int main(int argc, char *argv[])
{
unsigned long port;
DWORD ret;
DWORD reg;
DWORD val;

if(argc<4)
	{
	printf("usage:%s port register value\r\n",argv[0]);
	exit(1);
	}
port = atoi(argv[1]);
sscanf(argv[2],"%x",&reg);
sscanf(argv[3],"%x",&val);

if((ret = ESCCToolBox_Create(port))<0)
	{
	printf("create:%d\n",ret);
	exit(1);
	}
if((ret =ESCCToolBox_Write_Register(port,reg,val))<0)
	{
	printf("Error writing register:%d",ret);
	}
else  printf("reg:%x <= %x\n",reg,val);

ESCCToolBox_Destroy(port);
}
/* $Id$ */