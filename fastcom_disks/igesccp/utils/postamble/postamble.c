/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
postamble.c -- user mode function to enable/disable postamble generation

usage:
 postamble port delay

 The port can be any valid escc port (0,1) 
 
*/

#include "windows.h"
#include "..\escctoolbox.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"


int main(int argc, char *argv[])
{
unsigned long port;
DWORD ret;
DWORD delay;
DWORD onoff =1;

if(argc<3)
	{
	printf("usage:%s port delay [off]\r\n",argv[0]);
	exit(1);
	}
port = atoi(argv[1]);
delay = atol(argv[2]);
if(argc>3) onoff=0;
if((ret = ESCCToolBox_Create(port))<0)
	{
	printf("create:%d\n",ret);
	exit(1);
	}
if((ret =ESCCToolBox_Postamble_Enable(port,onoff))<0)
	{
	printf("error in Postamble Enable:%d\n",ret);
	}
else 
	{
	if(onoff==1)
		{
		if((ret =ESCCToolBox_Postamble_Delay(port,delay))<0)
			{
			printf("error in Postamble Delay:%d\n",ret);
			}
		else printf("postamble delay enabled and set to %lu\n",delay);
		}
	else printf("postamble delay disabled\n");
	}

ESCCToolBox_Destroy(port);
}
/* $Id$ */