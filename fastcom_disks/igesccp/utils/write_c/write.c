/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
write_c.c -- user mode function to write continuously to the escc port (using callback)

usage:
 write_c port

 The port can be any valid escc port (0,1) 
 
*/

#include "windows.h"
#include "..\escctoolbox.h"
#include "stdio.h"
#include "stdlib.h"

void __stdcall esccwritecb(DWORD port,char * tbuf,DWORD *bytestowrite);


void main(int argc,char *argv[])
{
int ret;
ULONG port;
if(argc<2)
	{
	printf("usage:%s port\r\n",argv[0]);
	exit(1);
	}
port = atoi(argv[1]);
if((ret = ESCCToolBox_Create(port))<0) 
	{
	printf("create:%d\n",ret);
	exit(0);
	}
ESCCToolBox_Flush_TX(port);
ret = ESCCToolBox_Register_Write_Callback(port,esccwritecb,4096);
if(ret<0)
	{
	printf("registerwritecallback:%d\n",ret);
	ESCCToolBox_Destroy(port);
	exit(0);
	}
printf("paused...any key to exit\n");
getchar();
ESCCToolBox_Destroy(port);
}

void __stdcall esccwritecb(DWORD port,char * tbuf,DWORD *bytestowrite)
{
ULONG i;

for(i=0;i<1023;i++) tbuf[i] = (char)(i&0xff);
bytestowrite[0] = 1023;

}
/* $Id$ */