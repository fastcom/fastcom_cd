/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
flushrx.c -- user mode function flush ESCC receiver/buffers

usage:
 flushrx port 

 The port can be any valid escc port (0,1) 

*/

#include "windows.h"
#include "..\escctoolbox.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"


int main(int argc, char *argv[])
{
unsigned long port;
DWORD ret;

if(argc<2)
	{
	printf("usage:%s port \r\n",argv[0]);
	exit(1);
	}
port = atoi(argv[1]);

if((ret = ESCCToolBox_Create(port))<0)
	{
	printf("create:%d\n",ret);
	exit(1);
	}
if((ret =ESCCToolBox_Flush_RX(port))<0)
	{
	printf("error in flushrx:%d\n",ret);
	}
else printf("RX flushed\r\n");

ESCCToolBox_Destroy(port);
}
/* $Id$ */