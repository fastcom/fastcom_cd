/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
write.c -- user mode function to write a text string to the escc port

usage:
 write port

 The port can be any valid escc port (0,1) 
 
*/

#include "windows.h"
#include "..\escctoolbox.h"
#include "stdio.h"
#include "stdlib.h"


void main(int argc,char *argv[])
{
DWORD ret;
ULONG port;
char buf[4096];
ULONG count;

if(argc<2)
	{
	printf("usage:%s port\r\n",argv[0]);
	exit(1);
	}
port = atoi(argv[1]);
if((ret = ESCCToolBox_Create(port))<0)
	{
	printf("create:%d\n",ret);
	exit(0);
	}
ESCCToolBox_Flush_TX(port);
printf("Enter text to send:\n");
scanf("%s",buf);
count = strlen(buf);
if((ret = ESCCToolBox_Write_Frame(port,buf,count,1000))<0) printf("write:%d\n",ret);
Sleep(1000);
ESCCToolBox_Destroy(port);
}
/* $Id$ */