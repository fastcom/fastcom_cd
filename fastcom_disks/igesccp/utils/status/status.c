/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
status.c -- user mode function to retrieve ESCC status (irq redirects)

usage:
 status port [mask]

 The port can be any valid escc port (0,1) 
 optional mask, can exclude any status values by making 0 here
 ex. mask = 0xFFFFFFFF & (~ST_CTSC)
 this will make the CTS change in state status not be returned

*/

#include "windows.h"
#include "..\escctoolbox.h"
#include "stdio.h"
#include "stdlib.h"

void __stdcall esccstatuscb(DWORD port,DWORD status);

void main(int argc,char *argv[])
{
int ret;
ULONG port;
ULONG mask;

if(argc<2)
	{
	printf("usage:%s port [mask]\r\n",argv[0]);
	exit(1);
	}
port = atoi(argv[1]);
mask = 0xFFFFFFFF;//default to all unmasked
if(argc>2) sscanf(argv[2],"%x",&mask);
if((ret = ESCCToolBox_Create(port))<0) printf("create:%d\n",ret);
ESCCToolBox_Register_Status_Callback(port,mask,esccstatuscb);
printf("paused...any key to exit\n");
getchar();
ESCCToolBox_Destroy(port);
}

void __stdcall esccstatuscb(DWORD port,DWORD status)
{
if((status&ST_RX_DONE)==ST_RX_DONE) printf("STATUS, RX_DONE\r\n");
if((status&ST_OVF)==ST_OVF) printf("STATUS, BUFFERS overflowed\n\r");
if((status&ST_RFS)==ST_RFS) printf("STATUS, Receive Frame Start\r\n");
if((status&ST_RX_TIMEOUT)==ST_RX_TIMEOUT) printf("STATUS, Receive Timeout\r\n");
if((status&ST_RSC)==ST_RSC) printf("STATUS, Receive Status Change\r\n");
if((status&ST_PERR)==ST_PERR) printf("STATUS, Parity Error\r\n");
if((status&ST_PCE)==ST_PCE) printf("STATUS, Protocol Error\r\n");
if((status&ST_FERR)==ST_FERR) printf("STATUS, Framing Error\r\n");
if((status&ST_SYN)==ST_SYN) printf("STATUS, SYN detected\r\n");
if((status&ST_DPLLA)==ST_DPLLA) printf("STATUS, DPLL Asynchronous\r\n");
if((status&ST_CDSC)==ST_CDSC) printf("STATUS, Carrier Detect Change State\r\n");
if((status&ST_RFO)==ST_RFO) printf("STATUS, Receive Frame Overflow(HARDWARE)\r\n");
if((status&ST_EOP)==ST_EOP) printf("STATUS, End of Poll\r\n");
if((status&ST_BRKD)==ST_BRKD) printf("STATUS, Break Detected\r\n");
if((status&ST_ONLP)==ST_ONLP) printf("STATUS, On Loop\r\n");
if((status&ST_BRKT)==ST_BRKT) printf("STATUS, Break Terminated\r\n");
if((status&ST_ALLS)==ST_ALLS) printf("STATUS, All Sent\r\n");
if((status&ST_EXE)==ST_EXE) printf("STATUS, Transmit Underrun\r\n");
if((status&ST_TIN)==ST_TIN) printf("STATUS, Timer Expired\r\n");
if((status&ST_CTSC)==ST_CTSC) printf("STATUS, CTS Changed State\r\n");
if((status&ST_XMR)==ST_XMR) printf("STATUS, Transmit Message Repeat\r\n");
if((status&ST_TX_DONE)==ST_TX_DONE) printf("STATUS, TX Done\r\n");
if((status&ST_DMA_TC)==ST_DMA_TC) printf("STATUS, DMA TC reached\r\n");
if((status&ST_DSR1C)==ST_DSR1C) printf("STATUS, Channel 1 DSR Changed\r\n");
if((status&ST_DSR0C)==ST_DSR0C) printf("STATUS, Channel 0 DSR Changed\r\n");

}
/* $Id$ */