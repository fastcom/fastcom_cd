/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
readfile.c -- user mode function to read frames from a escc port and stuff them in a file

usage:
 readfile port file [hdlc]

 The port can be any valid escc port (0,1) 
 if hdlc is present the last byte of every frame is dropped from the file (RSTA byte);
 
*/

#include "windows.h"
#include "..\escctoolbox.h"
#include "stdio.h"
#include "stdlib.h"

void __stdcall esccreadcb(DWORD port,char * rbuf,DWORD retbytes);

void main(int argc,char *argv[])
{
ULONG port;
FILE *fout;
ULONG frame;
char rbuf[4098];
ULONG retbytes;
DWORD ret;
ULONG hdlc;

hdlc=0;
frame=0;
fout=NULL;

if(argc<2)
	{
	printf("usage:%s port file [hdlc]\r\n",argv[0]);
	exit(1);
	}
port = atoi(argv[1]);
if(argc>2) hdlc=1;

fout = fopen(argv[2],"wb");
if(fout==NULL)
{
printf("cannot open output file:%s\r\n",argv[2]);
exit(1);
}
if((ret = ESCCToolBox_Create(port))<0) 
{
	printf("create:%d\n",ret);
	exit(0);
}
ESCCToolBox_Flush_RX(port);
while(1)
	{
	if((int)(ret=ESCCToolBox_Read_Frame(port,rbuf,4096,&retbytes,5000))>=0)
		{
		if(retbytes>0)
			{
			if(hdlc==0)	fwrite(rbuf,1,retbytes,fout);
			else fwrite(rbuf,1,retbytes-1,fout);
			if((rbuf[retbytes-1]&0x10)==0x10) printf("Receive Abort error in frame	:%d\r\n",frame);
			if((rbuf[retbytes-1]&0x20)!=0x20) printf("CRC error in frame			:%d\r\n",frame);
			if((rbuf[retbytes-1]&0x40)==0x40) printf("RDO error in frame			:%d\r\n",frame);
			if((rbuf[retbytes-1]&0x80)!=0x80) printf("Invalid frame error in frame	:%d\r\n",frame);
			frame++;
			}
		else printf("received 0 byte frame\r\n");
		}
		else
		{
		printf("read returned:%d\r\n",ret);
		if(ret==-4) 
			{
			printf("read timeout--ending transfer\r\n");
			goto done;//timeout occured, close up the file and leave
			}
		}
	}
done:
ESCCToolBox_Destroy(port);
fclose(fout);
}
/* $Id$ */