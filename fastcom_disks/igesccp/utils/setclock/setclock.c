/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
setclock.c -- user mode function to program the programmable clock

usage:
 setclock port frequency

 The port can be any valid escc port (0,1) 
 frequency can be any number between 6000000 and 33333333

*/
#include "windows.h"
#include "..\escctoolbox.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"


int main(int argc, char *argv[])
{
unsigned long port;
unsigned long freq;
int ret;
unsigned long clocktype;

if(argc<3)
	{
	printf("usage:%s port frequency\r\n",argv[0]);
	exit(1);
	}
port = atoi(argv[1]);
freq = atol(argv[2]);
if(argc>3) clocktype = atoi(argv[3]);

if((ret = ESCCToolBox_Create(port))<0)
	{
	printf("create:%d\n",ret);
	exit(1);
	}
if((ret =ESCCToolBox_Set_Clock(port,freq,clocktype))<0)
	{
	printf("error in setclock:%d\n",ret);
	}
else printf("Clock set to %d\r\n",freq);

ESCCToolBox_Destroy(port);
}
/* $Id$ */