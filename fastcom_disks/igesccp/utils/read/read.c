/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
read.c -- user mode function to receive frames (using callback function)

usage:
 read port 

 The port can be any valid escc port (0,1) 

*/

#include "windows.h"
#include "..\escctoolbox.h"
#include "stdio.h"
#include "stdlib.h"

void __stdcall esccreadcb(DWORD port,char * rbuf,DWORD retbytes);

void main(int argc,char *argv[])
{
int ret;
ULONG port;
if(argc<2)
	{
	printf("usage:%s port\r\n",argv[0]);
	exit(1);
	}
port = atoi(argv[1]);
if((ret = ESCCToolBox_Create(port))<0) 
	{
	printf("create:%d\n",ret);
	exit(0);
	}
ESCCToolBox_Flush_RX(port);

ESCCToolBox_Register_Read_Callback(port,esccreadcb,4096);
printf("paused...any key to exit\n");
getchar();
ESCCToolBox_Destroy(port);
}

void __stdcall esccreadcb(DWORD port,char * rbuf,DWORD retbytes)
{
ULONG i;
printf("return:%d\n",retbytes);
for(i=0;i<retbytes;i++) printf("%x,",rbuf[i]&0xff);
printf("\r\n");
}
/* $Id$ */