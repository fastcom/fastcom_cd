
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the ESCCTOOLBOX_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// ESCCTOOLBOX_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef ESCCTOOLBOX_EXPORTS
#define ESCCTOOLBOX_API __declspec(dllexport)
#else
#define ESCCTOOLBOX_API __declspec(dllimport)
#endif

#include "..\esccptest.h"

#define BOARD_UNKNOWN				0
#define BOARD_ESCC					1
#define BOARD_ESCC104				2
#define BOARD_ESCC104ET				3
#define BOARD_ESCCPCI				4
#define BOARD_ESCCPCMCIA			5	
#define BOARD_ESCCPCIV3				6
#define BOARD_ESCCPCIISO1			7

#define AUTOCLOCK	0
#define ICD2053B	1
#define ICS307		2
#define FS6131		3

#define MAXPORTS 12

#ifdef __cplusplus
extern "C" 
{
#endif
typedef void (__stdcall *readcallbackfn)(DWORD port,char *rdata,DWORD retbytes);

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Create(DWORD port);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Destroy(DWORD port);

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Block_Multiple_Io(DWORD port,DWORD onoff);

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Set_Clock(DWORD port,DWORD frequency,DWORD clocktype);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Get_Clock(DWORD port);

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Read_Frame(DWORD port,char * rbuf,DWORD szrbuf,DWORD *retbytes,DWORD timeout);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Write_Frame(DWORD port,char * tbuf,DWORD numbytes,DWORD timeout);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Get_Status(DWORD port,DWORD *status,DWORD mask,DWORD timeout);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Get_IStatus(DWORD port,DWORD *status);

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Register_Read_Callback(DWORD port,readcallbackfn fnptr,DWORD szread);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Register_Write_Callback(DWORD port,void (__stdcall *callbackfunction)(DWORD port,char * tbuf,DWORD *bytestowrite),DWORD szmaxwrite);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Register_Status_Callback(DWORD port,DWORD mask,void (__stdcall *callbackfunction)(DWORD port,DWORD status));


ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Flush_RX(DWORD port);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Flush_TX(DWORD port);

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Read_Register(DWORD port,DWORD regno);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Write_Register(DWORD port,DWORD regno,DWORD value);


ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Configure(DWORD port,SETUP *settings);

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Set_Txclk_TT(DWORD port,DWORD onoff);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Set_Txclk_ST(DWORD port,DWORD onoff);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Set_SD_485(DWORD port,DWORD onoff);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Set_TT_485(DWORD port,DWORD onoff);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Set_RD_Echo_Cancel(DWORD port,DWORD onoff);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Set_CTS_Disable(DWORD port,DWORD onoff);

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Bisync_Start_Pattern_Enable(DWORD port,DWORD onoff);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Bisync_Cutoff_Enable(DWORD port,DWORD onoff);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_LSB2MSB_Convert_Enable(DWORD port,DWORD onoff);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Bisync_End_Pattern_Enable(DWORD port,DWORD onoff);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Bisync_Start_Pattern(DWORD port,struct bisync_start_pattern pattern);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Bisync_Size_Cutoff(DWORD port,DWORD size);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Bisync_End_Pattern(DWORD port,struct bisync_start_pattern pattern);

ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Postamble_Enable(DWORD port,DWORD onoff);
ESCCTOOLBOX_API DWORD __stdcall ESCCToolBox_Postamble_Delay(DWORD port,DWORD delay);

#ifdef __cplusplus
}
#endif
