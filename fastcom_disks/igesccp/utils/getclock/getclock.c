/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
getclock.c -- user mode function to get current programmable clock frequency

usage:
 getclock port 

 The port can be any valid escc port (0,1) 

*/

#include "windows.h"
#include "..\escctoolbox.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"


int main(int argc, char *argv[])
{
unsigned long port;
DWORD ret;
unsigned long clocktype;

if(argc<2)
	{
	printf("usage:%s port\r\n",argv[0]);
	exit(1);
	}
port = atoi(argv[1]);
if(argc>3) clocktype = atoi(argv[3]);

if((ret = ESCCToolBox_Create(port))<0)
	{
	printf("create:%d\n",ret);
	exit(1);
	}
if((ret =ESCCToolBox_Get_Clock(port))<0)
	{
	printf("error in getclock:%d\n",ret);
	}
else printf("Clock set to %d\r\n",ret);

ESCCToolBox_Destroy(port);
}
/* $Id$ */