/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
writefile.c -- user mode function to write a file to a escc port

usage:
 writefile port block file [delay] [repeat]

 The port can be any valid escc port (0,1) 
 block is the block size to use to send (1-4095)
 file is the file to send
 delay is the time to add between block writes (in miliseconds)
 if repeat is present then the file will be rewound and resent repeat times
 
*/
#include "windows.h"
#include "..\escctoolbox.h"
#include "stdio.h"
#include "stdlib.h"

void __stdcall esccwritecb(DWORD port,char * tbuf,DWORD *bytestowrite);

FILE *fin;
ULONG tsize;
ULONG delay;
ULONG repeat;
ULONG finished;
void main(int argc,char *argv[])
{
int ret;
ULONG port;
tsize=32;
delay=0;
repeat=0;
finished=0;
fin=NULL;

if(argc<4)
	{
	printf("usage:%s port block file [delay] [repeat]\r\n",argv[0]);
	exit(1);
	}

port = atoi(argv[1]);
tsize = atol(argv[2]);
if(argc>4) delay = atol(argv[4]);
if(argc>5) repeat =atol(argv[5]);
printf("FILE:%s\r\n",argv[3]);
printf("delay:%d\r\n",delay);
if(repeat!=0) printf("repeat is ON,count%d\r\n",repeat);
if((tsize<1)||(tsize>4095))
{
printf("block size out of range: 1<%d<4095\r\n",tsize);
exit(1);
}
fin = fopen(argv[3],"rb");
if(fin==NULL)
{
printf("cannot open %s\r\n",argv[3]);
exit(1);
}


if((ret = ESCCToolBox_Create(port))<0) 
	{
	printf("create:%d\n",ret);
	exit(0);
	}
ESCCToolBox_Flush_TX(port);
ret = ESCCToolBox_Register_Write_Callback(port,esccwritecb,4096);
if(ret<0)
	{
	printf("registerwritecallback:%d\n",ret);
	ESCCToolBox_Destroy(port);
	exit(0);
	}
while(finished==0)
	{
	printf("writing...\r");
	Sleep(250);
	}
printf("writing...done\r\n");
ESCCToolBox_Destroy(port);
fclose(fin);
}

void __stdcall esccwritecb(DWORD port,char * tbuf,DWORD *bytestowrite)
{

if(delay!=0) Sleep(delay);
bytestowrite[0] = fread(tbuf,1,tsize,fin);
if(bytestowrite[0]==0)
{
	if(feof(fin))
	{
	if(repeat!=0) 
		{
		repeat--;
		rewind(fin);
		bytestowrite[0] = fread(tbuf,1,tsize,fin);
		if(bytestowrite[0]==0)
			{
			finished=1;
			}
		}
		else
		{
		finished=1;
		}
	}
}

}
/* $Id$ */