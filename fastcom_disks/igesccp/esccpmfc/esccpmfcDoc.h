// esccpmfcDoc.h : interface of the CEsccpmfcDoc class
//
/////////////////////////////////////////////////////////////////////////////
#include "..\esccptest.h"

class CEsccpmfcDoc : public CDocument
{
protected: // create from serialization only
	CEsccpmfcDoc();
	DECLARE_DYNCREATE(CEsccpmfcDoc)

// Attributes
public:
CString data;
CString status;
CString txdata;
struct setup Esccset;
OVERLAPPED ostx;
HANDLE hDevice;			//handle to the escc driver/device
//HANDLE      hstatThread ;	//handle to status thread 
//HANDLE      hreadThread ;	//handle to read thread
CWinThread *rT;
CWinThread *sT;
DWORD       statID;		//status thread ID storage
DWORD       readID;		//read thread ID storage
USHORT numbits;
ULONG clockbits;
ULONG freq;
BOOL connected;
ULONG statusID;//actual return from status call
ULONG pnum;
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEsccpmfcDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CEsccpmfcDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CEsccpmfcDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
