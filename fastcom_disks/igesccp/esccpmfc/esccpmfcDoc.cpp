// esccpmfcDoc.cpp : implementation of the CEsccpmfcDoc class
//

#include "stdafx.h"
#include "esccpmfc.h"

#include "esccpmfcDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEsccpmfcDoc

IMPLEMENT_DYNCREATE(CEsccpmfcDoc, CDocument)

BEGIN_MESSAGE_MAP(CEsccpmfcDoc, CDocument)
	//{{AFX_MSG_MAP(CEsccpmfcDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEsccpmfcDoc construction/destruction

CEsccpmfcDoc::CEsccpmfcDoc()
{
	// TODO: add one-time construction code here
	freq=0;
Esccset.mode =  0x88;
Esccset.timr = 0x1f;
Esccset.xbcl = 0x00;
Esccset.xbch = 0x00;
Esccset.ccr0 = 0x80;
Esccset.ccr1 = 0x16;
Esccset.ccr2 = 0x38;
Esccset.ccr3 = 0x00;
Esccset.ccr4 = 0x00;
Esccset.tsax = 0x00;
Esccset.tsar = 0x00;
Esccset.xccr = 0x00;
Esccset.rccr = 0x00;
Esccset.bgr = 0x00;
Esccset.iva = 0x00;
Esccset.ipc = 0x03;
Esccset.imr0 = 0x04;
Esccset.imr1 = 0x00;
Esccset.pvr = 0x00;
Esccset.pim = 0xff;
Esccset.pcr = 0xe0;
Esccset.xad1 = 0xff;
Esccset.xad2 = 0xff;
Esccset.rah1 = 0xff;
Esccset.rah2 = 0xff;
Esccset.ral1 = 0xff;
Esccset.ral2 = 0xff;
Esccset.rlcr = 0x00;
Esccset.aml = 0x00;
Esccset.amh = 0x00;
Esccset.pre = 0x00;
Esccset.xon = 0x00;
Esccset.xoff= 0x00;
Esccset.tcr = 0x00;
Esccset.dafo = 0x00;
Esccset.rfc = 0x00;
Esccset.tic = 0x00;
Esccset.mxn = 0x00;
Esccset.mxf = 0x00;
Esccset.synl = 0x00;
Esccset.synh = 0x00;
Esccset.n_rbufs=10;
Esccset.n_rfsize_max=4096;
Esccset.n_tbufs=10;
Esccset.n_tfsize_max=4096;

}

CEsccpmfcDoc::~CEsccpmfcDoc()
{
}

BOOL CEsccpmfcDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)
data = "";
status = "";
txdata = "";
numbits = 22;
clockbits = 0;
connected = FALSE;
statusID = 0;//actual return from status call
Esccset.mode =  0x88;
Esccset.timr = 0x1f;
Esccset.xbcl = 0x00;
Esccset.xbch = 0x00;
Esccset.ccr0 = 0x80;
Esccset.ccr1 = 0x16;
Esccset.ccr2 = 0x38;
Esccset.ccr3 = 0x00;
Esccset.ccr4 = 0x00;
Esccset.tsax = 0x00;
Esccset.tsar = 0x00;
Esccset.xccr = 0x00;
Esccset.rccr = 0x00;
Esccset.bgr = 0x00;
Esccset.iva = 0x00;
Esccset.ipc = 0x03;
Esccset.imr0 = 0x04;
Esccset.imr1 = 0x00;
Esccset.pvr = 0x00;
Esccset.pim = 0xff;
Esccset.pcr = 0xe0;
Esccset.xad1 = 0xff;
Esccset.xad2 = 0xff;
Esccset.rah1 = 0xff;
Esccset.rah2 = 0xff;
Esccset.ral1 = 0xff;
Esccset.ral2 = 0xff;
Esccset.rlcr = 0x00;
Esccset.aml = 0x00;
Esccset.amh = 0x00;
Esccset.pre = 0x00;
Esccset.xon = 0x00;
Esccset.xoff= 0x00;
Esccset.tcr = 0x00;
Esccset.dafo = 0x00;
Esccset.rfc = 0x00;
Esccset.tic = 0x00;
Esccset.mxn = 0x00;
Esccset.mxf = 0x00;
Esccset.synl = 0x00;
Esccset.synh = 0x00;
Esccset.n_rbufs=10;
Esccset.n_rfsize_max=4096;
Esccset.n_tbufs=10;
Esccset.n_tfsize_max=4096;

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CEsccpmfcDoc serialization

void CEsccpmfcDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CEsccpmfcDoc diagnostics

#ifdef _DEBUG
void CEsccpmfcDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CEsccpmfcDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CEsccpmfcDoc commands
