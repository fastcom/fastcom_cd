//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by fsccterm.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_FSCCTETYPE                  129
#define IDD_PORT_SELECT                 130
#define IDC_PORTSELECT                  1001
#define IDM_PORT                        32771
#define IDM_DISCONNECT                  32772
#define IDM_SETTINGS                    32773
#define IDC_FLUSHTX                     32774
#define IDC_FLUSH_RX                    32775

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32779
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
