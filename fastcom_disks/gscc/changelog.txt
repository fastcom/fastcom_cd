04/24/2008
- Windows: updates to merge QSSB code changes, should be "the same" as far as if the 
#define QSSB is not present Added wait for CEC after flush to prevent data loss on a 
write immediatly following a flushtx

08/01/2007
- linux: update for cancel rx function from WJC, added utility/demo
also changed back to auto major assignment, seems to work again (MAJOR=0=autoassign)

06/28/2007
- linux: update for fimrware fix (REV_03).  Support for gating the main interrupt.

06/05/2007
- Windows: Update for fimrware fix (REV_03).  Support for gating the main
  interrupt.

05/01/2007
- Linux: Update to add bisync pattern matching (start and end)

04/16/2007
- Linux: added modifications as per customer request.  Fixed a typeo on a define.
  Made extra memory allocation more explicit.
  Added masking code to cleanup (rmmod) so the board will not generate interrupts after the   driver is unloaded.
  Added a RRES to the bisync cutoff code.
  Added check for XFW prior to fifo writes.

04/12/2007
- Linux: Fixed transparent settings.
	Cleaned up some readme's
	Added bisync cutoff example + code

11/03/2006
- Windows: fixed bisync pattern match

10/10/2006
- Windows: synced with escc-pci-335 driver, updated canceling/flushing to
  prevent cases of bsod.  updated ISR servicing method.  added pcilocation
  code to lock devicenames to pci slots. automated build process.

09/11/2006
- Created Linux directory

08/21/2006 
- Windows: Fixed cancel issue

06/28/2006 
- Windows: Compile drivers for xp 2000 and 2003
