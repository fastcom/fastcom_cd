/* $Id$ */
/*
Copyright(C) 2002, Commtech, Inc.
read.c -- a user mode program to read bytes from a channel and stuff them to a file

  usage:
  read port size outfile
	
*/


#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "..\gsccptest.h"

int main(int argc, char *argv[])
{
	HANDLE rDevice;
	FILE *fout;
	ULONG t;
	DWORD nobytesread;
	char *rdata;
	OVERLAPPED  rq;
	int j,error;
	ULONG totalsent;
	ULONG totalread;
	ULONG totalerror;
	ULONG loop;
    char devname[25];
	struct serocco_setup gsccsetup; //setup structure for initializing the gscc registers (see gsccptest.h)

	if(argc<3)
	{
		printf("usage:\n");
		printf("read port outfile\n");
		exit(1);
	}
	fout=fopen(argv[2],"wb");
	if(fout==NULL)
	{
		printf("cannot open output file %s\n",argv[2]);
		exit(1);
	}
	
	sprintf(devname,"\\\\.\\GSCC%d",atoi(argv[1]));
	printf("devicename:%s\n",devname);
	
	memset( &rq, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
	rq.hEvent = CreateEvent( NULL,    // no security
		TRUE,    // explicit reset req
		FALSE,   // initial event reset
		NULL ) ; // no name
	if (rq.hEvent == NULL)
	{
		//MessageBox( NULL, "Failed to create event for thread!", "main Error!",
		//	MB_ICONEXCLAMATION | MB_OK ) ;
		printf("Failed to create event for thread!\n");
		fclose(fout);
		exit(1);
	}
	rDevice = CreateFile (devname,
		GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL,
		OPEN_ALWAYS,
		FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
		NULL
		);
	
    if (rDevice == INVALID_HANDLE_VALUE)
    {
		//for some reason the driver won't load or isn't loaded
		printf ("Can't get a handle to gsccdrv\n");
		fclose(fout);
		CloseHandle(rq.hEvent);
		exit(1);
		//abort and leave here!!!
	}
	//allocate memory for read/write
	rdata = (char*)malloc(4096);
	if(rdata==NULL)
	{
		printf("cannot allocate memory for data area\n");
		fclose(fout);
		CloseHandle(rDevice);
		CloseHandle(rq.hEvent);
		exit(1);
	}

gsccsetup.gcmdr   =0x0000;
gsccsetup.gmode   =0x003a;
gsccsetup.gpdirl  =0x0006;
gsccsetup.gpdirh  =0x00bf;
gsccsetup.gpdatl  =0x0000;
gsccsetup.gpdath  =0x0000;
gsccsetup.gpiml   =0x0007;
gsccsetup.gpimh   =0x00ff;
gsccsetup.dcmdr   =0x0000;
gsccsetup.dimr    =0x0000;
gsccsetup.cmdrl   =0x0000;
gsccsetup.cmdrh   =0x0000;
gsccsetup.ccr0l   =0x0018;
gsccsetup.ccr0h   =0x0080;
gsccsetup.ccr1l   =0x0002;
gsccsetup.ccr1h   =0x0000;
gsccsetup.ccr2l   =0x00e0;
gsccsetup.ccr2h   =0x0000;
gsccsetup.ccr3l   =0x0000;
gsccsetup.ccr3h   =0x0000;
gsccsetup.preamb  =0x0000;
gsccsetup.tolen   =0x0000;
gsccsetup.accm0   =0x0000;
gsccsetup.accm1   =0x0000;
gsccsetup.accm2   =0x0000;
gsccsetup.accm3   =0x0000;
gsccsetup.udac0   =0x0000;
gsccsetup.udac1   =0x0000;
gsccsetup.udac2   =0x0000;
gsccsetup.udac3   =0x0000;
gsccsetup.ttsa0   =0x0000;
gsccsetup.ttsa1   =0x0000;
gsccsetup.ttsa2   =0x0000;
gsccsetup.ttsa3   =0x0000;
gsccsetup.rtsa0   =0x0000;
gsccsetup.rtsa1   =0x0000;
gsccsetup.rtsa2   =0x0000;
gsccsetup.rtsa3   =0x0000;
gsccsetup.pcmtx0  =0x0000;
gsccsetup.pcmtx1  =0x0000;
gsccsetup.pcmtx2  =0x0000;
gsccsetup.pcmtx3  =0x0000;
gsccsetup.pcmrx0  =0x0000;
gsccsetup.pcmrx1  =0x0000;
gsccsetup.pcmrx2  =0x0000;
gsccsetup.pcmrx3  =0x0000;
gsccsetup.brrl    =0x0000;
gsccsetup.brrh    =0x0000;
gsccsetup.timer0  =0x00ff;
gsccsetup.timer1  =0x007f;
gsccsetup.timer2  =0x0000;
gsccsetup.timer3  =0x0000;
gsccsetup.xad1    =0x00ff;
gsccsetup.xad2    =0x00ff;
gsccsetup.ral1    =0x00ff;
gsccsetup.rah1    =0x00ff;
gsccsetup.ral2    =0x00ff;
gsccsetup.rah2    =0x00ff;
gsccsetup.amral1  =0x0000;
gsccsetup.amrah1  =0x0000;
gsccsetup.amral2  =0x0000;
gsccsetup.amrah2  =0x0000;
gsccsetup.rlcrl   =0x0000;
gsccsetup.rlcrh   =0x0000;
gsccsetup.xon     =0x0000;
gsccsetup.xoff    =0x0000;
gsccsetup.mxon    =0x0000;
gsccsetup.mxoff   =0x0000;
gsccsetup.tcr     =0x0000;
gsccsetup.ticr    =0x0000;
gsccsetup.imr0    =0x0001;
gsccsetup.imr1    =0x0003;
gsccsetup.imr2    =0x00fd;
gsccsetup.syncl   =0x0000;
gsccsetup.synch   =0x0000;

gsccsetup.n_rbufs   =0x000a;
gsccsetup.n_tbufs   =0x000a;
gsccsetup.n_rfsize_max=0x1000;
gsccsetup.n_tfsize_max=0x1000;
	
	
	//when called this IOCTL will set the registers of the gscc and
	//reset the TX and RX machines (issuing a separate XRES and RHR command)
	//if the resets time out the function will return FALSE, otherwise it will
	//return TRUE
	t = DeviceIoControl(rDevice,IOCTL_GSCCDRV_SET_SEROCCO,&gsccsetup,sizeof(struct serocco_setup),NULL,0,&t,NULL);
	if(t==TRUE)printf("SETTINGS SUCCESSFUL:%lu\n\r",t);
	if(t==FALSE)
	{
		printf("SETTINGS FAILED:%lu\n\r",t);
		CloseHandle(rDevice);
		exit(1);
	}

	/* Flush the RX Descriptors so not as to have any complete descriptors in their
	* the first read in hdlc will get those left over frames and this test program
	* would not be of any use. */
	//printf("flush rx\n");
	DeviceIoControl(rDevice,IOCTL_GSCCDRV_FLUSH_RX,NULL,0,NULL,0,&t,NULL);
	
	totalerror=0;
	totalsent=0;
	totalread=0;
	loop=0;
	
	while(!kbhit())
	{
		
		error=0;
		t = ReadFile(rDevice,&rdata[0],4096,&nobytesread,&rq);
		if(t==FALSE)  
		{
			//	printf("read blocked\n");
			t=GetLastError();
			if(t==ERROR_IO_PENDING)
			{
				do
				{
					j = WaitForSingleObject( rq.hEvent, 5000 );//5 second timeout -- must be greater than size*8*(1/bitrate)*1000
					//			printf("after wait:%lx\n",j);
					if(j==WAIT_TIMEOUT)
					{
						if(kbhit()) goto done;
						printf("Reciever Locked up... Resetting RX.\r\n");
						//DeviceIoControl(rDevice,IOCTL_GSCCDRV_FLUSH_TX,NULL,0,NULL,0,&returnsize,NULL);
					}
					if(j==WAIT_ABANDONED)
					{
						printf("Reciever Abandoned.\r\n");
						goto close;
					}
				} while(j!=WAIT_OBJECT_0);
				GetOverlappedResult(rDevice,&rq,&nobytesread,TRUE);
				//		printf("postblock read:%d\n",nobytesread);
			}
			else printf("READ ERROR: #%x\n",t);
		}
		//      else printf("read returned true:%d\n",nobytesread);
		
		printf("[%d]READ %d\n\n",t,nobytesread);
		if(nobytesread!=4096)
		{
			printf("received:%lu, expected 4096\n",nobytesread);
		}
		if(nobytesread!=0)
		{
			totalsent+=fwrite(rdata,1,4096,fout);
		}
		//printf("Found: %d errors\n",error);
		loop++;
		totalread+=nobytesread;
	}
done:
	getch();
	printf("Read  %lu bytes\n",totalread);
	printf("Wrote %lu bytes\n",totalsent);
	printf("Count %lu\n",loop);
	
	
close:
	free(rdata);
	fclose(fout);
	CloseHandle(rDevice);
	CloseHandle(rq.hEvent);
	return 0;
}
/* $Id$ */