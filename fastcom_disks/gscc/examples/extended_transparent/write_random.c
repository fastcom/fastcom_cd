/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
write_random.c -- a user mode program to write a random bitstream (with counter) to a port

  usage:
  write_random port size [savefile]
  
	size is the blocksize to use for writes
	savefile is the optional file to dump the data to as well as pumping it out the port
	
*/


#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "..\gsccptest.h"

int main(int argc, char *argv[])
{
	HANDLE wDevice;/* Handle for the SuperFastcom port */
	ULONG t;
	DWORD nobyteswritten;
	char *tdata;
	ULONG size;
	OVERLAPPED  wq;
	int j,x,error,tosend;
	ULONG totalsent;
	ULONG totalread;
	ULONG totalerror;
	ULONG loop;
    char devname[25];
	FILE *fout=NULL;
	struct serocco_setup gsccsetup; //setup structure for initializing the gscc registers (see gsccptest.h)
	ULONG  desfreq;

	
	if(argc<3)
	{
		printf("usage:\n");
		printf("write_random port blocksize [savefile]\n");
		exit(1);
	}
	if(argc>3)
	{
		fout = fopen(argv[3],"wb");
		if(fout==NULL)
		{
			printf("cannot open save file\n");
			exit(1);
		}
	}
	srand( (unsigned)time( NULL ) );
	size = atol(argv[2]);
	if(size==0)
	{
		printf("block size must be nonzero\n");
		exit(1);
	}
    sprintf(devname,"\\\\.\\GSCC%d",atoi(argv[1]));
	printf("devicename:%s\n",devname);
	printf("blocksize:%lu\n",size);
	
	memset( &wq, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
	wq.hEvent = CreateEvent( NULL,    // no security
		TRUE,    // explicit reset req
		FALSE,   // initial event reset
		NULL ) ; // no name
	if (wq.hEvent == NULL)
	{
		//MessageBox( NULL, "Failed to create event for thread!", "main Error!",
		//	MB_ICONEXCLAMATION | MB_OK ) ;
		printf("Failed to create event for thread!\n");
		return 1; 
	}
	//	printf("write overlapped event created\n");
	wDevice = CreateFile (devname,
		GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL,
		OPEN_ALWAYS,
		FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
		NULL
		);
    if (wDevice == INVALID_HANDLE_VALUE)
    {
		//for some reason the driver won't load or isn't loaded
		printf ("Can't get a handle to gsccdrv\n");
		exit(1);
		//abort and leave here!!!
	}
	//printf("gsccdrv handle created\n");
	tdata = (char*)malloc(size+1);
	if(tdata==NULL)
	{
		printf("unable to allocate data buffer\n");
		exit(1);
	}

	//Set clock generator to 18.432MHz
	desfreq = 18432000;
	if(DeviceIoControl(wDevice,IOCTL_GSCCDRV_SET_FREQ,&desfreq,sizeof(ULONG),NULL,0,&t,NULL))
	{
		printf("Actual Frequency Set to: %d\n",desfreq);
	}

gsccsetup.gcmdr   =0x0000;
gsccsetup.gmode   =0x003a;
gsccsetup.gpdirl  =0x0006;
gsccsetup.gpdirh  =0x00bf;
gsccsetup.gpdatl  =0x0000;
gsccsetup.gpdath  =0x0000;
gsccsetup.gpiml   =0x0007;
gsccsetup.gpimh   =0x00ff;
gsccsetup.dcmdr   =0x0000;
gsccsetup.dimr    =0x0000;
gsccsetup.cmdrl   =0x0000;
gsccsetup.cmdrh   =0x0000;
gsccsetup.ccr0l   =0x001f;
gsccsetup.ccr0h   =0x0080;
gsccsetup.ccr1l   =0x0002;
gsccsetup.ccr1h   =0x0000;
gsccsetup.ccr2l   =0x00c0;
gsccsetup.ccr2h   =0x0000;
gsccsetup.ccr3l   =0x0000;
gsccsetup.ccr3h   =0x0000;
gsccsetup.preamb  =0x0000;
gsccsetup.tolen   =0x0000;
gsccsetup.accm0   =0x0000;
gsccsetup.accm1   =0x0000;
gsccsetup.accm2   =0x0000;
gsccsetup.accm3   =0x0000;
gsccsetup.udac0   =0x0000;
gsccsetup.udac1   =0x0000;
gsccsetup.udac2   =0x0000;
gsccsetup.udac3   =0x0000;
gsccsetup.ttsa0   =0x0000;
gsccsetup.ttsa1   =0x0000;
gsccsetup.ttsa2   =0x0000;
gsccsetup.ttsa3   =0x0000;
gsccsetup.rtsa0   =0x0000;
gsccsetup.rtsa1   =0x0000;
gsccsetup.rtsa2   =0x0000;
gsccsetup.rtsa3   =0x0000;
gsccsetup.pcmtx0  =0x0000;
gsccsetup.pcmtx1  =0x0000;
gsccsetup.pcmtx2  =0x0000;
gsccsetup.pcmtx3  =0x0000;
gsccsetup.pcmrx0  =0x0000;
gsccsetup.pcmrx1  =0x0000;
gsccsetup.pcmrx2  =0x0000;
gsccsetup.pcmrx3  =0x0000;
gsccsetup.brrl    =0x002f;
gsccsetup.brrh    =0x0003;
gsccsetup.timer0  =0x00ff;
gsccsetup.timer1  =0x007f;
gsccsetup.timer2  =0x0000;
gsccsetup.timer3  =0x0000;
gsccsetup.xad1    =0x00ff;
gsccsetup.xad2    =0x00ff;
gsccsetup.ral1    =0x00ff;
gsccsetup.rah1    =0x00ff;
gsccsetup.ral2    =0x00ff;
gsccsetup.rah2    =0x00ff;
gsccsetup.amral1  =0x0000;
gsccsetup.amrah1  =0x0000;
gsccsetup.amral2  =0x0000;
gsccsetup.amrah2  =0x0000;
gsccsetup.rlcrl   =0x0000;
gsccsetup.rlcrh   =0x0000;
gsccsetup.xon     =0x0000;
gsccsetup.xoff    =0x0000;
gsccsetup.mxon    =0x0000;
gsccsetup.mxoff   =0x0000;
gsccsetup.tcr     =0x0000;
gsccsetup.ticr    =0x0000;
gsccsetup.imr0    =0x0001;
gsccsetup.imr1    =0x0003;
gsccsetup.imr2    =0x00fd;
gsccsetup.syncl   =0x0000;
gsccsetup.synch   =0x0000;

gsccsetup.n_rbufs   =0x000a;
gsccsetup.n_tbufs   =0x000a;
gsccsetup.n_rfsize_max=0x1000;
gsccsetup.n_tfsize_max=0x1000;

	
	
	//when called this IOCTL will set the registers of the gscc and
	//reset the TX and RX machines (issuing a separate XRES and RHR command)
	//if the resets time out the function will return FALSE, otherwise it will
	//return TRUE
	t = DeviceIoControl(wDevice,IOCTL_GSCCDRV_SET_SEROCCO,&gsccsetup,sizeof(struct serocco_setup),NULL,0,&t,NULL);
	if(t==TRUE)printf("SETTINGS SUCCESSFUL:%lu\n\r",t);
	if(t==FALSE)
	{
		printf("SETTINGS FAILED:%lu\n\r",t);
		CloseHandle(wDevice);
		exit(1);
	}
	
	DeviceIoControl(wDevice,IOCTL_GSCCDRV_FLUSH_TX,NULL,0,NULL,0,&t,NULL);
	
	totalerror=0;
	totalsent=0;
	totalread=0;
	loop=0;
	
	while(!kbhit())
	{
		error=0;
		
		// generate a random length 1 - 4095 bytes
		tosend=size;
		// generate a random string of our random length.
		for(x=0;x<tosend;x++) 
			tdata[x]=(UCHAR)(rand());
		sprintf(&tdata[10],"%c%c%8.8lu%c%c",0xff,0x00,loop,0x00,0xff);//put our frame counter in ascii in the data
		//printf("pre-write\n");
		t = WriteFile(wDevice,&tdata[0],tosend,&nobyteswritten,&wq);
		
//				Sleep(1000);	
		
		if(t==FALSE)  
		{
			t=GetLastError();
			
			
			if(t==ERROR_IO_PENDING)
			{
				do
				{
					j = WaitForSingleObject( wq.hEvent, 1000 );//5 second timeout -- must be larger than size*8*(1/bitrate)*1000
					if(j==WAIT_TIMEOUT)
					{
						printf("Transmitter Locked up... Resetting TX.\r\n");
						//DeviceIoControl(wDevice,IOCTL_GSCCDRV_FLUSH_TX,NULL,0,NULL,0,&returnsize,NULL);
					}
					if(j==WAIT_ABANDONED)
					{
						printf("Recieve Abandoned.\r\n");
						goto close;
					}
				} while(j!=WAIT_OBJECT_0);
				GetOverlappedResult(wDevice,&wq,&nobyteswritten,TRUE);
			}
			else printf("WRITE ERROR: #%d\n",t);
		}
		if(nobyteswritten!=size)
		{
			printf("unexpected actual:%lu, sent:%lu\n",nobyteswritten,size); 
		}
		loop++;
		totalsent+=nobyteswritten;
		if(fout!=NULL) fwrite(tdata,1,tosend,fout);
	}
	getch();
	printf("Wrote %lu bytes\n",totalsent);
	printf("count %lu\n",loop);
	
close:
	free(tdata);
	CloseHandle(wq.hEvent);
	CloseHandle (wDevice);
	if(fout!=NULL) fclose(fout);
	return 0;
	}
	/* $Id$ */