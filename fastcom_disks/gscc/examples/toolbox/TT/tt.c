/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
tt.c -- user mode function to connect TT output singal to txclk pin using the gscctoolbox.dll

The sab82532 has only one TxClk pin.  On the GSCC, this pin could either be connect to 
ST (clock in) or TT (clock out).  By default it is connected to TT, this function allows
it to be switched from TT to ST.  

YOU MUST SET BIT CCR2:TOE=1 BEFORE ENABLING THE TT TXCLK OUTPUT as you may damage the 82532

When enabling the TT txclk output you should disable the ST txclk.

It is possible to enable both the TT and ST txclk signals at the connector, but the bit
CCR2:TOE MUST BE SET TO 0, setting to 1 will damage the 82532.  This will allow an
ST signal to be used as both the TxClk IN and the TT out signal.

To use the gscctoolbox.DLL, you must do three things:

1. #include gscctoolbox.h in your source code
2. Add the file gscctoolbox.lib to your linker's Output/Library modules.  To do this in
   Visual Studio, Click on Settings in the Project menu.  In the Project settings dialog
   click on the Link tab.  In the Category drop down, choose General.  In the 
   Output/library modules box, add the path to gscctoolbox.lib.  It is easiest to copy the 
   file to your working directory and just add "gscctoolbox.lib"
3. The gscctoolbox.dll file must reside in the same directory as your compiled program .exe

usage:
 tt port 0|1

 The port can be any valid gscc port (0,1,2...) 
 
*/

#include "windows.h"
#include "..\gscctoolbox\gscctoolbox.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"


int main(int argc, char *argv[])
{
	unsigned long port;
	DWORD ret;
	DWORD onoff;
	
	if(argc<3)
	{
		printf("usage:%s port [0|1]\r\n",argv[0]);
		exit(1);
	}
	port = atoi(argv[1]);
	onoff = atoi(argv[2]);
	if((ret = GSCCToolBox_Create(port))<0)
	{
		printf("create:%d\n",ret);
		exit(1);
	}
	if((ret =GSCCToolBox_Set_Txclk_TT(port,onoff))<0)
	{
		printf("error in Set_Txckl_TT:%d\n",ret);
	}
	else 
	{
		if(onoff==0) printf("TT disconnected\r\n");
		else printf("txclk connected to TT\r\n");
	}
	
	GSCCToolBox_Destroy(port);
}
/* $Id$ */