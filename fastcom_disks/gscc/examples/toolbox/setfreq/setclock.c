/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
setclock.c -- user mode function to program the programmable clock using the gscctoolbox.dll

  To use the gscctoolbox.DLL, you must do three things:

1. #include gscctoolbox.h in your source code
2. Add the file gscctoolbox.lib to your linker's Output/Library modules.  To do this in
   Visual Studio, Click on Settings in the Project menu.  In the Project settings dialog
   click on the Link tab.  In the Category drop down, choose General.  In the 
   Output/library modules box, add the path to gscctoolbox.lib.  It is easiest to copy the 
   file to your working directory and just add "gscctoolbox.lib"
3. The gscctoolbox.dll file must reside in the same directory as your compiled program .exe

usage:
 setclock port frequency

 The port can be any valid gscc port (0,1,2...) 
 frequency can be any number between 6000000 and 33333333

*/
#include "windows.h"
#include "..\gscctoolbox\gscctoolbox.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"


int main(int argc, char *argv[])
{
	unsigned long port;
	unsigned long freq;
	int ret;
	
	if(argc!=3)
	{
		printf("usage:%s port frequency\r\n",argv[0]);
		exit(1);
	}
	port = atoi(argv[1]);
	freq = atol(argv[2]);
	
	if((ret = GSCCToolBox_Create(port))<0)
	{
		printf("create:%d\n",ret);
		exit(1);
	}
	if((ret =GSCCToolBox_Set_Clock(port,freq))<0)
	{
		printf("error in setclock:%d\n",ret);
	}
	else printf("Clock set to %d\r\n",freq);
	
	GSCCToolBox_Destroy(port);
}
/* $Id$ */