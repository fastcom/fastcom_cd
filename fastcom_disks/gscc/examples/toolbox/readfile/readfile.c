/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
readfile.c - user mode function to read frames from a gscc port and stuff them in a file
			 using the gscctoolbox.dll

Executes the same as readfile.c, but does not use the callback function

To use the gscctoolbox.DLL, you must do three things:

1. #include gscctoolbox.h in your source code
2. Add the file gscctoolbox.lib to your linker's Output/Library modules.  To do this in
   Visual Studio, Click on Settings in the Project menu.  In the Project settings dialog
   click on the Link tab.  In the Category drop down, choose General.  In the 
   Output/library modules box, add the path to gscctoolbox.lib.  It is easiest to copy the 
   file to your working directory and just add "gscctoolbox.lib"
3. The gscctoolbox.dll file must reside in the same directory as your compiled program .exe
usage:
 readfile port file [hdlc]

 The port can be any valid gscc port (0,1,2...) 
 if hdlc is present the last byte of every frame is dropped from the file (RSTA byte);
 
*/

#include "windows.h"
#include "..\gscctoolbox\gscctoolbox.h"
#include "stdio.h"
#include "stdlib.h"

void main(int argc,char *argv[])
{
	ULONG port;
	FILE *fout;
	ULONG frame;
	char rbuf[4098];
	ULONG retbytes;
	DWORD ret;
	ULONG hdlc;
	
	hdlc=0;
	frame=0;
	fout=NULL;
	
	if(argc<2)
	{
		printf("usage:%s port file [hdlc]\r\n",argv[0]);
		exit(1);
	}
	port = atoi(argv[1]);
	if(argc>2) hdlc=1;
	
	fout = fopen(argv[2],"wb");
	if(fout==NULL)
	{
		printf("cannot open output file:%s\r\n",argv[2]);
		exit(1);
	}
	if((ret = GSCCToolBox_Create(port))<0) 
	{
		printf("create:%d\n",ret);
		exit(0);
	}
	GSCCToolBox_Flush_RX(port);
	while(1)
	{
		if((int)(ret=GSCCToolBox_Read_Frame(port,rbuf,4096,&retbytes,5000))>=0)
		//port is the port number i.e. GSCC#
		//rbuf is the char buffer to store data
		//4096 is nNumberOfBytesToRead to ReadFile, 4096 will return only the amount of data available, it will not try to receive more than is there
		//retbytes is lpNumberOfBytesRead to ReadFile, the number of bytes actually received
		//5000 is the number of miliseconds for WaitForSingleObject to wait before the read is canceled and the handle is closed
		{
			if(retbytes>0)
			{
				if(hdlc==0)	fwrite(rbuf,1,retbytes,fout);
				else 
				{
					fwrite(rbuf,1,retbytes-1,fout);
					if((rbuf[retbytes-1]&0x10)==0x10) printf("Receive Abort error in frame	:%d\r\n",frame);
					if((rbuf[retbytes-1]&0x20)!=0x20) printf("CRC error in frame			:%d\r\n",frame);
					if((rbuf[retbytes-1]&0x40)==0x40) printf("RDO error in frame			:%d\r\n",frame);
					if((rbuf[retbytes-1]&0x80)!=0x80) printf("Invalid frame error in frame	:%d\r\n",frame);
					frame++;
				}
			}
			else printf("received 0 byte frame\r\n");
		}
		else
		{
			printf("read returned:%d\r\n",ret);
			if(ret==-4) 
			{
				printf("read timeout--ending transfer\r\n");
				goto done;//timeout occured, close up the file and leave
			}
		}
	}
done:
	GSCCToolBox_Destroy(port);
	fclose(fout);
}
/* $Id$ */