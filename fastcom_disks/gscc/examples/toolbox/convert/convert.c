#include "windows.h"
#include "conio.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"

//SEROCCO defines
//STARL defines
#define XREPE	0x80
#define TEC		0x20
#define CEC		0x10
#define FCS		0x08
#define XDOV	0x04
#define XFW		0x02
#define CTS		0x01
//STARH defines
#define RFNE	0x40
#define CD		0x20
#define RLI		0x10
#define SYNC	0x10
#define DPLA	0x08
#define WFA		0x04
#define XRNR	0x02
#define RRNR	0x01
//CMDRL defines
#define STI		0x80
#define TRES	0x40
#define XIF		0x20
#define TXON    0x20
#define XRES	0x10
#define XF		0x08
#define XME		0x04
#define XREP	0x02
#define TXOFF   0x01
//CMDRH defines
#define RMC		0x80
#define RNR		0x40
#define	RSUC	0x08
#define HUNT	0x08
#define RFRD	0x02
#define RRES	0x01
//CCR0L defines
#define VIS		0x80
#define PSD		0x40
#define BCR		0x20
#define TOE		0x10
#define SSEL	0x08
#define CM		0x07
//CCR0H defines
#define PU		0x80
#define SC		0x70
#define SM		0x03
//CCR1L defines
#define CRL		0x80
#define C32		0x40
#define SOC		0x30
#define SFLG	0x08
#define DIV		0x04
#define ODS		0x02
//CCR1H defines
#define ICD		0x40
#define RTS		0x10
#define FRTS	0x08
#define FCTS	0x04
#define CAS		0x02
#define TSCM	0x01
//CCR2L defines
#define MDS		0xc0
#define ADM		0x20
#define NRM		0x10
#define PPPM	0x0c
#define TLPO	0x02
#define TLP		0x01
#define SLEN	0x08
#define BISNC	0x04
//CCR2H defines
#define MCS		0x80
#define EPT		0x40
#define NPRE	0x30
#define ITF		0x08
#define OIN		0x02
#define XCRC	0x01
#define FLON	0x01
#define CAPP	0x02
#define CRCM	0x01
//CCR3L defines
#define ELC		0x80
#define AFX		0x40
#define CSF		0x20
#define SUET	0x10
#define RAC		0x08
#define ESS7	0x01
#define TCDE	0x80
#define SLOAD	0x40
#define CHL		0x30
#define DXS		0x04
#define XBRK	0x02
#define STOP	0x01
//CCR3H defines
#define DRCRC	0x40
#define PAR		0xc0
#define RCRC	0x20
#define PARE	0x20
#define RADD	0x10
#define DPS		0x10
#define RFDF	0x08
#define RFTH	0x03
//BRRL defines
#define BRN		0x3f
//BRRH defines
#define BRM		0x0f
//TIMER3 defines
#define SRC		0x80
#define TMD		0x10
#define CNT		0x07
//IMR0 defines
#define RDO		0x80
#define RFO		0x40
#define PCE		0x20
#define FERR	0x20
#define SCD		0x20
#define RSC		0x10
#define PERR	0x10
#define RPF		0x08
#define RME		0x04
#define TCD		0x04
#define RFS		0x02
#define TIMEO	0x02
#define FLEX	0x01
//IMR1 defines
#define TIN		0x80
#define CSC		0x40
#define XMR		0x20
#define XOFFI	0x20
#define XPR		0x10
#define ALLS	0x08
#define XDU		0x04
#define XONI	0x04
#define SUEX	0x02
#define BRK		0x02
#define BRKT	0x01
//IMR2 defines
#define PLLA	0x02
#define CDSC	0x01


struct serocco_setup{
unsigned gcmdr;
unsigned gmode;
//unsigned reserved1;
//unsigned gstar;
unsigned gpdirl;
unsigned gpdirh;
unsigned gpdatl;
unsigned gpdath;
unsigned gpiml;
unsigned gpimh;
//unsigned gpisl;
//unsigned gpish;
unsigned dcmdr;
//unsigned reserved2;
//unsigned disr;
unsigned dimr;
//unsigned fifo;
//unsigned starl;
//unsigned starh;
unsigned cmdrl;
unsigned cmdrh;
unsigned ccr0l;
unsigned ccr0h;
unsigned ccr1l;
unsigned ccr1h;
unsigned ccr2l;
unsigned ccr2h;
unsigned ccr3l;
unsigned ccr3h;
unsigned preamb;
unsigned tolen;
unsigned accm0;
unsigned accm1;
unsigned accm2;
unsigned accm3;
unsigned udac0;
unsigned udac1;
unsigned udac2;
unsigned udac3;
unsigned ttsa0;
unsigned ttsa1;
unsigned ttsa2;
unsigned ttsa3;
unsigned rtsa0;
unsigned rtsa1;
unsigned rtsa2;
unsigned rtsa3;
unsigned pcmtx0;
unsigned pcmtx1;
unsigned pcmtx2;
unsigned pcmtx3;
unsigned pcmrx0;
unsigned pcmrx1;
unsigned pcmrx2;
unsigned pcmrx3;
unsigned brrl;
unsigned brrh;
unsigned timer0;
unsigned timer1;
unsigned timer2;
unsigned timer3;
unsigned xad1;
unsigned xad2;
unsigned ral1;
unsigned rah1;
unsigned ral2;
unsigned rah2;
unsigned amral1;
unsigned amrah1;
unsigned amral2;
unsigned amrah2;
unsigned rlcrl;
unsigned rlcrh;
unsigned xon;
unsigned xoff;
unsigned mxon;
unsigned mxoff;
unsigned tcr;
unsigned ticr;
//unsigned isr0;
//unsigned isr1;
//unsigned isr2;
unsigned imr0;
unsigned imr1;
unsigned imr2;
//unsigned reserved3;
//unsigned rsta;
//unsigned reserved4;
unsigned syncl;
unsigned synch;

unsigned n_rbufs;
unsigned n_tbufs;
unsigned n_rfsize_max;
unsigned n_tfsize_max;

};

struct setup{
//used in all
unsigned cmdr;
unsigned mode;
unsigned timr;
unsigned xbcl;
unsigned xbch;
unsigned ccr0;
unsigned ccr1;
unsigned ccr2;
unsigned ccr3;
unsigned ccr4;
unsigned tsax;
unsigned tsar;
unsigned xccr;
unsigned rccr;
unsigned bgr;
unsigned iva;
unsigned ipc;
unsigned imr0;
unsigned imr1;
unsigned pvr;
unsigned pim;
unsigned pcr;
//escc register defines for HDLC/SDLC mode
unsigned xad1;
unsigned xad2;
unsigned rah1;
unsigned rah2;
unsigned ral1;
unsigned ral2;
unsigned rlcr;
unsigned aml;
unsigned amh;
unsigned pre;
//escc async register defines (used in bisync as well)
unsigned xon;
unsigned xoff;
unsigned tcr;
unsigned dafo;
unsigned rfc;
unsigned tic;
unsigned mxn;
unsigned mxf;

//escc bisync register defines
unsigned synl;
unsigned synh;

unsigned n_rbufs;
unsigned n_tbufs;
unsigned n_rfsize_max;
unsigned n_tfsize_max;

};


struct device {
struct setup ESCCsetup;
struct serocco_setup SEROCCOsetup;
};

void convertESCCtoSEROCCO(struct device *dev)
{
//go through dev->ESCCsetup and poke the right stuff into dev->SEROCCOsetup
memset(&dev->SEROCCOsetup,0,sizeof(struct serocco_setup));

if((dev->ESCCsetup.ccr0&0x03)==0x00)
{
//hdlc
if((dev->ESCCsetup.cmdr&0x80)==0x80) dev->SEROCCOsetup.cmdrh|=RMC;
if((dev->ESCCsetup.cmdr&0x40)==0x40) dev->SEROCCOsetup.cmdrh|=RRES;
if((dev->ESCCsetup.cmdr&0x20)==0x20) dev->SEROCCOsetup.cmdrh|=RNR;
if((dev->ESCCsetup.cmdr&0x10)==0x10) dev->SEROCCOsetup.cmdrl|=STI;
if((dev->ESCCsetup.cmdr&0x08)==0x08) dev->SEROCCOsetup.cmdrl|=XF;
if((dev->ESCCsetup.cmdr&0x04)==0x04) dev->SEROCCOsetup.cmdrl|=XIF;
if((dev->ESCCsetup.cmdr&0x02)==0x02) dev->SEROCCOsetup.cmdrl|=XME;
if((dev->ESCCsetup.cmdr&0x01)==0x01) dev->SEROCCOsetup.cmdrl|=XRES;
//hdlc
dev->SEROCCOsetup.ccr2l |= dev->ESCCsetup.mode&0xe0;//MDS1:MDS0:ADM
if((dev->ESCCsetup.mode&0x10)==0x10) dev->SEROCCOsetup.timer3 |= TMD;

dev->SEROCCOsetup.xad1 = dev->ESCCsetup.xad1;
dev->SEROCCOsetup.xad2 = dev->ESCCsetup.xad2;
dev->SEROCCOsetup.rah1 = dev->ESCCsetup.rah1;
dev->SEROCCOsetup.rah2 = dev->ESCCsetup.rah2;
dev->SEROCCOsetup.ral1 = dev->ESCCsetup.ral1;
dev->SEROCCOsetup.ral2 = dev->ESCCsetup.ral2;
if((dev->ESCCsetup.ccr1&0x80)==0x80) dev->SEROCCOsetup.ccr1l |= SFLG;
if(((((dev->ESCCsetup.ccr0&0x1c)>>2)&0x07)==0x01)||((((dev->ESCCsetup.ccr0&0x1c)>>2)&0x07)==0x03))
{
//if bus mode then ITF bit is OIN, and ITF is 0
if((dev->ESCCsetup.ccr1&0x08)==0x08) dev->SEROCCOsetup.ccr2h |= OIN;
}
else 
{
//not bus mode then ITF is ITF
if((dev->ESCCsetup.ccr1&0x08)==0x08) dev->SEROCCOsetup.ccr2h |= ITF;
}

if((dev->ESCCsetup.ccr2&0x02)==0x02) dev->SEROCCOsetup.ccr1l |= C32;
//if clockmode 1,4 or 5 then copy SOC bits from ccr2
if((dev->ESCCsetup.ccr1&0x07)==0x01) dev->SEROCCOsetup.ccr1l |= ((dev->ESCCsetup.ccr2&0xc0)>>2);
if((dev->ESCCsetup.ccr1&0x07)==0x04) dev->SEROCCOsetup.ccr1l |= ((dev->ESCCsetup.ccr2&0xc0)>>2);
if((dev->ESCCsetup.ccr1&0x07)==0x05) dev->SEROCCOsetup.ccr1l |= ((dev->ESCCsetup.ccr2&0xc0)>>2);

if((dev->ESCCsetup.ccr3&0x20)==0x20) dev->SEROCCOsetup.ccr2h |= EPT;
dev->SEROCCOsetup.ccr2h |= (dev->ESCCsetup.ccr3&0xc0)>>2;//pre0 and pre1
if((dev->ESCCsetup.ccr3&0x08)==0x08) dev->SEROCCOsetup.ccr1l |= CRL;

if((dev->ESCCsetup.ccr3&0x10)==0x10) dev->SEROCCOsetup.ccr3h |= RADD;
if((dev->ESCCsetup.ccr3&0x04)==0x04) dev->SEROCCOsetup.ccr3h |= RCRC;
if((dev->ESCCsetup.ccr3&0x02)==0x02) dev->SEROCCOsetup.ccr2h |= XCRC;

dev->SEROCCOsetup.ccr3h |= (dev->ESCCsetup.ccr4&0x3);//RFTH bits


dev->SEROCCOsetup.imr0=0xff;
dev->SEROCCOsetup.imr1=0xff;
dev->SEROCCOsetup.imr2=0xff;

//HDLC interrupts
if((dev->ESCCsetup.imr0&0x80)==0x00) dev->SEROCCOsetup.imr0 &= ~RME;
if((dev->ESCCsetup.imr0&0x40)==0x00) dev->SEROCCOsetup.imr0 &= ~RFS;
if((dev->ESCCsetup.imr0&0x20)==0x00) dev->SEROCCOsetup.imr0 &= ~RSC;
if((dev->ESCCsetup.imr0&0x10)==0x00) dev->SEROCCOsetup.imr0 &= ~PCE;
if((dev->ESCCsetup.imr0&0x08)==0x00) dev->SEROCCOsetup.imr2 &= ~PLLA;
if((dev->ESCCsetup.imr0&0x04)==0x00) dev->SEROCCOsetup.imr2 &= ~CDSC;
if((dev->ESCCsetup.imr0&0x02)==0x00) dev->SEROCCOsetup.imr0 &= ~RFO;
if((dev->ESCCsetup.imr0&0x01)==0x00) dev->SEROCCOsetup.imr0 &= ~RPF;

//if(dev->ESCCsetup.imr1&0x80)==0x80) dev->SEROCCOsetup.imr0 |= RME;
if((dev->ESCCsetup.imr1&0x40)==0x00) dev->SEROCCOsetup.imr0 &= ~RDO;
if((dev->ESCCsetup.imr1&0x20)==0x00) dev->SEROCCOsetup.imr1 &= ~ALLS;
if((dev->ESCCsetup.imr1&0x10)==0x00) dev->SEROCCOsetup.imr1 &= ~XDU;
if((dev->ESCCsetup.imr1&0x08)==0x00) dev->SEROCCOsetup.imr1 &= ~TIN;
if((dev->ESCCsetup.imr1&0x04)==0x00) dev->SEROCCOsetup.imr1 &= ~CSC;
if((dev->ESCCsetup.imr1&0x02)==0x00) dev->SEROCCOsetup.imr1 &= ~XMR;
if((dev->ESCCsetup.imr1&0x01)==0x00) dev->SEROCCOsetup.imr1 &= ~XPR;

dev->SEROCCOsetup.rlcrl = dev->ESCCsetup.rlcr&0x7f;
if((dev->ESCCsetup.rlcr&0x80)==0x80) dev->SEROCCOsetup.rlcrh |= 0x80;//turn on receive length check

dev->SEROCCOsetup.amral1 = dev->ESCCsetup.aml;
dev->SEROCCOsetup.amrah1 = dev->ESCCsetup.amh;
dev->SEROCCOsetup.amral2 = dev->ESCCsetup.aml;
dev->SEROCCOsetup.amrah2 = dev->ESCCsetup.amh;

dev->SEROCCOsetup.preamb = dev->ESCCsetup.pre;

}//end hdlc
if((dev->ESCCsetup.ccr0&0x03)==0x02)
{
//bisync
if((dev->ESCCsetup.cmdr&0x80)==0x80) dev->SEROCCOsetup.cmdrh|=RMC;
if((dev->ESCCsetup.cmdr&0x40)==0x40) dev->SEROCCOsetup.cmdrh|=RRES;
if((dev->ESCCsetup.cmdr&0x20)==0x20) dev->SEROCCOsetup.cmdrh|=RFRD;
if((dev->ESCCsetup.cmdr&0x10)==0x10) dev->SEROCCOsetup.cmdrl|=STI;
if((dev->ESCCsetup.cmdr&0x08)==0x08) dev->SEROCCOsetup.cmdrl|=XF;
if((dev->ESCCsetup.cmdr&0x04)==0x04) dev->SEROCCOsetup.cmdrh|=HUNT;
if((dev->ESCCsetup.cmdr&0x02)==0x02) dev->SEROCCOsetup.cmdrl|=XME;
if((dev->ESCCsetup.cmdr&0x01)==0x01) dev->SEROCCOsetup.cmdrl|=XRES;
//bisync
if((dev->ESCCsetup.mode&0x20)==0x20) dev->SEROCCOsetup.ccr2l |= SLEN;
if((dev->ESCCsetup.mode&0x10)==0x10) dev->SEROCCOsetup.ccr2l |= BISNC;

if((dev->ESCCsetup.ccr3&0x20)==0x20) dev->SEROCCOsetup.ccr2h |= EPT;
dev->SEROCCOsetup.ccr2h |= (dev->ESCCsetup.ccr3&0xc0)>>2;//pre0 and pre1
if((dev->ESCCsetup.ccr3&0x08)==0x08) dev->SEROCCOsetup.ccr1l |= CRL;

//if((dev->ESCCsetup.ccr3&0x10)==0x10) dev->SEROCCOsetup. |= CON;//exists where?
if((dev->ESCCsetup.ccr3&0x04)==0x04) dev->SEROCCOsetup.ccr2h |= CAPP;
if((dev->ESCCsetup.ccr3&0x02)==0x02) dev->SEROCCOsetup.ccr2h |= CRCM;

dev->SEROCCOsetup.imr0=0xff;
dev->SEROCCOsetup.imr1=0xff;
dev->SEROCCOsetup.imr2=0xff;

//BISYNC interrupts
if((dev->ESCCsetup.imr0&0x80)==0x00) dev->SEROCCOsetup.imr0 &= ~TCD;
//if((dev->ESCCsetup.imr0&0x40)==0x00) dev->SEROCCOsetup.imr0 &= ~TIMEO;
if((dev->ESCCsetup.imr0&0x20)==0x00) dev->SEROCCOsetup.imr0 &= ~PERR;
if((dev->ESCCsetup.imr0&0x10)==0x00) dev->SEROCCOsetup.imr0 &= ~SCD;
if((dev->ESCCsetup.imr0&0x08)==0x00) dev->SEROCCOsetup.imr2 &= ~PLLA;
if((dev->ESCCsetup.imr0&0x04)==0x00) dev->SEROCCOsetup.imr2 &= ~CDSC;
if((dev->ESCCsetup.imr0&0x02)==0x00) dev->SEROCCOsetup.imr0 &= ~RFO;
if((dev->ESCCsetup.imr0&0x01)==0x00) dev->SEROCCOsetup.imr0 &= ~RPF;

//if((dev->ESCCsetup.imr1&0x80)==0x80) dev->SEROCCOsetup.imr1 &= ~BRK;
//if((dev->ESCCsetup.imr1&0x40)==0x40) dev->SEROCCOsetup.imr0 &= ~BRKT;
if((dev->ESCCsetup.imr1&0x20)==0x00) dev->SEROCCOsetup.imr1 &= ~ALLS;
if((dev->ESCCsetup.imr1&0x10)==0x00) dev->SEROCCOsetup.imr1 &= ~XDU;
if((dev->ESCCsetup.imr1&0x08)==0x00) dev->SEROCCOsetup.imr1 &= ~TIN;
if((dev->ESCCsetup.imr1&0x04)==0x00) dev->SEROCCOsetup.imr1 &= ~CSC;
if((dev->ESCCsetup.imr1&0x02)==0x00) dev->SEROCCOsetup.imr1 &= ~XMR;
if((dev->ESCCsetup.imr1&0x01)==0x00) dev->SEROCCOsetup.imr1 &= ~XPR;

dev->SEROCCOsetup.preamb = dev->ESCCsetup.pre;

dev->SEROCCOsetup.tcr = dev->ESCCsetup.tcr;

if((dev->ESCCsetup.rfc&0x40)==0x40) dev->SEROCCOsetup.ccr3h |= DPS;

	//bisync
dev->SEROCCOsetup.ccr3h |= ((dev->ESCCsetup.dafo&0x1c)<<3);//PAR bits and PARE
dev->SEROCCOsetup.ccr3l |= ((dev->ESCCsetup.dafo&0x3)<<4);//CHL1:0
if((dev->ESCCsetup.rfc&0x10)==0x10) dev->SEROCCOsetup.ccr3h |= RFDF;

	if((dev->ESCCsetup.rfc&0x20)==0x20) dev->SEROCCOsetup.ccr3l |= SLOAD;

dev->SEROCCOsetup.ccr3h |= ((dev->ESCCsetup.rfc&0x0c)>>2);//RFTH bits

if((dev->ESCCsetup.rfc&0x01)==0x01) dev->SEROCCOsetup.ccr3l |= TCDE;

dev->SEROCCOsetup.syncl = dev->ESCCsetup.synl;
dev->SEROCCOsetup.synch = dev->ESCCsetup.synh;

}//end bisync
if((dev->ESCCsetup.ccr0&0x03)==0x03)
{
//async
if((dev->ESCCsetup.cmdr&0x80)==0x80) dev->SEROCCOsetup.cmdrh|=RMC;
if((dev->ESCCsetup.cmdr&0x40)==0x40) dev->SEROCCOsetup.cmdrh|=RRES;
if((dev->ESCCsetup.cmdr&0x20)==0x20) dev->SEROCCOsetup.cmdrh|=RFRD;
if((dev->ESCCsetup.cmdr&0x10)==0x10) dev->SEROCCOsetup.cmdrl|=STI;
if((dev->ESCCsetup.cmdr&0x08)==0x08) dev->SEROCCOsetup.cmdrl|=XF;
//if((dev->ESCCsetup.cmdr&0x04)==0x04) dev->SEROCCOsetup.cmdrh|=HUNT;
//if((dev->ESCCsetup.cmdr&0x02)==0x02) dev->SEROCCOsetup.cmdrl|=XME;
if((dev->ESCCsetup.cmdr&0x01)==0x01) dev->SEROCCOsetup.cmdrl|=XRES;
//async
if((dev->ESCCsetup.mode&0x40)==0x40) dev->SEROCCOsetup.ccr1h |= FRTS;
if((dev->ESCCsetup.mode&0x20)==0x20) dev->SEROCCOsetup.ccr1h |= FCTS;
if((dev->ESCCsetup.mode&0x10)==0x10) dev->SEROCCOsetup.ccr2h |= FLON;

if((dev->ESCCsetup.ccr1&0x08)==0x08) dev->SEROCCOsetup.ccr0l |= BCR;

dev->SEROCCOsetup.imr0=0xff;
dev->SEROCCOsetup.imr1=0xff;
dev->SEROCCOsetup.imr2=0xff;

//ASYNC interrupts
if((dev->ESCCsetup.imr0&0x80)==0x00) dev->SEROCCOsetup.imr0 &= ~TCD;
if((dev->ESCCsetup.imr0&0x40)==0x00) dev->SEROCCOsetup.imr0 &= ~TIMEO;
if((dev->ESCCsetup.imr0&0x20)==0x00) dev->SEROCCOsetup.imr0 &= ~PERR;
if((dev->ESCCsetup.imr0&0x10)==0x00) dev->SEROCCOsetup.imr0 &= ~FERR;
if((dev->ESCCsetup.imr0&0x08)==0x00) dev->SEROCCOsetup.imr2 &= ~PLLA;
if((dev->ESCCsetup.imr0&0x04)==0x00) dev->SEROCCOsetup.imr2 &= ~CDSC;
if((dev->ESCCsetup.imr0&0x02)==0x00) dev->SEROCCOsetup.imr0 &= ~RFO;
if((dev->ESCCsetup.imr0&0x01)==0x00) dev->SEROCCOsetup.imr0 &= ~RPF;

if((dev->ESCCsetup.imr1&0x80)==0x00) dev->SEROCCOsetup.imr1 &= ~BRK;
if((dev->ESCCsetup.imr1&0x40)==0x00) dev->SEROCCOsetup.imr1 &= ~BRKT;
if((dev->ESCCsetup.imr1&0x20)==0x00) dev->SEROCCOsetup.imr1 &= ~ALLS;
if((dev->ESCCsetup.imr1&0x10)==0x00) dev->SEROCCOsetup.imr1 &= ~XOFFI;
if((dev->ESCCsetup.imr1&0x08)==0x00) dev->SEROCCOsetup.imr1 &= ~TIN;
if((dev->ESCCsetup.imr1&0x04)==0x00) dev->SEROCCOsetup.imr1 &= ~CSC;
if((dev->ESCCsetup.imr1&0x02)==0x00) dev->SEROCCOsetup.imr1 &= ~XONI;
if((dev->ESCCsetup.imr1&0x01)==0x00) dev->SEROCCOsetup.imr1 &= ~XPR;

dev->SEROCCOsetup.xon = dev->ESCCsetup.xon;
dev->SEROCCOsetup.xoff =dev->ESCCsetup.xoff;

dev->SEROCCOsetup.mxon = dev->ESCCsetup.mxn;
dev->SEROCCOsetup.mxoff = dev->ESCCsetup.mxf;

dev->SEROCCOsetup.tcr = dev->ESCCsetup.tcr;

if((dev->ESCCsetup.rfc&0x40)==0x40) dev->SEROCCOsetup.ccr3h |= DPS;


	//async
if((dev->ESCCsetup.dafo&0x40)==0x40) dev->SEROCCOsetup.ccr3l |= XBRK;
if((dev->ESCCsetup.dafo&0x20)==0x20) dev->SEROCCOsetup.ccr3l |= STOP;
dev->SEROCCOsetup.ccr3h |= ((dev->ESCCsetup.dafo&0x1c)<<3);//PAR bits and PARE
dev->SEROCCOsetup.ccr3l |= ((dev->ESCCsetup.dafo&0x3)<<4);//CHL1:0
if((dev->ESCCsetup.rfc&0x10)==0x10) dev->SEROCCOsetup.ccr3h |= RFDF;

	if((dev->ESCCsetup.rfc&0x20)==0x20) dev->SEROCCOsetup.ccr3l |= DXS;


dev->SEROCCOsetup.ccr3h |= ((dev->ESCCsetup.rfc&0x0c)>>2);//RFTH bits

if((dev->ESCCsetup.rfc&0x01)==0x01) dev->SEROCCOsetup.ccr3l |= TCDE;

dev->SEROCCOsetup.tolen = 0x80;//the 82532 has 4 character timeout length so default it to this here

}//end async



if((dev->ESCCsetup.mode&0x08)==0x08) dev->SEROCCOsetup.ccr3l |= RAC;
if((dev->ESCCsetup.mode&0x04)==0x04) dev->SEROCCOsetup.ccr1h |= RTS;//RTS=1, FRTS=0 so RTS is activated when this bit is set
if((dev->ESCCsetup.mode&0x02)==0x02)
	{
	unsigned long tempval;
	//TRS=1, 512 clocks
	tempval = 0;
	if((dev->ESCCsetup.timr&0xe0)==0xe0)
		{
		tempval = (dev->ESCCsetup.timr&0x1f) +1;
		dev->SEROCCOsetup.timer3 |= ((dev->ESCCsetup.timr>>5)&0x7);///CNT value
		}
	else
		{
		tempval = (dev->ESCCsetup.timr&0xff) +1;
		}	
	tempval = tempval << 9; //*512
	tempval = tempval - 1;
	dev->SEROCCOsetup.timer0 = tempval&0xff;
	dev->SEROCCOsetup.timer1 = (tempval>>8)&0xff;
	dev->SEROCCOsetup.timer2 = (tempval>>16)&0xff;

	}
else
	{
	unsigned long tempval;
	//TRS=0, 32768 clocks
	tempval = 0;
	if((dev->ESCCsetup.timr&0xe0)==0xe0)
		{
		tempval = (dev->ESCCsetup.timr&0x1f) +1;
		dev->SEROCCOsetup.timer3 |= ((dev->ESCCsetup.timr>>5)&0x7);///CNT value
		}
	else
		{
		tempval = (dev->ESCCsetup.timr&0xff) +1;
		}	
	tempval = tempval << 15; //*32768
	tempval = tempval - 1;
	dev->SEROCCOsetup.timer0 = tempval&0xff;
	dev->SEROCCOsetup.timer1 = (tempval>>8)&0xff;
	dev->SEROCCOsetup.timer2 = (tempval>>16)&0xff;

	}

//dev->SEROCCOsetup.timer3 |= ((dev->ESCCsetup.timr>>5)&0x7);///CNT value
if((dev->ESCCsetup.mode&0x01)==0x01) dev->SEROCCOsetup.ccr2l|=TLP;


if((dev->ESCCsetup.ccr0&0x80)==0x80) dev->SEROCCOsetup.ccr0h |= PU;
if((dev->ESCCsetup.ccr0&0x80)==0x40);//no mce bit on serocco
dev->SEROCCOsetup.ccr0h |= ((dev->ESCCsetup.ccr0&0x1c)<<2);//line control (SC)
dev->SEROCCOsetup.ccr0h |= (dev->ESCCsetup.ccr0&0x03);//protocol mode (SM)


if((dev->ESCCsetup.ccr1&0x10)==0x10) dev->SEROCCOsetup.ccr1l |= ODS;

dev->SEROCCOsetup.ccr0l |= (dev->ESCCsetup.ccr1&0x07);//cm bits

if((dev->ESCCsetup.ccr2&0x01)==0x01) dev->SEROCCOsetup.ccr1l |= DIV;
if((dev->ESCCsetup.ccr2&0x08)==0x08) dev->SEROCCOsetup.ccr0l |= TOE;
if((dev->ESCCsetup.ccr2&0x10)==0x10) dev->SEROCCOsetup.ccr0l |= SSEL;


if((dev->ESCCsetup.ccr2&0x20)==0x00) 
	{
	//then the divisor is zero ie dev->SEROCCOsetup.brrl=0, and dev->SEROCCOsetup.brrh=0
	//(which is defaulted by the memset above)
	}
else
	{
	if((dev->ESCCsetup.ccr4&0x40)==0x40)
		{
		//ebgr map M and N
		dev->SEROCCOsetup.brrh = ((dev->ESCCsetup.ccr2&0xc0)>>4) | (dev->ESCCsetup.bgr>>6);
		dev->SEROCCOsetup.brrl = (dev->ESCCsetup.bgr&0x3f);
		}
	else
		{
		unsigned long tempval;
		tempval = dev->ESCCsetup.ccr2&0xc0;
		tempval = tempval <<2;
		tempval |= dev->ESCCsetup.bgr;
		if((tempval&0xffffffc0)==0)
			{
			dev->SEROCCOsetup.brrh = 1;
			dev->SEROCCOsetup.brrl = tempval&0x3f;
			}
		else
			{
			//well we have a conundrum here. They aren't in ebgr mode and the divisor is greater than 64, which makes 
			//it difficult, we are going to map it here by dividing their orignal value by 2 until it fits.
			tempval = tempval>>1;
			if((tempval&0xffffffc0)==0)
				{
				dev->SEROCCOsetup.brrh = 2;
				dev->SEROCCOsetup.brrl = tempval&0x3f;
				}
			else
				{
				tempval = tempval>>1;
				if((tempval&0xffffffc0)==0)
					{
					dev->SEROCCOsetup.brrh = 3;
					dev->SEROCCOsetup.brrl = tempval&0x3f;
					}
				else
					{
					tempval = tempval>>1;
					if((tempval&0xffffffc0)==0)
						{
						dev->SEROCCOsetup.brrh = 4;
						dev->SEROCCOsetup.brrl = tempval&0x3f;
						}
					else
						{
						tempval = tempval>>1;
						dev->SEROCCOsetup.brrh = 5;
						dev->SEROCCOsetup.brrl = tempval&0x3f;
						}
					}
				}
			}
		}
	}



if((dev->ESCCsetup.ccr3&0x01)==0x01) dev->SEROCCOsetup.ccr0l |= PSD;

if((dev->ESCCsetup.ccr4&0x10)==0x10) dev->SEROCCOsetup.ccr1h |= ICD;

//tsax,tsar,xccr,rccr, not supported in this conversion
//iva is irrelavent

if((dev->ESCCsetup.ipc&0x80)==0x80) dev->SEROCCOsetup.ccr0l |= VIS;
//bits IC0 and IC1 are overrided in the global config register to be pushpull active high
dev->SEROCCOsetup.gmode = 0x3a;//ipc0:1 both 1 (pushpull/high) osc powerdown and shaper disabled as we have an external clock driving XTAL1 not a crystal.
dev->SEROCCOsetup.gpdirl = 0x06;//bits 9&10 are inputs (DSR0&1) 8 is output (DTRB)
dev->SEROCCOsetup.gpdirh = 0xbf;//bit 6 is output (DTRA) bits 0,1,2 are inputs
dev->SEROCCOsetup.gpiml  = 0x7;
dev->SEROCCOsetup.gpimh  = 0xff;
if((dev->ESCCsetup.pim&0x20)==0x00) dev->SEROCCOsetup.gpiml &= 0xfd;//if dsr0 is unmasked on escc unmask it here
if((dev->ESCCsetup.pim&0x40)==0x00) dev->SEROCCOsetup.gpiml &= 0xfb;//if dsr1 is unmasked on escc unmask it here


if((dev->ESCCsetup.pvr&0x08)==0x08) dev->SEROCCOsetup.gpdath |=0x40;//set DTR0
if((dev->ESCCsetup.pvr&0x10)==0x10) dev->SEROCCOsetup.gpdatl |=0x01;//set DTR1


dev->SEROCCOsetup.n_rbufs = dev->ESCCsetup.n_rbufs;
dev->SEROCCOsetup.n_tbufs = dev->ESCCsetup.n_tbufs;
dev->SEROCCOsetup.n_rfsize_max = dev->ESCCsetup.n_rfsize_max;
dev->SEROCCOsetup.n_tfsize_max = dev->ESCCsetup.n_tfsize_max;
}

int main(int argc, char *argv[])
{
	int port;
	int i;
	FILE *fp;
	char inputline[100],*ptr;
	unsigned long value;
	DWORD ret;
	struct setup settings;
	struct device dev;
//open up setup struct and do conversion
if(argc<2)
{
printf("usage:%s esccsettingsfile\n",argv[0]);
exit(1);
}
	fp = fopen(argv[1],"r");
	if(fp==NULL)
	{
		printf("Cannot open file %s\n",argv[2]);
		exit(1);
	}
	
	memset(&settings,0,sizeof(struct setup));
	memset(inputline,0,sizeof(inputline));
	
	while(fgets(inputline,100,fp)!=NULL)
	{
		/* zero the comments so not to get confused with key words */
		/* # pound is the comment prefix */
		for(i=0;inputline[i]!='\n';i++)
		{
			if(inputline[i]=='#')
			{
				memset(&inputline[i],0, 100-i );	
				break;
			}
			
		}
		i = 0;
		while(inputline[i]!=0) 
		{
			inputline[i] = tolower(inputline[i]);
			i++;
		}
		
		if( (ptr = strchr(inputline,'='))!=NULL)
		{
			for(i=1;ptr[i]==' ' && ptr[i]!='\n';i++);
			value = (unsigned long)strtoul(ptr+i,NULL,16);
			//printf("value = 0x%lx\n",(long)value);
		}
		else	value=0;
		
		if(strncmp(inputline,"cmdr",4)==0) settings.cmdr = value;
		if(strncmp(inputline,"mode",4)==0) settings.mode = value;
		if(strncmp(inputline,"timr",4)==0) settings.timr = value;
		if(strncmp(inputline,"xbcl",4)==0) settings.xbcl = value;
		if(strncmp(inputline,"xbch",4)==0) settings.xbch = value;
		if(strncmp(inputline,"ccr0",4)==0) settings.ccr0 = value;
		if(strncmp(inputline,"ccr1",4)==0) settings.ccr1 = value;
		if(strncmp(inputline,"ccr2",4)==0) settings.ccr2 = value;
		if(strncmp(inputline,"ccr3",4)==0) settings.ccr3 = value;
		if(strncmp(inputline,"ccr4",4)==0) settings.ccr4 = value;
		if(strncmp(inputline,"tsax",4)==0) settings.tsax = value;
		if(strncmp(inputline,"tsar",4)==0) settings.tsar = value;
		if(strncmp(inputline,"xccr",4)==0) settings.xccr = value;
		if(strncmp(inputline,"rccr",4)==0) settings.rccr = value;
		if(strncmp(inputline,"bgr",3)==0) settings.bgr = value;
		if(strncmp(inputline,"iva",3)==0) settings.iva = value;
		if(strncmp(inputline,"ipc",3)==0) settings.ipc = value;
		if(strncmp(inputline,"imr0",4)==0) settings.imr0 = value;
		if(strncmp(inputline,"imr1",4)==0) settings.imr1 = value;
		if(strncmp(inputline,"pvr",3)==0) settings.pvr = value;
		if(strncmp(inputline,"pim",3)==0) settings.pim = value;
		if(strncmp(inputline,"pcr",3)==0) settings.pcr = value;
		if(strncmp(inputline,"xad1",4)==0) settings.xad1 = value;
		if(strncmp(inputline,"xad2",4)==0) settings.xad2 = value;
		if(strncmp(inputline,"rah1",4)==0) settings.rah1 = value;
		if(strncmp(inputline,"rah2",4)==0) settings.rah2 = value;
		if(strncmp(inputline,"ral1",4)==0) settings.ral1 = value;
		if(strncmp(inputline,"ral2",4)==0) settings.ral2 = value;
		if(strncmp(inputline,"rlcr",4)==0) settings.rlcr = value;
		if(strncmp(inputline,"aml",3)==0) settings.aml = value;
		if(strncmp(inputline,"amh",3)==0) settings.amh = value;
		if(strncmp(inputline,"pre",3)==0) settings.pre = value;
		if(strncmp(inputline,"xon",3)==0) settings.xon = value;
		if(strncmp(inputline,"xoff",4)==0) settings.xoff = value;
		if(strncmp(inputline,"tcr",3)==0) settings.tcr = value;
		if(strncmp(inputline,"dafo",4)==0) settings.dafo = value;
		if(strncmp(inputline,"rfc",3)==0) settings.rfc = value;
		if(strncmp(inputline,"tic",3)==0) settings.tic = value;
		if(strncmp(inputline,"mxn",3)==0) settings.mxn = value;
		if(strncmp(inputline,"mxf",3)==0) settings.mxf = value;
		if(strncmp(inputline,"synl",4)==0) settings.synl = value;
		if(strncmp(inputline,"synh",4)==0) settings.synh = value;
		if(strncmp(inputline,"n_rbufs",7)==0) settings.n_rbufs = value;
		if(strncmp(inputline,"n_tbufs",7)==0) settings.n_tbufs = value;
		if(strncmp(inputline,"n_rfsize_max",12)==0) settings.n_rfsize_max = value;
		if(strncmp(inputline,"n_tfsize_max",12)==0) settings.n_tfsize_max = value;
		memset(inputline,0,100);
	}

//here settings holds the escc setup
memcpy(&dev.ESCCsetup,&settings,sizeof(struct setup));
convertESCCtoSEROCCO(&dev);
//print out results:
printf("gcmdr   =0x%4.4x\n",dev.SEROCCOsetup.gcmdr);
printf("gmode   =0x%4.4x\n",dev.SEROCCOsetup.gmode);
printf("gpdirl  =0x%4.4x\n",dev.SEROCCOsetup.gpdirl);
printf("gpdirh  =0x%4.4x\n",dev.SEROCCOsetup.gpdirh);
printf("gpdatl  =0x%4.4x\n",dev.SEROCCOsetup.gpdatl);
printf("gpdath  =0x%4.4x\n",dev.SEROCCOsetup.gpdath);
printf("gpiml   =0x%4.4x\n",dev.SEROCCOsetup.gpiml);
printf("gpimh   =0x%4.4x\n",dev.SEROCCOsetup.gpimh);
printf("dcmdr   =0x%4.4x\n",dev.SEROCCOsetup.dcmdr);
printf("dimr    =0x%4.4x\n",dev.SEROCCOsetup.dimr);
printf("cmdrl   =0x%4.4x\n",dev.SEROCCOsetup.cmdrl);
printf("cmdrh   =0x%4.4x\n",dev.SEROCCOsetup.cmdrh);
printf("ccr0l   =0x%4.4x\n",dev.SEROCCOsetup.ccr0l);
printf("ccr0h   =0x%4.4x\n",dev.SEROCCOsetup.ccr0h);
printf("ccr1l   =0x%4.4x\n",dev.SEROCCOsetup.ccr1l);
printf("ccr1h   =0x%4.4x\n",dev.SEROCCOsetup.ccr1h);
printf("ccr2l   =0x%4.4x\n",dev.SEROCCOsetup.ccr2l);
printf("ccr2h   =0x%4.4x\n",dev.SEROCCOsetup.ccr2h);
printf("ccr3l   =0x%4.4x\n",dev.SEROCCOsetup.ccr3l);
printf("ccr3h   =0x%4.4x\n",dev.SEROCCOsetup.ccr3h);
printf("preamb  =0x%4.4x\n",dev.SEROCCOsetup.preamb);
printf("tolen   =0x%4.4x\n",dev.SEROCCOsetup.tolen);
printf("accm0   =0x%4.4x\n",dev.SEROCCOsetup.accm0);
printf("accm1   =0x%4.4x\n",dev.SEROCCOsetup.accm1);
printf("accm2   =0x%4.4x\n",dev.SEROCCOsetup.accm2);
printf("accm3   =0x%4.4x\n",dev.SEROCCOsetup.accm3);
printf("udac0   =0x%4.4x\n",dev.SEROCCOsetup.udac0);
printf("udac1   =0x%4.4x\n",dev.SEROCCOsetup.udac1);
printf("udac2   =0x%4.4x\n",dev.SEROCCOsetup.udac2);
printf("udac3   =0x%4.4x\n",dev.SEROCCOsetup.udac3);
printf("ttsa0   =0x%4.4x\n",dev.SEROCCOsetup.ttsa0);
printf("ttsa1   =0x%4.4x\n",dev.SEROCCOsetup.ttsa1);
printf("ttsa2   =0x%4.4x\n",dev.SEROCCOsetup.ttsa2);
printf("ttsa3   =0x%4.4x\n",dev.SEROCCOsetup.ttsa3);
printf("rtsa0   =0x%4.4x\n",dev.SEROCCOsetup.rtsa0);
printf("rtsa1   =0x%4.4x\n",dev.SEROCCOsetup.rtsa1);
printf("rtsa2   =0x%4.4x\n",dev.SEROCCOsetup.rtsa2);
printf("rtsa3   =0x%4.4x\n",dev.SEROCCOsetup.rtsa3);
printf("pcmtx0  =0x%4.4x\n",dev.SEROCCOsetup.pcmtx0);
printf("pcmtx1  =0x%4.4x\n",dev.SEROCCOsetup.pcmtx1);
printf("pcmtx2  =0x%4.4x\n",dev.SEROCCOsetup.pcmtx2);
printf("pcmtx3  =0x%4.4x\n",dev.SEROCCOsetup.pcmtx3);
printf("pcmrx0  =0x%4.4x\n",dev.SEROCCOsetup.pcmrx0);
printf("pcmrx1  =0x%4.4x\n",dev.SEROCCOsetup.pcmrx1);
printf("pcmrx2  =0x%4.4x\n",dev.SEROCCOsetup.pcmrx2);
printf("pcmrx3  =0x%4.4x\n",dev.SEROCCOsetup.pcmrx3);
printf("brrl    =0x%4.4x\n",dev.SEROCCOsetup.brrl);
printf("brrh    =0x%4.4x\n",dev.SEROCCOsetup.brrh);
printf("timer0  =0x%4.4x\n",dev.SEROCCOsetup.timer0);
printf("timer1  =0x%4.4x\n",dev.SEROCCOsetup.timer1);
printf("timer2  =0x%4.4x\n",dev.SEROCCOsetup.timer2);
printf("timer3  =0x%4.4x\n",dev.SEROCCOsetup.timer3);
printf("xad1    =0x%4.4x\n",dev.SEROCCOsetup.xad1);
printf("xad2    =0x%4.4x\n",dev.SEROCCOsetup.xad2);
printf("ral1    =0x%4.4x\n",dev.SEROCCOsetup.ral1);
printf("rah1    =0x%4.4x\n",dev.SEROCCOsetup.rah1);
printf("ral2    =0x%4.4x\n",dev.SEROCCOsetup.ral2);
printf("rah2    =0x%4.4x\n",dev.SEROCCOsetup.rah2);
printf("amral1  =0x%4.4x\n",dev.SEROCCOsetup.amral1);
printf("amrah1  =0x%4.4x\n",dev.SEROCCOsetup.amrah1);
printf("amral2  =0x%4.4x\n",dev.SEROCCOsetup.amral2);
printf("amrah2  =0x%4.4x\n",dev.SEROCCOsetup.amrah2);
printf("rlcrl   =0x%4.4x\n",dev.SEROCCOsetup.rlcrl);
printf("rlcrh   =0x%4.4x\n",dev.SEROCCOsetup.rlcrh);
printf("xon     =0x%4.4x\n",dev.SEROCCOsetup.xon);
printf("xoff    =0x%4.4x\n",dev.SEROCCOsetup.xoff);
printf("mxon    =0x%4.4x\n",dev.SEROCCOsetup.mxon);
printf("mxoff   =0x%4.4x\n",dev.SEROCCOsetup.mxoff);
printf("tcr     =0x%4.4x\n",dev.SEROCCOsetup.tcr);
printf("ticr    =0x%4.4x\n",dev.SEROCCOsetup.ticr);
printf("imr0    =0x%4.4x\n",dev.SEROCCOsetup.imr0);
printf("imr1    =0x%4.4x\n",dev.SEROCCOsetup.imr1);
printf("imr2    =0x%4.4x\n",dev.SEROCCOsetup.imr2);
printf("syncl   =0x%4.4x\n",dev.SEROCCOsetup.syncl);
printf("synch   =0x%4.4x\n",dev.SEROCCOsetup.synch);
printf("n_rbufs   =0x%4.4x\n",dev.SEROCCOsetup.n_rbufs);
printf("n_tbufs   =0x%4.4x\n",dev.SEROCCOsetup.n_tbufs);
printf("n_rfsize_max=0x%4.4x\n",dev.SEROCCOsetup.n_rfsize_max);
printf("n_tfsize_max=0x%4.4x\n",dev.SEROCCOsetup.n_tfsize_max);

fclose(fp);
}
