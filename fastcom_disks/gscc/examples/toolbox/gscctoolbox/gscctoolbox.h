
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the GSCCTOOLBOX_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// GSCCTOOLBOX_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef GSCCTOOLBOX_EXPORTS
#define GSCCTOOLBOX_API __declspec(dllexport)
#else
#define GSCCTOOLBOX_API __declspec(dllimport)
#endif

#include "..\..\gsccptest.h"

#define MAXPORTS 10

#ifdef __cplusplus
extern "C" 
{
#endif
#define INPUT
#define OUTPUT

GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_Create(INPUT DWORD port);
GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_Destroy(INPUT DWORD port);

GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_Block_Multiple_Io(INPUT DWORD port,INPUT DWORD onoff);

GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_Set_Clock(INPUT DWORD port,INPUT DWORD frequency);
GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_Get_Clock(INPUT DWORD port);

GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_Read_Frame(INPUT DWORD port,OUTPUT char * rbuf,INPUT DWORD szrbuf,OUTPUT DWORD *retbytes,INPUT DWORD timeout);
GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_Write_Frame(INPUT DWORD port,INPUT char * tbuf,INPUT DWORD numbytes,INPUT DWORD timeout);
GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_Get_Status(INPUT DWORD port,OUTPUT DWORD *status,INPUT DWORD mask,INPUT DWORD timeout);
GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_Get_IStatus(INPUT DWORD port,OUTPUT DWORD *status);

typedef void (__stdcall *readcallbackfn)(DWORD port,char *rdata,DWORD retbytes);
/* these are broken

GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_Register_Read_Callback(DWORD port,readcallbackfn fnptr,DWORD szread);
GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_Register_Write_Callback(DWORD port,void (__stdcall *callbackfunction)(DWORD port,char * tbuf,DWORD *bytestowrite),DWORD szmaxwrite);
GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_Register_Status_Callback(DWORD port,DWORD mask,void (__stdcall *callbackfunction)(DWORD port,DWORD status));
*/

GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_Flush_RX(INPUT DWORD port);
GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_Flush_TX(INPUT DWORD port);

GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_Read_Register(INPUT DWORD port,INPUT DWORD regno);
GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_Write_Register(INPUT DWORD port,INPUT DWORD regno,INPUT DWORD value);


GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_Configure(INPUT DWORD port,INPUT struct serocco_setup *settings);

GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_Set_Txclk_TT(INPUT DWORD port,INPUT DWORD onoff);
GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_Set_Txclk_ST(INPUT DWORD port,INPUT DWORD onoff);
GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_Set_SD_485(INPUT DWORD port,INPUT DWORD onoff);
GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_Set_TT_485(INPUT DWORD port,INPUT DWORD onoff);
GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_Set_RD_Echo_Cancel(INPUT DWORD port,INPUT DWORD onoff);
GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_Set_CTS_Disable(INPUT DWORD port,INPUT DWORD onoff);

GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_Bisync_Start_Pattern_Enable(INPUT DWORD port,INPUT DWORD onoff);
GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_Bisync_Cutoff_Enable(INPUT DWORD port,INPUT DWORD onoff);
GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_LSB2MSB_Convert_Enable(INPUT DWORD port,INPUT DWORD onoff);
GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_Bisync_End_Pattern_Enable(INPUT DWORD port,INPUT DWORD onoff);
GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_Bisync_Start_Pattern(INPUT DWORD port,INPUT struct bisync_start_pattern pattern);
GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_Bisync_Size_Cutoff(INPUT DWORD port,INPUT DWORD size);
GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_Bisync_End_Pattern(INPUT DWORD port,INPUT struct bisync_start_pattern pattern);

GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_Postamble_Enable(INPUT DWORD port,INPUT DWORD onoff);
GSCCTOOLBOX_API DWORD __stdcall GSCCToolBox_Postamble_Delay(INPUT DWORD port,INPUT DWORD delay);

#ifdef __cplusplus
}
#endif
