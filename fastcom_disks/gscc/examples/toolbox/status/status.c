/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
status.c -- user mode function to retrieve GSCC status (irq redirects) and send them to 
			the callback function using the gscctoolbox.dll

  The "callback" function means that this call will start up a thread issuing status calls
  in the DLL.  After the status has returned, the thread "calls back" to this program 
  to run the function gsccstatuscb at the end of each status.  You can modify the 
  gsccstatuscb function to do whatever you want with status data.

To use the gscctoolbox.DLL, you must do three things:

1. #include gscctoolbox.h in your source code
2. Add the file gscctoolbox.lib to your linker's Output/Library modules.  To do this in
   Visual Studio, Click on Settings in the Project menu.  In the Project settings dialog
   click on the Link tab.  In the Category drop down, choose General.  In the 
   Output/library modules box, add the path to gscctoolbox.lib.  It is easiest to copy the 
   file to your working directory and just add "gscctoolbox.lib"
3. The gscctoolbox.dll file must reside in the same directory as your compiled program .exe

usage:
 status port [mask]

 The port can be any valid gscc port (0,1,2...)
 optional mask, can exclude any status values by setting 0 at its bit position
 example:
	mask = 0xFFFFFFFF & (~ST_CTSC)
	this will make the CTS change in state status not be returned

 In order for an interrupt to show up in status, it must not be masked off in the GSCC's
 IMR registers.  Conversely, if you do not want an interrupt to show up, you can mask it
 off in the IMR register.

*/

#include "windows.h"
#include "..\gscctoolbox\gscctoolbox.h"
#include "stdio.h"
#include "stdlib.h"
#include "conio.h"

void __stdcall gsccstatuscb(DWORD port,DWORD status);

void main(int argc,char *argv[])
{
	int ret;
	ULONG port;
	ULONG mask;
	ULONG status;
	
	if( (argc<2) || (argc>3) )
	{
		printf("usage:%s port [mask]\r\n",argv[0]);
		exit(1);
	}
	port = atoi(argv[1]);
	mask = 0xFFFFFFFF;//default to all unmasked

	if(argc>2) sscanf(argv[2],"%x",&mask);
	if((ret = GSCCToolBox_Create(port))<0) printf("create:%d\n",ret);

	printf("press any key to exit\n");
	do
	{
		ret = GSCCToolBox_Get_Status(port,&status,mask,500);
		if((status&ST_RX_DONE)==ST_RX_DONE) printf("STATUS, RX_DONE\r\n");
		if((status&ST_OVF)==ST_OVF) printf("STATUS, BUFFERS overflowed\n\r");
		if((status&ST_RFS)==ST_RFS) printf("STATUS, Receive Frame Start\r\n");
		if((status&ST_RX_TIMEOUT)==ST_RX_TIMEOUT) printf("STATUS, Receive Timeout\r\n");
		if((status&ST_RSC)==ST_RSC) printf("STATUS, Receive Status Change\r\n");
		if((status&ST_PERR)==ST_PERR) printf("STATUS, Parity Error\r\n");
		if((status&ST_PCE)==ST_PCE) printf("STATUS, Protocol Error\r\n");
		if((status&ST_FERR)==ST_FERR) printf("STATUS, Framing Error\r\n");
		if((status&ST_SYN)==ST_SYN) printf("STATUS, SYN detected\r\n");
		if((status&ST_DPLLA)==ST_DPLLA) printf("STATUS, DPLL Asynchronous\r\n");
		if((status&ST_CDSC)==ST_CDSC) printf("STATUS, Carrier Detect Change State\r\n");
		if((status&ST_RFO)==ST_RFO) printf("STATUS, Receive Frame Overflow(HARDWARE)\r\n");
		if((status&ST_EOP)==ST_EOP) printf("STATUS, End of Poll\r\n");
		if((status&ST_BRKD)==ST_BRKD) printf("STATUS, Break Detected\r\n");
		if((status&ST_ONLP)==ST_ONLP) printf("STATUS, On Loop\r\n");
		if((status&ST_BRKT)==ST_BRKT) printf("STATUS, Break Terminated\r\n");
		if((status&ST_ALLS)==ST_ALLS) printf("STATUS, All Sent\r\n");
		if((status&ST_EXE)==ST_EXE) printf("STATUS, Transmit Underrun\r\n");
		if((status&ST_TIN)==ST_TIN) printf("STATUS, Timer Expired\r\n");
		if((status&ST_CTSC)==ST_CTSC) printf("STATUS, CTS Changed State\r\n");
		if((status&ST_XMR)==ST_XMR) printf("STATUS, Transmit Message Repeat\r\n");
		if((status&ST_TX_DONE)==ST_TX_DONE) printf("STATUS, TX Done\r\n");
		if((status&ST_DMA_TC)==ST_DMA_TC) printf("STATUS, DMA TC reached\r\n");
		if((status&ST_DSR1C)==ST_DSR1C) printf("STATUS, Channel 1 DSR Changed\r\n");
		if((status&ST_DSR0C)==ST_DSR0C) printf("STATUS, Channel 0 DSR Changed\r\n");
	}while(!_kbhit());

	_getch();
	GSCCToolBox_Destroy(port);
}

/*
void __stdcall gsccstatuscb(DWORD port,DWORD status)
{
	if((status&ST_RX_DONE)==ST_RX_DONE) printf("STATUS, RX_DONE\r\n");
	if((status&ST_OVF)==ST_OVF) printf("STATUS, BUFFERS overflowed\n\r");
	if((status&ST_RFS)==ST_RFS) printf("STATUS, Receive Frame Start\r\n");
	if((status&ST_RX_TIMEOUT)==ST_RX_TIMEOUT) printf("STATUS, Receive Timeout\r\n");
	if((status&ST_RSC)==ST_RSC) printf("STATUS, Receive Status Change\r\n");
	if((status&ST_PERR)==ST_PERR) printf("STATUS, Parity Error\r\n");
	if((status&ST_PCE)==ST_PCE) printf("STATUS, Protocol Error\r\n");
	if((status&ST_FERR)==ST_FERR) printf("STATUS, Framing Error\r\n");
	if((status&ST_SYN)==ST_SYN) printf("STATUS, SYN detected\r\n");
	if((status&ST_DPLLA)==ST_DPLLA) printf("STATUS, DPLL Asynchronous\r\n");
	if((status&ST_CDSC)==ST_CDSC) printf("STATUS, Carrier Detect Change State\r\n");
	if((status&ST_RFO)==ST_RFO) printf("STATUS, Receive Frame Overflow(HARDWARE)\r\n");
	if((status&ST_EOP)==ST_EOP) printf("STATUS, End of Poll\r\n");
	if((status&ST_BRKD)==ST_BRKD) printf("STATUS, Break Detected\r\n");
	if((status&ST_ONLP)==ST_ONLP) printf("STATUS, On Loop\r\n");
	if((status&ST_BRKT)==ST_BRKT) printf("STATUS, Break Terminated\r\n");
	if((status&ST_ALLS)==ST_ALLS) printf("STATUS, All Sent\r\n");
	if((status&ST_EXE)==ST_EXE) printf("STATUS, Transmit Underrun\r\n");
	if((status&ST_TIN)==ST_TIN) printf("STATUS, Timer Expired\r\n");
	if((status&ST_CTSC)==ST_CTSC) printf("STATUS, CTS Changed State\r\n");
	if((status&ST_XMR)==ST_XMR) printf("STATUS, Transmit Message Repeat\r\n");
	if((status&ST_TX_DONE)==ST_TX_DONE) printf("STATUS, TX Done\r\n");
	if((status&ST_DMA_TC)==ST_DMA_TC) printf("STATUS, DMA TC reached\r\n");
	if((status&ST_DSR1C)==ST_DSR1C) printf("STATUS, Channel 1 DSR Changed\r\n");
	if((status&ST_DSR0C)==ST_DSR0C) printf("STATUS, Channel 0 DSR Changed\r\n");
	
}
*/
/* $Id$ */