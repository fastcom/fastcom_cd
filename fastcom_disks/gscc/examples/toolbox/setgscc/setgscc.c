/* $Id$ */
/*
Copyright(c) 2002,2006 Commtech, Inc.
setgscc.c -- user mode function to setup the GSCC registers using the gscctoolbox.dll

To use the gscctoolbox.DLL, you must do three things:

1. #include gscctoolbox.h in your source code
2. Add the file gscctoolbox.lib to your linker's Output/Library modules.  To do this in
   Visual Studio, Click on Settings in the Project menu.  In the Project settings dialog
   click on the Link tab.  In the Category drop down, choose General.  In the 
   Output/library modules box, add the path to gscctoolbox.lib.  It is easiest to copy the 
   file to your working directory and just add "gscctoolbox.lib"
3. The gscctoolbox.dll file must reside in the same directory as your compiled program .exe

usage:
 setgscc port setfile

 The port can be any valid gscc port (0,1,2...).

 The setfile is a text file that contains the settings to use.  See the default 
 settings files 'hdlcset', 'asyncset' & 'bisyncset' for your chosen mode).

 This could be modified so that the settings values were set directly by your program, 
 instead of opening an external file.

*/


#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include "..\gscctoolbox\gscctoolbox.h" /* user code header */


int main(int argc, char *argv[])
{
	int port;
	int i;
	FILE *fp;
	char inputline[100],*ptr;
	unsigned long value;
	DWORD ret;
	struct serocco_setup settings;
	
	
	if(argc!=3)
	{
		printf("%s port settingsfile\n\n",argv[0]);
		printf(" The port is appended onto \\\\.\\GSCC\n");
		exit(1);
	}
	port=atoi(argv[1]);
	if((ret = GSCCToolBox_Create(port))<0)
	{
		printf("create:%d\n",ret);
		exit(1);
	}
	
	fp = fopen(argv[2],"r");
	if(fp==NULL)
	{
		printf("Cannot open file %s\n",argv[2]);
		exit(1);
	}
	
	memset(&settings,0,sizeof(struct serocco_setup));
	memset(inputline,0,sizeof(inputline));
	
	while(fgets(inputline,100,fp)!=NULL)
	{
		/* zero the comments so not to get confused with key words */
		/* # pound is the comment prefix */
		for(i=0;inputline[i]!='\n';i++)
		{
			if(inputline[i]=='#')
			{
				memset(&inputline[i],0, 100-i );	
				break;
			}
			
		}
		i = 0;
		while(inputline[i]!=0) 
		{
			inputline[i] = tolower(inputline[i]);
			i++;
		}
		
		if( (ptr = strchr(inputline,'='))!=NULL)
		{
			for(i=1;ptr[i]==' ' && ptr[i]!='\n';i++);
			value = (unsigned long)strtoul(ptr+i,NULL,16);
			//printf("value = 0x%lx\n",(long)value);
		}
		else	value=0;
		
		if(strncmp(inputline,"gcmdr",5)==0) settings.gcmdr = value;
		if(strncmp(inputline,"gmode",5)==0) settings.gmode = value;
		if(strncmp(inputline,"gpdirl",6)==0) settings.gpdirl = value;
		if(strncmp(inputline,"gpdirh",6)==0) settings.gpdirh = value;
		if(strncmp(inputline,"gpdatl",6)==0) settings.gpdatl = value;
		if(strncmp(inputline,"gpdath",6)==0) settings.gpdath = value;
		if(strncmp(inputline,"gpiml",5)==0) settings.gpiml = value;
		if(strncmp(inputline,"gpimh",5)==0) settings.gpimh = value;
		if(strncmp(inputline,"dcmdr",5)==0) settings.dcmdr = value;
		if(strncmp(inputline,"dimr",4)==0) settings.dimr = value;
		if(strncmp(inputline,"cmdrl",5)==0) settings.cmdrl = value;
		if(strncmp(inputline,"cmdrh",5)==0) settings.cmdrh = value;
		if(strncmp(inputline,"ccr0l",5)==0) settings.ccr0l = value;
		if(strncmp(inputline,"ccr0h",5)==0) settings.ccr0h = value;
		if(strncmp(inputline,"ccr1l",5)==0) settings.ccr1l = value;
		if(strncmp(inputline,"ccr1h",5)==0) settings.ccr1h = value;
		if(strncmp(inputline,"ccr2l",5)==0) settings.ccr2l = value;
		if(strncmp(inputline,"ccr2h",5)==0) settings.ccr2h = value;
		if(strncmp(inputline,"ccr3l",5)==0) settings.ccr3l = value;
		if(strncmp(inputline,"ccr3h",5)==0) settings.ccr3h = value;
		if(strncmp(inputline,"preamb",6)==0) settings.preamb = value;
		if(strncmp(inputline,"tolen",5)==0) settings.tolen = value;
		if(strncmp(inputline,"accm0",5)==0) settings.accm0 = value;
		if(strncmp(inputline,"accm1",5)==0) settings.accm1 = value;
		if(strncmp(inputline,"accm2",5)==0) settings.accm2 = value;
		if(strncmp(inputline,"accm3",5)==0) settings.accm3 = value;
		if(strncmp(inputline,"udac0",5)==0) settings.udac0 = value;
		if(strncmp(inputline,"udac1",5)==0) settings.udac1 = value;
		if(strncmp(inputline,"udac2",5)==0) settings.udac2 = value;
		if(strncmp(inputline,"udac3",5)==0) settings.udac3 = value;
		if(strncmp(inputline,"ttsa0",5)==0) settings.ttsa0 = value;
		if(strncmp(inputline,"ttsa1",5)==0) settings.ttsa1 = value;
		if(strncmp(inputline,"ttsa2",5)==0) settings.ttsa2 = value;
		if(strncmp(inputline,"ttsa3",5)==0) settings.ttsa3 = value;
		if(strncmp(inputline,"rtsa0",5)==0) settings.rtsa0 = value;
		if(strncmp(inputline,"rtsa1",5)==0) settings.rtsa1 = value;
		if(strncmp(inputline,"rtsa2",5)==0) settings.rtsa2 = value;
		if(strncmp(inputline,"rtsa3",5)==0) settings.rtsa3 = value;
		if(strncmp(inputline,"pcmtx0",6)==0) settings.pcmtx0 = value;
		if(strncmp(inputline,"pcmtx1",6)==0) settings.pcmtx1 = value;
		if(strncmp(inputline,"pcmtx2",6)==0) settings.pcmtx2 = value;
		if(strncmp(inputline,"pcmtx3",6)==0) settings.pcmtx3 = value;
		if(strncmp(inputline,"pcmrx0",6)==0) settings.pcmrx0 = value;
		if(strncmp(inputline,"pcmrx1",6)==0) settings.pcmrx1 = value;
		if(strncmp(inputline,"pcmrx2",6)==0) settings.pcmrx2 = value;
		if(strncmp(inputline,"pcmrx3",6)==0) settings.pcmrx3 = value;
		if(strncmp(inputline,"brrl",4)==0) settings.brrl = value;
		if(strncmp(inputline,"brrh",4)==0) settings.brrh = value;
		if(strncmp(inputline,"timer0",6)==0) settings.timer0 = value;
		if(strncmp(inputline,"timer1",6)==0) settings.timer1 = value;
		if(strncmp(inputline,"timer2",6)==0) settings.timer2 = value;
		if(strncmp(inputline,"timer3",6)==0) settings.timer3 = value;
		if(strncmp(inputline,"xad1",4)==0) settings.xad1 = value;
		if(strncmp(inputline,"xad2",4)==0) settings.xad2 = value;
		if(strncmp(inputline,"rah1",4)==0) settings.rah1 = value;
		if(strncmp(inputline,"rah2",4)==0) settings.rah2 = value;
		if(strncmp(inputline,"ral1",4)==0) settings.ral1 = value;
		if(strncmp(inputline,"ral2",4)==0) settings.ral2 = value;
		if(strncmp(inputline,"amral1",6)==0) settings.amral1 = value;
		if(strncmp(inputline,"amrah1",6)==0) settings.amrah1 = value;
		if(strncmp(inputline,"amral2",6)==0) settings.amral2 = value;
		if(strncmp(inputline,"amrah2",6)==0) settings.amrah2 = value;
		if(strncmp(inputline,"rlcrl",5)==0) settings.rlcrl = value;
		if(strncmp(inputline,"rlcrh",5)==0) settings.rlcrh = value;
		if(strncmp(inputline,"xon",3)==0) settings.xon = value;
		if(strncmp(inputline,"xoff",4)==0) settings.xoff = value;
		if(strncmp(inputline,"mxon",4)==0) settings.mxon = value;
		if(strncmp(inputline,"mxoff",5)==0) settings.mxoff = value;
		if(strncmp(inputline,"tcr",3)==0) settings.tcr = value;
		if(strncmp(inputline,"imr0",4)==0) settings.imr0 = value;
		if(strncmp(inputline,"imr1",4)==0) settings.imr1 = value;
		if(strncmp(inputline,"imr2",4)==0) settings.imr2 = value;
		if(strncmp(inputline,"syncl",5)==0) settings.syncl = value;
		if(strncmp(inputline,"synch",5)==0) settings.synch = value;
		if(strncmp(inputline,"n_rbufs",7)==0) settings.n_rbufs = value;
		if(strncmp(inputline,"n_tbufs",7)==0) settings.n_tbufs = value;
		if(strncmp(inputline,"n_rfsize_max",12)==0) settings.n_rfsize_max = value;
		if(strncmp(inputline,"n_tfsize_max",12)==0) settings.n_tfsize_max = value;
		memset(inputline,0,100);
	}

	if((ret=GSCCToolBox_Configure(port,&settings))<0)
	{
		printf("configure:%d\n",ret);
	}
	else	printf("Finished Settings.\n");	
	
	GSCCToolBox_Destroy(port);
	return 0;
}
/* $Id$ */