/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
ctsdisable.c -- user mode function enable/disable CTSDisable using the gscctoolbox.dll

Description	    : Toggles whether CTS is forced active or taken from connector


To use the gscctoolbox.DLL, you must do three things:

1. #include gscctoolbox.h in your source code
2. Add the file gscctoolbox.lib to your linker's Output/Library modules.  To do this in
Visual Studio, Click on Settings in the Project menu.  In the Project settings dialog
click on the Link tab.  In the Category drop down, choose General.  In the 
Output/library modules box, add the path to gscctoolbox.lib.  It is easiest to copy the 
file to your working directory and just add "gscctoolbox.lib"
3. The gscctoolbox.dll file must reside in the same directory as your compiled program .exe

usage:
ctsdisable port 0|1

 The port can be any valid gscc port (0,1,2...)
			
*/

#include "windows.h"
#include "..\gscctoolbox\gscctoolbox.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"


int main(int argc, char *argv[])
{
	unsigned long port;
	DWORD ret;
	DWORD onoff;
	
	if(argc<3)
    {
		printf("usage:%s port [0|1]\r\n",argv[0]);
		exit(1);
    }
	port = atoi(argv[1]);
	onoff = atoi(argv[2]);
	
	//open handle to port
	if((ret = GSCCToolBox_Create(port))<0)
    {
		printf("create:%d\n",ret);
		exit(1);
    }
	
	if((ret =GSCCToolBox_Set_CTS_Disable(port,onoff))<0)
    {
		printf("error in Set_CTS_Disable:%d\n",ret);
    }
	else 
    {
		if(onoff==0) printf("CTS signal from connector\r\n");//(CTS is taken from connector)
		else printf("CTS forced active (ignore connector)\r\n");//(force CTS active)
    }
	
	GSCCToolBox_Destroy(port);
}
/* $Id$ */