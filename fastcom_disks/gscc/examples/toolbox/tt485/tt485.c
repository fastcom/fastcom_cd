/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
tt485.c - user mode function to enable/disable RS485 on the TT signal using the gscctoolbox.dll
		  when enabled, the TT driver will be turned off when not transmitting so that
		  it does not drive the line

To use the gscctoolbox.DLL, you must do three things:

1. #include gscctoolbox.h in your source code
2. Add the file gscctoolbox.lib to your linker's Output/Library modules.  To do this in
   Visual Studio, Click on Settings in the Project menu.  In the Project settings dialog
   click on the Link tab.  In the Category drop down, choose General.  In the 
   Output/library modules box, add the path to gscctoolbox.lib.  It is easiest to copy the 
   file to your working directory and just add "gscctoolbox.lib"
3. The gscctoolbox.dll file must reside in the same directory as your compiled program .exe

usage:
 tt485 port 0|1

 The port can be any valid gscc port (0,1,2...) 
 
*/

#include "windows.h"
#include "..\gscctoolbox\gscctoolbox.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"


int main(int argc, char *argv[])
{
	unsigned long port;
	DWORD ret;
	DWORD onoff;
	
	if(argc<3)
	{
		printf("usage:%s port [0|1]\r\n",argv[0]);
		exit(1);
	}
	port = atoi(argv[1]);
	onoff = atoi(argv[2]);
	if((ret = GSCCToolBox_Create(port))<0)
	{
		printf("create:%d\n",ret);
		exit(1);
	}
	if((ret =GSCCToolBox_Set_TT_485(port,onoff))<0)
	{
		printf("error in Set_TT_485:%d\n",ret);
	}
	else 
	{
		if(onoff==0) printf("TT set to 422 mode\r\n");
		else printf("TT set to 485 mode\r\n");
	}
	
	GSCCToolBox_Destroy(port);
}
/* $Id$ */