/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
writefile.c -- user mode function to write a file to a gscc port using the gscctoolbox.dll
To use the gscctoolbox.DLL, you must do three things:

1. #include gscctoolbox.h in your source code
2. Add the file gscctoolbox.lib to your linker's Output/Library modules.  To do this in
   Visual Studio, Click on Settings in the Project menu.  In the Project settings dialog
   click on the Link tab.  In the Category drop down, choose General.  In the 
   Output/library modules box, add the path to gscctoolbox.lib.  It is easiest to copy the 
   file to your working directory and just add "gscctoolbox.lib"
3. The gscctoolbox.dll file must reside in the same directory as your compiled program .exe

usage:
 writefile port block file [delay] [repeat]

 The port can be any valid gscc port (0,1) 
 block is the block size to use to send (1-4095)
 file is the file to send
 delay is the time to add between block writes (in miliseconds)
 if repeat is present then the file will be rewound and resent repeat times

  
Note: The timeout for the write is set at 1 second, if the time that it takes to send
      one frame takes longer than 1 second (ie slow bitrate, or long framesize, or combination therof)
	  then this code will break, if you need to do such a thing, increase the Write_Frame timeout value (last parameter)
	  and recompile.
*/
#include "windows.h"
#include "..\gscctoolbox\gscctoolbox.h"
#include "stdio.h"
#include "stdlib.h"


void main(int argc,char *argv[])
{
	FILE *fin;
	ULONG tsize;
	ULONG delay;
	ULONG repeat;
	ULONG finished;
	ULONG status;
	ULONG bytestowrite;
	char tbuf[4096];
	int ret;
	ULONG port;
	ULONG timeout=0;
	tsize=32;
	delay=0;
	repeat=0;
	finished=0;
	fin=NULL;
	
	if(argc<4)
	{
		printf("usage:%s port block file [delay] [repeat]\r\n",argv[0]);
		exit(1);
	}
	
	port = atoi(argv[1]);
	tsize = atol(argv[2]);
	if(argc>4) delay = atol(argv[4]);
	if(argc>5) repeat =atol(argv[5]);
	printf("FILE:%s\r\n",argv[3]);
	printf("delay:%d\r\n",delay);
	if(repeat!=0) printf("repeat is ON,count%d\r\n",repeat);
	if((tsize<1)||(tsize>4095))
	{
		printf("block size out of range: 1<%d<4095\r\n",tsize);
		exit(1);
	}
	fin = fopen(argv[3],"rb");
	if(fin==NULL)
	{
		printf("cannot open %s\r\n",argv[3]);
		exit(1);
	}
	
	
	if((ret = GSCCToolBox_Create(port))<0) 
	{
		printf("create:%d\n",ret);
		exit(0);
	}
	GSCCToolBox_Flush_TX(port);
	GSCCToolBox_Get_IStatus(port,&status);//flush status
	do
	{
		if(delay!=0) Sleep(delay);
		bytestowrite = fread(tbuf,1,tsize,fin);
		if(bytestowrite!=0)
		{
			if((ret=GSCCToolBox_Write_Frame(port,tbuf,bytestowrite,1000))<0) printf("ERROR,write returned :%d\r\n",ret);
		}
		else
		{
			if(feof(fin))
			{
				if(repeat!=0) 
				{
					repeat--;
					rewind(fin);
				}
				else
				{
					finished=1;
				}
			}
			else printf("error 0 bytes read from file, NOT at EOF!!\r\n");
		}
	}while(finished==0);
	printf("finished write--waiting for ALLS\r\n");
	status = 0;
	timeout=0;
	while ((status & ST_ALLS) == 0)
	{
		ret = GSCCToolBox_Get_Status(port,&status,0xFFFFFFFF,500);
		timeout++;
		if(timeout>60)
		{
			printf("no ALLS after 30 seconds...aborting\r\n");
			GSCCToolBox_Destroy(port);
			fclose(fin);
			exit(1);
		}
	}
	
	printf("writing...done\r\n");
	
	GSCCToolBox_Destroy(port);
	fclose(fin);
}


/* $Id$ */
