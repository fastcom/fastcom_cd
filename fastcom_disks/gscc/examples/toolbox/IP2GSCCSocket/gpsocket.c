/*

	gpsocket.c  -- A windows sockets example for the Fastcom:GSCC-PCI-335 card

				-- This program when called as a server will relay data packets between
				-- a socket connection and an GSCC card.  Data comming in a
				-- socket is sent out the GSCC port, data comming in the 
				-- GSCC port is sent out socket.  This continues until the 
				-- client closes the socket.

				-- As a client this program takes keystrokes (up to a [CR])
				-- packages them up in a "frame" and sends them to the socket
				-- (it is then assumed that this server sends that data out the GSCC)
				-- It also receives any incomming data from the socket and displays
				-- it on the screen.

  Usage:
  gpsocket GSCCport IPaddress [client]
  
  To call as a client execute the program as:

  gpsocket 0 127.0.0.1 c

  To call as a server execute the program as:

  gpsocket 0 127.0.0.1 
*/


#include "winsock2.h"			//required header for using windows sockets
#include "stdio.h"				//printf etc
#include "stdlib.h"
#include "conio.h"				//kbhit
#include "string.h"				//strlen
#include "..\gscctoolbox\gscctoolbox.h"		//gets us the simple gscc interaction

//port that we will listen on
#define EP_PORT 5003		
//number of clients to queue	
#define MAX_PENDING_CONNECTS 4
//frequency to set the gscc clock generator to (6-33M)
#define OSC_FREQ 6000000

void setupgscc();							//function to initialize the gscc port, taken straight from setgscc.c reads in "hdlcset" file and initializes the port to those values
DWORD FAR PASCAL ReadProc( LPVOID lpData );	//thread that server uses to receive data from gscc port and write to socket connection
DWORD FAR PASCAL WriteProc( LPVOID lpData );//thread that server uses to read from the socket and write to the gscc port

BOOL connected;								//global variable that holds a "we are connected" state
SOCKET a_theSocket;							//global variable that holds the connected socket (used in readproc and writeproc)
int gscc_port;								//global variable that holds gscc port number (ie holds X where \\.\gsccX)

int main(int argc, char *argv[])
{
	SOCKET m_theSocket;							//socket to use for client, and for listening on for the server
	char buf[4096];								//data buffer for client, holds keypress data used to send to socket
	char *cb;									//character pointer used for getting internet addresses as strings from winsock, ie holds return value from inet_ntoa()
	int key;									//holds current user keypress
	int i;										//temp variable (loops)
	int j;										//temp variable (loops)
	int k;										//temp variable (loops)
	char rbuf[4096];							//data buffer for client, used to obtain data from socket in client mode
	WORD wVersionRequested;						//required winsock stuff
	WSADATA wsaData;							//required winsock stuff
	int err;									//required winsock stuff
	struct sockaddr_in   tcpaddr;				//address structure for passing to socket functions, used on client side for where to connect, and on server side for where to listen/bind
	struct sockaddr_in   saddr;					//holds ip address that server is listening on 
	struct sockaddr_in   inaddr;				//holds ip address of connected client after accept completes
	int saddr_len;								//holds sizeof saddr
	int inaddr_len;								//holds sizeof inaddr
	HANDLE            hreadThread ;				//handle to read thread
	DWORD            readID;					//read thread ID storage
	DWORD         dwThreadID ;					//temp Thread ID storage
	HANDLE            hwriteThread ;			//handle to write thread
	DWORD            writeID;					//write thread ID storage
	
	
	//if user doesn't give us enough parameters, let them know the usage
	if(argc<2)
	{
		printf("usage:%s gsccport ipaddress [client]\n",argv[0]);
		exit(1);
	}
	//startup winsock
	wVersionRequested = MAKEWORD( 2, 2 );
	
	err = WSAStartup( wVersionRequested, &wsaData );
	if ( err != 0 ) 
	{
		printf("cannot start winsock\r\n");
		exit(1);
	}
	//create the initial socket (THE socket for a client, the socket to listen on for the server)
	m_theSocket = socket( AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (m_theSocket == INVALID_SOCKET)
	{
		printf("cannot get a socket\n");
		exit(1);
	}
	//determine the gscc port number to use and store it.
	gscc_port = atoi(argv[1]);
	GSCCToolBox_Create(gscc_port);	//use the toolbox to do a CreateFile on the gscc device 
	
	//determine if we are to be a client or a server
	if(argc>3)
	{
		//we are a client, and we will 
		//try to connect up to a server.
		
		tcpaddr.sin_addr.S_un.S_addr = inet_addr(argv[2]);//get ip address from command line
		tcpaddr.sin_family = AF_INET;
		tcpaddr.sin_port = htons(EP_PORT);
		//this next call tries to hook this socket up to the server
		if(connect(m_theSocket,(SOCKADDR*)&tcpaddr,sizeof(tcpaddr))==SOCKET_ERROR)
		{
			printf("error in connect\n");
			exit(1);
		}
		//here we have a connected socket, we can send/receive to/from it now
		//we will do two things as a client, first we will take user keyboard input
		//storing the keystrokes in buf[] until the user hits [Enter].  Once
		//the user hits [Enter] the data in buf[] is sent on to the socket the buffer 
		//is cleared and we start over.
		//Second, if any data comes in from the socket, then we will receive and display it.
		//we continue on until the user presses [ESC] which will terminate the client program.
		
		//note the GSCC port isn't really used here, so you could move all of the initialization of the GSCC to 
		//the server side and remove the GSCCToolBox_Destroy() call at the end of this routine
		
		i=0;		//i is the index into buf[] for the current keystroke
		key = 0;	//has to not be [ESC]
		printf("enter text\n");
		while((key&0xff)!=27)	//loop until user presses [ESC]
		{
			if(kbhit()!=0)		//if a user presses a key
			{
				key = getch();	//get the keystroke
				buf[i]=key;		//store it in our buffer
				i++;			//inc the index
				printf("%c",key);//echo the character to the screen
				
				if(key==0x0d)	//if the key is [Enter] then
				{
					if(send(m_theSocket,buf,i-1,0)==SOCKET_ERROR) //send the data to the socket don't send the [CR]
					{
						printf("error in send\n");
						exit(1);
					}
					i=0;		//reset the index
					printf("\r\nenter text\r\n");//re-display the info mesage
				}
			}
			ioctlsocket(m_theSocket,FIONREAD,&j);//determine if there is data waiting in the socket to be received
			if(j>0)								//there is data to get
			{
				j = recv(m_theSocket,rbuf,sizeof(rbuf),0);//get the data
				printf("received:\r\n");				  //display message
				for(k=0;k<j;k++) printf("%c",rbuf[k]);	  //display data
				printf("\r\n");								
			}
		}//end of while![ESC]
		//we are done here so
		shutdown(m_theSocket,SD_BOTH);	//gracefull shutdown of socket
		closesocket(m_theSocket);		//release socket resources
		WSACleanup();					//done with winsock
		GSCCToolBox_Destroy(gscc_port);	//done with GSCC port
		exit(0);						//done with program
	}
	else
	{
		//we are a server, so setup to receive incomming socket connections
		if((i =GSCCToolBox_Set_Clock(gscc_port,OSC_FREQ))<0)	//set the onboard clock on the GSCC card to a value (#defined above)
		{
			printf("error in setclock:%d\n",i);
		}
		else printf("Clock set to %d\r\n",OSC_FREQ);
		setupgscc();											//set the registers/buffering of the GSCC card
		
		//set our ip info for the server, port and IP mostly
		tcpaddr.sin_family = AF_INET;
		tcpaddr.sin_port = htons(EP_PORT);
		tcpaddr.sin_addr.S_un.S_addr = inet_addr(argv[2]);//htonl(INADDR_LOOPBACK);
		//bind to this IP:port
		if (bind(m_theSocket,(SOCKADDR*)&tcpaddr, sizeof(tcpaddr)) == SOCKET_ERROR)
		{
			printf("unable to bind %8.8x\n",WSAGetLastError());
			exit(1);
		}
		else
		{
			//we are bound, now set the state of the socket to listen for incomming connections
			if (listen(m_theSocket, MAX_PENDING_CONNECTS ) == SOCKET_ERROR)
			{
				printf("error while listen\n");
				exit(1);
			}
			//we are entering the main loop of the server
s_top: 
			//this gets our IP address that the server is listening on (yes we allready know it from argv[2])
			saddr_len = sizeof(saddr);
			getsockname(m_theSocket,(SOCKADDR*)&saddr,&saddr_len);
			cb = inet_ntoa(saddr.sin_addr);
			printf("server, listening on : %s\r\n",cb);
			//at this point we are listening...what happens when someone connects?
			inaddr_len = sizeof(inaddr);//set size of inaddr for accept call
			//now we block until someone connects, we do this by calling accept()
			a_theSocket = accept(m_theSocket,(SOCKADDR*)&inaddr,&inaddr_len);
			if(a_theSocket==INVALID_SOCKET)
			{
				printf("error accepting\n");
				exit(1);
			}
			else
			{
				//accept returned, so we have a client connected
				//lets find their ip address
				cb = inet_ntoa(inaddr.sin_addr);
				printf("accepted: %s\r\n",cb);
				//at this point we have a client connected, so fire up the receive/transmit threads
				connected = TRUE;	//enable the read/write threads
				//create a thread for reading from the GSCC (writing to the socket)
				hreadThread =     CreateThread( (LPSECURITY_ATTRIBUTES) NULL,
					0, 
					(LPTHREAD_START_ROUTINE) ReadProc,
					(LPVOID) NULL,
					0, &dwThreadID );
				if(hreadThread==NULL)
				{
					//we were unable to make the thread, so 
					//cleanup the mess we have made
					printf("cannot start Data read thread\n\r");
					closesocket(m_theSocket);
					WSACleanup();
					GSCCToolBox_Destroy(gscc_port);
					exit(1);
				}
				readID=dwThreadID;//could be used to control the thread from here (currently unused)
				//create a thread for reading from the socket (writing to the GSCC)
				hwriteThread =     CreateThread( (LPSECURITY_ATTRIBUTES) NULL,
					0, 
					(LPTHREAD_START_ROUTINE) WriteProc,
					(LPVOID) NULL,
					0, &dwThreadID );
				
				if(hwriteThread==NULL)
				{
					//unable to create the write thread
					//cleanup the mess we have made
					printf("cannot start Data write thread\n\r");
					closesocket(m_theSocket);
					WSACleanup();
					GSCCToolBox_Destroy(gscc_port);
					exit(1);
				}
				writeID=dwThreadID;//could be used to control the thread from here (currently unused)
				
				//this part of the code is done, wait for the client to drop (will be detected in the WriteProc())
				//just wait for the connected to be reset
				while(connected==TRUE) Sleep(10);//could make this a bit longer...just increases the latency on re-listening for new clients
				Sleep(1000);//give the ReadProc() time to exit
				if(!kbhit()) goto s_top; //if the user didn't hit a key, then go back to the "waiting for a connection" state
				//the user hit a key...likely that they want to exit (note you have to hit the key in the server "BEFORE" exiting the client or
				//you end up back in the "waiting for client" state.
				
				
				
				shutdown(a_theSocket,SD_BOTH);//done with this clients socket, so close it up
				closesocket(a_theSocket);//release the socket resources
			}//end of accept case
		}//end of bound case
 }//end of server case
 closesocket(m_theSocket);//done with initial socket
 WSACleanup();//done with winsock
 GSCCToolBox_Destroy(gscc_port);//done with the GSCC port
}//end of main()


//this is the receive thread function.
DWORD FAR PASCAL ReadProc( LPHANDLE lpData )
{
	char buf[4096];//buffer for incomming/outgoing data
	DWORD rbytes;//number of bytes received
	int i;//return status from GSCCToolBox_Read_Frame()
	
	printf("readthread %d\n",gscc_port);//initial message "we are here"
	while(connected==TRUE)//loop as long as connected 
	{
		//get a frame from the GSCC, this will timeout every second (return -4) to 
		//let us know that it is doing something
		if((i=GSCCToolBox_Read_Frame(gscc_port,&buf[0],sizeof(buf),&rbytes,1000))==0)
		{
			//zero return indicates we have data,
			printf("gscc received %d\n",rbytes);//how much data
			if(send(a_theSocket,buf,rbytes,0)==SOCKET_ERROR)//take that data and shove it at the socket
			{
				//pretty much if the send to the socket failed, we are done, 
				//either the socket is closed, or we have some other error 
				printf("error in socket send :%8.8x\n",WSAGetLastError());
				printf("read thread terminating\n");
				return(FALSE);
			}
		}
		printf("read loop :%d\n",i);//this is the 1 per second "we are alive message"
	}//end of while connected
	printf("read thread terminating\n");//we are done
	return(TRUE);//gone
}//end of ReadProc()

//This is the write thread
DWORD FAR PASCAL WriteProc( LPHANDLE lpData )
{
	int i;//return value from recv()
	char buf[4096];//buffer for data transfer
	
	printf("write proc\n");//"we are here" message
	while(connected)//do as long as connected ==TRUE
	{
		i = recv(a_theSocket,buf,sizeof(buf),0);//blocks until there is data from the socket
		if(i>0) 
		{
			//nonzero positive return means data came in from the socket
			printf("socket received %d \n",i);
			//send it out the GSCC port
			//really should check to make sure that i <= rfsizemax (from hdlcset file)
			//or the write will fail with a too big message(invalid parameter?)
			//should also probably check the return value for other errors and not
			//do the infinte while loop it isn't a timeout
			while(GSCCToolBox_Write_Frame(gscc_port,buf,i,1000)!=0);
		}
		if(i<=0) 
		{
			//the socket returned without a positive value, so it is either closed
			//or some error occured, either way we are done
			connected=FALSE;//indicate that the threads are going away
			printf("connection closed, write thread terminating\n");//leaving message
			return(FALSE);//gone
		}
	}//end of while connected
	return(TRUE);//gone (I don't think we get here...unless someone outside of this routine makes connected==FALSE)                  
}//end of WriteProc()

//this function initializes the GSCC registers
//it is taken straight from the setgscc.c GsccToolBox example
//with minimal changes to make it a function.
//ie it assumes that the settings file name is "hdlcset"
void setupgscc()
{
	int i;
	FILE *fp;
	char inputline[100],*ptr;
	unsigned long value;
	DWORD ret;
	struct serocco_setup settings;
	//open settings file
	fp = fopen("hdlcset","r");
	if(fp==NULL)
	{
		printf("Cannot open file %s\n","hdlcset");
		exit(1);
	}
	//clear setup structure and input array	
	memset(&settings,0,sizeof(struct serocco_setup));
	memset(inputline,0,sizeof(inputline));
	//read in the file a line at a time (assumes line length <100 bytes)	
	while(fgets(inputline,100,fp)!=NULL)
	{
		/* zero the comments so not to get confused with key words */
		/* # pound is the comment prefix */
		for(i=0;inputline[i]!='\n';i++)
		{
			if(inputline[i]=='#')
			{
				memset(&inputline[i],0, 100-i );	
				break;
			}
			
		}
		i = 0;
		//make case insensitive
		while(inputline[i]!=0) 
		{
			inputline[i] = tolower(inputline[i]);
			i++;
		}
		//find the '=' in the string
		if( (ptr = strchr(inputline,'='))!=NULL)
		{
			//skip the spaces after the '='
			for(i=1;ptr[i]==' ' && ptr[i]!='\n';i++);
			//get the value in the file (assumes HEX)
			value = (unsigned long)strtoul(ptr+i,NULL,16);
			//printf("value = 0x%lx\n",(long)value);
		}
		else	value=0; //default case if no '=' found
		//figure which setting it is, and set it
		
		if(strncmp(inputline,"gcmdr",5)==0) settings.gcmdr = value;
		if(strncmp(inputline,"gmode",5)==0) settings.gmode = value;
		if(strncmp(inputline,"gpdirl",6)==0) settings.gpdirl = value;
		if(strncmp(inputline,"gpdirh",6)==0) settings.gpdirh = value;
		if(strncmp(inputline,"gpdatl",6)==0) settings.gpdatl = value;
		if(strncmp(inputline,"gpdath",6)==0) settings.gpdath = value;
		if(strncmp(inputline,"gpiml",5)==0) settings.gpiml = value;
		if(strncmp(inputline,"gpimh",5)==0) settings.gpimh = value;
		if(strncmp(inputline,"dcmdr",5)==0) settings.dcmdr = value;
		if(strncmp(inputline,"dimr",4)==0) settings.dimr = value;
		if(strncmp(inputline,"cmdrl",5)==0) settings.cmdrl = value;
		if(strncmp(inputline,"cmdrh",5)==0) settings.cmdrh = value;
		if(strncmp(inputline,"ccr0l",5)==0) settings.ccr0l = value;
		if(strncmp(inputline,"ccr0h",5)==0) settings.ccr0h = value;
		if(strncmp(inputline,"ccr1l",5)==0) settings.ccr1l = value;
		if(strncmp(inputline,"ccr1h",5)==0) settings.ccr1h = value;
		if(strncmp(inputline,"ccr2l",5)==0) settings.ccr2l = value;
		if(strncmp(inputline,"ccr2h",5)==0) settings.ccr2h = value;
		if(strncmp(inputline,"ccr3l",5)==0) settings.ccr3l = value;
		if(strncmp(inputline,"ccr3h",5)==0) settings.ccr3h = value;
		if(strncmp(inputline,"preamb",6)==0) settings.preamb = value;
		if(strncmp(inputline,"tolen",5)==0) settings.tolen = value;
		if(strncmp(inputline,"accm0",5)==0) settings.accm0 = value;
		if(strncmp(inputline,"accm1",5)==0) settings.accm1 = value;
		if(strncmp(inputline,"accm2",5)==0) settings.accm2 = value;
		if(strncmp(inputline,"accm3",5)==0) settings.accm3 = value;
		if(strncmp(inputline,"udac0",5)==0) settings.udac0 = value;
		if(strncmp(inputline,"udac1",5)==0) settings.udac1 = value;
		if(strncmp(inputline,"udac2",5)==0) settings.udac2 = value;
		if(strncmp(inputline,"udac3",5)==0) settings.udac3 = value;
		if(strncmp(inputline,"ttsa0",5)==0) settings.ttsa0 = value;
		if(strncmp(inputline,"ttsa1",5)==0) settings.ttsa1 = value;
		if(strncmp(inputline,"ttsa2",5)==0) settings.ttsa2 = value;
		if(strncmp(inputline,"ttsa3",5)==0) settings.ttsa3 = value;
		if(strncmp(inputline,"rtsa0",5)==0) settings.rtsa0 = value;
		if(strncmp(inputline,"rtsa1",5)==0) settings.rtsa1 = value;
		if(strncmp(inputline,"rtsa2",5)==0) settings.rtsa2 = value;
		if(strncmp(inputline,"rtsa3",5)==0) settings.rtsa3 = value;
		if(strncmp(inputline,"pcmtx0",6)==0) settings.pcmtx0 = value;
		if(strncmp(inputline,"pcmtx1",6)==0) settings.pcmtx1 = value;
		if(strncmp(inputline,"pcmtx2",6)==0) settings.pcmtx2 = value;
		if(strncmp(inputline,"pcmtx3",6)==0) settings.pcmtx3 = value;
		if(strncmp(inputline,"pcmrx0",6)==0) settings.pcmrx0 = value;
		if(strncmp(inputline,"pcmrx1",6)==0) settings.pcmrx1 = value;
		if(strncmp(inputline,"pcmrx2",6)==0) settings.pcmrx2 = value;
		if(strncmp(inputline,"pcmrx3",6)==0) settings.pcmrx3 = value;
		if(strncmp(inputline,"brrl",4)==0) settings.brrl = value;
		if(strncmp(inputline,"brrh",4)==0) settings.brrh = value;
		if(strncmp(inputline,"timer0",6)==0) settings.timer0 = value;
		if(strncmp(inputline,"timer1",6)==0) settings.timer1 = value;
		if(strncmp(inputline,"timer2",6)==0) settings.timer2 = value;
		if(strncmp(inputline,"timer3",6)==0) settings.timer3 = value;
		if(strncmp(inputline,"xad1",4)==0) settings.xad1 = value;
		if(strncmp(inputline,"xad2",4)==0) settings.xad2 = value;
		if(strncmp(inputline,"rah1",4)==0) settings.rah1 = value;
		if(strncmp(inputline,"rah2",4)==0) settings.rah2 = value;
		if(strncmp(inputline,"ral1",4)==0) settings.ral1 = value;
		if(strncmp(inputline,"ral2",4)==0) settings.ral2 = value;
		if(strncmp(inputline,"amral1",6)==0) settings.amral1 = value;
		if(strncmp(inputline,"amrah1",6)==0) settings.amrah1 = value;
		if(strncmp(inputline,"amral2",6)==0) settings.amral2 = value;
		if(strncmp(inputline,"amrah2",6)==0) settings.amrah2 = value;
		if(strncmp(inputline,"rlcrl",5)==0) settings.rlcrl = value;
		if(strncmp(inputline,"rlcrh",5)==0) settings.rlcrh = value;
		if(strncmp(inputline,"xon",3)==0) settings.xon = value;
		if(strncmp(inputline,"xoff",4)==0) settings.xoff = value;
		if(strncmp(inputline,"mxon",4)==0) settings.mxon = value;
		if(strncmp(inputline,"mxoff",5)==0) settings.mxoff = value;
		if(strncmp(inputline,"tcr",3)==0) settings.tcr = value;
		if(strncmp(inputline,"imr0",4)==0) settings.imr0 = value;
		if(strncmp(inputline,"imr1",4)==0) settings.imr1 = value;
		if(strncmp(inputline,"imr2",4)==0) settings.imr2 = value;
		if(strncmp(inputline,"syncl",5)==0) settings.syncl = value;
		if(strncmp(inputline,"synch",5)==0) settings.synch = value;
		if(strncmp(inputline,"rbufs",5)==0) settings.n_rbufs = value;
		if(strncmp(inputline,"tbufs",5)==0) settings.n_tbufs = value;
		if(strncmp(inputline,"rfsizemax",9)==0) settings.n_rfsize_max = value;
		if(strncmp(inputline,"tfsizemax",9)==0) settings.n_tfsize_max = value;
		memset(inputline,0,100);//clear the line
	}
	//have all of the settings, so lets go
	//(this assumes of course that the toolbox has allready been opened 
	// via a GSCCToolBox_Create() call, which must be done before calling this function)
	if((ret=GSCCToolBox_Configure(gscc_port,&settings))<0)
	{
		printf("configure:%d\n",ret);
	}
	else	printf("Finished gscc Settings.\n");	
}//end of setupgscc()