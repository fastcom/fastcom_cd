/*++

Copyright (c) 2005 Commtech, Inc Wichita ,KS

Module Name:

    loopback.c

Abstract:

    A simple Win32 app that runs a loopback on one port of the gscc device

Environment:

    user mode only

--*/

#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "..\gsccptest.h"


VOID
main(
    IN int   argc,
    IN char *argv[]
    )
/*++

Routine Description:

Arguments:

Return Value:

--*/
{
	HANDLE hDevice;			//handle to the gscc driver/device
	
	DWORD nobyteswritten=0;	//number of bytes that the driver returns as written to the device
	struct serocco_setup gsccsetup;
	char data[4096];		//character array for data storage (passing to and from the driver)
	char rdata[4096];		//character array for data storage (passing to and from the driver)
	DWORD nobytestoread=0;	//the number of bytes to read from the driver
	DWORD nobytestowrite=0;	//the number of bytes to write to the driver
	DWORD nobytesread=0;		//the number of bytes that the driver put in data[]
	ULONG j=0,k=0;				//temp vars
	DWORD returnsize=0;		//temp vars
	ULONG i=0;				//temp vars
	BOOL t;					//temp vars
	OVERLAPPED  os,osr ;				//overlapped structure for use in the transmit routine
	char devname[80];
	unsigned devno;
	ULONG freq=0;
	ULONG loop=0;
	ULONG x=0,error=0;
	ULONG totalsent=0;
	ULONG totalread=0;
	ULONG totalerror=0;
	DWORD lerror;
	ULONG passval[2];

	srand( (unsigned)time( NULL ) );
		
	memset( &os, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
	
	// create I/O event used for overlapped write
	
	os.hEvent = CreateEvent( NULL,    // no security
		TRUE,    // explicit reset req
		FALSE,   // initial event reset
		NULL ) ; // no name
	if (os.hEvent == NULL)
	{
		MessageBox( NULL, "Failed to create event for thread!", "main Error!",
			MB_ICONEXCLAMATION | MB_OK ) ;
		return; 
	}
	
	memset( &osr, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
	
	// create I/O event used for overlapped write
	
	osr.hEvent = CreateEvent( NULL,    // no security
		TRUE,    // explicit reset req
		FALSE,   // initial event reset
		NULL ) ; // no name
	if (osr.hEvent == NULL)
	{
		MessageBox( NULL, "Failed to create event for thread!", "main Error!",
			MB_ICONEXCLAMATION | MB_OK ) ;
		return; 
	}
	
	nobytestoread = 4096;    //this should allways be 4096, it is the maximum frame size returned from the driver at one time
	nobytestowrite = 1023;	//this is the number of bytes to send to the driver to send as a single frame (can be from 1 to 4096)
    
	//this will start up the driver, it also gives us a handle to send
	//and receive data from the driver
	//note that it is created with the FILE_FLAG_OVERLAPPED set
	//if it is not then you must make sequential calls to the driver
	//(ie wait until one call has returned before making another call)
	//the read/write and status functions are set up to use overlapped, they 
	//will return immediatly with a ERROR_IO_PENDING or immediatly with the required data
	//
	if(argc<3)
	{
		printf("Usage: loopback device# Mode\n\tdevice# = 0,1,2,3...\n\tMODE = a = async, b = bisync, h= hdlc\n");
		exit(1);
	}
	devno = 0;
	if(argc>1) devno = atoi(argv[1]);
	
	sprintf(devname,"\\\\.\\GSCC%u",devno);
    hDevice = CreateFile (devname,
		GENERIC_READ | GENERIC_WRITE,
		0,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
		NULL
		);
	
    if (hDevice == INVALID_HANDLE_VALUE)
    {
		//for some reason the driver won't load or isn't loaded
		printf ("Can't get a handle to gsccdrv\n");
		exit(1);
		//abort and leave here!!!
	}
	//we got our handle and are ready to go
	printf("Created gsccdrv--GSCC%u\n\r",devno);
	//this IOCTL function demonstrates how to set the clock generator on 
	//the gscc card
	//this function will return TRUE unless an invalid parameter is given
	//
	//the 335 board has a range from 6-33MHz, set to 6, and divide down in bgr
	//note the ioctl changed as well
//	freq = 16000000;
//	t = DeviceIoControl(hDevice,IOCTL_GSCCDRV_SET_FREQ,&freq,sizeof(ULONG),NULL,0,&returnsize,NULL);
	
	if(argc>2)
	{
		if((argv[2][0]=='H')||(argv[2][0]=='h'))
		{
			printf("HDLC settings\r\n");
			//make to use HDLC settings here

			gsccsetup.gcmdr   =0x0000;
			gsccsetup.gmode   =0x003a;
			gsccsetup.gpdirl  =0x0006;
			gsccsetup.gpdirh  =0x00bf;
			gsccsetup.gpdatl  =0x0000;
			gsccsetup.gpdath  =0x0000;
			gsccsetup.gpiml   =0x0007;
			gsccsetup.gpimh   =0x00ff;
			gsccsetup.dcmdr   =0x0000;
			gsccsetup.dimr    =0x0000;
			gsccsetup.cmdrl   =0x0000;
			gsccsetup.cmdrh   =0x0000;
			gsccsetup.ccr0l   =0x0018;
			gsccsetup.ccr0h   =0x0080;
			gsccsetup.ccr1l   =0x0002;
			gsccsetup.ccr1h   =0x0000;
			gsccsetup.ccr2l   =0x0080;
			gsccsetup.ccr2h   =0x0000;
			gsccsetup.ccr3l   =0x0008;
			gsccsetup.ccr3h   =0x0000;
			gsccsetup.preamb  =0x0000;
			gsccsetup.tolen   =0x0000;
			gsccsetup.accm0   =0x0000;
			gsccsetup.accm1   =0x0000;
			gsccsetup.accm2   =0x0000;
			gsccsetup.accm3   =0x0000;
			gsccsetup.udac0   =0x0000;
			gsccsetup.udac1   =0x0000;
			gsccsetup.udac2   =0x0000;
			gsccsetup.udac3   =0x0000;
			gsccsetup.ttsa0   =0x0000;
			gsccsetup.ttsa1   =0x0000;
			gsccsetup.ttsa2   =0x0000;
			gsccsetup.ttsa3   =0x0000;
			gsccsetup.rtsa0   =0x0000;
			gsccsetup.rtsa1   =0x0000;
			gsccsetup.rtsa2   =0x0000;
			gsccsetup.rtsa3   =0x0000;
			gsccsetup.pcmtx0  =0x0000;
			gsccsetup.pcmtx1  =0x0000;
			gsccsetup.pcmtx2  =0x0000;
			gsccsetup.pcmtx3  =0x0000;
			gsccsetup.pcmrx0  =0x0000;
			gsccsetup.pcmrx1  =0x0000;
			gsccsetup.pcmrx2  =0x0000;
			gsccsetup.pcmrx3  =0x0000;
			gsccsetup.brrl    =0x0000;
			gsccsetup.brrh    =0x0001;
			gsccsetup.timer0  =0x00ff;
			gsccsetup.timer1  =0x003f;
			gsccsetup.timer2  =0x0000;
			gsccsetup.timer3  =0x0000;
			gsccsetup.xad1    =0x00ff;
			gsccsetup.xad2    =0x00ff;
			gsccsetup.ral1    =0x00ff;
			gsccsetup.rah1    =0x00ff;
			gsccsetup.ral2    =0x00ff;
			gsccsetup.rah2    =0x00ff;
			gsccsetup.amral1  =0x0000;
			gsccsetup.amrah1  =0x0000;
			gsccsetup.amral2  =0x0000;
			gsccsetup.amrah2  =0x0000;
			gsccsetup.rlcrl   =0x0000;
			gsccsetup.rlcrh   =0x0000;
			gsccsetup.xon     =0x0000;
			gsccsetup.xoff    =0x0000;
			gsccsetup.mxon    =0x0000;
			gsccsetup.mxoff   =0x0000;
			gsccsetup.tcr     =0x0000;
			gsccsetup.ticr    =0x0000;
			gsccsetup.imr0    =0x0001;
			gsccsetup.imr1    =0x0003;
			gsccsetup.imr2    =0x00fd;
			gsccsetup.syncl   =0x0000;
			gsccsetup.synch   =0x0000;

		}
		if((argv[2][0]=='A')||(argv[2][0]=='a'))
		{
			//make to use async settings here
			printf("ASYNC settings\r\n");

			gsccsetup.gcmdr   =0x0000;
			gsccsetup.gmode   =0x003a;
			gsccsetup.gpdirl  =0x0006;
			gsccsetup.gpdirh  =0x00bf;
			gsccsetup.gpdatl  =0x0000;
			gsccsetup.gpdath  =0x0000;
			gsccsetup.gpiml   =0x0007;
			gsccsetup.gpimh   =0x00ff;
			gsccsetup.dcmdr   =0x0000;
			gsccsetup.dimr    =0x0000;
			gsccsetup.cmdrl   =0x0000;
			gsccsetup.cmdrh   =0x0000;
			gsccsetup.ccr0l   =0x003f;
			gsccsetup.ccr0h   =0x0083;
			gsccsetup.ccr1l   =0x0002;
			gsccsetup.ccr1h   =0x0000;
			gsccsetup.ccr2l   =0x0000;
			gsccsetup.ccr2h   =0x0000;
			gsccsetup.ccr3l   =0x0008;
			gsccsetup.ccr3h   =0x0003;
			gsccsetup.preamb  =0x0000;
			gsccsetup.tolen   =0x0080;
			gsccsetup.accm0   =0x0000;
			gsccsetup.accm1   =0x0000;
			gsccsetup.accm2   =0x0000;
			gsccsetup.accm3   =0x0000;
			gsccsetup.udac0   =0x0000;
			gsccsetup.udac1   =0x0000;
			gsccsetup.udac2   =0x0000;
			gsccsetup.udac3   =0x0000;
			gsccsetup.ttsa0   =0x0000;
			gsccsetup.ttsa1   =0x0000;
			gsccsetup.ttsa2   =0x0000;
			gsccsetup.ttsa3   =0x0000;
			gsccsetup.rtsa0   =0x0000;
			gsccsetup.rtsa1   =0x0000;
			gsccsetup.rtsa2   =0x0000;
			gsccsetup.rtsa3   =0x0000;
			gsccsetup.pcmtx0  =0x0000;
			gsccsetup.pcmtx1  =0x0000;
			gsccsetup.pcmtx2  =0x0000;
			gsccsetup.pcmtx3  =0x0000;
			gsccsetup.pcmrx0  =0x0000;
			gsccsetup.pcmrx1  =0x0000;
			gsccsetup.pcmrx2  =0x0000;
			gsccsetup.pcmrx3  =0x0000;
			gsccsetup.brrl    =0x0000;
			gsccsetup.brrh    =0x0001;
			gsccsetup.timer0  =0x00ff;
			gsccsetup.timer1  =0x0007;
			gsccsetup.timer2  =0x0000;
			gsccsetup.timer3  =0x0000;
			gsccsetup.xad1    =0x0000;
			gsccsetup.xad2    =0x0000;
			gsccsetup.ral1    =0x0000;
			gsccsetup.rah1    =0x0000;
			gsccsetup.ral2    =0x0000;
			gsccsetup.rah2    =0x0000;
			gsccsetup.amral1  =0x0000;
			gsccsetup.amrah1  =0x0000;
			gsccsetup.amral2  =0x0000;
			gsccsetup.amrah2  =0x0000;
			gsccsetup.rlcrl   =0x0000;
			gsccsetup.rlcrh   =0x0000;
			gsccsetup.xon     =0x0000;
			gsccsetup.xoff    =0x0000;
			gsccsetup.mxon    =0x0000;
			gsccsetup.mxoff   =0x0000;
			gsccsetup.tcr     =0x0000;
			gsccsetup.ticr    =0x0000;
			gsccsetup.imr0    =0x0081;
			gsccsetup.imr1    =0x0000;
			gsccsetup.imr2    =0x00fd;
			gsccsetup.syncl   =0x0000;
			gsccsetup.synch   =0x0000;

		}
		if((argv[2][0]=='B')||(argv[2][0]=='b'))
		{
			//make to use bisync settings here
			printf("BISYNC settings\r\n");

			gsccsetup.gcmdr   =0x0000;
			gsccsetup.gmode   =0x003a;
			gsccsetup.gpdirl  =0x0006;
			gsccsetup.gpdirh  =0x00bf;
			gsccsetup.gpdatl  =0x0000;
			gsccsetup.gpdath  =0x0000;
			gsccsetup.gpiml   =0x0007;
			gsccsetup.gpimh   =0x00ff;
			gsccsetup.dcmdr   =0x0000;
			gsccsetup.dimr    =0x0000;
			gsccsetup.cmdrl   =0x0000;
			gsccsetup.cmdrh   =0x0000;
			gsccsetup.ccr0l   =0x0018;
			gsccsetup.ccr0h   =0x0082;
			gsccsetup.ccr1l   =0x0002;
			gsccsetup.ccr1h   =0x0010;
			gsccsetup.ccr2l   =0x000c;
			gsccsetup.ccr2h   =0x0000;
			gsccsetup.ccr3l   =0x0088;
			gsccsetup.ccr3h   =0x0013;
			gsccsetup.preamb  =0x0000;
			gsccsetup.tolen   =0x0000;
			gsccsetup.accm0   =0x0000;
			gsccsetup.accm1   =0x0000;
			gsccsetup.accm2   =0x0000;
			gsccsetup.accm3   =0x0000;
			gsccsetup.udac0   =0x0000;
			gsccsetup.udac1   =0x0000;
			gsccsetup.udac2   =0x0000;
			gsccsetup.udac3   =0x0000;
			gsccsetup.ttsa0   =0x0000;
			gsccsetup.ttsa1   =0x0000;
			gsccsetup.ttsa2   =0x0000;
			gsccsetup.ttsa3   =0x0000;
			gsccsetup.rtsa0   =0x0000;
			gsccsetup.rtsa1   =0x0000;
			gsccsetup.rtsa2   =0x0000;
			gsccsetup.rtsa3   =0x0000;
			gsccsetup.pcmtx0  =0x0000;
			gsccsetup.pcmtx1  =0x0000;
			gsccsetup.pcmtx2  =0x0000;
			gsccsetup.pcmtx3  =0x0000;
			gsccsetup.pcmrx0  =0x0000;
			gsccsetup.pcmrx1  =0x0000;
			gsccsetup.pcmrx2  =0x0000;
			gsccsetup.pcmrx3  =0x0000;
			gsccsetup.brrl    =0x0000;
			gsccsetup.brrh    =0x0001;
			gsccsetup.timer0  =0x00ff;
			gsccsetup.timer1  =0x0007;
			gsccsetup.timer2  =0x0000;
			gsccsetup.timer3  =0x0000;
			gsccsetup.xad1    =0x0000;
			gsccsetup.xad2    =0x0000;
			gsccsetup.ral1    =0x0000;
			gsccsetup.rah1    =0x0000;
			gsccsetup.ral2    =0x0000;
			gsccsetup.rah2    =0x0000;
			gsccsetup.amral1  =0x0000;
			gsccsetup.amrah1  =0x0000;
			gsccsetup.amral2  =0x0000;
			gsccsetup.amrah2  =0x0000;
			gsccsetup.rlcrl   =0x0000;
			gsccsetup.rlcrh   =0x0000;
			gsccsetup.xon     =0x0000;
			gsccsetup.xoff    =0x0000;
			gsccsetup.mxon    =0x0000;
			gsccsetup.mxoff   =0x0000;
			gsccsetup.tcr     =0x00ff;
			gsccsetup.ticr    =0x0000;
			gsccsetup.imr0    =0x0083;
			gsccsetup.imr1    =0x0003;
			gsccsetup.imr2    =0x00fd;
			gsccsetup.syncl   =0x0000;
			gsccsetup.synch   =0x005e;

		}
	}

			gsccsetup.n_rbufs   =0x000a;
			gsccsetup.n_tbufs   =0x000a;
			gsccsetup.n_rfsize_max=0x1000;
			gsccsetup.n_tfsize_max=0x1000;

	//when called this IOCTL will set the registers of the gscc and
	//reset the TX and RX machines (issuing a separate XRES and RHR command)
	//if the resets time out the function will return FALSE, otherwise it will
	//return TRUE

	t = DeviceIoControl(hDevice,IOCTL_GSCCDRV_SET_SEROCCO,&gsccsetup,sizeof(struct serocco_setup),NULL,0,&returnsize,NULL);
	if(t==TRUE)printf("SETTINGS SUCCESSFUL:%lu\n\r",returnsize);
	if(t==FALSE)printf("SETTINGS FAILED:%lu\n\r",returnsize);

	DeviceIoControl(hDevice,IOCTL_GSCCDRV_FLUSH_RX,NULL,0,NULL,0,&returnsize,NULL);
	DeviceIoControl(hDevice,IOCTL_GSCCDRV_FLUSH_TX,NULL,0,NULL,0,&returnsize,NULL);

	//now we enter the main loop of this thread        
	//all we are going to do is wait for a keyhit and when we 
	//get one we will fill a frame with that key and send it out
	//the gscc using a WriteFile() to the gscc device
	//if the [ESC] key is pressed the program will terminate
	do
	{
		if( (argv[2][0]=='B')||(argv[2][0]=='b') )
		{
			passval[0] = 0x15;
			passval[1] = 0x04;
			DeviceIoControl(hDevice,IOCTL_GSCCDRV_WRITE_SEROCCO_REGISTER,&passval[0],2*sizeof(ULONG),NULL,0,&returnsize,NULL);

//			printf("HUNT command issued\n\r");
		}
		//here the user has pressed a key that was not t,r,i,p or [esc] so we will
		//take that character and fill a data buffer with it, then send that 
		//buffer out the gscc as a frame.
		
		for(i=0;i<1024;i++)
		{
			data[i] = rand()&0xff;//;(char)i;//fill the frame with the key
			if((data[i] == (char)0xff)&&( (argv[2][0]=='B')||(argv[2][0]=='b') )) data[i] = (char)0x0aa;
		}

		

		t = WriteFile(hDevice,&data,nobytestowrite,&nobyteswritten,&os);//send the frame
		//		printf("WRITEFILE gsccdrv%lu \n\r",nobyteswritten);//if nobyteswritten doesnt = sizeof(struct buf) something is wrong
		//		if(t==TRUE)printf("TX returned TRUE\n\r"); //if returned true then the IO request was started (txing has begun)
		if(t==FALSE)                    //if it returned FALSE then the IO request is queued (waiting for previous frame to get done sending)
		{                       //and we must wait until the os.event gets signaled before we try any more sending
			printf("TX returned FALSE\n\r");
			lerror = GetLastError();
			if (lerror == ERROR_IO_PENDING) //overlapped io indicates that the IRP isn't done being processed yet (status hasn't changed)
			{
				// wait for a second for this transmission to complete
				do
				{
					k = WaitForSingleObject( os.hEvent, 1000 );//1 second timeout
					if(k==WAIT_TIMEOUT)
					{
						printf("WRITE:TIMEOUT\r\n");
						//this will execute every 1 second
						//you could put a counter in here and if the 
						//driver takes an inordinate ammout of time
						//to complete, you could issue a flush TX command
						//and break out of this loop
					}
					if(k==WAIT_ABANDONED)
					{
						printf("WRITE:ABANDONED\r\n");
					}
				}while(k!=WAIT_OBJECT_0);
			}
			else printf("Write error #%d\n",lerror);
		}

		Sleep(30);
		t = ReadFile(hDevice,&rdata,nobytestoread,&nobytesread,&osr);
		
		if(t==FALSE)
		{
			lerror = GetLastError();
			if (lerror == ERROR_IO_PENDING) //overlapped io indicates that the IRP isn't done being processed yet (status hasn't changed)
			{
printf("blocked\n");
		//start a read request by calling ReadFile() with the gsccdevice handle
		//if it returns true then we received a frame 
		//if it returns false and ERROR_IO_PENDING then there are no
		//receive frames available so we wait until the overlapped struct
		//gets signaled.
				// wait for a receive frame to come in, note it will wait forever
				do
				{
					j = WaitForSingleObject( osr.hEvent, 1000 );//1 second timeout
					if(j==WAIT_TIMEOUT)
					{
						printf("READ:TIMEOUT\r\n");
						//this will execute every 1 second
						//you could put a counter in here and if the 
						//driver takes an inordinate ammout of time
						//to complete, you could issue a flush RX command
						//and break out of this loop
					}
					if(j==WAIT_ABANDONED)
					{
						printf("READ:ABANDONED\r\n");
					}
					
				}while(j!=WAIT_OBJECT_0);
				GetOverlappedResult(hDevice,&osr,&nobytesread,TRUE); //here to get the actual nobytesread!!!
			}
			else printf("Read error #%d",lerror);

			t=TRUE;
			
		}
		else
		{
		//printf("READ: returned TRUE!\n");
		}
		
		if( (argv[2][0]=='h')||(argv[2][0]=='H')||(argv[2][0]=='b')||(argv[2][0]=='B') )
		{
			//hdlc has one extra byte (RSTA) and bisync has one extra byte (TERMINATION character 0xff)
			
			if(nobytesread!=nobyteswritten+1)
			{
				printf("Byte Count ERROR rec:%d != sent:%d\n",nobytesread,nobyteswritten+1);
				error++;
			}
		}
		else
		{
			if(nobytesread!=nobyteswritten)
			{
				printf("Byte Count ERROR rec:%d != sent:%d\n",nobytesread,nobyteswritten);
				error++;
				DeviceIoControl(hDevice,IOCTL_GSCCDRV_FLUSH_RX,NULL,0,NULL,0,&returnsize,NULL);
				DeviceIoControl(hDevice,IOCTL_GSCCDRV_FLUSH_TX,NULL,0,NULL,0,&returnsize,NULL);

			}
		}
		
		if(nobytesread!=0)
		{
			if( (argv[2][0]=='h')||(argv[2][0]=='H')||(argv[2][0]=='b')||(argv[2][0]=='B') )
				{
					for(x=0;x<nobytesread-1;x++)
						if(rdata[x]!=data[x])	error++;
				}
				else 
				{
					//async
					for(x=0;x<nobytesread;x++)
						if(rdata[x]!=data[x])	error++;
				}
		}
		//printf("Found: %d errors\n",error);
		totalerror +=error;
		loop++;
		totalsent+=nobyteswritten;
		totalread+=nobytesread;
		printf("loop:%d\n",loop);
		
	}while(!_kbhit());

//	getch();
	printf("Found %d errors out of %d frames\n",totalerror,loop);
	printf("Wrote %d bytes.\n",totalsent);
			if( (argv[2][0]=='h')||(argv[2][0]=='H')||(argv[2][0]=='b')||(argv[2][0]=='B') )
	{
		printf("Read %d bytes.\n",totalread-loop);
	}
	else
	{
		printf("Read %d bytes.\n",totalread);
	}

	//when we want to exit the program there is still the possibility that
	//a frame is being transmitted so we spin in this IOCTL function until
	//the driver reports that it is not transmitting
	//technically you could omit this if you are leaving for good
	//(ie are done computing for the day, and are about to shutdown NT)
	//but it might cause problems if the gscc device is started again
	//without powering down, misc errors will occur when the device is 
	//re-opened as it was halted in mid transmitting
	//EXE interrupts are most likely
	//
	i = 0;
	j = 0;
	do
	{
		t = DeviceIoControl(hDevice,IOCTL_GSCCDRV_TX_ACTIVE,NULL,0,&j,sizeof(DWORD),&returnsize,NULL);
		//will return 0 in output buffer (j) if not active
		//will return 1 in output buffer (j) if active
	}while(j==1);//keep requesting until not active
	//carefull not to close the device while transmitting or receiving data

	Sleep(1000);
	CloseHandle (hDevice);// stops the gscc from interrupting (ie shuts it down)

	printf("exiting program\n\r");          //exit message
}                                               //done

							 

