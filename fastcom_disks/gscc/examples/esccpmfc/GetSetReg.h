// GetSetReg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGetSetReg dialog

class CGetSetReg : public CDialog
{
// Construction
public:
	CGetSetReg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CGetSetReg)
	enum { IDD = IDD_GETSETREG };
	CString	m_regs;
	CString	m_value;
	//}}AFX_DATA

ULONG m_idx;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGetSetReg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CGetSetReg)
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
