// Devno.cpp : implementation file
//

#include "stdafx.h"
#include "esccpmfc.h"
#include "Devno.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDevno dialog


CDevno::CDevno(CWnd* pParent /*=NULL*/)
	: CDialog(CDevno::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDevno)
	m_Devno = _T("");
	//}}AFX_DATA_INIT
}


void CDevno::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDevno)
	DDX_Text(pDX, IDC_EDIT1, m_Devno);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDevno, CDialog)
	//{{AFX_MSG_MAP(CDevno)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDevno message handlers
