/*
Copyright (c) 1995,1997 Commtech, Inc Wichita ,KS

Module Name:

    timerwrite.c

Abstract:

    A simple Win32 app that uses the gscc's hardware timer to write data.  You could easily
	use the timer for other purposes, but a timed write is easy to illustrate.

Environment:

    user mode only
*/

#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include "..\gsccptest.h"


VOID main(IN int   argc, IN char *argv[])
{
	HANDLE hDevice;			//handle to the gscc driver/device
	DWORD nobyteswritten;	//number of bytes that the driver returns as written to the device
	DWORD nobytestowrite = 128;	//the number of bytes to write to the driver
	struct serocco_setup gsccsetup;
	char data[128];		//character array for data storage (passing to and from the driver)
	OVERLAPPED  os ;		//overlapped structure for use in the transmit routine
	unsigned devnum;
	char devname[80];
	ULONG freq;
	DWORD returnsize;
	DWORD mask;
	ULONG output;
	BOOL t;
	int i;
	ULONG k;
	
	memset(&os,0,sizeof(OVERLAPPED));	//wipe the overlapped structure
	memset(&gsccsetup,0,sizeof(struct serocco_setup));	//wipe setup structure

	if(argc!=2)
	{
		printf("Usage:%s portnum (i.e. GSCC#)",argv[0]);
		exit(1);
	}
	
	//create I/O event used for overlapped write
	os.hEvent = CreateEvent( NULL,    // no security
		TRUE,    // explicit reset req
		FALSE,   // initial event reset
		NULL ) ; // no name
	if (os.hEvent == NULL)
	{
		printf("Failed to create event for thread!\n");
		exit(1); 
	}

	devnum = atoi(argv[1]);
	sprintf(devname,"\\\\.\\GSCC%u",devnum);
	hDevice = CreateFile (devname,
		  GENERIC_READ | GENERIC_WRITE,
		  0,
		  NULL,
		  OPEN_EXISTING,
		  FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
		  NULL
		  );

    if (hDevice == INVALID_HANDLE_VALUE)	//for some reason the driver won't load or isn't loaded
    {
		printf ("Can't get a handle to gsccdrv\n");
		CloseHandle( os.hEvent );	//done with event
		exit(1);	//abort and leave here!!!
	}
	freq = 7372800;
	t = DeviceIoControl(hDevice,IOCTL_GSCCDRV_SET_FREQ,&freq,sizeof(ULONG),NULL,0,&returnsize,NULL);

	gsccsetup.gcmdr   =0x0000;
	gsccsetup.gmode   =0x003a;
	gsccsetup.gpdirl  =0x0006;
	gsccsetup.gpdirh  =0x00bf;
	gsccsetup.gpdatl  =0x0000;
	gsccsetup.gpdath  =0x0000;
	gsccsetup.gpiml   =0x0007;
	gsccsetup.gpimh   =0x00ff;
	gsccsetup.dcmdr   =0x0000;
	gsccsetup.dimr    =0x0000;
	gsccsetup.cmdrl   =0x0000;
	gsccsetup.cmdrh   =0x0000;
	gsccsetup.ccr0l   =0x001f;
	gsccsetup.ccr0h   =0x0080;
	gsccsetup.ccr1l   =0x0002;
	gsccsetup.ccr1h   =0x0000;
	gsccsetup.ccr2l   =0x0080;
	gsccsetup.ccr2h   =0x0000;
	gsccsetup.ccr3l   =0x0008;
	gsccsetup.ccr3h   =0x0000;
	gsccsetup.preamb  =0x0000;
	gsccsetup.tolen   =0x0000;
	gsccsetup.accm0   =0x0000;
	gsccsetup.accm1   =0x0000;
	gsccsetup.accm2   =0x0000;
	gsccsetup.accm3   =0x0000;
	gsccsetup.udac0   =0x0000;
	gsccsetup.udac1   =0x0000;
	gsccsetup.udac2   =0x0000;
	gsccsetup.udac3   =0x0000;
	gsccsetup.ttsa0   =0x0000;
	gsccsetup.ttsa1   =0x0000;
	gsccsetup.ttsa2   =0x0000;
	gsccsetup.ttsa3   =0x0000;
	gsccsetup.rtsa0   =0x0000;
	gsccsetup.rtsa1   =0x0000;
	gsccsetup.rtsa2   =0x0000;
	gsccsetup.rtsa3   =0x0000;
	gsccsetup.pcmtx0  =0x0000;
	gsccsetup.pcmtx1  =0x0000;
	gsccsetup.pcmtx2  =0x0000;
	gsccsetup.pcmtx3  =0x0000;
	gsccsetup.pcmrx0  =0x0000;
	gsccsetup.pcmrx1  =0x0000;
	gsccsetup.pcmrx2  =0x0000;
	gsccsetup.pcmrx3  =0x0000;
	gsccsetup.brrl    =0x002f;
	gsccsetup.brrh    =0x0003;
	gsccsetup.timer0  =0x00ff;
	gsccsetup.timer1  =0x0025;
	gsccsetup.timer2  =0x0000;
	gsccsetup.timer3  =0x0007;
	gsccsetup.xad1    =0x00ff;
	gsccsetup.xad2    =0x00ff;
	gsccsetup.ral1    =0x00ff;
	gsccsetup.rah1    =0x00ff;
	gsccsetup.ral2    =0x00ff;
	gsccsetup.rah2    =0x00ff;
	gsccsetup.amral1  =0x0000;
	gsccsetup.amrah1  =0x0000;
	gsccsetup.amral2  =0x0000;
	gsccsetup.amrah2  =0x0000;
	gsccsetup.rlcrl   =0x0000;
	gsccsetup.rlcrh   =0x0000;
	gsccsetup.xon     =0x0000;
	gsccsetup.xoff    =0x0000;
	gsccsetup.mxon    =0x0000;
	gsccsetup.mxoff   =0x0000;
	gsccsetup.tcr     =0x0000;
	gsccsetup.ticr    =0x0000;
	gsccsetup.imr0    =0x0001;
	gsccsetup.imr1    =0x0003;
	gsccsetup.imr2    =0x00fd;
	gsccsetup.syncl   =0x0000;
	gsccsetup.synch   =0x0000;

	gsccsetup.n_rbufs   =0x000a;
	gsccsetup.n_tbufs   =0x000a;
	gsccsetup.n_rfsize_max=0x1000;
	gsccsetup.n_tfsize_max=0x1000;


	t = DeviceIoControl(hDevice,IOCTL_GSCCDRV_SET_SEROCCO,&gsccsetup,sizeof(struct serocco_setup),NULL,0,&returnsize,NULL);
	if(t==TRUE)printf("SETTINGS SUCCESSFUL:%lu\n\r",returnsize);
	if(t==FALSE)printf("SETTINGS FAILED:%lu\n\r",returnsize);

	DeviceIoControl(hDevice,IOCTL_GSCCDRV_START_TIMER,NULL,0,NULL,0,&returnsize,NULL);

	printf("timer command issued\n\r");

	for(i=0;i<128;i++)data[i] = (char)i;//fill the frame with the key
	do
	{
		mask = ST_TIN; //(mask out rxdone/txdone messages)
		t = DeviceIoControl(hDevice,IOCTL_GSCCDRV_STATUS,&mask,sizeof(ULONG),&output,sizeof(ULONG),&returnsize,&os);
		if(t==FALSE)
		{
			if (GetLastError() == ERROR_IO_PENDING) //overlapped io indicates that the IRP isn't done being processed yet (status hasn't changed)
			{
				do
				{
					k = WaitForSingleObject( os.hEvent, 1000 );//1 second timeout
					if(k==WAIT_TIMEOUT)
					{
						printf("STATUS:TIMEOUT\r\n");
						//this will execute every 1 second
						//if you want do do some periodic processing here would
						//be a good place to put it...
					}
					if(k==WAIT_ABANDONED)
					{
					}
					
				}while(k!=WAIT_OBJECT_0);//exit if we get signaled or if the main thread quits
				GetOverlappedResult(hDevice,&os,&returnsize,TRUE); //here to get the actual nobytesread!!!
			}
		}
		if((output&ST_TIN)==ST_TIN) printf("STATUS, Timer Expired\r\n");
		
		t = WriteFile(hDevice,&data,nobytestowrite,&nobyteswritten,&os);//send the frame
		if(t==TRUE)printf("TX returned TRUE\n\r"); //if returned true then the IO request was started (txing has begun)
		if(t==FALSE)                    //if it returned FALSE then the IO request is queued (waiting for previous frame to get done sending)
		{                       //and we must wait until the os.event gets signaled before we try any more sending
			printf("TX returned FALSE\n\r");
			if (GetLastError() == ERROR_IO_PENDING)  //IO_PENDING is the indication that the request was queued, if not this than a parameter error occured
			{
				// wait for a second for this transmission to complete
				do
				{
					k = WaitForSingleObject( os.hEvent, 1000 );//1 second timeout
					if(k==WAIT_TIMEOUT)
					{
						printf("Write timeout\n");
						//this will execute every 1 second
						//you could put a counter in here and if the 
						//driver takes an inordinate ammout of time
						//to complete, you could issue a flush TX command
						//and break out of this loop
					}
					if(k==WAIT_ABANDONED)
					{
					}
					
				}while(k!=WAIT_OBJECT_0);
			}
		}
	}while(!_kbhit());

	CloseHandle (hDevice);// stops the gscc from interrupting (ie shuts it down)
	CloseHandle(os.hEvent);

}