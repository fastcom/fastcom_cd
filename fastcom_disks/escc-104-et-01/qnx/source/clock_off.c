/******************************************************
 *
 * Copyright (C) 2000, 2001 Commtech, Inc. Wichita KS
 *
 * clock_off.c -- example code to turn off (tri-state) the TT clock 
 *
 * qnx 4.25 1/4/01
 ******************************************************/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <ioctl.h>
#include <sys/qioctl.h>
#include <string.h>
#include <sys/select.h>
#include <errno.h>
#include <time.h>

#include "esccdrv_user.h"


void main(int argc, char *argv[])
{
int fd;
unsigned long status;
int port;
char filename[40];

if(argc<2)
{
	printf("usage:\n");
	printf("clock_off port\n");
	exit(1);
}
else port = atoi(argv[1]);

//open device specified on command line
sprintf(filename,"/escc/%u",port);
fd = open(filename,O_RDWR);//|O_NONBLOCK);
if(fd == -1)
{
	perror(NULL);
exit(1);
}


//issue IOCTL call to turn off (tri-state) the clock signal for this port

status =0;
if(qnx_ioctl(fd,ESCC_SET_CLOCK,&status,sizeof(status),NULL,0)==-1)perror(NULL);
else printf("ESCC/%u TT clock is OFF\n",port);

close(fd);
}


