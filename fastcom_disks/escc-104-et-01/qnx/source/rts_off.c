/******************************************************
 *
 * Copyright (C) 2000, 2001 Commtech, Inc. Wichita KS
 *
 * rts_off.c -- example code to turn off the RTS line
 *
 * qnx 4.25 1/4/01
 ******************************************************/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <ioctl.h>
#include <sys/qioctl.h>
#include <string.h>
#include <sys/select.h>
#include <errno.h>
#include <time.h>

#include "esccdrv_user.h"


void main(int argc, char *argv[])
{
int fd;
unsigned long status;
int port;
char filename[40];
regsingle reg;

if(argc<2)
{
	printf("usage:\n");
	printf("rts_off port\n");
	exit(1);
}
else port = atoi(argv[1]);
//open port specified on command line
sprintf(filename,"/escc/%u",port);
fd = open(filename,O_RDWR);//|O_NONBLOCK);
if(fd == -1)
{
	perror(NULL);
exit(1);
}

//get current contents of the mode register from the 82532
reg.port = MODE;
if(qnx_ioctl(fd,ESCC_READ_REGISTER,&reg.port,sizeof(unsigned long),&status,sizeof(status))==-1)perror(NULL);

reg.port = MODE;
reg.data = (status&0xfb);//turn off RTS
//set the value of the mode regsiter on the 82532 to the new value
if(qnx_ioctl(fd,ESCC_WRITE_REGISTER,&reg,sizeof(regsingle),NULL,0)==-1)perror(NULL);
else printf("ESCC/%u RTS is OFF\n",port);

close(fd);
}


