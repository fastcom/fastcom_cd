/******************************************************
 *
 * Copyright (C) 2000, 2001 Commtech, Inc. Wichita KS
 *
 * write_test2.c -- example code to use the select function to issue write calls
 *
 * qnx 4.25 1/4/01
 ******************************************************/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <ioctl.h>
#include <sys/qioctl.h>
#include <string.h>
#include <sys/select.h>
#include <errno.h>
#include <time.h>

#include "esccdrv_user.h"

void decode_stat(unsigned long stat);

void main(int argc, char *argv[])
{
int fd;
char data[4096];
char sbuf[255];
char rbuf[10];
unsigned i;
setup settings;
board_settings bset;
clkset clk;
regsingle reg;
unsigned val;
unsigned long status;
unsigned writesize;
FILE *fin;
int ret;
int n;
fd_set wfd;
struct timeval tv;

//open port 0
fd = open("/escc/0",O_RDWR);//|O_NONBLOCK);
if(fd == -1)
{
	perror(NULL);
exit(1);
}

//open file given on command line from user
fin = fopen(argv[1],"rb");
if(fin==NULL)
{
printf("cannot open input file %s\n",argv[1]);
close(fd);
exit(1);
}



while(!feof(fin))//do this loop while there is still data to read from the user file
{
//printf("top of loop\n");

FD_ZERO(&wfd);
FD_SET(fd,&wfd);

tv.tv_sec = 1;//1 second timeout
tv.tv_usec = 0;

n = select(fd+1,0,&wfd,0,&tv);//issue select call
//printf("tv:%lu, tv:%lu\n",tv.tv_sec,tv.tv_usec);
switch(n)
	{
	case -1:
		perror("select error");
		break;
	case 0:
		printf("write timeout\n");//will be here once a second while there is not a transmit buffer available
		break;
	default:
		if(FD_ISSET(fd,&wfd))//will be here when select finishes and indicates that there is a transmit buffer available to complete a write call immediately
		{
		printf("isset true..start write\n");
			i = fread(data,1,1024,fin);//get data from user file
		if((ret = write(fd,data,i))==-1)//write data out escc port (note assumes that open_init is used to open the port in HDLC mode with 4k buffers)
			{
			if(errno!=EAGAIN)perror(NULL);
			printf("write error\n");
			}
		}
		else printf("select finished, no data\n");
	}
}
//get/clear the driver/82532 status
if(qnx_ioctl(fd,ESCC_IMMEDIATE_STATUS,NULL,0,&status,sizeof(status))==-1)perror(NULL);
//display status values
decode_stat(status);

fclose(fin);//done with user file
close(fd);//done with escc port/driver
}


void decode_stat(unsigned long stat)
{
if(stat==0)return;
printf("STATUS DECODE:\n");
if(stat&ST_RX_DONE) printf("Receive Done\n");
if(stat&ST_OVF) printf("RX BUFFERS overflow\n");
if(stat&ST_RFS) printf("Receive Frame Start\n");
if(stat&ST_RX_TIMEOUT) printf("RX timeout\n");
if(stat&ST_RSC) printf("Receive Status Change\n");
if(stat&ST_PERR) printf("Parity Error\n");
if(stat&ST_PCE) printf("Protocol Error\n");
if(stat&ST_FERR) printf("Framing Error\n");
if(stat&ST_SYN) printf("SYN detect\n");
if(stat&ST_DPLLA) printf("DPLL asyncronous\n");
if(stat&ST_CDSC) printf("CD changed state\n");
if(stat&ST_RFO) printf("Receive Frame Overflow\n");
if(stat&ST_EOP) printf("End of Poll\n");
if(stat&ST_BRKD) printf("Break Detected\n");
if(stat&ST_ONLP) printf("On Loop\n");
if(stat&ST_BRKT) printf("Break Terminated\n");
if(stat&ST_ALLS) printf("All Sent\n");
if(stat&ST_EXE) printf("Transmit Underrun\n");
if(stat&ST_TIN) printf("Timer Expired\n");
if(stat&ST_CTSC) printf("CTS changed state\n");
if(stat&ST_XMR) printf("Transmit Message Repeat\n");
if(stat&ST_TX_DONE) printf("Transmit Done\n");
if(stat&ST_DMA_TC) printf("DMA TC reached\n");
if(stat&ST_DSR0C) printf("DSR0 changed state\n");
if(stat&ST_DSR1C) printf("DSR1 changed state\n");
}
