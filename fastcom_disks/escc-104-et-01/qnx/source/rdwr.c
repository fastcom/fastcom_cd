/******************************************************
 *
 * Copyright (C) 2000, 2001 Commtech, Inc. Wichita KS
 *
 * rdwr.c -- example code; simple loopback that sends an 
 *			 ascii count out the port, and expects to receive
 *			 and display it loopback fashion.
 *
 * qnx 4.25 1/4/01
 ******************************************************/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <ioctl.h>
#include <sys/qioctl.h>
#include <string.h>
#include "esccdrv_user.h"

void decode_stat(unsigned long stat);

void main(int argc, char *argv[])
{
int fd;
char data[4096];
char sbuf[255];
char rbuf[10];
unsigned i;
setup settings;
board_settings bset;
clkset clk;
regsingle reg;
unsigned val;
unsigned long status;
unsigned writesize;
int ret;
unsigned long txcount;
char devname[40];
//open port given on command line
if(argc>1) sprintf(devname,"/escc/%u",atoi(argv[1]));
else sprintf(devname,"/escc/0");
printf("device:%s\n",devname);
fd = open(devname,O_RDWR);//|O_NONBLOCK);
if(fd == -1) perror(NULL);

txcount = 0;
while(!kbhit())//do while user doesn't hit a key
{
sprintf(data,"%10.10lu     ",txcount);//fill our transmit buffer with the current ascii count value
writesize = 15;
if((ret = write(fd,data,writesize))==-1)//send it out the port
	{
	perror(NULL);
	printf("write failed:%u\n",ret);
	}

if(ret!=15) printf("ERROR,write returned:%u\n",ret);
if(((ret = read(fd,data,4096)))==-1)//receive from the port (the count is expected back)
	{
	perror(NULL);
	printf("read failed:%u\n",ret);
	}
if(ret!=16)printf("read size problem:%u!=16\n",ret);//assumed that HDLC mode is setup via the open_init function, we should get 15 data bytes and one RSTA HDLC status byte back from the read call
for(i=0;i<ret-1;i++)printf("%c",data[i]);//display the received data
printf("\ntxcount:%lu\n",txcount);//display the current tx count
txcount++;//next count value

}
getch();
printf("paused...\n");
getch();
//get status value from driver
if(qnx_ioctl(fd,ESCC_STATUS,NULL,0,&status,sizeof(status))==-1)perror(NULL);
//display the status
decode_stat(status);


close(fd);
}


void decode_stat(unsigned long stat)
{
if(stat==0)return;
printf("STATUS DECODE:\n");
if(stat&ST_RX_DONE) printf("Receive Done\n");
if(stat&ST_OVF) printf("RX BUFFERS overflow\n");
if(stat&ST_RFS) printf("Receive Frame Start\n");
if(stat&ST_RX_TIMEOUT) printf("RX timeout\n");
if(stat&ST_RSC) printf("Receive Status Change\n");
if(stat&ST_PERR) printf("Parity Error\n");
if(stat&ST_PCE) printf("Protocol Error\n");
if(stat&ST_FERR) printf("Framing Error\n");
if(stat&ST_SYN) printf("SYN detect\n");
if(stat&ST_DPLLA) printf("DPLL asyncronous\n");
if(stat&ST_CDSC) printf("CD changed state\n");
if(stat&ST_RFO) printf("Receive Frame Overflow\n");
if(stat&ST_EOP) printf("End of Poll\n");
if(stat&ST_BRKD) printf("Break Detected\n");
if(stat&ST_ONLP) printf("On Loop\n");
if(stat&ST_BRKT) printf("Break Terminated\n");
if(stat&ST_ALLS) printf("All Sent\n");
if(stat&ST_EXE) printf("Transmit Underrun\n");
if(stat&ST_TIN) printf("Timer Expired\n");
if(stat&ST_CTSC) printf("CTS changed state\n");
if(stat&ST_XMR) printf("Transmit Message Repeat\n");
if(stat&ST_TX_DONE) printf("Transmit Done\n");
if(stat&ST_DMA_TC) printf("DMA TC reached\n");
if(stat&ST_DSR0C) printf("DSR0 changed state\n");
if(stat&ST_DSR1C) printf("DSR1 changed state\n");
}
