/******************************************************
 *
 * Copyright (C) 2000, 2001 Commtech, Inc. Wichita KS
 *
 * looptest.c -- example code to execute a loopback on a ESCC port
 *
 * qnx 4.25 1/4/01
 ******************************************************/
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <ioctl.h>
#include <sys/qioctl.h>
#include <string.h>
#include "esccdrv_user.h"

void decode_stat(unsigned long stat);

void main(int argc, char *argv[])
{
int fd;
char data[4096];
char sbuf[255];
char rbuf[10];
unsigned i;
setup settings;
board_settings bset;
clkset clk;
regsingle reg;
unsigned val;
unsigned long status;
unsigned writesize;
int ret;
unsigned long txcount;
char devname[40];

//open port specified on command line
if(argc>1) sprintf(devname,"/escc/%u",atoi(argv[1]));
else sprintf(devname,"/escc/0");
printf("device:%s\n",devname);
fd = open(devname,O_RDWR);//|O_NONBLOCK);
if(fd == -1) perror(NULL);

txcount = 0;
while(!kbhit()) //continue while no keyboard activity (will exit on keypress)
{
for(i=0;i<257;i++) data[i] = i;//fill our outgoing data struct with 0-ff
writesize = 256;//send 256 bytes of data
if((ret = write(fd,data,writesize))==-1)//send it out the escc port
	{
	perror(NULL);
	printf("write failed:%u\n",ret);
	}
if(ret!=256) printf("ERROR,write returned:%u\n",ret);//it should send all 256 bytes unless you opened the port with a buffer less than 256 bytes. (it is assumed here that the open_init program opened and configured the port with a 4k buffer size)

if(((ret = read(fd,data,4096)))==-1)//issue the read function call, should return with 257 bytes, our 256 looped back data bytes and a HDLC receive status byte (contents of the RSTA register will be the last byte received)
	{
	perror(NULL);
	printf("read failed:%u\n",ret);
	}
if(ret!=257)printf("read size problem:%u!=257\n",ret);//should be 256 data bytes and one status byte (again assuming that open_init opened/configured this port to be in HDLC mode)
for(i=0;i<ret-1;i++) //check the data bytes
{
	if(i!=data[i])
	{
	printf("error in received data at %u!=%u\n",i,data[i]);
	}
}
printf("Loopcount:%lu\n",txcount);//display the cycle we are on
txcount++;//increment the cycle counter

}
getch();//user pressed a key to break out of the loop get the key
printf("paused...\n");
getch();
//pull/clear the status from the driver 
if(qnx_ioctl(fd,ESCC_IMMEDIATE_STATUS,NULL,0,&status,sizeof(status))==-1)perror(NULL);
//display the status values
decode_stat(status);


close(fd);
}


void decode_stat(unsigned long stat)
{
if(stat==0)return;
printf("STATUS DECODE:\n");
if(stat&ST_RX_DONE) printf("Receive Done\n");
if(stat&ST_OVF) printf("RX BUFFERS overflow\n");
if(stat&ST_RFS) printf("Receive Frame Start\n");
if(stat&ST_RX_TIMEOUT) printf("RX timeout\n");
if(stat&ST_RSC) printf("Receive Status Change\n");
if(stat&ST_PERR) printf("Parity Error\n");
if(stat&ST_PCE) printf("Protocol Error\n");
if(stat&ST_FERR) printf("Framing Error\n");
if(stat&ST_SYN) printf("SYN detect\n");
if(stat&ST_DPLLA) printf("DPLL asyncronous\n");
if(stat&ST_CDSC) printf("CD changed state\n");
if(stat&ST_RFO) printf("Receive Frame Overflow\n");
if(stat&ST_EOP) printf("End of Poll\n");
if(stat&ST_BRKD) printf("Break Detected\n");
if(stat&ST_ONLP) printf("On Loop\n");
if(stat&ST_BRKT) printf("Break Terminated\n");
if(stat&ST_ALLS) printf("All Sent\n");
if(stat&ST_EXE) printf("Transmit Underrun\n");
if(stat&ST_TIN) printf("Timer Expired\n");
if(stat&ST_CTSC) printf("CTS changed state\n");
if(stat&ST_XMR) printf("Transmit Message Repeat\n");
if(stat&ST_TX_DONE) printf("Transmit Done\n");
if(stat&ST_DMA_TC) printf("DMA TC reached\n");
if(stat&ST_DSR0C) printf("DSR0 changed state\n");
if(stat&ST_DSR1C) printf("DSR1 changed state\n");
}
