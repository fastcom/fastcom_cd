/******************************************************
 *
 * Copyright (C) 2000, 2001 Commtech, Inc. Wichita KS
 *
 * read_test2.c -- example code to read from a port using the 
 *				   select function call
 *
 * qnx 4.25 1/4/01
 ******************************************************/
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <ioctl.h>
#include <sys/qioctl.h>
#include <string.h>
#include <sys/select.h>
#include <errno.h>
#include <time.h>

#include "esccdrv_user.h"

void decode_stat(unsigned long stat);

void main(int argc, char *argv[])
{
int fd;
char data[4096];
char sbuf[255];
char rbuf[10];
unsigned i;
setup settings;
board_settings bset;
clkset clk;
regsingle reg;
unsigned val;
unsigned long status;
unsigned writesize;
FILE *fout;
int ret;
int n;
fd_set rfd;
struct timeval tv;

//open port 0
fd = open("/escc/0",O_RDWR);//|O_NONBLOCK);
if(fd == -1)
{
	perror(NULL);
exit(1);
}

//open file specified on command line to put received data to
fout = fopen(argv[1],"wb");
if(fout==NULL)
{
printf("cannot open output file %s\n",argv[1]);
close(fd);
exit(1);
}

printf("hit any key to stop receiving to %s\n",argv[1]);


while(!kbhit())
{
//printf("top of loop\n");

FD_ZERO(&rfd);
FD_SET(fd,&rfd);

tv.tv_sec = 1;//1 second timeout
tv.tv_usec = 0;

n = select(fd+1,&rfd,0,0,&tv);//issue select call
//printf("tv:%lu, tv:%lu\n",tv.tv_sec,tv.tv_usec);
switch(n)
	{
	case -1:
		perror("select error");
		break;
	case 0:
		printf("read timeout\n");//should timeout once a second while no data is on the line
		break;
	default:
		if(FD_ISSET(fd,&rfd))
		{
//		printf("isset true..start read\n");//when a frame is available to read, the select will finish and we will end up here
		if((ret = read(fd,data,4096))==-1)//get the data from the driver
			{
			if(errno!=EAGAIN)perror(NULL);
			}
		if(ret!=-1)
			{
			//store the data in the file
			fwrite(data,1,ret-1,fout);//last char is status in hdlc mode
			fflush(fout);//force the data to be written to the file (not completely necessary, but is usefull for debugging)
			printf("read returned:%u\n",ret);
			}
		
		}
		else printf("select finished, no data\n");
	}
}
//pull/clear the status from the driver 
if(qnx_ioctl(fd,ESCC_STATUS,NULL,0,&status,sizeof(status))==-1)perror(NULL);
//display the status values
decode_stat(status);

fclose(fout);//done with the user file
close(fd);//done with the port/driver
}


void decode_stat(unsigned long stat)
{
if(stat==0)return;
printf("STATUS DECODE:\n");
if(stat&ST_RX_DONE) printf("Receive Done\n");
if(stat&ST_OVF) printf("RX BUFFERS overflow\n");
if(stat&ST_RFS) printf("Receive Frame Start\n");
if(stat&ST_RX_TIMEOUT) printf("RX timeout\n");
if(stat&ST_RSC) printf("Receive Status Change\n");
if(stat&ST_PERR) printf("Parity Error\n");
if(stat&ST_PCE) printf("Protocol Error\n");
if(stat&ST_FERR) printf("Framing Error\n");
if(stat&ST_SYN) printf("SYN detect\n");
if(stat&ST_DPLLA) printf("DPLL asyncronous\n");
if(stat&ST_CDSC) printf("CD changed state\n");
if(stat&ST_RFO) printf("Receive Frame Overflow\n");
if(stat&ST_EOP) printf("End of Poll\n");
if(stat&ST_BRKD) printf("Break Detected\n");
if(stat&ST_ONLP) printf("On Loop\n");
if(stat&ST_BRKT) printf("Break Terminated\n");
if(stat&ST_ALLS) printf("All Sent\n");
if(stat&ST_EXE) printf("Transmit Underrun\n");
if(stat&ST_TIN) printf("Timer Expired\n");
if(stat&ST_CTSC) printf("CTS changed state\n");
if(stat&ST_XMR) printf("Transmit Message Repeat\n");
if(stat&ST_TX_DONE) printf("Transmit Done\n");
if(stat&ST_DMA_TC) printf("DMA TC reached\n");
if(stat&ST_DSR0C) printf("DSR0 changed state\n");
if(stat&ST_DSR1C) printf("DSR1 changed state\n");
}
