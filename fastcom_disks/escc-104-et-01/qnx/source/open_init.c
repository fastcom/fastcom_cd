/******************************************************
 *
 * Copyright (C) 2000, 2001 Commtech, Inc. Wichita KS
 *
 * open_init.c -- example code to open and configure the escc card/port
 *
 * qnx 4.25 1/4/01
 ******************************************************/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <ioctl.h>
#include <sys/qioctl.h>
#include <string.h>
#include "esccdrv_user.h"

void decode_stat(unsigned long stat);

void main(int argc, char *argv[])
{
int fd;
char data[4096];
char sbuf[255];
char rbuf[10];
unsigned i;
setup settings;
board_settings bset;
clkset clk;
regsingle reg;
unsigned val;
unsigned long status;
unsigned writesize;
FILE *fin;
int ret;
char devname[40];
int channel_number;
unsigned long brate;
int N;

if(argc<2)
{
printf("usage:\n");
printf("open_init port rate\n");
exit(1);
}
channel_number = 0;//defaults to 0
if(argc>1)
{
sprintf(devname,"/escc/%u",atoi(argv[1]));
channel_number = atoi(argv[1]);//only works for single board install...ie channel # 2 doesn't make any sense.
}
else sprintf(devname,"/escc/0");

brate = 16000;//default to min rate
if(argc>2)
{
brate = atol(argv[2]);
}
if(brate == 0) brate =16000;//force min
if(brate > 2000000) brate = 2000000;//force max
if((brate > 1000000)&&(brate != 2000000 ))
	{
	printf("bitrates between 1E6 and 2E6 not supported \n");
	printf("in this clock mode (6b)\n");
	printf("try using clock mode 0b or 7b\n");
	}
/*
for clock mode 6b the bitrate is determined as:

  rate = (inclk/(N+1)*2)/16
  inclk = 32E6 so:
  rate = (32E6/(N+1)*2)/16
  N = ((32E6/(rate*16))/2)-1

*/

N = ((32000000/(brate*16))/2)-1;

if(N>1023)
{
printf("N > 1023; bitrate not supported\n");
exit(1);
}

printf("device:%s\n",devname);
fd = open(devname,O_RDWR);//|O_NONBLOCK);
if(fd == -1) perror(NULL);

//function to set the address/irq that the board is configured to
//it lets the driver know where to access the card
bset.base = 0x280; //base address switch on the board
bset.irq = 10; //interrupt switch on the board
bset.channel = channel_number; //channel number (port) 0 or 1
bset.dmar = 0;//not used
bset.dmat = 0;//not used
if(qnx_ioctl(fd,ESCC_ADD_BOARD,&bset,sizeof(board_settings),rbuf,sizeof(rbuf))==-1)perror(NULL);

//this is the function that configures the individual port registers
//on the 82532, it also sets up the buffering parameters.
memset(&settings,0,sizeof(setup));
settings.ccr0 = 0x80;
settings.ccr1 = 0x16;//(0x17)
if(brate>1000000)
{
settings.ccr2 = 0x18;//bdf = 0 rate = (inclk/1)/16 = 2E6
printf("using bitrate = 2000000 bps\n");
}
else
{
settings.ccr2 = 0x38;
printf("using bitrate = %lu bps\n", (32000000/((N+1)*2))/16);
if(N>255)
	{
	settings.ccr2 |= ( ( (N >> 8) << 6 ) & 0xc0 );
	}
}
settings.ccr3 = 0x00;
settings.ccr4 = 0x00;
settings.bgr = N & 0xff;
settings.mode = 0x88;
settings.n_rbufs = 10;
settings.n_tbufs = 10;
settings.n_rfsize_max = 4096;
settings.n_tfsize_max = 4096;

settings.timr = 0x1f;
settings.xbcl = 0x00;
settings.xbch = 0x00;
settings.iva = 0;
settings.ipc = 0x03;
settings.imr0 = 0x44;//disable receive frame start and CDSC interrupts
settings.imr1 = 0x0;
settings.pvr = 0x0;
settings.pim = 0xff;
settings.pcr = 0xe0;
settings.xad1 = 0xff;
settings.xad2 = 0xff;
settings.rah1 = 0xff;
settings.rah2 = 0xff;
settings.ral1 = 0xff;
settings.ral2 = 0xff;
settings.rlcr = 0x00;
settings.pre = 0x00;

printf("settings:%u\n",sizeof(setup));
if(qnx_ioctl(fd,ESCC_SETUP,&settings,sizeof(setup),rbuf,sizeof(rbuf))==-1)perror(NULL);

/*
Hold this fd open with this configuration until the user is done 
with the port (they hit a key)
*/
printf("Hit any key to close\n");
getch();

close(fd);
}


void decode_stat(unsigned long stat)
{
if(stat==0)return;
printf("STATUS DECODE:\n");
if(stat&ST_RX_DONE) printf("Receive Done\n");
if(stat&ST_OVF) printf("RX BUFFERS overflow\n");
if(stat&ST_RFS) printf("Receive Frame Start\n");
if(stat&ST_RX_TIMEOUT) printf("RX timeout\n");
if(stat&ST_RSC) printf("Receive Status Change\n");
if(stat&ST_PERR) printf("Parity Error\n");
if(stat&ST_PCE) printf("Protocol Error\n");
if(stat&ST_FERR) printf("Framing Error\n");
if(stat&ST_SYN) printf("SYN detect\n");
if(stat&ST_DPLLA) printf("DPLL asyncronous\n");
if(stat&ST_CDSC) printf("CD changed state\n");
if(stat&ST_RFO) printf("Receive Frame Overflow\n");
if(stat&ST_EOP) printf("End of Poll\n");
if(stat&ST_BRKD) printf("Break Detected\n");
if(stat&ST_ONLP) printf("On Loop\n");
if(stat&ST_BRKT) printf("Break Terminated\n");
if(stat&ST_ALLS) printf("All Sent\n");
if(stat&ST_EXE) printf("Transmit Underrun\n");
if(stat&ST_TIN) printf("Timer Expired\n");
if(stat&ST_CTSC) printf("CTS changed state\n");
if(stat&ST_XMR) printf("Transmit Message Repeat\n");
if(stat&ST_TX_DONE) printf("Transmit Done\n");
if(stat&ST_DMA_TC) printf("DMA TC reached\n");
if(stat&ST_DSR0C) printf("DSR0 changed state\n");
if(stat&ST_DSR1C) printf("DSR1 changed state\n");
	}
