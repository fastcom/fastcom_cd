The software in this directory is for the Fastcom:ESCC-104-01, and will compile/run on QNX 4.25 only.

To build the driver file:

cd \drivers
chmod 777 ccescc
./ccescc

then copy the resulting "esccdrv" file to the desired location.

Execute the driver as:

./esccdrv &

Then execute the equivalent code in open_init.c which is:

open the file "/escc/x" where X is a number (0 or 1) signifying the port to open.

execute qnx_ioctl(fd,ESCC_ADD_BOARD,...) to tell the driver where the card is, (sets the IO address and IRQ that the driver will use).

execute qnx_ioctl(fd,ESCC_SETUP,...) to initialize the SAB82532 registers to the desired settings.

execute read() or write() function calls to receive or send data frames.

finally close the handle to the open file and exit(when finished reading/writing)

To run a loopback test (assuming that you have a Fastcom:ESCC-104-01 card) you would execute:

./esccdrv &
./open_init 0 1000000

(open a second console window by pressing ctrl-alt-2)
in that second console window execute:

./looptest

The Fastcom:ESCC-104-01 does not have a clock generator on it, but rather has a fixed frequency 32MHz clock.