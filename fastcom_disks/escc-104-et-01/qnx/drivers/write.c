/******************************************************
 *
 * Copyright (C) 2000 Commtech, Inc. Wichita KS
 *
 * write.c -- IO_WRITE code for escc-isa module
 *
 * qnx 4.25 12/8/00
 ******************************************************/

#include "esccdrv.h"

void our_write(pid_t pid, io_msg *msg, Escc_Dev *dev)
{
unsigned i;
unsigned long flags;
unsigned chanstor;
unsigned port;
struct buf *tbuf;
unsigned j;
unsigned mode;

//do write here
port = dev->base; //required for all in/out instructions
			
if(dev->port_initialized)
	 {
	 //check to make sure it will fit in the txbuffer
	 if(msg->write.nbytes > dev->settings.n_tfsize_max)
		{
#ifdef ENABLE_DEBUG_MESSAGES
		sprintf(errbuf,"ERROR write larger than max tx buffer size, bytes:%u, max:%u",msg->write.nbytes,dev->settings.n_tfsize_max);
		DBGMSG(errbuf);
#endif
		msg->write_reply.status = EFBIG;
		msg->write_reply.nbytes = 0;
		Reply(pid,msg,sizeof(msg->write_reply));
		goto write_done;
		}

	if(atomic_read(dev->transmit_frames_available) == 0)
		{
		if(dev->nonblocking==1)
			{
#ifdef ENABLE_DEBUG_MESSAGES
			DBGMSG("Write would block");
#endif
			msg->write_reply.status = EAGAIN;
			msg->write_reply.nbytes = 0;
			Reply(pid,msg,sizeof(msg->write_reply));
			//if nonblocking then dump
			}
		else
			{
			//blocking write so prep for isr to signal our proxy that will return us
			//to the write function with an available transmit buffer to stuff
			dev->w_pid = pid;//save pid for return from block;
			dev->w_size = msg->write.nbytes;
			dev->write_pending=1;
			}
		//  PDEBUG("write sleeping \n");//otherwise block on a free buffer
		}//end of while not transmit_buffers_available
	else
		{
//		tx_wait_return_path:
		//here we have at least 1 transmit buffer, so fill it up and possibly start 
		//the interrupt tx chain

		//find the free txbuffer
		//note it must be done while isr is disabled
		flags = pswget();
		//save_flags(flags);
		disable();
		//cli();
		i = dev->current_txbuf;
		do
			{
			tbuf = &dev->escc_tbuf[i];
			if(tbuf->valid==0)
				{
				 //restore_flags(flags); 
				restore(flags);
				atomic_dec(dev->transmit_frames_available);//ISR increments this as it finishes sending frames, we decrement it as we queue them 
				if(Readmsg(pid,sizeof(msg->write)-1,tbuf->frame,msg->write.nbytes)==-1)
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR getting write data from user message");
#endif
					msg->write_reply.status = EINVAL;
					Reply(pid,msg,sizeof(msg->write_reply));
					goto write_done;
					}
				tbuf->max = msg->write.nbytes;
				tbuf->valid = 1;
				tbuf->no_bytes = 0;
				//check to see if we need to start the send.
				if(dev->is_transmitting==0)
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("is transmitting is 0, (tx is idle)\n");
#endif
					//is_transmitting flag is 0 so we are not currently transmitting, and there
					//are not going to be any more scheduled interrupts of the transmit varity 
					//until we issue a transmit command, so lets start-er up.

					//save channel

					//this is necessary due to the fact that the isa ESCC has
					//overlapping address space, it was a design constraint made
					//long ago, please forgive them for they know not what they do.
					//I really wish now (some years later) that I would have just
					//mapped it flat and said to hell with the fact that it eats 128 bytes 
					//of i/o space.
					//note that this should probably be done with interrupts disabled,
					//or atleast a spinlock attached to each board(2 ports), because
					//if we get diverted between the channel save and restore (other
					//than by the ISR which will save this context) then all 
					//bets are off as to if it will work anymore.
					if(sem_wait(&boardlock[dev->board])==-1)//down(&boardlock[dev->board]);
						{
#ifdef ENABLE_DEBUG_MESSAGES
						DBGMSG("ERROR boardlock failed");
						perror(NULL);
#endif
						msg->write_reply.status = EAGAIN;
						Reply(pid,msg,sizeof(msg->write_reply));
						goto write_done;
						}

					chanstor = inb(PVR);
					outb(PVR,(chanstor&0xFE)+dev->channel);

					dev->is_transmitting =1; //we are transmitting now
					//there are two conditions to deal with, either we have more than
					//32 bytes to send in which case we fill the hardware fifo on the82532 and
					//start the frame sending, or we have less than 32 bytes and we can
					//complete the send here and now
 
					if(tbuf->max <=32) //the complete now case
						{
					    atomic_inc(dev->transmit_frames_available);
						tbuf->valid = 0;//we will be done with this frame here
						outsb(tbuf->frame,tbuf->max,FIFO);//pump the data to the fifo
						tbuf->no_bytes = tbuf->max;//a formality, not needed
						//figure out the tx command depending on operating mode
						mode = dev->settings.ccr0&0x03;
						if(mode <= 2)//hdlc or bisync (or SDLC Loop) 
							{
							if((dev->settings.mode&0xc0)==0xc0) j = dev->tx_type;//extended transparent hdlc mode
							else j = dev->tx_type+XME;//hdlc/sdlc/bisync need closing xme to finish the frame
							}
						else j = 0x08;//async transmit command is allways 0x08
						}//end of <=32 block
					else
						{
						//number of bytes >32 so send first 32 to get XPR interrupt chain going
						outsw(tbuf->frame,16,FIFO); //write 32 bytes
						tbuf->no_bytes += 32;
						//figure out the tx command
						mode = dev->settings.ccr0&0x03;
						if(mode <= 2)//hdlc or bisync (or SDLC Loop) 
							{
							j = dev->tx_type;//extended transparent hdlc mode
							}
					   else j = 0x08;//async transmit command is allways 0x08
					   }//end of >32 block

					WAIT_WITH_TIMEOUT;//wait for CEC clear
					outb(CMDR,j);//start the send, will immed cause XPR
					//restore channel
					outb(PVR,chanstor); 
					if(sem_post(&boardlock[dev->board])==-1)//up(&boardlock[dev->board]);
						{
#ifdef ENABLE_DEBUG_MESSAGES
						DBGMSG("ERROR boardunlock failed");
						perror(NULL);
#endif
						}
					}//end of is_transmitting check
				msg->write_reply.status = EOK;
				msg->write_reply.nbytes = tbuf->max;
				msg->write_reply.zero = 0;
				Reply(pid,msg,sizeof(msg->write_reply));
#ifdef ENABLE_DEBUG_MESSAGES
				sprintf(errbuf,"finshed write:%u",tbuf->max);
				DBGMSG(errbuf);
#endif
				goto write_done;
				 }//end of if valid==0;
			i++;
			if(i==dev->settings.n_tbufs) i = 0;//rollover
			}while(i!=dev->current_txbuf);
		//restore_flags(flags);
		restore(flags);
		//should not get here as we had indication of a free tbuf
#ifdef ENABLE_DEBUG_MESSAGES
		DBGMSG("past while in write with transmit_frames_available >0\n");
#endif
		msg->write_reply.status = EAGAIN;
		msg->write_reply.nbytes = 0;
		Reply(pid,msg,sizeof(msg->write_reply));
		}//end of txbuffersavail>0
	}//end of if port_initialized
else 
	{
#ifdef ENABLE_DEBUG_MESSAGES
	DBGMSG("ERROR write with port not initialized");
#endif
	msg->write_reply.status = EBUSY;
	msg->write_reply.nbytes = 0;
	Reply(pid,msg,sizeof(msg->write_reply));
	}
write_done:
return;
}