/******************************************************
 * Copyright (C) 2000 Commtech, Inc. Wichita KS
 *
 * isr.c -- interrupt service routine for escc-isa module
 *
 * qnx 4.25 12/8/00
 ******************************************************/

#include "esccdrv.h"        /* local definitions */
extern volatile unsigned long ihit;
pid_t far escc_irq(void)
{

Escc_Dev *dev;
struct buf *rbuf;
struct buf *tbuf;
//unsigned i;
unsigned j = 0;
unsigned port;
unsigned chanstor;
unsigned gis;
unsigned isr0;
unsigned isr1;
unsigned pis;
unsigned opmode;
unsigned extendedtransparent;
unsigned i;
unsigned return_proxy;
unsigned irqhit;
ihit++;
return_proxy = 0;

do
{
irqhit=0;
for(i=0;i<escc_nr_devs;i++)
	{
	dev = &escc_devices[i];
	port = dev->base;
	if(port==0) goto service_next_port; //if not defined then it doesn't make much sense
									//to continue as most likely the rest of the 
									//structures are meaningless

	if(dev->port_initialized!=1)
	{
	//if port is not initialized then it would be a bad idea to let the isr
	//have at it, as there are no buffers allocated yet.
	outb(IMR0,0xff);//if this was the port that got us here this will prevent
	outb(IMR1,0xff);//it from happening again 
	outb(PIM,0xff);//at least until it is initialized
	goto service_next_port;
	}
   

rbuf = &dev->escc_rbuf[dev->current_rxbuf];
tbuf = &dev->escc_tbuf[dev->current_txbuf];

opmode = dev->settings.ccr0&0x3; //0 = HDLC, 1 = SDLCloop, 2 = BISYNC, 3 = ASYNC

if( (opmode == OPMODE_HDLC) && ((dev->settings.mode&0xc0)==0xc0) ) extendedtransparent = TRUE;
else extendedtransparent = FALSE;

//printf("irq\n");


gis = inb(GIS);
if(gis!=0)
 {
 //ok it is either us or the other channel attached to this base address,
 //check ours
 //save current channel
 chanstor = inb(PVR);
 outb(PVR,(chanstor&0xfe)+dev->channel);
//get interrupt status(s)
 isr0 = inb(ISR0);
 isr1 = inb(ISR1);
 pis =  inb(PIS);

 //cant return here...go to next port
 if((isr0+isr1+pis)==0) goto service_next_port;// return 0;//must be other channel, so let them have at it
	irqhit = 1;
 //it is ours so here we go.
 //do
  //{
   //we are inited here, so we have buffer space, service the interrupt
   if((isr0&RPF)==RPF)
    {
	dev->rpf++;
       if(opmode == OPMODE_HDLC)
	{
	if( (rbuf->no_bytes+32) > dev->settings.n_rfsize_max)
	 {
	 //received bytes would overfill buffer
	 if(extendedtransparent)
	  {
	  //could be normal for extended transparent mode so close up the 
	  //buffer and return it
	  rbuf->valid = 1;
	  atomic_inc(dev->received_frames_pending);
	  dev->status |= ST_RX_DONE;
	  dev->current_rxbuf++;
	  if(dev->current_rxbuf==dev->settings.n_rbufs) dev->current_rxbuf = 0;
          rbuf = &dev->escc_rbuf[dev->current_rxbuf];
          if(rbuf->valid ==1)
	   {
           dev->status |= ST_OVF;//
			//if we don't do this then reads will start failing
			//(the pending counter will be larger than the number
			//of frames buffered, so when all the frames
			//are read the read routine will break at the end
			//thinking that there are more frames, but never
			//decrementing the counter because it cant find them)
			//this is true for all overflow conditions following
		   atomic_dec(dev->received_frames_pending);
           
 	   }//end of rbuf->valid == 1
	  rbuf->no_bytes = 0;
	  rbuf->valid = 0;
	  insw(&rbuf->frame[rbuf->no_bytes],16,FIFO);
	  WAIT_WITH_TIMEOUT;
	  outb(CMDR,RMC);
	  rbuf->no_bytes += 32;
       	  //wake_up_interruptible(&dev->rq);	
//		  Trigger(dev->rq);
	  dev->rq = 1;
	  return_proxy = 1;
          }//end of extended transparent
	else
	 {
	 //we don't have enough room in the buffer to hold the incomming
	 //bytes, so 
	 //is an overflow condition so 
 	 //we just release the fifo without reading it (data is lost here)	 
         dev->status |= ST_RFO;
	 WAIT_WITH_TIMEOUT;
	 outb(CMDR,RMC);
 	 }//end of else (not extended transparent mode)
        }//end of bytes > max
       else
	{
	//bytes will fit in buffer so go get em
	  insw(&rbuf->frame[rbuf->no_bytes],16,FIFO);//pull the 32 bytes
	  WAIT_WITH_TIMEOUT;//wait for command executing
	  outb(CMDR,RMC);//release fifo
	  rbuf->no_bytes += 32;//inc byte count
        	
	}//end of else block -- (bytes <max)
       }//end of if HDLC
      if((opmode == OPMODE_ASYNC)||(opmode == OPMODE_BISYNC))
	{
	j = inb(RBCL);
	j = j&0x1f;
	if(j==0) j = 32;

	if(rbuf->no_bytes+j>dev->settings.n_rfsize_max)
		{
		rbuf->valid = 1;
		atomic_inc(dev->received_frames_pending);
      		dev->status |= ST_RX_DONE;
		dev->current_rxbuf++;
		if(dev->current_rxbuf == dev->settings.n_rbufs) dev->current_rxbuf = 0;
		rbuf = &dev->escc_rbuf[dev->current_rxbuf];
		if(rbuf->valid ==1)
		 {
		 dev->status|= ST_OVF;
	     atomic_dec(dev->received_frames_pending);
 	 	 }//end of if rbuf->valid ==1
		rbuf->valid = 0;
		rbuf->no_bytes = 0;
//         	Trigger(dev->rq);//wake_up_interruptible(&dev->rq);	
		  dev->rq = 1;
		  return_proxy = 1;

		}


	insb(&rbuf->frame[rbuf->no_bytes],j,FIFO);
	WAIT_WITH_TIMEOUT;
	outb(CMDR,RMC);
	rbuf->no_bytes += j;

	}//end of if async or bisync
	//handler for SDLC_LOOP noticably missing...possibly the same as HDLC?
    }//end of RPF

   if((isr0&RME)==RME)
    {
	dev->rme++;
//PDEBUG("RME\n");
if(opmode==OPMODE_HDLC)
	{
	j = ( (inb(RBCH)&0x0f) <<8 )+(inb(RBCL)&0xff);
//PDEBUG("j=%u\n",j);
	}//end of HDLC BLOCK
if( (opmode == OPMODE_ASYNC) || (opmode==OPMODE_BISYNC) )
	{
	//TCD interrupt
	j = inb(RBCL);
	j = j & 0x1f;
	if(j==0) j = 32;
	j = j + rbuf->no_bytes; //(to make compatible with code below)
	}

if(j > dev->settings.n_rfsize_max)
	{
	dev->status |= ST_RFO;//data will be lost
	j = dev->settings.n_rfsize_max;
//PDEBUG("j>max\n");
	}//endof if j out of bounds
if(j < rbuf->no_bytes)
	{
	dev->status |= RFO; 
//PDEBUG("j<nobytes\n");
	}
else
	{
	insb(&rbuf->frame[rbuf->no_bytes],j - rbuf->no_bytes,FIFO);
	rbuf->no_bytes = j;
	WAIT_WITH_TIMEOUT;
	outb(CMDR,RMC);
	rbuf->valid = 1;
	atomic_inc(dev->received_frames_pending);
	dev->status |= ST_RX_DONE;
	dev->current_rxbuf++;
	if(dev->current_rxbuf == dev->settings.n_rbufs) dev->current_rxbuf = 0;
	rbuf = &dev->escc_rbuf[dev->current_rxbuf];
	if(rbuf->valid ==1)
	 {
	 dev->status |= ST_OVF;
     atomic_dec(dev->received_frames_pending);
  	 }
	 rbuf->valid = 0;
	 rbuf->no_bytes = 0;
//         Trigger(dev->rq);//wake_up_interruptible(&dev->rq);
	  dev->rq = 1;
	  return_proxy = 1;

	 //PDEBUG("wakeup\n");
	 }//end of else block (j in bounds)
        
if(opmode == OPMODE_BISYNC)
	{
	WAIT_WITH_TIMEOUT;
	outb(CMDR,HUNT);
	}
    }//end of RME
   if((isr0&RFS)==RFS)
    {
    if(opmode == OPMODE_HDLC) dev->status |= ST_RFS;//receive frame start interrupt
    if(opmode == OPMODE_ASYNC)
	{
	//TIMEOUT interrupt
	if( (inb(STAR)&0x20) == 0x20)
	 {
	 WAIT_WITH_TIMEOUT;
	 outb(CMDR,0x20);//release fifo bytes(will force TCD int)
	 }//end of if bytes in fifo
	else
	 {
	 //no bytes in fifo, so just finish frame
	 if(rbuf->no_bytes > 0)
	  {
	  rbuf->valid = 1;
	  atomic_inc(dev->received_frames_pending);
	  dev->status |= ST_RX_DONE;
	  dev->current_rxbuf++;
	  if(dev->current_rxbuf == dev->settings.n_rbufs) dev->current_rxbuf = 0;
	  rbuf = &dev->escc_rbuf[dev->current_rxbuf];
	  if(rbuf->valid ==1)
		{
		dev->status |= ST_OVF;
	    atomic_dec(dev->received_frames_pending);
		}
	   rbuf->valid = 0;	
	   rbuf->no_bytes =0;
//	   Trigger(dev->rq);//wake_up_interruptible(&dev->rq);
	  dev->rq = 1;
	  return_proxy = 1;

	   }//end of rbuf->nobytes>0
	  }//end of else block (no bytes in fifo)
	}//end of opmode ASYNC

	//should not get here in BISYNC mode
   }//end of RFS

   if((isr0&RSC)==RSC)
    {//this could be simplified as the constant is the same for all...
	if(opmode == OPMODE_HDLC) dev->status |= ST_RSC;
	if(opmode == OPMODE_ASYNC) dev->status |= ST_PERR;
	if(opmode == OPMODE_BISYNC) dev->status |= ST_PERR;
   }//end of RSC

   if((isr0&PCE)==PCE)
    {//this could be simplified as the constant is the same for all...
	if(opmode == OPMODE_HDLC) dev->status |= ST_PCE;
	if(opmode == OPMODE_ASYNC) dev->status |= ST_FERR;
	if(opmode == OPMODE_BISYNC) dev->status |= ST_SYN;	
    }//end of PCE
  
  if((isr0&PLLA)==PLLA)
    {
	dev->status |= ST_DPLLA;

    }//end of PLLA
 
  if((isr0&CDSC)==CDSC)
    {
 	dev->status |= ST_CDSC;

    }//end of CDSC
 
  if((isr0&RFO)==RFO)
    {
	dev->status |= ST_RFO;
	WAIT_WITH_TIMEOUT;
	outb(CMDR,RHR);
    }//end of RFO

   if((isr1&XPR)==XPR)
    {
	dev->xpr++;
	if(tbuf->valid == 1)
	{
	 if((tbuf->max - tbuf->no_bytes) > 32)	
	  {
	  //send 32 bytes
	  outsw(&tbuf->frame[tbuf->no_bytes],16,FIFO);
	  WAIT_WITH_TIMEOUT;
	  outb(CMDR,dev->tx_type);
	  tbuf->no_bytes+=32;
	  dev->is_transmitting =1;

	  }//end of send 32
	else
	  {
	  //send < 32 (close frame)
	  outsb(&tbuf->frame[tbuf->no_bytes],tbuf->max - tbuf->no_bytes,FIFO);
	  WAIT_WITH_TIMEOUT;
	  if(opmode==OPMODE_HDLC)
		{
	  	if(extendedtransparent) outb(CMDR,dev->tx_type);
	  	else  outb(CMDR,dev->tx_type+XME); 
	  	}
	  if(opmode==OPMODE_ASYNC) outb(CMDR,0x08);
  	  if(opmode==OPMODE_BISYNC) outb(CMDR,dev->tx_type);
	  tbuf->no_bytes = tbuf->max;//formality, not really necessary
	  tbuf->valid = 0;//free it up for later
	  atomic_inc(dev->transmit_frames_available);
	  dev->current_txbuf++;
	  if(dev->current_txbuf == dev->settings.n_tbufs) dev->current_txbuf = 0;
	  dev->status |= ST_TX_DONE;	
//	  Trigger(dev->wq);//wake_up_interruptible(&dev->wq); 
	  dev->wq = 1;
	  return_proxy = 1;

	  }//end of send <32
	}//end of if tbuf->valid ==1
  	else
	 {
	 dev->is_transmitting = 0;//done transmitting (tbuf not valid at interrupt time)
	 dev->status |= ST_TX_DONE;
	 }//end of tbuf->valid = 0 (else block)
    }//end of XPR  
   if((isr1&EOP)==EOP)
    {
	if(opmode == OPMODE_HDLC) dev->status |= ST_EOP;
	if(opmode == OPMODE_ASYNC) dev->status |= ST_BRKD;
	//no function in bisync
    	
    }//end of EOP

   if((isr1&RDO)==RDO)
    {
	if(opmode == OPMODE_HDLC) dev->status |= ST_ONLP;
	if(opmode == OPMODE_ASYNC) dev->status |= ST_BRKT;
	//no function in bisync	
    }//end of RDO

   if((isr1&ALLS)==ALLS)
    {
	dev->status |= ST_ALLS;
//uncomment the next line if you are running in 485 mode and do not wish to
//receive what you send
//turn the receiver back on
#ifdef RS485_RX_ECHO_CANCEL
outb(MODE,dev->settings.mode|0x08);
#endif
#ifdef FORCE_485_RTS_CONTROL
outb(MODE,dev->settings.mode&0xFB);
#endif

    }//end of ALLS
   if((isr1&EXE)==EXE)
    {
	dev->status |= ST_EXE;
// these are necessary to continue, but let user do it with a flush call
//	WAIT_WITH_TIMEOUT;
//	outb(CMDR,XRES);
    }//end of EXE

   if((isr1&TIN)==TIN)
    {
	dev->status |= ST_TIN;
//	outb(TIMR,inb(TIMR));
//a write to the timer register will stop the timer, so a read write will
//stop further timer interrupts until the timer is started again.
//under most timer usages this is what you want, however there are those
//that use the timer to generate a periodic interrupt as to send a fairly
//precise timed frame send, in which case you want the interrupts to keep 
//occuring. either way doesn't matter to me.  Uncomment it if you only want
//one timer interrupt per start timer command.


    }//end of TIN

   if((isr1&CSC)==CSC)
    {
	dev->status |= ST_CTSC;
    }//end of CSC

   if((isr1&XMR)==XMR)
    {
	if(opmode == OPMODE_HDLC) dev->status |= ST_XMR;
	if(opmode == OPMODE_BISYNC) dev->status |= ST_XMR;
	//shouldn't happnen in async
    }//end of XMR
   
   if((pis&0x80)==0x80)
    {
	dev->status |= ST_DMA_TC;
//really should /could use this for early detection of end of dma transfers if need be
    }//end of DMA TC reached

   if((pis&0x40)==0x40)
    {
	dev->status |= ST_DSR1C;

    }//end of DSR1 changed state

   if((pis&0x20)==0x20)
    {
	dev->status |= ST_DSR0C;
    }//end of DSR0 changed state
   



 //isr0 = inb(ISR0);
 //isr1 = inb(ISR1);
 //pis =  inb(PIS);

//  }while((isr0+isr1+pis)!=0);  
 //restore channel
 outb(PVR,chanstor);
if((dev->status!=0)&&(dev->status_pending==1))// Trigger(dev->sq);//wake_up_interruptible(&dev->sq);
	{
	dev->sq = 1;
	return_proxy = 1;
	}
if((dev->status!=0)&&(dev->sselect_pending==1))// Trigger(dev->sq);//wake_up_interruptible(&dev->sq);
	{
	dev->sq = 1;
	return_proxy = 1;
	}

 }//end gis!=0 block
//cant be us because GIS==0
service_next_port:;
}//end for(i=0;i<escc_nr_devs;i++) block

}while(irqhit==1);

if(return_proxy ==1) return(isr_proxy);
else return(0);
}


