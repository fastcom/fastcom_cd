/******************************************************
 *
 * Copyright (C) 2000 Commtech, Inc. Wichita KS
 *
 * read.c -- IO_READ code for escc-isa module
 *
 * qnx 4.25 12/8/00
 ******************************************************/

#include "esccdrv.h"

void our_read(pid_t pid, io_msg *msg, Escc_Dev *dev)
{
unsigned i;
unsigned long flags;
struct buf *rbuf;
struct _mxfer_entry mx[2];

//do read here...
if(dev->port_initialized) //if the port isn't inited then skip the read
	 {
	 if(atomic_read(dev->received_frames_pending) == 0) //block if there are no frames
		{
		if(dev->nonblocking==1)
			{
			//if nonblocking==1 then we return if there are no frames to be had
#ifdef ENABLE_DEBUG_MESSAGES
			DBGMSG("Read would block");
#endif
			msg->read_reply.status = EAGAIN;
			msg->read_reply.nbytes = 0;
			Reply(pid,msg,sizeof(msg->read_reply));
			}
		else
			{
			//nonblocking =0 so block (actually just setup the info we need 
			//the next time round after the isr gets a frame to return,
			//such that the proxy can setup the info and we can finish the read)
			dev->r_pid = pid;
			dev->r_size = msg->read.nbytes;
			dev->read_pending=1;
			}
		}//end of while not pending frames
	else
		{
//	rx_wait_return_path:
	//if we get here we have at least one frame to return so return it
	//we have to do this with interrupts disabled so that our ISR doesn't update
	//the dev->current_rxbuf value while we are using it
	flags = pswget();//save_flags(flags);
	disable();//cli()
	i = dev->current_rxbuf; //get the current buffer
	do
	 {
	 i++; //start at current +1
	 if(i==dev->settings.n_rbufs) i = 0; //rollover
	 rbuf = &dev->escc_rbuf[i]; //get pointer to this buffer
	 if(rbuf->valid ==1) //it is assumed that 1 means the data in the buffer is valid
	  {
	  restore(flags);// restore_flags(flags); //we are done with dev->current_rbuf here, so we can let IRQ's fly
	  if(rbuf->no_bytes > msg->read.nbytes) 
	   {
#ifdef ENABLE_DEBUG_MESSAGES
		sprintf(errbuf,"ERROR recieved frame is larger than read size, rxbytes:%u, size:%u",rbuf->no_bytes, msg->read.nbytes);
		DBGMSG(errbuf);
#endif
		msg->read_reply.status = EFBIG;
		msg->read_reply.nbytes = 0;
		Reply(pid,msg,sizeof(msg->read_reply));
		goto read_done;
		
	//I realize that the book says that you can return less than count bytes
	//but it really does not make sense to break up a frame into multiple reads for
	//any of the operating modes execpt async.  And if you want to re-write it
	//to make async return partial frames then so be it.
	// if their buffer isn't big enough pump back an error...
	//would really like a different one, but haven't figured errno's out quite yet
	   }

	msg->read_reply.status = EOK;
	msg->read_reply.nbytes =  rbuf->no_bytes;
	msg->read_reply.data[0] = rbuf->frame[0];
	_setmx(mx+0,msg,sizeof(msg->read_reply));
	_setmx(mx+1,&rbuf->frame[1],rbuf->no_bytes > 0 ? rbuf->no_bytes-1 : 0);
	Replymx(pid,2,mx);

	rbuf->valid = 0;//make buffer free for later
	rbuf->no_bytes = 0;//clear for next use
	atomic_dec(dev->received_frames_pending);//decrement pending frame count
	goto read_done;
	  }//end of if rbuf->valid

	 }while(i!=dev->current_rxbuf); //keep looping until we get back to the current one
	restore(flags);//restore_flags(flags);//if we break from above we need to re-enable IRQ's
	//really should not get here
#ifdef ENABLE_DEBUG_MESSAGES
	DBGMSG("past while loop in read after received_frames_pending >0");
#endif
	msg->read_reply.status = EAGAIN;
	msg->read_reply.nbytes =  0;
	Reply(pid,msg,sizeof(msg->read_reply));
	goto read_done;
	}//end of frames pending >0
	}//end of port initialized
else 
	{
#ifdef ENABLE_DEBUG_MESSAGES
	DBGMSG("Read with port not initialized");
#endif
	msg->read_reply.status = EBUSY;
	msg->read_reply.nbytes =  0;
	Reply(pid,msg,sizeof(msg->read_reply));
	}

read_done:
return;
}