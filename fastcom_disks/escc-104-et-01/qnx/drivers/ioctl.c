/******************************************************
 *
 * Copyright (C) 2000 Commtech, Inc. Wichita KS
 *
 * ioctl.c -- IO_QIOCTL functions for escc-isa module
 *
 * qnx 4.25 12/8/00
 ******************************************************/

#include "esccdrv.h"

void our_ioctl(pid_t pid, io_msg *msg, Escc_Dev *dev)
{
unsigned long timeoutvalue;
unsigned long flags;
unsigned long status;
unsigned chanstor;
unsigned port;
board_settings board_switches;
regsingle regs;
clkset clock;
unsigned long passval;
char *tempbuf;
struct _mxfer_entry mx[2];
unsigned i;

port = dev->base; //required for access using 82532 register defines in esccdrv.h

//do qioctl here
			switch (msg->qioctl.request)
				{
			case ESCC_SETUP:
#ifdef ENABLE_DEBUG_MESSAGES
DBGMSG("in ioctl-setup");
#endif
				if(msg->qioctl.nbytes != sizeof(setup))
					{
#ifdef ENABLE_DEBUG_MESSAGES
					sprintf(errbuf,"ERROR invalid buffer size, input size:%u, expected:%u",msg->qioctl.nbytes,sizeof(setup));
					DBGMSG(errbuf);
#endif
					msg->qioctl_reply.status = EINVAL;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
//DBGMSG("past in size check");
				if(port==0)
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("Port not setup, no base address");
#endif
					msg->qioctl_reply.status = ENODEV;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
//DBGMSG("past port valid check");

				//get setup struct from user space (passed in)
				if(Readmsg(pid,sizeof(msg->qioctl)-1,&dev->settings,sizeof(setup))==-1)
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR reading setup struct from user message");
#endif
					msg->qioctl_reply.status = EINVAL;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
//DBGMSG("past read setup struct from user");
				dev->tx_type = XTF;//import this from somewhere...default to XTF's
				//lock our board for register access to it
				if(sem_wait(&boardlock[dev->board])==-1)//down(&boardlock[dev->board]);
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR boardlock failed");
					perror(NULL);
#endif
					msg->qioctl_reply.status = EAGAIN;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
//DBGMSG("past boardlock");
				//save channel
				chanstor = inb(PVR);
				outb(PVR,(inb(PVR)&0xfe)+dev->channel);
				//mask interrupts at device
				outb(IMR0,0xff);	
				outb(IMR1,0xff);
//DBGMSG("past channelset/irq masking");
				if((dev->settings.ccr0 & 0x03)==0)
					{
#ifdef ENABLE_DEBUG_MESSAGES
DBGMSG("starting HDLC SETTINGS");
#endif
					//init HDLC
					outb(CCR0,dev->settings.ccr0);
					outb(CCR1,dev->settings.ccr1);
					outb(CCR2,dev->settings.ccr2);
					outb(CCR3,dev->settings.ccr3);
					outb(CCR4,dev->settings.ccr4);
					outb(MODE,dev->settings.mode);
					outb(TIMR,dev->settings.timr);
					outb(XAD1,dev->settings.xad1);
					outb(XAD2,dev->settings.xad2);
					outb(RAH1,dev->settings.rah1);
					outb(RAH2,dev->settings.rah2);
					outb(RAL1,dev->settings.ral1);
					outb(RAL2,dev->settings.ral2);
					outb(XBCL,dev->settings.xbcl);
					outb(XBCH,dev->settings.xbch);
					outb(BGR,dev->settings.bgr);
					outb(RLCR,dev->settings.rlcr);
					outb(PRE,dev->settings.pre);
					outb(PVR,(dev->settings.pvr&0xfe)+dev->channel);
					outb(TSAX,dev->settings.tsax);
					outb(TSAR,dev->settings.tsar);
					outb(XCCR,dev->settings.xccr);
					outb(RCCR,dev->settings.rccr);
					outb(AMH,dev->settings.amh);
					outb(AML,dev->settings.aml);
//DBGMSG("finished HDLC SETTINGS");
					}
				if((dev->settings.ccr0 & 0x03)==1)
					{
#ifdef ENABLE_DEBUG_MESSAGES
DBGMSG("SDLC LOOP SETTINGS--Not Implemented");
#endif
					//init SDLC loop //possibly the same as HDLC settings, 
					//don't know I have never used this mode
					}
				if((dev->settings.ccr0 & 0x03)==2)
					{
#ifdef ENABLE_DEBUG_MESSAGES
DBGMSG("starting BISYNC SETTINGS");
#endif
					//init Bisync
					outb(CCR0,dev->settings.ccr0);
					outb(CCR1,dev->settings.ccr1);
					outb(CCR2,dev->settings.ccr2);
					outb(CCR3,dev->settings.ccr3);
					outb(CCR4,dev->settings.ccr4);
					outb(MODE,dev->settings.mode);
					outb(TIMR,dev->settings.timr);
					outb(SYNL,dev->settings.synl);
					outb(SYNH,dev->settings.synh);
					outb(TCR,dev->settings.tcr);
					outb(DAFO,dev->settings.dafo);
					outb(RFC,dev->settings.rfc);
					outb(XBCL,dev->settings.xbcl);
					outb(XBCH,dev->settings.xbch);
					outb(BGR,dev->settings.bgr);
					outb(PRE,dev->settings.pre);
					outb(TSAX,dev->settings.tsax);
					outb(TSAR,dev->settings.tsar);
					outb(XCCR,dev->settings.xccr);
					outb(RCCR,dev->settings.rccr);
					outb(PVR,(dev->settings.pvr&0xfe)+dev->channel);
//DBGMSG("finished BISYNC SETTINGS");
					}

				if((dev->settings.ccr0 & 0x03)==3)
					{
#ifdef ENABLE_DEBUG_MESSAGES
DBGMSG("starting ASYNC SETTINGS");
#endif
					//init async
					outb(MODE,dev->settings.mode);
					outb(TIMR,dev->settings.timr);
					outb(TCR,dev->settings.tcr);
					outb(DAFO,dev->settings.dafo);
					outb(RFC,dev->settings.rfc);
					outb(XBCL,dev->settings.xbcl);
					outb(XBCH,dev->settings.xbch);
					outb(CCR0,dev->settings.ccr0);
					outb(CCR1,dev->settings.ccr1);
					outb(CCR2,dev->settings.ccr2);
					outb(CCR3,dev->settings.ccr3);
					outb(CCR4,dev->settings.ccr4);
					outb(BGR,dev->settings.bgr);
					outb(XON,dev->settings.xon);
					outb(XOFF,dev->settings.xoff);
					outb(TSAX,dev->settings.tsax);
					outb(TSAR,dev->settings.tsar);
					outb(XCCR,dev->settings.xccr);
					outb(RCCR,dev->settings.rccr);
					outb(MXN,dev->settings.mxn);
					outb(MXF,dev->settings.mxf);
					outb(PVR,(dev->settings.pvr&0xfe)+dev->channel);
//DBGMSG("finished ASYNC SETTINGS");					
					}
				//could use the WAIT_FOR_CEC macro here, but
				//it is nice when debugging to know which one failed...
				//and we need to disable interrupts here if it fails
				//resets here
				//XRES & RHR
//DBGMSG("pre CEC check");
				timeoutvalue = 0;
				do
					{
					timeoutvalue++;
					if((inb(STAR)&CEC)==0x00) break;
					}while(timeoutvalue<MAX_TIMEOUT_VALUE);
				if(timeoutvalue>=MAX_TIMEOUT_VALUE)
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("TIMEOUT in waitforCEC");
#endif
					outb(IMR0,0xff);//disable (mask) interrupts
					outb(IMR1,0xff);//disable (mask) interrupts
					outb(PIM,0xff);//could fubar ch2/ch1 DTR/DSR interrupt settings
					//restore channel
					outb(PVR,chanstor);
					if(sem_post(&boardlock[dev->board])==-1)//up(&boardlock[dev->board]); //unlock board
						{
#ifdef ENABLE_DEBUG_MESSAGES
						DBGMSG("ERROR boardunlock failed");
						perror(NULL);
#endif
						}
					msg->qioctl_reply.status = EBUSY;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
//DBGMSG("post CEC check");
				//PDEBUG("XRES-ISSUED");
//DBGMSG("XRES issued");
				outb(CMDR,XRES);//reset transmit
//DBGMSG("pre CEC check");

				timeoutvalue = 0;
				do
					{
					timeoutvalue++;
					if((inb(STAR)&CEC)==0x00) break;
					}while(timeoutvalue<MAX_TIMEOUT_VALUE);
				if(timeoutvalue>=MAX_TIMEOUT_VALUE)
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("TIMEOUT in waitforCEC-xres");
#endif
					outb(IMR0,0xff);//disable (mask) interrupts
					outb(IMR1,0xff);//disable (mask) interrupts
					outb(PIM,0xff);//could fubar ch2/ch1 DTR/DSR interrupt settings
					//restore channel
					outb(PVR,chanstor);
					if(sem_post(&boardlock[dev->board])==-1)//up(&boardlock[dev->board]); //unlock board
						{
#ifdef ENABLE_DEBUG_MESSAGES
						DBGMSG("ERROR boardunlock failed");
						perror(NULL);
#endif
						}
					msg->qioctl_reply.status = EBUSY;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
//DBGMSG("post CEC check");
//DBGMSG("RHR issued");
				//PDEBUG("RHR-ISSUED");
				outb(CMDR,RHR);

//DBGMSG("pre CEC check");

				timeoutvalue = 0;
				do
					{
					timeoutvalue++;
					if((inb(STAR)&CEC)==0x00) break;
					}while(timeoutvalue<MAX_TIMEOUT_VALUE);
				if(timeoutvalue>=MAX_TIMEOUT_VALUE)
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("TIMEOUT in waitforCEC-rhr");
#endif
					outb(IMR0,0xff);//disable (mask) interrupts
					outb(IMR1,0xff);//disable (mask) interrupts
					outb(PIM,0xff);//could fubar ch2/ch1 DTR/DSR interrupt settings
					//restore channel
					outb(PVR,chanstor);
					if(sem_post(&boardlock[dev->board])==-1)//up(&boardlock[dev->board]); //unlock board
						{
#ifdef ENABLE_DEBUG_MESSAGES
						DBGMSG("ERROR boardunlock failed");
						perror(NULL);
#endif
						}
					msg->qioctl_reply.status = EBUSY;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
//DBGMSG("post CEC check");
//DBGMSG("channel restore");
				//restore channel
				outb(PVR,chanstor);
				if(sem_post(&boardlock[dev->board])==-1)//up(&boardlock[dev->board]); //unlock board
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR boardunlock failed");
					perror(NULL);
#endif
					msg->qioctl_reply.status = EAGAIN;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
//DBGMSG("board unlocked");
				//buffer allocation for internal driver buffers

				//check limits on n_rbufs/ntbufs/n_rfsize_max/n_tfsize_max 
				//and free if allready allocated
				//these are forced because the queuing routines must have at least 2 buffers to work with or they don't operate correctly
				//the min size is to match the hardware fifo size on the 82532
				//the max size is really the max HDLC received frame size before you cannot
				//tell using the chip how many bytes you have received, it is not currently 
				//enforced, but it would be wastefull to use frame buffers bigger than this
				if(dev->settings.n_rbufs<2) dev->settings.n_rbufs = 2;//
				if(dev->settings.n_tbufs<2) dev->settings.n_tbufs = 2;//
				if(dev->settings.n_rfsize_max<64) dev->settings.n_rfsize_max = 64;//
				if(dev->settings.n_tfsize_max<64) dev->settings.n_tfsize_max = 64;//

				//and finally technically you can use the overflow bit to get to 8194 bytes received
				//on the counter ((RBCH<<8)+RBCL), this has been done in the past by someone who needed 5000 byte 
				//hdlc frames.
				//if(dev->settings.n_rfsize_max>8194) dev->settings.n_rfsize_max = 8194;
				//if(dev->settings.n_tfsize_max>8194) dev->settings.n_tfsize_max = 8194;
//DBGMSG("free buffers");
				//free buffers if they allready exist
				if((dev->escc_rbuf!=NULL)&&(dev->escc_rbuf[0].frame !=NULL)) free(dev->escc_rbuf[0].frame);
				if((dev->escc_tbuf!=NULL)&&(dev->escc_tbuf[0].frame !=NULL)) free(dev->escc_tbuf[0].frame);
				if(dev->escc_rbuf!=NULL) free(dev->escc_rbuf);
				if(dev->escc_tbuf!=NULL) free(dev->escc_tbuf);

				//allocate memory for buffers
				dev->escc_rbuf = (struct buf*) malloc(dev->settings.n_rbufs*sizeof(struct buf));
				if(dev->escc_rbuf==NULL)
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR mem alloc rbufs");
#endif
					msg->qioctl_reply.status = ENOMEM;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
				dev->escc_tbuf = (struct buf*) malloc(dev->settings.n_tbufs*sizeof(struct buf)); 
				if(dev->escc_tbuf==NULL)
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR mem alloc tbufs");
#endif
					free(dev->escc_rbuf);
					msg->qioctl_reply.status = ENOMEM;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
				tempbuf = (char *)malloc(dev->settings.n_rbufs*dev->settings.n_rfsize_max);
				if(tempbuf==NULL)
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR mem alloc rbuf.frames");
#endif
					free(dev->escc_rbuf);
					free(dev->escc_tbuf);
					msg->qioctl_reply.status = ENOMEM;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
				for(i=0;i<dev->settings.n_rbufs;i++)
					{
					dev->escc_rbuf[i].frame = &tempbuf[i*dev->settings.n_rfsize_max];
					dev->escc_rbuf[i].valid = 0;
					dev->escc_rbuf[i].no_bytes = 0;
					dev->escc_rbuf[i].max =0;
					}
				tempbuf = (char *)malloc(dev->settings.n_tbufs*dev->settings.n_tfsize_max);
				if(tempbuf==NULL)
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR mem alloc tbuf.frames");
#endif
					free(dev->escc_rbuf[0].frame);
					free(dev->escc_rbuf);
					free(dev->escc_tbuf);
					msg->qioctl_reply.status = ENOMEM;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
				for(i=0;i<dev->settings.n_tbufs;i++)
					{
					dev->escc_tbuf[i].frame = &tempbuf[i*dev->settings.n_tfsize_max];
					dev->escc_tbuf[i].valid = 0;
					dev->escc_tbuf[i].no_bytes = 0;
					dev->escc_tbuf[i].max = 0;
					}
				//set the atomic access variables for frame counts in the buffers
				atomic_set(dev->transmit_frames_available,dev->settings.n_tbufs);
				atomic_set(dev->received_frames_pending,0);
//DBGMSG("allocated buffers");
				//make the board live, enable interrupts

				if(sem_wait(&boardlock[dev->board])==-1)//down(&boardlock[dev->board]);
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR boardlock failed");
					perror(NULL);
#endif
					msg->qioctl_reply.status = EAGAIN;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
//DBGMSG("board locked");
					//save channel
					chanstor = inb(PVR);
					outb(PVR,(inb(PVR)&0xfe)+dev->channel);
flags = pswget();
disable();

					outb(IMR0,dev->settings.imr0);
					outb(IMR1,dev->settings.imr1);
					outb(PIM,dev->settings.pim);
//DBGMSG("post unmask irq");
					if((dev->settings.ccr0 & 0x03)==3)
						{
						//bisync-enter hunt mode
						//wait with timeout;
						timeoutvalue = 0;
						do
							{
							timeoutvalue++;
							if((inb(STAR)&CEC)==0x00) break;
							}while(timeoutvalue<MAX_TIMEOUT_VALUE);
						if(timeoutvalue>=MAX_TIMEOUT_VALUE)
							{
#ifdef ENABLE_DEBUG_MESSAGES
							DBGMSG("TIMEOUT in waitforCEC-hunt");
#endif
							outb(IMR0,0xff);
							outb(IMR1,0xff);
							outb(PIM,0xff);//could fubar ch2/ch1 DTR/DSR interrupt settings
							//restore channel
							outb(PVR,chanstor);
							if(sem_post(&boardlock[dev->board])==-1)//up(&boardlock[dev->board]); //unlock board
								{
#ifdef ENABLE_DEBUG_MESSAGES
								DBGMSG("ERROR boardunlock failed");
								perror(NULL);
#endif
								}
restore(flags);
							msg->qioctl_reply.status = EBUSY;
							Reply(pid,msg,sizeof(msg->qioctl_reply));
							break;
							}
//DBGMSG("HUNT issued");
						outb(CMDR,HUNT);
						}

				//restore channel
				outb(PVR,chanstor);
				if(sem_post(&boardlock[dev->board])==-1)//up(&boardlock[dev->board]); //unlock board
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR boardunlock failed");
					perror(NULL);
#endif
restore(flags);
					msg->qioctl_reply.status = EAGAIN;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}

				dev->port_initialized = 1; //flag to let the rest of the driver know that the port has been setup
restore(flags);
#ifdef ENABLE_DEBUG_MESSAGES
DBGMSG("INIT COMPLETE");
#endif
				msg->qioctl_reply.status = EOK;
				Reply(pid,msg,sizeof(msg->qioctl_reply));
				break;
			case ESCC_READ_REGISTER:
#ifdef ENABLE_DEBUG_MESSAGES
				DBGMSG("in ioctl-read_reg");
#endif

				if(port==0)
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("Port not setup, no base address");
#endif
					msg->qioctl_reply.status = ENODEV;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
//DBGMSG("past port valid check");

				//get regsingle struct from user space (passed in)
				if(Readmsg(pid,sizeof(msg->qioctl)-1,&passval,sizeof(passval))==-1)
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR reading passval from user message");
#endif
					msg->qioctl_reply.status = EINVAL;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
				if(sem_wait(&boardlock[dev->board])==-1)//down(&boardlock[dev->board]);
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR boardlock failed");
					perror(NULL);
#endif
					msg->qioctl_reply.status = EAGAIN;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
				//DBGMSG("board locked");
				//save channel
				chanstor = inb(PVR);
				outb(PVR,(inb(PVR)&0xfe)+dev->channel);

				status = inb(dev->base+passval);//boy that was a lot of work just to do this one-liner...
#ifdef ENABLE_DEBUG_MESSAGES
				sprintf(errbuf,"READ_REGISTER in:%x ->%x",(unsigned)(passval+dev->base),status);	
				DBGMSG(errbuf);
#endif
				//done with the board
				outb(PVR,chanstor);
				if(sem_post(&boardlock[dev->board])==-1)//up(&boardlock[dev->board]); //unlock board
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR boardunlock failed");
					perror(NULL);
#endif
					}


				//copy the result back to user space
				msg->qioctl_reply.status = EOK;
				msg->qioctl_reply.ret_val =  sizeof(status);
//				msg->qioctl_reply.data[0] = (char*)regs[0];
				_setmx(mx+0,msg,sizeof(msg->qioctl_reply)-1);
				_setmx(mx+1,&status,sizeof(status));
				Replymx(pid,2,mx);
	
				break;
			case ESCC_WRITE_REGISTER:
#ifdef ENABLE_DEBUG_MESSAGES
				DBGMSG("in ioctl-write_reg");
#endif
				if(msg->qioctl.nbytes<sizeof(regsingle))
					{
#ifdef ENABLE_DEBUG_MESSAGES
					sprintf(errbuf,"ERROR invalid buffer size, input size:%u, expected:%u",msg->qioctl.nbytes,sizeof(regsingle));
					DBGMSG(errbuf);
#endif
					msg->qioctl_reply.status = EINVAL;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
//DBGMSG("past in size check");
				if(port==0)
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("Port not setup, no base address");
#endif
					msg->qioctl_reply.status = ENODEV;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
//DBGMSG("past port valid check");

				//get regsingle struct from user space (passed in)
				if(Readmsg(pid,sizeof(msg->qioctl)-1,&regs,sizeof(regsingle))==-1)
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR reading regsingle struct from user message");
#endif
					msg->qioctl_reply.status = EINVAL;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
				if(sem_wait(&boardlock[dev->board])==-1)//down(&boardlock[dev->board]);
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR boardlock failed");
					perror(NULL);
#endif
					msg->qioctl_reply.status = EAGAIN;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
				//DBGMSG("board locked");
				//save channel
				chanstor = inb(PVR);
				outb(PVR,(inb(PVR)&0xfe)+dev->channel);

				outb(dev->base + regs.port,regs.data);//again a lot of work for a one liner

#ifdef ENABLE_DEBUG_MESSAGES
				sprintf(errbuf,"WRITE_REGISTER out:%x ->%x",(unsigned)(regs.port+dev->base),regs.data);	
				DBGMSG(errbuf);
#endif
				//done with the board
				outb(PVR,chanstor);
				if(sem_post(&boardlock[dev->board])==-1)//up(&boardlock[dev->board]); //unlock board
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR boardunlock failed");
					perror(NULL);
#endif
					}


				//nothing returned
				msg->qioctl_reply.status = EOK;
				msg->qioctl_reply.ret_val = 0;
				Reply(pid,msg,sizeof(msg->qioctl_reply));
	
				break;
			case ESCC_STATUS:
#ifdef ENABLE_DEBUG_MESSAGES
				DBGMSG("in ioctl-status");
#endif
//st_wait_return_path:
				//note that this is a blocking status call
				//it will block until a status event occurs
				//(unless opened nonblocking)
				//to force nonblocking behavior call immediate_status
				our_status(pid,msg,dev);
				break;
			case ESCC_ADD_BOARD:
#ifdef ENABLE_DEBUG_MESSAGES
				DBGMSG("in ioctl-add_board");
#endif
				if(msg->qioctl.nbytes != sizeof(board_settings))
					{
#ifdef ENABLE_DEBUG_MESSAGES
					sprintf(errbuf,"ERROR invalid buffer size, input size:%u, expected:%u",msg->qioctl.nbytes,sizeof(board_settings));
					DBGMSG(errbuf);
#endif
					msg->qioctl_reply.status = EINVAL;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
				//get setup struct from user space (passed in)
				if(Readmsg(pid,sizeof(msg->qioctl)-1,&board_switches,sizeof(board_settings))==-1)
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR reading board_settings struct from user message");
#endif
					msg->qioctl_reply.status = EINVAL;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}

				//check to see if this base address/irq/channel combo is allready in use
				for(i=0;i<escc_nr_devs;i++)
					{
					if( (escc_devices[i].base == board_switches.base)&&
							(escc_devices[i].irq == board_switches.irq)&&
							(escc_devices[i].channel == board_switches.channel) )
						{
						//the region is allready there, return error
						//you must ESCC_REMOVE_BOARD before you can use the same
						//settings on a different /dev/esccx number
#ifdef ENABLE_DEBUG_MESSAGES
						DBGMSG("ERROR board address & irq currently exist");
#endif
						msg->qioctl_reply.status = EEXIST;
						Reply(pid,msg,sizeof(msg->qioctl_reply));
						goto done_addboard;
						}
					}

				//check to see if other port of the board is in list
				for(i=0;i<escc_nr_devs;i++)
					{
					if( (escc_devices[i].base == board_switches.base)&&
							(escc_devices[i].irq == board_switches.irq)&&
							(escc_devices[i].channel != board_switches.channel) )
						{
						dev->board = escc_devices[i].board;
						goto next_section;
						}
					}
				//allocate board # here..remember that the board# is used to 
				//gain access to the board via the boardlock[] semaphore array
				//so it must be in the range 0->MAX_ESCC_BOARDS;
				for(i=0;i<escc_nr_devs;i++)
					{
					if(used_board_numbers[i]==0)
						{
 						dev->board = i;
						used_board_numbers[i] = 1;
						break;
						}
					}

next_section:

				//if we have a previously allocated irq unhook it here
				if(dev->irq_hooked==1)
					{
					//release current irq mapping.
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("detach irq");
#endif
					qnx_hint_detach(dev->intID);
					dev->irq_hooked=0;
					}

				//save off switch info in hardware device storage area
				dev->base = board_switches.base;
				dev->irq = board_switches.irq;
				dev->dmar = board_switches.dmar;
				dev->dmat = board_switches.dmat;
				dev->channel = board_switches.channel;
				//set port for defined port offsets
				port = dev->base;
				outb(PCR,0xE0);//must be , set port config register on 82532
				outb(IPC,0x03);//must be , setup interrupt line config on 82532
				outb(IVA,0x00);//must be , not using interrupt vector, set to 0

				//hook the irq (since it wasn't done on open as this set of settings didn't exist yet)
				dev->intID = qnx_hint_attach(dev->irq,&escc_irq,FP_SEG(&devno));
				if(dev->intID==-1)
					{
#ifdef ENABLE_DEBUG_MESSAGES
					sprintf(errbuf,"request_irq failed (in add_board):%i",dev->intID);
					DBGMSG(errbuf);
#endif
					msg->open_reply.status = EBUSY;
					Reply(pid,msg,sizeof(msg->open_reply));
					break;
					}
				dev->irq_hooked = 1;

#ifdef ENABLE_DEBUG_MESSAGES
				sprintf(errbuf,"request irq:%u success",dev->irq);
				DBGMSG(errbuf);
#endif
				//debug info, display contents of escc_devices array board settings
#ifdef ENABLE_DEBUG_MESSAGES
				for(i=0;i<escc_nr_devs;i++)
					{
					sprintf(errbuf,"deivce:%u -> board:%u -> port%x",i,escc_devices[i].board,escc_devices[i].base);
					DBGMSG(errbuf);
					}
#endif

				msg->qioctl_reply.status = EOK;
				Reply(pid,msg,sizeof(msg->qioctl_reply));
done_addboard:
				break;
			case ESCC_TX_ACTIVE:
#ifdef ENABLE_DEBUG_MESSAGES
				DBGMSG("in ioctl-tx_active");
#endif
				if(dev->is_transmitting==0) status = 0;
				else status = 1;
				msg->qioctl_reply.status = EOK;
				msg->qioctl_reply.ret_val =  sizeof(status);
				_setmx(mx+0,msg,sizeof(msg->qioctl_reply)-1);
				_setmx(mx+1,&status,sizeof(status));
				Replymx(pid,2,mx);
				break;
			case ESCC_RX_READY:
#ifdef ENABLE_DEBUG_MESSAGES
				DBGMSG("in ioctl-rx_ready");
#endif
				status = atomic_read(dev->received_frames_pending);
				msg->qioctl_reply.status = EOK;
				msg->qioctl_reply.ret_val =  sizeof(status);
				_setmx(mx+0,msg,sizeof(msg->qioctl_reply)-1);
				_setmx(mx+1,&status,sizeof(status));
				Replymx(pid,2,mx);
				break;
			case ESCC_START_TIMER:
#ifdef ENABLE_DEBUG_MESSAGES
				DBGMSG("in ioctl-start_timer");
#endif
				if(port==0)
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("Port not setup, no base address");
#endif
					msg->qioctl_reply.status = ENODEV;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
//DBGMSG("past port valid check");

				if(sem_wait(&boardlock[dev->board])==-1)//down(&boardlock[dev->board]);
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR boardlock failed");
					perror(NULL);
#endif
					msg->qioctl_reply.status = EAGAIN;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
				//DBGMSG("board locked");
				//save channel
				chanstor = inb(PVR);
				outb(PVR,(inb(PVR)&0xfe)+dev->channel);
				WAIT_WITH_TIMEOUT;
				outb(CMDR,STIB);
				//done with the board
				outb(PVR,chanstor);
				if(sem_post(&boardlock[dev->board])==-1)//up(&boardlock[dev->board]); //unlock board
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR boardunlock failed");
					perror(NULL);
#endif
					}
				//nothing returned
				msg->qioctl_reply.status = EOK;
				msg->qioctl_reply.ret_val = 0;
				Reply(pid,msg,sizeof(msg->qioctl_reply));
				break;
			case ESCC_STOP_TIMER:
#ifdef ENABLE_DEBUG_MESSAGES
				DBGMSG("in ioctl-stop_timer");
#endif
				if(port==0)
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("Port not setup, no base address");
#endif
					msg->qioctl_reply.status = ENODEV;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
//DBGMSG("past port valid check");

				if(sem_wait(&boardlock[dev->board])==-1)//down(&boardlock[dev->board]);
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR boardlock failed");
					perror(NULL);
#endif
					msg->qioctl_reply.status = EAGAIN;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
				//DBGMSG("board locked");
				//save channel
				chanstor = inb(PVR);
				outb(PVR,(inb(PVR)&0xfe)+dev->channel);
				WAIT_WITH_TIMEOUT;
				outb(TIMR,dev->settings.timr);
				//done with the board
				outb(PVR,chanstor);
				if(sem_post(&boardlock[dev->board])==-1)//up(&boardlock[dev->board]); //unlock board
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR boardunlock failed");
					perror(NULL);
#endif
					}
				//nothing returned
				msg->qioctl_reply.status = EOK;
				msg->qioctl_reply.ret_val = 0;
				Reply(pid,msg,sizeof(msg->qioctl_reply));
				break;
			case ESCC_SET_TX_TYPE:
#ifdef ENABLE_DEBUG_MESSAGES
				DBGMSG("in ioctl-set_tx_type");
#endif
				//function to change the TX type (the command used to issue write frame commands)
				//the options are either send transparent frames or information frames,
				//use XTF if you are not in auto mode,
				//use XIF to send I frames in auto mode.
				//use XTF to send U frames in auto mode.

				//get user data
				if(Readmsg(pid,sizeof(msg->qioctl)-1,&passval,sizeof(passval))==-1)
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR reading passval from user message");
#endif
					msg->qioctl_reply.status = EINVAL;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
				if((passval!=XTF)&&(passval!=XIF))
					{
					msg->qioctl_reply.status = EINVAL;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
#ifdef ENABLE_DEBUG_MESSAGES
				sprintf(errbuf,"SET_TX_TYPE:%x",(unsigned)passval);
				DBGMSG(errbuf);
#endif
				if(passval==XIF) dev->tx_type = XIF;
				if(passval==XTF) dev->tx_type = XTF;

				msg->qioctl_reply.status = EOK;
				msg->qioctl_reply.ret_val = 0;
				Reply(pid,msg,sizeof(msg->qioctl_reply));
				break;
			case ESCC_SET_TX_ADDRESS:
#ifdef ENABLE_DEBUG_MESSAGES
				DBGMSG("in ioctl-set_tx_add");
#endif
				if(port==0)
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("Port not setup, no base address");
#endif
					msg->qioctl_reply.status = ENODEV;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
//DBGMSG("past port valid check");
		      	if(dev->is_transmitting == 1)
					{
					msg->qioctl_reply.status = EBUSY;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
				//get user data
				if(Readmsg(pid,sizeof(msg->qioctl)-1,&passval,sizeof(passval))==-1)
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR reading passval from user message");
#endif
					msg->qioctl_reply.status = EINVAL;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
				if(sem_wait(&boardlock[dev->board])==-1)//down(&boardlock[dev->board]);
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR boardlock failed");
					perror(NULL);
#endif
					msg->qioctl_reply.status = EAGAIN;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
				//DBGMSG("board locked");
				//save channel
				chanstor = inb(PVR);
				outb(PVR,(inb(PVR)&0xfe)+dev->channel);

      			outb(XAD1, (passval>>8)&0xfc);
      			outb(XAD2, (passval&0xff));
				//done with the board
				outb(PVR,chanstor);
				if(sem_post(&boardlock[dev->board])==-1)//up(&boardlock[dev->board]); //unlock board
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR boardunlock failed");
					perror(NULL);
#endif
					}
				//nothing returned
				msg->qioctl_reply.status = EOK;
				msg->qioctl_reply.ret_val = 0;
				Reply(pid,msg,sizeof(msg->qioctl_reply));
				break;
			case ESCC_FLUSH_RX:
#ifdef ENABLE_DEBUG_MESSAGES
				DBGMSG("in ioctl-flush_rx");
#endif
				if(dev->port_initialized==0)//could use a better error value, needs to convey that the port has not been initialized yet
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("Port not setup, no base address");
#endif
					msg->qioctl_reply.status = ENODEV;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
				if(sem_wait(&boardlock[dev->board])==-1)//down(&boardlock[dev->board]);
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR boardlock failed");
					perror(NULL);
#endif
					msg->qioctl_reply.status = EAGAIN;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}

//				DBGMSG("FLUSH_RX");
				flags = pswget();
				disable();
				//save channel
				chanstor = inb(PVR);
				outb(PVR,(inb(PVR)&0xfe)+dev->channel);
		
				WAIT_WITH_TIMEOUT;
				outb(CMDR,RHR);
				outb(PVR,chanstor);
				for(i=0;i<dev->settings.n_rbufs;i++)
					{
					dev->escc_rbuf[i].valid = 0;
					dev->escc_rbuf[i].no_bytes = 0;
					}
				dev->current_rxbuf = 0;
				atomic_set(dev->received_frames_pending,0);
				dev->rq = 0;

				restore(flags); 
				if(sem_post(&boardlock[dev->board])==-1)//up(&boardlock[dev->board]); //unlock board
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR boardunlock failed");
					perror(NULL);
#endif
					}

				//The flush will finish a pending read with 0 bytes
				//could possibly handle this differently,
				//either not finish the read or maybe finish with an error 
				if(dev->read_pending==1)
					{
					msg->read_reply.status = EOK;
					msg->read_reply.nbytes = 0;
					Reply(dev->r_pid,msg,sizeof(msg->read_reply));
					dev->r_pid = 0;
					dev->read_pending = 0;
					dev->r_size = 0;
					}
				//nothing returned
				msg->qioctl_reply.status = EOK;
				msg->qioctl_reply.ret_val = 0;
				Reply(pid,msg,sizeof(msg->qioctl_reply));

				break;
			case ESCC_FLUSH_TX:
#ifdef ENABLE_DEBUG_MESSAGES
				DBGMSG("in ioctl-flush_tx");
#endif
				if(dev->port_initialized==0)//could use a better error value, needs to convey that the port has not been initialized yet
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("Port not setup, no base address");
#endif
					msg->qioctl_reply.status = ENODEV;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
				if(sem_wait(&boardlock[dev->board])==-1)//down(&boardlock[dev->board]);
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR boardlock failed");
					perror(NULL);
#endif
					msg->qioctl_reply.status = EAGAIN;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}

//				DBGMSG("FLUSH_TX");
				flags = pswget();
				disable();
				//save channel
				chanstor = inb(PVR);
				outb(PVR,(inb(PVR)&0xfe)+dev->channel);
		
				for(i=0;i<dev->settings.n_tbufs;i++)
					{
					dev->escc_tbuf[i].valid = 0;
					dev->escc_tbuf[i].no_bytes = 0;
					}
				dev->current_txbuf = 0;
				dev->is_transmitting = 0;
				atomic_set(dev->transmit_frames_available,dev->settings.n_tbufs);
				dev->wq =0;
				WAIT_WITH_TIMEOUT;
				outb(CMDR,XRES);
				outb(PVR,chanstor);
				restore(flags);
				if(sem_post(&boardlock[dev->board])==-1)//up(&boardlock[dev->board]); //unlock board
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR boardunlock failed");
					perror(NULL);
#endif
					}

				//The flush will finish a pending write with 0 bytes
				//could possibly handle this differently,
				//either not finish the read or maybe finish with an error 
				if(dev->write_pending==1)
					{
					msg->write_reply.status = EOK;
					msg->write_reply.nbytes = 0;
					Reply(dev->w_pid,msg,sizeof(msg->write_reply));
					dev->w_pid = 0;
					dev->write_pending = 0;
					dev->w_size = 0;
					}
				//nothing returned
				msg->qioctl_reply.status = EOK;
				msg->qioctl_reply.ret_val = 0;
				Reply(pid,msg,sizeof(msg->qioctl_reply));
				break;
			case ESCC_CMDR:
#ifdef ENABLE_DEBUG_MESSAGES
				DBGMSG("in ioctl-cmdr");
#endif
				if(port==0)
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("Port not setup, no base address");
#endif
					msg->qioctl_reply.status = ENODEV;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}

				//get regsingle struct from user space (passed in)
				if(Readmsg(pid,sizeof(msg->qioctl)-1,&passval,sizeof(passval))==-1)
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR reading passval from user message");
#endif
					msg->qioctl_reply.status = EINVAL;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
				if(sem_wait(&boardlock[dev->board])==-1)//down(&boardlock[dev->board]);
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR boardlock failed");
					perror(NULL);
#endif
					msg->qioctl_reply.status = EAGAIN;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
				//DBGMSG("board locked");
				//save channel
				chanstor = inb(PVR);
				outb(PVR,(inb(PVR)&0xfe)+dev->channel);
				
				WAIT_WITH_TIMEOUT;
				outb(CMDR,passval);//again a lot of work for a one liner

#ifdef ENABLE_DEBUG_MESSAGES
				sprintf(errbuf,"CMDR out:%x ->%x",CMDR,passval);	
				DBGMSG(errbuf);
#endif
				//done with the board
				outb(PVR,chanstor);
				if(sem_post(&boardlock[dev->board])==-1)//up(&boardlock[dev->board]); //unlock board
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR boardunlock failed");
					perror(NULL);
#endif
					}

				//nothing returned
				msg->qioctl_reply.status = EOK;
				msg->qioctl_reply.ret_val = 0;
				Reply(pid,msg,sizeof(msg->qioctl_reply));
	

				break;
			case ESCC_GET_DSR:
#ifdef ENABLE_DEBUG_MESSAGES
				DBGMSG("in ioctl-get_dsr");
#endif
				if(port==0)
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("Port not setup, no base address");
#endif
					msg->qioctl_reply.status = ENODEV;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}

				//no need to lock the board, as the PVR register
				//is a single register mapped to both channels
				//as long as we are not writing it it doesn't need
				//to be protected
				if(dev->channel == 0)
					{
					status =  (inb(PVR)>>5) & 1;
					}
				if(dev->channel == 1)
					{
					status =  (inb(PVR)>>6) & 1;
					}      

#ifdef ENABLE_DEBUG_MESSAGES
				sprintf(errbuf,"GET_DSR:%x->%x",inb(PVR),status);		
				DBGMSG(errbuf);
#endif
				msg->qioctl_reply.status = EOK;
				msg->qioctl_reply.ret_val =  sizeof(status);
				_setmx(mx+0,msg,sizeof(msg->qioctl_reply)-1);
				_setmx(mx+1,&status,sizeof(status));
				Replymx(pid,2,mx);
				break;
			case ESCC_SET_DTR:
#ifdef ENABLE_DEBUG_MESSAGES
				DBGMSG("in ioctl-set_dtr");
#endif
				if(port==0)
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("Port not setup, no base address");
#endif
					msg->qioctl_reply.status = ENODEV;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
				//get passval from user space (passed in)
				if(Readmsg(pid,sizeof(msg->qioctl)-1,&passval,sizeof(passval))==-1)
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR reading passval from user message");
#endif
					msg->qioctl_reply.status = EINVAL;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
				if(sem_wait(&boardlock[dev->board])==-1)//down(&boardlock[dev->board]);
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR boardlock failed");
					perror(NULL);
#endif
					msg->qioctl_reply.status = EAGAIN;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
				//DBGMSG("board locked");
				
				//set DTR here
      			if(dev->channel == 0)
      				{
					outb(PVR,(inb(PVR)&0xF7)|((passval&1)<<3));
					}
      			if(dev->channel == 1)
      				{
					outb(PVR,(inb(PVR)&0xEF)|((passval&1)<<4));
					}
				//done with the board
				if(sem_post(&boardlock[dev->board])==-1)//up(&boardlock[dev->board]); //unlock board
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR boardunlock failed");
					perror(NULL);
#endif
					}
				//nothing returned
				msg->qioctl_reply.status = EOK;
				msg->qioctl_reply.ret_val = 0;
				Reply(pid,msg,sizeof(msg->qioctl_reply));
				break;
			case ESCC_SET_CLOCK:
#ifdef ENABLE_DEBUG_MESSAGES
				DBGMSG("in ioctl-set_clock");
#endif
				if(port==0)
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("Port not setup, no base address");
#endif
					msg->qioctl_reply.status = ENODEV;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
				//get passval from user space (passed in)
				if(Readmsg(pid,sizeof(msg->qioctl)-1,&passval,sizeof(passval))==-1)
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR reading passval from user message");
#endif
					msg->qioctl_reply.status = EINVAL;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
				if(sem_wait(&boardlock[dev->board])==-1)//down(&boardlock[dev->board]);
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR boardlock failed");
					perror(NULL);
#endif
					msg->qioctl_reply.status = EAGAIN;
					Reply(pid,msg,sizeof(msg->qioctl_reply));
					break;
					}
//				printf("passval:%x\n",passval);
				if(dev->channel==0) //turn clock /onoff
					{
					outb(PVR,(inb(PVR)&0xFD)|((passval&1)<<1));
//					printf("new pvr:%x\n",inb(PVR));
					}
				if(dev->channel==1) //turn clock on/off
					{
					outb(PVR,(inb(PVR)&0xFB)|((passval&1)<<2));
//					printf("new pvr:%x\n",inb(PVR));

					}
				//done with the board
				if(sem_post(&boardlock[dev->board])==-1)//up(&boardlock[dev->board]); //unlock board
					{
#ifdef ENABLE_DEBUG_MESSAGES
					DBGMSG("ERROR boardunlock failed");
					perror(NULL);
#endif
					}
				//nothing returned
				msg->qioctl_reply.status = EOK;
				msg->qioctl_reply.ret_val = 0;
				Reply(pid,msg,sizeof(msg->qioctl_reply));
				break;
			case ESCC_IMMEDIATE_STATUS:
#ifdef ENABLE_DEBUG_MESSAGES
				DBGMSG("in ioctl-immed_status");
#endif
				flags = pswget();
				disable();
				status = dev->status;//get status from driver info area
				dev->status = 0;//clear it
				restore(flags);//enable interrupts
				msg->qioctl_reply.status = EOK;
				msg->qioctl_reply.ret_val =  sizeof(status);
				_setmx(mx+0,msg,sizeof(msg->qioctl_reply)-1);
				_setmx(mx+1,&status,sizeof(status));
				Replymx(pid,2,mx);
#ifdef ENABLE_DEBUG_MESSAGES
				sprintf(errbuf,"STATUS:%lx",status);
				DBGMSG(errbuf);
#endif
				break;
			case ESCC_REMOVE_BOARD:
#ifdef ENABLE_DEBUG_MESSAGES
				DBGMSG("in ioctl-remove_board");
#endif
				//kill all board related info here...
				//all that is really necessary is to 
				//set irq = 0
				//set port =0
				//and set dev_initialized =0
				dev->base = 0;
				dev->irq = 0;
				dev->port_initialized =0;
				//if we wanted to go nuts with it we could determine if we need
				//to unhook the irq
				//and free the buffers
				//finish any read/write/status functions
				//etc
				//as once the board is removed all of these things are meaningless
				msg->qioctl_reply.status = EOK;
				msg->qioctl_reply.ret_val = 0;
				Reply(pid,msg,sizeof(msg->qioctl_reply));
				break;
			default:
#ifdef ENABLE_DEBUG_MESSAGES
				DBGMSG("in ioctl-default");
#endif
				msg->qioctl_reply.status = EOK;
				msg->qioctl_reply.ret_val = 0;
				Reply(pid,msg,sizeof(msg->qioctl_reply));
				break;
				}
return;
}