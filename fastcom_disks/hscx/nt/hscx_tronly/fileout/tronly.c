/*++

Copyright (c) 1998,1999 Commtech, Inc Wichita ,KS

Module Name:

    fileout.c (based on tronly.c)

Abstract:

    A simple Win32 app that uses the hscxdrv device to transmit only
	--modified to send a file specified from the command line as raw bits
Environment:

    user mode only

Notes:

    
Revision History:

    6/8/98             started
	4/15/99   --mod for file out as raw data
	10/27/99  --mod for bitrate specified on command line
	11/1/99	  --mod from escc to hscx
--*/



#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "hscxtest.h"


BOOL calculate_bits(double reffreq,double desired_freq,double *m_actual_clock,CLKSET *clk);

VOID
main(int argc, char *argv[])
{
HANDLE hDevice;			//handle to the escc driver/device
HANDLE fin;				//handle for input filename
DWORD nobyteswritten;   //number of bytes that the driver returns as written to the device
struct setup hscxsetup; //setup structure for initializing the escc registers (see escctest.h)
struct clkset clk;
char data[4096];        //character array for data storage (passing to and from the driver)
DWORD nobytestowrite;   //the number of bytes to write to the driver
ULONG j;                //temp vars
DWORD k;
DWORD returnsize;       //temp vars
BOOL t;                 //temp vars
OVERLAPPED  os ;        //overlapped structure for use in the transmit routine
DWORD nobytestoread;
DWORD nobytesread;
DWORD idle;
DWORD totbytesr;
DWORD totbytest;
double bitrate;
double desiredclock;
double referenceclock;
double actualclock;
unsigned N;
DWORD waittime;

if(argc<2)
{
printf("Usage:\r\n");
printf("sendit filetosend [bitrate]\r\n");
exit(1);
}
if(argc==3)
{
bitrate = atof(argv[2]);
if(bitrate==0.0) bitrate = 1000000.0; //if err use 1M
}
else bitrate = 1000000.0;//if not specified use 1M

if(bitrate >1200000.0)
{
bitrate = 1200000.0;
printf("bitrate specified over maximum\r\n");
printf("bitrate forced to maximum 1.2Mbps\r\n");
//you could actually force the maximum to be 4M, 
//but it would require switching from clock mode 2 
//to clock mode 0,1 or 4 and using an external clock
//(or the clock generator directly) for rates above 1.2M.
}

//printf("file %s, rate:%12.2f\r\n",argv[1],bitrate);

fin = CreateFile(argv[1],GENERIC_READ,0,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
if(fin==INVALID_HANDLE_VALUE)
	{
	printf("can not open specified file %s\r\n",argv[1]);
	exit(1);
	}


memset( &os, 0, sizeof( OVERLAPPED ) ) ;        //wipe the overlapped struct

// create I/O event used for overlapped write

os.hEvent = CreateEvent( NULL,    // no security
			    TRUE,    // explicit reset req
			    FALSE,   // initial event reset
			    NULL ) ; // no name
if (os.hEvent == NULL)
   {
      MessageBox( NULL, "Failed to create event for thread!", "main Error!",
		  MB_ICONEXCLAMATION | MB_OK ) ;
	  CloseHandle(fin);
      return; 
   }



//this will start up the driver, it also gives us a handle to send
//and receive data from the driver
//note that it is created with the FILE_FLAG_OVERLAPPED set
//if it is not then you must make sequential calls to the driver
//(ie wait until one call has returned before making another call)
//the read/write and status functions are set up to use overlapped, they 
//will return immediatly with a ERROR_IO_PENDING or immediatly with the required data
//

    hDevice = CreateFile ("\\\\.\\HSCX0",
			  GENERIC_READ | GENERIC_WRITE,
			  0,
			  NULL,
			  OPEN_EXISTING,
			  FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
			  NULL
			  );

    if (hDevice == INVALID_HANDLE_VALUE)
    {
		//for some reason the driver won't load or isn't loaded
	printf ("Can't get a handle to esccdrv\n");
	CloseHandle(fin);
	CloseHandle(os.hEvent);
		exit(1);
		//abort and leave here!!!
	}
referenceclock= 16000000.0;
//determine clock frequency (based on clock mode 2b);
if((bitrate*16.0) >= 391000.0)
	{
	//use clock generator directly
	hscxsetup.hscx_ccr2 = 0x18;//(BDF=0, forces inputclock/1 as ouput of bgr), rate = bgr/16
	hscxsetup.hscx_bgr =  0x00;//not used
	desiredclock = bitrate*16.0;
	calculate_bits(referenceclock,desiredclock,&actualclock,&clk);
	printf("data rate used:%12.2f\r\n",actualclock/16.0);

	}
else
	{
	//must divide down to get rate
	hscxsetup.hscx_ccr2 = 0x38;//(BDF=1, forces inputclock/((N+1)*2) as ouput of bgr), rate = bgr/16
	N = (((unsigned)(400000.0/(bitrate*16.0))+2)/2)-1;//force inclock to be >400k
	desiredclock = (bitrate*16.0)*((N+1)*2);//calcluate what inclock needs to be to get rate using N
	//make sure that N is attainable;
	//for simplicity limit N to 0-255, could actually go to 1023 by modifying CCR2 bits 7 and 8
	if(N>255)
		{
		printf("can't get to %12.2f easily\r\n",bitrate);
		//be nice and close handles here
		CloseHandle (hDevice);// stops the escc from interrupting (ie shuts it down)
		CloseHandle(os.hEvent);
		CloseHandle(fin);
		exit(1);
		}
	hscxsetup.hscx_bgr = N;
//	printf("N:%u,Desired clock:%12.2f\r\n",N,desiredclock);
	calculate_bits(referenceclock,desiredclock,&actualclock,&clk);
	printf("data rate used:%12.2f\r\n",(actualclock/((N+1)*2))/16.0);

	}
//this IOCTL function demonstrates how to set the clock generator on 
//the escc card
//this function will return TRUE unless an invalid parameter is given
//
t = DeviceIoControl(hDevice,IOCTL_HSCXDRV_SET_CLOCK_GENERATOR,&clk,sizeof(struct clkset),NULL,0,&returnsize,NULL);
	
	
	hscxsetup.hscx_mask = 0x06; //everything but channel A
	hscxsetup.hscx_mode = 0x0c0;//extended transparent mode 0 
	hscxsetup.hscx_timr = 0x1f;//not used
	hscxsetup.hscx_xad1 = 0xff;//no tx address/not used
	hscxsetup.hscx_xad2 = 0xff;//no tx address/not used
	hscxsetup.hscx_rah1 = 0xff;//receive address to receive frames by/not used
	hscxsetup.hscx_rah2 = 0xff;//optional 2nd address to receive frames by/not used
	hscxsetup.hscx_ral1 = 0xff;//low byte address (receive)/not used
	hscxsetup.hscx_ral2 = 0xff;//low byte address (receive)/not used
	hscxsetup.hscx_xbcl = 0x00;//used for dma /not used
	hscxsetup.hscx_xbch = 0x00;//used for dma (0x80 == use DMA, 0x00 == no DMA)/not used
	hscxsetup.hscx_rlcr = 0x00;//no receive length check
	hscxsetup.hscx_tsar = 0x00;//not used
	hscxsetup.hscx_tsax = 0x00;//not used
	hscxsetup.hscx_xccr = 0x00;//not used
	hscxsetup.hscx_rccr = 0x00;//not used
	hscxsetup.hscx_ccr1 = 0x92;//clock mode 2 (NRZ enconding)
	

	//when called this IOCTL will set the registers of the hscx and
	//reset the TX and RX machines (issuing a separate XRES and RHR command)
	//if the resets time out the function will return FALSE, otherwise it will
	//return TRUE
	t = DeviceIoControl(hDevice,IOCTL_HSCXDRV_SETUP,&hscxsetup,sizeof(struct setup),NULL,0,&returnsize,NULL);
	if(t==TRUE)printf("SETTINGS SUCCESSFUL:%lu\n\r",returnsize);
	if(t==FALSE)
		{
		printf("SETTINGS FAILED:%lu\n\r",returnsize);
		CloseHandle(fin);
		exit(1);
		}

nobytestoread = 4096;//read from file (requested)
nobytestowrite = 4096;  //this is the number of bytes to send to the driver to send as a single frame (can be from 1 to 4096) (requested)
nobytesread = 0; //what we get from the file (actual)
nobyteswritten = 0;//what is sent out the device (actual)
totbytesr = 0;//total bytes received (from file)
totbytest = 0;//total bytes transmitted (out device)
idle = 0;//idle counter (#times that writefile returns TRUE)
do
	{
	t= ReadFile(fin,data,nobytestoread,&nobytesread,NULL);//get data from the input file
	totbytesr += nobytesread;//inc the byte counter
	
	nobytestowrite = nobytesread;//set the bytes to write in case the read comes up short
	t = WriteFile(hDevice,data,nobytestowrite,&nobyteswritten,&os);//send the frame
	if(t==FALSE) //if it returned FALSE then the IO request is queued (waiting for previous frame to get done sending)
		{        //and we must wait until the os.event gets signaled before we try any more sending
		if (GetLastError() == ERROR_IO_PENDING)  //IO_PENDING is the indication that the request was queued, if not this than a parameter error occured
			{
			// wait for a second for this transmission to start
			//you should allways end up here other than on the first write.
			//if you do not get here then the data will not go out back to back
			//(there will be idle time between frames)
			//The first writefile should return true (indicating that the frame is started on the transmitter)
			//all writefile calls after that first one should return false/iopending
			//indicating that the frame is queued for transmit when the previous frame is complete.
			
			do
				{
				j = WaitForSingleObject( os.hEvent, (DWORD)(((double)(8*(4096+32))/bitrate)*(double)1000)+10);//bitrate dependent timeout, this number corresponds to the time to send 1 frame  +32 byte times
				if(j==WAIT_TIMEOUT)
					{
					printf("timeout\r\n");
					//this will execute every xx second that all of the tbuffers are full (queued)
					//the timeout value should be set depending on the bitrate used
					//it should be longer than the ammount of time it takes to send 1 of your frames

					//if this happens then something is wrong with the driver, or 
					//some event happened that prevented the card from finishing the write.
					//(be careful to make the timeout longer than the ammount of time that it
					//takes to send a frame :)

					//you could put a counter in here and if the 
					//driver takes an inordinate ammout of time
					//to complete, you could issue a flush TX command
					//and break out of this loop
					
					//printf("Transmitter Locked up...resetting tx\r\n");
					DeviceIoControl(hDevice,IOCTL_HSCXDRV_FLUSH_TX,NULL,0,NULL,0,&returnsize,NULL);
					//uncomment the next line if you are using the new driver
					/*
					DeviceIoControl(hDevice,IOCTL_HSCXDRV_IMMEDIATE_STATUS,NULL,0,&k,sizeof(DWORD),&returnsize,NULL);
					if(k!=0) printf("\r\nSTATUS: %lx\r\n",k);
					*/
					//the most common problem will be EXE/XDU or a data underrun, if that occurs then
					//the PC is not keeping up with the interrupt rate necessary to keep the 82526 fifo filled
					
					}
				if(j==WAIT_ABANDONED)
					{
					printf("abandoned\r\n");
					
					}
				
				}while(j!=WAIT_OBJECT_0);
			}
		}
	else
	{
		//write file returned true
		//this means that there was some idle pattern going out before this frame:
		//if all goes well this should only print 1 time when the program is started.
	idle++;
	//printf("idle detected\r\n");
	
	}
//GetOverlappedResult(hDevice,&os,&nobyteswritten,TRUE);//if you are speed challanged (ie the idle keeps being greater than 1) try removing this
//      totbytest += nobyteswritten;    //as it is only here to keep track of how many bytes are sent.

	}while(nobytesread!=0);
printf("filesend complete, waiting for end of tx...\r\n");

waittime = (DWORD)((double)(4096*8)/bitrate*(double)1000)+10;
Sleep(waittime);//make sure you wait until the last write(s) complete their sends, wait time should be greater than n_tbufs*(framesize*8)*(1/bitrate) )
printf("DONE\r\n");
	CloseHandle (hDevice);// stops the escc from interrupting (ie shuts it down)
	CloseHandle(os.hEvent);//finished with the event
	CloseHandle(fin);//finished with the input file
	printf("Idle:%u\r\n",idle);
	printf("bytes in:%lu\r\n",totbytesr);
//      printf("bytes out:%lu\r\n",totbytest);
	printf("exiting program\n\r");          //exit message

}                                               //done


//function to calcluate input clock params.
BOOL calculate_bits(double reffreq,double desired_freq,double *m_actual_clock,CLKSET *clk)
{
DWORD P;
DWORD Pprime;
DWORD Q;
DWORD Qprime;
DWORD M;
DWORD I;
DWORD D;
double fvco;
double desired_ratio;
double actual_ratio;
DWORD bestP;
DWORD bestQ;
DWORD bestM;
DWORD bestI;
double best_actual_ratio;
DWORD Progword;
DWORD Stuffedword;
DWORD bit1;
DWORD bit2;
DWORD bit3;
DWORD i,j;
char buf[256];
unsigned long rangelo;
unsigned long rangehi;
double best_diff;

D = 0x00000000;//from bitcalc, but the datasheet says to make this 1...
bestP = 0;
bestQ = 0;
best_actual_ratio = 1000000.0;//hopefully we can do better than this...
best_diff = 1000000.0;
rangelo = (unsigned long)floor(reffreq/1000000.0) +1;
rangehi = (unsigned long)floor(reffreq/200000.0);
if(rangelo <3) rangelo = 3;
if(rangehi>129) rangehi = 129;
sprintf(buf,"%u(%f), %u(%f)",rangelo,(reffreq/1000000.0) +1.0,rangehi,(reffreq/200000.0));
//MessageBox(buf,"rl(fl),rh(fh)",MB_OK);
for(i=0;i<=7;i++)
{
M = i;
fvco = desired_freq * (pow(2,i));
//sprintf(buf,"%f",fvco);
//MessageBox(NULL,buf,"Fvco",MB_OK);

if(fvco<80000000.0) I = 0x00000000;
if(fvco>=80000000.0) I = 0x00000008;
if((fvco>50000000.0)&&(fvco<150000000.0))
{
sprintf(buf,"%f",fvco);
//MessageBox(NULL,buf,"Fvco",MB_OK);
desired_ratio = fvco/(2.0 * reffreq);
sprintf(buf,"%f",desired_ratio);
//MessageBox(NULL,buf,"desired",MB_OK);

for(P=4;P<=130;P++)for(Q=rangelo;Q<=rangehi;Q++)
{

actual_ratio = (double)P/(double)Q;
if(actual_ratio==desired_ratio) 
	{
	sprintf(buf,"%u,%u",P,Q);
	//MessageBox(NULL,buf,"Direct Hit",MB_OK);
	bestP = P;
	bestQ = Q;
	bestM = M;
	bestI = I;
	best_actual_ratio = actual_ratio;
	goto donecalc;
	}
else 
	{
	if(fabs(desired_ratio - actual_ratio)<(best_diff)) 
		{
		best_diff = fabs(desired_ratio - actual_ratio);
		best_actual_ratio = actual_ratio;
		bestP = P;
		bestQ = Q;
		bestM = M;
		bestI = I;
		sprintf(buf,"desired:%f,actual:%f, best%f P%u,Q%u,fvco:%f,M:%u",desired_ratio,actual_ratio,best_diff,bestP,bestQ,fvco,M);
		//MessageBox(buf,"ratiocalc",MB_OK);
		}
	}	
}
}
}
donecalc:
if((bestP!=0)&&(bestQ!=0))
	{
	//here bestP BestQ are good to go.
	I = bestI;
	M = bestM;
	P = bestP;
	Q = bestQ;
	Pprime = bestP - 3;
	Qprime = bestQ - 2;
	sprintf(buf,"P':%u, Q':%u, M:%u, I:%u",Pprime,Qprime,M,I);
	//MessageBox(buf,"P,Q,M,I",MB_OK);
	Progword = 0;
	Progword =  (Pprime<<15) | (D<<14) | (M<<11) | (Qprime<<4) | I;
	sprintf(buf,"%lx",Progword);
//	MessageBox(buf,"Progword",MB_OK);
	bit1 = 0;
	bit2 = 0;
	bit3 = 0;
	Stuffedword = 0;
	i = 0;
	j = 0;
	bit1 = ((Progword>>i)&1);
	Stuffedword |=  (bit1<<j);
	i++;
	j++;
	bit2 = ((Progword>>i)&1);
	Stuffedword |=  (bit2<<j);
	i++;
	j++;
	bit3 = ((Progword>>i)&1);
	Stuffedword |=  (bit3<<j);
	j++;
	i++;
	while(i<=22)
		{
		if((bit1==1)&&(bit2==1)&&(bit3==1))
			{
			//force a 0 in the stuffed word;
			j++;
			bit3 = 0;
			sprintf(buf,"i,j : %u,%u",i,j);
//			MessageBox(buf,"Stuffing",MB_OK);
			}
		bit1 = bit2;
		bit2 = bit3;
		bit3 = ((Progword>>i)&1);
		Stuffedword |=  (bit3<<j);
		i++;
		j++;
		}
	sprintf(buf,"SW:%lx ,numbits:%u",Stuffedword,j);

clk->clockbits = Stuffedword;
clk->numbits = (USHORT)j-1;
*m_actual_clock = ((2.0 * reffreq) * ((double)P/(double)Q)) / pow(2,M);
//	MessageBox(buf,"stuffedword, numbits",MB_OK);
return TRUE;
	}
else
{
	printf("\r\nError in ICD calculation\r\n");
	return FALSE;

}
}
