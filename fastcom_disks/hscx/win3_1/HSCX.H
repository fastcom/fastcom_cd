#define VHSCXD_DEVICE_ID 0x3530
#define MAX_RBUFS 100           //max rbufs as defined in vhscxd
#define MAX_TBUFS 100           //max tbufs as defined in vhscxd

#define AUTO 1
#define TRANSP 2
#define EXT_TRANSPARENT 3

#define FLAGS  1
#define NO_FLGS  0

#define CD_AS_ON  1
#define CD_AS_OFF  0

#define INTERNAL_T 0
#define EXTERNAL_T 1
#define HIRES   0           
#define LORES   1           
#define REC_ACTIVE  0       
#define REC_INACTIVE  1     
#define TESTLP_OFF    0     
#define TESTLP_ON     1     
#define ENC_NRZ       0  
#define ENC_NRZI      1
#define ENC_BUS1      2
#define ENC_BUS2      3

#define NRM_ON	1
#define NRM_OFF 0


//CMDR commands
#define XRES 1
#define XME 2
#define XIF 4
#define XTF 8
#define STI 16
#define RNR 32
#define RHR 64
#define RMC 128
//ISTA codes
#define RME 128
#define RPF 64
#define RSC 32
#define XPR 16
#define TIN 8
#define ICA 4
#define EXA 2
#define EXB 1
//EXIR codes
#define XMR 128
#define EXE 64
#define PCE 32
#define RFO 16
#define CSC 8
#define RFS 4

//dma controller defines:
#define DMA_PAGE_CH0    0x87
#define DMA_PAGE_CH1    0x83
#define DMA_PAGE_CH2    0x81
#define DMA_PAGE_CH3    0x82
#define DMA_PAGE_CH5    0x8b
#define DMA_PAGE_CH6    0x89
#define DMA_PAGE_CH7    0x8a

#define DMA_BASE_LO     0x00
#define DMA_COMMAND_LO  0x08
#define DMA_MODE_LO     0x0b
#define DMA_REQUEST_LO  0x09
#define DMA_MASK_LO     0x0a
#define DMA_STATUS_LO   0x08
#define DMA_CLEAR_FFLO  0x0c
#define DMA_ADD_CH1     0x02
#define DMA_COUNT_CH1   0x03
#define DMA_ADD_CH3     0x06
#define DMA_COUNT_CH3   0x07

#define DMA_BASE_HI     0xc0
#define DMA_COMMAND_HI  0xd0
#define DMA_MODE_HI     0xd6
#define DMA_REQUEST_HI  0xd2
#define DMA_MASK_HI     0xd4
#define DMA_STATUS_HI   0xd0
#define DMA_CLEAR_FFHI  0xd8
#define DMA_ADD_CH5     0xc4
#define DMA_COUNT_CH5   0xc6
#define DMA_ADD_CH6     0xc8
#define DMA_COUNT_CH6   0xca
#define DMA_ADD_CH7     0xcc
#define DMA_COUNT_CH7   0xce




//structure for buffered frames
struct buf{
unsigned valid;         //indicator 1 = frame[] has data, 0 = frame[] has ???
unsigned no_bytes;      //number of bytes in frame[]
unsigned max;           //maximum number of bytes to send/receive
unsigned char frame[4090];      //data array for received/transmitted data
				//size is +32 for DMA functions that
				//will do at most 16 extra cycles for 
				//odd sized frames
};

//structure for initialization parameters
typedef struct hscx_set{
unsigned mode;          // options: AUTO,TRANSP,EXT_TRANSPARENT
unsigned address_recog; // options:     AUTO:0 = 8bit recog
			//              AUTO:1 = 16bit recog
			//              TRANSP:0 = no recog
			//              TRANSP:1 = 8bit recog in RAH1/2
			//              EXT_TRANSPARENT:0 = bytes in RAL1
			//              EXT_TRANSPARENT:1 = bytes in FIFO
unsigned address_t;     // options:     address_t -> XAD1:XAD2
unsigned address_r;     // options:     address_r -> RAH1:RAL1
unsigned clock_mode;    // options:     0 -> 7 = mode 0 -> 7
unsigned long baud_rate;// options:     1 -> 1000000 (only used in clock modes 2,3,6,7)
unsigned idle;          // options:     NO_FLGS,FLAGS (timefill = none, or flags)
unsigned timer;         // options:     INTERNAL_T, EXTERNAL_T ; use internal with AUTO
unsigned timer_count;   // options:     upper 3 are retry count,lower5 are count
unsigned timer_res;     // options:     HIRES = 512, LORES = 32768
unsigned receive_active;    // options:     REC_ACTIVE, REC_INACTIVE
unsigned test_loop;     // options:     TESTLP_ON, TESTLP_OFF
unsigned encoding;      // options:     ENC_NRZ,ENC_NRZI,ENC_BUS1,ENC_BUS2
unsigned cd_auto_start; // options:     CD_AS_ON, CD_AS_OFF
unsigned num_rbufs;     // options:     1->??
unsigned num_tbufs;     // options:     1->??
unsigned frame_sz;      // options:     1->4094;  
unsigned nrm_mode;		// options:		NRM_ON, NRM_OFF;
}HSCX_SET;


//windows specific defines

#define HSCX_TX_DONE 1
#define HSCX_TX_EXE  2
#define HSCX_RX_RFO  3
#define HSCX_RX_BUF_OVERFLOW 4
#define HSCX_RX_READY 5
#define HSCX_SETUP_OK 6
#define HSCX_TIMER 7
