This is the rev 1.0 HSCX driver for Windows 3.1. It includes:

vhscxd.386      --      A VxD for Windows 3.1
hscxdrva.dll    --      an installable driver interface to the VxD
oemsetup.inf    --      used when installing from control panel/drivers

To install the driver, run the Control Panel Drivers applet and select
"ADD", select "Unlisted or Updated Driver" and select "ok".  Browse to the 
location of the above files and select "ok".  It will respond with 
"HSCX driver rev 1a", select "ok".  It will then install the files (vhscxd.386 
and hscxdrva.dll will go into your windows\system directory, "device=vhscxd.386" 
will be added to your [386enh] section of the system.ini file, and the hscxdrva.dll will
be added to the [Drivers] section of the system.ini file).  When it is done
installing, it will put up a message box saying that vhscxd.386 is missing.
THIS IS NORMAL!!! It just means you haven't rebooted yet.  A dialog box for 
the base address/irq/dmareceive/dmatransmit will open.  Set the values as per
the installed board switches and click "ok".  It will ask you to reboot.  Choose
yes, and you are ready to go.

Also included is an example program WHSCX.EXE and its source codes.

The example program demonstrates how to interface to the FASTCOM:HSCX card
using the following functions.

  OpenDriver()          --opens the hscxdrva.dll for use
  CloseDriver()         --closes the hscxdrva.dll when done
  hscxsettings()        --sets the FASTCOM:HSCX parameters using a struct
  do_setup_dialog()     --uses an internal dialog box to set the parameters
                          in a point and click manner.
  gethscxsettings()     --gets the current settings from the driver 
                          (returned in a struct)
  rec_frame()           --receives a frame if available (returns a pointer to
                          a buf struct)
  hscx_get_xmit_buf()   --returns a pointer to the current tx buf struct
  send_frame()          --sends the current tx buf out the HSCX
  start_hscx_timer()    --starts the on-board HSCX timer

It also demonstrates notification messages when registered using
RegisterWindowMessage("HSCX_NOTIFICATION").
The messages returned will be (in wParam):
  HSCX_TX_DONE        --  a tx buf frame is done sending
  HSCX_TX_EXE         --  a transmit underrun occurred, (board isn't getting 
                          serviced quickly enough..reduce baud rate)
  HSCX_RX_RFO         --  HSCX receive overflow (internal, chip level)              
  HSCX_RX_BUF_OVERFLOW--  The rx buf's are not being serviced soon enough 
                          using rec_frame and rbuf->valid = 0, so all of the
                          receive buffers are full (software level)
  HSCX_RX_READY       --  a complete frame has been received and is available
                          to rec_frame()
  HSCX_TIMER          --  The onboard hscx timer has expired
  HSCX_SETUP_OK       --  setup is complete

The example program was designed to connect two HSCX boards (in different 
computers) and has been tested in the following configuration:

        Board1:                         Board2:
        sw1:280h                        sw1:280h
        sw2:all off                     sw2:all off
        sw3:irq 5                       sw3:irq 5
        sw4: 8 on only                  sw4:6&2 on only
        sw5: 4 on only                  sw5:4 on only
        sw6: 3&4on only                 sw6:3&4 on only
          this is the slave system        this is the master system
        
        The cable connecting the two is as follows:
        
        DB25F                           DB25F
        1            <->                1
        2            -->                3
        3            <--                2
        7--]                         [--7
        8--]                         [--8
        9            <--                11
        14           -->                16
        16           <--                14
        17           <--                24

This crosses the rx and tx pins between the master and the slave and connects
the Txclk of the master to the Rxclk of the slave the 7-8 jumpers are to force
Axclk to active so that it will receive in clock mode 1.

The Settings dialog is setup for both master and slave:

        Transparent mode
        External timer
        8 bit recog.       (== no recognition in transparent mode)
        Rec and tx addresses = 0xffff
        NRZ encoding
        receive buffers >=2
        tx buffers >=1
        receive active on
        idle flags off
        cd auto off
        testloop off
        frame size 2 -> 4090

Specific to each board:

Master:
        Clock Mode 2
        Baud rate --any
Slave:
        Clock Mode 1
        Baud rate --any

At this point, any key pressed in the Whscx main window will fill a buffer
with frame size number of that key and send it to the other board, where it 
will be received and displayed.  It also has a simple file transfer demo under
FILE/SEND, which will let you select a file and send it to the other side.
The protocol is not very strong, it will start a file transfer and acknowledge
each frame that is received.

Other settings for the boards and driver will work, and some will not. 
Specifically, DMA can be used.
The 4Meg mode can be used on the MASTER (DMA highly reccommended).
Both MASTER and SLAVE can be set to clock mode 2 and no clock lines wired.
If both are set to the same baud rate, it will work by recovering the clock
from the data stream.
Extended transparent mode will not work with DMA, and probably won't work 
with this demo.
Auto Mode will not work with this demo without modification 
(the receive buffers will get an extra byte at the beginning, so that byte 
must be stripped out before being processed).



rev 1.0 cg 5-31-95

