/*++

Copyright (c) 1995,1997,1998 Commtech, Inc. Wichita, KS

Module Name:

    HSCXdrv.h

Abstract:

Environment:


Revision History:


--*/

typedef struct setup{
unsigned mask;
unsigned mode;
unsigned timr;
unsigned xad1;
unsigned xad2;
unsigned rah1;
unsigned rah2;
unsigned ral1;
unsigned ral2;
unsigned xbcl;
unsigned bgr;
unsigned ccr2;
unsigned xbch;
unsigned rlcr;
unsigned ccr1;
unsigned tsax;
unsigned tsar;
unsigned xccr;
unsigned rccr;
DWORD port;
unsigned txtype;

unsigned n_rbufs;
unsigned n_tbufs;
unsigned n_rfsize_max;
unsigned n_tfsize_max;

}SETUP;


typedef struct clkset{
	DWORD port;
	ULONG clockbits;
	DWORD numbits;
} CLK;

typedef struct reg{
	DWORD port;
	DWORD reg_offset;
	DWORD reg_value;
} REGSINGLE;



//CMDR commands
#define XRES 1
#define XME 2
#define XIF 4
#define XTF 8
#define STIB 16
#define RNR 32
#define RHR 64
#define RMC 128
//ISTA codes
#define RME 128
#define RPF 64
#define RSC 32
#define XPR 16
#define TIN 8
#define ICA 4
#define EXA 2
#define EXB 1
//EXIR codes
#define XMR 128
#define EXE 64
#define PCE 32
#define RFO 16
#define CSC 8
#define RFS 4
//STAR codes
#define CEC 4
#define WFA 1
#define RLI 8
#define XFW 64
#define XDOV 128


#define ISTAT 0
#define EXIR 4
#define MODEB 2
#define STAR 1
#define CMDR 1
#define XAD1 4
#define XAD2 5
#define RAH2 7
#define CCR2 0x0c
#define CCR1 0x0f
#define FIFO 0x14
#define MASK 0
#define RBCL 5
#define RBCH 0x0d
#define BGR  0x0b
#define RAL1 8
#define RHCR 9
#define XBCL 0x0a
#define XBCH 0x0d
#define RAH1 6
#define RAL2 9
#define TIMR 3
#define RLCR 0x0e
#define VSTR 0x0e

#define TSAX 0x10
#define TSAR 0x11
#define XCCR 0x12
#define RCCR 0x13
#define DSRR 0x15
#define DTRR 0x15



struct board_switches
{
unsigned base;
unsigned irq;
unsigned dmar;
unsigned dmat;
};


//device io control STATUS function return values
#define ST_RX_DONE		0x00000001
#define ST_OVF			0x00000002
#define ST_RFS			0x00000004
#define ST_RX_TIMEOUT	0x00000008
#define ST_RSC			0x00000010
#define ST_PERR			0x00000020
#define ST_PCE			0x00000040
#define ST_FERR			0x00000080
#define ST_SYN			0x00000100
#define ST_DPLLA		0x00000200
#define ST_CDSC			0x00000400
#define ST_RFO			0x00000800
#define ST_EOP			0x00001000
#define ST_BRKD			0x00002000
#define ST_ONLP			0x00004000
#define ST_BRKT			0x00008000
#define	ST_ALLS			0x00010000
#define ST_EXE			0x00020000
#define ST_TIN			0x00040000
#define ST_CTSC			0x00080000
#define ST_CSC 			0x00080000
#define ST_XMR			0x00100000
#define ST_TX_DONE		0x00200000
#define ST_DMA_TC		0x00400000
#define ST_DSR1C		0x00800000
#define ST_DSR0C		0x01000000
#define TX_FUBAR		0x02000000

//hscxdrv.vxd ioctl functions
#define HSCX_READ			1
#define HSCX_WRITE			2
#define HSCX_INITIALIZE		3
#define HSCX_SET_CLOCK		4
#define HSCX_READ_REGISTER	5
#define HSCX_WRITE_REGISTER 6
#define HSCX_STATUS			7
#define HSCX_FLUSH_RX		8
#define HSCX_FLUSH_TX		9
#define HSCX_ADD_BOARD		10
#define HSCX_GET_MAX_PORT	11


//hscxdrv.vxd specific return values
#define ERROR_BAD_PORT_NUMBER			-2
#define ERROR_NO_DATA_RECEIVED			-3
#define ERROR_USER_BUFFER_TOO_SMALL		-4
#define ERROR_TRANSMIT_FRAME_TOO_LARGE	-5
#define ERROR_TRANSMIT_BUFFERS_FULL		-6
#define ERROR_TRANSMITTER_LOCKED_UP		-7
#define ERROR_CEC_TIMEOUT				-8
#define ERROR_MAX_BOARDS_INSTALLED		-9
#define ERROR_IRQ_IN_USE				-10
#define ERROR_NO_PORTS_DEFINED			-11
#define ERROR_READ_BUSY					-12
#define ERROR_BOARD_EXISTS				-13
#define ERROR_TOO_MANY_RBUFS			-14
#define ERROR_TOO_MANY_TBUFS			-15
#define ERROR_MAX_FRAME_SIZE			-16
#define ERROR_MEM_ALLOCATION			-17
#define ERROR_MIN_FRAME_SIZE			-18







