--BETA--BETA--BETA--BETA--BETA--
This driver and information is subject to change.
Initial tests show that this driver does function.  However, if
you do too much outside of what has been tested it will probably fail.
If it does fail, details about what failed and how you achieved it would
be helpful, email bug reports to commtech@southwind.net.
--BETA--BETA--BETA--BETA--BETA--

Windows 95
FASTCOM:HSCX driver highlights

Maximum number of HSCX boards supported: 6 
HSCX operating modes supported: HDLC(Auto mode, Transparent mode)
Data rates:     IRQ driven to 1Mbps (depends a lot on cpu/system speed, and can be as higher than 1Mbps)
				DMA is not supported in this driver.

Installation:
No install notes are really necessary, just make sure that the hscxdrv.vxd is 
somewhere in the path, or in the directory of the program that opens it.


Interface:
The Driver is based on the Win32 CreateFile() / DeviceIoControl() interface.
The driver is loaded when CreateFile is called as:

hHSCX = CreateFile("\\\\.\\HSCXDRV.VXD", 0,0,0,
		   CREATE_NEW, FILE_FLAG_DELETE_ON_CLOSE|FILE_FLAG_OVERLAPPED, 0);

The driver must be told where the boards are located using:
The board_switches structure is defined in the hscxdrv.h file.
The .base parameter is the address of the board as set on the base address switch.
The .irq parameter is the IRQ set on the board.
The .dmat .dmar are for future expansion (DMA settings),
and are not used by this driver.

DeviceIoControl(hHSCX, HSCX_ADD_BOARD,
		&boardsw, sizeof(struct board_switches),
				(LPVOID)NULL, 0,
		&cbBytesReturned, NULL);
//returns true if no problems found.
//returns ERROR_INVALID_PARAMETER if the input is not sizeof(struct board_switches)
//returns ERROR_MAX_BOARDS_INSTALLED if there are allready 3 boards.
//returns ERROR_BOARD_EXISTS if you have allready used that base address
//returns ERROR_IRQ_IN_USE if the driver cannot hook the IRQ


Once this function has been called the driver knows about the board, you can verify that
the board was found, and the irq hooked by calling:

DeviceIoControl(hHSCX, HSCX_GET_MAX_PORT,
		(LPVOID)NULL,0,
				&i, sizeof(DWORD),
		&cbBytesReturned, NULL))
//returns true and i = maximum usable port # (starting at 0)
//or ERROR_INVALID_PARAMETER if a DWORD is not given for the output buffer
//or ERROR_NO_PORTS_DEFINED if no ports are known to the driver

Now the driver knows about the board and it has been verified so all you need to do is
give the port settings for a port using:

The SETUP structure is defined in the hscxdrv.h file and is basically the 82526
registers, with the port and buffering information.

DeviceIoControl(hHSCX, HSCX_INITIALIZE,(LPVOID)&settings, sizeof(SETUP),(LPVOID)NULL, 0,&cbBytesReturned, NULL) )
//returns true if settings successfull
//returns ERROR_INVALID_PARAMETER if SETUP struct not passed
//returns ERROR_BAD_PORT_NUMBER if port is out of range
//returns ERROR_CEC_TIMEOUT if the command executing bit times out(ie no core clock)
//returns ERROR_TOO_MANY_RBUFS if n_rbufs > 1000
//returns ERROR_TOO_MANY_TBUFS if n_tbufs > 1000
//returns ERROR_MAX_FRAME_SIZE if n_rfsize_max or n_tfsize_max > 4096
//returns ERROR_MIN_FRAME_SIZE if n_rfsize_max or n_tfsize_max < 32
//returns ERROR_MEM_ALLOCATION if the driver cannot get enough memory to fill the requested sizes


Now it is possible to read/write to the device and have it actually do something.

To read a frame use:

DeviceIoControl(hHSCX, HSCX_READ,&Port, sizeof(DWORD),buffer, sizeof(buffer),&cbBytesReturned, &osr);
//returns true if a frame is received and copied to the buffer (in HDLC mode the last byte is the contents of the RSTA register)
//cbBytesReturned will be the number of bytes copied to the buffer
//or ERROR_INVALID_PARAMETER if not a DWORD and char array passed
//or ERROR_BAD_PORT_NUMBER if the port is out of range
//or ERROR_NO_DATA_RECEIVED if there are not any valid receive buffers waiting in the driver
//or ERROR_USER_BUFFER_TOO_SMALL if the received frame is bigger than the buffer passed.
//or ERROR_IO_PENDING (if overlapped was specified and given and the driver is not currently holding a received frame)

To write a frame out a port use:

DeviceIoControl(hHSCX, HSCX_WRITE,&tbuf, bytestosend,(LPVOID)NULL,0,&cbBytesReturned, &osw);
//returns true if a frame is written or queued
//ERROR_INVALID_PARAMETER if the bytestosend is not at least a sizeof(DWORD)(indicating a port value)
//ERROR_BAD_PORT_NUMBER if the port is out of range
//ERROR_TRANSMIT_FRAME_TOO_LARGE if bytestosend is > sizeof(DWORD)+4096
//ERROR_TRANSMIT_BUFFERS_FULL if all of the internal buffers are used and queued for transmit
//ERROR_TRANSMITTER_LOCKED_UP if the driver thinks that it is not transmitting but a valid transmit buffer is found.(requires a flush to transmit anything)
//ERROR_CEC_TIMEOUT if a command is executing (probably no core clock source (clock mode dependant))
//ERROR_IO_PENDING (if overlapped was specified and given and the driver transmit buffers are full)
make sure that your tbuf is setup as (or something similar):

struct mydata
{
DWORD port;
char data[n_tfsize_max];
};
and bytestosend = # bytes you want to go out in the frame plus sizeof(DWORD)
so if you want to send 256 bytes in your frame you would have:
bytestosend = 256 + sizeof(DWORD);
Be carefull of structure alignment, the driver expects the port number to be
the first DWORD in the buffer that is passed, and assumes that the next byte is
the start of data.


To check the status of a port use:

DeviceIoControl(hHSCX, HSCX_STATUS,
		&Port,sizeof(DWORD),
		&stat, sizeof(unsigned long),
		&cbBytesReturned, NULL);
//returns true with unsigned long status or (reads and clears the internal status value)
//ERROR_INVALID_PARAMETER if DWORD and unsigned long not passed
//ERROR_BAD_PORT_NUMBER if the port is out of range
//ERROR_IO_PENDING if overlapped was specified and overlapped struct is passed, and no status events have occured.

This returns the status values as defined in the hscxdrv.h file.  Most
of these values are reflections of 82526 interrupt sources. (these sources were based
on the ESCC driver so there are a few status returns that will never happen with
this driver, but will happen with the ESCC driver/board (like ST_ALLS and a few others)).

The Full list of available DeviceIoControl functions:

HSCX_READ                       -- used to read a buffer/frame from the driver
HSCX_WRITE                      -- used to write a buffer/frame to the driver
HSCX_INITIALIZE         -- used to initialize the port (register settings)
HSCX_SET_CLOCK          -- used to set the ICD2053b clock generator (changes the OSC input to the 82532)
HSCX_READ_REGISTER      -- used to obtain a single register from the 82526
HSCX_WRITE_REGISTER -- used to write a single register to the 82526
HSCX_STATUS                     -- used to get the port status from the driver
HSCX_FLUSH_RX           -- used to flush the receive buffers for the port
HSCX_FLUSH_TX           -- used to flush the transmit buffers for the port
HSCX_ADD_BOARD          -- used to add ports to the drivers list of known ports
HSCX_GET_MAX_PORT       -- used to find out how many ports the driver knows about

The READ_REGISTER and WRITE_REGISTER functions should not be used to change
operating modes.  In general you should be very careful with these functions, as
they have very little protection and can cause many bad things to happen internally 
with the driver.  They can safely be used to set/change the address registers, and 
to send some commands (like STI, RHR, RNR) but should not be used with RMC,RHR,XTF,XIF,XME, or XRES commands,
or to change any of the configuration registers (MODE,CCR1,CCR2,MASK).  You have been warned.

Examples of usage of these functions can be found in the hscxtest.c file.


changes and modifications:

11/1/98 Initial Beta release

2/11/99 Fixed Tx/Rx queuing routines.  Fixed short frame sends.


