#include "chscx.h"
#include "stdio.h"
#include "stdlib.h"
#include "conio.h"
#include "bios.h"
#include "dos.h"
#include "string.h"

Chscx hscxdrv;		//driver with ISR
void main(int argc, char *argv[])
{
char buf[4096];		//char buffer for calls to driver
char *data;			//pointer to data area of buffer
char *control;		//pointer to control byte of buffer

FILE *fin;			//file pointer for all file operations
unsigned port;      //storage for port# that is returned from driver
unsigned i,j,k;     //temps
char fname[256];    //file name storage
unsigned long fsize;    //file size
unsigned long rsize;    //received file size (current bytes received)
unsigned long allbytes; //all bytes including overheads
unsigned long totbytes; //all bytes sent from file
unsigned filexfr;       //flag for file transfer in progress
char frame_count;           //frame counter for sequencing of frames
char expected_count;        //frame counter for sequencing of frames
unsigned receive_active;    //flag for if the code is sending or receiving
unsigned txonce;

char temp[256];
char *t1;
unsigned base;
unsigned irq;
unsigned dmar;
unsigned dmat;                       
unsigned long clock;
unsigned nbits;
char filename[256];
unsigned ccr1;
unsigned ccr2;
unsigned bgr1;
unsigned mode;
unsigned timr;
unsigned xbch;
unsigned bufr;
unsigned buft;


receive_active =0;          //default = transmit
filexfr=0;                  //not in a file transfer yet

txonce =0;
if(argc<2)
	{
	printf("Usage:\r\n");
	printf("Filename param1 param2 param3 [param4]\r\n");
	printf("valid parameters are:\r\n");
	printf("-b:xxxx 		--Base address in HEX\r\n");
	printf("-i:uu   		--IRQ level\r\n");
	printf("-r:u    		--DMA receive channel\r\n");
	printf("-t:u    		--DMA transmit channel\r\n");
	printf("-c:xxxxxxxx 	--value to send to 2053b clock generator\r\n");
	printf("-nb:uu		--number of bits of c: value to send\r\n");
	printf("-ccr1:xx		--ccr1 register value in HEX\r\n");
	printf("-ccr2:xx		--ccr2 register value in HEX\r\n");
	printf("-bgr1:xx		--bgr  register value in HEX\r\n");
	printf("-mode:xx		--mode register value in HEX\r\n");
	printf("-timr:xx		--timr register value in HEX\r\n");
	printf("-xbch:xx		--xbch register value in HEX\r\n");
	printf("-bufr:uu		--number of receive buffers\r\n");
	printf("-buft:uu		--number of transmit buffers\r\n");
	printf("-f:ssssssss.sss -- file name to process\r\n");	
	printf("-rf				--set to receive file mode\r\n");
	printf("-d				--set to debug (transmit one frame per key) file mode\r\n");
	exit(1);
	}
//defaults 
base = 0x280;
irq = 5;
dmar = 0;
dmat = 0;
clock = 0x333208;
nbits = 22;

//clock = 0x1d40e0;	//4M, could also do 
					//8M  0x1d30e0	(24 bits)
					//1.544M  0x333208 (22 bits)
					//2.048M  0x142930 (22 bits)
					//2M	0x1d50e0 (24 bits)
					//use bitcalc to come up with others pay attention to how many bits are displayed in the
					//stuffed bits field (to use in the -nb: field
//nbits = 24;
ccr1 = 0x94;	//default to running off of osc input //was 92 for bgr mode
ccr2 = 0x08;	//default for clock mode 4 //default to baud mode 0x38
mode = 0x88;	//hdlc transparent mode
timr = 0xe2;
xbch = 0;                         
bufr = 10;
buft = 10;

for(i=1;i<argc;i++)
	{
	if(argv[i][0]=='-')
		{
		t1 = &argv[i][1];
		if(strncmp(t1,"rf",2)==0)
			{
			receive_active = 1;
			}                  
		if(strncmp(t1,"d",1)==0)
			{
			txonce = 1;
			}
		if(strncmp(t1,"b:",2)==0)
			{
			sscanf(t1+2,"%x",&base);
			}
		if(strncmp(t1,"i:",2)==0)
			{
			sscanf(t1+2,"%u",&irq);
			}
		if(strncmp(t1,"r:",2)==0)
			{
			sscanf(t1+2,"%u",&dmar);
			}                       
		if(strncmp(t1,"t:",2)==0)
			{
			sscanf(t1+2,"%u",&dmat);
			}
		if(strncmp(t1,"c:",2)==0)
			{
			sscanf(t1+2,"%lx",&clock);
			}
		if(strncmp(t1,"nb:",3)==0)
			{
			sscanf(t1+3,"%u",&nbits);
			}
		if(strncmp(t1,"f:",2)==0)
			{
			sscanf(t1+2,"%s",filename);
			}
		if(strncmp(t1,"ccr1:",5)==0)
			{
			sscanf(t1+5,"%x",&ccr1);
			}
		if(strncmp(t1,"ccr2:",5)==0)
			{
			sscanf(t1+5,"%x",&ccr2);
			}           
		if(strncmp(t1,"bgr1:",5)==0)
			{
			sscanf(t1+5,"%x",&bgr1);
			}           
		if(strncmp(t1,"mode:",5)==0)
			{
			sscanf(t1+5,"%x",&mode);
			}           
		if(strncmp(t1,"timr:",5)==0)
			{
			sscanf(t1+5,"%x",&timr);
			}           
		if(strncmp(t1,"xbch:",5)==0)
			{
			sscanf(t1+5,"%x",&xbch);
			}           
		if(strncmp(t1,"bufr:",5)==0)
			{
			sscanf(t1+5,"%u",&bufr);
			}           
		if(strncmp(t1,"buft:",5)==0)
			{
			sscanf(t1+5,"%u",&buft);
			}           

			
		}
	}
	
printf("HSCX:\r\n");
printf("BASE:%x\r\n",base);
printf("IRQ :%u\r\n",irq);
printf("DMAR:%u\r\n",dmar);
printf("DMAT:%u\r\n",dmat);
printf("CLK :%lx\r\n",clock);
printf("BITS:%u\r\n",nbits);
printf("filename -- %s\r\n",filename);
if(receive_active==1) printf("RECEIVE FILE MODE\r\n");
if(txonce==1) printf("TX ONCE DEBUG FILE MODE\r\n");
//intf("start\n\r");

if(receive_active==0)           //this is the transmit section
{
printf("adding board\n\r");
port = hscxdrv.add_port(base,irq,dmar,dmat);		//board at 280h I15, no dma
printf("port1 = %u\n\r",port);
hscxdrv.set_clock_generator(port,clock,nbits); //set clock up

printf("initing board for transmit\n\r");
if(hscxdrv.init_port(port,				//port
					0xee,				//mask
					0,				//cmdr
					mode,			//mode 0x88
					timr,			//timr (periodic)
					0xff,			//xad1
					0xff,			//xad2
					0xff,			//rah1
					0xff,			//rah2
					0xff,			//ral1
					0xff,			//ral2
					0,				//xbcl
					bgr1,			//bgr
					ccr2,			//ccr2 (1Mbaud)
					xbch,              //xbch
					0,				//rlcr
					ccr1,			//ccr1 (0xda  for NRZI with flags for IDLE) (92 = NRZ no flags on idle)
					0,				//tsax
					0,				//tsar
					0,				//xccr
					0,				//rccr
					bufr,				//#rbufs  (not used for transmit side leave = 2
					buft				//#tbufs  (adjustable within limits)
					) == FALSE)
	{
	printf("cannot init board\n\r");
	exit(1);
	}
hscxdrv.set_tx_type(port,TRANSPARENT_MODE); //type of frames to send,set call to driver

//get filename to send
fin = fopen(filename,"rb");                  //open file to tx
if(fin==NULL)                               //open failed
	{
	printf("cannot open file:%s\r\n",filename);
	exit(1);
	}
fseek(fin,0,SEEK_END);                      //get file size
fsize = ftell(fin);
fseek(fin,0,SEEK_SET);
printf("File:%s Size:%lu\r\n",filename,fsize);   //display stats
//form opening frame:
control = &buf[0];                              //set control and data pointers
data = &buf[1];
control[0] = 0x00;	//show that this is a header frame bit 7 clear
//header format byte 1 = 00 for control header byte (indicates a filename/size data frame)
//data is: filename, size 
//ending NULL is transmitted.
sprintf(data,"%s , %lu ",filename,fsize); 
printf("%s , %lu :CTRL:%x\r\n",filename,fsize,(char)control[0]&0xff); 

while(hscxdrv.tx_port(port,buf,strlen(data)+2)==0);		//send header frame
//set control frame to data:
//high bit set indicates a data frame
i = 0x80;
control[0] = (char)i;         
frame_count = 0;
totbytes = 0;              
allbytes = 0;
do
{
if(txonce==1)
{
printf("paused\r");  //usefull for debugging (sends one frame per keypress)
getch();
}
j = fread(data,1,4000,fin);         //get data from file
if(j!=0)
	{
	//data is ready to send

	while(hscxdrv.tx_port(port,buf,j+1)==0)     //driver returns number of bytes sent wait until it is >0
		{
		printf("waiting for a tx buffer\r");
		//_getch();
		}
	totbytes = totbytes +j;                     //total bytes sent (file) 
	allbytes = allbytes + j +5;                     //total bytes sent (actual including overhead;2 flags, 2byte CRC 1 Control byte)
	printf("file bytes transmitted:%lu -- total transmitted:%lu  CTRL:%x\r",totbytes,allbytes,(char)control[0]&0xff);	//display
	frame_count++;							//prep next frame count
	control[0] = (frame_count&0x7f) + 0x80; //setup control byte for next send high bit set + sequence #
	}
else
	{
	if(feof(fin)==0)printf("\r\nread error occured\r\n");   //file read error
	}
}while(feof(fin)==0);               //do until end of file
fclose(fin);                        //close file
printf("file:%s\r\n",filename);      //display statistics
printf("size:%lu\r\n",fsize);
printf("bytes transmitted:%lu\r\n",allbytes);
printf("complete\r\n");
printf("file bytes transmitted:%lu -- total transmitted:%lu\r",totbytes,allbytes);
printf("\r\n");
printf("any key to exit\r\n");      //pause (this is necessary to let any queued frames to be sent before the program exits)
getch();                            
}
else
{                                   //this is the receive section
printf("adding board\n\r");
port = hscxdrv.add_port(base,irq,dmar,dmat);		//board at 280h I15, rdma = 1 tdma = 7
printf("port1 = %u\n\r",port);          //(note use DMA on the receive side)

//do receive file
printf("initing board for receive\n\r");
if(hscxdrv.init_port(port,				//port
					0x3e,				//mask
					0,				//cmdr
					mode,			//mode 0x88
					timr,			//timr (periodic)
					0xff,			//xad1
					0xff,			//xad2
					0xff,			//rah1
					0xff,			//rah2
					0xff,			//ral1
					0xff,			//ral2
					0,				//xbcl
					bgr1,			//bgr
					ccr2,			//ccr2
					xbch,              //xbch
					0,				//rlcr
					ccr1,			//ccr1  (0xda = NRZI with flags as idle) set for NRZ with no flags on idle (this setting should match the above setting)
					0,				//tsax
					0,				//tsar
					0,				//xccr
					0,				//rccr
					bufr,				//#rbufs
					buft				//#tbufs
					) == FALSE)
	{
	printf("cannot init board\n\r");
	exit(1);
	}
hscxdrv.set_tx_type(port,TRANSPARENT_MODE);	//not used
filexfr=0;	                                            //no file in progress
fsize = 0;			//default filesize = 0
rsize = 0;			//received so far = 0
printf("waiting for next file (any key exits)\r\n");		//message
startit:
do
{
j = 0;
while( (j&RX_READY) !=RX_READY )
		{
		j = hscxdrv.get_port_status(port);	//loop here until data is ready
		printf ("\r",j);
		if(j!=0) 
			{                                                
//			printf("\n\r");
//			if((j&XMR_INTERRUPT)==XMR_INTERRUPT) printf("\n\rXMR\n\r");
//			if((j&PCE_INTERRUPT)==PCE_INTERRUPT) printf("\n\rPCE\n\r");
//			if((j&EXE_INTERRUPT)==EXE_INTERRUPT) printf("\n\rEXE\n\r");
			if((j&RFO_INTERRUPT)==RFO_INTERRUPT) printf("\n\rRFO\n\r");    //== receive frame overflow (hardware)
//			if((j&CTS_INTERRUPT)==CTS_INTERRUPT) printf("\n\rCTS\n\r");
//			if((j&RFS_INTERRUPT)==RFS_INTERRUPT) printf("\n\rRFS\n\r");
			if((j&RX_BUFFER_OVERFLOW)==RX_BUFFER_OVERFLOW)					//== software buffers overflowed (increase #rbufs)
				{
				printf("\n\rRX_BUFFER_OVERFLOW\n\r");
				exit(2);
				}
//			if((j&RSC_INTERRUPT)==RSC_INTERRUPT) printf("\n\rRSC\n\r");
//            if((j&TIMER_INTERRUPT)==TIMER_INTERRUPT) printf("\n\rTIMER\n\r");
//            if((j&RX_READY)==RX_READY) printf("\n\rRX READY\n\r");
			}
		if(kbhit()!=0) goto leavenow;
		}
while((j=hscxdrv.rx_port(port,buf,4096))==0); //get data
saveit:
//printf("ctrl:%x --Size:%u --RSTA:%x",(char)buf[0]&0xff,j,buf[j-1]);

if(((buf[j-1]&0x20)==0)||((buf[j-1]&0x40)==0x40))       //check RSTA for RFO and CRC errors
	{
	if((buf[j-1]&0x20)==0)printf("\r\nCRC-FAIL, aborting current operation\r\n");
	if((buf[j-1]&0x40)==0x40)printf("\r\nRFO-FAIL, aborting current operation\r\n");
	fclose(fin);                        //abort current file if it exists
	filexfr=0;	                        
	expected_count=0;
	}
else
{		//CRC checks OK so decode the frame
if(buf[0]==0)	//check control byte for header /data frame
{
//is a header frame are we allready receiveing a file?
if(filexfr==0)
	{
	//no so start the file transfer (open the file)
	//file transfer                     
	sscanf(&buf[1],"%s , %lu ",&fname,&fsize); 	//get filename and size from data frame
	printf("\r\nfile:%s , size:%lu\r\n",fname,fsize);
	fin = fopen(fname,"wb");                            //open the file
	if(fin==NULL)                                      	//error opening the file
		{
		printf("\r\nCannot open output file:%s\r\n",fname);
		break;
		}
	rsize = 0;     			//start with size = 0
	filexfr=1;	                        //we are doing a transfer now
	expected_count=0;					//initial frame sequence = 0
	}
else
	{
	//allready receiving a file and received another header
	printf("\r\nreceived header in middle of file\r\n"); //probably a severe error
	}
	
}
if( (buf[0]&0x80)==0x80) //is a data frame
{
if(filexfr==1)
	{                                     
	//we allready have a file open so save the data frame to the file
	
	//we have a data file and are about to get data for it
	if(j>=2) //must be at least 2 bytes long (byte 1 is the control byte, the last byte is the RSTA register from the HSCX
		{
		//check message sequence counter:
		if((buf[0]&0x7f)==(expected_count&0x7f))
			{
			k = fwrite(&buf[1],1,j-2,fin); //write the data to the file
			if(k != (j-2))
				{
				printf("\r\nfile write error\r\n");
				}
			rsize = rsize + k;	//update the # bytes received
			expected_count++;	//update the sequence counter
			}
		else
			{
			//the sequence numbers are off...probably should abort here
			printf("\r\nmessages out of sequence--aborting file transfer\r\n");                

			fclose(fin);	//stop data transfer
			filexfr=0;	                        //no longer in file xfr
			expected_count=0;					//reset sequence count
			}
		}
	else
		{
		printf("\r\ninvalid frame received\r\n"); //frame size must be bigger than 2
		}
	printf("%s:%lu/%lu --%u\r",fname,rsize,fsize,j);	//display stats
if(!((rsize<fsize)&&(filexfr==1)))			//check for end of file
	{
	//display end messge and reset file transfer vars.
	printf("\r\n");
	printf("File complete:%s\r\nReported size: %lu\n\rActual size:%lu\r\n",fname,fsize,rsize);
	fclose(fin);
	filexfr = 0;
	rsize = 0;
	fsize = 0;
	printf("waiting for next file (any key exits)\r\n");	
	break;
	}

	}
else
	{
    //executes this code if a data frame is received and no open file transfer is in progress
	}	

}//end of data frame block

if((j=hscxdrv.rx_port(port,buf,4096))!=0) //check if there is more data to get
	{
	hscxdrv.get_port_status(port); //clear the status
	
	goto saveit; // decode the frame - save the data 
	}
}//end of good frame block

}while((rsize<fsize)&&(filexfr==1));	//back to the main loop
goto startit;	//back to main loop

//exit code reached if a keypress happens while waiting for a received frame
leavenow:       
getch();
printf("\n\r");
printf("hit a key to leave\n\r");
getch();	

}//end of receive option block


}//end of main
