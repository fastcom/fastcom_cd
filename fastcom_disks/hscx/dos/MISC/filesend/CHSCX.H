#define FRAME_SIZE 4093         //maximum size of a received frame 
#define MAX_RBUFS 25            //number of receive buffers to use 
#define MAX_TBUFS 25
#define MAX_PORTS 6

#define AUTO 1
#define TRANSPARENT 2
#define EXT_TRANSPARENT 3

#define FLAGS  1
#define NO_FLGS  0

#define INTERNAL_T 0
#define EXTERNAL_T 1

#define AUTO_MODE 1
#define TRANSPARENT_MODE 2
//CMDR commands
#define XRES 1
#define XME 2
#define XIF 4
#define XTF 8
#define STI 16
#define RNR 32
#define RHR 64
#define RMC 128
//ISTA codes
#define RME 128
#define RPF 64
#define RSC 32
#define XPR 16
#define TIN 8
#define ICA 4
#define EXA 2
#define EXB 1
//EXIR codes
#define XMR 128
#define EXE 64
#define PCE 32
#define RFO 16
#define CSC 8
#define RFS 4

#define ISTAT 0
#define EXIR 4
#define MODE 2
#define STAR 1
#define CMDR 1
#define XAD1 4
#define XAD2 5
#define RAH2 7
#define CCR2 0x0c
#define CCR1 0x0f
#define FIFO 0x14
#define MASK 0
#define RBCL 5
#define RBCH 0x0d
#define BGR  0x0b
#define RAL1 8
#define RHCR 9
#define XBCL 0x0a
#define XBCH 0x0d
#define RAH1 6
#define RAL2 9
#define TSAX 0x10
#define TSAR 0x11
#define XCCR 0x12
#define RCCR 0x13 
#define TIMR 3
#define RLCR 0x0e	//write only
#define DTRR 0x15
#define VSTR 0x0e	//read only
//dma controller defines:
#define DMA_PAGE_CH0    0x87
#define DMA_PAGE_CH1    0x83
#define DMA_PAGE_CH2    0x81
#define DMA_PAGE_CH3    0x82
#define DMA_PAGE_CH5    0x8b
#define DMA_PAGE_CH6    0x89
#define DMA_PAGE_CH7    0x8a

#define DMA_BASE_LO     0x00
#define DMA_COMMAND_LO  0x08
#define DMA_MODE_LO     0x0b
#define DMA_REQUEST_LO  0x09
#define DMA_MASK_LO     0x0a
#define DMA_STATUS_LO   0x08
#define DMA_CLEAR_FFLO  0x0c
#define DMA_ADD_CH1     0x02
#define DMA_COUNT_CH1   0x03
#define DMA_ADD_CH3     0x06
#define DMA_COUNT_CH3   0x07

#define DMA_BASE_HI     0xc0
#define DMA_COMMAND_HI  0xd0
#define DMA_MODE_HI     0xd6
#define DMA_REQUEST_HI  0xd2
#define DMA_MASK_HI     0xd4
#define DMA_STATUS_HI   0xd0
#define DMA_CLEAR_FFHI  0xd8
#define DMA_ADD_CH5     0xc4
#define DMA_COUNT_CH5   0xc6
#define DMA_ADD_CH6     0xc8
#define DMA_COUNT_CH6   0xca
#define DMA_ADD_CH7     0xcc
#define DMA_COUNT_CH7   0xce
			
//status function defines
#define XMR_INTERRUPT   0x0001
#define EXE_INTERRUPT   0x0002
#define PCE_INTERRUPT   0x0004
#define RFO_INTERRUPT   0x0008
#define CTS_INTERRUPT   0x0010
#define RFS_INTERRUPT   0x0020
#define RX_BUFFER_OVERFLOW 0x0040
#define RSC_INTERRUPT   0x0080
#define TIMER_INTERRUPT 0x0100
#define RX_READY        0x0200

#define TRUE 1                                                                  
#define FALSE 0

//defines for 2053b
#define STARTWRD 0x1e05
#define MIDWRD   0x1e04
#define ENDWRD   0x1e00


struct buf{
unsigned valid;         //indicator 1 = frame[] has data, 0 = frame[] has ???
unsigned no_bytes;      //number of bytes in frame[]
unsigned max;           //maximum number of bytes to send/receive
unsigned frame[FRAME_SIZE];  //data array for received/transmitted data
//note that this struct is exactly 8192 bytes long which allows it to be arrayed and accessed using huge pointers				
//note also that this is somewhat wastefull of memory, but it is the least common denominator for using
//mixed dma. (using 16 bit DMA channels causes twice the memory to be read/written).  If DMA is not being used
//or only 8 bit dma channels are being used it is better to change the unsigned frame[FRAME_SIZE] to a 
//char frame[FRAME_SIZE] to conserve memory, FRAME_SIZE should be set to 4090 in this case such that the 
//struct size is 4096 bytes. (^2 is necessary to use huge pointers to access arrays greater than 64K)
//
};


class __far Chscx
{
protected:
//protected variables

unsigned port_list[MAX_PORTS];						//base address list
unsigned port_open_list[MAX_PORTS];                 	//port has been inited list
unsigned interrupt_list[MAX_PORTS];					//ports associated interrupt level (hardware)
unsigned port_dmat_list[MAX_PORTS];
unsigned port_dmar_list[MAX_PORTS];
unsigned hooked_irqs[16];						//list of irq vectors that are hooked (by number)
void (interrupt far *old_service_routines[16])();//hooked interrupt service vectors previous routines
unsigned next_port;								//holds the next port to be used (added)
unsigned next_irq;								//holds the next irq to be added
unsigned upper_irq;								//flag for ISR to send upper EOI if irq >8 is being used
struct buf huge *rxbuffer[MAX_PORTS][MAX_RBUFS];							//array of pointers for receive buffering
struct buf huge *txbuffer[MAX_PORTS][MAX_TBUFS];							//array of pointers for transmitt buffering
unsigned current_rxbuf[MAX_PORTS];
unsigned current_txbuf[MAX_PORTS];
unsigned max_rxbuf[MAX_PORTS];
unsigned max_txbuf[MAX_PORTS];
unsigned timer_status[MAX_PORTS];
unsigned tx_type[MAX_PORTS];
unsigned istxing[MAX_PORTS];	//==1 if a frame is being sent ,==0 if no txing is going on
unsigned port_status[MAX_PORTS];
//stuff for use with extended transparent mode 1
char huge *tempbuf[MAX_PORTS];
unsigned extx1flag[MAX_PORTS];
unsigned inptr[MAX_PORTS];
unsigned oldinptr[MAX_PORTS];
unsigned startpt[MAX_PORTS];
unsigned endpt[MAX_PORTS];
unsigned shiftcnt[MAX_PORTS];
char lastbyte[MAX_PORTS];
char start_byte[MAX_PORTS];
char end_byte[MAX_PORTS];
unsigned startflag[MAX_PORTS];

public:
//public varaiables
Chscx(); 
~Chscx();

// Operations
public:
//user callable functions
unsigned add_port(unsigned base, unsigned irq, unsigned dmar, unsigned dmat);//return port# (index into port..arrays)
unsigned kill_port(unsigned port);//true ==success
unsigned init_port(	unsigned port,
					unsigned mask,
					unsigned cmdr,
					unsigned mode,
					unsigned timr,
					unsigned xad1,
					unsigned xad2,
					unsigned rah1,
					unsigned rah2,
					unsigned ral1,
					unsigned ral2,
					unsigned xbcl,
					unsigned bgr,
					unsigned ccr2,
					unsigned xbch,
					unsigned rlcr,
					unsigned ccr1,
					unsigned tsax,
					unsigned tsar,
					unsigned xccr,
					unsigned rccr,
					unsigned rbufs,
					unsigned tbufs);//true ==success
unsigned rx_port(unsigned port,char far *buf, unsigned num_bytes);//returns # bytes transfered, 0 if fails
unsigned tx_port(unsigned port, char far *buf, unsigned num_bytes);//returns # bytes transfered,0 if fails
unsigned set_control_lines(unsigned port,unsigned dtr, unsigned rts);//sets or clears dtr/rts 1 = set 0 = clear
unsigned get_control_lines(unsigned port);//return  = bit flags  X X X DSR DTR DCD CTS RTS 
unsigned get_port_status(unsigned port);//returns status flag consisting of one or more of the following
													//XMR_INTERRUPT   0x0001
													//EXE_INTERRUPT   0x0002
													//PCE_INTERRUPT   0x0004
													//RFO_INTERRUPT   0x0008
													//CTS_INTERRUPT   0x0010
													//RFS_INTERRUPT   0x0020
													//RX_BUFFER_OVERFLOW 0x0040
													//RSC_INTERRUPT   0x0080
													//TIMER_INTERRUPT 0x0100
													//RX_READY        0x0200

unsigned clear_rx_buffer(unsigned port); //False if port not open
unsigned clear_tx_buffer(unsigned port); //False if port not open
unsigned set_tx_type(unsigned port,unsigned type); //False if port not open
unsigned set_tx_address(unsigned port , unsigned address); //False if port not open
unsigned set_rx_address1(unsigned port,unsigned address); //False if port not open
unsigned set_rx_address2(unsigned port,unsigned address); //False if port not open
unsigned start_timer(unsigned port); //False if port not open
unsigned is_timer_expired(unsigned port); //False if port not open
unsigned wait_for_timer_expired(unsigned port); //False if port not open
unsigned stop_timer(unsigned port);//Fals if port not open
unsigned setupextended(unsigned port,char startbt, char stopbt); //set start  and end char's for extended transparent mode
void set_clock_generator(unsigned port, unsigned long hval,unsigned nmbits);
// Implementation
protected:
//class defined functions	
void cdecl interrupt far hscx_isr(void);
void setupdmar(unsigned port,void far *address);
void setupdmat(unsigned port,void far *address,unsigned numbytes);

};

