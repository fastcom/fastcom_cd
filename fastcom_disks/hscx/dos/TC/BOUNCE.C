//      BOUNCE.C  1-20-95 based on:
//      
//      HSCX.C
//      Written by: Carl George
//      Compiled with Turbo C++ ver 1.0
//      Date 2-3-95        
//      Copyright(c) 1994,1995 COMMTECH,INC Wichita, KS

//BOUNCE.C      an example of using the FASTCOM:HSCX in TRANSPARENT MODE
//              using interrupt driven transmit and receive
//              this program will take as an input any keystroke
//              fill a frame with that keystroke and send it out the hscx
//              in addition it will receive any frames that come in, and
//              send the same frame back out.
//              ideally if two hscx's are connected together you would
//              hit a key on one and a frame of data will continually 
//              bounce back and forth between the two
//              the cable between the two must be at least:
//              rx+     ->      tx+
//              rx-     ->      tx-
//              tx+     ->      rx+
//              tx-     ->      rx+
//              gnd     ->      gnd

//
//
//      This compilation of functions will work with the FASTCOM:HSCX rev 3.1
//      the functions included are:
//      init_hscx( hscx_setup) -- will initialize the HSCX with the hscx_setup 
//                             -- parameters.
//      hscx_isr_t()           -- an interrupt service routine that will 
//                             -- service sending and receiving TRANSPARENT frames
//                             -- of type struct buf
//      hscx_isr_a()           -- an interrupt service routine that will 
//                             -- service sending and receiving I (automode) frames
//                             -- of type struct buf
//      hscx_isr_e()           -- an interrupt service routine that will 
//                             -- service sending and receiving (extended trasparent) frames
//                             -- of type struct buf
//      hscx_isr_dma()         -- will service sending and receiving frames
//                             -- when the DMA services are used
//      setup_dmar()           -- sets up the DMA channels in hscx_setup for receive
//      setup_dmat()           -- sets up the DMA channel for transmit
//      stop_hscx()            -- cleans up the IRQ and DMA channels for exit
//      send_frame()           -- preps and sends a frame if buffer is available
//      rec_frame()            -- checks if frame is ready and returns
//      all of these functions require that you have the hscx_setup and buf structures
//      built prior to calling.
//
#include "bios.h"
#include "dos.h"
#include "stdio.h"
#include "conio.h" 
#include "stdlib.h"
#include "string.h"
#include "time.h"
#include "hscx.h"

//structure for buffered frames
struct buf{
unsigned valid;         //indicator 1 = frame[] has data, 0 = frame[] has ???
unsigned no_bytes;      //number of bytes in frame[]
unsigned max;           //maximum number of bytes to send/receive
unsigned frame[FRAME_SIZE+32];  //data array for received/transmitted data
				//size is +32 for DMA functions that
				//will do at most 16 extra cycles for 
				//odd sized frames
};

//structure for initialization parameters
typedef struct hscx_set{
unsigned mode;          // options: AUTO,TRANSPARENT,EXT_TRANSPARENT
unsigned address_recog; // options:     AUTO:0 = 8bit recog
			//              AUTO:1 = 16bit recog
			//              TRANSPARENT:0 = no recog
			//              TRANSPARENT:1 = 8bit recog in RAH1/2
			//              EXT_TRANSPARENT:0 = bytes in RAL1
			//              EXT_TRANSPARENT:1 = bytes in FIFO
unsigned address_t;     // options:     address_t -> XAD1:XAD2
unsigned address_r;     // options:     address_r -> RAH1:RAL1
unsigned clock_mode;    // options:     0 -> 7 = mode 0 -> 7
unsigned dma_channelr;  // options:     1,3,5,7,0 ; 0 = no dma
unsigned dma_channelt;  // options:     1,3,5,7,0 ; 0 = no dma
unsigned irq;           // options:     3,4,5,6,7,9,10,11,12,15
unsigned long baud_rate;// options:     1 -> 1000000 (only used in clock modes 2,3,6,7)
unsigned idle;          // options:     NO_FLGS,FLAGS (timefill = none, or flags)
unsigned timer;         // options:     INTERNAL_T, EXTERNAL_T ; use internal with AUTO
unsigned frame_size;    // options:     0->4096 (not used, update FRAME_SIZE in hscx.h)
}HSCX_SET;
HSCX_SET hscx_setup;

void interrupt (*oldvect)();    //the old interrupt vector temp storage
void interrupt hscx_isr_t();      //ISR for handling interrupt driven xmit/receive
void interrupt hscx_isr_a();      //ISR for handling interrupt driven xmit/receive
void interrupt hscx_isr_e();      //ISR for handling interrupt driven xmit/receive
void interrupt hscx_isr_dma();  //ISR for handling DMA driven xmit/receive
int init_hscx(HSCX_SET hscx_setup);//initialize routine for hscx
void setup_dmar(unsigned *receive);//sets up receive DMA channel to deposit date in receive[]
void setup_dmat(unsigned *trans,unsigned size);  //sets up transmit DMA channel to take data from trans[]
void stop_hscx();               //cleans up IRQ's and DMA for exit
int rec_frame();                //tests for a received frame of data
int send_frame();                //sends a frame of data
void bounce();

struct buf rbuf[MAX_RBUFS];   //receive buffer structure must be global for ISR's to work
struct buf tbuf;      //transmit buffer structure must be global for ISR's to work  
unsigned current_buf; //current buffer index must be global for ISR's to work

void main(){
unsigned i,j,k;
char *b;
int v;

current_buf=0;
//setup the hscx_setup structure to initialize the HSCX

hscx_setup.idle = NO_FLGS;      //idle = high
hscx_setup.mode = TRANSPARENT;  //mode = transparent
hscx_setup.address_recog = 0;   //no address recognition
hscx_setup.address_t = 0xffff;  //transmit and receive addresses = 0xffff
hscx_setup.address_r = 0xffff;
hscx_setup.clock_mode = 2;      //clock mode 2 (baud rate generation)
hscx_setup.dma_channelr = 0;    //no DMA 
hscx_setup.dma_channelt = 0;    //no DMA 
hscx_setup.irq = 5;             //use IRQ 5 for interrupts
hscx_setup.baud_rate = 1000000;  //use baud rate of 1Mbps
hscx_setup.timer = EXTERNAL_T;  //use the timer externally

//try to initialize the HSCX if it returns -1 then it failed, 0 and it's ready to go
if(init_hscx(hscx_setup)!=0)
	{
	printf("can't init hscx\n\r");
	exit(1);
	}

//main menu:
start:
i = 0;
clrscr();
printf("BOUNCE\n\r");
printf("\n\r\n\r");
printf("Press any key to start the bounce\n\r");
printf("ESC to exit\n\r");
while(!kbhit())
	{
	//check for incomming frame
	v = -1;
	v = rec_frame();
	if(v != -1)
		{ 
		bounce();                
		goto start;
		}
	}
i = getch();
if(i==27) goto fini;
for(j=0;j<FRAME_SIZE;j++) tbuf.frame[j] = (i&0xff);     //fill the frame with data
while(send_frame()==(-1));      //send the frame
goto start;
fini:
stop_hscx();
}

void bounce()
{
unsigned i,j;
int v;
printf("\n\r");
v = rec_frame();        //get receive buffer
for(j=0;j<rbuf[v].no_bytes-1;j++) 
	{
	tbuf.frame[j] = rbuf[v].frame[j];//copy to xmit buffer
	printf("%c",rbuf[v].frame[j]);//display received data
	}
printf("\n\r");
rbuf[v].valid = 0;      //free up receive buffer

while(send_frame()==(-1));//send the frame out
}


int rec_frame()
{
int i;
i = current_buf;
do{
i++;
if(i>=MAX_RBUFS) i = 0;
if(rbuf[i].valid==1) return i;  //searches rbuf array for valid frames returns the index into rbuf[] for a valid frame
}while(i!=current_buf);
return (-1);
}

int send_frame()
{
int i;
//this function assumes that tbuf.frame has been filled
//and that hscx_setup exists
if(tbuf.valid==1) return (-1);  //allready sending a frame so return -1
//not sending a frame so prep a new one and send it
if(hscx_setup.dma_channelr==0)
	{
	if(FRAME_SIZE>32){
	for(i=0;i<32;i++) outportb(FIFO,tbuf.frame[i]);
	tbuf.no_bytes=i;  //filled fifo with 32 bytes
	tbuf.valid = 1;   //frame is valid until sent
	tbuf.max = FRAME_SIZE;  //total size of frame
	if(hscx_setup.mode == AUTO) outportb(CMDR,XIF);
	if(hscx_setup.mode != AUTO) outportb(CMDR,XTF);     //transmit transparent frame from HSCX
	}
	else{
	for(i=0;i<FRAME_SIZE;i++) outportb(FIFO,tbuf.frame[i]);
	tbuf.no_bytes=i;  //filled fifo with 32 bytes
	tbuf.valid = 1;   //frame is valid until sent
	tbuf.max = FRAME_SIZE;  //total size of frame
	if(hscx_setup.mode == AUTO) outportb(CMDR,XIF|XME);
	if(hscx_setup.mode != AUTO) outportb(CMDR,XTF|XME);     //transmit transparent frame from HSCX
	}
	return 1;
	}
if(hscx_setup.dma_channelr!=0)
	{
	tbuf.no_bytes = 0;    //dma is used so just need to validate the frame
	tbuf.max = FRAME_SIZE;//and send a XTF command the rest is to be 
	tbuf.valid = 1;       //consistant, but not needed since the 
	if(hscx_setup.mode == AUTO) outportb(CMDR,XIF);
	if(hscx_setup.mode != AUTO) outportb(CMDR,XTF);   //dma xfr is set up in registers (# of bytes etc)
	return 1;
	}
return (-1);
}



void stop_hscx()
{
unsigned i;
if(hscx_setup.irq<8)
	{
	setvect(0x08+hscx_setup.irq,oldvect);
	//disable irq on 8259 interrupt mask register
	i = inportb(0x21);
	if(hscx_setup.irq ==3) i = i|0x08;
	if(hscx_setup.irq ==4) i = i|0x10;
	if(hscx_setup.irq ==5) i = i|0x20;
	if(hscx_setup.irq ==6) i = i|0x40;
	if(hscx_setup.irq ==7) i = i|0x80;
	outportb(0x21,i);
	}
if(hscx_setup.irq>=8)
	{
	setvect(0x70+(hscx_setup.irq-8),oldvect);
	//disable irq on 8259 interrupt mask register
	i = inportb(0xa1);
	if(hscx_setup.irq ==9) i = i|0x02;
	if(hscx_setup.irq ==10) i = i|0x04;
	if(hscx_setup.irq ==11) i = i|0x08;
	if(hscx_setup.irq ==12) i = i|0x10;
	if(hscx_setup.irq ==15) i = i|0x80;
	outportb(0xa1,i);
	}
//disable DMA by masking it off
if(hscx_setup.dma_channelt==1) outportb(DMA_MASK_LO,5);
if(hscx_setup.dma_channelt==3) outportb(DMA_MASK_LO,7);
if(hscx_setup.dma_channelt==5) outportb(DMA_MASK_HI,5);
if(hscx_setup.dma_channelt==7) outportb(DMA_MASK_HI,7);
if(hscx_setup.dma_channelr==1) outportb(DMA_MASK_LO,5);
if(hscx_setup.dma_channelr==3) outportb(DMA_MASK_LO,7);
if(hscx_setup.dma_channelr==5) outportb(DMA_MASK_HI,5);
if(hscx_setup.dma_channelr==7) outportb(DMA_MASK_HI,7);
}


void interrupt hscx_isr_t(){
unsigned i,j,k;
do{
i = inportb(ISTAT);             //get interrupt status
if((i&XPR)==XPR) {      //transmit pool ready irq
		if(tbuf.valid==1){//if tbuf is valid then we have data to send
				//there are two conditions here either we have
				//more than 32 bytes to send or 32 or less to send
				//if more than 32 then fill the fifo and XTF it
				//if less then fill the fifo with the remaining
				//and XTF|XME it to close the frame
			   if((tbuf.max-tbuf.no_bytes)>32){
				for(j=0;j<32;j++)outportb(FIFO,tbuf.frame[j+tbuf.no_bytes]);
				outportb(CMDR,XTF);
				tbuf.no_bytes = tbuf.no_bytes + 32;
				}
			   else{
				for(j=tbuf.no_bytes;j<tbuf.max;j++) outportb(FIFO,tbuf.frame[j]);
				outportb(CMDR,XTF|XME);
				tbuf.no_bytes = tbuf.max;  //show how many bytes sent
				tbuf.valid=0;   //invalidate tbuf (we are done sending)
				}
			}
		}
if((i&EXB)==EXB) {
		
		j = inportb(EXIR); //get EXIR
		if(j&EXE==EXE) {        //if we get here then the computer
					//sending the data cannot keep up 
					//with the transmission rate
					//(ie cant get 32 bytes into the fifo
					//before the last 32 are sent out)
				tbuf.valid=0;   //invalidate tbuf
				tbuf.no_bytes=0;//no bytes sent (aborted)
				}
		 if(j&RFO==RFO){ 
			      //receive overflow, received more data
			      //than we have room for on the HSCX chip
			      //(more than 64 bytes came in before an irq 
			      //was serviced)
				outportb(CMDR,RHR);     //reset receiver
				rbuf[current_buf].no_bytes = 0;
				}
		}
if((i&RME)==RME){
		//last part of a frame is received
		//get the number of bytes total received
		//then fill the rbuf frame with the rest of the bytes
		k = ((inportb(RBCH)&0x0f)<<8)+inportb(RBCL);
		for(j=0;j<(k-(rbuf[current_buf].no_bytes));j++) rbuf[current_buf].frame[j+rbuf[current_buf].no_bytes]=inportb(FIFO);
		outportb(CMDR,RMC);     //release fifo
		rbuf[current_buf].valid = 1;         //validate rbuf
		rbuf[current_buf].no_bytes = k;      //number of bytes received in rbuf
		current_buf++;  
		if(current_buf==MAX_RBUFS) current_buf=0;                
		//if(rbuf[current_buf].valid==1); an error occured and the 
					       //   main program isn't done with 
					       //   the frame that is about
					       //   to be used for received data
		rbuf[current_buf].no_bytes = 0;  //prep buf for use
		rbuf[current_buf].valid = 0;    //invalidate it for use
		}
if((i&RPF)==RPF){
		//receive pool full, 32 bytes are available in the fifo
		//so go get them and put them in rbuf.frame
		for(j=0;j<32;j++) rbuf[current_buf].frame[j+rbuf[current_buf].no_bytes] = inportb(FIFO);
		rbuf[current_buf].no_bytes = rbuf[current_buf].no_bytes + 32;
		outportb(CMDR,RMC);     //release fifo
		}


}while(i!=0);

outportb(0x20,0x20);    //NONSPECFIC EOI
if(hscx_setup.irq>8) outportb(0xa0,0x20); //upper irq nonspecfic EOI
}

void interrupt hscx_isr_a(){
unsigned i,j,k;
do{
i = inportb(ISTAT);             //get interrupt status
if((i&XPR)==XPR) {      //transmit pool ready irq
		if(tbuf.valid==1){//if tbuf is valid then we have data to send
				//there are two conditions here either we have
				//more than 32 bytes to send or 32 or less to send
				//if more than 32 then fill the fifo and XTF it
				//if less then fill the fifo with the remaining
				//and XTF|XME it to close the frame
			   if((tbuf.max-tbuf.no_bytes)>32){
				for(j=0;j<32;j++)outportb(FIFO,tbuf.frame[j+tbuf.no_bytes]);
				outportb(CMDR,XIF);
				tbuf.no_bytes = tbuf.no_bytes + 32;
				}
			   else{
				for(j=tbuf.no_bytes;j<tbuf.max;j++) outportb(FIFO,tbuf.frame[j]);
				outportb(CMDR,XIF|XME);
				tbuf.no_bytes = tbuf.max;  //show how many bytes sent
				tbuf.valid=0;   //invalidate tbuf (we are done sending)
				}
			}
		}
if((i&EXB)==EXB) {
		
		j = inportb(EXIR); //get EXIR
		if(j&EXE==EXE) {        //if we get here then the computer
					//sending the data cannot keep up 
					//with the transmission rate
					//(ie cant get 32 bytes into the fifo
					//before the last 32 are sent out)
				tbuf.valid=0;   //invalidate tbuf
				tbuf.no_bytes=0;//no bytes sent (aborted)
				}
		 if(j&RFO==RFO){ 
			      //receive overflow, received more data
			      //than we have room for on the HSCX chip
			      //(more than 64 bytes came in before an irq 
			      //was serviced)
				outportb(CMDR,RHR);     //reset receiver
				rbuf[current_buf].no_bytes = 0;
				}
		}
if((i&RME)==RME){
		//last part of a frame is received
		//get the number of bytes total received
		//then fill the rbuf frame with the rest of the bytes
		k = ((inportb(RBCH)&0x0f)<<8)+inportb(RBCL);
		for(j=0;j<(k-(rbuf[current_buf].no_bytes));j++) rbuf[current_buf].frame[j+rbuf[current_buf].no_bytes]=inportb(FIFO);
		outportb(CMDR,RMC);     //release fifo
		rbuf[current_buf].valid = 1;         //validate rbuf
		rbuf[current_buf].no_bytes = k;      //number of bytes received in rbuf
		current_buf++;  
		if(current_buf==MAX_RBUFS) current_buf=0;                
		//if(rbuf[current_buf].valid==1); an error occured and the 
					       //   main program isn't done with 
					       //   the frame that is about
					       //   to be used for received data
		rbuf[current_buf].no_bytes = 0;  //prep buf for use
		rbuf[current_buf].valid = 0;    //invalidate it for use
		}
if((i&RPF)==RPF){
		//receive pool full, 32 bytes are available in the fifo
		//so go get them and put them in rbuf.frame
		for(j=0;j<32;j++) rbuf[current_buf].frame[j+rbuf[current_buf].no_bytes] = inportb(FIFO);
		rbuf[current_buf].no_bytes = rbuf[current_buf].no_bytes + 32;
		outportb(CMDR,RMC);     //release fifo
		}


}while(i!=0);

outportb(0x20,0x20);    //NONSPECFIC EOI
if(hscx_setup.irq>8) outportb(0xa0,0x20); //upper irq nonspecfic EOI
}

void interrupt hscx_isr_e(){
unsigned i,j,k;
do{
i = inportb(ISTAT);             //get interrupt status
if((i&XPR)==XPR) {      //transmit pool ready irq
		if(tbuf.valid==1){//if tbuf is valid then we have data to send
				//there are two conditions here either we have
				//more than 32 bytes to send or 32 or less to send
				//if more than 32 then fill the fifo and XTF it
				//if less then fill the fifo with the remaining
				//and XTF|XME it to close the frame
			   if((tbuf.max-tbuf.no_bytes)>32){
				for(j=0;j<32;j++)outportb(FIFO,tbuf.frame[j+tbuf.no_bytes]);
				outportb(CMDR,XTF);
				tbuf.no_bytes = tbuf.no_bytes + 32;
				}
			   else{
				for(j=tbuf.no_bytes;j<tbuf.max;j++) outportb(FIFO,tbuf.frame[j]);
				outportb(CMDR,XTF|XME);
				tbuf.no_bytes = tbuf.max;  //show how many bytes sent
				tbuf.valid=0;   //invalidate tbuf (we are done sending)
				}
			}
		}
if((i&EXB)==EXB) {
		
		j = inportb(EXIR); //get EXIR
		if(j&EXE==EXE) {        //if we get here then the computer
					//sending the data cannot keep up 
					//with the transmission rate
					//(ie cant get 32 bytes into the fifo
					//before the last 32 are sent out)
				tbuf.valid=0;   //invalidate tbuf
				tbuf.no_bytes=0;//no bytes sent (aborted)
				}
		 if(j&RFO==RFO){ 
			      //receive overflow, received more data
			      //than we have room for on the HSCX chip
			      //(more than 64 bytes came in before an irq 
			      //was serviced)
				outportb(CMDR,RHR);     //reset receiver
				rbuf[current_buf].no_bytes = 0;
				}
		}
if((i&RME)==RME){
		//last part of a frame is received
		//get the number of bytes total received
		//then fill the rbuf frame with the rest of the bytes
		k = ((inportb(RBCH)&0x0f)<<8)+inportb(RBCL);
		for(j=0;j<(k-(rbuf[current_buf].no_bytes));j++) rbuf[current_buf].frame[j+rbuf[current_buf].no_bytes]=inportb(FIFO);
		outportb(CMDR,RMC);     //release fifo
		rbuf[current_buf].valid = 1;         //validate rbuf
		rbuf[current_buf].no_bytes = k;      //number of bytes received in rbuf
		current_buf++;  
		if(current_buf==MAX_RBUFS) current_buf=0;                
		//if(rbuf[current_buf].valid==1); an error occured and the 
					       //   main program isn't done with 
					       //   the frame that is about
					       //   to be used for received data
		rbuf[current_buf].no_bytes = 0;  //prep buf for use
		rbuf[current_buf].valid = 0;    //invalidate it for use
		}
if((i&RPF)==RPF){
		//receive pool full, 32 bytes are available in the fifo
		//so go get them and put them in rbuf.frame
		for(j=0;j<32;j++) rbuf[current_buf].frame[j+rbuf[current_buf].no_bytes] = inportb(FIFO);
		rbuf[current_buf].no_bytes = rbuf[current_buf].no_bytes + 32;
		outportb(CMDR,RMC);     //release fifo
		if(rbuf[current_buf].no_bytes>=FRAME_SIZE) //this will be the normal end condition for EXTENDED TRANSPARENT MODE
			{
			rbuf[current_buf].valid = 1;         //validate rbuf
			current_buf++;  
			if(current_buf==MAX_RBUFS) current_buf=0;                
			//if(rbuf[current_buf].valid==1); an error occured and the 
					       //   main program isn't done with 
					       //   the frame that is about
					       //   to be used for received data
			rbuf[current_buf].no_bytes = 0;  //prep buf for use
			rbuf[current_buf].valid = 0;    //invalidate it for use
			}
		}


}while(i!=0);

outportb(0x20,0x20);    //NONSPECFIC EOI
if(hscx_setup.irq>8) outportb(0xa0,0x20); //upper irq nonspecfic EOI
}





int init_hscx(HSCX_SET hscx_setup){
unsigned i,j,k;

if((hscx_setup.mode!= AUTO)&&(hscx_setup.mode!= TRANSPARENT)&&(hscx_setup.mode!= EXT_TRANSPARENT) )
	{
	return (-1);    //invalid mode;
	}
if(hscx_setup.mode == AUTO)
	{
	i = inportb(MODE);
	i = i & 0x3f;
	outportb(MODE,i);
	}
if(hscx_setup.mode == TRANSPARENT)
	{
	i = inportb(MODE);
	i = i & 0x3f;
	i = i | 0x80;
	outportb(MODE,i);
	}
if(hscx_setup.mode == EXT_TRANSPARENT)
	{
	i = inportb(MODE);
	i = i & 0x3f;
	i = i | 0x0c0;
	outportb(MODE,i);
	}

if(hscx_setup.address_recog == 0)
	{
	i = inportb(MODE);
	i = i & 0x0df;
	outportb(MODE,i);
	}
if(hscx_setup.address_recog == 1)
	{
	i = inportb(MODE);
	i = i & 0x0df;
	i = i | 0x020;
	outportb(MODE,i);
	}


if(hscx_setup.timer == INTERNAL_T)
	{
	i = inportb(MODE);
	i = i & 0x0ef;
	i = i | 0x10;
	outportb(MODE,i);
	}
if(hscx_setup.timer == EXTERNAL_T)
	{
	i = inportb(MODE);
	i = i & 0x0ef;
	outportb(MODE,i);
	}

//setup timer res to 512 or 32,768
i = inportb(MODE);
i = i | 0x02;    //for 512
//i = i &0x0fd;  //for 32,768

i = i |0x08;     //receiver active (comment this out for extended transparent 0)
i = i & 0x0fe;   //testloop inactive
outportb(MODE,i);

//setup address stuff, if using address recog (AUTO Mode must have)
//otherwise set both addresses to 0xFFFF and forget about it
outportb(XAD1, (hscx_setup.address_t)>>8);
outportb(XAD2, (hscx_setup.address_t)&0xff);
outportb(RAH1, (hscx_setup.address_r)>>8);
outportb(RAL1, (hscx_setup.address_r)&0xff);
outportb(RAH2,0xff);    // alternate receive recog address
outportb(RAL2,0xff);

outportb(MASK,0x6);       //turn on all interrupts RME,RPF,RSC,XPR,TIN,EXB

//clock mode stuff
if((hscx_setup.clock_mode<0)||(hscx_setup.clock_mode>7))
	{
	return (-1);    //invalid clock mode
	}
i = inportb(CCR1);
i = i|0x80;     //powerup
i = i|0x10;     //output drivers must be push pull
i = i|( (hscx_setup.clock_mode)&0x07); //set clock mode;
if(hscx_setup.idle == FLAGS) i = i | 0x08;
if(hscx_setup.idle == NO_FLGS)  i = i & 0x0f7;

i = i & 0x9f;   //NRZ encoding
//i = i | 0x40; //NRZI encoding
//i = i | 0x20; //BUS1
//i = i | 0x60; //BUS2
outportb(CCR1,i);

//baud rate mode setup stuff
if( (hscx_setup.clock_mode==2)|| (hscx_setup.clock_mode==3)
||  (hscx_setup.clock_mode==6)|| (hscx_setup.clock_mode==7) )
	{
	//this baud rate stuff assumes that the 16Mhz signal is 
	//the clock input the the HSCX
	//if it is the max baud rate generated can be 1Mbps
	if(hscx_setup.baud_rate >=1000000)
		//make max baud rate by setting CCR2 bit 5 = 0
		//(divide by 1) no BGR
		{
		i = inportb(CCR2);
		i = i &0x0df;
		i = i|0x18;     //set txclk to output and make it BGR/16;
		i = i&0x0f8;     //disable CIE,RIE,DIV
		outportb(CCR2,i);
		}
	if(hscx_setup.baud_rate <1000000)
		{
		//again this assumes a 16Mhz input clock if so it will
		//generate the closest integer baud rate to what is 
		//specified (the smaller the bps the closer it will be)
		// max = 1Mbps
		// 0   = 500kbps
		// 1   = 250kbps
		// 2   = 125kbps
		// 3   = 62.5kbps, etc
		//if you want a baud rate between the values that it can 
		//generate use clock mode 1 with an external clock
		//and have the device that you are talking to generate
		//the clock, (ie 376kbps can not be generated without a
		//crystal change, but if you input 376khz into RXCLK
		//and use clock mode 1 it will transmit and receive at 376kbps)
		
		i =    (1000000/((hscx_setup.baud_rate)*2))-1;

		outportb(BGR,i&0x0ff);
		j = (i&0x0300)>>2;
		i = inportb(CCR2);
		i = i|j;
		i = i|0x020;
		i = i|0x18;     //set txclk to output and make it BGR/16;
		i = i&0x0f8;     //disable CIE,RIE,DIV
		outportb(CCR2,i);
		}
	}

outportb(XBCH,0);       //start with dma off
if(hscx_setup.dma_channelr!=0)
	{
	if(hscx_setup.irq<8)
		{
		//get old interupt 5 vector
		oldvect = getvect(0x08+hscx_setup.irq);

		//set to our routine
		setvect(0x08+hscx_setup.irq,hscx_isr_dma);
	i = inportb(0x21);
	if(hscx_setup.irq ==3) i = i&0xf7;
	if(hscx_setup.irq ==4) i = i&0xef;
	if(hscx_setup.irq ==5) i = i&0xdf;
	if(hscx_setup.irq ==6) i = i&0xbf;
	if(hscx_setup.irq ==7) i = i&0x7f;
	outportb(0x21,i);

		}
	if(hscx_setup.irq>=8)
		{
		oldvect = getvect(0x70+(hscx_setup.irq-8));
		//set to our routine
		setvect(0x70+(hscx_setup.irq-8),hscx_isr_dma);
	i = inportb(0xa1);
	if(hscx_setup.irq ==9)         i = i&0xfd;
	if(hscx_setup.irq ==10)        i = i&0xfb;
	if(hscx_setup.irq ==11)        i = i&0xf7;
	if(hscx_setup.irq ==12)        i = i&0xef;
	if(hscx_setup.irq ==15)        i = i&0x7f;
	outportb(0xa1,i);

		}
	if(hscx_setup.dma_channelr==hscx_setup.dma_channelt)
		{
		return (-1);    //cannot use same dma channel for xmit and receive
		}
	setup_dmar(rbuf[current_buf].frame); //sets address of rbuf.frame[] to dma channel and enables it
	setup_dmat(tbuf.frame,0); //sets address of tbuf.frame[] to dma channel and enables it


	}



if((hscx_setup.irq!=0)&&(hscx_setup.dma_channelr==0))
	{

	if(hscx_setup.irq<8)
		{

		//get old interupt vector
		oldvect = getvect(0x08+hscx_setup.irq);
		//set to our routine
		if(hscx_setup.mode ==AUTO) setvect(0x08+hscx_setup.irq,hscx_isr_a);
		if(hscx_setup.mode ==TRANSPARENT) setvect(0x08+hscx_setup.irq,hscx_isr_t);
		if(hscx_setup.mode ==EXT_TRANSPARENT) setvect(0x08+hscx_setup.irq,hscx_isr_e);
	//enable irq on 8259 interrupt mask register
	i = inportb(0x21);
	if(hscx_setup.irq ==3) i = i&0xf7;
	if(hscx_setup.irq ==4) i = i&0xef;
	if(hscx_setup.irq ==5) i = i&0xdf;
	if(hscx_setup.irq ==6) i = i&0xbf;
	if(hscx_setup.irq ==7) i = i&0x7f;
	outportb(0x21,i);

		}
	if(hscx_setup.irq>=8)
		{
		//get old interupt vector
		oldvect = getvect(0x70+(hscx_setup.irq-8));
		//set to our routine
		if(hscx_setup.mode ==AUTO) setvect(0x70+(hscx_setup.irq-8),hscx_isr_a);
		if(hscx_setup.mode ==TRANSPARENT) setvect(0x70+(hscx_setup.irq-8),hscx_isr_t);
		if(hscx_setup.mode ==EXT_TRANSPARENT) setvect(0x70+(hscx_setup.irq-8),hscx_isr_e);
	i = inportb(0xa1);
	if(hscx_setup.irq ==9)         i = i&0xfd;
	if(hscx_setup.irq ==10)        i = i&0xfb;
	if(hscx_setup.irq ==11)        i = i&0xf7;
	if(hscx_setup.irq ==12)        i = i&0xef;
	if(hscx_setup.irq ==15)        i = i&0x7f;
	outportb(0xa1,i);

		}


	}
i = 0;
j = 0;
outportb(CMDR,XRES);       //reset transmit
while((inportb(STAR)&0x04)==0x04) //wait for CEC = 0 (no command executing)
	{
	i++;
	if(i==0)
		{
		j++;
		if(j==1000) return (-1); //timed out if gets here(probably no clock)
		}
	}
i = 0;
j = 0;
outportb(CMDR,RHR);    //reset receive
while((inportb(STAR)&0x04)==0x04) //wait for CEC = 0
	{
	i++;
	if(i==0)
		{
		j++;
		if(j==1000) return (-1);  //timed out if gets here (probably no clock)
		}
	}
while(inportb(EXIR));//make sure no extended irq's are active
		     //(ie clear the board of interrupts
while(inportb(ISTAT));//make sure no irq's active

return 0;       //if here then the HSCX is setup and ready to go
}


//a note about the DMA channels
//the FASTCOM:HSCX is a 8 bit card, and the upper DMA channels are 16 bit 
// channels, so, if you select an upper DMA channel (5 or 7) the lower
// 8 bits of each transfer will go to the HSCX, the upper 8 bits will
// either be lost in vapor space if you are transmitting, or will be 
// received from vapor space if receiving (ie upper 8 bits = garbage).
// this leads to one simple rule when using 16 bit DMA channels,
// USE UNSIGNED ARRAYS WHEN USING DMA FROM THE HSCX ON A UPPER CHANNEL
// and more importantly only put the data that you want in the lower 8 bits
// of the unsigned array elements. and make sure the array is alligned on a
// word boundary.
// this leads to another good rule, if you are using 1 8 bit channel and 1 16bit
// channel use the 8 bit channel for receive, and the 16 bit channel for transmit
// the 8 bit channels will transfer the data as if from a character array
//
// so if you have the array: unsigned buffer[5] and it contains
// unsigned buffer: FF00, FF00, FF00, FF00, FF00
// a 16 bit dma transmit will send 00,00,00,00,00
// a 8 bit dma receive (of 5 bytes (11,11,11,11,11)) will produce:
// unsigned buffer: 1111, 1111, 1100, FF00, FF00
// so when using dma cast your array to char* for 8bit dma's 
// and cast your array to unsigned* for 16bit dma's
//


void setup_dmat(unsigned *trans,unsigned size)
{
unsigned i,j,k;
unsigned long address;
if (size ==0) size =FRAME_SIZE;

if(hscx_setup.dma_channelt==1)
	{
	outportb(DMA_COMMAND_LO,4);             //disables 8 bit dma
	outportb(DMA_MODE_LO,0x19);             //sets demand xfer on ch1
	outportb(DMA_CLEAR_FFLO,0);             //reset count hi/lo
	outportb(DMA_COUNT_CH1,(size-1)&0xff);    //program byte count 
	outportb(DMA_COUNT_CH1,(size-1)>>8);      //controller will do 
	address = _DS;                                  //one more cycle than programmed
	address = address<<4;
	address = address+ ((unsigned long)trans&0xffff);
	outportb(DMA_PAGE_CH1,address>>16);             //calculate linear address of buffer
	outportb(DMA_CLEAR_FFLO,0);                     //and store in DMA controller
	outportb(DMA_ADD_CH1,address&0xff);
	outportb(DMA_ADD_CH1,address>>8);
	outportb(DMA_MASK_LO,1);                        //enable ch1 mask
	outportb(DMA_COMMAND_LO,0);                     //enable 8bit dma
	
	}
if(hscx_setup.dma_channelt==3)
	{
	outportb(DMA_COMMAND_LO,4);
	outportb(DMA_MODE_LO,0x1B);
	outportb(DMA_CLEAR_FFLO,0);
	outportb(DMA_COUNT_CH3,(size-1)&0xff);
	outportb(DMA_COUNT_CH3,(size-1)>>8);
	address = _DS;
	address = address<<4;
	address = address+ ((unsigned long)trans&0xffff);
	outportb(DMA_PAGE_CH3,address>>16);
	outportb(DMA_CLEAR_FFLO,0);
	outportb(DMA_ADD_CH3,address&0xff);
	outportb(DMA_ADD_CH3,address>>8);
	outportb(DMA_MASK_LO,3);
	outportb(DMA_COMMAND_LO,0);
	
	}
if(hscx_setup.dma_channelt==5)
	{
	outportb(DMA_COMMAND_HI,4);     //disable dma
	outportb(DMA_MODE_HI,0x19);     //demand xfr on ch5
	outportb(DMA_CLEAR_FFHI,0);     //reset flip-flop
	outportb(DMA_COUNT_CH5,(size-1)&0xff);
	outportb(DMA_COUNT_CH5,(size-1)>>8);
	address = _DS;
	address = address<<4;
	address = address+ ((unsigned long)trans&0xffff);
	outportb(DMA_PAGE_CH5,address>>16);
	address = address>>1; //adjust for 16bit dma
	outportb(DMA_CLEAR_FFHI,0);
	outportb(DMA_ADD_CH5,address&0xff);
	outportb(DMA_ADD_CH5,address>>8);
	outportb(DMA_MASK_HI,1);        //enable chan5
	outportb(DMA_COMMAND_HI,0);     //enable dma
	}
if(hscx_setup.dma_channelt==7)
	{
	outportb(DMA_COMMAND_HI,4);     //disable dma
	outportb(DMA_MODE_HI,0x1b);     //demand xfr on ch7
	outportb(DMA_CLEAR_FFHI,0);     //reset flip-flop
	outportb(DMA_COUNT_CH7,(size-1)&0xff);
	outportb(DMA_COUNT_CH7,(size-1)>>8);
	address = _DS;
	address = address<<4;
	address = address+ ((unsigned long)trans&0xffff);
	outportb(DMA_PAGE_CH7,address>>16);
	address = address>>1; //adjust for 16bit dma
	outportb(DMA_CLEAR_FFHI,0);
	outportb(DMA_ADD_CH7,address&0xff);
	outportb(DMA_ADD_CH7,(address>>8)&0xff);
	outportb(DMA_MASK_HI,3);        //enable chan7
	outportb(DMA_COMMAND_HI,0);     //enable dma
	}

i = size-1;
i = i&0x0fff;
i = i|0x8000;
outportb(XBCL,i&0xff);
outportb(XBCH,i>>8);    //set for dma mode and FRAME_SIZE bytes per frame
}

void setup_dmar(unsigned *receive)
{
unsigned i,j,k;
unsigned long address;
unsigned size;

i = (FRAME_SIZE+1)/32;              //calculate number of 32 byte transfers
j = (FRAME_SIZE+1)-(i*32);          //calculate number of bytes in last transfer
if((j>15)&&(j<32)) size = 32-j; //if 16-32 bytes then use 32-last to get number of extra dma transfers to be ready for
if((j>7)&&(j<16)) size = 16-j;  //if 8-15 bytes then will have 16 transfers
if((j>3)&&(j<8)) size = 8-j;    //if 4-7 will have 8 transfers
if((j>0)&&(j<4)) size = 4-j;    //if 1-3 will have 4 transfers
if(j==0) size = 0;              //if 0 then will be even transfers
//the extra transfers are added to the frame size so if FRAME_SIZE+1 
//( the +1 is for the status bytes that is pushed into the RXFIFO) is set to 
//a non multiple of 32 the receive[] array will be valid and not overwritten
//by the unexpected transfers at the end of a frame. and subsequent frames
//will be received correctly
//in other words if a frame size of say 40 is used:
// frame_size+1 = 41
// then there will be 1 32 byte DMA cycle and leaving 9 bytes to be transfered
// 9 bytes will force a 16 byte DMA cycle giving us 7 extra bytes
// so we add 7 to the frame size to get the total number of cycles in a
// received frame( the received length will be 48 bytes 41 of which are valid)
if(hscx_setup.dma_channelr==1)
	{
	outportb(DMA_COMMAND_LO,4);     //disable dma
	outportb(DMA_MODE_LO,0x15);     //demand xfer ch1
	outportb(DMA_CLEAR_FFLO,0);     //clear ctrlr flip flop
	outportb(DMA_COUNT_CH1,(FRAME_SIZE+size)&0xff);//write corrected word count
	outportb(DMA_COUNT_CH1,(FRAME_SIZE+size)>>8);
	address = _DS;                  //calculate linear address of buffer
	address = address<<4;
	address = address+ ((unsigned long)receive&0xffff);
	outportb(DMA_PAGE_CH1,address>>16);
	outportb(DMA_CLEAR_FFLO,0);
	outportb(DMA_ADD_CH1,address&0xff);     //and store it in the controller
	outportb(DMA_ADD_CH1,address>>8);
	outportb(DMA_MASK_LO,1);                //unmask ch1
	outportb(DMA_COMMAND_LO,0);             //enable DMA
	
	}
if(hscx_setup.dma_channelr==3)
	{
	outportb(DMA_COMMAND_LO,4);
	outportb(DMA_MODE_LO,0x17);
	outportb(DMA_CLEAR_FFLO,0);
	outportb(DMA_COUNT_CH3,(FRAME_SIZE+size)&0xff);
	outportb(DMA_COUNT_CH3,(FRAME_SIZE+size)>>8);
	address = _DS;
	address = address<<4;
	address = address+ ((unsigned long)receive&0xffff);
	outportb(DMA_PAGE_CH3,address>>16);
	outportb(DMA_CLEAR_FFLO,0);
	outportb(DMA_ADD_CH3,address&0xff);
	outportb(DMA_ADD_CH3,address>>8);
	outportb(DMA_MASK_LO,3);
	outportb(DMA_COMMAND_LO,0);
	
	}
if(hscx_setup.dma_channelr==5)
	{
	outportb(DMA_COMMAND_HI,4);     //disable dma
	outportb(DMA_MODE_HI,0x15);     //demand xfr on ch5
	outportb(DMA_CLEAR_FFHI,0);     //reset flip-flop
	outportb(DMA_COUNT_CH5,(FRAME_SIZE+size)&0xff);
	outportb(DMA_COUNT_CH5,(FRAME_SIZE+size)>>8);
	address = _DS;
	address = address<<4;
	address = address+ ((unsigned long)receive&0xffff);
	outportb(DMA_PAGE_CH5,address>>16);
	address = address>>1; //adjust for 16bit dma
	outportb(DMA_CLEAR_FFHI,0);
	outportb(DMA_ADD_CH5,address&0xff);
	outportb(DMA_ADD_CH5,address>>8);
	outportb(DMA_MASK_HI,1);        //enable chan5
	outportb(DMA_COMMAND_HI,0);     //enable dma
	
	}
if(hscx_setup.dma_channelr==7)
	{
	outportb(DMA_COMMAND_HI,4);     //disable dma
	outportb(DMA_MODE_HI,0x17);     //demand xfr on ch7
	outportb(DMA_CLEAR_FFHI,0);     //reset flip-flop
	outportb(DMA_COUNT_CH7,(FRAME_SIZE+size)&0xff);
	outportb(DMA_COUNT_CH7,(FRAME_SIZE+size)>>8);
	address = _DS;
	address = address<<4;
	address = address+ ((unsigned long)receive&0xffff);
	outportb(DMA_PAGE_CH7,address>>16);
	address = address>>1; //adjust for 16bit dma
	outportb(DMA_CLEAR_FFHI,0);
	outportb(DMA_ADD_CH7,address&0xff);
	outportb(DMA_ADD_CH7,address>>8);
	outportb(DMA_MASK_HI,3);        //enable chan7
	outportb(DMA_COMMAND_HI,0);     //enable dma
	
	}
//this really isn't necessary except for the DMA bit of XBCH
//but the transmit count is programmed in setup_dmat()
i = FRAME_SIZE-1;       //transmit XBCH:XBCL +1 bytes so set XBCH:XBCL to framesize-1
i = i&0x0fff;
i = i|0x8000;
outportb(XBCL,i&0xff);
outportb(XBCH,i>>8);    //set for dma mode and FRAME_SIZE bytes per frame
}

void interrupt hscx_isr_dma(){
unsigned i,j,k;
do{
i = inportb(ISTAT);             //get interrupt status
if((i&XPR)==XPR)                //transmit ready
	{
		if(tbuf.valid==1)       //if tbuf is valid and we are here
					//then we are done sending
			{
			tbuf.no_bytes = FRAME_SIZE;     //show how many bytes sent
			tbuf.valid=0;                   //invalidate tbuf
			}
	}
if((i&EXB)==EXB) {      //extended interrupt
		
		j = inportb(EXIR);      //get the extended register
		if(j&EXE==EXE) {        //if we are here then someone is 
					//not giving up the bus to allow
					//our dma cycles to happen
					//frequently enough
				tbuf.valid=0;   //invalidate the frame
				tbuf.no_bytes=0;        //show no bytes transfered
				}
		 if(j&RFO==RFO){//if here we have received more bytes than we could 
				//handle at one time
				outportb(CMDR,RHR);    //reset the receiver
				rbuf[current_buf].no_bytes = 0;
				setup_dmar(rbuf[current_buf].frame); //sets address of rbuf.frame[] to dma channel and enables it
				}
		}
if((i&RME)==RME){//if we are here then the DMA reception has gone to completion
		 //(we have a complete frame)
		//get the number of bytes received (total)
		k = ((inportb(RBCH)&0x0f)<<8)+inportb(RBCL);
		outportb(CMDR,RMC);     //release the FIFO
		rbuf[current_buf].valid = 1;         //validate the frame buffer
		rbuf[current_buf].no_bytes = k;      //indicate the number of bytes received
		current_buf++;
		if(current_buf==MAX_RBUFS) current_buf = 0;
		//if(rbuf[current_buf].valid==1); an error occured and the 
					       //   main program isn't done with 
					       //   the frame that is about
					       //   to be used for received data
		rbuf[current_buf].no_bytes = 0;  //prep buf for use
		rbuf[current_buf].valid = 0;    //invalidate it for use
		setup_dmar(rbuf[current_buf].frame); //sets address of rbuf.frame[] to dma channel and enables it
		}
if((i&RPF)==RPF){       //we shouldn't ever be here in DMA mode
		outportb(CMDR,RMC); //release the fifo
		
		}


}while(i!=0);   //keep doing this until no more interrupts

outportb(0x20,0x20);    //nonspecific EOI
if(hscx_setup.irq>8) outportb(0xa0,0x20);//upper interrupt EOI
}


