/*++

Copyright (c) 1995,1997,1998 Commtech, Inc Wichita ,KS

Module Name:

    reconly.c

Abstract:

    A simple console app that will do receive only.
	Bare bones receive HDLC application.

Environment:

    user mode only

Notes:

    
Revision History:

    6/5/98             started
--*/



#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include "hscxtest.h"


		

VOID
main(
    )
/*++

Routine Description:

Arguments:

Return Value:

--*/
{
HANDLE hDevice;                 //handle to the hscx driver/device
struct clkset hscxclock;

struct setup hscxsetup; //setup structure for initializing the hscx registers (see hscxtest.h)
char data[4096];                //character array for data storage (passing to and from the driver)
DWORD nobytestoread;    //the number of bytes to read from the driver
DWORD nobytesread;
ULONG j,k;                              //temp vars
DWORD returnsize;               //temp vars
ULONG i;                                //temp vars
ULONG timeout;
BOOL t;                                 //temp vars
OVERLAPPED  os ;                                //overlapped structure for use in the transmit routine
DWORD rframecount;
DWORD errorcount;

memset( &os, 0, sizeof( OVERLAPPED ) ) ;        //wipe the overlapped struct

// create I/O event used for overlapped structure

os.hEvent = CreateEvent( NULL,    // no security
			    TRUE,    // explicit reset req
			    FALSE,   // initial event reset
			    NULL ) ; // no name
if (os.hEvent == NULL)
   {
      MessageBox( NULL, "Failed to create event for thread!", "main Error!",
		  MB_ICONEXCLAMATION | MB_OK ) ;
      return; 
   }


nobytestoread = 4096;    //this should allways be 4096, it is the maximum frame size returned from the driver at one time

    
//this will start up the driver, it also gives us a handle to send
//and receive data from the driver
//note that it is created with the FILE_FLAG_OVERLAPPED set
//if it is not then you must make sequential calls to the driver
//(ie wait until one call has returned before making another call)
//the read/write and status functions are set up to use overlapped, they 
//will return immediatly with a ERROR_IO_PENDING or immediatly with the required data
//


    hDevice = CreateFile ("\\\\.\\HSCX0",
			  GENERIC_READ | GENERIC_WRITE,
			  0,
			  NULL,
			  OPEN_EXISTING,
			  FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
			  NULL
			  );

    if (hDevice == INVALID_HANDLE_VALUE)
    {
		//for some reason the driver won't load or isn't loaded
	printf ("Can't get a handle to hscxdrv\n");
		exit(1);
		//abort and leave here!!!
	}
	
//NOTE!! The ICD2053B is only present on rev 3.4 and later Fastcom:HSCX boards
//The following will have no effect on earlier revisions of the board.
//
//this IOCTL sets the onboard ICD2053B clock generator output.
hscxclock.clockbits = 0x333208; //1.544Mhz
hscxclock.numbits = 22;

//4M 0x1d40e0 (24 bits)
//8M  0x1d30e0  (24 bits)
//1.544M  0x333208 (22 bits)
//2.048M  0x142930 (22 bits)
//2M    0x1d50e0 (24 bits)
//use bitcalc to come up with others pay attention to how many bits are displayed in the
//stuffed bits field (to use in the -numbits field)
//(or look at the calculate_bits() function found on the escc disk in the
//escc_rate_set example program.)

	t = DeviceIoControl(hDevice,IOCTL_HSCXDRV_SET_CLOCK_GENERATOR,&hscxclock,sizeof(struct clkset),NULL,0,&returnsize,NULL);




	//to initialize the hscx to hscxsetup parameters
	//this IOCTL function must be called to setup the hscx internal 
	//registers prior to calling any of the read/write functions
	//for information reguarding what each register does refer to 
	//the SIEMENS 82526 data sheet.
	//

	hscxsetup.hscx_mask = 0x06; //everything but channel A
	hscxsetup.hscx_mode = 0x08a;//transparent mode 0 (no address recog) external timer, res 512, no loop
	hscxsetup.hscx_timr = 0xe7;//not used
	hscxsetup.hscx_xad1 = 0xff;//no tx address/not used
	hscxsetup.hscx_xad2 = 0xff;//no tx address/not used
	hscxsetup.hscx_rah1 = 0xff;//receive address to receive frames by/not used
	hscxsetup.hscx_rah2 = 0xff;//optional 2nd address to receive frames by/not used
	hscxsetup.hscx_ral1 = 0xff;//low byte address (receive)/not used
	hscxsetup.hscx_ral2 = 0xff;//low byte address (receive)/not used
	hscxsetup.hscx_xbcl = 0x00;//used for dma /not used
	hscxsetup.hscx_bgr  = 0x00;//used for baud rate clock modes 
	hscxsetup.hscx_ccr2 = 0x38;//misc functions 
	hscxsetup.hscx_xbch = 0x00;//used for dma (0x80 == use DMA, 0x00 == no DMA)/not used
	hscxsetup.hscx_rlcr = 0x00;//no receive length check
	hscxsetup.hscx_tsar = 0x00;//not used
	hscxsetup.hscx_tsax = 0x00;//not used
	hscxsetup.hscx_xccr = 0x00;//not used
	hscxsetup.hscx_rccr = 0x00;//not used
	hscxsetup.hscx_ccr1 = 0x92;//using DPLL to recover clock at presumed to be 48250 bps nominal
				   //using this (clock mode 2) the bitrate
				   //will be 1.544E6/((0+1)*2)/16 = 48250 bps

	//when called this IOCTL will set the registers of the hscx and
	//reset the TX and RX machines (issuing a separate XRES and RHR command)
	//if the resets time out the function will return FALSE, otherwise it will
	//return TRUE
	t = DeviceIoControl(hDevice,IOCTL_HSCXDRV_SETUP,&hscxsetup,sizeof(struct setup),NULL,0,&returnsize,NULL);
	if(t==TRUE)printf("SETTINGS SUCCESSFUL:%lu\n\r",returnsize);
	if(t==FALSE)
		{
		printf("SETTINGS FAILED:%lu\n\r",returnsize);
		exit(1);
		}

//now we enter the main loop of this thread        
//all we are going to do is wait for frames to be received.
//if the [ESC] key is pressed the program will terminate
errorcount = 0;
rframecount= 0;
start:
do
	{

	//start a read request by calling ReadFile() with the hscxdevice handle
	//if it returns true then we received a frame 
	//if it returns false and ERROR_IO_PENDING then there are no
	//receive frames available so we wait until the overlapped struct
	//gets signaled. (indicating that a frame has been received)
	timeout = 0;
	t = ReadFile(hDevice,&data,nobytestoread,&nobytesread,&os);
	//if this returns true then your system is not reading the frames from the 
	//driver as fast as they are coming in.
	//if it returns false then the system is processing the frames quick enough
	
	if(t==FALSE)
		{
		if (GetLastError() == ERROR_IO_PENDING)
			{
			// wait for a receive frame to come in
			do
				{
				j = WaitForSingleObject( os.hEvent, 1000 );//1 second timeout
				if(j==WAIT_TIMEOUT)
					{
					//this will execute every 1 second that a frame is not received
					//you could put a counter in here and if the 
					//driver takes an inordinate ammout of time
					//to complete, you could issue a flush RX command
					//and break out of this loop
					timeout++;
					if(timeout > 60) 
						{
						printf("no data received for 1 minute...aborting\r\n");
						goto quitprog;
						}
					if(kbhit())
						{
						if(getch()==27)
							{
							printf("exiting on keypress\r\n");
							goto quitprog;
							}
						}
					}
				if(j==WAIT_ABANDONED)
					{
					}
				
				}while(j!=WAIT_OBJECT_0);//stay here until we get signaled or the main thread exits
			
				
			}
		}                                                     
		GetOverlappedResult(hDevice,&os,&nobytesread,TRUE); //here to get the actual nobytesread!!!
		if((data[nobytesread-1]&0x20)!=0x20) errorcount++;//last byte in received frame is contents of RSTA register, this is a check of the CRC if CRC fails then errorcount increments
		rframecount++;
		//to maximize speed you should not print the data to the screen, but 
		//rather insert your processing on the received data here.
		
		//printf("%lu %lu \r",rframecount,errorcount);
		//printf("received %u bytes:\n\r",nobytesread);    //display the number of bytes received
		for(i=0;i<nobytesread;i++)printf("%c",data[i]);  //display the buffer
		printf("\n\r");
		DeviceIoControl(hDevice,IOCTL_HSCXDRV_IMMEDIATE_STATUS,NULL,0,&k,sizeof(DWORD),&returnsize,NULL);
		if(k!=0) printf("\r\nSTATUS: %lx\r\n",k);
			//you could decode the returned here 
			//they are the ST_XXXXXX defines in the .h file
			//basically if you get a ST_OVF or a ST_RFO then you are having
			//problems getting the data through the system
			//(the RFO is a hardware overflow)
			//the OVF is a software (driver buffers) overflow.
	}while(!kbhit());         
	j = getch();
if(j!=27) goto start;
quitprog:
printf("%lu %lu \r",rframecount,errorcount);//display received framecount +#crc errors
printf("\r\n");


CloseHandle (hDevice);// stops the hscx from interrupting (ie shuts it down)
CloseHandle (os.hEvent);//done with event
}                                               //done

							 


