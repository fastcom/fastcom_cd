/*++

Copyright (c) 1995,1997 Commtech, Inc Wichita ,KS

Module Name:

    tronly.c

Abstract:

    A simple Win32 app that uses the hscxdrv device to transmit HDLC frames

Environment:

    user mode only

Notes:

    
Revision History:

    6/16/98             started
--*/



#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include "hscxtest.h"



VOID
main(
    )
/*++

Routine Description:

Arguments:

Return Value:

--*/
{
HANDLE hDevice;                 //handle to the hscx driver/device

DWORD nobyteswritten;   //number of bytes that the driver returns as written to the device
struct setup hscxsetup; //setup structure for initializing the hscx registers (see hscxtest.h)
struct clkset hscxclock;

char data[4096];                //character array for data storage (passing to and from the driver)
DWORD nobytestowrite;   //the number of bytes to write to the driver
ULONG j,k;                              //temp vars
DWORD returnsize;               //temp vars
ULONG i;                                //temp vars
BOOL t;                                 //temp vars
OVERLAPPED  os ;                                //overlapped structure for use in the transmit routine

memset( &os, 0, sizeof( OVERLAPPED ) ) ;        //wipe the overlapped struct

// create I/O event used for overlapped write

os.hEvent = CreateEvent( NULL,    // no security
			    TRUE,    // explicit reset req
			    FALSE,   // initial event reset
			    NULL ) ; // no name
if (os.hEvent == NULL)
   {
      MessageBox( NULL, "Failed to create event for thread!", "main Error!",
		  MB_ICONEXCLAMATION | MB_OK ) ;
      return; 
   }


nobytestowrite = 1024;   //this is the number of bytes to send to the driver to send as a single frame (can be from 1 to 4096)
    
//this will start up the driver, it also gives us a handle to send
//and receive data from the driver
//note that it is created with the FILE_FLAG_OVERLAPPED set
//if it is not then you must make sequential calls to the driver
//(ie wait until one call has returned before making another call)
//the read/write and status functions are set up to use overlapped, they 
//will return immediatly with a ERROR_IO_PENDING or immediatly with the required data
//


    hDevice = CreateFile ("\\\\.\\HSCX0",
			  GENERIC_READ | GENERIC_WRITE,
			  0,
			  NULL,
			  OPEN_EXISTING,
			  FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
			  NULL
			  );

    if (hDevice == INVALID_HANDLE_VALUE)
    {
		//for some reason the driver won't load or isn't loaded
	printf ("Can't get a handle to hscxdrv\n");
		exit(1);
		//abort and leave here!!!
	}

//NOTE!! The ICD2053B is only present on rev 3.4 and later Fastcom:HSCX boards
//The following will have no effect on earlier revisions of the board.
//
//this IOCTL sets the onboard ICD2053B clock generator output.
hscxclock.clockbits = 0x333208; //1.544Mhz
hscxclock.numbits = 22;

//4M 0x1d40e0 (24 bits)
//8M  0x1d30e0  (24 bits)
//1.544M  0x333208 (22 bits)
//2.048M  0x142930 (22 bits)
//2M    0x1d50e0 (24 bits)
//use bitcalc to come up with others pay attention to how many bits are displayed in the
//stuffed bits field (to use in the -numbits field)
//(or look at the calculate_bits() function found on the escc disk in the
//escc_rate_set example program.)

	t = DeviceIoControl(hDevice,IOCTL_HSCXDRV_SET_CLOCK_GENERATOR,&hscxclock,sizeof(struct clkset),NULL,0,&returnsize,NULL);


	

//register settings for transparent HDLC mode 0 (no address recognition)	
//will generate a frame sequence as:
// FLAG|data|CRC|CRC|FLAG, with FLAG == 0x7E, and data being what we send to writefile

	hscxsetup.hscx_mask = 0x06; //everything but channel A
	hscxsetup.hscx_mode = 0x88;//transparent mode 0 
	hscxsetup.hscx_timr = 0x1f;//not used
	hscxsetup.hscx_xad1 = 0xff;//no tx address/not used
	hscxsetup.hscx_xad2 = 0xff;//no tx address/not used
	hscxsetup.hscx_rah1 = 0xff;//receive address to receive frames by/not used
	hscxsetup.hscx_rah2 = 0xff;//optional 2nd address to receive frames by/not used
	hscxsetup.hscx_ral1 = 0xff;//low byte address (receive)/not used
	hscxsetup.hscx_ral2 = 0xff;//low byte address (receive)/not used
	hscxsetup.hscx_xbcl = 0x00;//used for dma /not used
	hscxsetup.hscx_bgr  = 0x00;//used for baud rate clock modes 
	hscxsetup.hscx_ccr2 = 0x38;//misc functions 
	hscxsetup.hscx_xbch = 0x00;//used for dma (0x80 == use DMA, 0x00 == no DMA)/not used
	hscxsetup.hscx_rlcr = 0x00;//no receive length check
	hscxsetup.hscx_tsar = 0x00;//not used
	hscxsetup.hscx_tsax = 0x00;//not used
	hscxsetup.hscx_xccr = 0x00;//not used
	hscxsetup.hscx_rccr = 0x00;//not used
	hscxsetup.hscx_ccr1 = 0x92;
				   //using this (clock mode 2) the bitrate
				   //will be 1.544E6/((0+1)*2)/16 = 48250 bps

	//when called this IOCTL will set the registers of the hscx and
	//reset the TX and RX machines (issuing a separate XRES and RHR command)
	//if the resets time out the function will return FALSE, otherwise it will
	//return TRUE
	t = DeviceIoControl(hDevice,IOCTL_HSCXDRV_SETUP,&hscxsetup,sizeof(struct setup),NULL,0,&returnsize,NULL);
	if(t==TRUE)printf("SETTINGS SUCCESSFUL:%lu\n\r",returnsize);
	if(t==FALSE)printf("SETTINGS FAILED:%lu\n\r",returnsize);
start:
do
	{
	for(i=0;i<1024;i++)data[i] = (char)0x55;//fill the frame 

    t = WriteFile(hDevice,&data,nobytestowrite,&nobyteswritten,&os);//send the frame
	//if this returns true then there was idle time before the frame went out.
	//in order to get back to back frames you must be able to call this fast enough
	//to keep it returning false/iopending to keep the next frame to be sent queued to go out immediately after the currently being sent frame.
	if(t==FALSE)                    //if it returned FALSE then the IO request is queued (waiting for previous frame to get done sending)
		{                       //and we must wait until the os.event gets signaled before we try any more sending
		if (GetLastError() == ERROR_IO_PENDING)  //IO_PENDING is the indication that the request was queued, if not this than a parameter error occured
			{
			// wait for a second for this transmission to complete
			do
				{
				j = WaitForSingleObject( os.hEvent, 1000 );//1 second timeout (one frame send should be 171mS (or (1024*8)+(4*8))/bitrate), so a 1sec timeout is sufficient)
				if(j==WAIT_TIMEOUT)
					{
					//this will execute every 1 second that the the write file does not return
					//you could put a counter in here and if the 
					//driver takes an inordinate ammout of time
					//to complete, you could issue a flush TX command
					//and break out of this loop
					
					printf("Transmitter Locked up...resetting tx\r\n");
					DeviceIoControl(hDevice,IOCTL_HSCXDRV_FLUSH_TX,NULL,0,NULL,0,&returnsize,NULL);
					DeviceIoControl(hDevice,IOCTL_HSCXDRV_IMMEDIATE_STATUS,NULL,0,&k,sizeof(DWORD),&returnsize,NULL);
					if(k!=0) printf("\r\nSTATUS: %lx\r\n",k);
					//the most likely cause of failure on tx is a data rate too fast for the machine
					//to keep up with (indicated by EXE/XDU interrupt occurance)
					//it basically means that the time that it takes to send 32 bytes out of the device 
					//is shorter than the ammount of time that the machine needs to service an interrupt
					}
				if(j==WAIT_ABANDONED)
					{
					printf("abandoned\r\n");
					
					}
				
				}while(j!=WAIT_OBJECT_0);
			}
		}               
tryagain:
;
Sleep(1000);	//if you are going for back to back frames remove this, as you will want no delay between writes
				//if you are using this to generate frames for display on say the reconly example program
				//then it would be good to leave the delay such that the reconly program has time to display the received frame contents 
				//before another frame arrives (or you will get a receive buffers overflow condition on receive).
	}while(!kbhit());
j = getch();
if(j!=27) goto start;
	
	CloseHandle (hDevice);// stops the hscx from interrupting (ie shuts it down)
	CloseHandle (os.hEvent);//done with event
	printf("exiting program\n\r");          //exit message
}                                               //done

