Refer to the examples directory to see how you can use the Superfastcom.

At a minimum, your program should issue the commands equivalent to:

'setclock.exe port rate'
(setfs6131clock.exe port rate if you have an extended temp board) 

and 

'sfcset.exe port setfile'
  -hdlcset sets a generic HDLC/SDLC configuration
  -asyncset sets a generic ASYNC configuration
  -bisyncset sets a generic BISYNC configuration)
  -Modify these settings as your environment demands.  
  -Refer to section 10.3.2 of the PEB20534 manual for detailed descriptions of these settings.


The raw directory contains an example of transmitting and receiving raw bits without any framing or any sync signals.

The utils_src directory contains the C code to all of the example utilits.

sfc_advanced_settings.exe is a graphical interface to the configuration settings of sfcset.exe

sfc_mfc.exe is a superfastcom terminal-like program that you can use for testing purposes.
