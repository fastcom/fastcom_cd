/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
simuln200.c -- generate 26 byte hdlc frames simulating a LN200 unit

  usage:
  simuln200 port 
  
	port is the SFC port to use
	
*/


#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "..\sfc.h"

struct LN_200_Data {
	signed short deltaV_x;
	signed short deltaV_y;
	signed short deltaV_z;
	signed short deltat_x;
	signed short deltat_y;
	signed short deltat_z;
	unsigned short IMU_status;
	unsigned short mode_bit;
	unsigned short mux_data;
	unsigned short raw_gyro[3];
	unsigned short checksum;
};

int main(int argc, char *argv[])
{
	HANDLE wDevice;/* Handle for the SuperFastcom port */
	ULONG t;
	DWORD nobyteswritten;
	char tdata[512];
	ULONG size;
	OVERLAPPED  wq;
	OVERLAPPED  st;
	int j,error,tosend;
	ULONG totalsent;
	ULONG totalread;
	ULONG totalerror;
	ULONG loop;
    char devname[25];
	SCC_REGS settings;
	clkset clk;
	ULONG i;
	ULONG passval[4];
	ULONG descparm[4];
	ULONG startsend=0;
	ULONG reg;
	ULONG val;
	char key;
	unsigned dvx,dvy,dvz,dtx,dty,dtz;
	struct LN_200_Data lndata;
	ULONG mask;
	ULONG status;
	ULONG temp;
	dvx = 0;
	dvy = 0;
	dvz = 0;
	dtx = 0;
	dty = 0;
	dtz = 0;
	
	memset(&lndata,0,sizeof(struct LN_200_Data));
	
	if(argc<2)
	{
		printf("usage:\n");
		printf("%s port \n",argv[0]);
		exit(1);
	}
    sprintf(devname,"\\\\.\\SFC%d",atoi(argv[1]));
	printf("devicename:%s\n",devname);
	memset(tdata,0,sizeof(tdata));
	memset( &st, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
	st.hEvent = CreateEvent( NULL,    // no security
		TRUE,    // explicit reset req
		FALSE,   // initial event reset
		NULL ) ; // no name
	if (st.hEvent == NULL)
	{
		//MessageBox( NULL, "Failed to create event for thread!", "main Error!",
		//	MB_ICONEXCLAMATION | MB_OK ) ;
		printf("Failed to create event for thread!\n");
		exit(1);
	}
	
	memset( &wq, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
	wq.hEvent = CreateEvent( NULL,    // no security
		TRUE,    // explicit reset req
		FALSE,   // initial event reset
		NULL ) ; // no name
	if (wq.hEvent == NULL)
	{
		printf("Failed to create event for thread!\n");
		return 1; 
	}
	//	printf("write overlapped event created\n");
	wDevice = CreateFile (devname,
		GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL,
		OPEN_ALWAYS,
		FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
		NULL
		);
    if (wDevice == INVALID_HANDLE_VALUE)
    {
		//for some reason the driver won't load or isn't loaded
		printf ("Can't get a handle to sfcdrv\n");
		exit(1);
		//abort and leave here!!!
	}
	//printf("sfcdrv handle created\n");
	
	clk.clockbits = 0x16b190;//set clock generator to 1.024 MHz 
	clk.numbits = 22;
	//this IOCTL function demonstrates how to set the clock generator on 
	//the sfc card
	//this function will return TRUE unless an invalid parameter is given
	//
	DeviceIoControl(wDevice,IOCTL_SFCDRV_SET_CLOCK,&clk,sizeof(clkset),NULL,0,&t,NULL);
	
	memset(&settings,0,sizeof(SCC_REGS));
	
	settings.cmdr = 0x01010000;
	settings.star = 0x0;
	settings.ccr0 = 0x80000037;//clock mode 7
	settings.ccr1 = 0x02248000;
	settings.ccr2 = 0x00040000;//start with hdlc receiver OFF (no receive)
	settings.imr = 0xFFFE3FFF;//disable all but CSC interrupt sources (1pps pulse), timer and xdu
	settings.timr = 0x070009ff;//perodic timer every 2560 txclock pulses (1/1024000*2560 = .0025 (or 400 timeouts/second))
	
/*
	//these all default to 0, on the memset above
	settings.accm = value;
	settings.udac = value;
	settings.tsax = value;
	settings.tsar = value;
	settings.pcmmtx = value;
	settings.pcmmrx = value;
	settings.brr = value;
	settings.xadr = value;
	settings.radr = value;
	settings.ramr = value;
	settings.rlcr = value;
	settings.xnxfr = value;
	settings.tcr = value;
	settings.ticr = value;

	settings.syncr = value;
	settings.isr = 0x0;
*/
	
	
	
	DeviceIoControl(wDevice,
		IOCTL_SFCDRV_SCC_REGISTERS_SET,         /* Device IOCONTROL */
		&settings,							/* write data */
		sizeof(settings)				,	/* write size */
		NULL,								/* read data */
		0,									/* read size */
		&t,								/* Returned Size */
		NULL);								/* overlap */

	i=1;//mask FI interrupts
	DeviceIoControl(wDevice,
		IOCTL_SFCDRV_SET_TX_FI_MASK,                            /* Device IOCONTROL */
		&i,								/* write data */
		sizeof(unsigned long ),						/* write size */
		NULL,								/* read data */
		0,									/* read size */
		&t,								/* Returned Size */
		NULL); /* overlap */

	//since we will be writing 400 frames per second, there will allways
	//be idle between frames, so we don't need any interrupts generated
	//to keep things going continuous...
	i=0; //minimum interrupts 
	DeviceIoControl(wDevice,
		IOCTL_SFCDRV_SET_TX_IRQ_RATE,                            /* Device IOCONTROL */
		&i,								/* write data */
		sizeof(unsigned long ),						/* write size */
		NULL,								/* read data */
		0,									/* read size */
		&t,								/* Returned Size */
		NULL); /* overlap */

	//bad things happen if you do many allocs/deallocs on a single boot
	//so it is probably a good idea to only alloc these one time
	DeviceIoControl(wDevice,IOCTL_SFCDRV_GET_DESC_PARAMETERS,NULL,0,&descparm,4*sizeof(ULONG),&t,NULL);
	if((descparm[2]<400)||(descparm[3]<32))
	{
		
		passval[0]= 5;//
		passval[1]= 32;//will only use 27 bytes of it, but has to be mult of 4
		passval[2]= 400;
		passval[3]= 32;

		DeviceIoControl(wDevice,
			IOCTL_SFCDRV_CHNG_DESC_LEN,                         /* Device IOCONTROL */
			&passval,								/* write data */
			4*sizeof(unsigned long ),						/* write size */
			NULL,								/* read data */
			0,									/* read size */
			&t,								/* Returned Size */
			NULL); /* overlap */
	}
	//start clean:
	DeviceIoControl(wDevice,IOCTL_SFCDRV_FLUSH_TX,NULL,0,NULL,0,&t,NULL);
	
	totalerror=0;
	totalsent=0;
	totalread=0;
	loop=0;
	size=26;	
top:
	while(!kbhit())
	{
		error=0;
		
		mask = 0x0001c000;//only care about XDU,TIN and CSC interrupts;
		t=	DeviceIoControl(wDevice,IOCTL_SFCDRV_STATUS,&mask,sizeof(ULONG),&status,sizeof(ULONG),&t,&st);
		if(t==FALSE)  
		{
			t=GetLastError();
			if(t==ERROR_IO_PENDING)
			{
				do
				{
					j = WaitForSingleObject( st.hEvent, 5000 );//5 second timeout -- must be greater than size*8*(1/bitrate)*1000
					if(j==WAIT_TIMEOUT)
					{
						if(kbhit())
						{
							CancelIo(wDevice);
							goto kht;
						}
						printf("Timeout.\r\n");
					}
					if(j==WAIT_ABANDONED)
					{
						printf("Abandoned.\r\n");
						goto close;
					}
				} while(j!=WAIT_OBJECT_0);
			}
			else printf("ERROR: #%x\n",t);
		}
		
		if((status&0x00004000)!=0)
		{
			//read state of CTS line (STAR bit24)
			reg = (0x104)+(0x80*(atoi(argv[1])%4));
			DeviceIoControl(wDevice,IOCTL_SFCDRV_READ_REGISTER,&reg,sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);
			if((val&0x01000000)!=0)
			{
				//cts is active (low) (ie falling edge of CTS)
				startsend=1;//enable output, on first timeout after now
				passval[0] = (0x100)+(0x80*(atoi(argv[1])%4));
				passval[1] = 0x00000100;//start timer
				DeviceIoControl(wDevice,IOCTL_SFCDRV_WRITE_REGISTER,&passval,2*sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);
				goto startsend;
			}
			else
			{
				//cts is inactive (high) (ie rising edge of CTS)
			}
		}
		
		if((status&0x00008000)!=0)
		{
			if(startsend==1)
			{
				//
startsend:
			tosend=size;
			lndata.raw_gyro[0]++; //debugging, put counter in gyro0, increments once per frame
			t = WriteFile(wDevice,&lndata,sizeof(lndata),&nobyteswritten,&wq);
			if(t==FALSE)  
			{
				t=GetLastError();
				if(t==ERROR_IO_PENDING)
				{
					do
					{
						j = WaitForSingleObject( wq.hEvent, 5000 );//5 second timeout -- must be larger than size*8*(1/bitrate)*1000
						if(j==WAIT_TIMEOUT)
						{
							printf("Transmitter Locked up... Resetting TX.\r\n");
							//DeviceIoControl(wDevice,IOCTL_SFCDRV_FLUSH_TX,NULL,0,NULL,0,&returnsize,NULL);
						}
						if(j==WAIT_ABANDONED)
						{
							printf("Recieve Abandoned.\r\n");
							goto close;
						}
					} while(j!=WAIT_OBJECT_0);
					GetOverlappedResult(wDevice,&wq,&nobyteswritten,TRUE);
				}
				else printf("WRITE ERROR: #%d\n",t);
			}
			if(nobyteswritten!=size)
			{
				printf("unexpected actual:%lu, sent:%lu\n",nobyteswritten,size); 
			}
			loop++;
			totalsent+=nobyteswritten;
			}
		}
		
		if((status&0x00010000)!=0)
		{
			//XDU
			printf("XDU interrupt, !!!\r\n");
		}
		
	}
kht:
	key = getch();
	if(key=='x')
	{
		dvx++;
		printf("deltav_x++ : %u\r\n",dvx);
	}
	if(key=='y')
	{
		dvy++;
		printf("deltav_y++ : %u\r\n",dvy);
	}
	if(key=='z')
	{
		dvz++;
		printf("deltav_z++ : %u\r\n",dvz);
	}
	if(key=='t') 
	{
		dtx++;
		dty++;
		dtz++;
		printf("deltat++: %u\r\n",dtx);
	}
	
	lndata.deltaV_x = dvx;
	lndata.deltaV_y = dvy;
	lndata.deltaV_z = dvz;
	lndata.deltat_x = dtx;
	lndata.deltat_y = dty;
	lndata.deltat_z = dtz;
	lndata.IMU_status = 0;
	lndata.mode_bit = 0;
	lndata.mux_data = 0;
	lndata.checksum = dvx+dvy+dvz+dtx+dty+dtz;
	//lndata.raw_gyro[0] = 0;
	lndata.raw_gyro[1] = 0;
	lndata.raw_gyro[2] = 0;
	
	if(key=='i')
	{
		//force start of timer as if CTS happened
		startsend=1;//enable output, on first timeout after now
		passval[0] = (0x100)+(0x80*(atoi(argv[1])%4));
		passval[1] = 0x00000100;//start timer
		DeviceIoControl(wDevice,IOCTL_SFCDRV_WRITE_REGISTER,&passval,2*sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);
	}
	
	if(key!=27) goto top;
	
	printf("count %lu\n",loop);
	DeviceIoControl(wDevice,IOCTL_SFCDRV_FLUSH_TX,NULL,0,NULL,0,&t,NULL);
close:
	passval[0] = (0x130)+(0x80*(atoi(argv[1])%4));
	passval[1] = 0x070009ff;//stop timer by writing it
	DeviceIoControl(wDevice,IOCTL_SFCDRV_WRITE_REGISTER,&passval,2*sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);
	
	CloseHandle(wq.hEvent);
	CloseHandle(st.hEvent);
	CloseHandle (wDevice);
	return 0;
}
/* $Id$ */
