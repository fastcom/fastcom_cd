/* $Id$ */
/*
  Copyright(C) 2002, Commtech, Inc.

  read_timetag.c -- a user mode program to read bytes from a channel and stuff them to a file

  Abstract:

    A simple console app that will do receive only.
	Bare bones receive HDLC application.
	
	The TIME_TAG_FRAMES will do a KeQuerySystemTime() call followed by
	at gettsc() call (returns CPU TSC register).  The systemtime is 
	turned into a TIME_FIELDS struct via a call to RtlTimeToTimeFields() call
	This time value is GMT time. the result is stored in the received frame
	that is then packed as:
	
	SYSTEMTIME timetag;
	__int64 tsc_value;
	char data[];


	The systemtime value seems to only be updated every 15mS or so on my system
	(the DDK docs say that it is updated  "approx every 10mS")
	however if you have many frames received in close proximity to the 
	changeover, then the tsc_value can be associated with the systemtime
	value at the point of systemtime change, further calculations from that tsc count will be
	resonable.  If you don't have many frames being received (per 10~15mS timeframe)
	but you do occasionally receive two or more frames with the same timetag, you can
	take the tsc_count difference between the frames and get a very good
	time resolution between the frames (albeit both/all off from "true" systemtime anywhere from 0->15mS)
	
	If you need Local time use SystemTimeToTzSpecificLocalTime() to convert it.  

	It appears that QueryPerformanceFrequency() can be used to get the scaling factor for the tsc_value.

	usage:
	read port size outfile
	
	  
*/


#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "..\sfc.h"

int main(int argc, char *argv[])
{
	HANDLE rDevice;
	FILE *fout;
	ULONG t;
	int i;
	DWORD nobytesread;
	char *rdata;
	ULONG size;
	OVERLAPPED  rq;
	int j,error;
	ULONG totalsent;
	ULONG totalread;
	ULONG totalerror;
	ULONG loop;
    char devname[25];
	ULONG desc_parm[4];
	SYSTEMTIME *tag;
	__int64 *tsc;
	char *buffer;
	
	SYSTEMTIME taglast;
	__int64 tsclast = 0;
	double cpuspeed = 1000000000.0; //I was working on a dual 1GHz PIII, set appropriatly to your CPU clock
	double tdiff;
	ULONG passval;
	LARGE_INTEGER licpuspd;

	//seems to work on my system anyway...if not hardcode above and comment this out
	QueryPerformanceFrequency(&licpuspd);
	cpuspeed = (double)(licpuspd.QuadPart);
	printf("CPUSPEED:%.2f\n",cpuspeed);

	memset(&taglast,0,sizeof(SYSTEMTIME));
	
	if(argc<4)
	{
		printf("usage:\n");
		printf("read port size outfile\n");
		exit(1);
	}
	size = atol(argv[2]);
	if(size==0)
	{
		printf("block size cannot be 0\n");
		exit(1);
	}
	fout=fopen(argv[3],"wb");
	if(fout==NULL)
	{
		printf("cannot open output file %s\n",argv[3]);
		exit(1);
	}
	
	sprintf(devname,"\\\\.\\SFC%d",atoi(argv[1]));
	printf("devicename:%s\n",devname);
	
	memset( &rq, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
	rq.hEvent = CreateEvent( NULL,    // no security
		TRUE,    // explicit reset req
		FALSE,   // initial event reset
		NULL ) ; // no name
	if (rq.hEvent == NULL)
	{
		//MessageBox( NULL, "Failed to create event for thread!", "main Error!",
		//	MB_ICONEXCLAMATION | MB_OK ) ;
		printf("Failed to create event for thread!\n");
		fclose(fout);
		exit(1);
	}
	rDevice = CreateFile (devname,
		GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL,
		OPEN_ALWAYS,
		FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
		NULL
		);
	
    if (rDevice == INVALID_HANDLE_VALUE)
    {
		//for some reason the driver won't load or isn't loaded
		printf ("Can't get a handle to sfcdrv\n");
		fclose(fout);
		CloseHandle(rq.hEvent);
		exit(1);
		//abort and leave here!!!
	}
	//allocate memory for read/write
	rdata = (char*)malloc(size+1);
	if(rdata==NULL)
	{
		printf("cannot allocate memory for data area\n");
		fclose(fout);
		CloseHandle(rDevice);
		CloseHandle(rq.hEvent);
		exit(1);
	}
	DeviceIoControl(rDevice,IOCTL_SFCDRV_GET_DESC_PARAMETERS,NULL,0,&desc_parm,4*sizeof(ULONG),&t,NULL);
	if(desc_parm[0]*desc_parm[1]<size)
	{
		printf("driver descriptor total is less than size\n");
		printf("please increase buffering parameters or decrease block size\n");
		free(rdata);
		fclose(fout);
		CloseHandle(rDevice);
		CloseHandle(rq.hEvent);
		exit(1);
	}
	passval=1;//set to timetag on RME (FE) interrupts
	DeviceIoControl(rDevice,IOCTL_SFCDRV_SET_TIMETAG_CONTROL,&passval,sizeof(unsigned long ),NULL,0,&t,NULL);
	/* Flush the RX Descriptors so not as to have any complete descriptors in their
	* the first read in hdlc will get those left over frames and this test program
	* would not be of any use. */
	//printf("flush rx\n");
	DeviceIoControl(rDevice,IOCTL_SFCDRV_FLUSH_RX,NULL,0,NULL,0,&t,NULL);
	
	totalerror=0;
	totalsent=0;
	totalread=0;
	loop=0;
	
	while(!kbhit())
	{
		
		error=0;
		t = ReadFile(rDevice,&rdata[0],size,&nobytesread,&rq);
		if(t==FALSE)  
		{
			//	printf("read blocked\n");
			t=GetLastError();
			if(t==ERROR_IO_PENDING)
			{
				do
				{
					j = WaitForSingleObject( rq.hEvent, 5000 );//5 second timeout -- must be greater than size*8*(1/bitrate)*1000
					//			printf("after wait:%lx\n",j);
					if(j==WAIT_TIMEOUT)
					{
						if(kbhit()) goto done;
						printf("Reciever Locked up... Resetting RX.\r\n");
						//DeviceIoControl(rDevice,IOCTL_SFCDRV_FLUSH_TX,NULL,0,NULL,0,&returnsize,NULL);
					}
					if(j==WAIT_ABANDONED)
					{
						printf("Reciever Abandoned.\r\n");
						goto close;
					}
				} while(j!=WAIT_OBJECT_0);
				GetOverlappedResult(rDevice,&rq,&nobytesread,TRUE);
				//		printf("postblock read:%d\n",nobytesread);
			}
			else printf("READ ERROR: #%x\n",t);
		}
		//      else printf("read returned true:%d\n",nobytesread);
		
		printf("[%d]READ %d\n\n",t,nobytesread);
		if(nobytesread!=size)
		{
			printf("received:%lu, expected %lu\n",nobytesread,size);
		}
		if(nobytesread!=0)
		{
			//totalsent+=fwrite(rdata,1,size,fout);
			tag = (SYSTEMTIME *)&rdata[0];
			tsc = (__int64 *)&rdata[sizeof(SYSTEMTIME)];
			buffer = (char *)&rdata[sizeof(SYSTEMTIME)+sizeof(__int64)];
			
			if((tag->wMonth==taglast.wMonth)&&(tag->wYear==taglast.wYear)&&
				(tag->wDay==taglast.wDay)&&(tag->wHour==taglast.wHour)&&
				(tag->wMinute==taglast.wMinute)&&(tag->wSecond==taglast.wSecond)&&
				(tag->wMilliseconds==taglast.wMilliseconds))
			{
				printf("here\n");
			}
			else
			{
				printf("tag update\n");
				memcpy(&taglast,tag,sizeof(SYSTEMTIME));
				tsclast = tsc[0];
			}
			//			printf("%I64u %I64u %.2f\n",tsc[0],tsclast,cpuspeed);
			tdiff = (double)(tsc[0]-tsclast);
			//			printf("tdiff:%.2f\n",tdiff);
			tdiff = (tdiff*1000000.0)/cpuspeed;
			//			printf("tdiff:%.2f\n",tdiff);
			
			printf("%d/%d/%d %d:%d.%d.%d (%I64u)--+[%.2fuS]",tag->wMonth,tag->wDay,tag->wYear,tag->wHour,tag->wMinute,tag->wSecond,tag->wMilliseconds,tsc[0],tdiff);
			
			for(i=0;i<nobytesread-(sizeof(SYSTEMTIME)+sizeof(__int64));i++)printf("%x:",buffer[i]&0xff);  //display the buffer
			printf("\n\r");
			
		}
		//printf("Found: %d errors\n",error);
		loop++;
		totalread+=nobytesread;
	}
done:
	getch();
	printf("Read  %lu bytes\n",totalread);
	printf("Wrote %lu bytes\n",totalsent);
	printf("Count %lu\n",loop);
	
	passval=0;//turn timetagging off
	DeviceIoControl(rDevice,IOCTL_SFCDRV_SET_TIMETAG_CONTROL,&passval,sizeof(unsigned long ),NULL,0,&t,NULL);
	
close:
	free(rdata);
	fclose(fout);
	CloseHandle(rDevice);
	CloseHandle(rq.hEvent);
	return 0;
}
/* $Id$ */