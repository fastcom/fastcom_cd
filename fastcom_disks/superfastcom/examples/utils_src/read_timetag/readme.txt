8/9/2006
the special driver is no longer needed as of 2.0.0.18, there is a deviceiocontrol function call
that will enable/disable the timetagging (IOCTL_SFCDRV_SET_TIMETAG_CONTROL).  The read_timetag.c code has been modified accordingly.



This bit of code requires a special driver (located in this directory).
To use it, copy the sfcdrv.sys file into your \winnt\system32\drivers directory
reboot, and then give it a try.  This special driver will only work for Windows 2000
or Windows XP.  Do not try to use this driver or example in Windows NT.

The timetag is the time of the approximate end of the reception of the frame
ie the system time is the time of the last byte in the frame NOT the time of
the first byte received.

