/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
sfcset.c -- user mode function to setup the SCC registers & fifo's

usage:
 sfcset port setfile

 The port can be any valid sfc port (0,1,2,3).

 The setfile is a text file that contains the settings to use
 (see the example settings file 'hdlcset')

*/


#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>


#include "..\sfc.h" /* user code header */


int main(int argc, char *argv[])
{
        HANDLE hDevice;/* Handler for the SFC driver */
	int i;
	FILE *fp;
	char dev[9];
	char inputline[100],*ptr;
	unsigned long value;
	DWORD ret;
	SCC_REGS settings;

	if(argc<3)
	{
		printf("%s [port] [file]\n\n",argv[0]);
		printf(" The port is appended onto \\\\.\\SFC\n");
		exit(1);
	}
	i=atoi(argv[1]);
	sprintf(dev,"\\\\.\\SFC%d",i);
	hDevice = CreateFile (dev,
			  GENERIC_READ | GENERIC_WRITE,
			  0,
			  NULL,
			  OPEN_EXISTING,
			  FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
			  NULL
			  );

    if (hDevice == INVALID_HANDLE_VALUE)
    {
		printf("Can't get a handle to sfcdrv @ %s\n",dev);
		exit(1);
	}

	fp = fopen(argv[2],"r");
	if(fp==NULL)
	{
		printf("Cannot open file %s\n",argv[2]);
		exit(1);
	}

	memset(&settings,0,sizeof(SCC_REGS));
	memset(inputline,0,sizeof(inputline));
	
	while(fgets(inputline,100,fp)!=NULL)
	{
		/* zero the comments so not to get confused with key words */
		/* # pound is the comment prefix */
		for(i=0;inputline[i]!='\n';i++)
		{
			if(inputline[i]=='#')
			{
				memset(&inputline[i],0, 100-i );	
				break;
			}

		}
		i = 0;
		while(inputline[i]!=0) 
		{
			inputline[i] = tolower(inputline[i]);
			i++;
		}
		
		if( (ptr = strchr(inputline,'='))!=NULL)
		{
			for(i=1;ptr[i]==' ' && ptr[i]!='\n';i++);
			value = (unsigned long)strtoul(ptr+i,NULL,16);
			//printf("value = 0x%lx\n",(long)value);
		}
		else
			value=0;
			
		if(strncmp(inputline,"cmdr",4)==0) settings.cmdr = value;
		if(strncmp(inputline,"star",4)==0) settings.star = value;
		if(strncmp(inputline,"ccr0",4)==0) settings.ccr0 = value;
		if(strncmp(inputline,"ccr1",4)==0) settings.ccr1 = value;
		if(strncmp(inputline,"ccr2",4)==0) settings.ccr2 = value;
		
		if(strncmp(inputline,"accm",4)==0) settings.accm = value;
		if(strncmp(inputline,"udac",4)==0) settings.udac = value;
		if(strncmp(inputline,"ttsa",4)==0) settings.ttsa = value;
		if(strncmp(inputline,"rtsa",4)==0) settings.rtsa = value;
		if(strncmp(inputline,"pcmmtx",6)==0) settings.pcmmtx = value;
		if(strncmp(inputline,"pcmmrx",6)==0) settings.pcmmrx = value;
		if(strncmp(inputline,"brr",3)==0) settings.brr = value;
		if(strncmp(inputline,"timr",4)==0) settings.timr = value;
		if(strncmp(inputline,"xadr",4)==0) settings.xadr = value;
		if(strncmp(inputline,"radr",4)==0) settings.radr = value;
		if(strncmp(inputline,"ramr",4)==0) settings.ramr = value;
		if(strncmp(inputline,"rlcr",4)==0) settings.rlcr = value;
		if(strncmp(inputline,"xnxfr",5)==0) settings.xnxfr = value;
		if(strncmp(inputline,"tcr",3)==0) settings.tcr = value;
		if(strncmp(inputline,"ticr",4)==0) settings.ticr = value;

		if(strncmp(inputline,"syncr",5)==0) settings.syncr = value;
		if(strncmp(inputline,"imr",3)==0) settings.imr = value;
		if(strncmp(inputline,"isr",3)==0) settings.isr = value;
		
		memset(inputline,0,100);
	}

	DeviceIoControl(hDevice,
                        IOCTL_SFCDRV_SCC_REGISTERS_SET,         /* Device IOCONTROL */
			&settings,							/* write data */
			sizeof(settings)				,	/* write size */
			NULL,								/* read data */
			0,									/* read size */
			&ret,								/* Returned Size */
			NULL);								/* overlap */

	printf("Finished Settings.\n");	

	CloseHandle(hDevice);

	return 0;
}
/* $Id$ */