/* $Id$ */
/*
Copyright(C) 2002 Commtech, Inc.
getbufs.c -- user mode function to get descriptor parameters

usage:
 getbufs port

 The port can be any valid sfc port (0,1,2,3).
 
*/


#include <windows.h>
#include <stdio.h>
#include <math.h> /* floor, pow */

#include "..\sfc.h"



int main(int argc, char * argv[])
{
	//clkset clock;
	char nbuf[80];
	int port,t;
	HANDLE hDevice; 
	unsigned long temp;
	unsigned long passval[4];

        if(argc<2) {
                printf("usage: getbufs port \n");
		exit(1);
	}

	port = atoi(argv[1]);

        sprintf(nbuf,"\\\\.\\SFC%u",port);

	printf("Opening: %s\n",nbuf);

	if((hDevice = CreateFile (
			nbuf, 
			GENERIC_READ | GENERIC_WRITE,
			0,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
			NULL)) == INVALID_HANDLE_VALUE ) 
	{
                printf("Can't get a handle to sfcdrv @ %s\n",nbuf);
		exit(1);
	}

        t = DeviceIoControl(hDevice,
                        IOCTL_SFCDRV_GET_DESC_PARAMETERS,                         /* Device IOCONTROL */
			NULL,								/* write data */
			0,						/* write size */
			&passval,								/* read data */
			4*sizeof(unsigned long ),									/* read size */
			&temp,								/* Returned Size */
			NULL); /* overlap */
		if(!t) printf("failed:%lx\n",GetLastError());
		else
			{
			printf("number of receive  descriptors:%d\n",passval[0]);
			printf("size of receive descriptor    :%d\n",passval[1]);
			printf("number of transmit descriptors:%d\n",passval[2]);
			printf("size of transmit descriptor   :%d\n",passval[3]);
			}
		CloseHandle(hDevice);
	return 0;
}



/* $Id$ */
