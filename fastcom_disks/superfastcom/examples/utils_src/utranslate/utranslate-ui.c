//$Header$
/*
Universal clock translator.  Works for icd2053b, ics307, and fs6131 clock generators.  
Allows for direct clkbits to clkbits translations, desired frequency to clkbits
translations, and clkbits to frequency translations.
*/

#include <stdlib.h>
#include <stdio.h>
#include <windows.h>
#include <math.h> /* floor, pow */
#include <conio.h> //_kbhit() //getch()
#include "utranslate.h"


void GetFS6131BitsUI(double frequency, char *data);
clkset GetICD2053BitsUI(double frequency);
unsigned long GetICS307BitsUI(double frequency);

void main(int argc, char * argv[])
{
	char selection1;	//used to store desired selection
	unsigned char data[10];
	int clock1=0;	//used to set desired clock
	int clock2=0;
	double frequency=0.0;	//use to set the desired frequency
	unsigned long progbits=0;
	unsigned long temp=0;
	clkset clk={0};		//program data for icd2053

	if (argc>1) 
	{
		printf("Invalid usage!\n");
		exit(1);
	}

	memset(data,0,sizeof(data));

	system("cls");
	printf("1 - Translate program bits of one clock to the bits of another clock.\n\n");
	printf("2 - Translate a frequency into program bits for desired clock.\n\n");
	printf("3 - Translate a clock's program bits into the actual frequency.\n\n");
	printf("\tEnter your selection [1-3]: ");

	while (!_kbhit())
	{
	}
	selection1=_getche();

	system("cls");

	switch(selection1)
	{
	case '1':
		printf("\n\nTranslating bits from clock X to bits for clock Y.\n");
		printf("1 = ICD2053  2 = ICS307  3 = FS6131 \n\n");

		printf("Enter the source clock and destination clock numbers:\n");
		printf("Example:  '1 2' will translate from the 2053 to the 307\n");
		printf("          '3 1' will translate from the 6131 to the 2053\n\n");
		scanf("%d %d",&clock1, &clock2);

		if ((clock1 == 1) && (clock2 ==2))
		{
			printf("Translating ICD2053 clock bits to ICS307 clock bits.\n\n");

			printf("Enter the ICD2053 clock bits: 0x");
			scanf("%x",&(clk.clockbits));
			
			printf("\nEnter the number of clock bits: ");
			scanf("%d",&(clk.numbits));
			
			frequency = GetICD2053Freq_decode(clk);
			progbits = GetICS307BitsUI(frequency);
			printf("\nThe ICS307 programming bits are: 0x%X\n",progbits);

		}

		else if ((clock1 == 1) && (clock2 ==3))
		{
			printf("Translating ICD2053 clock bits to FS6131 clock bits.\n");

			printf("Enter the ICD2053 clock bits: 0x");
			scanf("%x",&(clk.clockbits));
			
			printf("\nEnter the number of clock bits: ");
			scanf("%d",&(clk.numbits));
			
			frequency = GetICD2053Freq_decode(clk);

			
			GetFS6131BitsUI(frequency, &data[0]);
		}

		else if ((clock1 == 2) && (clock2 ==1))
		{
			printf("Translating ICS307 clock bits to ICD2053 clock bits.\n");

			printf("Enter the ICS307 clock bits: 0x");
			scanf("%x",&progbits);
			frequency = GetICS307Freq_decode(progbits);
			
			clk = GetICD2053BitsUI(frequency);
		}

		else if ((clock1 == 2) && (clock2 ==3))
		{
			printf("Translating ICS307 clock bits to FS6131 clock bits.\n");

			printf("Enter the ICS307 clock bits: 0x");
			scanf("%x",&progbits);
			frequency = GetICS307Freq_decode(progbits);
			
			GetFS6131BitsUI(frequency, &data[0]);
		}

		else if ((clock1 == 3) && (clock2 ==1))
		{
			printf("Translating FS6131 clock bits to ICD2053 clock bits.\n");
			printf("Enter the 8 program bytes for the FS6131:\n");

			printf("\tByte 0: 0x");
			scanf("%x",&data[1]);
			printf("\tByte 1: 0x");
			scanf("%x",&data[2]);
			printf("\tByte 2: 0x");
			scanf("%x",&data[3]);
			printf("\tByte 3: 0x");
			scanf("%x",&data[4]);
			printf("\tByte 4: 0x");
			scanf("%x",&data[5]);
			printf("\tByte 5: 0x");
			scanf("%x",&data[6]);
			printf("\tByte 6: 0x");
			scanf("%x",&data[7]);
			printf("\tByte 7: 0x");
			scanf("%x",&data[8]);


			frequency = GetFS6131Freq_decode(&data[0]);
			clk = GetICD2053BitsUI(frequency);
			printf("The ICD2053 programming bits are: 0x%X\n",clk.clockbits);
			printf("The number of programming bits is: %d\n\n",clk.numbits);
			printf("The actual frequency of these bits is: %lf\n",clk.actualclock);

		}

		else if ((clock1 == 3) && (clock2 ==2))
		{
			printf("Translating FS6131 clock bits to ICS307 clock bits.\n");
			printf("Enter the 8 program bytes for the FS6131:\n");

			printf("\tByte 0: 0x");
			scanf("%x",&data[1]);
			printf("\tByte 1: 0x");
			scanf("%x",&data[2]);
			printf("\tByte 2: 0x");
			scanf("%x",&data[3]);
			printf("\tByte 3: 0x");
			scanf("%x",&data[4]);
			printf("\tByte 4: 0x");
			scanf("%x",&data[5]);
			printf("\tByte 5: 0x");
			scanf("%x",&data[6]);
			printf("\tByte 6: 0x");
			scanf("%x",&data[7]);
			printf("\tByte 7: 0x");
			scanf("%x",&data[8]);


			frequency = GetFS6131Freq_decode(&data[0]);
			progbits = GetICS307BitsUI(frequency);
			printf("\nThe ICS307 programming bits are: 0x%X\n",progbits);

		}

		else printf("Error, cannot tranlsate that combination.  Try again.\n");
		break;

	case '2':
		printf("\n\nTranslating frequency to program bits.\n");
		printf("1 = ICD2053  2 = ICS307  3 = FS6131 \n\n");

		printf("Which clock do you want to program?");
		scanf("%d",&clock1);
		printf("\n\nWhat frequency do you want to set in MHz? (i.e. 1.8432, 16.0, etc): ");
		scanf("%lf",&frequency);

		if (clock1 == 1)
		{
			printf("\n\nCalculating program bits for the ICD2053 to %lf MHz...\n",frequency);
			clk = GetICD2053BitsUI(frequency);
			frequency = GetICD2053Freq_decode(clk);
			printf("The closest attainable frequency is: %lf MHz\n\n",clk.actualclock);
			printf("The ICD2053 programming data is:\n\n\tProgram word: 0x%X\n\tNumber of Bits: %d\n",clk.clockbits,clk.numbits);
		}

		else if (clock1 == 2)
		{
			printf("Calculating program bits for the ICS307 to %lf MHz...\n",frequency);
			progbits = GetICS307BitsUI(frequency);
			frequency = GetICS307Freq_decode(progbits);
			printf("The ICS307 programming bits are: 0x%X\n",progbits);
		}

		else if (clock1 == 3)
		{
			printf("Calculating program bits for the FS6131 to %lf MHz...\n\n",frequency);
			
			printf("The FS6131 programming data is:\n");
	
			GetFS6131BitsUI(frequency, &data[0]);

		}

		else printf("Error!\n");

		break;

	case '3':
		printf("\n\nTranslating program bits to equivalent frequency.\n");
		printf("1 = ICD2053  2 = ICS307  3 = FS6131 \n\n");

		printf("Which clock do you want to calculate the frequency for? ");
		scanf("%d",&clock1);

		if (clock1 == 1)
		{
			printf("\n\nCalculating ICD2053 frequency from program bits.\n\n");
			printf("Enter the bitstuffed programming word: 0x");
			scanf("%x",&clk.clockbits);
			printf("\nEnter the number of bits: ");
			scanf("%d",&clk.numbits);
			temp = 0;
			frequency = GetICD2053Freq_decode(clk);
			printf("The calculated frequency is: %lf MHz\n",frequency);
		}

		else if (clock1 == 2)
		{
			printf("\n\nCalculating ICS307 frequency from program bits.\n");
			printf("Enter the program bits in hex digits: 0x");
			scanf("%x",&progbits);
			frequency = GetICS307Freq_decode(progbits);
			printf("The calculated frequency is: %lf MHz\n",frequency);
		}

		else if (clock1 == 3)
		{
			printf("\n\nCalculating FS6131 frequency from program bits.\n");
			printf("Enter the 8 program bytes for the FS6131:\n");

			printf("\tByte 0: 0x");
			scanf("%x",&data[1]);
			printf("\tByte 1: 0x");
			scanf("%x",&data[2]);
			printf("\tByte 2: 0x");
			scanf("%x",&data[3]);
			printf("\tByte 3: 0x");
			scanf("%x",&data[4]);
			printf("\tByte 4: 0x");
			scanf("%x",&data[5]);
			printf("\tByte 5: 0x");
			scanf("%x",&data[6]);
			printf("\tByte 6: 0x");
			scanf("%x",&data[7]);
			printf("\tByte 7: 0x");
			scanf("%x",&data[8]);


			frequency = GetFS6131Freq_decode(&data[0]);

			printf("The calculated frequncy is: %lf Mhz\n",frequency);


		}
		
		else printf("Error!\n");

		break;
	default:
		printf("Oops!\n");
		break;
	}


}

void GetFS6131BitsUI(double frequency, char *data)
{
	int ret;
			printf("FS6131 bit calc\n");
			ret = GetFS6131Bits(frequency, &data[0]);
			if(ret==0)
			{
			printf("\tRegister Map (Pg.12, FS6131-01 datasheet)\n\n");
			printf("\t\tByte 0: 0x%X\n",data[1]&0xff);
			printf("\t\tByte 1: 0x%X\n",data[2]&0xff);
			printf("\t\tByte 2: 0x%X\n",data[3]&0xff);
			printf("\t\tByte 3: 0x%X\n",data[4]&0xff);
			printf("\t\tByte 4: 0x%X\n",data[5]&0xff);
			printf("\t\tByte 5: 0x%X\n",data[6]&0xff);
			printf("\t\tByte 6: 0x%X\n",data[7]&0xff);
			printf("\t\tByte 7: 0x%X\n",data[8]&0xff);
			printf("\tClosest attainable output frequency:%lf MHz\n",GetFS6131Freq(data));
			printf("---------------------------------------------\r\n");
			}
			else if(ret==-1)
			{
			printf("desired frequency must be greater than 200000\r\n");
			}
			else if(ret==-2)
			{
			printf("warning: maximum input clock of 82532/20534 is 33MHz\r\n");
			}
			else if(ret==-3)
			{
			printf("No Solution Found\r\n");
			}
			else if(ret==-4)
			{
			printf("?? bad return\r\n");
			}
}

clkset GetICD2053BitsUI(double frequency)
{
	int ret;
	clkset clk;

			ret = GetICD2053Bits(frequency,&clk);
			if(ret==0)
			{
			printf("---------------------------------------------\r\n");
			printf("The ICD2053 programming bits are: 0x%X\n",clk.clockbits);
			printf("The number of programming bits is: %d\n\n",clk.numbits);
			printf("The actual frequency of these bits is: %lf\n",clk.actualclock);
			return clk;
			}
			else if(ret==-1)
			{
			printf("\r\nError in ICD calculation\r\n");
			clk.clockbits = 0;
			clk.numbits=0;
			clk.actualclock=0.0;
			return clk;
			}
			else if(ret==-2)
			{
			printf("ERROR!  Frequency out of range 0.390625MHz <= Fout <= 100.0MHz\n");
			clk.clockbits = 0;
			clk.numbits=0;
			clk.actualclock=0.0;
			return clk;
			}

clk.clockbits = 0;
clk.numbits=0;
clk.actualclock=0.0;
return clk;

}
unsigned long GetICS307BitsUI(double frequency)
{
int ret;
unsigned long bits;

ret = GetICS307Bits(frequency,&bits);
if(ret==0)
{
	printf("Closest attainable output frequency: %lf MHz\n",GetICS307Freq(bits));
	printf("---------------------------------------------\r\n");
	return bits;
}
else if(ret==-1)
{
	printf("The rate %d too low.  Aborting.\n",frequency);
	bits = 0;
	return bits;
}
else if(ret==-2)
{
	printf("The rate %d is too high.  Aborting.\n",frequency);
	bits = 0;
	return bits;

}
else if(ret==-3)
{
	printf("?? bad decode\r\n");
	bits = 0;
	return bits;

}
bits = 0;
return bits;

}