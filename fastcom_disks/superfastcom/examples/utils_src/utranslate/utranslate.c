//$Header$
/*
Universal clock translator.  Works for icd2053b, ics307, and fs6131 clock generators.  
Allows for direct clkbits to clkbits translations, desired frequency to clkbits
translations, and clkbits to frequency translations.
*/

#include <stdlib.h>
#include <stdio.h>
#include <windows.h>
#include <math.h> /* floor, pow */
#include <conio.h> //_kbhit() //getch()
#include "utranslate.h"



// Function name	: GetICS307Freq
// Description	    : Function takes ICS307's program bits as an input and translates
//					  them into the actual frequency that they set.
// Return type		: double 
// Argument         : ULONG bits
double GetICS307Freq(ULONG bits)
{
	ULONG rdw=0;	//Reference Divider Word R6:R0
	ULONG vdw=0;	//VCO Divider Word V8:V0
	ULONG od=0;		//Output Divider select S2:S0
	ULONG f=0;		//Function of CLK2 Output
	ULONG ttl=0;	//Duty Cycle Setting
	ULONG cap=0;	//Internal Load Capacitance for Crystal
	double freq;

	rdw = (bits & 0x0000007f);
//	printf("\n\tThe Reference Divider Word (RDW) = 0x%X\n",rdw);

	vdw = (bits & 0x0000ff80);
	vdw >>= 7;
//	printf("\tThe VCO Divider Word (VDW) = 0x%X\n",vdw);

	od = (bits & 0x00070000);
	od >>= 16;

	switch(od)		//Output Divide decoding table pg 4 ICS307 data sheet
	{
	case 0:
		od=10;
//		printf("\tThe Output Divider = %d",od);
		break;
	case 1:
		od=2;
//		printf("\tThe Output Divider = %d",od);
		break;
	case 2:
		od=8;
//		printf("\tThe Output Divider = %d",od);
		break;
	case 3: 
		od=4;
//		printf("\tThe Output Divider = %d",od);
		break;
	case 4:
		od=5;
//		printf("\tThe Output Divider = %d",od);
		break;
	case 5:
		od=7;
//		printf("\tThe Output Divider = %d",od);
		break;
	case 6:
		od=3;
//		printf("\tThe Output Divider = %d",od);
		break;
	case 7:
		od=6;
//		printf("\tThe Output Divider = %d",od);
		break;
	default:
//		printf("\tShouldn't get here.\n");
		return 0.0;
	}

	f = (bits & 0x00180000);
	f >>= 19;
	switch(f)
	{
	case 0:
//		printf("\tThe Function of CLK2 is: REF\n");
		break;
	case 1:
//		printf("\tThe function of CLK2 is: REF/2\n");
		break;
	case 2:
//		printf("\tThe function of CLK2 is: OFF\n");
		break;
	case 3:
//		printf("\tThe function of CLK2 is: CLK1/2\n");
		break;
	}

	ttl = (bits & 0x00200000);
	ttl >>= 21;
//	if (ttl == 0) printf("\tOutput Duty Cycle Configuration = TTL (Vcc=5V)\n");
//	else printf("\tOutput Duty Cycle Configuration = CMOS (Vcc=3.3V)\n");

	cap = (bits & 0x00c00000);
	cap >>= 22;
//	printf("\tCrystal Load Capacitance = 0x%X\n",cap);
//	printf("\t\this value should be 0x0 for all Fastcom boards.\n\t\tRefer to page 4 of ICS307 datasheet for more info.\n\n");


	freq = (((18.432 * 2) / ((rdw + 2)* od)) * (vdw + 8) );
//	printf("Freq = (((18.432 * 2) / ((rdw + 2)* od)) * (vdw + 8) )\n");
//	printf("---------------------------------------------\r\n");	
	return freq;
}




// Function name	: GetICS307Bits
// Description	    : Function takes desired frequency as a unsigned long and returns the best
//					  programming bit stream to achieve that frequency on the ICS307 clock generator.
// Return type		: unsigned long 
// Argument         : ULONG desired
int GetICS307Bits(double desired,unsigned long *bits)
{
	ULONG bestVDW=1;	//Best calculated VCO Divider Word
	ULONG bestRDW=1;	//Best calculated Reference Divider Word
	ULONG bestOD=1;		//Best calculated Output Divider
	ULONG result=0;
	ULONG t=0;
	ULONG i=0;
	ULONG j=0;
	ULONG tempValue=0;
	ULONG temp=0;
	ULONG vdw=1;		//VCO Divider Word
	ULONG rdw=1;		//Reference Divider Word
	ULONG od=1;			//Output Divider
	ULONG lVDW=1;		//Lowest vdw
	ULONG lRDW=1;		//Lowest rdw
	ULONG lOD=1;		//Lowest OD
	ULONG hVDW=1;		//Highest vdw
	ULONG hRDW=1;		//Highest rdw
	ULONG hOD=1;		//Highest OD
	ULONG hi;			//initial range freq Max
	ULONG low;			//initial freq range Min
	ULONG check;		//Calculated clock
	ULONG clk1;			//Actual clock 1 output
	ULONG inFreq=18432000;	//Input clock frequency
	ULONG range1=0;		//Desired frequency range limit per ics307 mfg spec.
	ULONG range2=0;		//Desired frequency range limit per ics307 mfg spec.

	desired = (ULONG)(desired*1000000);

	while ((desired < 6000000 )||(desired>33000000))
	{
		if (desired < 6000000 )
		{
			return -1;
		}
		else
		{
			return -2;
		}
	}
	hi=(ULONG)(desired+(desired/10));
	low=(ULONG)(desired-(desired/10));
//	printf("Hi = %lf  Low = %lf \n",hi, low);
	od = 2;
	while (od <= 10)
	{
		rdw = 1;
		while (rdw <= 127)
		{
			vdw = 4;
			while (vdw <= 511)
			{
				check = (ULONG)(( (((__int64)inFreq * 2)* ((__int64)vdw + 8)) / (((rdw + 2)*od)))  );	//calculate a check frequency
				range1 = (ULONG)(((__int64)inFreq * 2 * ((__int64)vdw + 8)) / ((rdw + 2)*10));
				range2 = (ULONG)((__int64)inFreq / ((rdw + 2)*10));

				if ( ((range1) > 5500000) && ((range1) < 40000000) && ((range2) > 20000) )
				{
					if (check == low)
					{
						printf("rdw:%d,vdw:%d,od:%d,freq:%d\r\n",rdw,vdw,od,check);
						if (lRDW > rdw)
						{
							lVDW=vdw;
							lRDW=rdw;
							lOD=od;
							low=check;
						}
						else if ((lRDW == rdw) && (lVDW < vdw))
						{
							lVDW=vdw;
							lRDW=rdw;
							lOD=od;
							low=check;
						}
					}
					else if (check == hi)
					{
						printf("rdw:%d,vdw:%d,od:%d,freq:%d\r\n",rdw,vdw,od,check);

						if (hRDW > rdw)
						{
							hVDW=vdw;
							hRDW=rdw;
							hOD=od;
							hi=check;
						}
						else if ((hRDW == rdw) && (hVDW < vdw))
						{
							hVDW=vdw;
							hRDW=rdw;
							hOD=od;
							hi=check;
						}
					}
					if ((check > low) && (check < hi))		//if difference is less than previous difference
					{
						printf("rdw:%d,vdw:%d,od:%d,freq:%d\r\n",rdw,vdw,od,check);

						if ((check) > desired)
						{
							hi=check;
							hVDW=vdw;
							hRDW=rdw;
							hOD=od;
						}
						else 
						{
							low=check;
							lVDW = vdw;
							lRDW = rdw;
							lOD = od;
						}
					}
			}
			vdw++;
		}
		rdw++;
	}
	od++;
	if (od==9)
		od++;
	}
	if (((hi) - desired) < (desired - (low)))
	{
		bestVDW=hVDW;
		bestRDW=hRDW;
		bestOD=hOD;
		clk1=hi;
	}
	else
	{
		bestVDW=lVDW;
		bestRDW=lRDW;
		bestOD=lOD;
		clk1=low;
	}
	switch(bestOD)
	{
	case 2:
		result=0x11;
		break;
	case 3:
		result=0x16;
		break;
	case 4:
		result=0x13;
		break;
	case 5:
		result=0x14;
		break;
	case 6:
		result=0x17;
		break;
	case 7:
		result=0x15;
		break;
	case 8:
		result=0x12;
		break;
	case 10:
		result=0x10;
		break;
	default:
		return -3;
		
	}
	range1 = (ULONG)(((__int64)inFreq * 2 * ((__int64)bestVDW + 8)) / (bestRDW + 2));
	range2 = (inFreq/(bestRDW + 2));
	clk1 = (ULONG)(((((__int64)inFreq * 2)* ((__int64)bestVDW + 8)) / ((bestRDW + 2)*bestOD))  );


	result<<=9;
	result|=bestVDW;
	result<<=7;
	result|=bestRDW;
	bits[0] = result;
	return 0;
	
}



// Function name	: GetICD2053Bits
// Description	    : Taken from calculate_bits function.  Calculates the programming word
//					  and number of bits for an ICD2053 clock generator.  Input is an frequency
//					  returns a clkset structure that contains the word, numbits, and actual frequency.
// Return type		: clkset 
// Argument         : DOUBLE desired
int  GetICD2053Bits(DOUBLE desired,clkset *clk)
{
	unsigned long P;
	unsigned long Pprime;
	unsigned long Q;
	unsigned long Qprime;
	unsigned long M;
	unsigned long I;
	unsigned long D;
	double fvco;
	double desired_ratio;
	double actual_ratio;
	unsigned long bestP;
	unsigned long bestQ;
	unsigned long bestM;
	unsigned long bestI;
	double best_actual_ratio;
	unsigned long Progword;
	unsigned long Stuffedword;
	unsigned long bit1;
	unsigned long bit2;
	unsigned long bit3;
	unsigned long i,j;
	unsigned long rangelo;
	unsigned long rangehi;
	double best_diff;
	double reffreq=18.432;

	if ((desired>=0.390625)&&(desired<=100.0))
	{
		D = 0x00000001;//from bitcalc, but the datasheet says to make this 1...
		bestP = 0;
		bestQ = 0;
		best_actual_ratio = 1.0;//hopefully we can do better than this...
		best_diff = 1.0;
		rangelo = (unsigned long)floor(reffreq/1.0) +1;
		rangehi = (unsigned long)floor(reffreq/0.200000);
		if(rangelo <3) rangelo = 3;
		if(rangehi>129) rangehi = 129;
		for(i=0;i<=7;i++)
		{
			M = i;
			fvco = desired * (pow(2,i));
			if(fvco<80.0) I = 0x00000000;
			if(fvco>=80.0) I = 0x00000008;
			if((fvco>50.0)&&(fvco<150.0))
			{
				desired_ratio = fvco/(2.0 * reffreq);
				for(P=4;P<=130;P++)for(Q=rangelo;Q<=rangehi;Q++)
				{
					actual_ratio = (double)P/(double)Q;
					if(actual_ratio==desired_ratio) 
					{
						bestP = P;
						bestQ = Q;
						bestM = M;
						bestI = I;
						best_actual_ratio = actual_ratio;
						goto donecalc;
					}
					else 
					{
						if(fabs(desired_ratio - actual_ratio)<(best_diff)) 
						{
							best_diff = fabs(desired_ratio - actual_ratio);
							best_actual_ratio = actual_ratio;
							bestP = P;
							bestQ = Q;
							bestM = M;
							bestI = I;
						}
					}	
				}
			}
		}
		donecalc:
		if((bestP!=0)&&(bestQ!=0))
		{
			//here bestP BestQ are good to go.
			I = bestI;
			M = bestM;
			P = bestP;
			Q = bestQ;
			Pprime = bestP - 3;
			Qprime = bestQ - 2;
			Progword = 0;
			Progword =  (Pprime<<15) | (D<<14) | (M<<11) | (Qprime<<4) | I;
			bit1 = 0;
			bit2 = 0;
			bit3 = 0;
			Stuffedword = 0;
			i = 0;
			j = 0;
			bit1 = ((Progword>>i)&1);
			Stuffedword |=  (bit1<<j);
			i++;
			j++;
			bit2 = ((Progword>>i)&1);
			Stuffedword |=  (bit2<<j);
			i++;
			j++;
			bit3 = ((Progword>>i)&1);
			Stuffedword |=  (bit3<<j);
			j++;
			i++;
			while(i<=22)
			{
				if((bit1==1)&&(bit2==1)&&(bit3==1))
				{
					//force a 0 in the stuffed word;
					j++;
					bit3 = 0;
				}
				bit1 = bit2;
				bit2 = bit3;
				bit3 = ((Progword>>i)&1);
				Stuffedword |=  (bit3<<j);
				i++;
				j++;
			}
			clk->clockbits = Stuffedword;
			clk->numbits = (USHORT)j-1;
			clk->actualclock = (((2.0 * reffreq) * ((double)P/(double)Q)) / pow(2,M))*1000000.0;
			return 0;
		}
		else
		{
		return -1;
		}
	}
	else 
	{
		return -2;
	}
return -3;	
}


// Function name	: GetICD2053Freq
// Description	    : Decodes ICD2053 clock bits into the actual frequency attained
// Return type		: double 
// Argument         : clkset clock
double GetICD2053Freq(clkset clock)
{
	double fvco=0.0;
	double fout=0.0;
	double fref=18.432;
	unsigned long p=0;
	unsigned long q=0;
//	double p=0.0;
//	double q=0.0;
	unsigned long pprime=0;	//p counter value
	unsigned long qprime=0;	//q counter value
	unsigned long d=0;	//Duty cycle adjust up
	unsigned long m=0;	//Mux
	unsigned long divisor=0;	//divisor
	unsigned long i=0;
	unsigned long stuffed=0;
	unsigned long unstuffed=0;
	unsigned long temp=0;
	unsigned long lcv1=0;	//loop control variable 1
	unsigned long numbits = 0;

	if (clock.clockbits==22)
	{
		i = (clock.clockbits & 0x0000000f);
		qprime = (clock.clockbits & 0x000007f0);
		qprime >>= 4;
		m = (clock.clockbits & 0x00003800);
		m >>= 11;
		d = (clock.clockbits & 0x00004000);
		d >>= 14;
		pprime = (clock.clockbits & 0x00ff8000);
		pprime >>= 15;
		q = qprime + 2;
		p = pprime + 3;
	}
	else
	{
		stuffed = clock.clockbits;
		unstuffed = 0;
		while(lcv1<clock.numbits)
		{
			if ((stuffed & 0x00000001)&&(stuffed & 0x00000002)&&(stuffed & 0x00000004))
			{
				temp <<= 3;
				temp |= 0x00000007;
				stuffed >>= 4;
				lcv1 += 4;
				numbits += 3;
			}
			else
			{
				temp <<= 1;
				if (stuffed & 0x00000001) temp |= 0x00000001;
				stuffed >>= 1;
				lcv1++;
				numbits += 1;
			}
		}
		lcv1 = 0;
		while (lcv1<22)
		{
			unstuffed <<= 1;
			if (temp & 0x00000001) unstuffed |= 0x00000001;
			temp >>= 1;
			lcv1++;
		}
		i = (unstuffed & 0x0000000f);
		qprime = (unstuffed & 0x000007f0);
		qprime >>= 4;
		q = qprime + 2;
		m = (unstuffed & 0x00003800);
		m >>= 11;
		d = (unstuffed & 0x00004000);
		d >>= 14;
		pprime = (unstuffed & 0x00ff8000);
		pprime >>= 15;
		p = pprime + 3;

	}
//	printf("\n\n\tThe unstuffed word is 0x%X\n\n",unstuffed);
	fvco = (2*fref*((double)p/(double)q));
//	printf("\tFvco = %lf\n",fvco);
//	printf("\tP = 0x%X   P' = 0x%X \n",p,pprime);
//	printf("\tQ = 0x%X   Q' = 0x%X \n",q,qprime);
//	printf("\tD = 0x%X\n",d);
//	printf("\tM = 0x%X\n",m);
//	printf("\tI = 0x%X\n\n",i);
//	printf("\tFvco = 2*18.432*(p/q)\n");
//	printf("\tFout = Fvco/2^m\n\n");
	fout = (fvco / pow(2,m));
//	printf("---------------------------------------------\r\n");
	return fout;
}
		


// Function name	: GetFS6131Bits
// Description	    : Calculates the programming bytes necessary to set the FS6131 clock
//					  generator to the given frequency.
// Return type		: void 
// Argument         : double Freq
// Argument         : unsigned char *data
int GetFS6131Bits(double Freq, unsigned char *data)
{
	static long int Np1_range[4] = {1,2,4,8};
	static long int Np2_range[4] = {1,3,5,4};
	static long int Np3_range[4] = {1,3,5,4};

	int i,j,k;
	double Fout;
	double Fref=18432000.0;
	double Fvco;
	double ppm=500.0;
	long int Nf;
	long int Nr;
	long int Np1;
	long int Np2;
	long int Np3;

	double Fbest;
	double Fvco_best;
	long int Nf_best;
	long int Nr_best;
	long int Np1_best;
	long int Np2_best;
	long int Np3_best;
	int i_best;
	int j_best;
	int k_best;
	long int vcospd;
	long int solution_found=0;


	Freq = Freq * 1000000.0;
	if(Freq<=200000.0)
	{
	return -1;
	}
	if(Freq>33333333.3)
	{
	return -2;
	}
	Fref = 18432000.0;
	Fbest=Freq-(ppm*Freq/1000000.0)-.01;
	Fvco_best = 0.0;
	vcospd=0;//allways use fast vco

	for(i=0;i<4;i++)
	{
		Np1 = Np1_range[i];
		for(j=0;j<4;j++)
		{
			Np2 = Np2_range[j];
			for(k=0;k<4;k++)
			{
				Np3 = Np3_range[k];
				for(Nr=1;Nr<4095;Nr++)
				{
					for(Nf=57;Nf<16383;Nf++)
					{
						Fvco = Fref*((double)Nf/(double)Nr);
						if(Fvco<40000000.0) break;
						if(Fvco>230000000.0) break;
						Fout = Fvco*(1.0/((double)Np1*(double)Np2*(double)Np3));

						if(Fout<(Freq+(ppm*Freq/1000000.0)))
						if(Fout>(Freq-(ppm*Freq/1000000.0)))
						{
							if(fabs(Fout-Freq)<fabs(Fbest-Freq)) Fvco_best=0.0;
							if(fabs(Fout-Freq)<=fabs(Fbest-Freq))
							{
								if(Fvco>Fvco_best)
								{
									Fbest = Fout;
									Fvco_best = Fvco;
									Nf_best = Nf;
									Nr_best = Nr;
									Np1_best = Np1;
									Np2_best = Np2;
									Np3_best = Np3;
									i_best = i;
									j_best = j;
									k_best = k;
									solution_found=1;
								}
								else if(Fvco==Fvco_best)
								{
									if((Nf+Nr)<(Nf_best+Nr_best))
									{
										Fbest = Fout;
										Fvco_best = Fvco;
										Nf_best = Nf;
										Nr_best = Nr;
										Np1_best = Np1;
										Np2_best = Np2;
										Np3_best = Np3;
										i_best = i;
										j_best = j;
										k_best = k;
										solution_found=1;
									}
								}
							}
						}

					}
				}
			}
		}
	}
	
	if(solution_found==0)
		{
		return -3;
		}
	else
	{
		Freq = Freq/1000000.0;
		Fbest = Fbest/1000000.0;
		Fvco_best=Fvco_best/1000000.0;

//		printf("\tFreq:%f\n",Freq);
//		printf("\tFout:%f\n",Fbest);
//		printf("\tNf:%d\n",Nf_best);
//		printf("\tNr:%d\n",Nr_best);
//		printf("\tNp1:%d\n",Np1_best);
//		printf("\tNp2:%d\n",Np2_best);
//		printf("\tNp3:%d\n",Np3_best);
//		printf("\tFvco:%f\n",Fvco_best);
/*		printf("i_best:%d\n",i_best);
		printf("j_best:%d\n",j_best);
		printf("k_best:%d\n",k_best);
*/
		data[0] = 0;//starting register location

		
		data[1] = Nr_best&0xff;

		data[2] = (Nr_best>>8)&0x0f;
		data[2] = (data[2] | 0x10)&0x1f;//set REFDSRC=1, SHUT=0,PDREF=0,PDFBK=0
		data[3] = ((k_best&0x03)<<4)|((j_best&0x03)<<2)|(i_best&0x03);
		data[4] = (Nf_best&0xff);
		data[5] = (Nf_best>>8)&0x3f;
		data[5] |= 0x80;//set FBKDSRC = 10
		data[6] = 0x22;//OUTMUX==00(VCO output),OSCTYPE=1(FS6031 OSC),VCOSPD=0 (highspeed),LFTC=0 (short rc),EXTLF=0 (internal loop filter),MLCP=10 (8uA)
		data[7] = 0xCE;//XLPDEN=1,XLSWAP=1,XLCP=00,XLROM=111,GBL=0
		data[8] = 0x90;//STAT=10 (stat=main loop phase),XLVTEN=0,CMOS=1,VXCO=0

		return 0;
	}
return -4;
}


// Function name	: GetFS6131Freq
// Description	    : Decode the FS6131 clock bytes into the actual frequency attained.
// Return type		: double 
// Argument         : unsigned char *data
double GetFS6131Freq(unsigned char *data)
{
	double Fout;
	double Fref=18.432;
	double Fvco;
	unsigned long Nf;
	unsigned long Nr;
	unsigned long Np1;
	unsigned long Np2;
	unsigned long Np3;

	Nf = data[5] & 0x3f;
	Nf <<= 8;
	Nf |= data[4];

//	printf("\tNf = %d\n",Nf);

	Nr = data[2] & 0x0f;
	Nr <<= 8;
	Nr |= data[1];

//	printf("\tNr = %d\n",Nr);

	Np1 = data[3] & 0x03;	//i_best

	Np2 = data[3] & 0x0c;	//j_best
	Np2 >>= 2;

	Np3 = data[3] & 0x30;	//k_best
	Np3 >>= 4;

	switch (Np1)
	{
	case 0:
//		printf("\tNp1 = 1\n");
		Np1 = 1;
		break;
	case 1:
//		printf("\tNp1 = 2\n");
		Np1 = 2;
		break;
	case 2:
//		printf("\tNp1 = 4\n");
		Np1 = 4;
		break;
	case 3:
//		printf("\tNp1 = 8\n");
		Np1 = 8;
		break;
	default:
//		printf("Error!");
		return 0.0;
	}

	switch (Np2)
	{
	case 0:
//		printf("\tNp2 = 1\n");
		Np2 = 1;
		break;
	case 1:
//		printf("\tNp2 = 3\n");
		Np2 = 3;
		break;
	case 2:
//		printf("\tNp2 = 5\n");
		Np2 = 5;
		break;
	case 3:
//		printf("\tNp2 = 4\n");
		Np2 = 4;
		break;
	default:
//		printf("Error!");
		return 0.0;
	}

	switch (Np3)
	{
	case 0:
//		printf("\tNp3 = 1\n");
		Np3 = 1;
		break;
	case 1:
//		printf("\tNp3 = 3\n");
		Np3 = 3;
		break;
	case 2:
//		printf("\tNp3 = 5\n");
		Np3 = 5;
		break;
	case 3:
//		printf("\tNp3 = 4\n");
		Np3 = 4;
		break;
	default:
//		printf("Error!");
		return 0.0;
	}

	Fvco = (Fref * ((double)Nf/(double)Nr));
//	printf("Fvco = (18.432 * (Nf / Nr))\n");
//	printf("\tFvco = %lf\n",Fvco);

	Fout = Fvco * (1.0/((double)Np1*(double)Np2*(double)Np3));
//	printf("Fout = Fvco * (1 / (Np1 * Np2 * Np3))\n");

//	printf("\tFout = %lf \n",Fout);
//	printf("---------------------------------------------\r\n");

	return Fout;
}

double GetFS6131Freq_decode(unsigned char *data)
{
	double Fout;
	double Fref=18.432;
	double Fvco;
	unsigned long Nf;
	unsigned long Nr;
	unsigned long Np1;
	unsigned long Np2;
	unsigned long Np3;

	Nf = data[5] & 0x3f;
	Nf <<= 8;
	Nf |= data[4];

	printf("\tNf = %d\n",Nf);

	Nr = data[2] & 0x0f;
	Nr <<= 8;
	Nr |= data[1];

	printf("\tNr = %d\n",Nr);

	Np1 = data[3] & 0x03;	//i_best

	Np2 = data[3] & 0x0c;	//j_best
	Np2 >>= 2;

	Np3 = data[3] & 0x30;	//k_best
	Np3 >>= 4;

	switch (Np1)
	{
	case 0:
		printf("\tNp1 = 1\n");
		Np1 = 1;
		break;
	case 1:
		printf("\tNp1 = 2\n");
		Np1 = 2;
		break;
	case 2:
		printf("\tNp1 = 4\n");
		Np1 = 4;
	case 3:
		printf("\tNp1 = 8\n");
		Np1 = 8;
		break;
	default:
		printf("Error!");
		return 0.0;
	}

	switch (Np2)
	{
	case 0:
		printf("\tNp2 = 1\n");
		Np2 = 1;
		break;
	case 1:
		printf("\tNp2 = 3\n");
		Np2 = 3;
		break;
	case 2:
		printf("\tNp2 = 5\n");
		Np2 = 5;
	case 3:
		printf("\tNp2 = 4\n");
		Np2 = 4;
		break;
	default:
		printf("Error!");
		return 0.0;
	}

	switch (Np3)
	{
	case 0:
		printf("\tNp3 = 1\n");
		Np3 = 1;
		break;
	case 1:
		printf("\tNp3 = 3\n");
		Np3 = 3;
		break;
	case 2:
		printf("\tNp3 = 5\n");
		Np3 = 5;
	case 3:
		printf("\tNp3 = 4\n");
		Np3 = 4;
		break;
	default:
		printf("Error!");
		return 0.0;
	}

	Fvco = (Fref * ((double)Nf/(double)Nr));
	printf("Fvco = (18.432 * (Nf / Nr))\n");
	printf("\tFvco = %lf\n",Fvco);

	Fout = Fvco * (1.0/((double)Np1*(double)Np2*(double)Np3));
	printf("Fout = Fvco * (1 / (Np1 * Np2 * Np3))\n");

	printf("\tFout = %lf \n",Fout);
	printf("---------------------------------------------\r\n");

	return Fout;
}

double GetICD2053Freq_decode(clkset clock)
{
	double fvco=0.0;
	double fout=0.0;
	double fref=18.432;
	unsigned long p=0;
	unsigned long q=0;
//	double p=0.0;
//	double q=0.0;
	unsigned long pprime=0;	//p counter value
	unsigned long qprime=0;	//q counter value
	unsigned long d=0;	//Duty cycle adjust up
	unsigned long m=0;	//Mux
	unsigned long divisor=0;	//divisor
	unsigned long i=0;
	unsigned long stuffed=0;
	unsigned long unstuffed=0;
	unsigned long temp=0;
	unsigned long lcv1=0;	//loop control variable 1
	unsigned long numbits = 0;

	if (clock.clockbits==22)
	{
		i = (clock.clockbits & 0x0000000f);
		qprime = (clock.clockbits & 0x000007f0);
		qprime >>= 4;
		m = (clock.clockbits & 0x00003800);
		m >>= 11;
		d = (clock.clockbits & 0x00004000);
		d >>= 14;
		pprime = (clock.clockbits & 0x00ff8000);
		pprime >>= 15;
		q = qprime + 2;
		p = pprime + 3;

	}
	else
	{
		stuffed = clock.clockbits;
		unstuffed = 0;
		while(lcv1<clock.numbits)
		{
			if ((stuffed & 0x00000001)&&(stuffed & 0x00000002)&&(stuffed & 0x00000004))
			{
				temp <<= 3;
				temp |= 0x00000007;
				stuffed >>= 4;
				lcv1 += 4;
				numbits += 3;
			}
			else
			{
				temp <<= 1;
				if (stuffed & 0x00000001) temp |= 0x00000001;
				stuffed >>= 1;
				lcv1++;
				numbits += 1;
			}
		}
		lcv1 = 0;
		while (lcv1<22)
		{
			unstuffed <<= 1;
			if (temp & 0x00000001) unstuffed |= 0x00000001;
			temp >>= 1;
			lcv1++;
		}
		i = (unstuffed & 0x0000000f);
		qprime = (unstuffed & 0x000007f0);
		qprime >>= 4;
		q = qprime + 2;
		m = (unstuffed & 0x00003800);
		m >>= 11;
		d = (unstuffed & 0x00004000);
		d >>= 14;
		pprime = (unstuffed & 0x00ff8000);
		pprime >>= 15;
		p = pprime + 3;

	}
	printf("\n\n\tThe unstuffed word is 0x%X\n\n",unstuffed);
	fvco = (2*fref*((double)p/(double)q));
	printf("\tFvco = %lf\n",fvco);
	printf("\tP = 0x%X   P' = 0x%X \n",p,pprime);
	printf("\tQ = 0x%X   Q' = 0x%X \n",q,qprime);
	printf("\tD = 0x%X\n",d);
	printf("\tM = 0x%X\n",m);
	printf("\tI = 0x%X\n\n",i);
	printf("\tFvco = 2*18.432*(p/q)\n");
	printf("\tFout = Fvco/2^m\n\n");
	fout = (fvco / pow(2,m));
	printf("---------------------------------------------\r\n");
	return fout;
}

double GetICS307Freq_decode(ULONG bits)
{
	ULONG rdw=0;	//Reference Divider Word R6:R0
	ULONG vdw=0;	//VCO Divider Word V8:V0
	ULONG od=0;		//Output Divider select S2:S0
	ULONG f=0;		//Function of CLK2 Output
	ULONG ttl=0;	//Duty Cycle Setting
	ULONG cap=0;	//Internal Load Capacitance for Crystal
	double freq;

	rdw = (bits & 0x0000007f);
	printf("\n\tThe Reference Divider Word (RDW) = 0x%X\n",rdw);

	vdw = (bits & 0x0000ff80);
	vdw >>= 7;
	printf("\tThe VCO Divider Word (VDW) = 0x%X\n",vdw);

	od = (bits & 0x00070000);
	od >>= 16;

	switch(od)		//Output Divide decoding table pg 4 ICS307 data sheet
	{
	case 0:
		od=10;
		printf("\tThe Output Divider = %d",od);
		break;
	case 1:
		od=2;
		printf("\tThe Output Divider = %d",od);
		break;
	case 2:
		od=8;
		printf("\tThe Output Divider = %d",od);
		break;
	case 3: 
		od=4;
		printf("\tThe Output Divider = %d",od);
		break;
	case 4:
		od=5;
		printf("\tThe Output Divider = %d",od);
		break;
	case 5:
		od=7;
		printf("\tThe Output Divider = %d",od);
		break;
	case 6:
		od=3;
		printf("\tThe Output Divider = %d",od);
		break;
	case 7:
		od=6;
		printf("\tThe Output Divider = %d",od);
		break;
	default:
		printf("\tShouldn't get here.\n");
		return 0.0;
	}

	f = (bits & 0x00180000);
	f >>= 19;
	switch(f)
	{
	case 0:
		printf("\tThe Function of CLK2 is: REF\n");
		break;
	case 1:
		printf("\tThe function of CLK2 is: REF/2\n");
		break;
	case 2:
		printf("\tThe function of CLK2 is: OFF\n");
		break;
	case 3:
		printf("\tThe function of CLK2 is: CLK1/2\n");
		break;
	}

	ttl = (bits & 0x00200000);
	ttl >>= 21;
	if (ttl == 0) printf("\tOutput Duty Cycle Configuration = TTL (Vcc=5V)\n");
	else printf("\tOutput Duty Cycle Configuration = CMOS (Vcc=3.3V)\n");

	cap = (bits & 0x00c00000);
	cap >>= 22;
	printf("\tCrystal Load Capacitance = 0x%X\n",cap);
	printf("\t\this value should be 0x0 for all Fastcom boards.\n\t\tRefer to page 4 of ICS307 datasheet for more info.\n\n");


	freq = (((18.432 * 2) / ((rdw + 2)* od)) * (vdw + 8) );
	printf("Freq = (((18.432 * 2) / ((rdw + 2)* od)) * (vdw + 8) )\n");
	printf("---------------------------------------------\r\n");	
	return freq;
}
