//$Header$
/*
Universal clock translator.  Works for icd2053b, ics307, and fs6131 clock generators.  
Allows for direct clkbits to clkbits translations, desired frequency to clkbits
translations, and clkbits to frequency translations.
*/
#ifndef SFC_H
typedef struct dscc_clkset
{
	unsigned long clockbits;
	unsigned long numbits;
	double actualclock;
} clkset;
#endif

int GetICS307Bits(double desired,unsigned long *bits);
int GetICD2053Bits(double desired,clkset *clk);
int GetFS6131Bits(double Freq, unsigned char *data);

double GetICS307Freq(unsigned long bits);
double GetICD2053Freq(clkset clock);
double GetFS6131Freq(unsigned char *data);

double GetICS307Freq_decode(unsigned long bits);
double GetICD2053Freq_decode(clkset clock);
double GetFS6131Freq_decode(unsigned char *data);
