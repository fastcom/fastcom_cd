/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
setrirq.c -- user mode function to set the interrupt rate for receive descriptors
                     

usage:
 setrirq port value

port is any valid SFC port (0,1,2,3).

value is the frequency of receive descriptor interrupts, as:
  0 = interrupt only once (minimum interrupts generated)
  1 = interrupt on every receive descriptor(maximum interrupts generated)
  2 = interrupt on every other descriptor
  3 = interrupt on every 3rd descriptor
..etc up to the number of descriptors in the chain, passing a value greater than
the # of receive descriptors is equivalent to passing 0.

   

*/


#include <windows.h>
#include <stdio.h>
#include <math.h> /* floor, pow */

#include "..\sfc.h"



int main(int argc, char * argv[])
{
	//clkset clock;
	char nbuf[80];
	int port,t;
	HANDLE hDevice; 
    unsigned long reg,val,temp;

	unsigned long passval[2];

	if(argc<3) {
                printf("usage:\n setrirq port value\n");
		exit(1);
	}

	port = atoi(argv[1]);
    val = atol(argv[2]);


		
		passval[0] = val;

        sprintf(nbuf,"\\\\.\\SFC%u",port);

	printf("Opening: %s\n",nbuf);

	if((hDevice = CreateFile (
			nbuf, 
			GENERIC_READ | GENERIC_WRITE,
			0,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
			NULL)) == INVALID_HANDLE_VALUE ) 
	{
                printf("Can't get a handle to sfcdrv @ %s\n",nbuf);
		exit(1);
	}

        t = DeviceIoControl(hDevice,
                        IOCTL_SFCDRV_SET_RX_IRQ_RATE,                            /* Device IOCONTROL */
			&passval,								/* write data */
			sizeof(unsigned long ),						/* write size */
			NULL,								/* read data */
			0,									/* read size */
			&temp,								/* Returned Size */
			NULL); /* overlap */
        if(t)  printf("set SFC%d receive irq rate to %d\n",port,passval[0]);
        else printf("Problem setting receive irq rate on SFC%d\n",port);/* check owners manual */
	CloseHandle(hDevice);
	return 0;
}
/* $Id$ */