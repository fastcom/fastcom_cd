
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the SFCDLL_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// SFCDLL_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef SFCDLL_EXPORTS
#define SFCDLL_API __declspec(dllexport)
#else
#define SFCDLL_API __declspec(dllimport) 
#endif

#include "..\..\sfc.h"


SFCDLL_API long int __stdcall OpenPort(long int portnumber);
SFCDLL_API long int __stdcall ClosePort(long int portnumber);
SFCDLL_API long int __stdcall ReadFrame(long int portnumber, char *data, long int maxsize, long int timeout);
//SFCDLL_API long int ReadFrameEx(long int portnumber, char *data, long int maxsize, long int timeout, void (__stdcall *functptr)(char *,unsigned long ));
SFCDLL_API long int __stdcall WriteFrame(long int portnumber, char *data, long int size, long int timeout);
SFCDLL_API long int __stdcall Status(long int portnumber,long int *status,long int mask,long int timeout);
SFCDLL_API long int __stdcall ImmediateStatus(long int portnumber,long int status,long int mask);
SFCDLL_API long int __stdcall InitializePort(long int portnumber, SCC_REGS *registers);
SFCDLL_API long int __stdcall ReadRegister(long int portnumber, long int reg);
SFCDLL_API long int __stdcall WriteRegister(long int portnumber, long int reg, long int value);
SFCDLL_API long int __stdcall ReadRegisterLB(long int portnumber, long int reg);
SFCDLL_API long int __stdcall WriteRegisterLB(long int portnumber, long int reg, long int value);
SFCDLL_API long int __stdcall FlushRX(long int portnumber);
SFCDLL_API long int __stdcall FlushTX(long int portnumber);
SFCDLL_API long int __stdcall EnterHunt(long int portnumber);
SFCDLL_API long int __stdcall SetClock(long int portnumber,double frequency,double *actualfrequency);
SFCDLL_API long int __stdcall SetClock2(long int portnumber,double frequency,double *actualfrequency);
SFCDLL_API long int __stdcall GetClock(long int portnumber,double *frequency);
SFCDLL_API long int __stdcall GetClock2(long int portnumber,double *frequency);

