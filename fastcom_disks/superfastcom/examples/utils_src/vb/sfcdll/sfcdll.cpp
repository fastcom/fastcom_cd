// sfcdll.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "sfcdll.h"
#include "stdio.h"
#include "stdlib.h"
#include "math.h"

#include "..\..\sfc.h"

BOOL calculate_bits(double reffreq,double desired_freq,double *m_actual_clock,clkset *clk);
HANDLE openports[9] ={INVALID_HANDLE_VALUE,INVALID_HANDLE_VALUE,INVALID_HANDLE_VALUE,INVALID_HANDLE_VALUE,INVALID_HANDLE_VALUE,INVALID_HANDLE_VALUE,INVALID_HANDLE_VALUE,INVALID_HANDLE_VALUE,INVALID_HANDLE_VALUE};

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
//			MessageBox(NULL,"PROC ATTACH","sfcdll.dll",MB_OK);
			break;
		case DLL_THREAD_ATTACH:
//			MessageBox(NULL,"THREAD ATTACH","sfcdll.dll",MB_OK);
			break;
		case DLL_THREAD_DETACH:
//			MessageBox(NULL,"THREAD DETACH","sfcdll.dll",MB_OK);
			break;
		case DLL_PROCESS_DETACH:
//			MessageBox(NULL,"PROC DETACH","sfcdll.dll",MB_OK);
			break;
    }
    return TRUE;
}


SFCDLL_API long int __stdcall OpenPort(long int portnumber)
{
char devname[64];

if(openports[portnumber]==INVALID_HANDLE_VALUE)
{
	sprintf(devname,"\\\\.\\SFC%d",portnumber);
	openports[portnumber] = CreateFile(devname,GENERIC_WRITE | GENERIC_READ , FILE_SHARE_READ | FILE_SHARE_WRITE,NULL,OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED, NULL);
	
	if(openports[portnumber] == INVALID_HANDLE_VALUE)
		{
		return -1;
		}
	else return 0;
}
else return 0;//allready open?

}
SFCDLL_API long int __stdcall ClosePort(long int portnumber)
{
if(openports[portnumber]!=INVALID_HANDLE_VALUE)
	{
	CloseHandle(openports[portnumber]);
	openports[portnumber]=INVALID_HANDLE_VALUE;
	return 0;
	}
return -1;
}
SFCDLL_API long int __stdcall  ReadFrame(long int portnumber, char *data, long int maxsize, long int timeout)
{
OVERLAPPED rq;
ULONG t;
ULONG j;
ULONG nobytesread;
HANDLE rdevice;
ULONG temp;

rdevice = openports[portnumber];
if(rdevice==INVALID_HANDLE_VALUE) return -1;
	memset( &rq, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
	rq.hEvent = CreateEvent( NULL,    // no security
			    TRUE,    // explicit reset req
			    FALSE,   // initial event reset
			    NULL ) ; // no name
	if (rq.hEvent == NULL)
	{
	return -2;
	}
		t = ReadFile(rdevice,&data[0],maxsize,&nobytesread,&rq);
		if(t==FALSE)  
		{
			j=GetLastError();
			if(j==ERROR_IO_PENDING)
			{
				do
				{
					j = WaitForSingleObject( rq.hEvent, timeout );
					if(j==WAIT_TIMEOUT)
					{
					//cancel read and leave	
					DeviceIoControl(rdevice,IOCTL_SFCDRV_CANCEL_RX,NULL,0,NULL,0,&temp,NULL);
					CloseHandle(rq.hEvent);
					return -3;
					}
					if(j==WAIT_ABANDONED)
					{
					//return error
					CloseHandle(rq.hEvent);
					return -4;
					}
				} while(j!=WAIT_OBJECT_0);
				GetOverlappedResult(rdevice,&rq,&nobytesread,TRUE);
			}
			else 
				{
				CloseHandle(rq.hEvent);
				return -5;
				}
		}
		CloseHandle(rq.hEvent);
		return nobytesread;

}


SFCDLL_API long int __stdcall WriteFrame(long int portnumber, char *data, long int size, long int timeout)
{
OVERLAPPED wq;
ULONG nobyteswritten;
HANDLE wdevice;
ULONG t;
ULONG j;
ULONG temp;
//char buf[256];
wdevice = openports[portnumber];
if(wdevice==INVALID_HANDLE_VALUE) return -1;


	memset( &wq, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
	wq.hEvent = CreateEvent( NULL,    // no security
			    TRUE,    // explicit reset req
			    FALSE,   // initial event reset
			    NULL ) ; // no name
	if (wq.hEvent == NULL)
	{
	return -2;
	}

	t = WriteFile(wdevice,&data[0],size,&nobyteswritten,&wq);
		if(t==FALSE)  
		{
		j=GetLastError();
		if(j==ERROR_IO_PENDING)
			{
			do
				{
					j = WaitForSingleObject( wq.hEvent, timeout );
					if(j==WAIT_TIMEOUT)
					{
					//cancel and return
					DeviceIoControl(wdevice,IOCTL_SFCDRV_CANCEL_TX,NULL,0,NULL,0,&temp,NULL);
					CloseHandle(wq.hEvent);
					return -3;
					}
					if(j==WAIT_ABANDONED)
					{
					CloseHandle(wq.hEvent);
					return -4;
					}
				} while(j!=WAIT_OBJECT_0);
				GetOverlappedResult(wdevice,&wq,&nobyteswritten,TRUE);
			}
			else 
				{
				CloseHandle(wq.hEvent);
//				sprintf(buf,"%8.8x",t);
//				MessageBox(NULL,buf,"ERROR",MB_OK);
				return -5;
				}
		}
		CloseHandle(wq.hEvent);
		return nobyteswritten;


}
SFCDLL_API long int __stdcall  Status(long int portnumber,ULONG *status,ULONG mask,long int timeout)
{
OVERLAPPED st;
HANDLE stdevice;
ULONG t;
ULONG j;
ULONG temp;

stdevice = openports[portnumber];
if(stdevice==INVALID_HANDLE_VALUE) return -1;

	memset( &st, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
	st.hEvent = CreateEvent( NULL,    // no security
			    TRUE,    // explicit reset req
			    FALSE,   // initial event reset
			    NULL ) ; // no name
	if (st.hEvent == NULL)
	{
	return -2;
	}

t=	DeviceIoControl(stdevice,IOCTL_SFCDRV_STATUS,&mask,sizeof(ULONG),&status,sizeof(ULONG),&temp,&st);
		if(t==FALSE)  
		{
			j=GetLastError();
			if(j==ERROR_IO_PENDING)
			{
				do
				{
					j = WaitForSingleObject( st.hEvent, timeout );
					if(j==WAIT_TIMEOUT)
					{
					//cancel and return
					DeviceIoControl(stdevice,IOCTL_SFCDRV_CANCEL_STATUS,NULL,0,NULL,0,&temp,NULL);
					CloseHandle(st.hEvent);
					return -3;
					}
					if(j==WAIT_ABANDONED)
					{
					CloseHandle(st.hEvent);
					return -4;
					}
				} while(j!=WAIT_OBJECT_0);
				GetOverlappedResult(stdevice,&st,&temp,TRUE);

			}
			else
			{
				CloseHandle(st.hEvent);
				return -5;
			}
		}
		
	CloseHandle(st.hEvent);
	return 0;

}
SFCDLL_API long int __stdcall ImmediateStatus(long int portnumber,ULONG *status,ULONG mask)
{
HANDLE stdevice;
ULONG temp;

stdevice = openports[portnumber];
if(stdevice==INVALID_HANDLE_VALUE) return -1;

DeviceIoControl(stdevice,IOCTL_SFCDRV_IMMEDIATE_STATUS,&mask,sizeof(ULONG),status,sizeof(ULONG),&temp,NULL);
return 0;

}
SFCDLL_API long int __stdcall InitializePort(long int portnumber, SCC_REGS *registers)
{
HANDLE hdevice;
ULONG temp;

hdevice = openports[portnumber];
if(hdevice==INVALID_HANDLE_VALUE) return -1;

if(DeviceIoControl(hdevice,IOCTL_SFCDRV_SCC_REGISTERS_SET,registers,sizeof(SCC_REGS),NULL,0,&temp,NULL))
{
return 0;
}
else return -2;
}
SFCDLL_API ULONG __stdcall ReadRegister(long int portnumber, ULONG reg)
{
HANDLE hdevice;
ULONG temp;
ULONG val;

hdevice = openports[portnumber];
if(hdevice==INVALID_HANDLE_VALUE) return -1;

DeviceIoControl(hdevice,IOCTL_SFCDRV_READ_REGISTER,&reg,sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);
return val;
}
SFCDLL_API long int __stdcall  WriteRegister(long int portnumber, ULONG reg, ULONG value)
{
HANDLE hdevice;
ULONG temp;
ULONG val;
ULONG passval[2];

hdevice = openports[portnumber];
if(hdevice==INVALID_HANDLE_VALUE) return -1;
passval[0] = reg;
passval[1] = value;
DeviceIoControl(hdevice,IOCTL_SFCDRV_WRITE_REGISTER,&passval[0],2*sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);
return 0;
}
SFCDLL_API ULONG __stdcall ReadRegisterLB(long int portnumber, ULONG reg)
{
HANDLE hdevice;
ULONG temp;
ULONG val;

hdevice = openports[portnumber];
if(hdevice==INVALID_HANDLE_VALUE) return -1;

DeviceIoControl(hdevice,IOCTL_SFCDRV_READ_LB,&reg,sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);
return val;
}
SFCDLL_API long int __stdcall WriteRegisterLB(long int portnumber, ULONG reg, ULONG value)
{
HANDLE hdevice;
ULONG temp;
ULONG val;
ULONG passval[2];

hdevice = openports[portnumber];
if(hdevice==INVALID_HANDLE_VALUE) return -1;
passval[0] = reg;
passval[1] = value;
DeviceIoControl(hdevice,IOCTL_SFCDRV_WRITE_LB,&passval[0],2*sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);
return 0;
}
SFCDLL_API long int __stdcall  FlushRX(long int portnumber)
{
HANDLE hdevice;
ULONG temp;

hdevice = openports[portnumber];
if(hdevice==INVALID_HANDLE_VALUE) return -1;
DeviceIoControl(hdevice,IOCTL_SFCDRV_FLUSH_RX,NULL,0,NULL,0,&temp,NULL);
return 0;
}
SFCDLL_API long int __stdcall FlushTX(long int portnumber)
{
HANDLE hdevice;
ULONG temp;

hdevice = openports[portnumber];
if(hdevice==INVALID_HANDLE_VALUE) return -1;
DeviceIoControl(hdevice,IOCTL_SFCDRV_FLUSH_TX,NULL,0,NULL,0,&temp,NULL);
return 0;
}
SFCDLL_API long int __stdcall  EnterHunt(long int portnumber)
{
HANDLE hdevice;
ULONG temp;

hdevice = openports[portnumber];
if(hdevice==INVALID_HANDLE_VALUE) return -1;
DeviceIoControl(hdevice,IOCTL_SFCDRV_HUNT,NULL,0,NULL,0,&temp,NULL);
return 0;
}
SFCDLL_API long int __stdcall SetClock(long int portnumber,double frequency,double *actualfrequency)
{
HANDLE hdevice;
ULONG temp;
clkset clk;

hdevice = openports[portnumber];
if(hdevice==INVALID_HANDLE_VALUE) return -1;
if(calculate_bits(18432000.0,frequency,actualfrequency,&clk))
{
DeviceIoControl(hdevice,IOCTL_SFCDRV_SET_CLOCK,&clk,sizeof(clkset),NULL,0,&temp,NULL);
actualfrequency[0] = clk.actualclock;
return 0;
}
else
{
return -1;
}
}
SFCDLL_API long int __stdcall SetClock2(long int portnumber,double frequency,double *actualfrequency)
{
HANDLE hdevice;
ULONG temp;
clkset clk;

hdevice = openports[portnumber];
if(hdevice==INVALID_HANDLE_VALUE) return -1;
if(calculate_bits(18432000.0,frequency,actualfrequency,&clk))
{
DeviceIoControl(hdevice,IOCTL_SFCDRV_SET_CLOCK2,&clk,sizeof(clkset),NULL,0,&temp,NULL);
actualfrequency[0] = clk.actualclock;
return 0;
}
else
{
return -1;
}

}
SFCDLL_API long int __stdcall GetClock(long int portnumber,double *frequency)
{
HANDLE hdevice;
ULONG temp;
clkset clk;

hdevice = openports[portnumber];
if(hdevice==INVALID_HANDLE_VALUE) return -1;
DeviceIoControl(hdevice,IOCTL_SFCDRV_GET_CLOCK,NULL,0,&clk,sizeof(clkset),&temp,NULL);
frequency[0] = clk.actualclock;
return 0;
}
SFCDLL_API long int __stdcall GetClock2(long int portnumber,double *frequency)
{
HANDLE hdevice;
ULONG temp;
clkset clk;

hdevice = openports[portnumber];
if(hdevice==INVALID_HANDLE_VALUE) return -1;
DeviceIoControl(hdevice,IOCTL_SFCDRV_GET_CLOCK2,NULL,0,&clk,sizeof(clkset),&temp,NULL);
frequency[0] = clk.actualclock;
return 0;

}


//function to calcluate input clock params.
BOOL calculate_bits(double reffreq,double desired_freq,double *m_actual_clock,clkset *clk)
{
DWORD P;
DWORD Pprime;
DWORD Q;
DWORD Qprime;
DWORD M;
DWORD I;
DWORD D;
double fvco;
double desired_ratio;
double actual_ratio;
DWORD bestP;
DWORD bestQ;
DWORD bestM;
DWORD bestI;
double best_actual_ratio;
DWORD Progword;
DWORD Stuffedword;
DWORD bit1;
DWORD bit2;
DWORD bit3;
DWORD i,j;
//char buf[256];
unsigned long rangelo;
unsigned long rangehi;
double best_diff;

D = 0x00000000;//from bitcalc, but the datasheet says to make this 1...
bestP = 0;
bestQ = 0;
best_actual_ratio = 1000000.0;//hopefully we can do better than this...
best_diff = 1000000.0;
rangelo = (unsigned long)floor(reffreq/1000000.0) +1;
rangehi = (unsigned long)floor(reffreq/200000.0);
if(rangelo <3) rangelo = 3;
if(rangehi>129) rangehi = 129;
//sprintf(buf,"%u(%f), %u(%f)",rangelo,(reffreq/1000000.0) +1.0,rangehi,(reffreq/200000.0));
//MessageBox(buf,"rl(fl),rh(fh)",MB_OK);
for(i=0;i<=7;i++)
{
M = i;
fvco = desired_freq * (pow(2,i));
//sprintf(buf,"%f",fvco);
//MessageBox(NULL,buf,"Fvco",MB_OK);

if(fvco<80000000.0) I = 0x00000000;
if(fvco>=80000000.0) I = 0x00000008;
if((fvco>50000000.0)&&(fvco<150000000.0))
{
//sprintf(buf,"%f",fvco);
//MessageBox(NULL,buf,"Fvco",MB_OK);
desired_ratio = fvco/(2.0 * reffreq);
//sprintf(buf,"%f",desired_ratio);
//MessageBox(NULL,buf,"desired",MB_OK);

for(P=4;P<=130;P++)for(Q=rangelo;Q<=rangehi;Q++)
{

actual_ratio = (double)P/(double)Q;
if(actual_ratio==desired_ratio) 
	{
	//sprintf(buf,"%u,%u",P,Q);
	//MessageBox(NULL,buf,"Direct Hit",MB_OK);
	bestP = P;
	bestQ = Q;
	bestM = M;
	bestI = I;
	best_actual_ratio = actual_ratio;
	goto donecalc;
	}
else 
	{
	if(fabs(desired_ratio - actual_ratio)<(best_diff)) 
		{
		best_diff = fabs(desired_ratio - actual_ratio);
		best_actual_ratio = actual_ratio;
		bestP = P;
		bestQ = Q;
		bestM = M;
		bestI = I;
		//sprintf(buf,"desired:%f,actual:%f, best%f P%u,Q%u,fvco:%f,M:%u",desired_ratio,actual_ratio,best_diff,bestP,bestQ,fvco,M);
		//MessageBox(buf,"ratiocalc",MB_OK);
		}
	}	
}
}
}
donecalc:
if((bestP!=0)&&(bestQ!=0))
	{
	//here bestP BestQ are good to go.
	I = bestI;
	M = bestM;
	P = bestP;
	Q = bestQ;
	Pprime = bestP - 3;
	Qprime = bestQ - 2;
	//sprintf(buf,"P':%u, Q':%u, M:%u, I:%u",Pprime,Qprime,M,I);
	//MessageBox(buf,"P,Q,M,I",MB_OK);
	Progword = 0;
	Progword =  (Pprime<<15) | (D<<14) | (M<<11) | (Qprime<<4) | I;
	//sprintf(buf,"%lx",Progword);
//	MessageBox(buf,"Progword",MB_OK);
	bit1 = 0;
	bit2 = 0;
	bit3 = 0;
	Stuffedword = 0;
	i = 0;
	j = 0;
	bit1 = ((Progword>>i)&1);
	Stuffedword |=  (bit1<<j);
	i++;
	j++;
	bit2 = ((Progword>>i)&1);
	Stuffedword |=  (bit2<<j);
	i++;
	j++;
	bit3 = ((Progword>>i)&1);
	Stuffedword |=  (bit3<<j);
	j++;
	i++;
	while(i<=22)
		{
		if((bit1==1)&&(bit2==1)&&(bit3==1))
			{
			//force a 0 in the stuffed word;
			j++;
			bit3 = 0;
			//sprintf(buf,"i,j : %u,%u",i,j);
//			MessageBox(buf,"Stuffing",MB_OK);
			}
		bit1 = bit2;
		bit2 = bit3;
		bit3 = ((Progword>>i)&1);
		Stuffedword |=  (bit3<<j);
		i++;
		j++;
		}
	//sprintf(buf,"SW:%lx ,numbits:%u",Stuffedword,j);

clk->clockbits = Stuffedword;
clk->numbits = (USHORT)j-1;
clk->actualclock = ((2.0 * reffreq) * ((double)P/(double)Q)) / pow(2,M);
*m_actual_clock = ((2.0 * reffreq) * ((double)P/(double)Q)) / pow(2,M);
//	MessageBox(buf,"stuffedword, numbits",MB_OK);
return TRUE;
	}
else
{
//	printf("\r\nError in ICD calculation\r\n");
	return FALSE;

}
}
