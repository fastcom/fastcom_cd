VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   4620
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6375
   LinkTopic       =   "Form1"
   ScaleHeight     =   4620
   ScaleWidth      =   6375
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Receive 
      Caption         =   "Receive"
      Height          =   372
      Left            =   2760
      TabIndex        =   14
      Top             =   1800
      Width           =   732
   End
   Begin VB.TextBox IncommingText 
      Height          =   972
      Left            =   240
      TabIndex        =   13
      Text            =   "Incomming Message"
      Top             =   1320
      Width           =   2172
   End
   Begin VB.TextBox Register_value 
      Height          =   288
      Left            =   5400
      TabIndex        =   12
      Text            =   "val#"
      Top             =   2640
      Width           =   852
   End
   Begin VB.CommandButton Register_Write 
      Caption         =   "WriteReg"
      Height          =   492
      Left            =   5400
      TabIndex        =   11
      Top             =   3000
      Width           =   972
   End
   Begin VB.TextBox Register_number 
      Height          =   288
      Left            =   4320
      TabIndex        =   10
      Text            =   "reg#"
      Top             =   2640
      Width           =   732
   End
   Begin VB.CommandButton Register_Read 
      Caption         =   "ReadReg"
      Height          =   492
      Left            =   4200
      TabIndex        =   9
      Top             =   3000
      Width           =   972
   End
   Begin VB.CommandButton RX_Flush 
      Caption         =   "Flush RX"
      Height          =   372
      Left            =   3960
      TabIndex        =   8
      Top             =   1800
      Width           =   852
   End
   Begin VB.CommandButton TX_Flush 
      Caption         =   "Flush TX"
      Height          =   372
      Left            =   3960
      TabIndex        =   7
      Top             =   480
      Width           =   852
   End
   Begin VB.CommandButton Init 
      Caption         =   "Init Port"
      Height          =   492
      Left            =   2760
      TabIndex        =   6
      Top             =   3000
      Width           =   852
   End
   Begin VB.TextBox info 
      Height          =   852
      Left            =   120
      TabIndex        =   5
      Text            =   "Text1"
      Top             =   3720
      Width           =   3492
   End
   Begin VB.TextBox Portnumber 
      Height          =   288
      Left            =   240
      TabIndex        =   4
      Text            =   "Port"
      Top             =   2640
      Width           =   972
   End
   Begin VB.CommandButton ClosePortButton 
      Caption         =   "Close Port"
      Height          =   492
      Left            =   1560
      TabIndex        =   3
      Top             =   3000
      Width           =   852
   End
   Begin VB.CommandButton OpenPortButton 
      Caption         =   "Open Port"
      Height          =   492
      Left            =   240
      TabIndex        =   2
      Top             =   3000
      Width           =   852
   End
   Begin VB.CommandButton Send 
      Caption         =   "Send"
      Height          =   372
      Left            =   2760
      TabIndex        =   1
      Top             =   480
      Width           =   732
   End
   Begin VB.TextBox TextToSend 
      Height          =   852
      Left            =   240
      TabIndex        =   0
      Text            =   "Outbound message"
      Top             =   240
      Width           =   2172
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub ClosePortButton_Click()
Dim ret As Integer
Dim port As Integer
port = Val(Portnumber.Text)
ret = ClosePort(port)
If (ret = 0) Then
info.Text = "port " + Str(port) + " closed"
Else
info.Text = "port close FAILED " + Str(ret)
End If

End Sub

Private Sub Init_Click()
Dim ret As Long
Dim port As Long
Dim freq As Double
Dim actualfreq As Double
Dim sfcset As SETUP
sfcset.cmdr = &H1010000
sfcset.ccr0 = &H80000030
sfcset.ccr1 = &H2248000
sfcset.ccr2 = &H8040000
sfcset.accm = 0
sfcset.udac = 0
sfcset.ttsa = 0
sfcset.rtsa = 0
sfcset.pcmmtx = 0
sfcset.pcmmrx = 0
sfcset.brr = 0
sfcset.timr = 0
sfcset.xadr = 0
sfcset.radr = 0
sfcset.ramr = 0
sfcset.rlcr = 0
sfcset.xnxfr = 0
sfcset.tcr = 0
sfcset.ticr = 0
sfcset.syncr = 0
sfcset.imr = &HFFFEFFFF
freq = 1000000#

port = Val(Portnumber.Text)
ret = SetClock(port, freq, actualfreq)
ret = InitializePort(port, sfcset)
If (ret < 0) Then
info.Text = "port init FAILED " + Str(ret)
Else
info.Text = "port Initialized to HDLC @" + Str(actualfreq)
End If




End Sub

Private Sub OpenPortButton_Click()
Dim ret As Integer
Dim port As Integer

port = Val(Portnumber.Text)

ret = OpenPort(port)
If (ret = 0) Then
info.Text = "port " + Str(port) + " opened"
Else
info.Text = "port open FAILED " + Str(ret)

End If


End Sub


Private Sub Register_Read_Click()
Dim ret As Long
Dim port As Long
Dim value As Long

port = Val(Portnumber.Text)
value = Val(Register_number)
ret = ReadRegister(port, value)
info.Text = "register " + Str(value) + " : " + Str(ret)

End Sub

Private Sub Register_Write_Click()
Dim ret As Long
Dim port As Long
Dim value As Long
Dim reg As Long

port = Val(Portnumber.Text)
reg = Val(Register_number)
value = Val(Register_value)
ret = WriteRegister(port, reg, value)
If (ret > 0) Then
info.Text = "register " + Str(reg) + " : " + Str(value)
Else
info.Text = "problem writing register " + Str(value) + " : " + Str(ret)
End If

End Sub


Private Sub RX_Flush_Click()
Dim ret As Long
Dim port As Long
port = Val(Portnumber.Text)
ret = FlushRX(port)
If (ret >= 0) Then
info.Text = "RX FLUSHED"
Else
info.Text = "problem Flushing RX :" + Str(ret)
End If

End Sub


Private Sub Send_Click()
Dim size As Long
Dim data(4096) As Byte
Dim ret As Long
Dim port As Long
Dim timeout As Long
timeout = 1000
port = Val(Portnumber.Text)


size = Len(TextToSend)
For i = 0 To size - 1 Step 1
data(i) = Asc(Mid(TextToSend, i + 1, 1))
Next

ret = WriteFrame(port, data(0), size, timeout)

If (ret > 0) Then
info.Text = "data sent :" + Str(ret)
Else
info.Text = "problem sending :" + Str(ret)
End If

End Sub

Private Sub TX_Flush_Click()
Dim ret As Long
Dim port As Long
port = Val(Portnumber.Text)
ret = FlushTX(port)
If (ret >= 0) Then
info.Text = "TX FLUSHED"
Else
info.Text = "problem Flushing TX :" + Str(ret)
End If

End Sub
Private Sub Receive_Click()
Dim size As Long
Dim maxsize As Long
Dim data(4096) As Byte
Dim ret As Long
Dim port As Long
Dim timeout As Long
timeout = 1000
port = Val(Portnumber.Text)
maxsize = 4096
size = ReadFrame(port, data(0), maxsize, timeout)
If (size <= 0) Then
info.Text = "Receive Failed :" + Str(size)
Else
IncommingText = ""

For i = 0 To (size - 1) Step 1
IncommingText = IncommingText + Chr$(data(i))
Next

End If
End Sub

