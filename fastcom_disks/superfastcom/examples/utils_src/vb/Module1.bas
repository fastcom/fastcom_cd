Attribute VB_Name = "Module1"
Public doread As Long
Public Type CLKSET
clockbits As Long
numbits As Long
actualclock As Double
End Type
Public Type SETUP
cmdr As Long
star As Long
ccr0 As Long
ccr1 As Long
ccr2 As Long
accm As Long
udac As Long
ttsa As Long
rtsa As Long
pcmmtx As Long
pcmmrx As Long
brr As Long
timr As Long
xadr As Long
radr As Long
ramr As Long
rlcr As Long
xnxfr As Long
tcr As Long
ticr As Long
syncr As Long
imr As Long
isr As Long
gap1 As Long
gap2 As Long
gap3 As Long
gap4 As Long
gap5 As Long
gap6 As Long
gap7 As Long
gap8 As Long
gap9 As Long
End Type


Public Declare Function ClosePort Lib "sfcdll.dll" Alias "#1" (ByVal Portnumber As Long) As Long
Public Declare Function EnterHunt Lib "sfcdll.dll" Alias "#2" (ByVal Portnumber As Long) As Long
Public Declare Function FlushRX Lib "sfcdll.dll" Alias "#3" (ByVal Portnumber As Long) As Long
Public Declare Function FlushTX Lib "sfcdll.dll" Alias "#4" (ByVal Portnumber As Long) As Long
Public Declare Function GetClock2 Lib "sfcdll.dll" Alias "#5" (ByVal Portnumber As Long, ByRef frequency As Double) As Long
Public Declare Function GetClock Lib "sfcdll.dll" Alias "#6" (ByVal Portnumber As Long, ByRef frequency As Double) As Long
Public Declare Function Immediatestatus Lib "sfcdll.dll" Alias "#7" (ByVal Portnumber As Long, status As Long, ByVal mask As Long, ByVal timeout As Long) As Long
Public Declare Function InitializePort Lib "sfcdll.dll" Alias "#8" (ByVal Portnumber As Long, ByRef registers As SETUP) As Long
Public Declare Function OpenPort Lib "sfcdll.dll" Alias "#9" (ByVal Portnumber As Long) As Long
Public Declare Function ReadFrame Lib "sfcdll.dll" Alias "#10" (ByVal Portnumber As Long, data As Byte, ByVal maxsize As Long, ByVal timeout As Long) As Long
Public Declare Function ReadRegister Lib "sfcdll.dll" Alias "#11" (ByVal Portnumber As Long, ByVal reg As Long) As Long
Public Declare Function ReadRegisterLB Lib "sfcdll.dll" Alias "#12" (ByVal Portnumber As Long, ByVal reg As Long) As Long
Public Declare Function SetClock2 Lib "sfcdll.dll" Alias "#13" (ByVal Portnumber As Long, ByVal frequency As Double, ByRef actualfrequency As Double) As Long
Public Declare Function SetClock Lib "sfcdll.dll" Alias "#14" (ByVal Portnumber As Long, ByVal frequency As Double, ByRef actualfrequency As Double) As Long
Public Declare Function status Lib "sfcdll.dll" Alias "#15" (ByVal Portnumber As Long, status As Long, ByVal mask As Long, ByVal timeout As Long) As Long
Public Declare Function WriteFrame Lib "sfcdll.dll" Alias "#16" (ByVal Portnumber As Long, data As Byte, ByVal size As Long, ByVal timeout As Long) As Long
Public Declare Function WriteRegister Lib "sfcdll.dll" Alias "#17" (ByVal Portnumber As Long, ByVal reg As Long, ByVal value As Long) As Long
Public Declare Function WriteRegisterLB Lib "sfcdll.dll" Alias "#18" (ByVal Portnumber As Long, ByVal reg As Long, ByVal value As Long) As Long

