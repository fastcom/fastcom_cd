#include "windows.h"
#include "stdio.h"
#include "string.h"
int set_fs6131(int port, char data[10]);
static long int Np1_range[4] = {1,2,4,8};
static long int Np2_range[4] = {1,3,5,4};
static long int Np3_range[4] = {1,3,5,4};

void main(int argc, char *argv[])
{
char data[10];
int port;
int i,j,k;
double Freq;
double Fout;
double Fref;
double Fvco;
double ppm;
long int Nf;
long int Nr;
long int Np1;
long int Np2;
long int Np3;

double Fbest;
double Fvco_best;
long int Nf_best;
long int Nr_best;
long int Np1_best;
long int Np2_best;
long int Np3_best;
int i_best;
int j_best;
int k_best;
long int vcospd;
long int solution_found;
solution_found=0;
ppm=0.0;
if(argc<3)
{
printf("usage: %s port desired_frequency [ppm]\r\n",argv[0]);
exit(1);
}
port = atoi(argv[1]);
Freq = atof(argv[2]);
if(argc>3) ppm  = atof(argv[3]);
if(ppm==0.0) ppm = 50.0;

printf("Freq:%f  ppm:%f\r\n",Freq,ppm);
if(Freq<=200000.0)
{
printf("desired frequency must be greater than 200000\r\n");
exit(1);
}
if(Freq>33333333.3)
{
printf("warning: maximum input clock of 82532/20534 is 33MHz\r\n");
}
Fref = 18432000.0;
Fbest=Freq-(ppm*Freq/1000000.0)-.01;
Fvco_best = 0.0;
vcospd=0;//allways use fast vco

for(i=0;i<4;i++)
	{
	Np1 = Np1_range[i];
	for(j=0;j<4;j++)
		{
		Np2 = Np2_range[j];
		for(k=0;k<4;k++)
			{
			Np3 = Np3_range[k];
			for(Nr=1;Nr<4095;Nr++)
				{
				for(Nf=57;Nf<16383;Nf++)
					{
					Fvco = Fref*((double)Nf/(double)Nr);
					if(Fvco<40000000.0) break;
					if(Fvco>230000000.0) break;
					Fout = Fvco*(1.0/((double)Np1*(double)Np2*(double)Np3));

					if(Fout<(Freq+(ppm*Freq/1000000.0)))
					if(Fout>(Freq-(ppm*Freq/1000000.0)))
						{
						//printf("Fout:%f :%f :%d:%d:%d:%d:%d --%f\r\n",Fout,fabs(Fout-Freq),Nf,Nr,Np1,Np2,Np3,Fvco);

						if(fabs(Fout-Freq)<fabs(Fbest-Freq)) Fvco_best=0.0;
						if(fabs(Fout-Freq)<=fabs(Fbest-Freq))
							{
							if(Fvco>Fvco_best)
								{
								Fbest = Fout;
								Fvco_best = Fvco;
								Nf_best = Nf;
								Nr_best = Nr;
								Np1_best = Np1;
								Np2_best = Np2;
								Np3_best = Np3;
								i_best = i;
								j_best = j;
								k_best = k;
								solution_found=1;
								}
							else if(Fvco==Fvco_best)
								{
								if((Nf+Nr)<(Nf_best+Nr_best))
									{
									Fbest = Fout;
									Fvco_best = Fvco;
									Nf_best = Nf;
									Nr_best = Nr;
									Np1_best = Np1;
									Np2_best = Np2;
									Np3_best = Np3;
									i_best = i;
									j_best = j;
									k_best = k;
									solution_found=1;
									}
								}
							}
						}

					}
				}
			}
		}
	}
printf("---------------------------------------------\r\n");
if(solution_found==0)printf("No Solution Found\r\n");
else
{
printf("Freq:%f\r\n",Freq);
printf("Fout:%f\r\n",Fbest);
printf("Nf:%d\r\n",Nf_best);
printf("Nr:%d\r\n",Nr_best);
printf("Np1:%d\r\n",Np1_best);
printf("Np2:%d\r\n",Np2_best);
printf("Np3:%d\r\n",Np3_best);
printf("Fvco:%f\r\n",Fvco_best);
//printf("vcospd:%d\r\n",vcospd);
memset(data,0,sizeof(data));
data[0] = 0;//starting register location
data[1] = Nr_best&0xff;
data[2] = (Nr_best>>8)&0x0f;
data[2] = (data[2] | 0x10)&0x1f;//set REFDSRC=1, SHUT=0,PDREF=0,PDFBK=0
data[3] = ((k_best&0x03)<<4)|((j_best&0x03)<<2)|(i_best&0x03);
data[4] = (Nf_best&0xff);
data[5] = (Nf_best>>8)&0x3f;
data[5] = data[5] | 0x80;//set FBKDSRC = 10
data[6] = 0x22;//OUTMUX==00(VCO output),OSCTYPE=1(FS6031 OSC),VCOSPD=0 (highspeed),LFTC=0 (short rc),EXTLF=0 (internal loop filter),MLCP=10 (8uA)
data[7] = (char)0xCE;//XLPDEN=1,XLSWAP=1,XLCP=00,XLROM=111,GBL=0
data[8] = (char)0x90;//STAT=10 (stat=main loop phase),XLVTEN=0,CMOS=1,VXCO=0

set_fs6131(port, data);


}
}