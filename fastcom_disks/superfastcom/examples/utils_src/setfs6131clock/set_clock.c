/* Linux Source for the SetClock function of the 
 * Fastcom: Xsa fs6131 Industry Standard Clock
 * generators. */
//hacked to run on w2k -- 5/15/2002
//hacked to run on SuperFastcom:/2-104+
#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define GPDATA_REG	0x404

/* Global File Descriptor */

HANDLE hDevice; 

unsigned long writel(long port, unsigned long val);
unsigned long readl(long port);
void WriteGPDATA(unsigned long val);
unsigned long ReadGPDATA();
void DELAY();


int set_fs6131(int port, char CData[10])
{
	unsigned tempval;
	char curval;
	unsigned mask;
	unsigned i,j;
	char devname[128];

	sprintf(devname,"\\\\.\\SFC%d",port);
	if((hDevice = CreateFile (
			devname, 
			GENERIC_READ | GENERIC_WRITE,
			0,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
			NULL)) == INVALID_HANDLE_VALUE ) 
	{
                printf("Can't get a handle to sfcdrv @ %s\n",devname);
		exit(1);
	}
	printf("Opened the %s Successfully!\n",devname);

/****** SET THE GPDIR (Direction Register) to Output State *******/
	writel(0x400, 0x0000FF00);

/****** SENDING START CONDITION ******/
	curval = 3;
	/* bit 0 = data */
	/* bit 1 = clock; */
	WriteGPDATA(curval);
	DELAY();
	
	/* send start condition: */

	WriteGPDATA(2);/* transiton dat 1->0 with clock =1 */
	DELAY();
	WriteGPDATA(0);/* take clock to 0 */
	DELAY();
	tempval = 0x58;
	mask = 0x0040;   
	printf("Address: 0x%8.8x\r\n",tempval);

	/* send address: */
	for(i=0;i<7;i++)
	{
		if( (tempval&mask)==mask) 
			curval = 1;
		else 
			curval = 0;                               
		/* printf("%u",curval); */
		WriteGPDATA(curval);/* set bit with clock low */
		DELAY();
		WriteGPDATA(curval|2);/* take clock to 1 */
		DELAY();
		WriteGPDATA(curval&1);/* take clock to 0 */
		DELAY();
		mask = mask >> 1;
	}

	/* printf("\r\n");*/
	curval = 0;
	WriteGPDATA(curval);/* indicate write cycle */
	DELAY();
	WriteGPDATA(curval|2);/* take clock to 1 */
	DELAY();
	WriteGPDATA(curval&1);/* take clock to 0 */
	DELAY();

	/* ack clock cycle ,slave will drive 0 on data */
	WriteGPDATA(curval|2);/* take clock to 1 */
	DELAY();
	WriteGPDATA(curval&1);/* take clock to 0 */ 
	DELAY();

	for(j=0;j<9;j++)
	{
		tempval = CData[j]&0xff;
		printf("CData[%u]: 0x%8.8x\n", j, tempval);
		mask = 0x0080;
		/* send register location: */
		for(i=0;i<8;i++)
		{
			if( (tempval&mask) == mask) 
				curval = 1;
			else 
				curval = 0;
			/* printf("%u",curval); */
			WriteGPDATA(curval);/* set bit with clock low */
			DELAY();
			WriteGPDATA(curval|2);/* take clock to 1 */
			DELAY();
			WriteGPDATA(curval&1);/* take clock to 0 */
			DELAY();
			mask = mask >> 1;
		}
		/* printf("\r\n"); */
		curval = 0;
		/* ack clock cycle ,slave will drive 0 on data */
		WriteGPDATA(curval|2);/* take clock to 1 */
		DELAY();
		WriteGPDATA(curval&1);/* take clock to 0 */
		DELAY();
	}

	/* send stop condition: */
	WriteGPDATA(2);		/* set clock high */           
	DELAY();
	WriteGPDATA(3);		/* force 0->1 transition on data with clock high */
	DELAY();
	/*** ?done? ***/
	//WriteGPDATA(0xa0);//set clock as output on TT
	printf("\nDONE\n");
	CloseHandle(hDevice);
	return 0;
}

#define IOCTL_SFCDRV_READ_REGISTER                      0x830020f8 //15
#define IOCTL_SFCDRV_WRITE_REGISTER                     0x830020fc //16

/* Uses the XSA Device I/O Control to Write a value at port */
unsigned long writel(long port, unsigned long val)
{
	unsigned long t[2];
	unsigned long retv;
	unsigned long temp;
	t[0]=port;
	t[1]=val;

	DeviceIoControl(hDevice,IOCTL_SFCDRV_WRITE_REGISTER,&t,2*sizeof(unsigned long ),&retv,sizeof(unsigned long),&temp,NULL);
	return 1;
}

/* Uses the XSA Device I/O Control To Read a value at port */
unsigned long readl(long port)
{
	unsigned long val=port;
	unsigned long rval;
	unsigned long temp;
	DeviceIoControl(hDevice,IOCTL_SFCDRV_READ_REGISTER,&val,sizeof(unsigned long ),&rval,sizeof(unsigned long),&temp,NULL);
	
	return rval;
}

/* Writes a value to the GPDATA */
void WriteGPDATA(unsigned long val)
{
	/* Shift it over 8 bits for the for the proper Clock data Output */
	writel(GPDATA_REG,(val << 8 ));
}

/* Reads the GPDATA */
unsigned long ReadGPDATA()
{
	return readl(GPDATA_REG);
}

void DELAY()
{
Sleep(1);
}



