/* $Id$ */
/*
Copyright(C) 2006, Commtech, Inc.
settimetag.c -- a user mode program to enable/disable timetagging of received frames

 usage:
  settimetag port [0|1|2|3]

  0 = no timetags on received frames
  1 = timetag on RME interrupt (FE)
  2 = timetag on HI interrupt (descriptor complete)
  3 = timetag on SYN detect interrupt (bisync mode only)
  4 = timetag on RFS, currently not operating, do not use

With timetagging turned on the driver will return a timetag for each received frame,

  	The TIME_TAG_FRAMES will do a KeQuerySystemTime() call followed by
	at gettsc() call (returns CPU TSC register).  The systemtime is 
	turned into a TIME_FIELDS struct via a call to RtlTimeToTimeFields() call
	This time value is GMT time. the result is stored in the received frame
	that is then packed as:
	
	SYSTEMTIME timetag;
	__int64 tsc_value;
	char data[];


	The systemtime value seems to only be updated every 15mS or so on my system
	(the DDK docs say that it is updated  "approx every 10mS")
	however if you have many frames received in close proximity to the 
	changeover, then the tsc_value can be associated with the systemtime
	value at the point of systemtime change, further calculations from that tsc count will be
	resonable.  If you don't have many frames being received (per 10~15mS timeframe)
	but you do occasionally receive two or more frames with the same timetag, you can
	take the tsc_count difference between the frames and get a very good
	time resolution between the frames (albeit both/all off from "true" systemtime anywhere from 0->15mS)
	
	If you need Local time use SystemTimeToTzSpecificLocalTime() to convert it.  

	It appears that QueryPerformanceFrequency() can be used to get the scaling factor for the tsc_value.

*/


#include "windows.h"
#include "stdio.h"
#include "../sfc.h"

int main(int argc,char *argv[])
{
ULONG mode;
HANDLE hdevice;
ULONG port;
char devname[25];
ULONG temp;

if(argc<3)
{
printf("usage: %s port [0|1|2|3]\n",argv[0]);
exit(1);
}
port = atoi(argv[1]);


sprintf(devname,"\\\\.\\SFC%d",port);
printf("devicename:%s\n",devname);
hdevice = CreateFile (devname,GENERIC_READ | GENERIC_WRITE,FILE_SHARE_READ | FILE_SHARE_WRITE,NULL,OPEN_ALWAYS,FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,NULL);
    if (hdevice == INVALID_HANDLE_VALUE)
		{
		//for some reason the driver won't load or isn't loaded
        printf ("Can't get a handle to %s\n",devname);
		exit(1);
		}

mode = 0;//default

mode = atol(argv[2]);

if(DeviceIoControl(hdevice,IOCTL_SFCDRV_SET_TIMETAG_CONTROL,&mode,sizeof(unsigned long ),NULL,0,&temp,NULL))
{
if(mode==0) printf("Timetagging disabled on port:%d\n",port);
else if(mode==1) printf("Timetagging on RME enabled on port:%d\n",port);
else if(mode==2) printf("Timetagging on HI enabled on port:%d\n",port);
else if(mode==3) printf("Timetagging on SYN denabled on port:%d\n",port);
else printf("ERROR, timetaging enabled, but incorrect mode selected on port:%d\n");
}
else
{
printf("Settimetag failed! :%8.8x\n",GetLastError());
}
CloseHandle(hdevice);
}


/* $Id$ */