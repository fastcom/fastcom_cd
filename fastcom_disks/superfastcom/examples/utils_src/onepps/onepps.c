/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
onepps.c -- user mode function to twiggle port 1(cable #2)
            RTS once a sec, uses rough Windows timing.
			(ie not very consistant or accurate)

usage:
 onepps port

 The port can be any valid sfc port (0,1,2,3) and it doesn't matter which
 port you select, only you must be able to successfuly open it
 (ie can't be in use by another program/user).


*/


#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <math.h> /* floor, pow */

#include "..\sfc.h"



int main(int argc, char * argv[])
{
	//clkset clock;
	char nbuf[80];
	int port;
	HANDLE hDevice; 
    unsigned long val,temp;

	unsigned long passval[2];

	if(argc<2) {
                printf("usage: %s port\n",argv[0]);
		exit(1);
	}

	port = atoi(argv[1]);

		passval[0] = 0x18C;//ccr1 of port1
		passval[1] = 0x02348000;//force RTS low

        sprintf(nbuf,"\\\\.\\SFC%u",port);

	printf("Opening: %s\n",nbuf);

	if((hDevice = CreateFile (
			nbuf, 
			GENERIC_READ | GENERIC_WRITE,
			0,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
			NULL)) == INVALID_HANDLE_VALUE ) 
	{
                printf("Can't get a handle to sfcdrv @ %s\n",nbuf);
		exit(1);
	}
	do{
		passval[0] = 0x18C;//ccr1 of port1
		passval[1] = 0x02348000;//force RTS low

		DeviceIoControl(hDevice,IOCTL_SFCDRV_WRITE_REGISTER,&passval,2*sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,	NULL);
		Sleep(500);
		passval[0] = 0x18C;//ccr1 of port1
		passval[1] = 0x023c8000;//force RTS high
		DeviceIoControl(hDevice,IOCTL_SFCDRV_WRITE_REGISTER,&passval,2*sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,	NULL);
		Sleep(500);

	}while(!kbhit());
	getch();
		
		CloseHandle(hDevice);
	return 0;
}
/* $Id$ */