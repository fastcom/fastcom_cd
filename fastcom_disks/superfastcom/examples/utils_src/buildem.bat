c:\"my programs"\"Microsoft Visual Studio"\VC98\bin\vcvars32
cd display_binary
nmake ..\console.mak FILE=disp_bin
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd flushrx
nmake ..\console.mak FILE=flushrx
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd flushtx
nmake ..\console.mak FILE=flushtx
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd getbufs
nmake ..\console.mak FILE=getbufs
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd getclock
nmake ..\console.mak FILE=getclock
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd getclock2
nmake ..\console.mak FILE=getclock2
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd loopback
nmake ..\console.mak FILE=loop
copy loop.exe ..\loopback.exe
rd /S temp <..\y
del *.exe
cd ..

cd lsb2msb
nmake ..\console.mak FILE=lsb2msb
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd onepps
nmake ..\console.mak FILE=onepps
copy onepps.exe ..\onepps.exe
rd /s temp <..\y
del *.exe
cd ..

cd read
nmake ..\console.mak FILE=read
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd read_file_hdlc
nmake ..\console.mak FILE=read
copy read.exe ..\read_file_hdlc.exe
rd /S temp <..\y
del *.exe
cd ..

cd read_timetag
nmake ..\console.mak FILE=read_timetag
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd readlb
nmake ..\console.mak FILE=readlb
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd readreg
nmake ..\console.mak FILE=readreg
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd sdlc_frame_gen
nmake ..\console.mak FILE=sdlc_frame_gen
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd sdlcsnoop
nmake ..\console.mak FILE=sdlcsnoop
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd send
nmake ..\console.mak FILE=xsend
copy xsend.exe ..\send.exe
rd /S temp <..\y
del *.exe
cd ..

cd send_file
nmake ..\console.mak FILE=sendfile
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd setbufs
nmake ..\console.mak FILE=bufs
copy bufs.exe ..\setbufs.exe
rd /S temp <..\y
del *.exe
cd ..

cd setchecktimeout
nmake ..\console.mak FILE=setchecktimeout
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd setclock
nmake ..\console.mak FILE=setclock
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd setclock_sfc104et
nmake ..\console.mak FILE=setclock_sfc104et
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd setclock2
nmake ..\console.mak FILE=progclock
copy progclock.exe ..\setclock2.exe
rd /S temp <..\y
del *.exe
cd ..

cd setfreq
nmake ..\console.mak FILE=setfreq
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd setfreq2
nmake ..\console.mak FILE=setfreq2
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd setrfi
nmake ..\console.mak FILE=setrfi
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd setrirq
nmake ..\console.mak FILE=setrirq
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd settfi
nmake ..\console.mak FILE=settfi
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd settirq
nmake ..\console.mak FILE=settirq
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd sfcset
nmake ..\console.mak FILE=sfcset
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd shifter
nmake ..\console.mak FILE=shifter
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd simuln200
nmake ..\console.mak FILE=simuln200
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd st_tt
nmake ..\console.mak FILE=st_tt
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd status
nmake ..\console.mak FILE=status
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd timed_transmit
nmake ..\console.mak FILE=timed_transmit
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd ts_comp
nmake ..\console.mak FILE=ts_comp
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd write_random
nmake ..\console.mak FILE=write_random
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd writelb
nmake ..\console.mak FILE=writelb
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd writereg
nmake ..\console.mak FILE=writereg
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd bisync_pattern
nmake ..\console.mak FILE=bisync_pattern_tx
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd continuous_tx
nmake ..\console.mak FILE=continuous_write
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

cd continuous_tx
nmake ..\console.mak FILE=commandpump
copy *.exe ..
rd /S temp <..\y
del *.exe
cd ..

