!IF "$(FILE)" == ""
!MESSAGE NMAKE /f console.mak FILE=infile
!MESSAGE infile is assumed as .c, output will be infile.exe
!ERROR No File specified
!ENDIF


!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
RSC=rc.exe


OUTDIR=.\.
INTDIR=.\temp
# Begin Custom Macros
OutDir=.\.
# End Custom Macros

ALL : "$(OUTDIR)\$(FILE).exe"


CLEAN :
        -@erase "$(INTDIR)\$(FILE).obj"
	-@erase "$(INTDIR)\vc60.idb"
        -@erase "$(OUTDIR)\$(FILE).exe"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /ML /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\$(FILE).bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib  kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /incremental:no /pdb:"$(OUTDIR)\$(FILE).pdb" /machine:I386 /out:"$(OUTDIR)\$(FILE).exe" 
LINK32_OBJS= \
        "$(INTDIR)\$(FILE).obj"

"$(OUTDIR)\$(FILE).exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<


.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("$(FILE).dep")
!INCLUDE "$(FILE).dep"
!ELSE 
!MESSAGE Warning: cannot find "$(FILE).dep"
!ENDIF 
!ENDIF 


SOURCE=.\$(FILE).c

"$(INTDIR)\$(FILE).obj" : $(SOURCE) "$(INTDIR)"



