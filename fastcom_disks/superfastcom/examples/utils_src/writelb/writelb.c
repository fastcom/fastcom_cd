/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
writelb.c -- user mode function to write a SuperFastcom special register

usage:
 writelb port register_offset value

 The port can be any valid sfc port (0,1,2,3) and it doesn't matter which
 port you select, only you must be able to successfuly open it
 (ie can't be in use by another program/user).

 The register_offset can be any in the range 0 to 3.
 (see the SuperFastcom hardware reference manual for descriptions of these
  registers)

 The value is a DWORD value taken in HEX, but only the lower 8 bits are used

*/


#include <windows.h>
#include <stdio.h>
#include <math.h> /* floor, pow */

#include "..\sfc.h"



int main(int argc, char * argv[])
{
	//clkset clock;
	char nbuf[80];
	int port,t;
	HANDLE hDevice; 
    unsigned long reg,val,temp;

	unsigned long passval[2];

	if(argc<4) {
                printf("usage: writelb port register value\n");
		exit(1);
	}

	port = atoi(argv[1]);
        sscanf(argv[2],"%lx",&reg);
		sscanf(argv[3],"%lx",&val);

		passval[0] = reg;
		passval[1] = val;

        sprintf(nbuf,"\\\\.\\SFC%u",port);

	printf("Opening: %s\n",nbuf);

	if((hDevice = CreateFile (
			nbuf, 
			GENERIC_READ | GENERIC_WRITE,
			0,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
			NULL)) == INVALID_HANDLE_VALUE ) 
	{
                printf("Can't get a handle to sfcdrv @ %s\n",nbuf);
		exit(1);
	}

        t = DeviceIoControl(hDevice,
                        IOCTL_SFCDRV_WRITE_LB,                          /* Device IOCONTROL */
			&passval,								/* write data */
			2*sizeof(unsigned long ),						/* write size */
			&val,								/* read data */
			sizeof(unsigned long),									/* read size */
			&temp,								/* Returned Size */
			NULL); /* overlap */
        //if(temp!=0) 
		printf("wrote %8.8lx -> register:%lx\n",passval[1],passval[0]);
        //else printf("Problem writing register:%x.\n",passval[0]);/* check owners manual */
	CloseHandle(hDevice);
	return 0;
}
/* $Id$ */