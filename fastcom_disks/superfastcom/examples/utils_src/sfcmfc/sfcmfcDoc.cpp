// sfcmfcDoc.cpp : implementation of the CSfcmfcDoc class
//

#include "stdafx.h"
#include "sfcmfc.h"

#include "sfcmfcDoc.h"
#include "properties.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

UINT ReadProc( LPVOID lpData );

/////////////////////////////////////////////////////////////////////////////
// CSfcmfcDoc

IMPLEMENT_DYNCREATE(CSfcmfcDoc, CDocument)

BEGIN_MESSAGE_MAP(CSfcmfcDoc, CDocument)
	//{{AFX_MSG_MAP(CSfcmfcDoc)
	ON_COMMAND(IDM_PROPERTIES, OnProperties)
	ON_COMMAND(IDM_DISCONNECT, OnDisconnect)
	ON_COMMAND(IDM_SEND_DATA, OnSendData)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSfcmfcDoc construction/destruction

CSfcmfcDoc::CSfcmfcDoc()
{
	// TODO: add one-time construction code here
	m_sfcportnumber=0;
	m_port_opened = 0;
	m_cur_tloc = 0;
	m_cur_rloc = 0;
}

CSfcmfcDoc::~CSfcmfcDoc()
{
}

BOOL CSfcmfcDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CSfcmfcDoc serialization

void CSfcmfcDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CSfcmfcDoc diagnostics

#ifdef _DEBUG
void CSfcmfcDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CSfcmfcDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSfcmfcDoc commands

void CSfcmfcDoc::OnProperties() 
{
	// TODO: Add your command handler code here
	CProperties cp;
	char buf[80];

if(m_port_opened==1)
	{
	MessageBox(NULL,"Must Disconnect first","ERROR",MB_OK);
	}
else
{
	sprintf(buf,"%d",m_sfcportnumber);
	cp.m_sfcportnumber = buf;

	if(cp.DoModal()==IDOK)
		{
		m_sfcportnumber = atoi(LPCSTR(cp.m_sfcportnumber));
		sprintf(buf,"sfcport:%d",m_sfcportnumber);
		MessageBox(NULL,buf,"",MB_OK);
		sprintf(buf,"\\\\.\\SFC%d",m_sfcportnumber);
		m_hsfc = CreateFile (buf,
			  GENERIC_READ | GENERIC_WRITE,
			  FILE_SHARE_READ | FILE_SHARE_WRITE,
			  NULL,
			  OPEN_ALWAYS,
			  FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
			  NULL
			  );
		if (m_hsfc == INVALID_HANDLE_VALUE)
			{
			MessageBox(NULL,"Cannot open SFC port","ERROR",MB_OK);
			return;
			}
		else m_port_opened = 1;
	memset( &m_osr, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
	memset( &m_ost, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
	m_osr.hEvent = CreateEvent( NULL,TRUE,FALSE,NULL );
	if (m_osr.hEvent == NULL)
		{
		MessageBox( NULL, "Failed to create read event for thread!", "main Error!",	MB_ICONEXCLAMATION | MB_OK ) ;
		CloseHandle(m_hsfc);
		m_port_opened = 0;
		return;
		}
	m_ost.hEvent = CreateEvent( NULL,TRUE,FALSE,NULL );
	if(m_ost.hEvent == NULL)
		{
		MessageBox( NULL, "Failed to create write event for thread!", "main Error!",	MB_ICONEXCLAMATION | MB_OK ) ;
		CloseHandle(m_hsfc);
		CloseHandle(m_osr.hEvent);
		m_port_opened = 0;
		return;
		}

		SetTitle(buf);		

AfxBeginThread(ReadProc,this,THREAD_PRIORITY_NORMAL,0,0,NULL);

		}	
	else MessageBox(NULL,"Canceled","",MB_OK);
}

}

void CSfcmfcDoc::OnDisconnect() 
{
	// TODO: Add your command handler code here
char buf[80];
		CloseHandle(m_hsfc);
		CloseHandle(m_osr.hEvent);
		CloseHandle(m_ost.hEvent);
		m_port_opened = 0;
		sprintf(buf,"PORT CLOSED");
		SetTitle(buf);		
	
}

void CSfcmfcDoc::OnSendData() 
{
	// TODO: Add your command handler code here

BOOL t;
ULONG nobyteswritten;
ULONG k;

    t = WriteFile(m_hsfc,m_tdata,m_cur_tloc,&nobyteswritten,&m_ost);//send the frame
	if(t==FALSE)                    //if it returned FALSE then the IO request is queued (waiting for previous frame to get done sending)
		{                       //and we must wait until the os.event gets signaled before we try any more sending
		//printf("returned FALSE\n\r");
		if (GetLastError() == ERROR_IO_PENDING)  //IO_PENDING is the indication that the request was queued, if not this than a parameter error occured
			{
		// wait for a second for this transmission to complete
	    do
			{
			k = WaitForSingleObject( m_ost.hEvent, 1000 );//1 second timeout
			if(k==WAIT_TIMEOUT)
				{
				//this will execute every 1 second
				//you could put a counter in here and if the 
				//driver takes an inordinate ammout of time
				//to complete, you could issue a flush TX command
				//and break out of this loop
				}
			if(k==WAIT_ABANDONED)
				{
				}
			
			}while(k!=WAIT_OBJECT_0);
			}
		//here it is done sent..(well started anyway)
		}
//MessageBox("sent it","",MB_OK);

m_cur_tloc =0 ;
UpdateAllViews(NULL,0,NULL);
	
}

UINT ReadProc( LPVOID lpData )
{
DWORD j;			//temp
BOOL t;				//temp
DWORD nobytestoread;	//the number of bytes that can be put in data[] (max)
DWORD nobytesread;		//the number of bytes that the driver put in data[]

CSfcmfcDoc* pDoc = (CSfcmfcDoc*)lpData;


nobytestoread = 4096;
//printf("read thread started\n\r");     //entry message
//MessageBox(NULL,"read thread started","",MB_OK);
do
	{
	//start a read request by calling ReadFile() with the sfcdevice handle
	//if it returns true then we received a frame 
	//if it returns false and ERROR_IO_PENDING then there are no
	//receive frames available so we wait until the overlapped struct
	//gets signaled.

	t = ReadFile(pDoc->m_hsfc,pDoc->m_rdata,nobytestoread,&nobytesread,&pDoc->m_osr);
	
	if(t==TRUE)
		{
//		MessageBox(NULL,"read TRUE","",MB_OK);
		//we have some data here so do something with it ... display it?
		//display our newly received frame
		//printf("received %u bytes:\n\r",nobytesread);
		pDoc->m_rdata[nobytesread-1] = 0;
		pDoc->m_cur_rloc = nobytesread-1;

		pDoc->UpdateAllViews(NULL,0,NULL);
		
		//		::PostMessage(tp->msgwnd,WM_USER_READ_UPDATE,0,0);

		}
	else
		{
		if (GetLastError() == ERROR_IO_PENDING)
			{
			// wait for a receive frame to come in, note it will wait forever
			do
				{
				j = WaitForSingleObject( pDoc->m_osr.hEvent, 1000 );//1 second timeout
				if(j==WAIT_TIMEOUT)
					{
					//this will execute every 1 second
					//you could put a counter in here and if the 
					//driver takes an inordinate ammout of time
					//to complete, you could issue a flush RX command
					//and break out of this loop
					}
				if(j==WAIT_ABANDONED)
					{
					}
				
				}while((j!=WAIT_OBJECT_0)&&(pDoc->m_port_opened==1));//stay here until we get signaled or the main thread exits
			
				if(pDoc->m_port_opened==1)
					{
//					MessageBox(NULL,"read complete","",MB_OK);
					GetOverlappedResult(pDoc->m_hsfc,&pDoc->m_osr,&nobytesread,TRUE); //here to get the actual nobytesread!!!
					//printf("received %u bytes:\n\r",nobytesread);    //display the number of bytes received
					pDoc->m_rdata[nobytesread-1] = 0;
					pDoc->m_cur_rloc = nobytesread-1;

					
					t=TRUE;
					pDoc->UpdateAllViews(NULL,0,NULL);

					//					::PostMessage(tp->msgwnd,WM_USER_READ_UPDATE,0,0);
					}
			}
		}
	}while(pDoc->m_port_opened==1);              //do until we want to terminate

//MessageBox(NULL,"read exiting","",MB_OK);
	//MessageBox("read thread done","",MB_OK);
	//printf("exiting read thread\n\r");      //exit messge
	return(TRUE);                   //outta here
	

}