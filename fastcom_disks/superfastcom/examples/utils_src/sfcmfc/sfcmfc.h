// sfcmfc.h : main header file for the SFCMFC application
//

#if !defined(AFX_SFCMFC_H__6EC3E346_C17A_11D5_908B_0010B540FE77__INCLUDED_)
#define AFX_SFCMFC_H__6EC3E346_C17A_11D5_908B_0010B540FE77__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CSfcmfcApp:
// See sfcmfc.cpp for the implementation of this class
//

class CSfcmfcApp : public CWinApp
{
public:
	CSfcmfcApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSfcmfcApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CSfcmfcApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SFCMFC_H__6EC3E346_C17A_11D5_908B_0010B540FE77__INCLUDED_)
