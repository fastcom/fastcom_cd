// sfcmfcDoc.h : interface of the CSfcmfcDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_SFCMFCDOC_H__6EC3E34E_C17A_11D5_908B_0010B540FE77__INCLUDED_)
#define AFX_SFCMFCDOC_H__6EC3E34E_C17A_11D5_908B_0010B540FE77__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CSfcmfcDoc : public CDocument
{
protected: // create from serialization only
	CSfcmfcDoc();
	DECLARE_DYNCREATE(CSfcmfcDoc)

// Attributes
public:
int m_port_opened;
unsigned long m_sfcportnumber;
HANDLE m_hsfc;
OVERLAPPED m_osr;
OVERLAPPED m_ost;
char m_tdata[4096];
char m_rdata[4096];
unsigned m_cur_tloc;
unsigned m_cur_rloc;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSfcmfcDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSfcmfcDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CSfcmfcDoc)
	afx_msg void OnProperties();
	afx_msg void OnDisconnect();
	afx_msg void OnSendData();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SFCMFCDOC_H__6EC3E34E_C17A_11D5_908B_0010B540FE77__INCLUDED_)
