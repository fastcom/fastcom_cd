// sfcmfcView.cpp : implementation of the CSfcmfcView class
//

#include "stdafx.h"
#include "sfcmfc.h"

#include "sfcmfcDoc.h"
#include "sfcmfcView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSfcmfcView

IMPLEMENT_DYNCREATE(CSfcmfcView, CView)

BEGIN_MESSAGE_MAP(CSfcmfcView, CView)
	//{{AFX_MSG_MAP(CSfcmfcView)
	ON_WM_CHAR()
	ON_COMMAND(IDM_DEBUG_IT, OnDebugIt)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSfcmfcView construction/destruction

CSfcmfcView::CSfcmfcView()
{
	// TODO: add construction code here

}

CSfcmfcView::~CSfcmfcView()
{
}

BOOL CSfcmfcView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CSfcmfcView drawing

void CSfcmfcView::OnDraw(CDC* pDC)
{
	CSfcmfcDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
pDC->SetTextColor(RGB(0,0,0));
pDC->TextOut(0,0,"Transmit:",9);
pDC->TextOut(0,20,pDoc->m_tdata,pDoc->m_cur_tloc);

pDC->SetTextColor(RGB(255,0,0));
pDC->TextOut(0,90,"Received:",9);
pDC->TextOut(0,110,pDoc->m_rdata,pDoc->m_cur_rloc);
	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CSfcmfcView printing

BOOL CSfcmfcView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CSfcmfcView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CSfcmfcView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CSfcmfcView diagnostics

#ifdef _DEBUG
void CSfcmfcView::AssertValid() const
{
	CView::AssertValid();
}

void CSfcmfcView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CSfcmfcDoc* CSfcmfcView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CSfcmfcDoc)));
	return (CSfcmfcDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSfcmfcView message handlers

void CSfcmfcView::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	CSfcmfcDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
USHORT i;
	
for(i=0;i<nRepCnt;i++)
	{
	pDoc->m_tdata[pDoc->m_cur_tloc] = nChar;
	pDoc->m_cur_tloc++;
	if(pDoc->m_cur_tloc>4095) pDoc->m_cur_tloc=0;
	}
Invalidate(TRUE);
	CView::OnChar(nChar, nRepCnt, nFlags);
}


void CSfcmfcView::OnDebugIt() 
{
	// TODO: Add your command handler code here
	CSfcmfcDoc* pDoc = GetDocument();
	char buf[80];
	ASSERT_VALID(pDoc);


sprintf(buf,"%d",pDoc->m_cur_tloc);
MessageBox(pDoc->m_tdata,buf,MB_OK);

sprintf(buf,"%d",pDoc->m_cur_rloc);
MessageBox(pDoc->m_rdata,buf,MB_OK);
}
