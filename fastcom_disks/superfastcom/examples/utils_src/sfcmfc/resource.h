//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by sfcmfc.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_SFCMFCTYPE                  129
#define IDD_PROPERTIES                  130
#define IDC_SFCPORTNUMBER               1000
#define IDM_PROPERTIES                  32771
#define IDM_DISCONNECT                  32772
#define IDM_DEBUG_IT                    32773
#define IDM_SEND_DATA                   32774

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32775
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
