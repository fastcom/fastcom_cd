// sfcmfcView.h : interface of the CSfcmfcView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_SFCMFCVIEW_H__6EC3E350_C17A_11D5_908B_0010B540FE77__INCLUDED_)
#define AFX_SFCMFCVIEW_H__6EC3E350_C17A_11D5_908B_0010B540FE77__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CSfcmfcView : public CView
{
protected: // create from serialization only
	CSfcmfcView();
	DECLARE_DYNCREATE(CSfcmfcView)

// Attributes
public:
	CSfcmfcDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSfcmfcView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSfcmfcView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CSfcmfcView)
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnDebugIt();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in sfcmfcView.cpp
inline CSfcmfcDoc* CSfcmfcView::GetDocument()
   { return (CSfcmfcDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SFCMFCVIEW_H__6EC3E350_C17A_11D5_908B_0010B540FE77__INCLUDED_)
