/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
bufs.c -- user mode function to set buffering parameters for a port

usage:
 bufs #rbufs sizerbufs #tbufs sizetbufs

 The port can be any valid sfc port (0,1,2,3).
 the size parameters must be DWORD aligned (ie divisible by 4) (4->8188) (probably ought to keep it 4->4096)
 the min number of rbufs/tbufs is 3.
 the max number of rbufs/tbufs is system dependant (how much physical system ram do you have???)
*/


#include <windows.h>
#include <stdio.h>
#include <math.h> /* floor, pow */

#include "..\sfc.h"



int main(int argc, char * argv[])
{
	//clkset clock;
	char nbuf[80];
	int port,t;
	HANDLE hDevice; 
	double  desfreq;
	double actualfreq;
	unsigned long temp;
	unsigned long passval[4];

	if(argc<6) {
                printf("usage: bufs port numr sizer numt sizet \n");
		exit(1);
	}

	port = atoi(argv[1]);
	passval[0]= atol(argv[2]);
	passval[1]= atol(argv[3]);
	passval[2]= atol(argv[4]);
	passval[3]= atol(argv[5]);

        sprintf(nbuf,"\\\\.\\SFC%u",port);

	printf("Opening: %s\n",nbuf);

	if((hDevice = CreateFile (
			nbuf, 
			GENERIC_READ | GENERIC_WRITE,
			0,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
			NULL)) == INVALID_HANDLE_VALUE ) 
	{
                printf("Can't get a handle to sfcdrv @ %s\n",nbuf);
		exit(1);
	}

        printf("number of receive  descriptors:%d\n",passval[0]);
		printf("size of receive descriptor    :%d\n",passval[1]);
		printf("number of transmit descriptors:%d\n",passval[2]);
		printf("size of transmit descriptor   :%d\n",passval[3]);
        t = DeviceIoControl(hDevice,
                        IOCTL_SFCDRV_CHNG_DESC_LEN,                         /* Device IOCONTROL */
			&passval,								/* write data */
			4*sizeof(unsigned long ),						/* write size */
			NULL,								/* read data */
			0,									/* read size */
			&temp,								/* Returned Size */
			NULL); /* overlap */
		if(!t) printf("failed:%lx\n",GetLastError());
		CloseHandle(hDevice);
	return 0;
}
/* $Id$ */
