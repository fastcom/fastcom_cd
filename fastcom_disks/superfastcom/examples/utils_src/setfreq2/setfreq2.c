/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
setfreq2.c -- user mode function to set the PROGCLK output frequency 

usage:
 setfreq port frequency type

 The port can be any valid sfc port (0,1,2,3) and it doesn't matter which
 port you select,  There is only 1 OSC input that is used by all 4 channels.

 The frequency can be any in the range 391000 to 40000000

 The type is 0 = icd2053b (superfastcom/superfastcom/232)
             1 = ics307   (superfastcom/2-104-ET rev 2.0)
			 2 = fs6131   (superfastcom/2-104-ET rev 1.0)

*/


#include <windows.h>
#include <stdio.h>

#include "..\sfc.h"



int main(int argc, char * argv[])
{
	//clkset clock;
	char nbuf[80];
	int port;
	HANDLE hDevice; 
	unsigned long temp;
	unsigned long type;
	unsigned long desfreq;
	unsigned long passval[2];
	unsigned long out;
	if(argc!=4) 
	{
        printf("\nusage: %s port frequency type\n",argv[0]);
		printf("The type is \n\t0 = icd2053b\n\t1 = ics307\n\t2 = fs6131\n");
		exit(1);
	}
	
	port = atoi(argv[1]);
	desfreq = atol(argv[2]);
	type    = atoi(argv[3]);
	
    sprintf(nbuf,"\\\\.\\SFC%u",port);
	
	printf("Opening: %s\n",nbuf);
	
	passval[0] = desfreq;
	if( (type==0) && (desfreq>=1000000) && (desfreq<=33000000) )
	{
		passval[1] = 5;//is icd2053b
	}
	else
	{
		if(type==0)
		{
		printf("Out of range 1MHz <= Frequency <= 33MHz\n");
		exit(1);
		}
	}

	if( (type==1)  && (desfreq>=6000000) && (desfreq<=33000000) )
	{
		passval[1] = 3;//is ics307
	}
	else
	{
		if(type==1)
		{
		printf("Out of range 6MHz <= Frequency <= 33MHz\n");
		exit(1);
		}
	}

	if( (type==2)  && (desfreq>=1000000) && (desfreq<=33000000) )
	{
		passval[1] = 0;//is fs6131
	}
	else
	{
		if(type==2)
		{
		printf("Out of range 1MHz <= Frequency <= 33MHz\n");
		exit(1);
		}
	}
	
	printf("Opening: %s\n",nbuf);
	
	if((hDevice = CreateFile (
		nbuf, 
		GENERIC_READ | GENERIC_WRITE,
		0,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
		NULL)) == INVALID_HANDLE_VALUE ) 
	{
		printf("Can't get a handle to sfcdrv @ %s\n",nbuf);
		exit(1);
	}
	
	printf("Desired Frequency: %d\n",desfreq);
	

	if(	DeviceIoControl(hDevice,IOCTL_SFCDRV_SET_FREQUENCY2,&passval[0],2*sizeof(ULONG),&out,sizeof(ULONG),&temp,NULL))
	{
		printf("frequency set\n");
	}
	else printf("Problem Setting clock frequency.\n");/* check owners manual */       

	
	CloseHandle(hDevice);
	return 0;
}
