/*++

Copyright (c) 1998,1999 Commtech, Inc Wichita ,KS

Module Name:

    sdlc_frame_gen.c

Abstract:

    A simple Win32 app that uses the sfcdrv device to transmit only
	--modified to generate and send SDLC frames
	designed to generate frames to be received by the SDLCSNOOP example program
	(with a SD+(cable#2) ->RD+(cable#1), SD-(cable#2) ->RD-(cable#1) connection.)
	(this is a SD->RD loop from sfc1 to sfc0).

Environment:

    user mode only

Notes:

    
Revision History:

    6/8/98             started
	10/27/99  --mod for SDLC frame send
--*/



#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "..\sfc.h"




VOID
main(int argc, char *argv[]
    )
/*++

Routine Description:

Arguments:

Return Value:

--*/
{
	HANDLE hDevice;                 //handle to the sfc driver/device
	DWORD nobyteswritten;   //number of bytes that the driver returns as written to the device
	char buffer[4096];                //character array for data storage (passing to and from the driver)
	ULONG j;                                //temp vars
	unsigned i;
	unsigned key;
	DWORD k;
	DWORD returnsize;               //temp vars
	BOOL t;                                 //temp vars
	OVERLAPPED  os ;                                //overlapped structure for use in the transmit routine
	unsigned ns,nr,pf;
	unsigned address;
	unsigned port;
	char devname[64];
	ULONG passval[2];
	if(argc<2)
	{
		printf("usage:\r\n");
		printf("%s port\r\n",argv[0]);
		exit(1);
	}
	port = atoi(argv[1]);
	
	
	
	
	
	memset( &os, 0, sizeof( OVERLAPPED ) ) ;        //wipe the overlapped struct
	
	// create I/O event used for overlapped write
	
	os.hEvent = CreateEvent( NULL,    // no security
		TRUE,    // explicit reset req
		FALSE,   // initial event reset
		NULL ) ; // no name
	if (os.hEvent == NULL)
	{
		MessageBox( NULL, "Failed to create event for thread!", "main Error!",
			MB_ICONEXCLAMATION | MB_OK ) ;
		return; 
	}
	
	
	
	//this will start up the driver, it also gives us a handle to send
	//and receive data from the driver
	//note that it is created with the FILE_FLAG_OVERLAPPED set
	//if it is not then you must make sequential calls to the driver
	//(ie wait until one call has returned before making another call)
	//the read/write and status functions are set up to use overlapped, they 
	//will return immediatly with a ERROR_IO_PENDING or immediatly with the required data
	//
	
	//default to using port 1 (cable #2)
	sprintf(devname,"\\\\.\\SFC%d",port);
	printf("using device:%s\r\n",devname);
    hDevice = CreateFile (devname,
		GENERIC_READ | GENERIC_WRITE,
		0,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
		NULL
		);
	
    if (hDevice == INVALID_HANDLE_VALUE)
    {
		//for some reason the driver won't load or isn't loaded
		printf ("Can't get a handle to %s\n",devname);
		CloseHandle(os.hEvent);
		exit(1);
		//abort and leave here!!!
	}
	//use whatever settings they had coming into this
	DeviceIoControl(hDevice,IOCTL_SFCDRV_FLUSH_TX,NULL,0,NULL,0,&t,NULL);
	passval[0] = 0x0100+(port*0x80);//cmdr
	passval[1] = 0x01000000;//reset tx
	DeviceIoControl(hDevice,IOCTL_SFCDRV_WRITE_REGISTER,&passval[0],2*sizeof(DWORD),NULL,0,&t,NULL);
	
	do
	{
		//get info and build SDLC frame
		printf("send I, S, N(U) frame?\r\n");
		i = getch();
		key = i;
		if((i=='i')||(i=='I'))
		{
			printf("enter NS (0-7):");
			scanf("%u",&ns);	
			printf("\r\n");
			printf("enter NR (0-7):");
			scanf("%u",&nr);	
			printf("\r\n");
			printf("enter p/f (0,1):");
			scanf("%u",&pf);	
			printf("\r\n");
			ns = ns &0x07;//mask off unused.
			nr = nr &0x07;//mask off unused.
			pf = pf &0x01;//mask off unused.
			printf("enter address (HEX):");
			scanf("%x",&address);
			printf("\r\n");
			buffer[0] = address;//set address field
			buffer[1] = (nr<<5)|(pf<<4)|(ns<<1);//build control field		
			printf("enter info:([esc] to send)\r\n");
			k  = 2;
			do
			{
				j = getche();
				if(j!=27)buffer[k++]= (char)j;
			}while(j!=27);
			printf("\r\n");
			goto sendtheframe;
		}
		if((i=='s')||(i=='S'))
		{
			printf("1. RR\r\n");
			printf("2. RNR\r\n");
			printf("3. REJ\r\n");
			j = getch();
			
			printf("enter NR (0-7):");
			scanf("%u",&nr);	
			printf("\r\n");
			printf("enter p/f (0,1):");
			scanf("%u",&pf);	
			printf("\r\n");
			nr = nr &0x07;//mask off unused.
			pf = pf &0x01;//mask off unused.
			printf("enter address (HEX):");
			scanf("%x",&address);
			printf("\r\n");
			buffer[0] = address;//set address field
			
			if(j =='1')
			{
				buffer[1] = (nr<<5)|(pf<<4)|1;
			}
			if(j=='2')
			{
				buffer[1] = (nr<<5)|(pf<<4)|5;
			}
			if(j=='3')
			{
				buffer[1] = (nr<<5)|(pf<<4)|9;
			}                            
			k = 2;
			goto sendtheframe;
		}
		if((i=='n')||(i=='N')||(i=='u')||(i=='U'))
		{
			printf("1. NSI\r\n");
			printf("2. RQI\r\n");
			printf("3. SIM\r\n");
			printf("4. SNRM\r\n");
			printf("5. ROL\r\n");
			printf("6. DISC\r\n");
			printf("7. NSA\r\n");
			printf("8. CMDR\r\n");
			printf("9. ORP\r\n");
			j = getch();
			
			printf("enter address (HEX):");
			scanf("%x",&address);
			printf("\r\n");
			buffer[0] = address;//set address field
			
			if(j=='1')
			{
				printf("enter p/f (0,1):");
				scanf("%u",&pf);	
				printf("\r\n");
				pf = pf &0x01;//mask off unused.
				buffer[1] = (pf<<4)|3;
				printf("enter info:([esc] to send)\r\n");
				k  = 2;
				do
				{
					j = getche();
					if(j!=27)buffer[k++]= (char)j;
				}while(j!=27);
				printf("\r\n");
				goto sendtheframe;
			}
			if(j=='2')
			{
				buffer[1] = 0x07;
				k = 2;
				goto sendtheframe;
			}		
			if(j=='3')
			{
				buffer[1] = 0x17;
				k = 2;
				goto sendtheframe;
			}		
			if(j=='4')
			{
				buffer[1] = (char)0x93;
				k = 2;
				goto sendtheframe;
			}		        
			if(j=='5')
			{
				buffer[1] = 0x0f;
				k = 2;
				goto sendtheframe;
			}		        
			if(j=='6')
			{
				buffer[1] = 0x53;
				k = 2;
				goto sendtheframe;
			}		        
			if(j=='7')
			{
				buffer[1] = 0x63;
				k = 2;
				goto sendtheframe;
			}		        
			if(j=='8')
			{
				buffer[1] = (char)0x87;
				k = 2;
				goto sendtheframe;
			}	
			if(j=='9')
			{
				buffer[1] = 0x33;
				k = 2;
				goto sendtheframe;
			}		
		}
		
sendtheframe:
		//send the frame
		t = WriteFile(hDevice,buffer,k,&nobyteswritten,&os);//send the frame
		if(t==FALSE)                    //if it returned FALSE then the IO request is queued (waiting for previous frame to get done sending)
		{                       //and we must wait until the os.event gets signaled before we try any more sending
			if (GetLastError() == ERROR_IO_PENDING)  //IO_PENDING is the indication that the request was queued, if not this than a parameter error occured
			{
				do
				{
					j = WaitForSingleObject( os.hEvent, 10000);
					if(j==WAIT_TIMEOUT)
					{
						printf("timeout\r\n");
						DeviceIoControl(hDevice,IOCTL_SFCDRV_FLUSH_TX,NULL,0,NULL,0,&returnsize,NULL);
						passval[0] = 0x0100+(port*0x80);//cmdr
						passval[1] = 0x01000000;//reset tx
						DeviceIoControl(hDevice,IOCTL_SFCDRV_WRITE_REGISTER,&passval[0],2*sizeof(DWORD),NULL,0,&returnsize,NULL);
					}
				}while(j!=WAIT_OBJECT_0);
			}
		}
	}while(key!=27);
	
	CloseHandle (hDevice);// stops the sfc from interrupting (ie shuts it down)
	CloseHandle(os.hEvent);//finished with the event
	printf("exiting program\n\r");          //exit message
}                                               //done

