/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
setlsb2msb.c -- user mode function to toggle the driver mode lsb2msb function (converts lsb2msb on read and write in driver)
             -- note this is functionally the same as running the convertlsb2msb() function from the lsb2msb directory
			 -- on your data before writing, or on the received data after ReadFile completes.
			 -- there is not a hardware switching function for this, the 20534 ONLY transmits/receives lsb first!

             -- also since it simply does the convertlsb2msb() on the entire data array
			 -- if you are in hdlc mode, the RSTA byte will be mirrored.
			 -- To decode it properly you need to lsb2msb convert it back.


usage:
 setlsb2msb port value

port is any valid SFC port (0,1,2,3).

value :
0 => transmit/receive lsb first (default)
1 => transmit/receive msb first (conversion in software)
   

*/


#include <windows.h>
#include <stdio.h>
#include <math.h> /* floor, pow */

#include "..\sfc.h"



int main(int argc, char * argv[])
{
	//clkset clock;
	char nbuf[80];
	int port,t;
	HANDLE hDevice; 
    unsigned long reg,val,temp;

	unsigned long passval[2];

	if(argc<3) {
                printf("usage:\n %s port value\n",argv[0]);
		exit(1);
	}

	port = atoi(argv[1]);
    val = atol(argv[2]);


		
		passval[0] = val;

        sprintf(nbuf,"\\\\.\\SFC%u",port);

	printf("Opening: %s\n",nbuf);

	if((hDevice = CreateFile (
			nbuf, 
			GENERIC_READ | GENERIC_WRITE,
			0,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
			NULL)) == INVALID_HANDLE_VALUE ) 
	{
                printf("Can't get a handle to sfcdrv @ %s\n",nbuf);
		exit(1);
	}

        t = DeviceIoControl(hDevice,
                        IOCTL_SFCDRV_CONVERT_LSB_TO_MSB,                            /* Device IOCONTROL */
			&passval,								/* write data */
			sizeof(unsigned long ),						/* write size */
			NULL,								/* read data */
			0,									/* read size */
			&temp,								/* Returned Size */
			NULL); /* overlap */
        if(t)  
		{
			if(val==0)printf("set SFC%d configured to tx/rx LSB first\n",port);
			if(val==1)printf("set SFC%d configured to tx/rx MSB first\n",port);
		}
        else printf("Problem configuring LSB2MSB state on SFC%d\n",port);/* check owners manual */
	CloseHandle(hDevice);
	return 0;
}
/* $Id$ */