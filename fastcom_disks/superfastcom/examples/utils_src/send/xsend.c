/* Debug! */
/* SFC Test Utility.  Opens a file and Sends it through port 0*/


#include <windows.h>
#include <fcntl.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <io.h>

#include "..\sfc.h"

int main(int argc, char *argv[])
{
        HANDLE wDevice;                         /* Handler for the XSA driver */
	int fd;						/* Handler for the to Open */
	int size,error,t,j;
	DWORD nobyteswritten;
	DWORD nobytesread;
	char *txbuffer, *tmp, buf[1];
	OVERLAPPED  wq;

	
	if(argc<2)
	{
		printf("usage: %s [file]\n",argv[0]);
		printf("Opens the file then sends it out port 0\n");
		exit(1);
	}

/********************************
******* Crate File Handle *******
********************************/
	/* Open the File to sends */
	if((fd = _open(argv[1],O_RDONLY))==-1)
	{
		perror("XSEND ERROR");
		exit(1);
	}
	size=0;
	while(_read(fd,buf,1)!=0) size++;
	_lseek(fd,0,SEEK_SET);
	printf("%s: %d bytes\n",argv[1],size);
	txbuffer = (char *)malloc(size);
	
	if(txbuffer == NULL){
		printf("Could not Allocate %d bytes of memory for txbuffer.!\n",size);
		exit(1);
	}

	memset(txbuffer,0,size);
	(char*)tmp = (char*)txbuffer;
	while(_read(fd,tmp++,1)!=0); 
	_close(fd);




/*****************************************
******* Create Port 0 Device Handle ******
*****************************************/
	memset( &wq, 0, sizeof( OVERLAPPED ) );
	wq.hEvent = CreateEvent( NULL,TRUE,FALSE,NULL);
	if (wq.hEvent == NULL)
	{
		printf("Failed to create event for thread!\n");
		return 1; 
	}
        wDevice = CreateFile ("\\\\.\\SFC0", GENERIC_READ|GENERIC_WRITE,
			FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,NULL);
    if (wDevice == INVALID_HANDLE_VALUE)
    {
		LPVOID lpMsgBuf;
                printf ("ERROR: Can't get a handle to the SFC Driver.\n");
		
		j = GetLastError();
		FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER|FORMAT_MESSAGE_FROM_SYSTEM,    
			NULL,
			j,
			MAKELANGID(LANG_NEUTRAL,SUBLANG_DEFAULT),
			(LPTSTR) &lpMsgBuf,
			0,
			NULL 
		);
		
		printf("%d: %s\n",j,lpMsgBuf);
		free(txbuffer);
		exit(1);
	}

	//while(1)
	//{
		error=0;

		t = WriteFile(wDevice,&txbuffer,size,&nobyteswritten,&wq);
		if(t==FALSE)  
		{
			LPVOID lpMsgBuf;
			t=GetLastError();
			FormatMessage(
					FORMAT_MESSAGE_ALLOCATE_BUFFER | 
					FORMAT_MESSAGE_FROM_SYSTEM | 
					FORMAT_MESSAGE_IGNORE_INSERTS,    
					NULL,   // message source
					t,  // message identifier
					MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),  // language identifier
					(LPTSTR) &lpMsgBuf,    // message buffer
					0,        // maximum size of message buffer
					NULL  // array of message inserts
					);
			printf("%s\n",lpMsgBuf);
		
			if(t==ERROR_IO_PENDING)
			{
				do
				{
					j = WaitForSingleObject( wq.hEvent, 1500 );//1.5 second timeout
					if(j==WAIT_TIMEOUT)
					{
						printf("Transmitter Locked up... Resetting TX.\r\n");
                                                //DeviceIoControl(wDevice,IOCTL_SFCDRV_FLUSH_TX,NULL,0,NULL,0,&returnsize,NULL);
					}
					if(j==WAIT_ABANDONED)
					{
						printf("Recieve Abandoned.\r\n");
						goto close;
					}
				} while(j!=WAIT_OBJECT_0);
			}
		}
		printf("WROTE: %d bytes.\n",nobyteswritten);
	//}

close:
	free(txbuffer);
	CloseHandle (wDevice);

	return 0;
}
