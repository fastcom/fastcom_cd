/* $Id$ */
/*
Copyright(C) 2005, Commtech, Inc.
bisync_pattern_rx.c -- user mode program expecting to receive a repetitave 60 byte sequence that starts
					0x55 0x3e 0xaa

Note that the bisync start pattern detection only occurs when a descriptor is complete (ie on a receive HI interrupt)
This means that if the descriptor size is set to 4096, that 4097 bytes must be clocked into the receiver (including the syncl/synch bytes)
before the pattern is tested.  To improve responsiveness you must decrease the descriptor size, keep in mind
that doing so will increase the interrupt rate, and that this is unavoidable.
To make this be "the same" as the ESCC card, set the descriptor size to 32.
This code does not set the descriptors, to tweek them use the getbufs/setbufs examples, or 
the sfc_advanced_settings utility program.


 usage:
  bisync_pattern_rx port 


*/


#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "..\sfc.h"

int main(int argc, char *argv[])
{
	HANDLE rDevice;
	ULONG t;
	DWORD nobytesread;
	char *rdata;
	ULONG size;
	OVERLAPPED  rq;
	int j,error;
	unsigned i;
	ULONG totalsent;
	ULONG totalread;
	ULONG totalerror;
	ULONG loop;
    char devname[25];
	ULONG desc_parm[4];
	SCC_REGS settings;
	BISYNCSTART bsp;
	ULONG temp;
	ULONG passval[2];
	ULONG rcnt, rcnt_last;
	
	rcnt=0;
	rcnt_last=0;
	if(argc<2)
	{
		printf("usage:\n");
		printf("bisync_pattern_rx port \n");
		exit(1);
	}
	size = 60;	//this is the number of bytes to receive on each read, this is the equivalent of what you would set the bisync_cutoff_size to be on the ESCC card
	
	
	sprintf(devname,"\\\\.\\SFC%d",atoi(argv[1]));
	printf("devicename:%s\n",devname);
	
	memset( &rq, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
	rq.hEvent = CreateEvent( NULL,    // no security
		TRUE,    // explicit reset req
		FALSE,   // initial event reset
		NULL ) ; // no name
	if (rq.hEvent == NULL)
	{
		printf("Failed to create event for read\n");
		exit(1);
	}
	rDevice = CreateFile (devname,
		GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL,
		OPEN_ALWAYS,
		FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
		NULL
		);
	
    if (rDevice == INVALID_HANDLE_VALUE)
    {
		//for some reason the driver won't load or isn't loaded
		printf ("Can't get a handle to %s\n",devname);
		CloseHandle(rq.hEvent);
		exit(1);
		//abort and leave here!!!
	}
	
	//configure the bisync pattern match (start) here
	bsp.count=3;			//we are expecting a 3 byte sync pattern
	bsp.pattern[0] = 0x55;  //the first byte (SYNL)
	bsp.pattern[1] = 0x3e;  //the second byte (SYNH)
	bsp.pattern[2] = 0xaa;  //the third byte
	
	//note that it is expected that these sync bytes will be repeating in the data every size bytes.
	
	DeviceIoControl(rDevice,IOCTL_SFCDRV_SET_BISYNC_START_PATTERN,&bsp,sizeof(bsp),NULL,0,&t,NULL);
	
	
	temp = 1; //enable the bisync pattern match detection
	DeviceIoControl(rDevice,IOCTL_SFCDRV_BISYNC_START_PATTERN_MATCH_EN,&temp,sizeof(ULONG),NULL,0,&t,NULL);
	
	
	memset(&settings,0,sizeof(SCC_REGS));//wipe the settings structure (default everything to 0)
	
	settings.cmdr = 0x01050000;//reset transmit/receive, enter hunt state
	settings.star = 0x0;
	settings.ccr0 = 0x80020030;//power up, bisync mode, clock mode 0b
	settings.ccr1 = 0x02240C00;//ODS=1, ICD=1, cts disabled, bisync mode, 
	settings.ccr2 = 0x09040000;//8 bit chars, receiver enabled, store sync, no parity 32 byte triggers, no preambles
	settings.imr = 0xFFFEFFEF;//disable all but  xdu
	
	settings.syncr = 0x00003e55;//set SYNL = 0x55 , SYNH = 0x3e
	/*
	//these all default to 0, on the memset above
	settings.timr = value;
	settings.accm = value;
	settings.udac = value;
	settings.tsax = value;
	settings.tsar = value;
	settings.pcmmtx = value;
	settings.pcmmrx = value;
	settings.brr = value;
	settings.xadr = value;
	settings.radr = value;
	settings.ramr = value;
	settings.rlcr = value;
	settings.xnxfr = value;
	settings.tcr = value;
	settings.ticr = value;
	settings.isr = 0x0;
	*/
	
	DeviceIoControl(rDevice,IOCTL_SFCDRV_SCC_REGISTERS_SET,&settings,sizeof(settings),NULL,0,&t,NULL);
	
	
	//allocate memory for read/write
	rdata = (char*)malloc(size+1);
	if(rdata==NULL)
	{
		printf("cannot allocate memory for data area\n");
		CloseHandle(rDevice);
		CloseHandle(rq.hEvent);
		exit(1);
	}
	
	//make sure there is enough buffering in the driver to do the read
	DeviceIoControl(rDevice,IOCTL_SFCDRV_GET_DESC_PARAMETERS,NULL,0,&desc_parm,4*sizeof(ULONG),&t,NULL);
	if(desc_parm[0]*desc_parm[1]<size)
	{
		printf("driver descriptor total is less than size:%d \n",size);
		printf("please increase buffering parameters or decrease block size\n");
		free(rdata);
		CloseHandle(rDevice);
		CloseHandle(rq.hEvent);
		exit(1);
	}
	
	//issue FLUSH_RX
	DeviceIoControl(rDevice,IOCTL_SFCDRV_FLUSH_RX,NULL,0,NULL,0,&t,NULL);
	
	//issue HUNT
	passval[0] = 0x100+(0x80*atoi(argv[1]));
	passval[1] = 0x00040000;
    DeviceIoControl(rDevice,IOCTL_SFCDRV_WRITE_REGISTER,&passval,2*sizeof(unsigned long ),&t,sizeof(unsigned long),&temp,NULL);
	
	totalerror=0;
	totalsent=0;
	totalread=0;
	loop=0;
	
	while(!kbhit())//main loop, do until someone hits a key
	{
		error=0;
		t = ReadFile(rDevice,&rdata[0],size,&nobytesread,&rq);
		if(t==FALSE)  
		{
			//	printf("read blocked\n");
			t=GetLastError();
			if(t==ERROR_IO_PENDING)
			{
				do
				{
					j = WaitForSingleObject( rq.hEvent, 5000 );//5 second timeout -- must be greater than size*8*(1/bitrate)*1000
					if(j==WAIT_TIMEOUT)
					{
						if(kbhit()) goto done;
						printf("Reciever Timeout.\r\n");
					}
					if(j==WAIT_ABANDONED)
					{
						printf("Reciever Abandoned.\r\n");
						goto close;
					}
				} while(j!=WAIT_OBJECT_0);
				GetOverlappedResult(rDevice,&rq,&nobytesread,TRUE);
			}
			else printf("READ ERROR: #%x\n",t);
		}
		
		if(nobytesread!=size)
		{
			printf("received:%lu, expected %lu\n",nobytesread,size);
		}
		if(nobytesread!=0)
		{
			for(i=0;i<nobytesread;i++) printf("%x:",rdata[i]&0xff);
			printf("\n");
			//this bit depends on the bisync_test code, it is sending an ascii counter in the 
			//bytes after the sync, we just make sure that all reads have counts that make sense
			//this should display on the first read (as the count is obviously not going to be right)
			//and any time that the sequence gets out of sorts (stopped transmit, missing frames etc)
			rcnt=atol(&rdata[3]);
			if(rcnt!=rcnt_last+1) printf("out of sync:%d != %d\n",rcnt,rcnt_last+1);
			rcnt_last = rcnt;
		}
		loop++;
		totalread+=nobytesread;
	}
done:
	getch();//clear the keyboard buffer
	printf("Read  %lu bytes\n",totalread);
	printf("Count %lu\n",loop);
	
close:
	free(rdata);//done with the read buffer
	CloseHandle(rDevice);//done with the SuperFastcom port
	CloseHandle(rq.hEvent);//done with the overlapped event
	return 0; //bye
}
/* $Id$ */