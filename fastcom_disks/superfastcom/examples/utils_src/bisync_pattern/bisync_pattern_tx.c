/* $Id$ */
/*
Copyright(c) 2005, Commtech, Inc.
bisync_pattern_tx.c -- user program that generates a continuous stream of data to be received by the bisync_pattern.c example
			  -- configures the SuperFatcom port to be in extended transparent mode, sets the clock to 1M
			  -- and fills 1000 x 60 byte frames and sends them (hopefully continuously)

 usage:
  bisync_pattern_tx port 

port is the SFC port to use

*/


#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "..\sfc.h"

int main(int argc, char *argv[])
{
	HANDLE wDevice;/* Handle for the SuperFastcom port */
	ULONG t;
	DWORD nobyteswritten;
	char *tdata;
	ULONG size;
	OVERLAPPED  wq;
	int j,x,error,tosend;
	ULONG totalsent;
	ULONG totalread;
	ULONG totalerror;
	ULONG loop;
    char devname[25];
	ULONG desc_parm[4];
	FILE *fout=NULL;
	unsigned long fcount;
	SCC_REGS settings;
	ULONG passval[2];
	ULONG out;
	ULONG temp;
	int i;
	
	srand( (unsigned)time( NULL ) );//seed the rng
	
	if(argc<2)
	{
		printf("usage:\n");
		printf("bisync_pattern_tx port \n");
		exit(1);
	}
	size = 60*1000;//block size to send to the port
	
    sprintf(devname,"\\\\.\\SFC%d",atoi(argv[1]));
	printf("devicename:%s\n",devname);
	printf("blocksize:%lu\n",size);
	
	memset( &wq, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
	wq.hEvent = CreateEvent( NULL,    // no security
		TRUE,    // explicit reset req
		FALSE,   // initial event reset
		NULL ) ; // no name
	if (wq.hEvent == NULL)
	{
		printf("Failed to create event for thread!\n");
		return 1; 
	}
	//open the SuperFastcom port
	wDevice = CreateFile (devname,
		GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL,
		OPEN_ALWAYS,
		FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
		NULL
		);
    if (wDevice == INVALID_HANDLE_VALUE)
    {
		printf ("Can't get a handle to sfcdrv %s\n",devname);
		exit(1);
		//abort and leave here!!!
	}
	
	passval[0] = 1000000;//1Meg
	passval[1] = 5;//assume SuperFastcom PCI card (icd2053b)
	if(	DeviceIoControl(wDevice,IOCTL_SFCDRV_SET_FREQUENCY,&passval[0],2*sizeof(ULONG),&out,sizeof(ULONG),&temp,NULL))
	{
		printf("frequency set: %d\n",passval[0]);
	}
	else printf("Problem Setting clock frequency.\n");/* check owners manual */       
	
	
	memset(&settings,0,sizeof(SCC_REGS));
	
	settings.cmdr = 0x01010000;//reset transmit & receive
	settings.star = 0x0;
	settings.ccr0 = 0x80000037;//power up, HDLC mode, NRZ, clock mode 7b
	settings.ccr1 = 0x0224e000;//extended transparent mode
	settings.ccr2 = 0x00040000;//start with hdlc receiver OFF (no receive)
	settings.imr = 0xFFFEFFFF;//disable all but  xdu
	settings.timr = 0x070009ff;//not used
	settings.brr = 0x0;//baud rate = 1Mbps = inclk/((0+1)*2^0)
	
	/*
	//these all default to 0, on the memset above
	settings.accm = value;
	settings.udac = value;
	settings.tsax = value;
	settings.tsar = value;
	settings.pcmmtx = value;
	settings.pcmmrx = value;
	settings.brr = value;
	settings.xadr = value;
	settings.radr = value;
	settings.ramr = value;
	settings.rlcr = value;
	settings.xnxfr = value;
	settings.tcr = value;
	settings.ticr = value;

	 settings.syncr = value;
	 settings.isr = 0x0;
	*/
	
	DeviceIoControl(wDevice,IOCTL_SFCDRV_SCC_REGISTERS_SET,&settings,sizeof(settings),NULL,0,&t,NULL);
	
	tdata = (char*)malloc(size+1);//allocate transmit data array
	if(tdata==NULL)
	{
		printf("unable to allocate data buffer\n");
		exit(1);
	}
	//make sure that there is enough driver buffering to accomodate this write size
	DeviceIoControl(wDevice,IOCTL_SFCDRV_GET_DESC_PARAMETERS,NULL,0,&desc_parm,4*sizeof(ULONG),&t,NULL);
	if(desc_parm[2]*desc_parm[3]<size)
	{
		printf("driver descriptor total is less than blocksize:%d\n",size);
		printf("please increase buffering parameters or decrease block size\n");
		free(tdata);
		CloseHandle(wDevice);
		CloseHandle(wq.hEvent);
	}
	
	//start fresh
	DeviceIoControl(wDevice,IOCTL_SFCDRV_FLUSH_TX,NULL,0,NULL,0,&t,NULL);
	
	totalerror=0;
	totalsent=0;
	totalread=0;
	loop=0;
	fcount=0;
	
	while(!kbhit())//main loop, do this until someone hits a key
	{
		error=0;
		// prefill the data array with randomness
		tosend = size;
		for(x=0;x<tosend;x++) tdata[x]=(UCHAR)(rand());
		
		//then fill in our frames with the start sequence and a frame counter
		for(i=0;i<1000;i++)
		{
			tdata[0 +(i*60)] = 0x55; //will be seen as SYNL
			tdata[1 +(i*60)] = 0x3e; //will be seen as SYNH
			tdata[2 +(i*60)] = (UCHAR)0xaa; //will be seen as the third byte of our pattern match
			sprintf(&tdata[3 +(i*60)],"%8.8lu",fcount++);//ascii frame counter (to detect frame gaps)
		}
		//send the block
		t = WriteFile(wDevice,&tdata[0],tosend,&nobyteswritten,&wq);
		if(t==FALSE)  
		{
			t=GetLastError();
			if(t==ERROR_IO_PENDING)
			{
				do
				{
					j = WaitForSingleObject( wq.hEvent, 10000 );//10 second timeout -- must be larger than size*8*(1/bitrate)*1000
					if(j==WAIT_TIMEOUT)
					{
						printf("Transmitter Timeout, Locked up?\r\n");
					}
					if(j==WAIT_ABANDONED)
					{
						printf("Tx Abandoned.\r\n");
						goto close;
					}
				} while(j!=WAIT_OBJECT_0);
				GetOverlappedResult(wDevice,&wq,&nobyteswritten,TRUE);
			}
			else printf("WRITE ERROR: #%d\n",t);
		}
		if(nobyteswritten!=size)
		{
			printf("unexpected actual:%lu, sent:%lu\n",nobyteswritten,size); 
		}
		loop++;
		totalsent+=nobyteswritten;
	}
	getch();//clear the keyboard buffer
	printf("Wrote %lu bytes\n",totalsent);
	printf("count %lu\n",loop);
	
close:
	free(tdata);//done with the transmit data buffer
	CloseHandle(wq.hEvent);//done with the overlapped event
	CloseHandle (wDevice);//done with the SuperFastcom port
	return 0;//bye
}
/* $Id$ */
