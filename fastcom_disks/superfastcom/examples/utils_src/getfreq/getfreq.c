/* $Id$ */
/*
	Copyright(C) 2002, Commtech, Inc.

	getfreq.c --   a user program to get the current clock generator setting 
			
usage:
 getfreq port

 The port can be any valid sfc port (0,1,2,3).
 
*/
#include <windows.h>
#include <stdio.h>
#include "..\sfc.h"

int main(int argc, char * argv[])
{
	HANDLE dev;
	unsigned long t;
	int res;
	char devname[25];

if(argc<2)
{
printf("usage:\ngetclock port\n");
exit(1);
}
sprintf(devname,"\\\\.\\SFC%d",atoi(argv[1]));
	dev = CreateFile(devname,GENERIC_WRITE | GENERIC_READ , FILE_SHARE_READ | FILE_SHARE_WRITE,
	NULL,OPEN_EXISTING, 0, NULL);
	
	if(dev== INVALID_HANDLE_VALUE)
	{
		printf("ERROR: Could not open device!\n");
		exit(1);
	}

        DeviceIoControl(dev, IOCTL_SFCDRV_GET_FREQUENCY,
			NULL,
			0,
			&t,
			sizeof(unsigned long),
			&res,
			NULL);
	
	printf("Clock Generator is set to: %ld\n",t);

	CloseHandle(dev);

	return 0;

}
/* $Id$ */