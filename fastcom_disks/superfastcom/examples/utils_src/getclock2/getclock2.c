/* $Id$ */
/*
	Copyright(C) 2002, Commtech, Inc.

	getclock2.c --  a user program to get the current clock generator
			setting for the programmable clock output on 
			port0 (cable #1).

usage:
 progclock port

 The port can be any valid sfc port (0,1,2,3).
 
*/

#include <windows.h>
#include <stdio.h>

#include "..\sfc.h"

int main()
{
	HANDLE dev;
	clkset t;
	int res;


	dev = CreateFile("\\\\.\\SFC0",GENERIC_WRITE | GENERIC_READ , FILE_SHARE_READ | FILE_SHARE_WRITE,
	NULL,OPEN_EXISTING, 0, NULL);
	
	if(dev== INVALID_HANDLE_VALUE)
	{
		printf("ERROR: Could not open device!\n");
		exit(1);
	}

        DeviceIoControl(dev, IOCTL_SFCDRV_GET_CLOCK2,
			NULL,
			0,
			&t,
			sizeof(clkset),
			&res,
			NULL);
	
	printf("double: %f\n",t.actualclock);

	CloseHandle(dev);

	return 0;

}
/* $Id$ */