/* $Id$ */
/*
Copyright(C) 2002, Commtech, Inc.
st_tt.c -- a user mode program to switch the onboard txclk multiplexer between the TT output and the ST input

 usage:
  st_tt port [st|tt]


*/


#include "windows.h"
#include "stdio.h"
#include "../sfc.h"

#define ACTIVATE_ST 0
#define ACTIVATE_TT 1
void set_st_or_tt(HANDLE hsfc,ULONG port,ULONG st_or_tt);
int main(int argc,char *argv[])
{
ULONG mode;
HANDLE hdevice;
ULONG port;
char devname[25];

if(argc<3)
{
printf("usage: %s port tt|st\n",argv[0]);
exit(1);
}
port = atoi(argv[1]);


sprintf(devname,"\\\\.\\SFC%d",port);
printf("devicename:%s\n",devname);
hdevice = CreateFile (devname,GENERIC_READ | GENERIC_WRITE,FILE_SHARE_READ | FILE_SHARE_WRITE,NULL,OPEN_ALWAYS,FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,NULL);
    if (hdevice == INVALID_HANDLE_VALUE)
		{
		//for some reason the driver won't load or isn't loaded
        printf ("Can't get a handle to %s\n",devname);
		exit(1);
		}

mode = ACTIVATE_TT;//default

if(strncmp(argv[2],"st",2)==0) mode = ACTIVATE_ST;
if(strncmp(argv[2],"tt",2)==0) mode = ACTIVATE_TT;

set_st_or_tt(hdevice,port,mode);

if(mode==ACTIVATE_ST) printf("%s txclk connected to ST\r\n",devname);
if(mode==ACTIVATE_TT) printf("%s txclk connected to TT\r\n",devname);
CloseHandle(hdevice);
}

void set_st_or_tt(HANDLE hsfc,ULONG port,ULONG st_or_tt)
{
//hsfc is an open handle to superfastcom port
//port is the SFC port number that was opened (ie 0,1,2,3)
//st_or_tt is either ACTIVATE_ST or ACTIVATE_TT
ULONG reg;
ULONG val;
ULONG temp;
ULONG regmask;
ULONG activest;
ULONG activett;
ULONG passval[2];

#ifdef XSA104
if(st_or_tt==ACTIVATE_ST) val=0x00005000;
if(st_or_tt==ACTIVATE_TT) val=0x0000a000;
passval[0] = 0x404; //gpdata
passval[1] = val;
DeviceIoControl(hsfc,IOCTL_SFCDRV_WRITE_REGISTER,&passval,2*sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);

#else
//superfastcom and superfastcom232/4-pci and superfastcom/2-104
reg = 2;
DeviceIoControl(hsfc,IOCTL_SFCDRV_READ_LB,&reg,sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);

regmask = 0x03;
regmask = regmask<<(2*(port%4));
//bit0 is 0, st is enabled
//bit0 is 1, st is disabled
//bit1 is 0, tt is enabled
//bit1 is 1, tt is disabled
activest = 0x02<<(2*(port%4));//disable TT, enable ST
activett = 0x01<<(2*(port%4));//disable ST, enable TT
if(st_or_tt==ACTIVATE_ST) val=(val&(~regmask))|activest;
if(st_or_tt==ACTIVATE_TT) val=(val&(~regmask))|activett;
passval[0] = 2;
passval[1] = val;
DeviceIoControl(hsfc,IOCTL_SFCDRV_WRITE_LB,&passval,2*sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);
#endif
}
/* $Id$ */
