// $Id$
/*

	sfcsocket.c  -- A windows sockets example for the SuperFastcom card

				-- This program when called as a server will relay data packets between
				-- a socket connection and an SFC card.  Data comming in a
				-- socket is sent out the SFC port, data comming in the 
				-- SFC port is sent out socket.  This continues until the 
				-- client closes the socket.

				-- As a client this program takes keystrokes (up to a [CR])
				-- packages them up in a "frame" and sends them to the socket
				-- (it is then assumed that this server sends that data out the SFC)
				-- It also receives any incomming data from the socket and displays
				-- it on the screen.

				It is assumed that the SFC port has been configured prior to running this code
				i.e.:

					setfreq 0 1000000 0
					sfcset 0 hdlcset
				

  Usage:
  sfcsocket SFCport IPaddress [client]
  
  To call as a client execute the program as:

  sfcsocket 0 127.0.0.1 c

  To call as a server execute the program as:

  sfcsocket 0 127.0.0.1 



*/


#include "winsock2.h"			//required header for using windows sockets
#include "stdio.h"				//printf etc
#include "stdlib.h"
#include "conio.h"				//kbhit
#include "string.h"				//strlen
#include "..\sfctoolbox.h"		//gets us the simple sfc interaction

//port that we will listen on
#define S_PORT 5003		
//number of clients to queue	
#define MAX_PENDING_CONNECTS 4

DWORD FAR PASCAL ReadProc( LPVOID lpData );	//thread that server uses to receive data from sfc port and write to socket connection
DWORD FAR PASCAL WriteProc( LPVOID lpData );//thread that server uses to read from the socket and write to the sfc port

BOOL connected;								//global variable that holds a "we are connected" state
SOCKET a_theSocket;							//global variable that holds the connected socket (used in readproc and writeproc)
int sfc_port;								//global variable that holds sfc port number (ie holds X where \\.\sfcX)

int main(int argc, char *argv[])
{
	SOCKET m_theSocket;							//socket to use for client, and for listening on for the server
	char buf[4096];								//data buffer for client, holds keypress data used to send to socket
	char *cb;									//character pointer used for getting internet addresses as strings from winsock, ie holds return value from inet_ntoa()
	int key;									//holds current user keypress
	int i;										//temp variable (loops)
	int j;										//temp variable (loops)
	int k;										//temp variable (loops)
	char rbuf[4096];							//data buffer for client, used to obtain data from socket in client mode
	WORD wVersionRequested;						//required winsock stuff
	WSADATA wsaData;							//required winsock stuff
	int err;									//required winsock stuff
	struct sockaddr_in   tcpaddr;				//address structure for passing to socket functions, used on client side for where to connect, and on server side for where to listen/bind
	struct sockaddr_in   saddr;					//holds ip address that server is listening on 
	struct sockaddr_in   inaddr;				//holds ip address of connected client after accept completes
	int saddr_len;								//holds sizeof saddr
	int inaddr_len;								//holds sizeof inaddr
	HANDLE            hreadThread ;				//handle to read thread
	DWORD            readID;					//read thread ID storage
	DWORD         dwThreadID ;					//temp Thread ID storage
	HANDLE            hwriteThread ;			//handle to write thread
	DWORD            writeID;					//write thread ID storage
	
	
	//if user doesn't give us enough parameters, let them know the usage
	if(argc<2)
	{
		printf("usage:%s sfcport ipaddress [client]\n",argv[0]);
		exit(1);
	}
	//startup winsock
	wVersionRequested = MAKEWORD( 2, 2 );
	
	err = WSAStartup( wVersionRequested, &wsaData );
	if ( err != 0 ) 
	{
		printf("cannot start winsock\r\n");
		exit(1);
	}
	//create the initial socket (THE socket for a client, the socket to listen on for the server)
	m_theSocket = socket( AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (m_theSocket == INVALID_SOCKET)
	{
		printf("cannot get a socket\n");
		exit(1);
	}
	//determine the sfc port number to use and store it.
	sfc_port = atoi(argv[1]);
	SFCToolBox_Create(sfc_port);	//use the toolbox to do a CreateFile on the sfc device 
	
	//determine if we are to be a client or a server
	if(argc>3)
	{
		//we are a client, and we will 
		//try to connect up to a server.
		
		tcpaddr.sin_addr.S_un.S_addr = inet_addr(argv[2]);//get ip address from command line
		tcpaddr.sin_family = AF_INET;
		tcpaddr.sin_port = htons(S_PORT);
		//this next call tries to hook this socket up to the server
		if(connect(m_theSocket,(SOCKADDR*)&tcpaddr,sizeof(tcpaddr))==SOCKET_ERROR)
		{
			printf("error in connect\n");
			exit(1);
		}
		//here we have a connected socket, we can send/receive to/from it now
		//we will do two things as a client, first we will take user keyboard input
		//storing the keystrokes in buf[] until the user hits [Enter].  Once
		//the user hits [Enter] the data in buf[] is sent on to the socket the buffer 
		//is cleared and we start over.
		//Second, if any data comes in from the socket, then we will receive and display it.
		//we continue on until the user presses [ESC] which will terminate the client program.
		
		//note the SFC port isn't really used here, so you could move all of the initialization of the SFC to 
		//the server side and remove the SFCToolBox_Destroy() call at the end of this routine
		
		i=0;		//i is the index into buf[] for the current keystroke
		key = 0;	//has to not be [ESC]
		printf("enter text\n");
		while((key&0xff)!=27)	//loop until user presses [ESC]
		{
			if(kbhit()!=0)		//if a user presses a key
			{
				key = getch();	//get the keystroke
				buf[i]=key;		//store it in our buffer
				i++;			//inc the index
				printf("%c",key);//echo the character to the screen
				
				if(key==0x0d)	//if the key is [Enter] then
				{
					if(send(m_theSocket,buf,i-1,0)==SOCKET_ERROR) //send the data to the socket don't send the [CR]
					{
						printf("error in send\n");
						exit(1);
					}
					i=0;		//reset the index
					printf("\r\nenter text\r\n");//re-display the info mesage
				}
			}
			ioctlsocket(m_theSocket,FIONREAD,&j);//determine if there is data waiting in the socket to be received
			if(j>0)								//there is data to get
			{
				j = recv(m_theSocket,rbuf,sizeof(rbuf),0);//get the data
				printf("received:\r\n");				  //display message
				for(k=0;k<j;k++) printf("%c",rbuf[k]);	  //display data
				printf("\r\n");								
			}
		}//end of while![ESC]
		//we are done here so
		shutdown(m_theSocket,SD_BOTH);	//gracefull shutdown of socket
		closesocket(m_theSocket);		//release socket resources
		WSACleanup();					//done with winsock
		SFCToolBox_Destroy(sfc_port);	//done with SFC port
		exit(0);						//done with program
	}
	else
	{
		//use the SFC port as is, assume it was configured prior to entering this program
		//set our ip info for the server, port and IP mostly
		tcpaddr.sin_family = AF_INET;
		tcpaddr.sin_port = htons(S_PORT);
		tcpaddr.sin_addr.S_un.S_addr = inet_addr(argv[2]);//htonl(INADDR_LOOPBACK);
		//bind to this IP:port
		if (bind(m_theSocket,(SOCKADDR*)&tcpaddr, sizeof(tcpaddr)) == SOCKET_ERROR)
		{
			printf("unable to bind %8.8x\n",WSAGetLastError());
			exit(1);
		}
		else
		{
			//we are bound, now set the state of the socket to listen for incomming connections
			if (listen(m_theSocket, MAX_PENDING_CONNECTS ) == SOCKET_ERROR)
			{
				printf("error while listen\n");
				exit(1);
			}
			//we are entering the main loop of the server
s_top: 
			//this gets our IP address that the server is listening on (yes we allready know it from argv[2])
			saddr_len = sizeof(saddr);
			getsockname(m_theSocket,(SOCKADDR*)&saddr,&saddr_len);
			cb = inet_ntoa(saddr.sin_addr);
			printf("server, listening on : %s\r\n",cb);
			//at this point we are listening...what happens when someone connects?
			inaddr_len = sizeof(inaddr);//set size of inaddr for accept call
			//now we block until someone connects, we do this by calling accept()
			a_theSocket = accept(m_theSocket,(SOCKADDR*)&inaddr,&inaddr_len);
			if(a_theSocket==INVALID_SOCKET)
			{
				printf("error accepting\n");
				exit(1);
			}
			else
			{
				//accept returned, so we have a client connected
				//lets find their ip address
				cb = inet_ntoa(inaddr.sin_addr);
				printf("accepted: %s\r\n",cb);
				//at this point we have a client connected, so fire up the receive/transmit threads
				connected = TRUE;	//enable the read/write threads
				//create a thread for reading from the SFC (writing to the socket)
				hreadThread =     CreateThread( (LPSECURITY_ATTRIBUTES) NULL,
					0, 
					(LPTHREAD_START_ROUTINE) ReadProc,
					(LPVOID) NULL,
					0, &dwThreadID );
				if(hreadThread==NULL)
				{
					//we were unable to make the thread, so 
					//cleanup the mess we have made
					printf("cannot start Data read thread\n\r");
					closesocket(m_theSocket);
					WSACleanup();
					SFCToolBox_Destroy(sfc_port);
					exit(1);
				}
				readID=dwThreadID;//could be used to control the thread from here (currently unused)
				//create a thread for reading from the socket (writing to the SFC)
				hwriteThread =     CreateThread( (LPSECURITY_ATTRIBUTES) NULL,
					0, 
					(LPTHREAD_START_ROUTINE) WriteProc,
					(LPVOID) NULL,
					0, &dwThreadID );
				
				if(hwriteThread==NULL)
				{
					//unable to create the write thread
					//cleanup the mess we have made
					printf("cannot start Data write thread\n\r");
					closesocket(m_theSocket);
					WSACleanup();
					SFCToolBox_Destroy(sfc_port);
					exit(1);
				}
				writeID=dwThreadID;//could be used to control the thread from here (currently unused)
				
				//this part of the code is done, wait for the client to drop (will be detected in the WriteProc())
				//just wait for the connected to be reset
				while(connected==TRUE) Sleep(10);//could make this a bit longer...just increases the latency on re-listening for new clients
				Sleep(1000);//give the ReadProc() time to exit
				if(!kbhit()) goto s_top; //if the user didn't hit a key, then go back to the "waiting for a connection" state
				//the user hit a key...likely that they want to exit (note you have to hit the key in the server "BEFORE" exiting the client or
				//you end up back in the "waiting for client" state.
				
				
				
				shutdown(a_theSocket,SD_BOTH);//done with this clients socket, so close it up
				closesocket(a_theSocket);//release the socket resources
			}//end of accept case
		}//end of bound case
 }//end of server case
 closesocket(m_theSocket);//done with initial socket
 WSACleanup();//done with winsock
 SFCToolBox_Destroy(sfc_port);//done with the SFC port
}//end of main()


//this is the receive thread function.
DWORD FAR PASCAL ReadProc( LPHANDLE lpData )
{
	char buf[4096];//buffer for incomming/outgoing data (can make it bigger for the SFC)
	DWORD rbytes;//number of bytes received
	int i;//return status from SFCToolBox_Read_Frame()
	
	printf("readthread %d\n",sfc_port);//initial message "we are here"
	while(connected==TRUE)//loop as long as connected 
	{
		//get a frame from the SFC, this will timeout every second (return -4) to 
		//let us know that it is doing something
		if((i=SFCToolBox_Read_Frame(sfc_port,&buf[0],sizeof(buf),&rbytes,1000))==0)
		{
			//zero return indicates we have data,
			printf("sfc received %d\n",rbytes);//how much data
			if(send(a_theSocket,buf,rbytes,0)==SOCKET_ERROR)//take that data and shove it at the socket
			{
				//pretty much if the send to the socket failed, we are done, 
				//either the socket is closed, or we have some other error 
				printf("error in socket send :%8.8x\n",WSAGetLastError());
				printf("read thread terminating\n");
				return(FALSE);
			}
		}
		printf("read loop :%d\n",i);//this is the 1 per second "we are alive message"
	}//end of while connected
	printf("read thread terminating\n");//we are done
	return(TRUE);//gone
}//end of ReadProc()

//This is the write thread
DWORD FAR PASCAL WriteProc( LPHANDLE lpData )
{
	int i;//return value from recv()
	char buf[4096];//buffer for data transfer
	
	printf("write proc\n");//"we are here" message
	while(connected)//do as long as connected ==TRUE
	{
		i = recv(a_theSocket,buf,sizeof(buf),0);//blocks until there is data from the socket
		if(i>0) 
		{
			//nonzero positive return means data came in from the socket
			printf("socket received %d \n",i);
			//send it out the SFC port
			//really should check to make sure that i <= rfsizemax (from hdlcset file)
			//or the write will fail with a too big message(invalid parameter?)
			//should also probably check the return value for other errors and not
			//do the infinte while loop it isn't a timeout
			while(SFCToolBox_Write_Frame(sfc_port,buf,i,1000)!=0);
		}
		if(i<=0) 
		{
			//the socket returned without a positive value, so it is either closed
			//or some error occured, either way we are done
			connected=FALSE;//indicate that the threads are going away
			printf("connection closed, write thread terminating\n");//leaving message
			return(FALSE);//gone
		}
	}//end of while connected
	return(TRUE);//gone (I don't think we get here...unless someone outside of this routine makes connected==FALSE)                  
}//end of WriteProc()

