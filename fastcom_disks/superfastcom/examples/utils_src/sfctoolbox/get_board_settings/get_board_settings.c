#include "..\sfctoolbox.h"
#include "windows.h"
#include "stdio.h"
#include "stdlib.h"

void main(int argc,char *argv[])
{
	ULONG i;
	double clock;
	int ret;
	int m,n;
	ULONG clockmode;
	ULONG port;
	ULONG ccr0;
	ULONG ccr1;
	ULONG ccr2;
	ULONG brr;
	ULONG timr;
	ULONG imr;
	ULONG star;
	ULONG lbreg[6];
	ULONG passval[4];
	
	if(argc<1)
	{
		printf("usage:%s\r\n",argv[0]);
		exit(1);
	}
	port = 0;
	while((ret = SFCToolBox_Create(port))>=0)
	{
		printf("SFC%d:\r\n",port);
		printf("-------------------------------------------------------------------------------\r\n");
		clock = (double)SFCToolBox_Get_Clock(port);
		printf("Main OSC frequency:\t%ld\r\n",SFCToolBox_Get_Clock(port));
		printf("ProgClk frequency: \t%ld\r\n",SFCToolBox_Get_ProgClock(port));
		printf("Raw Main Settings:\r\n");
		printf("CCR0:%8.8x\r\n",ccr0=SFCToolBox_Read_Register(port,((port%4)*0x80)+0x108));
		printf("CCR1:%8.8x\r\n",ccr1=SFCToolBox_Read_Register(port,((port%4)*0x80)+0x10C));
		printf("CCR2:%8.8x\r\n",ccr2=SFCToolBox_Read_Register(port,((port%4)*0x80)+0x110));
		printf("BRR: %8.8x\r\n",brr=SFCToolBox_Read_Register(port,((port%4)*0x80)+0x12C));
		printf("TIMR:%8.8x\r\n",timr=SFCToolBox_Read_Register(port,((port%4)*0x80)+0x130));
		printf("STAR:%8.8x\r\n",star=SFCToolBox_Read_Register(port,((port%4)*0x80)+0x104));
		printf("IMR: %8.8x\r\n",imr=SFCToolBox_Read_Register(port,((port%4)*0x80)+0x154));
		printf("Raw Minor Settings:\r\n");
		printf("ACCM:%8.8x\r\n",SFCToolBox_Read_Register(port,((port%4)*0x80)+0x114));
		printf("UDAC:%8.8x\r\n",SFCToolBox_Read_Register(port,((port%4)*0x80)+0x118));
		printf("TTSA:%8.8x\r\n",SFCToolBox_Read_Register(port,((port%4)*0x80)+0x11C));
		printf("RTSA:%8.8x\r\n",SFCToolBox_Read_Register(port,((port%4)*0x80)+0x120));
		printf("PCMT:%8.8x\r\n",SFCToolBox_Read_Register(port,((port%4)*0x80)+0x124));
		printf("PCMR:%8.8x\r\n",SFCToolBox_Read_Register(port,((port%4)*0x80)+0x128));
		printf("XADR:%8.8x\r\n",SFCToolBox_Read_Register(port,((port%4)*0x80)+0x134));
		printf("RADR:%8.8x\r\n",SFCToolBox_Read_Register(port,((port%4)*0x80)+0x138));
		printf("RAMR:%8.8x\r\n",SFCToolBox_Read_Register(port,((port%4)*0x80)+0x13c));
		printf("RLCR:%8.8x\r\n",SFCToolBox_Read_Register(port,((port%4)*0x80)+0x140));
		printf("XNXF:%8.8x\r\n",SFCToolBox_Read_Register(port,((port%4)*0x80)+0x144));
		printf("TCR: %8.8x\r\n",SFCToolBox_Read_Register(port,((port%4)*0x80)+0x148));
		printf("TICR:%8.8x\r\n",SFCToolBox_Read_Register(port,((port%4)*0x80)+0x14C));
		printf("SYNC:%8.8x\r\n",SFCToolBox_Read_Register(port,((port%4)*0x80)+0x150));
		printf("-------------------------------------------------------------------------------\r\n");
		printf("Decoded Settings \r\n");
		printf("-------------------------------------------------------------------------------\r\n");
		printf("CCR0:%8.8x\r\n",ccr0);
		if((ccr0&0x80000000)!=0) printf("Powerup is ON\r\n");
		else printf("Powerup is OFF\r\n");
		if((ccr0&0x30000)==0) printf("HDLC mode selected\r\n");
		if((ccr0&0x30000)==0x20000) printf("BISYNC mode selected\r\n");
		if((ccr0&0x30000)==0x30000) printf("ASYNC mode selected\r\n");
		
		printf("using clock mode: %d",ccr0&0x7);
		clockmode = (ccr0&0x7)*2;
		if((ccr0&0x10)==0x10)
		{
			printf("b\r\n");
			clockmode++;
		}
		else 
		{
			printf("a\r\n");
		}
		
		
		if((ccr0&0x700000)==0x000000) printf("NRZ Line encoding selected\r\n");
		if((ccr0&0x700000)==0x100000) printf("BUS1 Line encoding selected\r\n");
		if((ccr0&0x700000)==0x200000) printf("NRZI Line encoding selected\r\n");
		if((ccr0&0x700000)==0x300000) printf("BUS2 Line encoding selected\r\n");
		if((ccr0&0x700000)==0x400000) printf("FM0 Line encoding selected\r\n");
		if((ccr0&0x700000)==0x500000) printf("FM1 Line encoding selected\r\n");
		if((ccr0&0x700000)==0x600000) printf("Manchester Line encoding selected\r\n");
		
		if((ccr0&0x20)==0x20) printf("txclk configured as output\r\n");
		else printf("txclk configured as input\r\n");
		if((ccr0&0x80)==0x80) printf("16x oversampling enabled\r\n");
		
		m = (brr>>8)&0x0f;
		n = (brr&0x3f);
		
		if((clockmode==0)||(clockmode==4)||(clockmode==8)||(clockmode==9)||(clockmode==12))printf("effective transmit baudrate:EXTERNAL-txclk(ST)\r\n");
		if((clockmode==2)||(clockmode==3)||(clockmode==10)||(clockmode==11))printf("effective transmit baudrate:EXTERNAL-rxclk(RT)\r\n");
		if((clockmode==1)||(clockmode==15))printf("effective transmit baudrate:%lf\r\n",clock/((n+1)*pow(2.0,(double)m)));
		if((clockmode==13))printf("effective transmit baudrate:%lf\r\n",(clock/((n+1)*pow(2.0,(double)m)))/16.0);
		if((clockmode==14))printf("effective transmit baudrate:DPLL-Nominal:%lf\r\n",(clock/((n+1)*pow(2.0,(double)m)))/16.0);
		if((clockmode==6))printf("effective transmit baudrate:DPLL-Nominal: EXTERNAL-rxclk(RT)/%lf\r\n",(((n+1)*pow(2.0,(double)m)))*16.0);
		if((clockmode==5))printf("effective transmit baudrate: EXTERNAL-rxclk(RT)/%lf\r\n",(((n+1)*pow(2.0,(double)m)))*16.0);
		if((clockmode==7))printf("effective transmit baudrate: EXTERNAL-rxclk(RT)/%lf\r\n",((n+1)*pow(2.0,(double)m)));
		
		
		if((clockmode==0)||(clockmode==1)||(clockmode==2)||(clockmode==3)||(clockmode==8)||(clockmode==9)||(clockmode==10)||(clockmode==11))printf("effective receive baudrate:EXTERNAL-rxclk(RT)\r\n");
		if((clockmode==15))printf("effective receive baudrate:%lf\r\n",clock/((n+1)*pow(2.0,(double)m)));
		if((clockmode==12)||(clockmode==13)||(clockmode==14))printf("effective receive baudrate:DPLL-Nominal:%lf\r\n",(clock/((n+1)*pow(2.0,(double)m)))/16.0);
		if((clockmode==4)||(clockmode==5)||(clockmode==6))printf("effective receive baudrate:DPLL-Nominal: EXTERNAL-rxclk(RT)/%lf\r\n",(((n+1)*pow(2.0,(double)m)))*16.0);
		if((clockmode==7))printf("effective receive baudrate: EXTERNAL-rxclk(RT)/%lf\r\n",(((n+1)*pow(2.0,(double)m))));
		
		if((ccr0&0x1000)==0x1000) printf("masked interrupts will be visible\r\n");
		if((ccr0&0x100)==0x100) printf("DPLL phase shift disabled, windows for phase adjustment are extended\r\n");
		if((ccr0&0x8)==0x8) printf("HDLC High Speed mode enabled, (should be using clock mode 4)\r\n");
		
		printf("-------------------------------------------------------------------------------\r\n");
		printf("CCR1:%8.8x\r\n",ccr1);
		
		if((ccr0&0x30000)==0)
		{
			//	printf("HDLC mode selected\r\n");
			if((ccr1&0x30000000)==0) printf("RTS output active during frame transmission\r\n");
			if((ccr1&0x30000000)==0x20000000) printf("RTS allways inactive (high)\r\n");
			if((ccr1&0x30000000)==0x30000000) printf("RTS output is active during frame reception\r\n");
			if((ccr1&0x200000)==0x200000) printf("internal txclock is output on RTS (HighSpeed mode only!)\r\n");
			if((ccr1&0xe000)==0) printf("automode, 8 bit address field;\r\n frame structure:(0x7E,address,control,data[n],crc,crc,0x7E)\r\n");
			if((ccr1&0xe000)==0x2000) printf("automode, 16 bit address field;\r\n frame structure:(0x7E,address,address,control,data[n],crc,crc,0x7E)\r\n");
			if((ccr1&0xe000)==0x4000) printf("non-automode, 8 bit address field;\r\n frame structure:(0x7E,address,data[n],crc,crc,0x7E)\r\n");
			if((ccr1&0xe000)==0x6000) printf("non-automode, 16 bit address field;\r\n frame structure:(0x7E,address,address,data[n],crc,crc,0x7E)\r\n");
			if((ccr1&0xe000)==0x8000) printf("address mode 0 (hdlc transparent mode 0), no address recognition;\r\n frame structure:(0x7E,data[n],crc,crc,0x7E)\r\n");
			if((ccr1&0xe000)==0xa000) printf("address mode 1 (hdlc transparent mode 1), 8 bit address recognition;\r\n frame structure:(0x7E,address,data[n],crc,crc,0x7E)\r\n");
			if((ccr1&0xe000)==0xc000)
			{
				printf("Extended Transparent HDLC mode (raw bits)\r\n (you should really set ADM=1);\r\n frame structure:(data[n]) \r\n");
				printf("\r\n20534 glitch alert!\r\n");
				printf("you must successfully initialize the port in clock mode 1\r\n");
				printf("before data reception will occur\r\n");
				printf("you can change clock modes once it has been initialized,\r\n");
				printf("but to successfully configure the port in clock mode 1\r\n");
				printf("you must have an external clock source on RT+/- during initialization!!!\r\n\r\n");
			}
			if((ccr1&0xe000)==0xe000)
			{
				printf("Extended Transparent HDLC mode (raw bits);\r\n frame structure:(data[n]) \r\n");
				printf("\r\n20534 glitch alert!\r\n");
				printf("you must successfully initialize the port in clock mode 1\r\n");
				printf("before data reception will occur\r\n");
				printf("you can change clock modes once it has been initialized,\r\n");
				printf("but to successfully configure the port in clock mode 1\r\n");
				printf("you must have an external clock source on RT+/- during initialization!!!\r\n\r\n");
			}
			if((ccr1&0x1000)==0x1000) printf("NRM mode selected (only pertinent in automodes)\r\n");
			if((ccr1&0xc00)==0x400) printf("octet synchronous PPP mode\r\n");
			if((ccr1&0xc00)==0x800) printf("asynchronus PPP mode\r\n");
			if((ccr1&0xc00)==0xc00) printf("bit synchronous PPP mode\r\n");
			if((ccr1&0x200)==0x200) printf("two byte control field (for modes with control fields)\r\n");
			else printf("one byte control field (for modes with control fields)\r\n");
			if((ccr1&0x80)==0x80) printf("shared flags enabled (closing flag can be opening flag of back to back frames),pertinent to tx only (rx allways has shared flags enabled)\r\n");
			if((ccr1&0x2)==0) printf("CRC reset level 0xFFFF(FFFF)\r\n");
			else printf("CRC reset level 0x0000(0000)\r\n");
			if((ccr1&1)==0) printf("CRC-CCITT selected\r\n");
			else printf("CRC32 selected\r\n");
			
		}
		if((ccr0&0x30000)==0x20000)
		{
			//	printf("BISYNC mode selected\r\n");
			if((ccr1&0x400)==0x400)
			{
				if((ccr1&0x800)==0x800) printf("16 bit BISYNC\r\n");
				else printf("12 bit BISYNC\r\n");
			}
			else
			{
				if((ccr1&0x800)==0x800) printf("8 bit MONOSYNC\r\n");
				else printf("6 bit MONOSYNC\r\n");
			}
		}
		if((ccr0&0x30000)==0x30000)
		{
			//	printf("ASYNC mode selected\r\n");
			if((ccr1&0x80)==0x80) printf("Automatic timeout processing enabled: timeout=%d character times\r\n",((ccr1&0x7f)+1)*4);
			else printf("no automatic timeout processing\r\n");
		}
		if((ccr1&0x4000000)==0x4000000) printf("data is received/transmitted inverted (must be NRZ encoding)\r\n");
		if((ccr1&0x2000000)==0x2000000) printf("TXD is push-pull (required!)\r\n");
		else printf("TXD is open drain (BAD!!!)\r\n");
		if((ccr1&0x400000)==0x400000) printf("CD is active LOW\r\n");
		else printf("CD is active HIGH\r\n");
		if((ccr1&0x180000)==0x000000) printf("auto RTS, active while transmitting\r\n");
		if((ccr1&0x180000)==0x100000) printf("auto RTS, flow control, active when SCC fifo is empty\r\n");
		if((ccr1&0x180000)==0x080000) printf("Force RTS active \r\n");
		if((ccr1&0x180000)==0x180000) printf("Force RTS inactive\r\n");
		
		if((ccr1&0x40000)==0x40000) printf("CTS signal ignored\r\n");
		else printf("CTS controls transmitter, transmit stops if CTS is inactive\r\n");
		
		if((ccr1&0x20000)==0x20000) printf("CD pin enables/disables receiver (carrier detect auto start)\r\n");
		else printf("CD is a digital input \r\n");
		if((ccr1&0x100)==0x100) printf("Testloop active (on chip loopback enabled)\r\n");
		
		printf("-------------------------------------------------------------------------------\r\n");
		printf("CCR2:%8.8x\r\n",ccr2);
		
		if((ccr0&0x30000)==0)
		{
			//	printf("HDLC mode selected\r\n");
			if((ccr2&0x400000)==0x400000) printf("CRC not enabled/checked\r\n");
			else printf("Hardware CRC checking enabled\r\n");
			if((ccr2&0x200000)==0x200000) printf("CRC bits pushed into receive FIFO (CRC bits received as data)\r\n");
			else printf("CRC bits not received as data\r\n");
			if((ccr2&0x100000)==0x100000) printf("Receive address bytes pushed into receive fifo (in applicable modes)\r\n");
			else printf("Receive address bytes stripped from receive stream (in applicable modes)\r\n");
			if((ccr2&0x80)==0x80) 
			{
				if(((ccr2>>4)&3)==0)printf("preamble enabled, preamble byte:%2.2x, 1 repetition\r\n",(ccr2&0xff00)>>8);
				if(((ccr2>>4)&3)==1)printf("preamble enabled, preamble byte:%2.2x, 2 repetition\r\n",(ccr2&0xff00)>>8);
				if(((ccr2>>4)&3)==2)printf("preamble enabled, preamble byte:%2.2x, 4 repetition\r\n",(ccr2&0xff00)>>8);
				if(((ccr2>>4)&3)==3)printf("preamble enabled, preamble byte:%2.2x, 8 repetition\r\n",(ccr2&0xff00)>>8);
			}
			else printf("no preamble\r\n");
			if((ccr2&0x8)==8) printf("IDLE FLAG sequences (0x7E)\r\n");
			else printf("IDLE 1's (0xff)\r\n");
			if((ccr2&0x4)==4) printf("Transmit I frames (automode)\r\n");
			else printf("Transmit U frames (automode)\r\n");
			if((ccr2&0x2)==2) printf("One insertion enabled\r\n(transmit a 1 after 7 consecutive 0's,\r\nreceiver strips a 1 after 7 consecutive 0's)\r\n");
			if((ccr2&0x1)==1) printf("Hardware CRC generation disabled, last 2/4 data bytes of frame will be the CRC\r\n");
			else printf("Hardware CRC generation (transmit) enabled, CRC will automatically be appended\r\n");
			if((ccr2&0x80000)==0x80000)
			{
				if((ccr2&0x70000)==0x0)	printf("two byte threshold enabled\r\n");
			}
			
			
		}
		if((ccr0&0x30000)==0x20000)
		{
			//	printf("BISYNC mode selected\r\n");
			if(((ccr2>>28)&3)==0) printf("8 bit data\r\n");
			if(((ccr2>>28)&3)==1) printf("7 bit data\r\n");
			if(((ccr2>>28)&3)==2) printf("6 bit data\r\n");
			if(((ccr2>>28)&3)==3) printf("5 bit data\r\n");
			if((ccr2&0x1000000)==0x1000000) printf("SYN characters received as data\r\n");
			else printf("SYN characters stripped from received data\r\n");
			if((ccr2&0x200000)==0x200000) 
			{
				if(((ccr2>>22)&3)==0) printf("Space parity (0)\r\n");
				if(((ccr2>>22)&3)==1) printf("Odd parity\r\n");
				if(((ccr2>>22)&3)==2) printf("Even parity\r\n");
				if(((ccr2>>22)&3)==3) printf("Mark parity (1)\r\n");
				if((ccr2&0x100000)==0x100000) printf("parity not stored in data byte\r\n");
			}
			else printf("parity generation/checking disabled\r\n");
			if((ccr2&0x80)==0x80) 
			{
				if(((ccr2>>4)&3)==0)printf("preamble enabled, preamble byte:%2.2x, 1 repetition\r\n",(ccr2&0xff00)>>8);
				if(((ccr2>>4)&3)==1)printf("preamble enabled, preamble byte:%2.2x, 2 repetition\r\n",(ccr2&0xff00)>>8);
				if(((ccr2>>4)&3)==2)printf("preamble enabled, preamble byte:%2.2x, 4 repetition\r\n",(ccr2&0xff00)>>8);
				if(((ccr2>>4)&3)==3)printf("preamble enabled, preamble byte:%2.2x, 8 repetition\r\n",(ccr2&0xff00)>>8);
			}
			if((ccr2&0x8)==8) printf("IDLE SYN sequences \r\n");
			else printf("IDLE 1's (0xff)\r\n");
			if((ccr2&0x4)==0) printf("CRC reset level 0xFFFF(FFFF)\r\n");
			else printf("CRC reset level 0x0000(0000)\r\n");
			if((ccr2&0x2)==2) printf("CRC byte(s) automatically appended to the end of each transmit\r\n");
			else printf("no transmit hardware CRC checking performed\r\n");
			if((ccr2&1)==1) printf("CRC-CCITT polynomial selected (x^16 + x^12 + x^5 + 1)\r\n");
			else printf("CRC-16 polynomial selected  (x^16 + x^15 + x^2 + 1)\r\n");
			if((ccr2&0x80000)==0x80000) printf("data & status stored in fifo\r\n");
			else printf("just data bytes stored in fifo\r\n");
		}
		if((ccr0&0x30000)==0x30000) 
		{
			//	printf("ASYNC mode selected\r\n");
			if(((ccr2>>28)&3)==0) printf("8 bit data\r\n");
			if(((ccr2>>28)&3)==1) printf("7 bit data\r\n");
			if(((ccr2>>28)&3)==2) printf("6 bit data\r\n");
			if(((ccr2>>28)&3)==3) printf("5 bit data\r\n");
			if((ccr2&0x1000000)==0x1000000) printf("two stop bits\r\n");
			else printf("one stop bit\r\n");
			if((ccr2&0x200000)==0x200000) 
			{
				if(((ccr2>>22)&3)==0) printf("Space parity (0)\r\n");
				if(((ccr2>>22)&3)==1) printf("Odd parity\r\n");
				if(((ccr2>>22)&3)==2) printf("Even parity\r\n");
				if(((ccr2>>22)&3)==3) printf("Mark parity (1)\r\n");
				if((ccr2&0x100000)==0x100000) printf("parity not stored in data byte\r\n");
			}
			else printf("parity generation/checking disabled\r\n");
			
			if((ccr2&0x4000000)==0x4000000) printf("XON/XOFF characters filtered out (not received)\r\n");
			else printf("all characters received (including XON/XOFF)\r\n");
			if((ccr2&0x2000000)==0x2000000) printf("BREAK is being transmitted\r\n");
			if((ccr2&1)==1) printf("XON/OXFF enabled\r\n");
			else printf("XON/XOFF disabled\r\n");
			if((ccr2&0x80000)==0x80000) printf("data & status stored in fifo\r\n");
			else printf("just data bytes stored in fifo\r\n");
			
		}
		if((ccr2&0x8000000)==0x8000000) printf("receiver is enabled\r\n");
		else printf("receiver is disabled\r\n");
		if((ccr2&0x70000)==0) printf("receive threshold = 1\r\n");
		if((ccr2&0x70000)==0x10000) printf("receive threshold = 4\r\n");
		if((ccr2&0x70000)==0x20000) printf("receive threshold = 16\r\n");
		if((ccr2&0x70000)==0x30000) printf("receive threshold = 24\r\n");
		if((ccr2&0x70000)==0x40000) printf("receive threshold = 32\r\n");
		if((ccr2&0x70000)==0x50000) printf("receive threshold = 60\r\n");
		if((ccr2&0x70000)==0x60000) printf("receive threshold = 1\r\n");
		if((ccr2&0x70000)==0x70000) printf("receive threshold = 1\r\n");
		
		
		printf("-------------------------------------------------------------------------------\r\n");
		
		for(i=0;i<6;i++) lbreg[i] = SFCToolBox_Read_Local_Register(port,i);
		i = lbreg[0] + (lbreg[1]<<8);
		i = (i>>((port%4)*4))&0x0f;
		if((i&1)==0) printf("Receive Echo cancel Enabled\r\n");
		else printf("Receive Echo Cancel Disabled\r\n");
		if((i&2)==0) printf("SD RS-485 Enabled\r\n");
		else printf("SD RS-485 Disabled\r\n");
		if((i&4)==0) printf("TT RS-485 Enabled\r\n");
		else printf("TT RS-485 Disabled\r\n");
		
		i = lbreg[2]; 
		i = (i>>((port%4)*2))&0x0f;
		if((i&1)==1) printf("ST not connected to txclk\r\n");
		else printf("ST connected to txclk\r\n");
		if((i&2)==2) printf("TT not connected to txclk\r\n");
		else printf("TT connected to txclk\r\n");
		
		
		if((lbreg[3]==3)&&(lbreg[4]==4))
		{
			if((lbreg[5]&4)==0)	printf("SuperFastcom:422/4-PCI detected\r\n");
		}
		else
		{
			if((lbreg[5]&4)==4) printf("SuperFastcom:232/4-PCI detected\r\n");
		}
		if((lbreg[5]&4)==0)
		{//is RS232
			printf("\t{\r\n");
			i = lbreg[3] + (lbreg[4]<<8);
			i = (i>>((port%4)*4))&0x0f;
			if((i&1)==1) printf("\tRT Inverted\r\n");
			else printf("\tRT Normal\r\n");
			if((i&2)==2) printf("\tTT Inverted\r\n");
			else printf("\tTT Normal\r\n");
			if((i&4)==4) printf("\tST Inverted\r\n");
			else printf("\tST Normal\r\n");
			printf("\t}\r\n");
		}
		if((lbreg[5]&2)==2) printf("ICS307-2 Clock generators detected\r\nMinimum Clock setting 6MHz, Maximum 33MHz\r\n");
		if((lbreg[5]&1)==1) printf("ICD2053B Clock generators detected\r\nMinimum Clock setting 1MHz, Maximum 33MHz\r\n");
		
		SFCToolBox_Get_Buffering(port,&passval[0]);
		printf("Driver Buffer Settings:\r\n");
		printf("#receive  buffers: %d @ %d bytes each\r\n",passval[0],passval[1]);
		printf("#transmit buffers: %d @ %d bytes each\r\n",passval[2],passval[3]);
		
		SFCToolBox_Destroy(port);
		port++;
		printf("-------------------------------------------------------------------------------\r\n");
}//end while

}//end main