#include "windows.h"
#include "stdlib.h"
#include "stdio.h"
#include "conio.h"
#include "../sfctoolbox.h"


int main(int argc,char *argv[])
{
ULONG port,frequency,clocktype,actual;
int ret;
if(argc<3)
	{
	printf("usage:\r\n%s port frequency [type]\r\n");
	exit(1);
	}
clocktype = AUTOCLOCK;
if(argc>3)	
	{
	clocktype = atoi(argv[3]);
	if(clocktype==ICD2053B)     printf("Autoclock override: ICD2053B selected\r\n");
	else if(clocktype==FS6131)  printf("Autoclock override: FS6131-01 selected\r\n");
	else if(clocktype==ICS307)  printf("Autoclock override: ICS307-2 selected\r\n");
	else
		{
		printf("Autoclock override: Autoclock selected\r\n");
		clocktype = AUTOCLOCK;
		}
	}
port = atoi(argv[1]);
frequency = atol(argv[2]);

if((ret = SFCToolBox_Create(port))<0)
	{
	printf("create:%d\n",ret);
	exit(1);
	}
actual=SFCToolBox_Set_Clock(port,frequency,clocktype);

if((long int)actual<0)
{
printf("Error setting clock!, frequency %ld not set!!!\r\n",frequency);
exit(1);
}
else printf("Port %d clock set to %d \r\n",port,actual);
SFCToolBox_Destroy(port);
return 0;
}
