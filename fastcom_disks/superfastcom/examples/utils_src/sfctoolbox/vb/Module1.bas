Attribute VB_Name = "Module1"
Public Type SCCREGS
cmdr As Long
star As Long
ccr0 As Long
ccr1 As Long
ccr2 As Long
accm As Long
udac As Long
ttsa As Long
rtsa As Long
pcmmtx As Long
pcmmrx As Long
brr As Long
timr As Long
xadr As Long
radr As Long
ramr As Long
rlcr As Long
xnxfr As Long
tcr As Long
ticr As Long
syncr As Long
imr As Long
isr As Long
gap1 As Long
gap2 As Long
gap3 As Long
gap4 As Long
gap5 As Long
gap6 As Long
gap7 As Long
gap8 As Long
gap9 As Long
End Type


Public Declare Function SFCToolBox_Create Lib "sfctoolbox.dll" Alias "#3" (ByVal Port As Long) As Long
Public Declare Function SFCToolBox_Destroy Lib "sfctoolbox.dll" Alias "#4" (ByVal Port As Long) As Long

Public Declare Function SFCToolBox_Set_Clock Lib "sfctoolbox.dll" Alias "#24" (ByVal Port As Long, ByVal frequency As Long, ByVal boardtype As Long) As Long
Public Declare Function SFCToolBox_Get_Clock Lib "sfctoolbox.dll" Alias "#8" (ByVal Port As Long) As Long

Public Declare Function SFCToolBox_Set_ProgClock Lib "sfctoolbox.dll" Alias "#25" (ByVal Port As Long, ByVal frequency As Long, ByVal boardtype As Long) As Long
Public Declare Function SFCToolBox_Get_ProgClock Lib "sfctoolbox.dll" Alias "#10" (ByVal Port As Long) As Long

Public Declare Function SFCToolBox_Read_Frame Lib "sfctoolbox.dll" Alias "#17" (ByVal Port As Long, ByRef rbuf As Byte, ByVal szrbuf As Long, ByRef retbytes As Long, ByVal timeout As Long) As Long
Public Declare Function SFCToolBox_Write_Frame Lib "sfctoolbox.dll" Alias "#34" (ByVal Port As Long, ByRef tbuf As Byte, ByVal numbytes As Long, ByVal timeout As Long) As Long
Public Declare Function SFCToolBox_Get_Status Lib "sfctoolbox.dll" Alias "#11" (ByVal Port As Long, ByRef status As Long, ByVal mask As Long, ByVal timeout As Long) As Long
Public Declare Function SFCToolBox_Get_IStatus Lib "sfctoolbox.dll" Alias "#9" (ByVal Port As Long, ByRef status As Long) As Long

Public Declare Function SFCToolBox_Register_Read_Callback Lib "sfctoolbox.dll" Alias "#20" (ByVal Port As Long, ByVal ReadCallbackHandler As Long, ByVal szread As Long, ByRef data As Byte) As Long
Public Declare Function SFCToolBox_Register_Write_Callback Lib "sfctoolbox.dll" Alias "#22" (ByVal Port As Long, ByVal WriteCallbackHandler As Long, ByVal szmaxwrite As Long) As Long
Public Declare Function SFCToolBox_Register_Status_Callback Lib "sfctoolbox.dll" Alias "#21" (ByVal Port As Long, ByVal mask As Long, ByVal StatusCallbackHandler As Long) As Long

Public Declare Function SFCToolBox_Flush_RX Lib "sfctoolbox.dll" Alias "#5" (ByVal Port As Long) As Long
Public Declare Function SFCToolBox_Flush_TX Lib "sfctoolbox.dll" Alias "#6" (ByVal Port As Long) As Long

Public Declare Function SFCToolBox_Read_Register Lib "sfctoolbox.dll" Alias "#19" (ByVal Port As Long, ByVal regno As Long) As Long
Public Declare Function SFCToolBox_Write_Register Lib "sfctoolbox.dll" Alias "#36" (ByVal Port As Long, ByVal regno As Long, ByVal value As Long) As Long

Public Declare Function SFCToolBox_Read_Local_Register Lib "sfctoolbox.dll" Alias "#18" (ByVal Port As Long, ByVal regno As Long) As Long
Public Declare Function SFCToolBox_Write_Local_Register Lib "sfctoolbox.dll" Alias "#35" (ByVal Port As Long, ByVal regno As Long, ByVal value As Long) As Long

Public Declare Function SFCToolBox_Configure Lib "sfctoolbox.dll" Alias "#2" (ByVal Port As Long, ByRef settings As SCCREGS) As Long

Public Declare Function SFCToolBox_Set_Txclk_TT Lib "sfctoolbox.dll" Alias "#33" (ByVal Port As Long, ByVal onoff As Long) As Long
Public Declare Function SFCToolBox_Set_Txclk_ST Lib "sfctoolbox.dll" Alias "#32" (ByVal Port As Long, ByVal onoff As Long) As Long
Public Declare Function SFCToolBox_Set_SD_485 Lib "sfctoolbox.dll" Alias "#29" (ByVal Port As Long, ByVal onoff As Long) As Long
Public Declare Function SFCToolBox_Set_TT_485 Lib "sfctoolbox.dll" Alias "#30" (ByVal Port As Long, ByVal onoff As Long) As Long
Public Declare Function SFCToolBox_Set_RD_Echo_Cancel Lib "sfctoolbox.dll" Alias "#26" (ByVal Port As Long, ByVal onoff As Long) As Long

Public Declare Function SFCToolBox_Invert_RT Lib "sfctoolbox.dll" Alias "#12" (ByVal Port As Long, ByVal onoff As Long) As Long
Public Declare Function SFCToolBox_Invert_ST Lib "sfctoolbox.dll" Alias "#13" (ByVal Port As Long, ByVal onoff As Long) As Long
Public Declare Function SFCToolBox_Invert_TT Lib "sfctoolbox.dll" Alias "#14" (ByVal Port As Long, ByVal onoff As Long) As Long

Public Declare Function SFCToolBox_Set_Check_Timeout Lib "sfctoolbox.dll" Alias "#23" (ByVal Port As Long, ByVal miliseconds As Long) As Long
Public Declare Function SFCToolBox_Set_RX_Irq_Rate Lib "sfctoolbox.dll" Alias "#27" (ByVal Port As Long, ByVal descriptorsperirq As Long) As Long
Public Declare Function SFCToolBox_Set_TX_Irq_Rate Lib "sfctoolbox.dll" Alias "#31" (ByVal Port As Long, ByVal descriptorsperirq As Long) As Long

Public Declare Function SFCToolBox_Mask_RX_Frame_End Lib "sfctoolbox.dll" Alias "#15" (ByVal Port As Long, ByVal onoff As Long) As Long
Public Declare Function SFCToolBox_Mask_TX_Frame_End Lib "sfctoolbox.dll" Alias "#16" (ByVal Port As Long, ByVal onoff As Long) As Long

Public Declare Function SFCToolBox_Set_RX_Multiple Lib "sfctoolbox.dll" Alias "#28" (ByVal Port As Long, ByVal framesperread As Long) As Long


