VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   3675
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6120
   LinkTopic       =   "Form1"
   ScaleHeight     =   3675
   ScaleWidth      =   6120
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Receivedata 
      Caption         =   "Receive data"
      Height          =   375
      Left            =   2880
      TabIndex        =   11
      Top             =   2160
      Width           =   1455
   End
   Begin VB.CommandButton sendit 
      Caption         =   "Send data"
      Height          =   375
      Left            =   2880
      TabIndex        =   10
      Top             =   840
      Width           =   1455
   End
   Begin VB.CommandButton Command1 
      Caption         =   "stop/exit"
      Height          =   492
      Left            =   4440
      TabIndex        =   9
      Top             =   2520
      Width           =   1452
   End
   Begin VB.TextBox status 
      Height          =   372
      Left            =   1080
      TabIndex        =   7
      Top             =   3240
      Width           =   4932
   End
   Begin VB.CommandButton Start 
      Caption         =   "Start"
      Height          =   492
      Left            =   4440
      TabIndex        =   6
      Top             =   1920
      Width           =   1452
   End
   Begin VB.TextBox Port 
      Height          =   372
      Left            =   3960
      TabIndex        =   2
      Text            =   "0"
      Top             =   240
      Width           =   1092
   End
   Begin VB.TextBox txdata 
      Height          =   972
      Left            =   600
      MultiLine       =   -1  'True
      TabIndex        =   1
      Top             =   720
      Width           =   2172
   End
   Begin VB.TextBox rxdata 
      Height          =   972
      Left            =   600
      MultiLine       =   -1  'True
      TabIndex        =   0
      Top             =   2160
      Width           =   2172
   End
   Begin VB.Label Label4 
      Caption         =   "Status"
      Height          =   252
      Left            =   0
      TabIndex        =   8
      Top             =   3240
      Width           =   852
   End
   Begin VB.Label Label3 
      Caption         =   "RX"
      Height          =   252
      Left            =   120
      TabIndex        =   5
      Top             =   1920
      Width           =   372
   End
   Begin VB.Label Label2 
      Caption         =   "TX"
      Height          =   252
      Left            =   120
      TabIndex        =   4
      Top             =   360
      Width           =   372
   End
   Begin VB.Label Label1 
      Caption         =   "Port"
      Height          =   252
      Left            =   3240
      TabIndex        =   3
      Top             =   240
      Width           =   492
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim portnum As Long


Private Sub Command1_Click()
Dim ret As Long
ret = SFCToolBox_Destroy(portnum)
End Sub

Private Sub Form_Load()
portnum = 0
End Sub

Private Sub Form_Terminate()
ret = SFCToolBox_Destroy(portnum)
End Sub

Private Sub Port_Change()
portnum = Port.Text

End Sub

Private Sub Receivedata_Click()
Dim ret As Long
Dim i As Long
Dim data(4096) As Byte
Dim retbytes As Long
Dim s As String

ret = SFCToolBox_Read_Frame(portnum, data(1), 4096, retbytes, 1000)
If (ret = 0) Then
status.Text = "RX OK"
s = retbytes
rxdata.Text = "received " + s + " bytes "
For i = 1 To retbytes Step 1
s = data(i)
rxdata.Text = rxdata.Text + ":" + s
Next
End If
If (ret < 0) Then
status.Text = "RX Failed" + Str(ret)
End If

End Sub

Private Sub sendit_Click()
Dim ret As Long
Dim i As Long
Dim data(1024) As Byte

For i = 1 To Len(txdata) Step 1
data(i) = Asc(Mid(txdata, i, 1))
Next

ret = SFCToolBox_Write_Frame(portnum, data(1), Len(txdata.Text), 1000)
If (ret = 0) Then
status.Text = "TX OK"
End If
If (ret < 0) Then
status.Text = "TX Failed" + Str(ret)
End If

End Sub

Private Sub Start_Click()
Dim settings As SCCREGS
Dim ret As Long
ret = SFCToolBox_Create(portnum)
If (ret < 0) Then
status.Text = "Create Error: " + Str(ret)
MsgBox ("ERR")
End If
ret = SFCToolBox_Set_Clock(portnum / 4, 2000000, 1)
If (ret < 0) Then
status.Text = "SetClock Error: " + Str(ret)
MsgBox ("ERR")
End If

        settings.cmdr = &H1010000
        settings.star = &H0
        settings.ccr0 = &H80000030
        settings.ccr1 = &H2048000
        settings.ccr2 = &H8040000
        settings.imr = &HFFFEFFFF

        settings.accm = 0
        settings.udac = 0
        settings.ttsa = 0
        settings.rtsa = 0
        settings.pcmmtx = 0
        settings.pcmmrx = 0
        settings.brr = 0
        settings.timr = 0
        settings.xadr = 0
        settings.radr = 0
        settings.ramr = 0
        settings.rlcr = 0
        settings.xnxfr = 0
        settings.tcr = 0
        settings.ticr = 0
        settings.syncr = 0
        settings.isr = 0



ret = SFCToolBox_Configure(portnum, settings)
If (ret < 0) Then
status.Text = "Configure Error: " + Str(ret)
MsgBox ("ERR")
End If

ret = SFCToolBox_Flush_RX(portnum)
ret = SFCToolBox_Flush_TX(portnum)


Rem this is broken, doesn't work, if you know how to make it work please contact me!
Rem ret = SFCToolBox_Register_Read_Callback(portnum, AddressOf ReadCallbackFunction, 1024, rdata(1))
Rem If (ret < 0) Then
Rem status.Text = "Unable to register read callback: " + Str(ret)
Rem MsgBox ("ERR")
Rem End If


End Sub

