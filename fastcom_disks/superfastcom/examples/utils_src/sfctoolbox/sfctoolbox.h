
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the SFCTOOLBOX_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// SFCTOOLBOX_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef SFCTOOLBOX_EXPORTS
#define SFCTOOLBOX_API __declspec(dllexport)
#else
#define SFCTOOLBOX_API __declspec(dllimport)
#endif

#include "../sfc.h"

#define BOARD_UNKNOWN				0
#define BOARD_SUPERFASTCOM			1
#define BOARD_XSA_104				2
#define BOARD_SUPERFASTCOM_1_104	3	
#define BOARD_SUPERFASTCOM_2_104	4
#define BOARD_SUPERFASTCOM_232		5

#define AUTOCLOCK	0
#define ICD2053B	1
#define ICS307		2
#define FS6131		3

#define MAXPORTS 80

#ifdef __cplusplus
extern "C" 
{
#endif
typedef void (__stdcall *readcallbackfn)(DWORD port,char *rdata,DWORD retbytes);

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Create(DWORD port);
SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Destroy(DWORD port);

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Set_Clock(DWORD port,DWORD frequency,DWORD clocktype);
SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Get_Clock(DWORD port);

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Set_ProgClock(DWORD port,DWORD frequency,DWORD clocktype);
SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Get_ProgClock(DWORD port);

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Read_Frame(DWORD port,char * rbuf,DWORD szrbuf,DWORD *retbytes,DWORD timeout);
SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Write_Frame(DWORD port,char * tbuf,DWORD numbytes,DWORD timeout);
SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Get_Status(DWORD port,DWORD *status,DWORD mask,DWORD timeout);
SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Get_IStatus(DWORD port,DWORD *status);

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Register_Read_Callback(DWORD port,readcallbackfn fnptr,DWORD szread);
SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Register_Write_Callback(DWORD port,void (__stdcall *callbackfunction)(DWORD port,char * tbuf,DWORD *bytestowrite),DWORD szmaxwrite);
SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Register_Status_Callback(DWORD port,DWORD mask,void (__stdcall *callbackfunction)(DWORD port,DWORD status));


SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Flush_RX(DWORD port);
SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Flush_TX(DWORD port);

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Read_Register(DWORD port,DWORD regno);
SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Write_Register(DWORD port,DWORD regno,DWORD value);

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Read_Local_Register(DWORD port,DWORD regno);
SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Write_Local_Register(DWORD port,DWORD regno,DWORD value);

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Configure(DWORD port,SCC_REGS *settings);

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Set_Txclk_TT(DWORD port,DWORD onoff);
SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Set_Txclk_ST(DWORD port,DWORD onoff);
SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Set_SD_485(DWORD port,DWORD onoff);
SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Set_TT_485(DWORD port,DWORD onoff);
SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Set_RD_Echo_Cancel(DWORD port,DWORD onoff);
SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Invert_RT(DWORD port,DWORD onoff);
SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Invert_ST(DWORD port,DWORD onoff);
SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Invert_TT(DWORD port,DWORD onoff);

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Set_Check_Timeout(DWORD port,DWORD miliseconds);
SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Set_RX_Irq_Rate(DWORD port,DWORD descriptorsperirq);
SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Set_TX_Irq_Rate(DWORD port,DWORD descriptorsperirq);
SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Mask_RX_Frame_End(DWORD port,DWORD onoff);
SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Mask_TX_Frame_End(DWORD port,DWORD onoff);
SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Set_RX_Multiple(DWORD port,DWORD framesperread);

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Block_Multiple_Io(DWORD port,DWORD onoff);
SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Get_Buffering(DWORD port,ULONG *passval);
#ifdef __cplusplus
}
#endif


