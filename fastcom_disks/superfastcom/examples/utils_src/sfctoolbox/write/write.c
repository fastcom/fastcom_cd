#include "..\sfctoolbox.h"
#include "windows.h"
#include "stdio.h"
#include "stdlib.h"


void main(int argc,char *argv[])
{
int ret;
ULONG port;
char buf[4096];
ULONG count;

if(argc<2)
	{
	printf("usage:%s port\r\n",argv[0]);
	exit(1);
	}
port = atoi(argv[1]);
if((ret = SFCToolBox_Create(port))<0)
	{
	printf("create:%d\n",ret);
	exit(0);
	}
printf("Enter text to send:\n");
scanf("%s",buf);
count = strlen(buf);
if((ret = SFCToolBox_Write_Frame(port,buf,count,1000))<0) printf("write:%d\n",ret);
SFCToolBox_Destroy(port);
}

