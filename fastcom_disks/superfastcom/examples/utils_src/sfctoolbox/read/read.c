#include "..\sfctoolbox.h"
#include "windows.h"
#include "stdio.h"
#include "stdlib.h"

void __stdcall sfcreadcb(DWORD port,char * rbuf,DWORD retbytes);

void main(int argc,char *argv[])
{
int ret;
ULONG port;
if(argc<2)
	{
	printf("usage:%s port\r\n",argv[0]);
	exit(1);
	}
port = atoi(argv[1]);
if((ret = SFCToolBox_Create(port))<0) 
{
	printf("create:%d\n",ret);
	exit(0);
}

SFCToolBox_Register_Read_Callback(port,sfcreadcb,4096);
printf("paused...any key to exit\n");
getchar();
SFCToolBox_Destroy(port);
}

void __stdcall sfcreadcb(DWORD port,char * rbuf,DWORD retbytes)
{
ULONG i;
printf("return:%d\n",retbytes);
for(i=0;i<retbytes;i++) printf("%x,",rbuf[i]&0xff);
printf("\r\n");
}