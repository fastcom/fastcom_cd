#include "..\sfctoolbox.h"
#include "windows.h"
#include "stdio.h"
#include "stdlib.h"

void __stdcall sfcstatuscb(DWORD port,DWORD status);

void main(int argc,char *argv[])
{
int ret;
ULONG port;

if(argc<2)
	{
	printf("usage:%s port\r\n",argv[0]);
	exit(1);
	}
port = atoi(argv[1]);
if((ret = SFCToolBox_Create(port))<0) printf("create:%d\n",ret);
SFCToolBox_Register_Status_Callback(port,0xFFFFFFFF,sfcstatuscb);
printf("paused...any key to exit\n");
getchar();
SFCToolBox_Destroy(port);
}

void __stdcall sfcstatuscb(DWORD port,DWORD status)
{
if((status&ST_HI)==ST_HI)		printf("ST_HI	--Host Initiated interrupt\n");
if((status&ST_FI)==ST_FI)		printf("ST_FI	--Frame Indication interrupt\n");
if((status&ST_ERR)==ST_ERR)		printf("ST_ERR	--Error indication interrupt\n");
if((status&ST_ALLS)==ST_ALLS)	printf("ST_ALLS	--All Sent interrupt\n");
if((status&ST_XDU)==ST_XDU)		printf("ST_XDU	--Transmit Data Underrun interrupt\n");
if((status&ST_XOFF)==ST_XOFF)	printf("ST_XOFF	--XOFF character detected interrupt\n");
if((status&ST_TIN)==ST_TIN)		printf("ST_TIN	--Timer interrupt\n");
if((status&ST_CSC)==ST_CSC)		printf("ST_CSC	--CTS Status Change\n");
if((status&ST_XMR)==ST_XMR)		printf("ST_XMR	--Transmit Message Repeat\n");
if((status&ST_XON)==ST_XON)		printf("ST_XON	--XON character detected interrupt\n");
if((status&ST_XPR)==ST_XPR)		printf("ST_XPR	--Transmit Pool Ready interrupt\n");
if((status&ST_BRK)==ST_BRK)		printf("ST_BRK	--Break interrupt\n");
if((status&ST_BRKT)==ST_BRKT)	printf("ST_BRKT	--Break Terminiated interrupt\n");
if((status&ST_RDO)==ST_RDO)		printf("ST_RDO	--Receive Data Overflow interrupt\n");
if((status&ST_TCD)==ST_TCD)		printf("ST_TCD	--Termination Character Detected interrupt\n");
if((status&ST_RFS)==ST_RFS)		printf("ST_RFS	--Receive Frame Start interrupt\n");
if((status&ST_TIME)==ST_TIME)	printf("ST_TIME	--Time out interrupt\n");
if((status&ST_RSC)==ST_RSC)		printf("ST_RSC	--Receive Status Change interrupt\n");
if((status&ST_PERR)==ST_PERR)	printf("ST_PERR	--Parity Error interrupt\n");
if((status&ST_PCE)==ST_PCE)		printf("ST_PCE	--Protocol Error interrupt\n");
if((status&ST_FERR)==ST_FERR)	printf("ST_FERR	--Framing Error interrupt\n");
if((status&ST_SCD)==ST_SCD)		printf("ST_SCD	--SYN Character Detected interrupt\n");
if((status&ST_PLLA)==ST_PLLA)	printf("ST_PLLA	--DPLL Asynchronous interrupt\n");
if((status&ST_CDSC)==ST_CDSC)	printf("ST_CDSC	--Carrier Detected Status Change interrupt\n");
if((status&ST_RFO)==ST_RFO)		printf("ST_RFO	--Receive FIFO Overflow interrupt\n");
if((status&ST_FLEX)==ST_FLEX)	printf("ST_FLEX	--Frame Length Exceeded interrupt\n");

}