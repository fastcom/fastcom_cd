#include "windows.h"
#include "stdlib.h"
#include "stdio.h"
#include "conio.h"
#include "../sfctoolbox.h"

void main(int argc,char *argv[])
{
int ret;
ULONG port;
SCC_REGS settings;
char tdata[4096];
char rdata[4096];
unsigned long err;
unsigned long tosend;
unsigned long bytesin;
unsigned long i;
unsigned long loop;
unsigned long bytesxfred;

if(argc<2)
	{
	printf("usage:%s port\r\n",argv[0]);
	exit(1);
	}
port = atoi(argv[1]);
if((ret = SFCToolBox_Create(port))<0) 
{
	printf("Problem creating port...Aborting\r\n");
	printf("create:%d\n",ret);
	exit(0);
}
if(SFCToolBox_Get_Clock(port)!=2000000)
{
if((ret =SFCToolBox_Set_Clock(port,2000000,BOARD_SUPERFASTCOM))<0)
{
	printf("Problem setting clock generator...Aborting\r\n");
	printf("setclock:%d\n",ret);
	exit(0);
}
}
else printf("Clock allready 2M\r\n");

memset(&settings,0,sizeof(settings));
		settings.cmdr = 0x01010000;//reset receiver and transmitter
		settings.star = 0x0;
		settings.ccr0 = 0x80000030;//clock mode 0b
		settings.ccr1 = 0x02048000;//ODS=1,FCTS=1,HDLC transparent mode 0
		settings.ccr2 = 0x08040000;//receiver on,RFTH=4 (32byte trigger)
		settings.imr = 0xFFFEFFFF;//disable all but XDU interrupt sources

/*		//these all default to 0, on the memset above
		settings.accm = 0;
		settings.udac = 0;
		settings.tsax = 0;
		settings.tsar = 0;
		settings.pcmmtx = 0;
		settings.pcmmrx = 0;
		settings.brr = 0;
		settings.timr = 0;
		settings.xadr = 0;
		settings.radr = 0;
		settings.ramr = 0;
		settings.rlcr = 0;
		settings.xnxfr = 0;
		settings.tcr = 0;
		settings.ticr = 0;
		settings.syncr = 0;
		settings.isr = 0x0;
*/
// Note BRR=0, and clock mode 0b in use, which makes the bitrate == Clock Generator setting.

if((ret = SFCToolBox_Configure(port,&settings))<0)
{
	printf("Problem Configuring port...Aborting\r\n");
	printf("configure:%d\n",ret);
	exit(0);
}
printf("paused\r\n");
getchar();

if((ret = SFCToolBox_Flush_RX(port))<0) printf("flushrx:%d\n",ret);
if((ret = SFCToolBox_Flush_TX(port))<0) printf("flushtx:%d\n",ret);
err=0;
loop=0;
bytesxfred=0;
do
{

		tosend=(ULONG)(rand() % 4095);
		if(tosend==0) tosend=1;
// generate a random string of our random length.
		for(i=0;i<tosend;i++) tdata[i]=(UCHAR)(rand());
//printf("pre-write\n");
bytesin=0;
if((ret = SFCToolBox_Write_Frame(port,tdata,tosend,1000))<0) printf("write failed:%d\n",ret);
if((ret = SFCToolBox_Read_Frame(port,rdata,4096,&bytesin,5000))<0) printf("read failed:%d\n",ret);
if(bytesin!=(tosend+1))
	{
	printf("receive bytecount error: %d != %d\r\n",bytesin,tosend+1);
	err++;
	}
if(bytesin!=0)
	{
	for(i=0;i<bytesin-1;i++)
		{
		if((tdata[i]&0xff)!=(rdata[i]&0xff))
			{
			printf("tx-rx byte mismatch rx:%x != tx:%x\r\n",rdata[i]&0xff,tdata[i]&0xff);
			err++;
			}
		}
	}
printf("loop:%ld %d\r\n",loop,tosend);
loop++;
bytesxfred+=tosend;
}while(!kbhit());
getch();
SFCToolBox_Destroy(port);
printf("loops           \t:%ld\r\n",loop);
printf("errors          \t:%ld\r\n",err);
printf("bytes transfered\t:%ld\r\n",bytesxfred);
}//end of main