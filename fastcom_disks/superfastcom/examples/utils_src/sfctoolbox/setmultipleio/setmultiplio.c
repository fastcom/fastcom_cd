#include "..\sfctoolbox.h"
#include "windows.h"
#include "stdio.h"
#include "stdlib.h"

void main(int argc,char *argv[])
{
int ret;
ULONG port;

if(argc<2)
	{
	printf("usage:%s port\r\n",argv[0]);
	exit(1);
	}
port = atoi(argv[1]);
if((ret = SFCToolBox_Create(0))<0) printf("create:%d\n",ret);
SFCToolBox_Block_Multiple_Io(port,0);
SFCToolBox_Destroy(port);
}