/* $Id$ */
// sfctoolbox.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "sfctoolbox.h"
#include "stdio.h"
#include "math.h"
#include "malloc.h"
extern "C" {
#include "..\utranslate\utranslate.h"
}


ULONG refcount = 0;

readcallbackfn readcallbacks[MAXPORTS]= {0};
void (__stdcall *writecallbacks[MAXPORTS])(DWORD port,char * tbuf,DWORD *bytestowrite)= {NULL};
void (__stdcall *statuscallbacks[MAXPORTS])(DWORD port,DWORD status)= {NULL};

static HANDLE hSFC[MAXPORTS] = {NULL};
BOOL connected[MAXPORTS] = {0};
DWORD statusmask[MAXPORTS] = {0};
DWORD readsize[MAXPORTS] = {0};
DWORD maxwritesize[MAXPORTS] = {0};
HANDLE hReadThread[MAXPORTS] = {NULL};
HANDLE hWriteThread[MAXPORTS] = {NULL};
HANDLE hStatusThread[MAXPORTS] = {NULL};
static DWORD refcnt[MAXPORTS] = {0};
HMODULE mymod[MAXPORTS]= {NULL};




DWORD FAR PASCAL StatusProc( DWORD port );
DWORD FAR PASCAL ReadProc( DWORD port );
DWORD FAR PASCAL WriteProc( DWORD port );


BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{


    switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
			break;
		case DLL_THREAD_ATTACH:
			break;
		case DLL_THREAD_DETACH:
			break;
		case DLL_PROCESS_DETACH:
			break;
    }
    return TRUE;
}


SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Create(DWORD port)
{
char devname[25];
ULONG onoff;
ULONG temp;
if(port>MAXPORTS) return -1;
if(hSFC[port]!=NULL) return -2;
//MessageBox(NULL,"in create\n","",MB_OK);	
sprintf(devname,"\\\\.\\SFC%d",port);
        hSFC[port] = CreateFile (devname,
			  GENERIC_READ | GENERIC_WRITE,
			  FILE_SHARE_READ | FILE_SHARE_WRITE,
			  NULL,
			  OPEN_ALWAYS,
			  FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
			  NULL
			  );

    if (hSFC[port] == INVALID_HANDLE_VALUE)
    {
//	MessageBox(NULL,"create failed\n","",MB_OK);
	return -3;
	}
onoff=0;//has to be...
DeviceIoControl(hSFC[port],IOCTL_SFCDRV_BLOCK_MULTIPLE_IO,&onoff,sizeof(ULONG),NULL,0,&temp,NULL);

return 0;
}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Destroy(DWORD port)
{
DWORD ret;
if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;


	connected[port]=0;
	if(hReadThread[port]!=NULL)
		{
		DeviceIoControl(hSFC[port],IOCTL_SFCDRV_CANCEL_RX,NULL,0,NULL,0,&ret,NULL);
		Sleep(0);
		CloseHandle(hReadThread[port]);
		}
	if(hWriteThread[port]!=NULL)
		{
		DeviceIoControl(hSFC[port],IOCTL_SFCDRV_CANCEL_TX,NULL,0,NULL,0,&ret,NULL);
		Sleep(0);
		CloseHandle(hWriteThread[port]);
		}
	if(hStatusThread[port]!=NULL)
		{
		DeviceIoControl(hSFC[port],IOCTL_SFCDRV_CANCEL_STATUS,NULL,0,NULL,0,&ret,NULL);
		Sleep(0);
		CloseHandle(hStatusThread[port]);
		}
	CloseHandle(hSFC[port]);
	hReadThread[port]=NULL;
	hWriteThread[port]=NULL;
	hStatusThread[port]=NULL;
	hSFC[port]=NULL;
return 0;
}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Set_Clock(DWORD port,DWORD frequency,DWORD clocktype)
{
DWORD temp;
ULONG actualf;
ULONG passval[2];

if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;

passval[0] = frequency;
if(clocktype==AUTOCLOCK)
{
if(	DeviceIoControl(hSFC[port],IOCTL_SFCDRV_SET_FREQUENCY,&frequency,sizeof(ULONG),&actualf,sizeof(ULONG),&temp,NULL)!=0)
	{
	return actualf;
	}
else return -3;
}
else if(clocktype==ICD2053B)
{
passval[1] = 1;//ICD2053B
if(	DeviceIoControl(hSFC[port],IOCTL_SFCDRV_SET_FREQUENCY,&passval[0],2*sizeof(ULONG),&actualf,sizeof(ULONG),&temp,NULL)!=0)
	{
	return actualf;
	}
else return -3;
}
else if(clocktype==ICS307)
{
passval[1] = 2;//ICS307
if(DeviceIoControl(hSFC[port],IOCTL_SFCDRV_SET_FREQUENCY,&passval[0],2*sizeof(ULONG),&actualf,sizeof(ULONG),&temp,NULL)!=0)
	{
	return actualf;
	}
else return -3;
}
else if (clocktype==FS6131)
{
if(DeviceIoControl(hSFC[port],IOCTL_SFCDRV_SET_FREQUENCY,&passval[0],2*sizeof(ULONG),&actualf,sizeof(ULONG),&temp,NULL)!=0)
	{
	return actualf;
	}
return -3;
}
else return -4;

}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Get_Clock(DWORD port)
{
clkset t;
DWORD res;

if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;
DeviceIoControl(hSFC[port], IOCTL_SFCDRV_GET_CLOCK,
			NULL,
			0,
			&t,
			sizeof(clkset),
			&res,
			NULL);

return (DWORD)(t.actualclock);

}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Set_ProgClock(DWORD port,DWORD frequency,DWORD clocktype)
{
DWORD temp;
ULONG actualf;
ULONG passval[2];

if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;

passval[0] = frequency;

if(clocktype==AUTOCLOCK)
{
if(	DeviceIoControl(hSFC[port],IOCTL_SFCDRV_SET_FREQUENCY2,&frequency,sizeof(ULONG),&actualf,sizeof(ULONG),&temp,NULL)!=0)
	{
	return actualf;
	}
else return -3;
}
else if(clocktype==ICD2053B)
{
passval[1] = 1;//ICD2053B
if(	DeviceIoControl(hSFC[port],IOCTL_SFCDRV_SET_FREQUENCY2,&passval[0],2*sizeof(ULONG),&actualf,sizeof(ULONG),&temp,NULL)!=0)
	{
	return actualf;
	}
else return -3;
}
else if(clocktype==ICS307)
{
passval[1] = 2;//ICS307
if(DeviceIoControl(hSFC[port],IOCTL_SFCDRV_SET_FREQUENCY2,&passval[0],2*sizeof(ULONG),&actualf,sizeof(ULONG),&temp,NULL)!=0)
	{
	return actualf;
	}
else return -3;
}
else if (clocktype==FS6131)
{
if(DeviceIoControl(hSFC[port],IOCTL_SFCDRV_SET_FREQUENCY2,&passval[0],2*sizeof(ULONG),&actualf,sizeof(ULONG),&temp,NULL)!=0)
	{
	return actualf;
	}
return -3;
}
else return -4;

}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Get_ProgClock(DWORD port)
{
clkset t;
DWORD res;

if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;
DeviceIoControl(hSFC[port], IOCTL_SFCDRV_GET_CLOCK2,
			NULL,
			0,
			&t,
			sizeof(clkset),
			&res,
			NULL);

return (DWORD)(t.actualclock);
}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Read_Frame(DWORD port,char * rbuf,DWORD szrbuf,DWORD *retbytes,DWORD timeout)
{
	ULONG t;
	OVERLAPPED  rq;
	int j;
	ULONG temp;

if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;

memset( &rq, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
rq.hEvent = CreateEvent( NULL,TRUE,FALSE,NULL );
if (rq.hEvent == NULL)
	{
	return -3;
	}

t = ReadFile(hSFC[port],rbuf,szrbuf,retbytes,&rq);
if(t==FALSE)  
	{
	t=GetLastError();
	if(t==ERROR_IO_PENDING)
		{
		do
			{
			j = WaitForSingleObject( rq.hEvent, timeout );
			if(j==WAIT_TIMEOUT)
				{
                DeviceIoControl(hSFC[port],IOCTL_SFCDRV_CANCEL_RX,NULL,0,NULL,0,&temp,NULL);
				retbytes[0] = 0;
				CloseHandle(rq.hEvent);
				return -4;
				}
				if(j==WAIT_ABANDONED)
				{
				retbytes[0] = 0;
				CloseHandle(rq.hEvent);
				return -5;
				}
			} while(j!=WAIT_OBJECT_0);
		GetOverlappedResult(hSFC[port],&rq,retbytes,TRUE);
		}
	else 
		{
//		char buf[256];
//		sprintf(buf,"%8.8x error:%8.8x\n",hSFC[port],ret);
//		MessageBox(NULL,buf,"ERROR",MB_OK);

		retbytes[0] = 0;
		CloseHandle(rq.hEvent);
		return -6;
		}
	}
CloseHandle(rq.hEvent);
return 0;


}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Write_Frame(DWORD port,char * tbuf,DWORD numbytes,DWORD timeout)
{
OVERLAPPED  wq;
BOOL t;
DWORD nobyteswritten;
DWORD j;
DWORD ret;
DWORD temp;

if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;

memset( &wq, 0, sizeof( OVERLAPPED ) );
wq.hEvent = CreateEvent( NULL,TRUE,FALSE,NULL);
if (wq.hEvent == NULL)
	{
	return -3;
	}

t = WriteFile(hSFC[port],tbuf,numbytes,&nobyteswritten,&wq);
if(t==FALSE)  
	{
	ret=GetLastError();
	if(ret==ERROR_IO_PENDING)
		{
		do
			{
			j = WaitForSingleObject( wq.hEvent, timeout );
			if(j==WAIT_TIMEOUT)
				{
                DeviceIoControl(hSFC[port],IOCTL_SFCDRV_CANCEL_TX,NULL,0,NULL,0,&temp,NULL);
				CloseHandle(wq.hEvent);
				return -4;
				}
				if(j==WAIT_ABANDONED)
				{
				CloseHandle(wq.hEvent);
				return -5;
				}
			} while(j!=WAIT_OBJECT_0);
		GetOverlappedResult(hSFC[port],&wq,&nobyteswritten,TRUE);
		}
	else
		{
//		char buf[256];
//		sprintf(buf,"%8.8x error:%8.8x\n",hSFC[port],ret);
//		MessageBox(NULL,buf,"ERROR",MB_OK);
		CloseHandle(wq.hEvent);
		return -6;
		}
	}
CloseHandle(wq.hEvent);
return 0;

}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Get_Status(DWORD port,DWORD *status,DWORD mask,DWORD timeout)
{
	ULONG t;
	ULONG temp;
	OVERLAPPED  st;
	DWORD j;

status[0]=0;
if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;

memset( &st, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
st.hEvent = CreateEvent( NULL,TRUE,FALSE,NULL);
if (st.hEvent == NULL)
	{
	return -3;
	}


t=	DeviceIoControl(hSFC[port],IOCTL_SFCDRV_STATUS,&mask,sizeof(ULONG),&status,sizeof(ULONG),&temp,&st);
if(t==FALSE)  
	{
	t=GetLastError();
	if(t==ERROR_IO_PENDING)
		{
		do
			{
			j = WaitForSingleObject( st.hEvent, timeout );
			if(j==WAIT_TIMEOUT)
				{
                DeviceIoControl(hSFC[port],IOCTL_SFCDRV_CANCEL_STATUS,NULL,0,NULL,0,&temp,NULL);
				CloseHandle(st.hEvent);
				return -4;
				}
			if(j==WAIT_ABANDONED)
				{
				CloseHandle(st.hEvent);
				return -5;
				}
			} while(j!=WAIT_OBJECT_0);
		GetOverlappedResult(hSFC[port],&st,&temp,TRUE);		
		}
	else
		{
//		char buf[256];
//		sprintf(buf,"%8.8x error:%8.8x\n",hSFC[port],ret);
//		MessageBox(NULL,buf,"ERROR",MB_OK);

		CloseHandle(st.hEvent);
		return -6;
		}
	}
CloseHandle(st.hEvent);
return 0;
}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Get_IStatus(DWORD port,DWORD *status)
{
BOOL t;
DWORD temp;
DWORD mask = 0xFFFFFFFF;

status[0]=0;
if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;

t = DeviceIoControl(hSFC[port],IOCTL_SFCDRV_IMMEDIATE_STATUS,&mask,sizeof(ULONG),&status,sizeof(ULONG),&temp,NULL);
if(t) return 0;
else return -3;
}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Register_Read_Callback(DWORD port,readcallbackfn fnptr,DWORD szread)
{

HANDLE            hreadThread ;	//handle to read thread
DWORD         dwThreadID ;		//temp Thread ID storage
if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;
readsize[port] = szread;
readcallbacks[port] =  fnptr;
connected[port]|=1;
//create read thread here.



hreadThread =     CreateThread( (LPSECURITY_ATTRIBUTES) NULL,
				    0, 
				    (LPTHREAD_START_ROUTINE) ReadProc,
				    (LPVOID) port,
				    0, &dwThreadID );

if(hreadThread==NULL)
	{
	readcallbacks[port] = NULL;
	connected[port]&=0xFFFFFFFE;
	return -3;
	}
hReadThread[port] = hreadThread;

return 0;
}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Register_Write_Callback(DWORD port,void (__stdcall *callbackfunction)(DWORD port,char * tbuf,DWORD *bytestowrite),DWORD szmaxwrite)
{
HANDLE            hwriteThread ;	//handle to write thread
DWORD         dwThreadID ;		//temp Thread ID storage

if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;
maxwritesize[port] = szmaxwrite;
writecallbacks[port] = callbackfunction;
connected[port]|=2;
//create write thread here


hwriteThread =     CreateThread( (LPSECURITY_ATTRIBUTES) NULL,
				    0, 
				    (LPTHREAD_START_ROUTINE) WriteProc,
				    (LPVOID) port,
				    0, &dwThreadID );

if(hwriteThread==NULL)
	{
	writecallbacks[port] = NULL;
	connected[port]&=0xFFFFFFFD;
	return -3;
	}
hWriteThread[port] = hwriteThread;
return 0;
}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Register_Status_Callback(DWORD port,DWORD mask,void (__stdcall *callbackfunction)(DWORD port,DWORD status))
{
HANDLE            hstatusThread ;	//handle to status thread
DWORD         dwThreadID ;		//temp Thread ID storage

if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;
statusmask[port] = mask;
statuscallbacks[port] = callbackfunction;
connected[port]|=4;
//create status thread here

hstatusThread =     CreateThread( (LPSECURITY_ATTRIBUTES) NULL,
				    0, 
				    (LPTHREAD_START_ROUTINE) StatusProc,
				    (LPVOID) port,
				    0, &dwThreadID );

if(hstatusThread==NULL)
	{
	statuscallbacks[port] = NULL;
	connected[port]&=0xFFFFFFFB;
	return -3;
	}
hStatusThread[port] = hstatusThread;
return 0;
}


SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Flush_RX(DWORD port)
{
DWORD temp;
if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;

DeviceIoControl(hSFC[port],
                        IOCTL_SFCDRV_FLUSH_RX,                          /* Device IOCONTROL */
			NULL,								/* write data */
			0,						/* write size */
			NULL,								/* read data */
			0,									/* read size */
			&temp,								/* Returned Size */
			NULL); /* overlap */
return 0;
}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Flush_TX(DWORD port)
{
DWORD temp;
if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;
DeviceIoControl(hSFC[port],
                        IOCTL_SFCDRV_FLUSH_TX,                          /* Device IOCONTROL */
			NULL,								/* write data */
			0,						/* write size */
			NULL,								/* read data */
			0,									/* read size */
			&temp,								/* Returned Size */
			NULL); /* overlap */
return 0;
}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Read_Register(DWORD port,DWORD regno)
{
DWORD val,temp;
BOOL t;
if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;

	t = DeviceIoControl(hSFC[port],
                        IOCTL_SFCDRV_READ_REGISTER,                             /* Device IOCONTROL */
			&regno,								/* write data */
			sizeof(unsigned long ),						/* write size */
			&val,								/* read data */
			sizeof(unsigned long),									/* read size */
			&temp,								/* Returned Size */
			NULL); /* overlap */
        if(temp!=0)  return val;
        else 
			{	
			//-1/-2/-3 are all valid returns...
			return -3;
			}

}
SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Write_Register(DWORD port,DWORD regno,DWORD value)
{
BOOL t;
DWORD passval[2];
DWORD val,temp;
if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;
passval[0] = regno;
passval[1] = value;
	t = DeviceIoControl(hSFC[port],
                        IOCTL_SFCDRV_WRITE_REGISTER,                            /* Device IOCONTROL */
			&passval,								/* write data */
			2*sizeof(unsigned long ),						/* write size */
			&val,								/* read data */
			sizeof(unsigned long),									/* read size */
			&temp,								/* Returned Size */
			NULL); /* overlap */
        if(t)  return 0;
        else return -3;

}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Read_Local_Register(DWORD port,DWORD regno)
{
DWORD val,temp;
if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;

DeviceIoControl(hSFC[port],
                        IOCTL_SFCDRV_READ_LB,                           /* Device IOCONTROL */
			&regno,								/* write data */
			sizeof(unsigned long ),						/* write size */
			&val,								/* read data */
			sizeof(unsigned long),									/* read size */
			&temp,								/* Returned Size */
			NULL); /* overlap */
return val&0xff;

}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Write_Local_Register(DWORD port,DWORD regno,DWORD value)
{

DWORD passval[2];
DWORD val,temp;
if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;
passval[0] = regno;
passval[1] = value;

DeviceIoControl(hSFC[port],
                        IOCTL_SFCDRV_WRITE_LB,                          /* Device IOCONTROL */
			&passval,								/* write data */
			2*sizeof(unsigned long ),						/* write size */
			&val,								/* read data */
			sizeof(unsigned long),									/* read size */
			&temp,								/* Returned Size */
			NULL); /* overlap */
return 0;
	

}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Configure(DWORD port,SCC_REGS *settings)
{
DWORD ret;
BOOL t;

if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;
//MessageBox(NULL,"In Configure\n","",MB_OK);
t =	DeviceIoControl(hSFC[port],
                        IOCTL_SFCDRV_SCC_REGISTERS_SET,         /* Device IOCONTROL */
			settings,							/* write data */
			sizeof(SCC_REGS)				,	/* write size */
			NULL,								/* read data */
			0,									/* read size */
			&ret,								/* Returned Size */
			NULL);								/* overlap */
if(t) return 0;
else {
//char buf[512];
//	sprintf(buf,"%8.8x\n%8.8x\n%8.8x",settings->ccr0,settings->ccr1,settings->ccr2);
//	MessageBox(NULL,buf,"CONFIGERR",MB_OK);
	return -3;
}
}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Set_Txclk_TT(DWORD port,DWORD onoff)
{
ULONG reg;
ULONG val;
ULONG temp;
ULONG regmask;
ULONG activett;
ULONG passval[2];
if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;

#ifdef XSA104
reg = 0x404;//gpdata
DeviceIoControl(hSFC[port],
                        IOCTL_SFCDRV_READ_REGISTER,                             /* Device IOCONTROL */
			&reg,								/* write data */
			sizeof(unsigned long ),						/* write size */
			&val,								/* read data */
			sizeof(unsigned long),									/* read size */
			&temp,								/* Returned Size */
			NULL); /* overlap */

if(onoff==1) val=val&0xFFFEFFFF;
else val = val|0x00010000;


passval[0] = 0x404; //gpdata
passval[1] = val;
DeviceIoControl(hSFC[port],IOCTL_SFCDRV_WRITE_REGISTER,&passval,2*sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);

#else
//superfastcom and superfastcom232/4-pci and superfastcom/2-104
reg = 2;
DeviceIoControl(hSFC[port],IOCTL_SFCDRV_READ_LB,&reg,sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);

regmask = 0x02;
regmask = regmask<<(2*(port%4));
//bit0 is 0, st is enabled
//bit0 is 1, st is disabled
//bit1 is 0, tt is enabled
//bit1 is 1, tt is disabled

activett = 0x02<<(2*(port%4));//disable TT

if(onoff==0) val=(val&(~regmask))|activett;
else val=(val&(~regmask));

passval[0] = 2;
passval[1] = val;
DeviceIoControl(hSFC[port],IOCTL_SFCDRV_WRITE_LB,&passval,2*sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);
#endif

return 0;

}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Set_Txclk_ST(DWORD port,DWORD onoff)
{
ULONG reg;
ULONG val;
ULONG temp;
ULONG regmask;
ULONG activest;
ULONG passval[2];
if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;

#ifdef XSA104
reg = 0x404;//gpdata
DeviceIoControl(hSFC[port],
                        IOCTL_SFCDRV_READ_REGISTER,                             /* Device IOCONTROL */
			&reg,								/* write data */
			sizeof(unsigned long ),						/* write size */
			&val,								/* read data */
			sizeof(unsigned long),									/* read size */
			&temp,								/* Returned Size */
			NULL); /* overlap */

if(onoff==1) val=val&0xFFFDFFFF;
else val = val|0x00020000;

passval[0] = 0x404; //gpdata
passval[1] = val;
DeviceIoControl(hSFC[port],IOCTL_SFCDRV_WRITE_REGISTER,&passval,2*sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);

#else
//superfastcom and superfastcom232/4-pci and superfastcom/2-104
reg = 2;
DeviceIoControl(hSFC[port],IOCTL_SFCDRV_READ_LB,&reg,sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);

regmask = 0x01;
regmask = regmask<<(2*(port%4));
//bit0 is 0, st is enabled
//bit0 is 1, st is disabled
//bit1 is 0, tt is enabled
//bit1 is 1, tt is disabled
activest = 0x01<<(2*(port%4));//disable ST
if(onoff==0) val=(val&(~regmask))|activest;
else val=(val&(~regmask));

passval[0] = 2;
passval[1] = val;
DeviceIoControl(hSFC[port],IOCTL_SFCDRV_WRITE_LB,&passval,2*sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);
#endif
return 0;
}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Set_SD_485(DWORD port,DWORD onoff)
{
ULONG reg;
ULONG val,val1;
ULONG temp;
ULONG regmask;
ULONG active;
ULONG passval[2];
if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;

//superfastcom and superfastcom/2-104
reg = 0;
DeviceIoControl(hSFC[port],IOCTL_SFCDRV_READ_LB,&reg,sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);
reg = 1;
DeviceIoControl(hSFC[port],IOCTL_SFCDRV_READ_LB,&reg,sizeof(unsigned long ),&val1,sizeof(unsigned long),&temp,NULL);
temp = (val1<<8)+val;

regmask = 0x02;
regmask = regmask<<(4*(port%4));
active = 0x02<<(4*(port%4));//disable ST
if(onoff==0) val=(temp&(~regmask))|active;
else val=(val&(~regmask));

passval[0] = 0;
passval[1] = val&0xff;
DeviceIoControl(hSFC[port],IOCTL_SFCDRV_WRITE_LB,&passval,2*sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);
passval[0] = 1;
passval[1] = (val>>8)&0xff;
DeviceIoControl(hSFC[port],IOCTL_SFCDRV_WRITE_LB,&passval,2*sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);

return 0;
}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Set_TT_485(DWORD port,DWORD onoff)
{
ULONG reg;
ULONG val,val1;
ULONG temp;
ULONG regmask;
ULONG active;
ULONG passval[2];
if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;

//superfastcom and superfastcom/2-104
reg = 0;
DeviceIoControl(hSFC[port],IOCTL_SFCDRV_READ_LB,&reg,sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);
reg = 1;
DeviceIoControl(hSFC[port],IOCTL_SFCDRV_READ_LB,&reg,sizeof(unsigned long ),&val1,sizeof(unsigned long),&temp,NULL);
temp = (val1<<8)+val;

regmask = 0x04;
regmask = regmask<<(4*(port%4));
active = 0x04<<(4*(port%4));//disable ST
if(onoff==0) val=(temp&(~regmask))|active;
else val=(val&(~regmask));

passval[0] = 0;
passval[1] = val&0xff;
DeviceIoControl(hSFC[port],IOCTL_SFCDRV_WRITE_LB,&passval,2*sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);
passval[0] = 1;
passval[1] = (val>>8)&0xff;
DeviceIoControl(hSFC[port],IOCTL_SFCDRV_WRITE_LB,&passval,2*sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);

return 0;

}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Set_RD_Echo_Cancel(DWORD port,DWORD onoff)
{
ULONG reg;
ULONG val,val1;
ULONG temp;
ULONG regmask;
ULONG active;
ULONG passval[2];
if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;

//superfastcom and superfastcom/2-104
reg = 0;
DeviceIoControl(hSFC[port],IOCTL_SFCDRV_READ_LB,&reg,sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);
reg = 1;
DeviceIoControl(hSFC[port],IOCTL_SFCDRV_READ_LB,&reg,sizeof(unsigned long ),&val1,sizeof(unsigned long),&temp,NULL);
temp = (val1<<8)+val;

regmask = 0x01;
regmask = regmask<<(4*(port%4));
active = 0x01<<(4*(port%4));//disable ST
if(onoff==0) val=(temp&(~regmask))|active;
else val=(val&(~regmask));

passval[0] = 0;
passval[1] = val&0xff;
DeviceIoControl(hSFC[port],IOCTL_SFCDRV_WRITE_LB,&passval,2*sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);
passval[0] = 1;
passval[1] = (val>>8)&0xff;
DeviceIoControl(hSFC[port],IOCTL_SFCDRV_WRITE_LB,&passval,2*sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);

return 0;

}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Invert_RT(DWORD port,DWORD onoff)
{
ULONG reg;
ULONG val,val1;
ULONG temp;
ULONG regmask;
ULONG active;
ULONG passval[2];
if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;

//superfastcom and superfastcom/2-104
reg = 3;
DeviceIoControl(hSFC[port],IOCTL_SFCDRV_READ_LB,&reg,sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);
reg = 4;
DeviceIoControl(hSFC[port],IOCTL_SFCDRV_READ_LB,&reg,sizeof(unsigned long ),&val1,sizeof(unsigned long),&temp,NULL);
temp = (val1<<8)+val;

regmask = 0x01;
regmask = regmask<<(4*(port%4));
active = 0x01<<(4*(port%4));//disable ST
if(onoff==0) val=(temp&(~regmask))|active;
else val=(val&(~regmask));

passval[0] = 3;
passval[1] = val&0xff;
DeviceIoControl(hSFC[port],IOCTL_SFCDRV_WRITE_LB,&passval,2*sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);
passval[0] = 4;
passval[1] = (val>>8)&0xff;
DeviceIoControl(hSFC[port],IOCTL_SFCDRV_WRITE_LB,&passval,2*sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);

return 0;


}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Invert_ST(DWORD port,DWORD onoff)
{
ULONG reg;
ULONG val,val1;
ULONG temp;
ULONG regmask;
ULONG active;
ULONG passval[2];
if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;

//superfastcom and superfastcom/2-104
reg = 3;
DeviceIoControl(hSFC[port],IOCTL_SFCDRV_READ_LB,&reg,sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);
reg = 4;
DeviceIoControl(hSFC[port],IOCTL_SFCDRV_READ_LB,&reg,sizeof(unsigned long ),&val1,sizeof(unsigned long),&temp,NULL);
temp = (val1<<8)+val;

regmask = 0x04;
regmask = regmask<<(4*(port%4));
active = 0x04<<(4*(port%4));//disable ST
if(onoff==0) val=(temp&(~regmask))|active;
else val=(val&(~regmask));

passval[0] = 3;
passval[1] = val&0xff;
DeviceIoControl(hSFC[port],IOCTL_SFCDRV_WRITE_LB,&passval,2*sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);
passval[0] = 4;
passval[1] = (val>>8)&0xff;
DeviceIoControl(hSFC[port],IOCTL_SFCDRV_WRITE_LB,&passval,2*sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);

return 0;

}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Invert_TT(DWORD port,DWORD onoff)
{
ULONG reg;
ULONG val,val1;
ULONG temp;
ULONG regmask;
ULONG active;
ULONG passval[2];
if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;

//superfastcom and superfastcom/2-104
reg = 3;
DeviceIoControl(hSFC[port],IOCTL_SFCDRV_READ_LB,&reg,sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);
reg = 4;
DeviceIoControl(hSFC[port],IOCTL_SFCDRV_READ_LB,&reg,sizeof(unsigned long ),&val1,sizeof(unsigned long),&temp,NULL);
temp = (val1<<8)+val;

regmask = 0x02;
regmask = regmask<<(4*(port%4));
active = 0x02<<(4*(port%4));//disable ST
if(onoff==0) val=(temp&(~regmask))|active;
else val=(val&(~regmask));

passval[0] = 3;
passval[1] = val&0xff;
DeviceIoControl(hSFC[port],IOCTL_SFCDRV_WRITE_LB,&passval,2*sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);
passval[0] = 4;
passval[1] = (val>>8)&0xff;
DeviceIoControl(hSFC[port],IOCTL_SFCDRV_WRITE_LB,&passval,2*sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);

return 0;

}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Set_Check_Timeout(DWORD port,DWORD miliseconds)
{
BOOL t;
DWORD passval;
DWORD temp;

if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;

	t = DeviceIoControl(hSFC[port],
                        IOCTL_SFCDRV_SET_CHECK_TIMEOUT,                            /* Device IOCONTROL */
			&passval,								/* write data */
			sizeof(unsigned long ),						/* write size */
			NULL,								/* read data */
			0,									/* read size */
			&temp,								/* Returned Size */
			NULL); /* overlap */
        if(t)  return 0;
        else return -3;

}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Set_RX_Irq_Rate(DWORD port,DWORD descriptorsperirq)
{
BOOL t;
DWORD passval;
DWORD temp;
if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;
passval = descriptorsperirq;
        t = DeviceIoControl(hSFC[port],
                        IOCTL_SFCDRV_SET_RX_IRQ_RATE,                            /* Device IOCONTROL */
			&passval,								/* write data */
			sizeof(unsigned long ),						/* write size */
			NULL,								/* read data */
			0,									/* read size */
			&temp,								/* Returned Size */
			NULL); /* overlap */
        if(t)  return 0;
        else return -3;

}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Set_TX_Irq_Rate(DWORD port,DWORD descriptorsperirq)
{
BOOL t;
DWORD passval;
DWORD temp;
if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;
passval = descriptorsperirq;
        t = DeviceIoControl(hSFC[port],
                        IOCTL_SFCDRV_SET_TX_IRQ_RATE,                            /* Device IOCONTROL */
			&passval,								/* write data */
			sizeof(unsigned long ),						/* write size */
			NULL,								/* read data */
			0,									/* read size */
			&temp,								/* Returned Size */
			NULL); /* overlap */
        if(t)  return 0;
        else return -3;


}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Mask_RX_Frame_End(DWORD port,DWORD onoff)
{
BOOL t;
DWORD passval;
DWORD temp;
if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;

if(onoff==1) passval = 0;
else passval = 1;

	t = DeviceIoControl(hSFC[port],
                        IOCTL_SFCDRV_SET_RX_FI_MASK,                            /* Device IOCONTROL */
			&passval,								/* write data */
			sizeof(unsigned long ),						/* write size */
			NULL,								/* read data */
			0,									/* read size */
			&temp,								/* Returned Size */
			NULL); /* overlap */
        if(t)  return 0;
		else return -3;

}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Mask_TX_Frame_End(DWORD port,DWORD onoff)
{
BOOL t;
DWORD passval;
DWORD temp;
if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;

if(onoff==1) passval = 0;
else passval = 1;

	t = DeviceIoControl(hSFC[port],
                        IOCTL_SFCDRV_SET_TX_FI_MASK,                            /* Device IOCONTROL */
			&passval,								/* write data */
			sizeof(unsigned long ),						/* write size */
			NULL,								/* read data */
			0,									/* read size */
			&temp,								/* Returned Size */
			NULL); /* overlap */
        if(t)  return 0;
		else return -3;

}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Set_RX_Multiple(DWORD port,DWORD framesperread)
{
DWORD t;
if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;

DeviceIoControl(hSFC[port],IOCTL_SFCDRV_SET_RECEIVE_MULTIPLE,&framesperread,sizeof(ULONG),NULL,0,&t,NULL);
return 0;
}

SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Block_Multiple_Io(DWORD port,DWORD onoff)
{
DWORD t;
if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;

DeviceIoControl(hSFC[port],IOCTL_SFCDRV_BLOCK_MULTIPLE_IO,&onoff,sizeof(ULONG),NULL,0,&t,NULL);
return 0;

}


SFCTOOLBOX_API DWORD __stdcall SFCToolBox_Get_Buffering(DWORD port,ULONG *passval)
{
DWORD t;
if(port>MAXPORTS) return -1;
if(hSFC[port]==NULL) return -2;

DeviceIoControl(hSFC[port],IOCTL_SFCDRV_GET_DESC_PARAMETERS,NULL,0,passval,4*sizeof(ULONG),&t,NULL);
return 0;

}



DWORD FAR PASCAL StatusProc( DWORD port )
{
DWORD k;
BOOL t;
DWORD returnsize;
OVERLAPPED  os ;
DWORD status;

				
memset( &os, 0, sizeof( OVERLAPPED ) );


os.hEvent = CreateEvent( NULL,TRUE,FALSE,NULL );
if (os.hEvent == NULL)
   {
   return -3;
   }
do
	{

	t = DeviceIoControl(hSFC[port],IOCTL_SFCDRV_STATUS,&statusmask[port],sizeof(ULONG),&status,sizeof(ULONG),&returnsize,&os);
	if(t==FALSE)
		{
		if (GetLastError() == ERROR_IO_PENDING)
			{
			do
				{
				k = WaitForSingleObject( os.hEvent, 5000 );//5 second timeout
				if(k==WAIT_TIMEOUT)
					{
					}
				if(k==WAIT_ABANDONED)
					{
					}
				}while((k!=WAIT_OBJECT_0)&&((connected[port]&4)!=0));//exit if we get signaled or if the main thread quits
				
			if((connected[port]&4)!=0)
				{
				GetOverlappedResult(hSFC[port],&os,&returnsize,TRUE); 
				t=TRUE;
				}
			}
		else
			{
			//actual error, what to do, what to do
			}
		}
	//if we get here then either it was true immediatly, so we return, or we waited and fell through and have the overlapped complete, either way signal it
	if((connected[port]&4)!=0)                   //if not connected then j is invalid
		{
		statuscallbacks[port](port,status);
		}
	}while((connected[port]&4)!=0);              //keep making requests until we want to terminate
CloseHandle( os.hEvent ) ;              //we are terminating so close the event
return 0;                          //done
}


DWORD FAR PASCAL ReadProc( DWORD port )
{
DWORD j;		
BOOL t;			
char *data;	
DWORD nobytestoread;
DWORD nobytesread;	
OVERLAPPED  os ;	

memset( &os, 0, sizeof( OVERLAPPED ) );

os.hEvent = CreateEvent( NULL,TRUE,FALSE,NULL);
if (os.hEvent == NULL)
   {
   return -3 ;
   }

data = (char *)malloc(readsize[port]);

if(data==NULL) return -4;

nobytestoread = readsize[port];

do
	{
	t = ReadFile(hSFC[port],data,nobytestoread,&nobytesread,&os);
	if(t==FALSE)
		{
		if (GetLastError() == ERROR_IO_PENDING)
			{
			do
				{
				j = WaitForSingleObject( os.hEvent, 5000 );//5 second timeout
				if(j==WAIT_TIMEOUT)
					{
					//timeout processing
					}
				if(j==WAIT_ABANDONED)
					{
					//??
					}
				}while((j!=WAIT_OBJECT_0)&&((connected[port]&1)!=0));//stay here until we get signaled or the main thread exits
			if((connected[port]&1)!=0)
					{                                                       
					GetOverlappedResult(hSFC[port],&os,&nobytesread,TRUE); //here to get the actual nobytesread!!!
					t=TRUE;
					}
			}
		else
			{
			//actual read error here...what to do, what to do
			//MessageBox(NULL,"READ ERROR","",MB_OK);
			}
		}
	//if we get here then either it was true immediatly, so we return, or we waited and fell through and have the overlapped complete, either way signal it
	if((connected[port]&1)!=0)
		{
		readcallbacks[port](port,data,nobytesread);
		}
	}while((connected[port]&1)!=0);              //do until we want to terminate
	CloseHandle( os.hEvent ) ;      //done with event
	free(data);
	return 0;                   //outta here
}

DWORD FAR PASCAL WriteProc( DWORD port )
{
OVERLAPPED  wq;
BOOL t;
DWORD nobyteswritten;
DWORD nobytestowrite;
DWORD j;
DWORD ret;
char *data;


memset( &wq, 0, sizeof( OVERLAPPED ) );
wq.hEvent = CreateEvent( NULL,TRUE,FALSE,NULL);
if (wq.hEvent == NULL)
	{
	return -3;
	}
data = (char *)malloc(maxwritesize[port]);
if(data==NULL) return -4;

do
{

if((connected[port]&2)!=0)                   
	{
	writecallbacks[port](port,data,&nobytestowrite);
	}

t = WriteFile(hSFC[port],data,nobytestowrite,&nobyteswritten,&wq);
if(t==FALSE)  
	{
	ret=GetLastError();
	if(ret==ERROR_IO_PENDING)
		{
		do
			{
			j = WaitForSingleObject( wq.hEvent, 5000 );
			if(j==WAIT_TIMEOUT)
				{
				//timeout processing
				}
				if(j==WAIT_ABANDONED)
				{
				//??
				}
			} while((j!=WAIT_OBJECT_0)&&((connected[port]&2)!=0));
		GetOverlappedResult(hSFC[port],&wq,&nobyteswritten,TRUE);
		}
	else
		{
		//write error...what to do, what to do
		}
	}
//returned true, so its done or queued
}while((connected[port]&2)!=0);
CloseHandle(wq.hEvent);
free(data);
return 0;
}
