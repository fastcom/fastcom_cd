#include "..\sfctoolbox.h"
#include "windows.h"
#include "stdio.h"
#include "stdlib.h"

void __stdcall sfcwritecb(DWORD port,char * tbuf,DWORD *bytestowrite);


void main(int argc,char *argv[])
{
int ret;
ULONG port;
if(argc<2)
	{
	printf("usage:%s port\r\n",argv[0]);
	exit(1);
	}
port = atoi(argv[1]);
if((ret = SFCToolBox_Create(port))<0) 
	{
	printf("create:%d\n",ret);
	exit(0);
	}
ret = SFCToolBox_Register_Write_Callback(port,sfcwritecb,4096);
if(ret<0)
	{
	printf("registerwritecallback:%d\n",ret);
	SFCToolBox_Destroy(port);
	exit(0);
	}
printf("paused...any key to exit\n");
getchar();
SFCToolBox_Destroy(port);
}

void __stdcall sfcwritecb(DWORD port,char * tbuf,DWORD *bytestowrite)
{
ULONG i;

for(i=0;i<1023;i++) tbuf[i] = (char)(i&0xff);
bytestowrite[0] = 1023;

}