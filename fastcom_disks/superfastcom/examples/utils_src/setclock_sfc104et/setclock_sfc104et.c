/*$Id$*/
/*
 * Example program to detect and set the different clocks on the SuperFastcom/2-104-ET rev 1.0 
 * and rev 2.0 to both achieve the same clock frequency.
 *
 * Do not use this code unless you are using an ET version of the SuperFastcom PC/104+ card.
 *
 */


#include <windows.h>
#include <stdio.h>

#include "..\sfc.h"


int main(int argc, char * argv[])
{
	char nbuf[80];
	int port;
	HANDLE hDevice; 
	ULONG reg;
	ULONG val;
	ULONG temp;
	ULONG out;
	ULONG passval[2];
	ULONG freq;
	
	if(argc<3) 
	{
        printf("usage:%s port frequency (in Hz)\n",argv[0]);
		exit(1);
	}
	
	port = atoi(argv[1]);
	freq = atoi(argv[2]);
	
    sprintf(nbuf,"\\\\.\\SFC%u",port);
	
	printf("Opening: %s\n",nbuf);
	
	if((hDevice = CreateFile (
		nbuf, 
		GENERIC_READ | GENERIC_WRITE,
		0,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
		NULL)) == INVALID_HANDLE_VALUE ) 
	{
		printf("Can't get a handle to sfcdrv @ %s\n",nbuf);
		exit(1);
	}
	
	//board/clock detection code, run this to determine which way to call SET_FREQUENCY
	
	reg = 5;
	
	DeviceIoControl(hDevice,IOCTL_SFCDRV_READ_LB,&reg,sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);
	
	if((val&0xff)==0x05)
	{
		if( (freq >= 1000000) && (freq <= 33000000) )
		{
			passval[0] = freq;
			passval[1] = 0;//is fs6131
			DeviceIoControl(hDevice,IOCTL_SFCDRV_SET_FREQUENCY,&passval[0],2*sizeof(ULONG),&out,sizeof(ULONG),&temp,NULL);
		}
		else
		{
			printf("Frequency is out of allowable range! Max=33MHz Min=1MHz\n");
			exit(1);
		}
	}
	else
	{
		if( (freq >= 6000000) && (freq <= 33000000) )
		{
			passval[0] = freq;
			passval[1] = 3;//is ics307
			DeviceIoControl(hDevice,IOCTL_SFCDRV_SET_FREQUENCY,&passval[0],2*sizeof(ULONG),&out,sizeof(ULONG),&temp,NULL);
		}
		else
		{
			printf("Frequency is out of allowable range! Max=33MHz Min=6MHz\n");
			exit(1);
		}

	}
}


