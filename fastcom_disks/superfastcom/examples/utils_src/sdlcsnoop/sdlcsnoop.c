/*++

Copyright (c) 1998,1999 Commtech, Inc Wichita ,KS

Module Name:

    sdlcsnoop.c

Abstract:

    A simple console app that will do receive only.
	Bare bones receive HDLC application.
	
	reconly prog modified to:
	take bitrate and line encoding from command line 
	then display SDLC frame information as frames are received
	
Environment:

    user mode only

Notes:

    
Revision History:

    8/25/98             started
	10/27/99			mod for bitrate/line encoding from cmd line
--*/



#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "..\sfc.h"

void decode_sdlc(char *buffer,unsigned size);

void main(int argc, char *argv[])
{
	HANDLE hDevice;			//handle to the sfc driver/device
	char data[4096];		//character array for data storage (passing to and from the driver)
	DWORD nobytestoread;	//the number of bytes to read from the driver
	DWORD nobytesread;
	ULONG i,j,k;				//temp vars
	DWORD returnsize;		//temp vars
	ULONG timeout;
	BOOL t;					//temp vars
	OVERLAPPED  os;				//overlapped structure for use in the receive routine
	DWORD debug;
	unsigned port;
	char devname[64];
	ULONG passval[2];
	if(argc<2)
	{
		printf("usage:\r\n");
		printf("%s port\r\n",argv[0]);
		exit(1);
	}
	port = atoi(argv[1]);
	
	printf("SDLC SNOOPER (C)1999 Commtech, Inc.\r\n");
	
	debug = 0;//set to 1 to get sfc status messages with data
	
	
	memset( &os, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
	
	// create I/O event used for overlapped structure
	
	os.hEvent = CreateEvent( NULL,    // no security
		TRUE,    // explicit reset req
		FALSE,   // initial event reset
		NULL ) ; // no name
	if (os.hEvent == NULL)
	{
		MessageBox( NULL, "Failed to create event for thread!", "main Error!",
			MB_ICONEXCLAMATION | MB_OK ) ;
		return; 
	}
	
	
	nobytestoread = 4096;    //this should allways be 4096, it is the maximum frame size returned from the driver at one time
	
    
	//this will start up the driver, it also gives us a handle to send
	//and receive data from the driver
	//note that it is created with the FILE_FLAG_OVERLAPPED set
	//if it is not then you must make sequential calls to the driver
	//(ie wait until one call has returned before making another call)
	//the read/write and status functions are set up to use overlapped, they 
	//will return immediatly with a ERROR_IO_PENDING or immediatly with the required data
	//
	
	sprintf(devname,"\\\\.\\SFC%d",port);
	printf("using device:%s\r\n",devname);
    hDevice = CreateFile (devname,
		GENERIC_READ | GENERIC_WRITE,
		0,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
		NULL
		);
	
    if (hDevice == INVALID_HANDLE_VALUE)
    {
		//for some reason the driver won't load or isn't loaded
		printf ("Can't get a handle to %s\n",devname);
		exit(1);
		//abort and leave here!!!
	}
	
	//use whatever settings they had coming into this
	DeviceIoControl(hDevice,IOCTL_SFCDRV_FLUSH_RX,NULL,0,NULL,0,&t,NULL);
	passval[0] = 0x0100+(port*0x80);//cmdr
	passval[1] = 0x00010000;//reset rx
	DeviceIoControl(hDevice,IOCTL_SFCDRV_WRITE_REGISTER,&passval[0],2*sizeof(DWORD),NULL,0,&t,NULL);
	
start:
	do
	{
		
		//start a read request by calling ReadFile() with the sfc device handle
		//if it returns true then we received a frame 
		//if it returns false and ERROR_IO_PENDING then there are no
		//receive frames available so we wait until the overlapped struct
		//gets signaled. (indicating that a frame has been received)
		timeout = 0;
		t = ReadFile(hDevice,&data,nobytestoread,&nobytesread,&os);
		//if this returns true then your system is not reading the frames from the 
		//driver as fast as they are coming in.
		//if it returns false then the system is processing the frames quick enough
		
		if(t==FALSE)
		{
			if (GetLastError() == ERROR_IO_PENDING)
			{
				// wait for a receive frame to come in
				do
				{
					j = WaitForSingleObject( os.hEvent, 1000 );//1 second timeout
					if(j==WAIT_TIMEOUT)
					{
						//this will execute every 1 second that a frame is not received
						//you could put a counter in here and if the 
						//driver takes an inordinate ammout of time
						//to complete, you could issue a flush RX command
						//and break out of this loop
						timeout++;
						if(timeout > 60) 
						{
							printf("no data received for 1 minute...aborting\r\n");
							goto quitprog;
						}
						if(kbhit())
						{
							if(getch()==27)
							{
								printf("exiting on keypress\r\n");
								goto quitprog;
							}
						}
					}
					if(j==WAIT_ABANDONED)
					{
					}
					
				}while(j!=WAIT_OBJECT_0);//stay here until we get signaled
				
				
			}
		}                                                     
		GetOverlappedResult(hDevice,&os,&nobytesread,TRUE); //here to get the actual nobytesread!!!
		if(nobytesread>=1)
		{
			for(i=0;i<40;i++)printf("-");
			printf("\r\n");
			printf("received :%d\n",nobytesread);
			decode_sdlc(data,nobytesread);
			for(i=0;i<40;i++)printf("-");
			printf("\r\n");
		}
		else
		{
			for(i=0;i<40;i++)printf("-");
			printf("\r\n");
			printf("received 0 byte frame\r\n");
			for(i=0;i<40;i++)printf("-");
			printf("\r\n");
		}
		if(debug==1)
		{
			DeviceIoControl(hDevice,IOCTL_SFCDRV_IMMEDIATE_STATUS,NULL,0,&k,sizeof(DWORD),&returnsize,NULL);
			printf("\r\nSTATUS: %lx\r\n",k);
			//you could decode the returned here 
			//they are the ST_XXXXXX defines in the .h file
		}
	}while(!kbhit());         
	j = getch();
	if(j!=27) goto start;
quitprog:
	CloseHandle (os.hEvent);//done with the event
	CloseHandle (hDevice);// stops the sfc from interrupting (ie shuts it down)
}                                               //done


//function that decodes SDLC frames
void decode_sdlc(char *buffer,unsigned size)
{
unsigned j,i;

j = size;

if(j>=2)
{
printf("address:0x%x\r\n",buffer[0]&0xff);
printf("control:0x%x\r\n",buffer[1]&0xff);              
if((buffer[1]&0x01)==0)
	{
	printf("I Frame, NR = %u, NS = %u p/f = %u\r\n",(buffer[1]&0xe0)>>5,(buffer[1]&0x0e)>>1,(buffer[1]&0x10)>>4);
	}     
if((buffer[1]&0x0f)==0x01)
	{
	printf("S-Frame-RR: NR = %u, p/f = %u\r\n",(buffer[1]&0xe0)>>5,(buffer[1]&0x10)>>4);
	}	
if((buffer[1]&0x0f)==0x05)
	{
	printf("S-Frame-RNR: NR = %u, p/f = %u\r\n",(buffer[1]&0xe0)>>5,(buffer[1]&0x10)>>4);
	}	
if((buffer[1]&0x0f)==0x09)
	{
	printf("S-Frame-REJ: NR = %u, p/f = %u\r\n",(buffer[1]&0xe0)>>5,(buffer[1]&0x10)>>4);
	}	
if((buffer[1]&0xff)==0x03) printf("Nonsequenced-NSI p/f = 0\r\n");
if((buffer[1]&0xff)==0x13) printf("Nonsequenced-NSI p/f = 1\r\n");
if((buffer[1]&0xff)==0x07) printf("Nonsequenced-RQI\r\n");
if((buffer[1]&0xff)==0x17) printf("Nonsequenced-SIM\r\n");
if((buffer[1]&0xff)==0x93) printf("Nonsequenced-SNRM\r\n");
if((buffer[1]&0xff)==0x0F) printf("Nonsequenced-ROL\r\n");
if((buffer[1]&0xff)==0x53) printf("Nonsequenced-DISC\r\n");
if((buffer[1]&0xff)==0x63) printf("Nonsequenced-NSA\r\n");
if((buffer[1]&0xff)==0x87) printf("Nonsequenced-CMDR\r\n");
if((buffer[1]&0xff)==0x33) printf("Nonsequenced-ORP\r\n");



printf("data:\r\n");
for(i=2;i<j-1;i++) printf("0x%x,",buffer[i]&0xff);
printf("\r\n");	
}
if(j>=1)
{
printf("RSTA:0x%x\r\n",buffer[j-1]&0xff);
if((buffer[j-1]&0x20)==0x20) printf("CRC passed\r\n");
else printf("CRC failed\r\n");
if((buffer[j-1]&0x40)==0x40) printf("Receive Data Overflow (hardware)\r\n");
if((buffer[j-1]&0x80)==0x00) printf("Invalid frame\r\n");
if((buffer[j-1]&0x10)==0x10) printf("Receive Frame Abort indicated\r\n");
printf("\r\n");
}
}

