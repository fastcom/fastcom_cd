/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
flushtx.c -- user mode function flush Superfastcom transmit/buffers

usage:
 flushtx port 

 The port can be any valid sfc port (0,1,2,3) 

*/


#include <windows.h>
#include <stdio.h>
#include <math.h> /* floor, pow */

#include "..\sfc.h"



int main(int argc, char * argv[])
{
	//clkset clock;
	char nbuf[80];
	int port,t;
	HANDLE hDevice; 
    unsigned long reg,val,temp;

	unsigned long passval[2];

	if(argc<2) {
                printf("usage: %s port\n",argv[0]);
		exit(1);
	}

	port = atoi(argv[1]);
        sprintf(nbuf,"\\\\.\\SFC%u",port);

	printf("FLUSHING TX: %s\n",nbuf);

	if((hDevice = CreateFile (
			nbuf, 
			GENERIC_READ | GENERIC_WRITE,
			0,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
			NULL)) == INVALID_HANDLE_VALUE ) 
	{
                printf("Can't get a handle to sfcdrv @ %s\n",nbuf);
		exit(1);
	}

        t = DeviceIoControl(hDevice,
                        IOCTL_SFCDRV_FLUSH_TX,                          /* Device IOCONTROL */
			NULL,								/* write data */
			0,						/* write size */
			NULL,								/* read data */
			0,									/* read size */
			&temp,								/* Returned Size */
			NULL); /* overlap */
	CloseHandle(hDevice);
	return 0;
}
/* $Id$ */