/* $Id$ */
#ifndef SFC_H
#define SFC_H

/*
  sfc.h -- header file for user mode programs
*/
/* Interrupt Descriptor Defines.

IRQ_DEV_QUEUES  -	is the 5 different sections in which the 
                                        SFC driver divides the interrupts types.
	0 is the CFG
	1 is the DMAC
	2 is the SCC
	3 is the SCC Peripheral
	4 is the LBI Peripheral


MAX_IRQ_ENTRIES -	is the size/offset of each IRQ_DEV_QUEUES in DWORDs.

Look at the Infineon Manual for more info at chapter 11.2.
*/
#define IRQ_DEV_QUEUES		5
#define MAX_IRQ_ENTRIES		32
//2d array to pass with IOCTL_SFCDRV_SFC_IQ
//DWORD INTERRUPT_QUEUE[IRQ_DEV_QUEUES][MAX_IRQ_ENTRIES]


/* File Names to Open the Windows SFC Driver */
#define SFC_PORT0       "\\\\.\\SFC0"
#define SFC_PORT1       "\\\\.\\SFC1"
#define SFC_PORT2       "\\\\.\\SFC2"
#define SFC_PORT3       "\\\\.\\SFC3"


/* User Code Header for the Commtech Fastcom SFC dscc4 adapter */

#define IOCTL_SFCDRV_TX_READY                           0x830020c0 //1
/*
Not currently implemented
*/
#define IOCTL_SFCDRV_RX_READY                           0x830020c4 //2
/*
Not currently implemented
*/
#define IOCTL_SFCDRV_SETUP                                      0x830020c8 //3
/*
Not currently implemented
*/
#define IOCTL_SFCDRV_STATUS                                     0x830020cc //4
/*
Same as IOCTL_SFCDRV_IMMEDIATE_STATUS but does block.  Will not return until
a status event occurs. (Probably ought to only use with OVERLAPPED otherwise program
execution stops when you call this IOCTL).

input ULONG, return ULONG
Input is a ULONG that will mask off the corresponding interrupt according to the 
vector table if you set the corresponding bit to 0.
Returns masked table of interrupt vectors.

 		31	30	29	28	27	26	25	24
	----------------------------------------------------------
HDLC	1	0	0	0	0	0	0	0
ASYNC	1	0	0	0	0	0	0	0
BISYNC	1	0	0	0	0	0	0	0

		23	22	21	20	19	18	 17	16	15	 14	 13	 12
	--------------------------------------------------------------------------------------------
HDLC	0	HI	FI	ERR	0	ALLS 0	XDU	 TIN CSC XMR XPR
ASYNC	0	HI	FI	ERR	0	ALLS 0	XOFF TIN CSC XON XPR
BISYNC	0	HI	FI	ERR	0	ALLS 0	XDU	 TIN CSC XMR XPR

		11	10	9	8		7	6	 5	  4		3	 2	  1		0
	--------------------------------------------------------------------------------------------
HDLC	0	0	0	0		RDO	RFS	 RSC  PCE	PLLA CDSC RFO	FLEX
ASYNC	0	0	BRK	BRKT	TCD	TIME PERR FERR	PLLA CDSC RFO	0
BISYNC	0	0	0	0		TCD	0	 PERR SCD	PLLA CDSC RFO	0

NOTE:  (22:20) See Table 103 PEB20534 manual
       (19:0) See Table 104 PEB20534 manual
*/


#define IOCTL_SFCDRV_START_TIMER                        0x830020d0 //5
/*
Not currently implemented
*/
#define IOCTL_SFCDRV_STOP_TIMER                         0x830020d4 //6
/*
Not currently implemented
*/
#define IOCTL_SFCDRV_SET_TX_TYPE                        0x830020d8 //7
/*
Not currently implemented
*/
#define IOCTL_SFCDRV_SET_TX_ADD                         0x830020dc //8
/*
Not currently implemented
*/
#define IOCTL_SFCDRV_FLUSH_RX                           0x830020e0 //9
/*
input nothing, return nothing
Resets rx dma, flushes/resets descriptors, issues RHR command to SCC
command register.  Will also unblock/cancel outstanding blocked ReadFile
functions.
*/
#define IOCTL_SFCDRV_FLUSH_TX                           0x830020e4 //10
/*
input nothing, return nothing
Flushes descriptor buffers & resets 20534 TX dma
Adds XRES command to SCC command register
Will also unblock/cancel outstanding blocked WriteFile functions.
*/
#define IOCTL_SFCDRV_CANCEL_RX                          0x830020e8 //11
/*
Input nothing.  Output nothing.
Cancels impending read.  Overlapped will return with 0 bytes returned w/ status canceled.
*/
#define IOCTL_SFCDRV_CANCEL_TX                          0x830020ec //12
/*
Input nothing.  Output nothing.
Cancels impending write.  Overlapped will return with 0 bytes written w/ status canceled.
*/
#define IOCTL_SFCDRV_CANCEL_STATUS                      0x830020f0 //13
/*
Input nothing.  Output nothing.
Cancels impending status.  Overlapped will return w/ status canceled.
*/
#define IOCTL_SFCDRV_SET_CLOCK                          0x830020f4 //14
/*
input CLKSET struct, return nothing
typedef struct clkset
{
ULONG clockbits;
ULONG numbits;
ULONG actualclock[2];
} clkset;
 
   or

typedef struct dscc_clkset
{
	unsigned long clockbits;
	unsigned long numbits;
	double actualclock;
} clkset;

The clockbits/numbits values are calculated using either bitcalc or
calculate_bits(), the actualclock value is passed and held/returned on the
GET of this ioctl, but is not used internally for anything.
*/
#define IOCTL_SFCDRV_READ_REGISTER                      0x830020f8 //15
/*
input ULONG, return ULONG.  
Input is register offset from base of 20534.
Output is contents of that register.
*/
#define IOCTL_SFCDRV_WRITE_REGISTER                     0x830020fc //16
/*
input ULONG[2], return nothing
Writes ulong[1] to 20534 base+ulong[0]
*/
#define IOCTL_SFCDRV_LOOPBACK                           0x83002100 //17
/*
input nothing, return nothing (garbage)
Switches loopback bit of CCR1 (clears if set, sets if clear)
(returns 1 byte if turns loopback on, 0 bytes if turns it off)
*/
#define IOCTL_SFCDRV_READ_TXDESC                        0x83002104 //18
/*
(DEBUG)
input nothing, return (20*#txdescriptors*ULONG) (20=sizeof descriptor)
Dumps tx descriptor memory to user space

    typedef struct descriptor 
    {
    	ULONG control;
    	ULONG pnext;
    	ULONG data;
    	ULONG status;
    	ULONG frameend;
    } DESC;
*/
#define IOCTL_SFCDRV_READ_RXDESC                        0x83002108 //19
/*
(DEBUG)
input nothing, return (20*#rxdescriptors*ULONG) (20=sizeof descriptor)
Dumps rx descriptor memory to user space
*/
#define IOCTL_SFCDRV_SCC_REGISTERS_SET          0x8300210c //20
/*
input SCC_REGS struct, return nothing
Nothing will happen until this ioctl has been called.
*/
#define IOCTL_SFCDRV_FIFO_CHNG                          0x83002110 //21
/*
Reserved for developer use, do not call
*/
#define IOCTL_SFCDRV_TXDMA_RESET                        0x83002114 //22
/*
input nothing, return nothing
Flushes descriptor buffers & resets 20534 TX dma
*/
#define IOCTL_SFCDRV_CHNG_DESC_LEN                      0x83002118 //23
/*
input ULONG[4] return nothing
ULONG[0] = # of receive descriptors
ULONG[1] = size of each receive descriptor
ULONG[2] = # of transmit descriptors
ULONG[3] = size of each transmit descriptor
# has to be greater than 3, size must be DWORD aligned (ie multiple of 4)

There are known issues with calling this ioctl more than once per (reboot).
basically if the ioctl fails, you must reboot.
*/
#define IOCTL_SFCDRV_SFC_IQ                           0x8300211c //24
/*
input nothing, output is ULONG[5][1024]
(currently only usefull as a debugging aid, I believe it is only
valid/works for the first port ie SFC0)
the 5 arrays are the IQ interrupt values for each port each direction with
[0] being the config iq array.
*/
#define IOCTL_SFCDRV_HUNT                                       0x83002120 //25
/*
input nothing, return nothing
Issues HUNT command to cmdr.
*/

#define IOCTL_SFCDRV_GET_CLOCK                          0x83002124 //26
/*
input nothing, return CLKSET struct
Returns current clock setting, only valid after SET_CLOCK has been called.
*/
//											0x83002128 //27
//											0x8300212c //28
//											0x83002130 //29
//											0x83002134 //30
//											0x83002138 //31
//											0x8300213c //32
//											0x83002140 //33
//											0x83002144 //34
#define IOCTL_SFCDRV_WRITE_LB                           0x83002148 //35
/*
input ULONG[2] return nothing
Writes special configuration register of SuperFastcom board.
Writes (char)ULONG[1] to localbus base + ULONG[0]
(see page 20 of the SuperFastcom hardware reference manual pdf file
(printed page# 14, section: PROGRAMMING) for a description of these
registers).
*/
#define IOCTL_SFCDRV_READ_LB                            0x8300214c //36
/*
input ULONG return ULONG
Reads special configuration register of SuperFastcom board.
Returns value of localbus base + ULONG(in) (character value returned in a
ULONG)
*/
#define IOCTL_SFCDRV_SET_CLOCK2                         0x83002150 //37
/*
input clkset structure, return nothing
Sets the second programable clock output (progclk+/- on cable#1)
*/
#define IOCTL_SFCDRV_GET_CLOCK2                         0x83002154 //38
/*
input nothing, return clkset struct
Returns second programmable clock value(s)
*/
#define IOCTL_SFCDRV_GET_DESC_PARAMETERS                0x83002158 //39
/*
input nothing, return ULONG[4]
returns descriptor settings
ULONG[0] = #rx descriptors
ULONG[1] = size of rx descriptor (data area)
ULONG[2] = #tx descriptors
ULONG[3] = size of tx descriptor (data area)
*/
#define IOCTL_SFCDRV_SET_CHECK_TIMEOUT                  0x8300215c //40
/*
(IRQ rate reduction ioctls)
input ULONG, return nothing
Sets timeout value to ULONG mS.  The driver will check for new
data(incomming/outgoing) every ULONG mS.  Default setting is 750mS
*/
#define IOCTL_SFCDRV_SET_RX_IRQ_RATE                    0x83002160 //41
/*
(IRQ rate reduction ioctls)
input ULONG, return nothing
Sets HI bit of descriptor every ULONG descriptors.
1 = irq every descriptor
2 = irq every other descriptor
...etc
0 = irq once per all descriptors allocated
*/
#define IOCTL_SFCDRV_SET_TX_IRQ_RATE                    0x83002164 //42
/*
(IRQ rate reduction ioctls)
input ULONG, return nothing
Sets HI bit of descriptor every ULONG descriptors.
1 = irq every descriptor
2 = irq every other descriptor
...etc
0 = irq once per all descriptors allocated
*/
#define IOCTL_SFCDRV_SET_RX_FI_MASK                     0x83002168 //43
/*
(IRQ rate reduction ioctls)
input ULONG, return nothing
0 = unmasked
1 = masked
Unmasked will generate interrupt (check) every time a frame end is
encountered
(end of HDLC frame, or timeout interrupt in async mode)
*/
#define IOCTL_SFCDRV_SET_TX_FI_MASK                     0x8300216c //44
/*
(IRQ rate reduction ioctls)
input ULONG, return nothing
0 = unmasked
1 = masked
Unmasked will generate interrupt (check) every time a frame end is
encountered
*/

//		Custom controls
//		0x83002170 //45
//		0x83002174 //46
//		0x83002178 //47
//		0x8300217c //48



#define IOCTL_SFCDRV_SET_RECEIVE_MULTIPLE               0x83002180 //49
/*
input ULONG, return nothing
Sets number of frames to return per ReadFile function call.
0 = one frame per ReadFile
ULONG = ULONG frames per ReadFile
*/

//		Custom controls
//		0x83002184 //50
//		0x83002188 //51


#define IOCTL_SFCDRV_IMMEDIATE_STATUS			 0x8300218c //52
/*
Same as IOCTL_SFCDRV_STATUS but doesn't block.  Will return 0 if no status
events currently available.

input ULONG, return ULONG
Input is a ULONG that will mask off the corresponding interrupt according to the 
vector table if you set the corresponding bit to 0.
Returns masked table of interrupt vectors.

 		31	30	29	28	27	26	25	24
	----------------------------------------------------------
HDLC	1	0	0	0	0	0	0	0	
ASYNC	1	0	0	0	0	0	0	0
BISYNC	1	0	0	0	0	0	0	0	

		23	22	21	20	19	18	 17	16	15	 14	 13	 12
	--------------------------------------------------------------------------------------------
HDLC	0	HI	FI	ERR	0	ALLS 0	XDU	 TIN CSC XMR XPR
ASYNC	0	HI	FI	ERR	0	ALLS 0	XOFF TIN CSC XON XPR
BISYNC	0	HI	FI	ERR	0	ALLS 0	XDU	 TIN CSC XMR XPR

		11	10	9	8		7	6	 5	  4		3	 2	  1		0
	--------------------------------------------------------------------------------------------
HDLC	0	0	0	0		RDO	RFS	 RSC  PCE	PLLA CDSC RFO	FLEX
ASYNC	0	0	BRK	BRKT	TCD	TIME PERR FERR	PLLA CDSC RFO	0
BISYNC	0	0	0	0		TCD	0	 PERR SCD	PLLA CDSC RFO	0

NOTE:  (22:20) See Table 103 PEB20534 manual
       (19:0) See Table 104 PEB20534 manual

*/
#define IOCTL_SFCDRV_BLOCK_MULTIPLE_IO			0x83002190 //53
/*
input ULONG, return nothing
0 = Can call CreateFile as many times as you like on a single port
1 = Additional CreatFile(s) will fail on an already open port
Default value = 0
*/

#define IOCTL_SFCDRV_SET_FREQUENCY                      0x83002194 //54
/*
input ULONG[2], return nothing
sets the onboard frequency generator.
ULONG[0] = the frequency to set
ULONG[1] = clock type
clock type:
5 = ICD2053B
3 = ICS307
0 = FS6131

 alternate:
you can pass a single ULONG as the frequency, and the code will try to autodetect
which clock generator you have.  There are instances where this is known not to work.
(it can not tell the difference between a SuperFastcom/2-104-ET and a SuperFastcom/2-104)

The frequency can be any in the range 1000000 to 33333333 for the ICD2053B or FS6131
The frequency can be any in the range 6000000 to 33333333 for the ICS307

 ICD2053B is on the SuperFastcom, SuperFastcom/232, SuperFastcom/2-104 (commercial temp) rev 1.0 
 ICS307   is on the SuperFastcom/2-104-ET rev 2.0, and SuperFastcom/2-104 rev 2.0
 FS6131   is on the SuperFastcom/2-104-ET rev 1.0

*/

#define IOCTL_SFCDRV_SET_FREQUENCY2                    0x83002198 //55
/*
input ULONG[2], return nothing
sets the onboard frequency generator #2.
ULONG[0] = the frequency to set
ULONG[1] = clock type
clock type:
5 = ICD2053B
3 = ICS307
0 = FS6131

 alternate:
you can pass a single ULONG as the frequency, and the code will try to autodetect
which clock generator you have.  There are instances where this is known not to work.
(it can not tell the difference between a SuperFastcom/2-104-ET and a SuperFastcom/2-104)
*/

#define IOCTL_SFCDRV_GET_FREQUENCY                      0x8300219C //56
/*
input nothing, return ULONG
returns the frequency set via IOCTL_SFCDRV_SET_FREQUENCY
*/
#define IOCTL_SFCDRV_GET_FREQUENCY2                    0x830021A0 //57
/*
input nothing, return ULONG
returns the frequency set via IOCTL_SFCDRV_SET_FREQUENCY2
*/
#define IOCTL_SFCDRV_CONVERT_LSB_TO_MSB					0x830021A4 //58
/*
input ULONG return nothing
0 = no conversion
1 = convert from LSB first data to MSB first data

the 20534 only sends/receives LSB first in hardware.  This ioctl
will do a software conversion on all data passed back and forth
from the driver to mirror every byte.  The conversion is equivalent to 
running convertlsb2msb() from the lsb2msb example code on your buffer(s).
A side effect of this function is that if you are in HDLC mode the RSTA byte will be
mirrored, and you must convert it back or change your definition of each bit.
*/


#define IOCTL_SFCDRV_SET_TIMED_TRANSMIT                 0x830021A8 //59
/*
input ULONG return nothing

0 = no timed transmit
1 = timed transmit one per TIN
2 = timed transmit one per CTS (low) (falling edge of CTS+)
3 = timed transmit one per CTS (high)(rising edge of CTS+)
4 = timed transmit one per CD (high)  (rising edge of CD+)
5 = timed transmit one per CD (low) (falling edge of CD+)
default value =0
notes:
the ICD bit has no effect on this. (ie #4 and #5 only depend on the CD+/- pins, ICD does not effect it)

restrictions:
the frames to be transmitted on each event must fit completely in a descriptor.
you then queue up the writes (fill all the descriptors) and 
the driver will send one descriptor(/frame) per event.
*/

#define IOCTL_SFCDRV_BISYNC_START_PATTERN_MATCH_EN      0x830021AC //60
#define IOCTL_SFCDRV_SET_BISYNC_START_PATTERN           0x830021b0 //61

#define IOCTL_SFCDRV_SET_TIMETAG_CONTROL				0x830021b4 //62



/* Define Macro to help simplify Error Messages. returns pointer to string to print. */
#include <windows.h>
#define PRINT_ERROR(String) FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER|FORMAT_MESSAGE_FROM_SYSTEM|FORMAT_MESSAGE_IGNORE_INSERTS,NULL,GetLastError(),0,(LPTSTR)&String,0,NULL);


//device io control STATUS function return values
#define ST_HI		0x00400000	//Host Initiated interrupt
#define ST_FI		0x00200000	//Frame Indication interrupt
#define ST_ERR		0x00100000	//Error indication interrupt
#define ST_ALLS		0x00040000	//All Sent interrupt
#define ST_XDU		0x00010000	//Transmit Data Underrun interrupt
#define ST_XOFF		0x00010000	//XOFF character detected interrupt
#define ST_TIN		0x00008000	//Timer interrupt
#define ST_CSC		0x00004000	//CTS Status Change
#define ST_XMR		0x00002000	//Transmit Message Repeat
#define ST_XON		0x00002000	//XON character detected interrupt
#define ST_XPR		0x00001000	//Transmit Pool Ready interrupt
#define ST_BISYNC_RESTART 0x00000400      //bisync pattern match forced restart
#define ST_BRK		0x00000200	//Break interrupt
#define ST_BRKT		0x00000100	//Break Terminiated interrupt
#define ST_RDO		0x00000080	//Receive Data Overflow interrupt
#define ST_TCD		0x00000080	//Termination Character Detected interrupt
#define ST_RFS		0x00000040	//Receive Frame Start interrupt
#define ST_TIME		0x00000040	//Time out interrupt
#define ST_RSC		0x00000020	//Receive Status Change interrupt
#define ST_PERR		0x00000020	//Parity Error interrupt
#define ST_PCE		0x00000010	//Protocol Error interrupt
#define ST_FERR		0x00000010	//Framing Error interrupt
#define ST_SCD		0x00000010	//SYN Character Detected interrupt
#define ST_PLLA		0x00000008	//DPLL Asynchronous interrupt
#define ST_CDSC		0x00000004	//Carrier Detected Status Change interrupt
#define ST_RFO		0x00000002	//Receive FIFO Overflow interrupt
#define ST_FLEX		0x00000001	//Frame Length Exceeded interrupt


#pragma pack(push)
#pragma pack(1)

/*Structure for IOCTL_SFCDRV_SCC_REGISTERS_SET*/
typedef struct
{	/* Registers that are used in BISYNC, ASYNC, and HDLC modes.,and/or Read/Write */
	long cmdr;		/* H/A/B 	  w 	*/
	long star;		/* H/A/B 	r 		*/
	long ccr0;		/* H/A/B 	r/w 	*/
	long ccr1;		/* H/A/B 	r/w 	*/
	long ccr2;		/* H/A/B 	r/w 	*/
	long accm;		/* H 		r/w 	*/
	long udac;		/* H 		r/w 	*/
	long ttsa;		/* H/A/B 	r/w 	*/
	long rtsa;		/* H/A/B 	r/w 	*/
	long pcmmtx;	/* H/A/B 	r/w 	*/
	long pcmmrx;	/* H/A/B 	r/w 	*/
	long brr;		/* H/A/B  	r/w 	*/
	long timr;		/* H/A/B 	r/w 	*/
	long xadr;		/* H  		r/w 	*/
	long radr;		/* H  		r/w 	*/
	long ramr;		/* H  		r/w 	*/
	long rlcr;		/* H  		r/w 	*/
	long xnxfr;		/*   A  	r/w 	*/
	long tcr;		/*   A/B  	r/w 	*/
	long ticr;		/*   A  	r/w 	*/
	long syncr;		/* H/A/B 	r/w 	*/
	long imr;		/*     B	r/w 	*/
	long isr;		/* H/A/B  	r 	`	*/
	long gap[9];	/* Does Nothing */	
} SCC_REGS;

/* Structure for FIFO. IOCTL_SFCDRV_FIFO_CHNG */
typedef struct sfcfifo
{
	unsigned long fifocr1;
	unsigned long fifocr2;
	unsigned long fifocr3;
	unsigned long fifocr4;
} FIFO_REGS;



/* dscc_clkset: declare this as a variable in your program if you want to change external clock 
 * rate, then call function: calculate_bits( 18432000.0 , DESIRED_FREQUENCY, double *actual_clock, clkset &);
 * after this check if actual_clock is the value you set the clock to or close to it, if not you will have
 * to call this function with a different value, it should be easily set the first time.  Once you have 
 * the clock value right then call the DeviceIoControl with the change clock IOCTL with &clkset as the input */
typedef struct dscc_clkset
{
	unsigned long clockbits;
	unsigned long numbits;
	double actualclock;
} clkset;



/* DESC_SET: Declare this struct when there needs to be a change in the size and length of the
 * Rx/Tx Descriptors and Data Frames.  Read the Tech Docs on how to set this. */
typedef struct desc_descriptor
{
	unsigned long RxDescLen;/* Number of Descriptors not how many bytes */
	unsigned long RxBufSize;/* Has to be a Multiple of 4 bytes, bytes per descriptor not how much total */
	unsigned long TxDescLen;/* Number of Descriptors not how many bytes */
	unsigned long TxBufSize;/* Has to be a Multiple of 4 bytes, bytes per descriptor not how much total  */
} DESC_SET;

#define MAX_PATTERN_RECOG 16 //used for bisync pattern recognition 

typedef struct bisync_start_pattern{
	ULONG count;
	UCHAR pattern[MAX_PATTERN_RECOG];
} BISYNCSTART;


#pragma pack(pop)
#endif
/* $Id$ */
