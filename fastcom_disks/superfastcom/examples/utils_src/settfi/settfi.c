/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
settfi.c -- user mode function to mask/unmask the frame end interrupt indication for transmit descriptors
                     

usage:
 settfi port value

port is any valid SFC port (0,1,2,3).

value :
0 => unmask the FI interrupts
1 => mask the FI interrupt
   

*/


#include <windows.h>
#include <stdio.h>
#include <math.h> /* floor, pow */

#include "..\sfc.h"



int main(int argc, char * argv[])
{
	//clkset clock;
	char nbuf[80];
	int port,t;
	HANDLE hDevice; 
    unsigned long reg,val,temp;

	unsigned long passval[2];

	if(argc<3) {
                printf("usage:\n settfi port value\n");
		exit(1);
	}

	port = atoi(argv[1]);
    val = atol(argv[2]);


		
		passval[0] = val;

        sprintf(nbuf,"\\\\.\\SFC%u",port);

	printf("Opening: %s\n",nbuf);

	if((hDevice = CreateFile (
			nbuf, 
			GENERIC_READ | GENERIC_WRITE,
			0,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
			NULL)) == INVALID_HANDLE_VALUE ) 
	{
                printf("Can't get a handle to sfcdrv @ %s\n",nbuf);
		exit(1);
	}

        t = DeviceIoControl(hDevice,
                        IOCTL_SFCDRV_SET_TX_FI_MASK,                            /* Device IOCONTROL */
			&passval,								/* write data */
			sizeof(unsigned long ),						/* write size */
			NULL,								/* read data */
			0,									/* read size */
			&temp,								/* Returned Size */
			NULL); /* overlap */
        if(t)  
		{
			if(val==0)printf("set SFC%d TX FI unmasked\n",port);
			if(val==1)printf("set SFC%d TX FI masked\n",port);
		}
        else printf("Problem TX FI mask state on SFC%d\n",port);/* check owners manual */
	CloseHandle(hDevice);
	return 0;
}
/* $Id$ */