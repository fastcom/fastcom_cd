/* $Id$ */
/*
Copyright(c) 2002 Commtech, Inc.
loop.c -- a user mode program to effect a loopback on a SuperFastcom channel

 usage:
  loop port


*/


#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "..\sfc.h"

#define ASYNCMODE 3
#define BISYNCMODE 2
#define HDLCMODE 1

int main(int argc, char *argv[])
{
	HANDLE wDevice;/* Handle for the SuperFastcom port */
	HANDLE rDevice;
	ULONG t;
	DWORD nobyteswritten;
	DWORD nobytesread;
	char tdata[10000];
	char rdata[10000];
	OVERLAPPED  wq,rq;
	int j,x,error,tosend;
	ULONG totalsent;
	ULONG totalread;
	int i;
	ULONG totalerror;
	ULONG error_av_frame;
	ULONG loop;
    char devname[25];
	ULONG type;
	
	if(argc<3)
	{
		printf("usage:\nloopback X Y\n");
		printf("      X port number\n");
		printf("      Y type [a=async b=bisync h=hdlc]\n");
		
		exit(1);
	}
	srand( (unsigned)time( NULL ) );
	
    if(argc>=2) sprintf(devname,"\\\\.\\SFC%d",atoi(argv[1]));
	printf("devicename:%s\n",devname);
	
	if(argv[2][0] == 'a') type =ASYNCMODE;
	else if(argv[2][0] =='b') type =BISYNCMODE;
	else if(argv[2][0] =='h') type =HDLCMODE;
	else type =HDLCMODE;//default
	memset( &wq, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
	wq.hEvent = CreateEvent( NULL,    // no security
		TRUE,    // explicit reset req
		FALSE,   // initial event reset
		NULL ) ; // no name
	if (wq.hEvent == NULL)
	{
		//MessageBox( NULL, "Failed to create event for thread!", "main Error!",
		//	MB_ICONEXCLAMATION | MB_OK ) ;
		printf("Failed to create event for thread!\n");
		return 1; 
	}
	//printf("write overlapped event created\n");
	wDevice = CreateFile (devname,
		GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL,
		OPEN_ALWAYS,
		FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
		NULL
		);
    if (wDevice == INVALID_HANDLE_VALUE)
    {
		//for some reason the driver won't load or isn't loaded
		printf ("Can't get a handle to sfcdrv\n");
		exit(1);
		//abort and leave here!!!
	}
	//printf("sfcdrv handle created\n");
	
	memset( &rq, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
	rq.hEvent = CreateEvent( NULL,    // no security
		TRUE,    // explicit reset req
		FALSE,   // initial event reset
		NULL ) ; // no name
	if (rq.hEvent == NULL)
	{
		//MessageBox( NULL, "Failed to create event for thread!", "main Error!",
		//	MB_ICONEXCLAMATION | MB_OK ) ;
		printf("Failed to create event for thread!\n");
		return 1; 
	}
	//printf("read overlapped event created\n");
	/*
	rDevice = CreateFile ("\\\\.\\SFC1",
	GENERIC_READ | GENERIC_WRITE,
	FILE_SHARE_READ | FILE_SHARE_WRITE,
	NULL,
	OPEN_ALWAYS,
	FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
	NULL
	);
	
	  if (rDevice == INVALID_HANDLE_VALUE)
	  {
	  //for some reason the driver won't load or isn't loaded
	  printf ("Can't get a handle to sfcdrv\n");
	  exit(1);
	  //abort and leave here!!!
	  }
	*/
	rDevice = wDevice;
	/* Flush the RX Descriptors so not as to have any complete descriptors in their
	* the first read in hdlc will get those left over frames and this test program
	* would not be of any use. */
	//printf("flush rx\n");
	DeviceIoControl(rDevice,IOCTL_SFCDRV_FLUSH_TX,NULL,0,NULL,0,&t,NULL);
	DeviceIoControl(rDevice,IOCTL_SFCDRV_FLUSH_RX,NULL,0,NULL,0,&t,NULL);
	
	totalerror=0;
	totalsent=0;
	totalread=0;
	loop=0;
	
	while(!kbhit())
	{
		error=0;
		
		//generate a random length 1 - 4096 bytes
		tosend=(ULONG)(rand() % 4096);
		//tosend = atoi(argv[2]);
		if(tosend==0) tosend=1;
		printf("tosend:%d\n",tosend);
		//generate a random string of our random length.
		for(x=0;x<tosend;x++) 
		{
			tdata[x]=(UCHAR)(rand());
			if((type==BISYNCMODE)&&((tdata[x]&0xff)==0xff)) tdata[x]=0x01;//cant have termination character (0xFF) in the data!!!
		}
		//printf("pre-write\n");
		if(type==BISYNCMODE)
		{
			DeviceIoControl(rDevice,IOCTL_SFCDRV_HUNT,NULL,0,NULL,0,&t,NULL);
		}
		t = WriteFile(wDevice,&tdata,tosend,&nobyteswritten,&wq);
		
		//Sleep(1);	
		
		if(t==FALSE)  
		{
			t=GetLastError();
			printf("WRITE ERROR: #%d\n",t);
			
			if(t==ERROR_IO_PENDING)
			{
				do
				{
					j = WaitForSingleObject( wq.hEvent, 1500 );//1.5 second timeout
					if(j==WAIT_TIMEOUT)
					{
						printf("Transmitter Locked up... Resetting TX.\r\n");
						//DeviceIoControl(wDevice,IOCTL_SFCDRV_FLUSH_TX,NULL,0,NULL,0,&returnsize,NULL);
					}
					if(j==WAIT_ABANDONED)
					{
						printf("Recieve Abandoned.\r\n");
						goto close;
					}
				} while(j!=WAIT_OBJECT_0);
			}
		}
		t = ReadFile(rDevice,&rdata,8000,&nobytesread,&rq);
		if(t==FALSE)  
		{
			t=GetLastError();
			if(t==ERROR_IO_PENDING)
			{
				do
				{
					j = WaitForSingleObject( rq.hEvent, 5000 );//5 second timeout
					if(j==WAIT_TIMEOUT)
					{
						printf("Reciever Locked up... Resetting RX.\r\n");
						//DeviceIoControl(rDevice,IOCTL_SFCDRV_FLUSH_TX,NULL,0,NULL,0,&returnsize,NULL);
					}
					if(j==WAIT_ABANDONED)
					{
						printf("Reciever Abandoned.\r\n");
						goto close;
					}
				} while(j!=WAIT_OBJECT_0);
				GetOverlappedResult(rDevice,&rq,&nobytesread,TRUE);
			}
			else printf("READ ERROR: #%x\n",t);
		}
		//else printf("read returned true:%d\n",nobytesread);
		
		//printf("[%d]READ %d\n\n",t,nobytesread);
		if((type==HDLCMODE)||(type==BISYNCMODE))
		{
			//hdlc has one extra byte (RSTA) and bisync has one extra byte (TERMINATION character 0xff)
			
			if(nobytesread!=tosend+1)
			{
				printf("Byte Count ERROR rec:%d != sent:%d\n",nobytesread,tosend+1);
				error++;
			}
		}
		else if(type==ASYNCMODE)
		{
			if(nobytesread!=tosend)
			{
				printf("Byte Count ERROR rec:%d != sent:%d\n",nobytesread,tosend);
				error++;
			}
		}
		
		
		if(nobytesread!=0)
		{
			if(nobytesread<8000)
				if((type==HDLCMODE)||(type==BISYNCMODE))
				{
					for(x=0;x<nobytesread-1;x++)
						if(rdata[x]!=tdata[x])	error++;
				}
				else 
				{
					//async
					for(x=0;x<nobytesread;x++)
						if(rdata[x]!=tdata[x])	error++;
				}
		}
		//printf("Found: %d errors\n",error);
		totalerror +=error;
		loop++;
		totalsent+=nobyteswritten;
		totalread+=nobytesread;
		printf("loop:%d\r",loop);
	}
	getch();
	printf("Found %d errors out of %d frames\n",totalerror,loop);
	printf("Wrote %d bytes.\n",totalsent);
	if((type==HDLCMODE)||(type==BISYNCMODE))
	{
		printf("Read %d bytes.\n",totalread-loop);
	}
	else
	{
		printf("Read %d bytes.\n",totalread);
	}
	
close:
	CloseHandle (wDevice);
	//CloseHandle (rDevice);
	
	return 0;
}
/* $Id$ */