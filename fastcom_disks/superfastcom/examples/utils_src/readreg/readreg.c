/* $Id$ */
/*
Copyright(c) 2002, Commtech, Inc.
readreg.c -- user mode function to read a 20534 register

usage:
 readreg port register_offset

 The port can be any valid sfc port (0,1,2,3) and it doesn't matter which
 port you select, only you must be able to successfuly open it
 (ie can't be in use by another program/user).

 The register_offset can be any in the range 0 to 0x0408, and is taken as a
 hex value from the command line.  This is the register offset as given in 
 the PEB20534 data sheet.

*/


#include <windows.h>
#include <stdio.h>
#include <math.h> /* floor, pow */

#include "..\sfc.h"



int main(int argc, char * argv[])
{
	//clkset clock;
	char nbuf[80];
	int port,t;
	HANDLE hDevice; 
    unsigned long reg,val,temp;
	
	if(argc<3) {
                printf("usage: readreg port register\n");
		exit(1);
	}

	port = atoi(argv[1]);
        sscanf(argv[2],"%lx",&reg);

        sprintf(nbuf,"\\\\.\\SFC%u",port);

	printf("Opening: %s\n",nbuf);

	if((hDevice = CreateFile (
			nbuf, 
			GENERIC_READ | GENERIC_WRITE,
			0,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
			NULL)) == INVALID_HANDLE_VALUE ) 
	{
                printf("Can't get a handle to sfcdrv @ %s\n",nbuf);
		exit(1);
	}

        t = DeviceIoControl(hDevice,
                        IOCTL_SFCDRV_READ_REGISTER,                             /* Device IOCONTROL */
			&reg,								/* write data */
			sizeof(unsigned long ),						/* write size */
			&val,								/* read data */
			sizeof(unsigned long),									/* read size */
			&temp,								/* Returned Size */
			NULL); /* overlap */
        if(temp!=0)  printf("in from register:%lx -> %8.8lx\n",reg,val);
        else printf("Problem reading register:%lx.\n",reg);/* check owners manual */
	CloseHandle(hDevice);
	return 0;
}

/* $Id$ */