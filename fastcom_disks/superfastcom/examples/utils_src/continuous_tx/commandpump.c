/*
Copyright (C) 2005 Commtech, Inc. Wichita, KS.

Module name:
  
	commandpump.c

Abstract:

	Win32 console program that demonstrates how to connect to the named
	pipe created using the continuous_write example program.
	essentially opens the \\.\pipe\commandpipe named pipe
	and sends data to the pipe to be sent out the SuperFastcom.

Environment:

    user mode only, compiled using Visual C++ ver 6.0

Notes:

    
Revision History:

    1/19/2005   started (modified commandpump.c from escc tree)
	8/10/2005	modified to optionally accept a file as the output.  mds

*/

#include "windows.h"
#include "stdio.h"
#include "conio.h"
#include "time.h"

int main(int argc, char *argv[])
{
	HANDLE	commandpipeh;	//handle for named pipe
	char	*data;	//data array for command to send
	DWORD	i;		//temp vars		
	BOOL t;
	DWORD	byteswritten;	//holds number of bytes written out pipe
	DWORD	framecount;		//frame counter
	unsigned long writesize;
	unsigned long loopcnt;
	unsigned long bytecnt;
	FILE *fd;
	unsigned long size;
	
	if((argc!=2)&&(argc!=3))
	{
		printf("USAGE:\t%s blocksize [file_to_send]\n",argv[0]);
		printf("\tNote: If you do not specify a file, random data will be generated\n");
		exit(1);
	}
	else if (argc==3)
	{
		if((fd = fopen(argv[2],"rb"))==NULL)
		{
			printf("Cannot open specified file %s\r\n",argv[2]);
			exit(1);
		}
		size=0;
		
		fseek(fd,0,SEEK_END);
		size = ftell(fd);
		fseek(fd,0,SEEK_SET);
		printf("%s: %d bytes\n",argv[2],size);

	}
	srand(time(NULL));
	writesize=atoi(argv[1]);
	framecount=0;			//start frame count at 0
	//create the named pipe (open it actually)
	commandpipeh = CreateFile("\\\\.\\pipe\\commandpipe",GENERIC_WRITE,0,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
	if(commandpipeh==INVALID_HANDLE_VALUE)
	{
		printf("cannot open command pipe\r\n");
		exit(1);
	}
	data = (char*)malloc(writesize);
	if(data==NULL)
	{
		printf("cannot allcoate memory for buffer\r\n");
		exit(1);
	}
	loopcnt=0;
	bytecnt=0;
	
	printf("Press any key to terminate the command pump.\n");
	do
	{
		if (argc!=3)
		{
			for(i=0;i<writesize;i++) data[i] = (rand()>>8)&0xff;
			t = WriteFile(commandpipeh,&data[0],writesize,&byteswritten,NULL);
		}
		else
		{
			size= fread(data,1,writesize,fd);
			if (feof(fd)) goto end;
			t = WriteFile(commandpipeh,&data[0],size,&byteswritten,NULL);
		}

		if(t==FALSE)
		{
			i=GetLastError();
			printf("WritePipe error:%8.8x\r\n",i);
		}
		//printf("loop:%ld, byte:%ld\r\n",loopcnt,bytecnt);
		loopcnt++;
		bytecnt+=byteswritten;
		if((loopcnt%1000)==0) printf("loop:%ld, bytes:%ld\r\n",loopcnt,bytecnt);
		//need to sleep/delay a very short bit of time here, as to 
		//simulate data that is "coming in" from a source, as if we let it
		//free-run, we will definately just "work" by filling the pipe.
	}while(!kbhit());
	getch();
end:
	//done with the pipe
	CloseHandle(commandpipeh);
	//done
}
//$Id$