This example will allow you to maintain a sustained continuous transmit by using 
a named pipe (command pump) to queue data to be sent out.

Open two command prompt windows and run the commands like this:

Window 1
setbufs 0 50 4096 1000 4096 (only run this command once or set as boot defaults!)
continuous_write.exe 0 1000000

    where 0 is the sfc port to be used and 1000000 is the data rate

Window2
commandpump 1034

    Where 1034 is the blocksize that you want


See the comments in the .c files for a better understanding of what is going on.