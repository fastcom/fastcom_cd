/*
continuous_write.c
  
	Win32 continuous write example program
	This program Configures a SuperFastcom Port to output data continuously 
	at a given frequency.  
	The input stream is coming from a named pipe (\\\\.\\pipe\commandpipe)
	This program reads bytes from the named pipe, aggregates them and writes
	them out the SuperFastcom port.

  In order to achieve continouous data output you must have a fair bit of buffering
  in the driver, so a few words about how the driver and 20534 work as far as sending
  data goes will follow.

  The driver buffering scheme used is one of a circular queue of buffers each of a given size.
  The buffering parameters are set (sfc_advanced_settings.exe, or setbufs.c) as the 
  number of buffers (ntbufs) and the size of each buffer (ntbufsize).
  Given these two parameters and a outgoing bitrate the maximum achievable ammount
  of time buffering available is (ntbufs*ntbufsize*8/bitrate).  The minimum ammount of 
  time buffering available is (ntbufs*1*8/bitrate).  The driver takes each WriteFile()
  call and moves the data to send into the driver buffers, then tells the 20534 to "go".
  If the WriteFile() consists of a loop such as:
  while(1)
	{
	WriteFile(hdev,&data[0],1,&bytesout,&os);
	...(overlapped handling)...
	}
  Then each write of 1 byte will occupy one driver buffer, effectively giving only
  ntbufs bytes of buffering.
  It should be obvious from this that to achieve continuous data output that you should 
  avoid this situation (filling the buffers with less than their maximum ammount of data).
  Given that the 20534 has no "frame" boudnaries in extended 
  transparent mode (we are sending raw bits here), there is no reason not to pass ntbufsize ammounts of data to the
  driver on each write, as passing anything less will simply reduce the total ammount 
  of time buffering available.
  For a simple rendering of what time buffering means, it is basically the ammount of time
  that you can not call WriteFile() and still have data transmitting (assuming you have the
  buffers filled to start with).  Before you say, "but I am continuously calling WriteFile()",
  I caution you that you are running windows, and in general you do not "own" the CPU, 
  other tasks/processes/drivers are constantly taking turns passing CPU time around.  It is 
  quite possible that a long period of time can go by without your thread running.
  (long period of time is a bit nebulous, as windows is far from a RTOS, I tend to err on
  the side of caution, and give multiple seconds worth of buffering as a guideline, but there
  is nothing to say that there is ever "enough", I just figure that if a couple of seconds
  has gone by and my thread has not executed, then the system is probably hosed, and discontinuous data
  output is the least of ones problems)  Your level of comfort should be arrived at by 
  witnessing/tinkering on your actual system, as there is no one size fits all answer here.

  So goal #1 of this code is to have the basic form of the write loop be:
  while(1)
	{
	WriteFile(hdev,&data[0],ntbufsize,&bytesout,&os)
	...overlapped handling...
	}

  The code below actually uses 2*ntbufsize, but any integer multiple would do.


  Second, if you start from a non transmitting state, and send a write that fills
  one descriptor, you have a boundary condition, where you must call another write
  within (ntbufsize*8/freq) seconds.  In general (if your data is coming in at about
  the same rate that it is going out, then you are always living on the edge of running 
  out of data.  To compensate for this, we are going to prefill the drivers write buffers
  by issuing a single write at the beginning for (all but one of the buffers).  This
  starts out the routine giving us about the maximum time buffering available, and allows 
  for some play in the acquiring and transmitting of the next block of data.
  You can prefill any ammount of data that you want, To make sure it will work, I would
  make sure that you send at least 2*ntbufsize bytes as the first write, and then ensure
  that you call WriteFile() again before ntbufsize*8/freq seconds expire.  Doing such 
  will guarantee that the 20534 will not have processed the second descriptor (buffer)
  before the new one is added on.

  Third, the 20534 has issues with HI/FI interrupts.  The issue is that it will not 
  process the "last" descriptor (buffer) if HI/FI interrupts are enabled.  This is 
  usually not an issue for continuous sending (as if you never want to reach the 
  "end" of the buffers)  And is only a concern if you are sending "single" frames with
  idle between them. (where the first write isn't sent until the second one is issued,
  etc.)  The HI/FI interrupts are disabled by default, and as such, the only way that
  the blocked WriteFile() is checked for the ability to unblock is on (any other interrupt condition)
  or maximaly at the setchecktimeout boundaries.  By default this is 750ms.  It can be
  set to anything, but just make sure that the checktimeout is less than half of the
  time buffering available, (preferably much less).  I chose 1/10th of the available buffering,
  which means that the buffer in use will bounce between full and 8/10ths full. (depending 
  on the actual numbers.
  If you are really paranoid, you can turn on the HI interrupts, and tune the checking 
  to happen as each buffer is consumed by the 20534.  This is all simply a trade off between
  processing (CPU usage) and buffer utilization.  The more frequently the buffers are checked
  the more (on average) the buffers will be "full", the down side is that it will take lots 
  of CPU cycles to do it.  By setting the checktimeout at 1/10th of the buffer size the 
  driver only checks to see if there is room to complete a write every (checktimeout) miliseconds.
  This should work OK as long as the data streaming into the pipe comes in fast enough to 
  refill the 20% of the buffers that is potentially empty.  Basically depending on the 
  numbers the writing code will have bursts of heavy activity, with up checktimeout ammount of 
  blocked/idle.
  It may be better here to change the number of blocks written into the driver to be 
  the same as the checktimeout value, particularly if the data coming into the pipe 
  comes in at about the same rate as the data going out the port.

basic operation goes something like this:

  prefill (ntbufs-1)*ntbufsize bytes and send out the SFC port.
  2*ntbufsize bytes are pulled from the named pipe. (in whatever form the pipe wants to give them to me)
  (note I tried the simple give me X bytes from the pipe, but it seems to give whatever it feals like, and blocks forever if you peek the pipe
  waiting for that X # bytes...arg, no idea why, but thats how it is).
  2*ntbufsize bytes are written to the SuperFastcom. (this WriteFile() blocks, as there isn't room in the driver buffers to store it).
  (as the checktimeout runs async to this write, somewhere between 0 and checktimeout seconds elapse)
  (assuming that the elapsed time is greater than ntbufsize*8/freq seconds) the 
  WriteFile() completes and the driver buffers are "full" (and slowly leaking data out the port)
  2*ntbufsize bytes are pulled from the named pipe again
  WriteFile is issued to the SuperFastcom and blocks.
  checktimeout time elapses and the write is completed.
  at this point checktimeout represents (ntbufs/10) buffers, which means that 
  when WriteFile is called at this point it should return TRUE about (ntbufs/10)/2 times 
  (as we are sending 2 ntbufs at a time per WriteFile) as there is that much space 
  in the driver.  Finally a WriteFile occurs that there isn't room to process, it blocks
  and the cycle repeats.
  If for whatever reason, checktimeout+(ntbufsize*8/freq) seconds go by and the write
  has not unblocked, the probability of the transmission going discontinuous is high, 
  as it is likely that the 20534 is no longer pulling data in to the serial controller from
  the driver buffers.

  It has been done on the linux driver, that an ioctl was generated to determine the number
  of driver buffers either in use or empty, by calling this you can determine (before the
  serial controller runs out of data) that it is going to happen.  If this happens to be due
  to WriteFile not being called fast enough, you can hurridly get some data and push it out 
  to the driver, averting discontinutiy, however if it is due to the 20534 not pulling data
  out of the buffers anymore...the only way out is to flush and start over, which will
  force a discontinuity.

  As it is now once you start transmitting the detection of discontinuous output would be 
  by the occurance of an XDU interrupt. (as returned from the IOCTL_SFCDRV_STATUS, or IOCTL_SFCDRV_IMMEDIATE_STATUS function)

  On this note, it has been observed that drivers previous to 1/21/2005 will exhibit strange
  behavior, in that you will not obtain a XDU interrupt.  The explaination is fairly straightforward,
  in that the driver was setting the FE flag in the final descriptor of any given write, 
  and in so doing, the 20534 took the block as "complete" and generated an ALLS when that 
  descriptor was processed, and since it "knew" that there was an expected end of data it
  does not generate a XDU when the last bytes go out.  As a result of this all drivers
  previous to 1/21/2005 will be unable to detect idle going out on the line as a result of 
  not calling WriteFile() quickly enough.  (you will still get XDU interrupts as a result
  of PCI bus loading issues/inappropriate FIFOCR settings, as when the SCC runs out of data
  but there are still descriptors to be had, (ie problems transfering the data from 
  system memory to the chip quick enough)).  This case has been fixed as of 1/21/2005, 
  and now in extended transparent mode it acts as expected.  (Generating a XDU when
  the transmitter runs out of data).  One side effect of this is that there is no
  way to determine if it was a PCI loading/FIFOCR setting issue or a WriteFile speed issue.
  To make that determination one would have to call the (as yet nonexistant) ioctl functino
  that returns the number of descriptors (driver buffers) in use.  If the buffers were 
  not empty then the XDU was a result of PCI problems, if they are empty then it is a 
  WriteFile queueing issue.


*/
#include "windows.h"
#include "stdio.h"
#include "conio.h"
#include "..\sfc.h"

int main(int argc,char *argv[])
{
	DWORD		i,j,k;			//temp storage
	BOOL		t;				//temp storage
	OVERLAPPED	osw;			//overlapped struct for the write to SFC
	DWORD		writesize;		//number of bytes to write used for WriteFile to SFC
	DWORD		byteswritten;	//holds the result of the WriteFile() 
	DWORD		nobyteswritten;	//holds the result of the WriteFile() 
	HANDLE		inpipeh;		//handle to named pipe to get commands from
	DWORD		bytesread;		//number of bytes received from the pipe
	DWORD		loopcount;
	ULONG       totalbytestransmitted=0;
	ULONG		xducount=0;
	HANDLE		wDevice;
	SCC_REGS settings;
	char devname[25];
	ULONG desc_parm[4];
	char		*prefillbuf;
	char		*data;
	ULONG ntbufs;
	ULONG ntbufsize;
	ULONG maxtsize;
	ULONG freq;
	ULONG usebrr;
	ULONG clockfreq;
	ULONG m,n;
	ULONG passval[2];
	ULONG timeout;
	ULONG mask;
	ULONG status;
	ULONG truecount;
	ULONG reconnecting=0;
	ULONG sfctxtimeout=0;
	
	if (argc!=3)
	{
		printf("USAGE: %s port datarate\n");
		exit(1);
	}
	
	truecount=0;
	loopcount=0;
    sprintf(devname,"\\\\.\\SFC%d",atoi(argv[1]));
	printf("devicename:%s\n",devname);
	
	freq = atoi(argv[2]);
	if(freq<1000000)
	{
		usebrr=1;
		i=freq;
		while(i<1000000) i=i*2;
		clockfreq = i;
		k = i/freq;
		if((k-1)<64)
		{
			m=0;
			n=k-1;
		}
		else
		{
			j = k;
			for(m=1;m<15;m++)
			{
				j = j/2;
				if((j-1)<64)
				{
					//m=m 
					n=j-1;
					break;
				}
			}
		}
	}
	else 
	{
		clockfreq = freq;
		usebrr=0;
		m=0;
		n=0;
	}
	//there are other ways to arrive at this...this is but one of them.
	//not necessarily the "best" one either.  It may turn out that the 
	//resulting clock generator setting is not directly attainable, and by 
	//using a different factor a clock setting that was attainable could be had.
	//as it is this will do for now.
	
	
	wDevice = CreateFile (devname,GENERIC_READ | GENERIC_WRITE,FILE_SHARE_READ | FILE_SHARE_WRITE,NULL,OPEN_ALWAYS,FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,NULL);
    if (wDevice == INVALID_HANDLE_VALUE)
    {
		//for some reason the driver won't load or isn't loaded
		printf ("Can't get a handle to %s\n",devname);
		exit(1);
		//abort and leave here!!!
	}
	//printf("sfcdrv handle created\n");
	
	//configure port
	
	passval[0] = clockfreq;
	passval[1] = 5;//is icd2053b
	//passval[1] = 3;//is ics307
	//passval[1] = 0;//is fs6131
	if(	DeviceIoControl(wDevice,IOCTL_SFCDRV_SET_FREQUENCY,&passval[0],2*sizeof(ULONG),&i,sizeof(ULONG),&j,NULL))
	{
		printf("frequency set %lu\r\n",i);
	}
	else printf("Problem Setting clock frequency.\r\n");/* check owners manual */       
	
	
	
	memset(&settings,0,sizeof(SCC_REGS));
	
	settings.cmdr = 0x01010000;
	settings.star = 0x0;
	settings.ccr0 = 0x80000037;//clock mode 7
	settings.ccr1 = 0x0204e000;//extended transparent mode 0
	settings.ccr2 = 0x00040000;//start with hdlc receiver OFF (no receive)
	settings.imr = 0xFFFeFFFF;//disable all but XDU
	settings.timr = 0x070009ff;//perodic timer every 2560 txclock pulses (not used)
	settings.brr = ((m&0xf)<<8)|((n&0x3f));
	
	/*
	//these all default to 0, on the memset above
	settings.accm = value;
	settings.udac = value;
	settings.tsax = value;
	settings.tsar = value;
	settings.pcmmtx = value;
	settings.pcmmrx = value;
	settings.xadr = value;
	settings.radr = value;
	settings.ramr = value;
	settings.rlcr = value;
	settings.xnxfr = value;
	settings.tcr = value;
	settings.ticr = value;
	
	  settings.syncr = value;
	  settings.isr = 0x0;
	*/
	
	DeviceIoControl(wDevice,IOCTL_SFCDRV_SCC_REGISTERS_SET,&settings,sizeof(settings),NULL,0,&t,NULL);
	
	//disable hi/fi interrupts.
	//this bit can easily change performance...
	//for continuous operation it may be better to have the interrupts firing,
	//for discontinuous operation if hi/fi is enabled the last descriptor is not processed
	//(which obviously causes issues).  For continuous operation we should never get to 
	//the last descriptor so this shouldn't be a problem.
	//with these disabled (as per default) the servicing of writes occurs at the set check timeout boundary
	
	
	i=1;//mask FI interrupts
	DeviceIoControl(wDevice,IOCTL_SFCDRV_SET_TX_FI_MASK,&i,sizeof(unsigned long ),NULL,0,&t,NULL);
	
	//since we will be writing 400 frames per second, there will allways
	//be idle between frames, so we don't need any interrupts generated
	//to keep things going continuous...
	i=0; //no interrupts 
	DeviceIoControl(wDevice,IOCTL_SFCDRV_SET_TX_IRQ_RATE,&i,sizeof(unsigned long ),NULL,0,&t,NULL);
	
	
	
	DeviceIoControl(wDevice,IOCTL_SFCDRV_GET_DESC_PARAMETERS,NULL,0,&desc_parm,4*sizeof(ULONG),&t,NULL);
	ntbufs = desc_parm[2];
	ntbufsize = desc_parm[3];
	
	if( (((double)(ntbufs)*(double)(ntbufsize)*8.0)/(double)freq ) <2.0)
	{
		printf("driver allocated buffering too small\r\n");
		printf("only %.2f seconds of buffering allocated\r\n",(((double)(ntbufs)*(double)(ntbufsize)*8.0)/(double)freq ));
		printf("more than 2 seconds of buffering suggested for continuous operation\r\n");
		printf("either slow down the data rate, or increase the number (or size) of driver buffers!\r\n");
		//for debugging I took these out, so I could purposly break it
		//CloseHandle(wDevice);
		//exit(1);
	}
	
	timeout = ( (__int64)((__int64)ntbufs*(__int64)ntbufsize*8*1000)/freq)/10;
	passval[0]= timeout;// checktimeout, set to 1/10 of the max buffering time
	DeviceIoControl(wDevice,IOCTL_SFCDRV_SET_CHECK_TIMEOUT,&passval[0],sizeof(unsigned long ),NULL,0,&i,NULL);
	
	//end port configuration
	
	//start freash
	DeviceIoControl(wDevice,IOCTL_SFCDRV_FLUSH_TX,NULL,0,NULL,0,&t,NULL);
	
	//this flushes the status value(s)
	mask=0xFFFFFFFF;
	DeviceIoControl(wDevice,IOCTL_SFCDRV_IMMEDIATE_STATUS,&mask,sizeof(ULONG),&status,sizeof(ULONG),&t,NULL);
	
	maxtsize = (ntbufs-1)*ntbufsize; //prefill size, mostly full...
	
	prefillbuf = (char*)malloc(maxtsize+(2*ntbufsize));//sized to hold one extra read of ntbufsize (unpredictable pipe output)
	if(prefillbuf==NULL)
	{
		printf("unable to allocate prefill buffer\n");
		exit(1);
	}
	
	data = (char*)malloc(ntbufsize*4);//has to be at least big enough to hold 2*ntbufsize + ntbufsize bytes (what we are sending out the port + what could possibly be returned from the pipe (ie if we had (2*ntbufsize)-1 bytes and the pipe returned ntbufsize we would end up with (3*ntbufsize)-1 bytes in the buffer before we empty 2*ntbufsize out the WriteFile.
	if(data==NULL)
	{
		printf("unable to allocate data buffer\n");
		exit(1);
	}
	
	//create the named pipe used to get outbound data from
	//if you want to buffer more data in the pipe, just
	//give a bigger value for the buffer size for thepipe (the ntbufsize*16 values below).
	inpipeh = CreateNamedPipe("\\\\.\\pipe\\commandpipe",
						  PIPE_ACCESS_INBOUND,
						  PIPE_TYPE_BYTE|PIPE_READMODE_BYTE|PIPE_WAIT,
						  1,ntbufsize*32,ntbufsize*32,500,NULL);
	if(inpipeh==INVALID_HANDLE_VALUE)
	{
		printf("cannot create command pipe\r\n");
		printf("ERROR:%x",GetLastError());
		return (FALSE);
	}
	
	
	memset( &osw, 0, sizeof( OVERLAPPED ) ) ; //wipe the overlapped structure
	// create I/O event used for overlapped write
	osw.hEvent = CreateEvent( NULL,    // no security
		TRUE,    // explicit reset req
		FALSE,   // initial event reset
		NULL ) ; // no name
	if (osw.hEvent == NULL)
	{
		printf("Failed to create event for thread!\r\n");
		return ( FALSE ) ;
	}
	
	
	//wait for named pipe to be connected (ie there is a command pump going at
	//the other end of the pipe).
reconnect:
	if(ConnectNamedPipe(inpipeh,NULL)==0)
	{
		printf("command pipe connect failed:%8.8x\r\n",i);
		goto done;
	}
	if(reconnecting==1)
	{
		mask=0xFFFFFFFF;
		DeviceIoControl(wDevice,IOCTL_SFCDRV_IMMEDIATE_STATUS,&mask,sizeof(ULONG),&status,sizeof(ULONG),&t,NULL);
		if((status&ST_XDU)==ST_XDU)
		{
			//if we entered from a disconnect, we are discontinuous here!
			//if we entered from initial conditions, this should not happen!
			printf("XDU detected, prefilling\r\n");
			//act as if new, do prefill etc,etc
			xducount++;
		}
		else 
		{
			goto main_loop;//skip prefill, an XDU hasn't happened yet, (it is impending)
			//if we re-enter the main loop and get back to sending before
			//we get an XDU then all is well, otherwise we are toast
		}
		
	}
	//prefill the driver buffers
prefillit:
	printf("prefill...size:%d (%.2f ms)\r\n",maxtsize,((double)maxtsize*8.0*1000.0)/(double)freq);
	k=0;
	do
	{
		t = ReadFile(inpipeh,&prefillbuf[k],ntbufsize,&bytesread,NULL);//get the message
		if(t==FALSE)
		{
			i=GetLastError();
			printf("ReadFilePipe ERROR:%8.8x\n",i);
		}
		k+=bytesread;
	}while(k<maxtsize);
	//ok, here k is something larger than maxtsize (and presumably smaller than maxtsize+ntbufsize)
	//send out maxtsize of it, and move the rest into the regular data buffer
	for(i=0;i<k-maxtsize;i++)
	{
		data[i] = prefillbuf[i+maxtsize];
	}
	writesize = k-maxtsize;
	printf("initiating SFC write sequence (prefill)\r\n");
	
	mask=0xFFFFFFFF;
	DeviceIoControl(wDevice,IOCTL_SFCDRV_IMMEDIATE_STATUS,&mask,sizeof(ULONG),&status,sizeof(ULONG),&t,NULL);
	if((status&ST_XDU)==ST_XDU)
	{
		//if we entered from a disconnect, we are discontinuous here!
		//if we entered from initial conditions, this should not happen!
		printf("Transmit Underrun detected!\r\n");
		xducount++;
	}
	t = WriteFile(wDevice,&prefillbuf[0],maxtsize,&byteswritten,&osw);//send the frame
	if(t==FALSE) 
	{
		i=GetLastError();
		printf("SFC prefill write error:%8.8x\r\n",i);
		if(i==ERROR_IO_PENDING) GetOverlappedResult(wDevice,&osw,&nobyteswritten,TRUE);//this prevents badness on re-entry (broken/reconnected pipe)(or if there is allready data in the driver going out)
		//TODO:if reconnecting we need to determine how much data is still in the driver
		//if someone reconnects before we run out of data the prefill WriteFile() fails with 
		//(ERROR_IO_PENDING) and the next WriteFile in the loop will fail with ERROR_BUSY
		//this will suffice for now (waiting until this WriteFile() unblocks)
	}
	
	//enter main sending loop
main_loop:
	printf("\ntc: True Count - the number of times the write returns true (kind of the number of descriptors that were free.\n");
	printf("\npb: Pipe Bytes - number of bytes available in the named pipe at some time.\n");
	printf("\nst: Immediate Status - output of SFC STATUS command at some time.\n\n");
	do
	{
		//pull data from the pipe
		t = ReadFile(inpipeh,&data[writesize],ntbufsize,&bytesread,NULL);
		if(t==FALSE)
		{
			i=GetLastError();
			printf("ReadFilePipe Error:%8.8x\n",i);
			//likely a named pipe disconnect, go back to the top:
			printf("Data going discontinuous in approx %.2f mseconds\r\n",(((maxtsize-(ntbufsize))*8.0*1000.0)/(double)freq) );
			DisconnectNamedPipe(inpipeh);			//force the pipe closed
			reconnecting=1;
			goto reconnect;
		}
		writesize += bytesread;
		
		if(writesize>=(2*ntbufsize)) 
		{
			//when we get more than 2 buffers worth of data, send it on to the SuperFastcom driver
			t = WriteFile(wDevice,&data[0],2*ntbufsize,&byteswritten,&osw);//send the frame
			//if it returned FALSE then the IO request is queued (waiting for previous frame(s) to get done sending)
			//and we must wait until the os.event gets signaled before we try any more sending
			//enter wait for single object loop.
			//if loop times out in (writesize*8/bitrate) seconds then we are likely no longer transmitting...bummer
			
			if(t==FALSE)  
			{
				t=GetLastError();
				if(t==ERROR_IO_PENDING)
				{
					do
					{
						//debugging stuff
						PeekNamedPipe(inpipeh,NULL,0,NULL,&j,NULL);//get number of bytes in the pipe
						mask=0xFFFFFFFF;//get the status of the SFC port (the only thing we get here is XDU...and if we get it it is bad!
						DeviceIoControl(wDevice,IOCTL_SFCDRV_IMMEDIATE_STATUS,&mask,sizeof(ULONG),&status,sizeof(ULONG),&t,NULL);
						printf("tc:%3d pb:%10d st:%8.8x\n",truecount,j,status);
						if((status&ST_XDU)==ST_XDU)
						{
							printf("TRANSMIT UNDERRUN (XDU)!\n");
							printf("initiating a flush/restart\r\n");
							xducount++;
							DeviceIoControl(wDevice,IOCTL_SFCDRV_FLUSH_TX,NULL,0,NULL,0,&t,NULL);
							mask=0xFFFFFFFF;//get the status of the SFC port (the only thing we get here is XDU...and if we get it it is bad!
							DeviceIoControl(wDevice,IOCTL_SFCDRV_IMMEDIATE_STATUS,&mask,sizeof(ULONG),&status,sizeof(ULONG),&t,NULL);
							goto prefillit;
						}
						else if((status&0x100000)==0x100000)
						{
							printf("PCI controller DMA error\r\n");
							printf("initiating a flush/restart\r\n");
							xducount++;
							DeviceIoControl(wDevice,IOCTL_SFCDRV_FLUSH_TX,NULL,0,NULL,0,&t,NULL);
							mask=0xFFFFFFFF;//get the status of the SFC port (the only thing we get here is XDU...and if we get it it is bad!
							DeviceIoControl(wDevice,IOCTL_SFCDRV_IMMEDIATE_STATUS,&mask,sizeof(ULONG),&status,sizeof(ULONG),&t,NULL);
							goto prefillit;
						}
						truecount=0;
						//end debugging stuff
						j = WaitForSingleObject( osw.hEvent, ((((ntbufsize*4)*8)*1000)/freq)+timeout );//4 descriptor timeout, (if this much time has elapsed without unblocking, then either the transmitter is stopped, or something else bad has happened
						if(j==WAIT_TIMEOUT)
						{
							//DeviceIoControl(wDevice,IOCTL_SFCDRV_FLUSH_TX,NULL,0,NULL,0,&returnsize,NULL);
							//if we end up here then the output will go discontinuous in 
							//(n_tbufs-1)*ntfsize_max*8/bitrate seconds from now
							//recover from this how exactly? at least tell someone about it.
							//printf("Transmitter Timeout...Data going discontinuous in approx %.2f mseconds\r\n",(((maxtsize-(ntbufsize*4))*8.0*1000.0)/(double)freq)-(double)timeout );
							printf("SuperFastcom WriteFile Timeout\n");
							sfctxtimeout++;
							if(sfctxtimeout>100)
							{
								printf("SuperFastcom Transmitter not transmitting\r\n");
								printf("initiating a flush/restart\r\n");
								DeviceIoControl(wDevice,IOCTL_SFCDRV_FLUSH_TX,NULL,0,NULL,0,&t,NULL);
								mask=0xFFFFFFFF;//get the status of the SFC port (the only thing we get here is XDU...and if we get it it is bad!
								DeviceIoControl(wDevice,IOCTL_SFCDRV_IMMEDIATE_STATUS,&mask,sizeof(ULONG),&status,sizeof(ULONG),&t,NULL);
								goto prefillit;
							}
						}
						if(j==WAIT_ABANDONED)
						{
							printf("Write Abandoned.\r\n");
							goto done;
						}
					} while(j!=WAIT_OBJECT_0);
					GetOverlappedResult(wDevice,&osw,&nobyteswritten,TRUE);
				}
				else printf("SuperFastcom WriteFile ERROR: 0x%8.8x\n",t);
			}
			else 
			{
				//write returned true, driver buffers are not full!
				//monitor how unfull they are here, if continuously dropping then
				//we aren't entering this fast enough to keep up continuous data output
				truecount++;
			}
			//move the unwritten data down
			for(i=0;i<writesize-(2*ntbufsize);i++)
			{
				data[i] = data[i+(2*ntbufsize)];
			}
			writesize -= 2*ntbufsize;//we just sent this much, this is whats left
			totalbytestransmitted+=2*ntbufsize;
			sfctxtimeout=0;
		}
	}while(!kbhit());              //keep making requests until we want to terminate
	getch();//clear the keyboard buffer
done:
	DisconnectNamedPipe(inpipeh);	//force the pipe closed
	CloseHandle( osw.hEvent ) ;     //we are terminating so close the event
	CloseHandle(inpipeh);			//we are done with the pipe
	CloseHandle(wDevice);			//we are done with the SuperFastcom port
	printf("XDU count:%d\n",xducount);
	printf("total transmitted :%ld\n",totalbytestransmitted);
	printf("done\n\r");             //exit message
	return 0;
}
