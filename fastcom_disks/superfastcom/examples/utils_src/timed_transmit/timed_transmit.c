//only define one of these three

//if you define this (0A) you must have clock signal on both RT and ST!
//#define CLOCKMODE0A

//if you define this (0B) you must have a clock signal on RT!
//#define CLOCKMODE0B

//if you define this (7B) you do not need any external clock signals
#define CLOCKMODE7B

/* $Id$ */
/*
Copyright(c) 2004, Commtech, Inc.
timed_transmit.c -- generate 26 byte hdlc frames simulating a LN200 unit
				 -- operates with custom driver that only sends a single frame (descriptor)
				 -- on a TIN interrupt.
                                 -- requires driver sfcdrv.sys dated on/after 11/15/2004


  usage:
  timed_transmit port frequency bitrate [compensate]
  
	port is the SFC port to use
	frequency is the frame per second rate to use
	bitrate is the line bitrate to use
	compensate (optional) is the number of bit times to subtract from the timeout to compensate for ISR/latency  
						  (increasing this number will make the actual frame rate faster)

*/


#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "..\sfc.h"

struct LN_200_Data {
	signed short deltaV_x;
	signed short deltaV_y;
	signed short deltaV_z;
	signed short deltat_x;
	signed short deltat_y;
	signed short deltat_z;
	unsigned short IMU_status;
	unsigned short mode_bit;
	unsigned short mux_data;
	unsigned short raw_gyro[3];
	unsigned short checksum;
};



int main(int argc, char *argv[])
{
	HANDLE wDevice;/* Handle for the SuperFastcom port */
	ULONG t;
	DWORD nobyteswritten;
	char tdata[512];
	ULONG size;
	OVERLAPPED  wq;
	int j,tosend;
	ULONG totalsent;
	ULONG totalread;
	ULONG totalerror;
	ULONG loop;
    char devname[25];
	SCC_REGS settings;
	ULONG i;
	ULONG passval[4];
	ULONG descparm[4];
	ULONG startsend=0;
	ULONG val;
	char key;
	unsigned dvx,dvy,dvz,dtx,dty,dtz;
	struct LN_200_Data lndata;
	ULONG temp;
	ULONG bitrate;
	ULONG framerate;
	ULONG testrate;
	ULONG n,m;
	ULONG tval;
	ULONG cnt;
	ULONG compensate;
	double frametime;
	double bittime;
	ULONG out;
	ULONG reg;
	ULONG port;
	ULONG regmask;

	dvx = 0;
	dvy = 0;
	dvz = 0;
	dtx = 0;
	dty = 0;
	dtz = 0;
	
	memset(&lndata,0,sizeof(struct LN_200_Data));
	
	if(argc<4)
	{
		printf("usage:\n");
		printf("%s port frequency bitrate [compensate]\n",argv[0]);
		exit(1);
	}
	if(argc>=5) compensate= atol(argv[4]);
	else compensate=0;
	bitrate= atol(argv[3]);
	framerate = atol(argv[2]);
	if(bitrate==0)
	{
	printf("cannot have zero bitrate\n");
	exit(1);
	}
	if(framerate==0)
	{
	printf("cannot have zero framerate\n");
	exit(1);
	}
	port = atol(argv[1]);
    sprintf(devname,"\\\\.\\SFC%d",port);
	printf("devicename:%s\n",devname);
	memset(tdata,0,sizeof(tdata));
	
	memset( &wq, 0, sizeof( OVERLAPPED ) ) ;	//wipe the overlapped struct
	wq.hEvent = CreateEvent( NULL,    // no security
		TRUE,    // explicit reset req
		FALSE,   // initial event reset
		NULL ) ; // no name
	if (wq.hEvent == NULL)
	{
		printf("Failed to create event for thread!\n");
		return 1; 
	}
	//	printf("write overlapped event created\n");
	wDevice = CreateFile (devname,
		GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL,
		OPEN_ALWAYS,
		FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
		NULL
		);
    if (wDevice == INVALID_HANDLE_VALUE)
    {
		//for some reason the driver won't load or isn't loaded
		printf ("Can't get a handle to sfcdrv\n");
		exit(1);
		//abort and leave here!!!
	}
	//printf("sfcdrv handle created\n");



//use framerate and bitrate to determine clock setting and TIMR setting.
//note that once I was done I realized that it doesn't matter what the 
//inclk is set to, the timer runs off of the txclk.  So instead of multiplying
//up the input clock you could just use it directly (as long as it is in the 
//range 1-33E6 or (6-33E6);  It would simplify most of this stuff to do this.
//as you would just SET_FREQUENCY to be bitrate
//and skip down to the calculating of the TIMR register
if(0)
{
testrate=0;
i=0;
	while(testrate<=33333333)
	{
	i++;
	testrate= bitrate*i;
	}
i--;
if(i<=64)
{
//we can use it directly and set brr=i-1 and clock to testrate 
//we will then get the bitrate.
	
	testrate= bitrate*i;
	i=i-1;
	m=0;
	n=i;
}
else
{
//else we have to use rate={n+1}*2^m, which means we have to be %2.
testrate=0;
i=0;
	while(testrate<=33333333)
	{
	i=i+2;
	testrate= bitrate*i;
	}
i=i-2;
testrate = bitrate*i;

for(m=1;m<14;m++)
	{
	n = (i / (1<<m)) - 1;
	if(n<64) break;
	}
if(n>64) 
	{
	printf("can't get here from there\n");
	printf("can't get bitrate:%ld from inclk:%ld\n",bitrate,testrate);
	exit(1);
	}
}
}
else
{
testrate=bitrate;
m=0;
n=0;
}
//here testrate should be the inclk we need
//n and m should be good for the BRR register to get bitrate output
i = testrate/((n+1)*(1<<m));
printf("inclk:                        %ld\n",testrate);
printf("m:                            %ld\n",m);
printf("n:                            %ld\n",n);
printf("actual bitrate:               %ld\n",i);


//now to get frequency out of TIMR
//we are going to be in clock mode 7b, transmitting HDLC frames (transparent mode 0)
//so the timer source will be the BRR register.
frametime = 1.0/(double)(framerate);
bittime = 1.0/(double)(bitrate);

tval = (unsigned long)((frametime/bittime)-1.0);
cnt = 7;
if(tval>0xffffff)
{
printf("panic, timer value exceeds TIMR register\n");
exit(1);
}
tval = (cnt<<24) | tval;

printf("frametime:                    %.7f\n",frametime);
printf("bittime:                      %.7f\n",bittime);
printf("timervalue:                   %.7f\n",((tval&0xffffff)+1)*bittime);
printf("actual framerate:             %.2f\n",1.0/(((tval&0xffffff)+1)*bittime));
printf("compensated framert:          %.2f\n",1.0/((((tval-compensate)&0xffffff)+1)*bittime));
printf("TIMR:                         %8.8lx\n",tval);
tval=tval-compensate;

passval[0] = testrate;
passval[1] = 5;
	DeviceIoControl(wDevice,IOCTL_SFCDRV_SET_FREQUENCY,&passval[0],2*sizeof(ULONG),&out,sizeof(ULONG),&t,NULL);
	
	memset(&settings,0,sizeof(SCC_REGS));
	
	settings.cmdr = 0x01010000;
	settings.star = 0x0;
#ifdef CLOCKMODE0B
	settings.ccr0 = 0x80000030;//clock mode 0b

reg = 2;
DeviceIoControl(wDevice,IOCTL_SFCDRV_READ_LB,&reg,sizeof(unsigned long ),&val,sizeof(unsigned long),&t,NULL);

regmask = 0x03;
regmask = regmask<<(2*(port%4));
val=(val&(~regmask))|(0x01<<(2*(port%4)));//disable ST, enable TT
passval[0] = 2;
passval[1] = val;
DeviceIoControl(wDevice,IOCTL_SFCDRV_WRITE_LB,&passval,2*sizeof(unsigned long ),&val,sizeof(unsigned long),&t,NULL);

#endif
#ifdef CLOCKMODE0A
	settings.ccr0 = 0x80000000;//clock mode 0a
reg = 2;
DeviceIoControl(wDevice,IOCTL_SFCDRV_READ_LB,&reg,sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);
regmask = 0x03;
regmask = regmask<<(2*(port%4));
val=(val&(~regmask))|(0x02<<(2*(port%4)));//disable TT, enable ST
passval[0] = 2;
passval[1] = val;
DeviceIoControl(wDevice,IOCTL_SFCDRV_WRITE_LB,&passval,2*sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);

#endif
#ifdef CLOCKMODE7B
	settings.ccr0 = 0x80000037;//clock mode 7b
reg = 2;
DeviceIoControl(wDevice,IOCTL_SFCDRV_READ_LB,&reg,sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);
regmask = 0x03;
regmask = regmask<<(2*(port%4));
val=(val&(~regmask))|(0x01<<(2*(port%4)));//disable ST, enable TT
passval[0] = 2;
passval[1] = val;
DeviceIoControl(wDevice,IOCTL_SFCDRV_WRITE_LB,&passval,2*sizeof(unsigned long ),&val,sizeof(unsigned long),&t,NULL);
#endif
    settings.ccr1 = 0x02248000;
	settings.ccr2 = 0x00040000;//start with hdlc receiver OFF (no receive)
	settings.imr = 0xFFFE3FFB;//disable all but CSC interrupt sources , timer and xdu, and CDSC
	settings.timr = tval;//from above
	
/*
	//these all default to 0, on the memset above
	settings.accm = value;
	settings.udac = value;
	settings.tsax = value;
	settings.tsar = value;
	settings.pcmmtx = value;
	settings.pcmmrx = value;
	settings.brr = value;
	settings.xadr = value;
	settings.radr = value;
	settings.ramr = value;
	settings.rlcr = value;
	settings.xnxfr = value;
	settings.tcr = value;
	settings.ticr = value;

	settings.syncr = value;
	settings.isr = 0x0;
*/
	
	
	
	DeviceIoControl(wDevice,
		IOCTL_SFCDRV_SCC_REGISTERS_SET,         /* Device IOCONTROL */
		&settings,							/* write data */
		sizeof(settings)				,	/* write size */
		NULL,								/* read data */
		0,									/* read size */
		&t,								/* Returned Size */
		NULL);								/* overlap */

	i=1;//mask FI interrupts
	DeviceIoControl(wDevice,
		IOCTL_SFCDRV_SET_TX_FI_MASK,                            /* Device IOCONTROL */
		&i,								/* write data */
		sizeof(unsigned long ),						/* write size */
		NULL,								/* read data */
		0,									/* read size */
		&t,								/* Returned Size */
		NULL); /* overlap */

	//since we will be writing 400 frames per second, there will allways
	//be idle between frames, so we don't need any interrupts generated
	//to keep things going continuous...
	i=0; //minimum interrupts 
	DeviceIoControl(wDevice,
		IOCTL_SFCDRV_SET_TX_IRQ_RATE,                            /* Device IOCONTROL */
		&i,								/* write data */
		sizeof(unsigned long ),						/* write size */
		NULL,								/* read data */
		0,									/* read size */
		&t,								/* Returned Size */
		NULL); /* overlap */
//make sure that someone setup enough buffers
//we need at least a seconds worth, preferably more
//and the size of each must be at least as big as the biggest frame (up to 4k)
//if you are trying to timed transmit frames larger than 4k contact technical support
//as this driver is limited to sending a single descriptor per TIN and frames
//that span descriptors will break this scheme.

//note that I set the size to 128 because I know that this code won't ever use more than 28 bytes
//you can make it as big or as small as you need to (4 bytes -> 8191)(sizes from 4096-8191 may or may not work!)
//just make sure that you allocate enough of them to have ~ 1-2  seconds worth of buffering.

	//bad things happen if you do many allocs/deallocs on a single boot
	//so it is probably a good idea to only alloc these one time
	//it is much better to use the sfc_advanced_settings program and set the buffering parameters as boot 
	//settings, and remove this code!
	DeviceIoControl(wDevice,IOCTL_SFCDRV_GET_DESC_PARAMETERS,NULL,0,&descparm,4*sizeof(ULONG),&t,NULL);
	if((descparm[2]<1200)||(descparm[3]<128))
	{
		//note this re-alloc only happens if the minimums are violated!!
		//do not call this function on every execution or you will surely cause a BSOD!
		passval[0]= 5;//
		passval[1]= 32;//will only use 27 bytes of it, but has to be mult of 4
		passval[2]= 1200;
		passval[3]= 128;

		DeviceIoControl(wDevice,
			IOCTL_SFCDRV_CHNG_DESC_LEN,                         /* Device IOCONTROL */
			&passval[0],								/* write data */
			4*sizeof(unsigned long ),						/* write size */
			NULL,								/* read data */
			0,									/* read size */
			&t,								/* Returned Size */
			NULL); /* overlap */
	}
	//configure timed transmit
	passval[0] = 1;//timed transmit via TIN interrupt
	DeviceIoControl(wDevice,IOCTL_SFCDRV_SET_TIMED_TRANSMIT,&passval[0],sizeof(ULONG),NULL,0,&t,NULL);

	//start clean:
	DeviceIoControl(wDevice,IOCTL_SFCDRV_FLUSH_TX,NULL,0,NULL,0,&t,NULL);
	
	totalerror=0;
	totalsent=0;
	totalread=0;
	loop=0;
	size=26;	

//this starts up the transmit loop by initiating the timer (periodic)
startsend=1;//enable output, on first timeout after now
passval[0] = (0x100)+(0x80*(atoi(argv[1])%4));
passval[1] = 0x00000100;//start timer
DeviceIoControl(wDevice,IOCTL_SFCDRV_WRITE_REGISTER,&passval[0],2*sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);

top:
	while(!kbhit())
	{
		//insert your code here to fill in the transmit buffer data and size
		//(making sure you adjusted the bufs parameters above if the size grows
		//beyond 128 bytes)
		
		//fill in tdata[] with your data to send
		//set size to be the number of bytes to send
		//t = WriteFile(wDevice,&tdata,size,&nobyteswritten,&wq);

			tosend=size;
			lndata.raw_gyro[0]++; //debugging, put counter in gyro0, increments once per frame
			t = WriteFile(wDevice,&lndata,sizeof(lndata),&nobyteswritten,&wq);
			if(t==FALSE)  
			{
				t=GetLastError();
				if(t==ERROR_IO_PENDING)
				{
					do
					{
						j = WaitForSingleObject( wq.hEvent, 5000 );//5 second timeout -- must be larger than size*8*(1/bitrate)*1000
						if(j==WAIT_TIMEOUT)
						{
							printf("Transmitter Locked up\r\n");
							//DeviceIoControl(wDevice,IOCTL_SFCDRV_FLUSH_TX,NULL,0,NULL,0,&returnsize,NULL);
							//note the timeout should be larger than (1/framerate)*1000
							//since we are sending at a high rate of speed this 5 second timeout will 
							//indicate that something is very wrong
						}
						if(j==WAIT_ABANDONED)
						{
							printf("Abandoned.\r\n");
							goto close;
						}
					} while(j!=WAIT_OBJECT_0);
					GetOverlappedResult(wDevice,&wq,&nobyteswritten,TRUE);
				}
				else printf("WRITE ERROR: #%d, :0x%8.8x\n",t,t);
			}
			if(nobyteswritten!=size)
			{
				printf("unexpected actual:%lu, sent:%lu\n",nobyteswritten,size); 
			}
			loop++;
			totalsent+=nobyteswritten;
	}
//this is all specific to the LN200 simulation
//keypresses will increment values in the frame structure for the LN200
//subsequent frame writes will have the update/new values
//pressing [ESC] will exit the program
	key = getch();
	if(key=='x')
	{
		dvx++;
		printf("deltav_x++ : %u\r\n",dvx);
	}
	if(key=='y')
	{
		dvy++;
		printf("deltav_y++ : %u\r\n",dvy);
	}
	if(key=='z')
	{
		dvz++;
		printf("deltav_z++ : %u\r\n",dvz);
	}
	if(key=='t') 
	{
		dtx++;
		dty++;
		dtz++;
		printf("deltat++: %u\r\n",dtx);
	}
	
	lndata.deltaV_x = dvx;
	lndata.deltaV_y = dvy;
	lndata.deltaV_z = dvz;
	lndata.deltat_x = dtx;
	lndata.deltat_y = dty;
	lndata.deltat_z = dtz;
	lndata.IMU_status = 0;
	lndata.mode_bit = 0;
	lndata.mux_data = 0;
	lndata.checksum = dvx+dvy+dvz+dtx+dty+dtz;
	//lndata.raw_gyro[0] = 0;
	lndata.raw_gyro[1] = 0;
	lndata.raw_gyro[2] = 0;
	
	
	if(key!=27) goto top;
	
	printf("count %lu\n",loop);
	DeviceIoControl(wDevice,IOCTL_SFCDRV_FLUSH_TX,NULL,0,NULL,0,&t,NULL);
close:
	passval[0] = (0x130)+(0x80*(atoi(argv[1])%4));
	passval[1] = tval;//stop timer by writing it
	DeviceIoControl(wDevice,IOCTL_SFCDRV_WRITE_REGISTER,&passval,2*sizeof(unsigned long ),&val,sizeof(unsigned long),&temp,NULL);

	//turn off timed transmit
	passval[0] = 0;//no timed transmit
	DeviceIoControl(wDevice,IOCTL_SFCDRV_SET_TIMED_TRANSMIT,&passval[0],sizeof(ULONG),NULL,0,&t,NULL);
	
	CloseHandle(wq.hEvent);
	CloseHandle (wDevice);
	return 0;
}
/* $Id$ */
