To get this example to work, I had port 1 connected to port 2 via a crossover 
cable (SD->RD, TT->RT).

open a console window and execute:

setbufs 0 10 1024 100 4096
setbufs 1 300 4096 10 1024

(the actual numbers/buffersizes used here are up to you, these are just the ones
I ended up using)
(if either of these programs (buffer setting) returns an error,
reboot and start over before proceeding)


setclock 0 10000000
sfcset 1 extset0	//See Note 1 Below
sfcset 0 extset
sfcset 1 extset

At this point it is good to go.

I opened two console windows and executed:

write_random 0 10000 wout

in one window and:

read 1 750000 out

in the other.

Let it run for a bit, then stop the read window (hit a key). Then stop the
transmitter (hit a key).

I was able to do this a couple of times, I verified a file that was about 5megs
and one that was about 27megs.

The load on my machine was noticable, it was running at 80/20 privilaged/user time
with an interrupt rate around 500-700.

Please note that the machine that I was doing this on is a 233MHz MMX pentium
with 64M of ram.  A more well endowed machine will probably not even notice that
this is going on...

To verify the files a couple of utilities were generated (ts_comp,shifter,disp_bin)
The out file may or may not be the same as wout, since more than likely the
two programs aren't started at the same time, the beginning points in the file
will be different, but more importantly the receiver will not do any kind of byte
alignment in the mode we are using, so the entire stream could very well be shifted by 0-7 bit positions
from the output stream.  (see the accompanying readme.txt file).

If you run the shifter program on the read output file as:

shifter 1 out out1
shifter 2 out out2
shifter 3 out out3
shifter 4 out out4
shifter 5 out out5
shifter 6 out out6
shifter 7 out out7

Then one of the files out,out1,out2,out3,out4,out5,out6,out7 will compare with
the wout file successfully.
The ts_comp program can be used to verify the data, it scans the input file for
the framecounter, and then scans the wout file for the same counter value, then
compares the rest of the bytes in the file.
You should expect that the last couple of bytes will fail, as they are artifacts
of the shifter program.
one of:

ts_comp out wout
ts_comp out1 wout
ts_comp out2 wout
ts_comp out3 wout
ts_comp out4 wout
ts_comp out5 wout
ts_comp out6 wout
ts_comp out7 wout

should successfully compare the bulk of the saved files, and indicate only
the handfull of errors at the end of the file.

The read program will dump data read from a port using the given blocksize
for the reads, and save the data to the specified file.
(if you make the block size small, and the bitrate fast, you will need to
remove all the printfs from this program)


The write_random program will generate a random bitstream with a 12 byte
framecounter placed every (blocksize) bytes in the file. (the 12 byte sequence
is 0xff,0x00,8 digit ascii counter,0x00,0xff).  It outputs this stream to the
given superfastcom port, and optionally also to a specified file.
(if you make the blocksize small and the bitrate fast, and the system is
loaded too much you will not end up with a continuous output, which you
will see as long strings of 0xff's in the received file) increasing the blocksize
should take care of this.

The disp_bin program dumps a binary file to the screen as hex digits,
25 bytes to a row.

The lsb2msb program can be used to change the bit order of a file from lsb to
msb first.  This code might be necessary if you expect your data to be msb first
 on the line.
(as the 82532/20534 receives and transmits data lsb first)

The shifter implements an algorithm to shift a lsb received data stream by the
given number of bits.

The ts_comp program searches the input file for the 12 byte framecounter sequence
(as per write_random) and when it finds a likely suspect, it then looks for that
framecount in the compare file, and then compares the rest of the input file
to the data in the compare file.

The extset0 file is the settings file to setup extended transparent mode, it
sets up clock mode 1 which is necessary to get the 20534 to start generating
interrupts/receiving data.

The extset file is the settings file to setup extended transparent mode,
it sets up clock mode 0b which is the desired mode to work with (external receive
clock, generated transmit clock).  You may need to modify this file to setup
for clock mode 0a, if you end up using an external transmit clock.  You will
also have to either use the writelb program or sfc_advanced_Settings to enable
the st->txclk connection for an external transmit clock). (or if you only have
a single external clock, just use the extset0 settings for clock mode 1)

Note1: The PEB20534 has a quirk/bug in it that adds an extra step to setting up a 
receiver in extended transparent mode.  There are two ways to get around this quirk:

  a) In this example, we initialize the receive port in clock mode 1 (with a clock
  present on the RT pins) and then setting it to use your preferred clock mode.

  b) If you cannot connect a clock to the RT pins, you can alternatively use this method.
  Connect the RTS and DCD pins together.  With a transmit clock present, set bits 
  CCR1:RTS=1 & CCR1:FRTS=0.  Then set the same bits CCR1:RTS=1 & CCR1:FRTS=1.  This 
  will create a low to high transition on the CD pin using RTS, and will enable the 
  receiver in extended transparent mode.  For this to work properly, you must use
  use writereg.exe to change the RTS anc FRTS registers, and not sfcset.  Sfcset.exe
  will issue a reset receiver which will break the process.

  In this example, you could use:
    writereg 1 18c 0274e000	//make RTS low on port 2
    writereg 1 18c 027ce000	//make RTS high on port 2
  to enable the receiver.


Note2: You can reduce the load on the PC by using the settfi/setrfi/setrirq/settirq
utilities and setting IMR to 0xFFFFFFFF, effectively reducing the interrupt
rate to something that the machine can tolerate.



