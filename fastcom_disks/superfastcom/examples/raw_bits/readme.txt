The difficulty in using extended transparent mode without a data sync signal,
is that you have no idea where valid receive data starts and stops.  You must 
shift through the bit stream until you find valid data.  Once you have found 
valid data and noted the correct bit alignment, if the data reception remains 
constant, you should be able to shift all incoming data by the correct number 
of bits and you will then have correct data.

-------------------------Begin LSB to LSB------------------------------------

This example is sending out of a device that does LSB first and receiving with an SFC.

Lets start by sending the following message: 0x515253 this is in binary (MSB):
01010001 01010010 01010011

and will get sent out the card as (the LSB is transmitted first):
10001010 01001010 11001010 

now the idle is 1's so the message will be received (depending on how many 1's are 
clocked in prior to the actual data) the possibilities are:

11111111 10001010 01001010 11001010 11111111
1111111 10001010 01001010 11001010 111111111
111111 10001010 01001010 11001010 1111111111
11111 10001010 01001010 11001010 11111111111
1111 10001010 01001010 11001010 111111111111
111 10001010 01001010 11001010 1111111111111
11 10001010 01001010 11001010 11111111111111
1 10001010 01001010 11001010 111111111111111

re-aligning this to byte boundaries:

11111111 10001010 01001010 11001010 11111111
11111111 00010100 10010101 10010101 11111111
11111110 00101001 00101011 00101011 11111111
11111100 01010010 01010110 01010111 11111111
11111000 10100100 10101100 10101111 11111111
11110001 01001001 01011001 01011111 11111111
11100010 10010010 10110010 10111111 11111111
11000101 00100101 01100101 01111111 11111111

remembering that the LSB is on the left, changing it to "normal"(MSB) gives
possible received messages:

11111111 01010001 01010010 01010011 11111111 = 0xff515253ff - NO SHIFT
11111111 00101000 10101001 10101001 11111111 = 0xff28a9a9ff - SHIFTED by 1
01111111 10010100 11010100 11010100 11111111 = 0x7f94d4d4ff - SHIFTED by 2
00111111 01001010 01101010 11101010 11111111 = 0x3f4a6aeaff - SHIFTED by 3
00011111 00100101 00110101 11110101 11111111 = 0x1f2535f5ff - SHIFTED by 4
10001111 10010010 10011010 11111010 11111111 = 0x8f929afaff - SHIFTED by 5
01000111 01001001 01001101 11111101 11111111 = 0x47494dfdff - SHIFTED by 6
10100011 10100100 10100110 11111110 11111111 = 0xa3a4a5feff - SHIFTED by 7

They are all representative of the original 0x515253 that was sent, however only 1 of 
them directly shows that upon receive without shifting.

And once you determine how many bits you need to shift to get valid data,
everything that you read in will need to be shifted by that number of bits
until you stop transmitting data.  If you stop transmitting, then you have
to do the same thing over again once you start transmitting data again.

This can be avoided by utilizing a receive data strobe in an appropriate clock 
mode.  If the transmitting device can supply a strobe signal that activates at 
the beginning of the of the data and deactivates at the end, the receiver will 
only be activated during the active phase of this signal, and hopefully the 
data will have the correct alignment.

-------------------------END LSB to LSB--------------------------------------

-------------------------Begin MSB to LSB------------------------------------
This example is sending out of a device that does MSB first and receiving with an SFC.

Lets start by sending the following message: 0x515253 this is in binary (MSB):
01010001 01010010 01010011

now the idle is 1's so the message will be received (depending on how many 1's are 
clocked in prior to the actual data) the possibilities are:

11111111 01010001 01010010 01010011 11111111
1111111 01010001 01010010 01010011 111111111
111111 01010001 01010010 01010011 1111111111
11111 01010001 01010010 01010011 11111111111
1111 01010001 01010010 01010011 111111111111
111 01010001 01010010 01010011 1111111111111
11 01010001 01010010 01010011 11111111111111
1 01010001 01010010 01010011 111111111111111

re-aligning this to byte boundaries:

11111111 01010001 01010010 01010011 11111111
11111110 10100010 10100100 10100111 11111111
11111101 01000101 01001001 01001111 11111111
11111010 10001010 10010010 10011111 11111111
11110101 00010101 00100101 00111111 11111111
11101010 00101010 01001010 01111111 11111111
11010100 01010100 10010100 11111111 11111111
10101000 10101001 00101001 11111111 11111111

remembering that the LSB is on the left, changing it to "normal"(MSB) gives
possible received messages:

11111111 10001010 01001010 11001010 11111111 = 0xff8a4acaff
01111111 01000101 00100101 11100101 11111111 = 0x7f4525e5ff
10111111 10100010 10010010 11110010 11111111 = 0xbfa292f2ff
01011111 01010001 01001001 11111001 11111111 = 0x5f5149f9ff
10101111 10101000 10100100 11111100 11111111 = 0xafa8a4fcff
01010111 01010100 01010010 11111110 11111111 = 0x575452feff
00101011 00101010 00101001 11111111 11111111 = 0x2b2a29ffff
00010101 10010101 10010100 11111111 11111111 = 0x159594ffff

They are all representative of the original 0x515253 that was sent, however only 1 of 
them directly shows that upon receive without shifting.

And once you determine how many bits you need to shift to get valid data,
everything that you read in will need to be shifted by that number of bits
until you stop transmitting data.  If you stop transmitting, then you have
to do the same thing over again once you start transmitting data again.

This can be avoided by utilizing a receive data strobe in an appropriate clock 
mode.  If the transmitting device can supply a strobe signal that activates at 
the beginning of the of the data and deactivates at the end, the receiver will 
only be activated during the active phase of this signal, and hopefully the 
data will have the correct alignment.

-------------------------END MSB to LSB--------------------------------------
Is this all clear as mud now?
