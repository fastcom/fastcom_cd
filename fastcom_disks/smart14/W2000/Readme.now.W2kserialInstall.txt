Windows 2000 installation procedure for Fastcom(tm) ISA serial boards.  

Note:  The w2kserialinstall file on this disk is a Microsoft Word format of this document.  

1.  run the add device wizard (start->controlpanel->add/remove hardware
2.  click Next
3.  Select add/troubleshoot device, click next.
4.  Wait for plugnplay search to finish, select Add New Device, click next.
5.  Select No, select from list, click Next.
6.  Select standard port types->communications port, click Next.
7.  Click OK to close the ! dialog box warning you that you must enter
    settings manually.
8.  Select "basic configuration 0008".
9.  Double click the Input/Output range, enter 0280-0287, click OK.
10. Double click the Interrupt request, select 05, click OK.
11. Click OK to close the resources dialog.
12. Click Next to get to the finish dialog.
13. Click Finish to exit the wizard.
14. Select No to the restart computer messagebox.
15. Repeat steps 1-14 for each port on the card.
    for the Fastcom:232/4-ISA card the addresses are:
    0280-0287  ->first port
    0288-028E  ->second port
    0290-0297  ->third port
    0298-029E  ->fourth port
    All of the Interrupt requests should be set to the same value ( 05 in the example)
    and should match the interrupt switch setting on the board.
16. Reboot your PC.
17. Test the ports using hyperterm or the tty example program, using a loopback
    connector, and connecting to each of the new ports.

3/27/00



