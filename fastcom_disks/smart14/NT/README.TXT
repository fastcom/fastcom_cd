1/3/97 Serial.sys  4:53p 64128 bytes
modifications:

The Serial.sys file included is the DDK serial comm driver modified to do the 
following.  The enableing of the fifo now includes the steps to enable the 64 
byte fifo on the 16C750 UART.  The Trigger levels can be set in the same way 
as on the 16550.  (Using the RxFIFO value in the parameters section of the 
registery, valid values are 1,4,8,14 they correspond to 1,16,32,56 on the 750)

The following registry entries are now valid:

Auto485 REG_DWORD = 0 or 1
Hispeed REG_DWORD = 0 or 1
Fifo82510 REG_DWORD = 0 or 1

These parameters should only be added if the Uart is a 82510.  
The Auto485 parameter will set the 82510 in it's auto RTS mode where the RTS 
line will be active while the uart is transmitting data, and inactive when it 
is not.  In order for this to operate correctly the port must be opened with 
the DCB RTS setting set to dcb.fRtsControl = RTS_CONTROL_DISABLE.  This is 
due to the fact that the 82510 will only do the automatic switching if RTS 
is inactive to start with.
The Hispeed parameter will set the 82510 so that it's input clock is 9216000 
instead of 1843200.  This effectively gives a X5 on all baud rates, so if you
set the port at 57.6K you will get 288K, 38.4K = 192K, etc.  The software will
not recognize a parameter of 288K as far as it knows it is setting a 16450 Uart
up to run at 57.6K baud, the hardware however is running at 5X what the software
expects.
The Fifo82510 parameter will enable the 4 byte fifo depth on the 82510. 

All of these parameters will be enabled when the REG_DWORD = 1, and disabled
if the REG_DWORD = 0 or if it isn't present in the registry. 

If you change a parameter from 1 to 0 the system must be rebooted for it to
take effect.  

A typical registry entry for a fastcom:422 board at address 280h interrupt 5, 
running at 5X baud rates, using 485 mode, with it's fifo's enabled would be:

HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Serial\Parameters\Serial10000

Auto485:REG_DWORD:0x1
DosDevices:REG_SZ:COM3
Fifo82510:REG_DWORD:0x1
ForceFifoEnable:REG_DWORD:0
Hispeed:REG_DWORD:0x1
Interrupt:REG_DWORD:0x5
PortAddress:REG_DWORD:0x280

This will set up the Fastcom:422 to be identified as com3, at address 0x280, 
interrupt 5, The 82510 4 byte fifo will be enabled.  The baud rates used will
be 5 times what is selected, and the RTS line will frame the transmitted data.

If you use this serial.sys driver and you have a 82510 Uart board (a 
FASTCOM:422, FASTCOM:422/4, or FASTCOM:IG422) you must have at least 1 of the
parameters set (Fifo82510 = 1), If not the port will be unusable, due to the 
fact that the 16750 FIFO enable code will cause a bank switch on the 82510.  
This is not a problem with the original serial.sys driver as it does not cause
a bank switch (meaning if you use the original driver the 82510 will act as a 
16450 and operate normally).  It is also not a problem if one of the above   
parameters are set, as the code that initializes any of the above 82510 special
functions bypasses the 16550 fifo setup code.


