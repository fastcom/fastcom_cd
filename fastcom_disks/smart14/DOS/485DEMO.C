// 485DEMO.C  An example of using SMART14(tm) to send/receive data using
//            a RS485 link, with SMART14(tm) configured to 485 mode.
//
//        When SMART14(tm) is in its 485 mode the port that is configured
//        for RS485 will respond in the following way:
//
//              the first call to _bios_serialcom(_COM_SEND,port,data)
//              will disable the receiver for that port and enable the
//              RTS line (to enable the transmitter).  The receiver for
//              the specified port will remain disabled until the
//              transmitter has completed sending the data and
//              a call to _bios_serialcom(_COM_STATUS,port,0) has been made
//              the return value for _bios_serialcom(_COM_STATUS,port,0)
//              will indicate if the receiver is on or off as follows
//
//              returnvalue&0x8000 == 0      --->   receiver on
//              returnvalue&0x8000 == 0x8000 --->   receiver off
//
//              When used in this manner the following proceedure should be
//              used:
//              send data using:
//              status = _bios_serialcom(_COM_SEND,port,data);
//              wait for transmitter to be complete by using:
//              (this will also turn the receiver back on when the transmitter
//              is done.)
//              while((_bios_serialcom(_COM_STATUS,port,0)&0x8000)>0);
//              wait for received data:
//              while((_bios_serialcom(_COM_STATUS,port,0)&0x100)==0);
//              receive data:
//              status = _bios_serialcom(_COM_RECEIVE,port,0);
//              data = status&0xff;
//              status = status>>8;

//Written by: Carl George
//Date: 6-10-93
//Copyright (c) 1993 COMMTECH,INC Wichita, KS
//
#include "bios.h"
#define COM1 0
main(){
unsigned status,i;
char data[25];
//Initialize the communications port (COM1) to 9600 baud N,8,1
_bios_serialcom(_COM_INIT,COM1,_COM_9600|_COM_NOPARITY|_COM_STOP1|_COM_CHR8);
//get a message to send
printf("Enter a message to send:\n\r");
gets(data);

printf("Sending :\n\r%s\n\r",data);
i = 0;
//send the data using _bios_serialcom(),
//note: when this function is called with SMART14(tm) installed for 485 mode
//      the first transmit call will disable the receiver on the uart to
//      prevent filling the receive buffer with the data that is being
//      transmitted.  The receiver will remain disabled until _bios_serialcom()
//      is called with the _COM_STATUS function AND the transmitter is idle
//      (ie the transmit buffer is empty and the uart is done sending data)

//transmit until at end of the data string
while(data[i]!=0){
//send the byte
status = _bios_serialcom(_COM_SEND,COM1,data[i]);
//do next byte
i++;
//unless the status indicates a timeout , then back up and do the last byte again
if((status&0x8000)>0) i--;
}
//this loop is necessary to turn the receiver back on, we wait here until
//the transmitter is done transmitting the receiver is then turned on
//and we continue to wait until a data byte is received (data ready flag set
// in the status reg).

do{
status = _bios_serialcom(_COM_STATUS,COM1,0);
}while((status&0x8000)>0);
printf("Receiver on\n\r");
printf("Waiting for reply:\n\r");
//wait for data to be ready
do{
status = _bios_serialcom(_COM_STATUS,COM1,0);
}while((status&0x100)==0);

i = 0;
//now we just start receiving data until we receive a termination character 
// ('0')
do{
//do the receive function
status = _bios_serialcom(_COM_RECEIVE,COM1,0);
//if a timeout was indicated then the data is invalid so skip it
if((status&0x8000)==0){
			data[i] = status&0xff;
			printf("%c",data[i]);
			i++;
			}
}while(data[i-1]!=0x30);
}

