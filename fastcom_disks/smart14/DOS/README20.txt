Smart14(tm) for DOS v 2.0 Features and Enhancements

Rev 2.0 has all the features of v 1.4 with more control over port
parameters.  The Install Program has been revamped to allow multi-board
installation.  The new install program will allow system ports to be at 
COM3 and COM4 as well as COM1 and COM2.  Handshaking, Baudswapping, Xon-Xoff,
and buffer sizes are available on a port by port basis.  The new opening
banner when smart14 is run displays which options are enabled for which ports
as follows:


IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII¯
§      F A S T C O M : SMART14(tm)           §
§              Version 2.0                   §
§ COPYRIGHT (C) 1992,1993 COMMTECH, INC.     §
§                                            §
§ COM01:3f8 H F B X R  0400 0400 82510       §
§ COM02:2f8 H                    16450       §
§ IRQ: 4;                                    §
§ XON:11  XOFF:13                            §
EIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII¬


 COM01:3f8 H F B X R  0400 0400 82510       
   ^
   
The COMxx is the Bios COM port number, the :xxx is the hex base address of 
the port.  

 COM01:3f8 H F B X R  0400 0400 82510       
           ^ ^ ^ ^ ^

The next 5 letters are enabled if shown and disabled if blank.
H == Handshaking enabled
F == FIFO enabled
B == Baudswapping enabled
X == Xon/Xoff enabled
R == RS485 auto RTS enabled

 COM01:3f8 H F B X R  0400 0400 82510       
                       ^    ^
The next two numbers are the buffer sizes, the first is the receive buffer size
in hex.  The second is the transmitt buffer size in hex.

 COM01:3f8 H F B X R  0400 0400 82510       
                                 ^
The last bit of information is the UART type for the port, the options are
82510, 16450, 16550, 16750.  

 IRQ: 4;                                    

The IRQ: line displays which IRQ lines are connected to the SMART14 buffering 
program (displayed in hex).  

 XON:11  XOFF:13                            

The last line displays the global values used for xon and xoff in hex (if enabled).


So for the above example, COM1 is at base address 3f8 Handshaking, FIFO, 
baudswapping, xon/xoff, and RS485 control are all enabled, the receive buffer
is 1024 bytes the transmit buffer is 1024 bytes and the UART is an intel 82510.
COM2 is at base address 2f8, handshaking is enabled and the UART type is a
16450.  Interrupt 4 is hooked.  XON is 17 and XOF is 19.

Please Note that not all combinations are valid, for instance selecting
handshaking and RS485 auto RTS control at the same time will not work.
(specifically in this case the handshaking is ignored).  

The Baudswapping feature is dependant on the UART type in rev 2.0.  Previous
revisions of Smart14 assumed that all ports were of the same UART type.  With
the multiboard version there are 3 possible baud tables.  The standard baud 
table, 16xxx baudswapped and 82510 baudswapped.  The Standard table is used
for all port types if Baudswapping is not enabled. If the port type is a 
16450, 16550 , or a 16750 and baudswapping is enabled the 16xxx baud table
is used.  If the port type is a 82510 and baudswapping is enabled the baud
table is the 82510 baudswapped table.

STANDARD:
bios baud rate  |       actual baud rate        | physical divisor
-------------------------------------------------------------------
9600            |       9600                    |       12
4800            |       4800                    |       24
2400            |       2400                    |       48
1200            |       1200                    |       96
600             |       600                     |       192
300             |       300                     |       384
150             |       150                     |       768
110             |       110                     |       1047

16xxx:
bios baud rate  |       actual baud rate        | physical divisor
-------------------------------------------------------------------
9600            |       9600                    |       12
4800            |       4800                    |       24
2400            |       2400                    |       48
1200            |       1200                    |       96
600             |       115.2K                  |       1
300             |       56.7K                   |       2
150             |       38.4K                   |       3
110             |       19.2K                   |       6

82510:
bios baud rate  |       actual baud rate        | physical divisor
-------------------------------------------------------------------
9600            |       9600                    |       60
4800            |       4800                    |       120
2400            |       2400                    |       240
1200            |       1200                    |       480
600             |       288K                    |       2
300             |       56.7K                   |       10
150             |       38.4K                   |       15
110             |       19.2K                   |       30

There are numerous benefits to having port by port control over these 
parameters, most importantly it allows somewhat different boards to be 
configured and used in the same system, (like a Fastcom:422 on COM3 and 4
and a Fastcom:8 on COM5-12) utilizing the features of both the 82510 and the
16c550.  Also since the buffer sizes are selectable it is possible to have 
Smart14 take up less of the system's ram as it is possible to use much less
than the maximum 64K that previous versions of Smart14 used.

New Stuff on the DISK
There is lots of new stuff on the disk, specifically:
\win  -> smart14 for windows and it's example program Wterm
\NT   -> text file on how to set up FASTCOM boards in Windows NT and 16750 
         enabled version of serial.sys
\W95  -> text file on how to set up FASTCOM boards in Windows 95
\win32-> win32 (windows 95 and Windows NT) example programs (from the win32 SDK)
\win31-> windows 3.1 replacement comm.drv for 9 ports and a utility to modify
         system.ini comm port parameters. (this comm.drv is 16750 enabled)
         example program (from win3.1 SDK) using Microsoft COMM API.


