#include "bios.h"
#include "dos.h"
#include "conio.h"
// test.c       a generic program to show functions 8 and 9 of SMART14(tm)
// 8-1-94       Carl George
// Copyright (c) 1994  COMMTECH, INC Wichita, KS
//
void main(){
unsigned i,j;
unsigned buff[25];
//will return actual UART LSR and MSR
i = bioscom(0,0xe3,2);
printf("status returned from init:%x\n\r",i);
//will return virtual LSR and B0h for MSR if buffering is enabled
//or if disabled will return LSR/MSR from uart
i = bioscom(3,0x00,2);
printf("status returned from status:%x\n\r",i);
// if the function number passed to int14 is 8 the return value (in ax) will
// be the number of bytes in the buffer for that port
i = bioscom(8,0x00,2);
printf("bytes in buffer:%u\n\r",i);
//setup for function 9 
asm        push         es
asm        push         ds
asm        pop          es
asm        lea     bx,buff
//if function number 9 is passed to int 14 then it will store the following
//information at ES:BX
//      ES:BX  ->       port address
//      ES:BX+2->       buffersize max
//      ES:BX+4->       buffered flag (0 = not buffered, 1 = tx, 2 = rx, 3 = both)
//      ES:BX+6->       segment of buffer
//      ES:BX+8->       offset of buffer
//      ES:BX+10->      input pointer (receive)
//      ES:BX+12->      ouput pointer (receive)
bioscom(9,0,2);
//cleanup for function 9
asm     pop     es
//display pert info
printf("port #2: base addres    = %xh\n\r",buff[0]);
printf("port #2: max buffersize = %u\n\r",buff[1]); 
if((buff[2]&1)==1)printf("port #2: is tx buffered\n\r");
if((buff[2]&2)==2)printf("port #2: is rx buffered\n\r");
printf("port #2: buffer segment = %xh\n\r",buff[3]);
printf("port #2: buffer offset  = %xh\n\r",buff[4]);
printf("port #2: input pointer  = %u\n\r",buff[5]);
printf("port #2: output pointer = %u\n\r",buff[6]);


}
