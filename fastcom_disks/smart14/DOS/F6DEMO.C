#include "dos.h"
#include "bios.h"
#include "conio.h"
#include "stdio.h"
#include "malloc.h"
#include "ctype.h"
#include "graph.h"
#define ESC        27

//      f6demo.c
//      by: Carl George
//      completed: 11/4/93
//      COPYRIGHT (c) 1993 COMMTECH,INC. WICHITA, KS
//      
//      f6demo.c illustrates the new function 6 of SMART14(TM)
//      this test program is a modification of the MSTERM.C 
//      program and just does a simple terminal emulation
//      using the _get_receive_buffer() (defined in this code)
//      to get the received characters in blocks instead of one
//      at a time as _bios_serialcom(RECEIVE...) does
//      
//      _get_receive_buffer() takes the unsigned port number and a pointer
//      to a far or huge character array as arguments, it will then copy
//      the internal receive buffer of smart14 into the char array and
//      return the number of bytes transfered.  
//      when copying this function into your code you must have the include
//      files for _bios_serialcom() as it will call this function
//      
//
//############################################################################





unsigned get_receive_buffer(unsigned port,char huge *buffer);
char huge *inbuf;
unsigned count;
unsigned size;
main(){

inbuf = _fmalloc(7500);      //allocate memory for getting buffer data
			     //must be a far pointer or huge pointer
			     //using max size of buffer
			     //fastcom4w : 7500 bytes
			     //fastcom422: 30000 bytes
			     //fastcom422/2: 15000 bytes

if (inbuf==NULL){ printf("cannot allocate memory for buffer\n\r");
		  exit(1);
		  }

term();

_ffree(inbuf);
}



term()
{
	int ch, port,i;
	_clearscreen(_GCLEARSCREEN);
	_settextposition(1,22);
	printf("SMART14 SIMPLE TERMINAL DEMONSTRATION");
	_settextposition(4,10);
	printf("ENTER PORT NUMBER TO TEST:");
	_settextposition(5,10);
	printf("(NOTE: PORT 0 = COM1)");
	_settextposition(4,38);

	port = (getche() - '0');  /*CORRECT FOR ASCII*/

	_settextposition(7,10);
	printf("DEFAULT PARAMETERS: 1200,E,7,1");
	_settextposition(8,10);
	printf("TERMINAL RUNNING, PRESS 'ESC' TO QUIT \n");

/*Initialize selected port to 1200,E,7,1 */
_bios_serialcom(_COM_INIT,port,_COM_1200|_COM_EVENPARITY|_COM_CHR7|_COM_STOP1);   /* initialize the port */

	while (ch != ESC)
	{
		if (_bios_keybrd(_KEYBRD_READY) != 0)
		{
			ch = _bios_keybrd(_KEYBRD_READ);
			_bios_serialcom(_COM_SEND,port,ch);     /* transmit the character */
			ch = (ch & 0X00FF);                     /* Delete scan code in upper byte */
			if (ch == 13)                           /* carriage return*/
				printf("\n");
		}
		// get all the bytes from the receive buffer on port
		count = get_receive_buffer(port,inbuf);
		// and display them on the screen
		for(i=0;i<count;i++){
					printf("%c",inbuf[i]);
				     }

	}
}

unsigned get_receive_buffer(unsigned port,char huge *buffer){
unsigned count;
		//setup ES:BX for smart14 function 6
_asm            push    ES;
_asm            les  bx,buffer;
		count = _bios_serialcom(6,port,0);
_asm            pop     ES;
return(count);
}


