 Smart14(tm) for DOS  (DO NOT USE WITH WINDOWS VERSION!!!!!!)

rev 1.4 of SMART14(tm):

FEATURES:
function 9:     returns internal status of port
function 9:     before call to int14; ah = 9, dx= port, ES:BX = buffer
		returns:        ES:BX      port address
				ES:BX+2    max buffer size
				ES:BX+4    buffer flag (0=none,1=tx,2=rx,3=both)
				ES:BX+6    segment of buffer
				ES:BX+8    offset of buffer
				ES:BX+10   input pointer(receive)
				ES:BX+12   output pointer(receive)
function 8:             returns the number of bytes received on a port
function 8:     before call to int14; ah = 8, dx = port
		returns:
				ax = number of bytes in rxbuffer

function 7:     returns an indication of if a carriage return or line feed has been received
		    (calling this function will clear the status)
function 7:     before call to int14; ah = 7, dx = port
		returns:
				ax = CR-LF status
				ax = 0 no CR or LF in rbuf
				ax = 1 CR in buf no LF
				ax = 2 LF in buf no CR
				ax = 3 CR and LF in buf

function 6:     returns the entire contents of receive buffer to user location
function 6:     before call to int14; ah =6, dx = port, ES:BX = buffer
		returns:        ax = number of bytes transfered
				ES:BX = all bytes from receive buffer


function 5:     clear buffer contents (tx and rx) for a port
function 5:     before call to int14; ah =5, dx = port
		returns:                nothing

function 4:     returns buffer remaining (bytes)
function 4:     before call to int14; ah =4, dx = port
		returns:                ax = buffer remaining

function 3:     port status function
function 3:     before call to int14; ah =3, dx = port
		returns:                ax = status
								ah = LSR        line status (errors/timouts/data ready)
								al = MSR        modem status (input signals, cts/ri/dsr/dcd)

function 2:     receive the next character from a buffer
function 2:     before call to int14; ah =2, dx = port
		returns:                al = received byte
								ah = status indicator (errors/timout/data ready)

function 1:     transmit a byte
function 1:     before call to int14; ah =1, dx = port, al = byte to tx
		returns:                ah = status, (errors)

function 0:     initialize a port
function 0:     before call to int14; ah =0, dx = port, al = init params
		returns                 ax = status (same as function 3)
				
release notes:
Version 2.0  Install program and Smart14 modified to allow multiboard
	     configurations and mixed configurations (each port has control
	     over handshaking,baudswapping, etc.) 16C750 support added for
	     the Fastcom:4W-GT and the Fastcom:422-GT.

Version 1.4  adds functions 7->9, enabling indication of CR/LF pairs in the
	     received data stream simplifying communications that send blocks 
	     of data separated by CR/LF, just call function 7 until it returns
	     with CR/LF set and then call function 6 to get the block of data.
	     also added an indication of the # bytes in the buffer with function 8.
Version 1.1b includes the new function 6 to SMART14(tm); enabling
	     block mode transfers of the receive buffer to a C program.
	     Look at F6DEMO.C for an explanation of how to take advantage
	     of this new function.
	     Also, initializes the uarts correctly for the Fastcom:8.
	     The install program has been updated so that multiple
	     ports can take advantage of the 485 mode of SMART14(tm).

Version 1.1a includes the automatic rts control for 82510 uarts in 485 mode
	     to enable/disable the drivers.  It also displays the installed
	     parameters in the program banner when it is first loaded.

Version 1.1  Bug fix release. Fixed the problem of the system locking up
	     when two or more ports were receiving data at the same time.

Version 1.0  Initial release.

the file 510DEMO.C has been added to the distribution disk, it includes
example functions for direct register level programming of the 82510 uart
for use with the FASTCOM:422 and FASTCOM:422/4 boards.  The source includes
functions for initializing the uart, transmitting a character, receiving a 
character, enabling the uart fifo, enabling automatic RTS control of drivers
for 485 mode, enabling the high speed mode of the baud rate generator, 
and switching between the different banks of registers on the uart.  This 
source is not intended to be used with SMART14.EXE, but rather is included
to guide engineers that need to access the uart directly, and are using their
own register level programming.
the file 485DEMO.c has been added to demonstrate the new features of SMART14
automatic handling of the driver enable (RTS) and disabling the receiver
while transmitting data.
the file F6DEMO.C has been added to demonstrate the get_receive_buffer() 
function that allows an entire receive buffer to be transmitted at one time.


cg 7/31/95

