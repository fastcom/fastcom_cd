/*
Ser_init.c -- an example of how to initialize a serial port (qnx)

created: 6/5/2001
author:  cg


*/
#include "termios.h"
#include "stdio.h"
#include "stdlib.h"
#include "conio.h"
#include "string.h"
#include "math.h"
#include "fcntl.h"

void main(int argc,char *argv[])
{
struct termios tios; //structure for serial port parameters
int commfd = -1;     //handle(file descriptor) for com port
long rate=0;		 //baud rate to set
long bits;			 //number of data bits to set
long parity;		 //parity to use
long stopbits;		 //number of stop bits to use

if(argc < 5)
{
printf("usage:\r\n");
printf("ser_init device speed parity databits stopbits\r\n");
exit(1);
}
commfd = open(argv[1],O_RDWR);	//get a handle to the com port
if(commfd<0)					//if unable to open port then dump out
{
printf("cannot open port %s\r\n",argv[1]);
exit(1);
} 
rate = atol(argv[2]);			//convert the bitrate from the command line
if(rate==0) rate = 115200;		//check if valid, default to 115200bps if doesn't convert
bits = atol(argv[4]);			//get #data bits from command line
if(bits==0) bits = 8;			//if doesn't convert default to 8 bits
if(bits==8) bits = CS8;			//change #bits to #define value to pass to termios structure
else if(bits==7) bits = CS7;	//change #bits to #define value to pass to termios structure
else if(bits==6) bits = CS6;	//change #bits to #define value to pass to termios structure
else if(bits==5) bits = CS5;	//change #bits to #define value to pass to termios structure
else bits=CS8;					//change #bits to #define value to pass to termios structure
parity =-1;
if((argv[3][0]=='N')||(argv[3][0]=='n')) parity = 0;			   //get parity from command line					(No parity)
if((argv[3][0]=='O')||(argv[3][0]=='o')) parity = PARENB|PARODD;   //and set with correct values for termios struct (Odd parity)
if((argv[3][0]=='E')||(argv[3][0]=='e')) parity = PARENB;														//  (Even Parity)
if((argv[3][0]=='S')||(argv[3][0]=='s')) parity = PARENB|PARSTK;												//	(stick parity???)
if(parity==-1) parity = 0; //default to No parity if doesn't convert off of the command line ie if it is not (N,n,O,o,E,e,S,s)

stopbits=atol(argv[5]);				//get stopbits off of command line
if(stopbits==0) stopbits =1;		//default to 1 if doesn't convert
if(stopbits==2) stopbits = CSTOPB;	//set to 2 if given as 2
else stopbits = 0;					//default to 1 if anything else other than 2

printf("setting %s to %ldbps ",argv[1],rate);//give up a message showing what we are doing

if(parity==0) printf("N,");
if(parity==(PARENB|PARODD)) printf("O,");
if(parity==PARENB) printf("E,");
if(parity==(PARENB|PARSTK))printf("S,");
if(bits==CS8) printf("8,");
if(bits==CS7) printf("7,");
if(bits==CS6) printf("6,");
if(bits==CS5) printf("5,");
if(stopbits==0) printf("1\r\n");
if(stopbits==CSTOPB) printf("2\r\n");


tcgetattr(commfd,&tios);			//get current serial port settings
tios.c_ispeed = rate;				//set the input speed
tios.c_ospeed = rate;				//set the output speed
tios.c_iflag = IGNBRK|PARMRK|INPCK;	//set the input flags to ignore break, mark parity errors and enable input parity checking
									//note that these parity settings are for the terminal not the serial port, so 
									//it is possible to have parity enabled (have a parity line setting) and have the 
									//driver ignore the results of the parity check done by hardware.
									//the setting here lets the driver do the parity checking, presumably it is ignored if the No parity option
									//is selected for the hardware.
tios.c_oflag = 0;					//don't enable any of output modes (possible to change CR/LF handling here as well as tab expansion)
tios.c_cflag = bits|stopbits|parity|CREAD|CLOCAL;	//note CLOCAL ignores modem lines
													//this is the setting for the hardware line, (UART line control register)
tios.c_lflag &= ~(ICANON|ISIG|ECHO);	//don't want canonical(line) mode, don't generate signals and turn local echo off 
tios.c_cc[VTIME] = 10;					//check every second
tios.c_cc[VMIN] = 0;					//vmin=0,vtim>0 will return a read every vtime*.1seconds with or without data as the case may be
tcsetattr(commfd,TCSANOW,&tios);		//enable the new settings
close(commfd);							//done with the serial port


}