/*
Fileout.c -- a qnx example for outputing data to a com port

			(essentially the same as 'cat file >/dev/serX')

created: 6/5/2001
author:  cg

*/

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"
#include "conio.h"
#include "fcntl.h"
#include "unistd.h"

void main( int argc, char *argv[])
{
int commfd=-1;	//file descriptor for com port
int fin=-1;		//file descriptor for file to read data from
char buf[257];	//buffer to transfer data 
long rsz=256;	//read size (from file)
long wsz=0;		//write size (to com port)

if(argc<3) 
{
printf("usage:\n");
printf("fileout device filetosend\n");
exit(1);
}

commfd = open(argv[1],O_RDWR);//open handle to com port
if(commfd==-1) //if port doesn't open the dump out
{
printf("cannot open device :%s\n",argv[1]);
exit(1);
}
fin = open(argv[2],O_RDONLY);//open file to read data from
if(fin==-1) //if file doesn't open the exit
{
printf("cannot open file to send :%s\n",argv[2]);
exit(1);
}

while(rsz!=0)//keep doing this loop as long as we read data out of the file
{
rsz = read(fin,buf,256);	//read up to 256 bytes out of file
wsz = write(commfd,buf,rsz);//write the bytes read from the file out the com port
//you could check wsz here to make sure that it is = rsz if it isn't then
//some sort of write error occured on the com port
//(out of buffer space, timeout, etc.)
}

//could also do a fsync or something similar to make sure that all of 
//the bytes went out the serial port before exiting

close(fin);			//done with the input file
close(commfd);		//done with the com port
}