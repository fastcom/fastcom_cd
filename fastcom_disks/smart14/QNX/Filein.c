/*
Filein.c -- a qnx example for receiving data from a com port

created: 6/5/2001
author:  cg



*/

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"
#include "conio.h"
#include "fcntl.h"
#include "unistd.h"

void main( int argc, char *argv[])
{
int commfd=-1; //file descriptor for com port
int fout=-1;   //file descriptor for file to write to
char buf[257]; //buffer for data transfer
long rsz=256;  //read size from port
long wsz=0;    //write size to file
long noreccount=0;//how many receives get no bytes back (timeouts)

if(argc<3) 
{
printf("usage:\n");
printf("fileout device filetowrite\n");
exit(1);
}

commfd = open(argv[1],O_RDWR);//open the com port 
if(commfd==-1) //if -1 then the port didn't open
{
printf("cannot open device :%s\n",argv[1]);
exit(1); //no point in continuing without an open comport
}
fout = open(argv[2],O_RDWR|O_CREAT);//create/open the file to dump data into
if(fout==-1)//if it doesn't open we might as well leave
{
printf("cannot open file to write :%s\n",argv[2]);
printf("errno:%d\n",errno);
exit(1);
}

while(1) //main loop
{
rsz = read(commfd,buf,256);//read up to 256 bytes from the com port 
						   //if the port was setup with the ser_init
						   //program then the port will return somewhere
						   //between 0 and 256 bytes depending on if
					       //the driver has physically received any bytes
						   //and the timeout value (VTIME) has not been 
						   //exceeded.  ser_init sets the timeout to 1 second
						   //so if nothing is received in 1 second then this
						   //call will return with rsz=0
						   //if anything is received then it will be returned
						   //immediately to us.
if(rsz==0)
{
noreccount++;			   //increment our timeout counter
if(noreccount>5) goto end; //if we get 5 seconds of timeouts in a row then
						   //we drop from the loop
}
else 
{			
noreccount=0;			   //otherwise we reset our timeout counter and go again
wsz = write(fout,buf,rsz); //write the received data to the file
}

}

end:
close(fout);				//we are done with the file
close(commfd);				//and done with the com port
}