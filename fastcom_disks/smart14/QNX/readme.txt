This is the QNX serial examples for the Fastcom:422/4-104 card.

To get QNX to recognize the ports simply run:

Dev.ser base,irq base+8,irq base+16,irq base+24,irq &

you may do this either from the startup init file, or from the command line after qnx is up and running.

For example if the board is set to base address 0x280 and using IRQ 5 the
command would be:

Dev.ser -t 8 280,5 288,5 290,5 298,5 &

Once this is done there will be 4 more /dev/serX devices.

You can initialize the ports to different line parameters using the ser_init program, its basic format is:

ser_init device baudrate parity databits stopbits

so to set the 3rd serial port of the system to 115.2Kbps using
no parity 8 data bits and one stop bit you would execute:

ser_init /dev/ser3 115200 N 8 1

The filein.c and fileout.c examples are simple examples showing how to open and read or write to the device.

fileout is similar to doing a 'cat file >/dev/serX'
The command line format for fileout is:

fileout device filetosend

filein will take what is received on a port and dump it to the specified file until 5 seconds has elapsed with no data coming in at which point it closes both the device file and the output file and exits.
The format for filein is:

filein device filetosave

It is also possible to use the built in qnx terminal program to do loopback type testing on the port.  A command such as:

qtalk -m/dev/ser3 -b115200,n,8,1

would open a terminal session on the third serial port using no parity, 8 data bits, and one stop bit at 115200 bits per second.


cg
6/5/2001


