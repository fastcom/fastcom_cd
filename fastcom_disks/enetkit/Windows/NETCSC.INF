; NETCSC.INF
;
; Crystal Seminconductor Ethernet Network Interface Cards.
;
; Copyright 1997, Crystal Semiconductor Corporation

[version]
signature="$Windows 95$"
Class=Net
provider=%V_CRYSTAL%

[Manufacturer]
%V_CRYSTAL%=CRYSTAL
%V_IBM%=IBM

[CRYSTAL]
%pnpCS8920.DeviceDesc%=pnpCS8920.ndi, ISAPNP\CSC6040    ; Crystal Semiconductor CS8920 in PnP Mode
%*CS8920.DeviceDesc%=*CS8920.ndi, *CSC6040              ; Crystal Semiconductor CS8920 in non PnP Mode
%*CS8900.DeviceDesc%=*CS8900.ndi, *CSC0040              ; Crystal Semiconductor CS8900

[IBM]
%pnpIBM1010.DeviceDesc%=pnpIBM1010.ndi, ISAPNP\IBM1010  ; IBM EtherJet Adapter
%*IBM1010.DeviceDesc%=*IBM1010.ndi, *IBM1010            ; IBM EtherJet Adapter in non PnP Mode

;****************************************************************************
; CS8920 ISA Adapter in PnP Mode
;****************************************************************************
[pnpCS8920.ndi]
AddReg=pnpCS8920.ndi.reg,cs89xx.ndi.reg,cs89xx.params,cs8920.extra.params

[pnpCS8920.ndi.reg]
HKR,Ndi,DeviceID,,"ISAPNP\CSC6040"
HKR,NDI,CardType,,"ISAPNP"

;****************************************************************************
; IBM EtherJet in PnP Mode
;****************************************************************************
[pnpIBM1010.ndi]
AddReg=pnpIBM1010.ndi.reg,cs89xx.ndi.reg,cs89xx.params,cs8920.extra.params

[pnpIBM1010.ndi.reg]
HKR,Ndi,DeviceID,,"ISAPNP\IBM1010"
HKR,NDI,CardType,,"ISAPNP"

;****************************************************************************
; CS8920 ISA Adapter in non PnP Mode
;****************************************************************************
[*CS8920.ndi]
AddReg=*CS8920.ndi.reg,cs89xx.ndi.reg,cs89xx.params,cs8920.extra.params
LogConfig=*CS8920.LogConfig

[*CS8920.ndi.reg]
HKR,Ndi,DeviceID,,"*CSC6040"

[*CS8920.LogConfig]
ConfigPriority=HARDRECONFIG
IOConfig=10@200-3FF%FFF0(FFFF::)
IRQConfig=3,4,5,6,7,8,9,10,11,12,14,15
;MemConfig=1000@B0000-EFFFF%FFFFF000

;****************************************************************************
; IBM EtherJet in non PnP Mode
;****************************************************************************
[*IBM1010.ndi]
AddReg=*IBM1010.ndi.reg,cs89xx.ndi.reg,cs89xx.params
LogConfig=*CS8920.LogConfig

[*IBM1010.ndi.reg]
HKR,Ndi,DeviceID,,"*IBM1010"

;****************************************************************************
; CS8900 ISA Adapter in Legacy Mode
;****************************************************************************
[*CS8900.ndi]
AddReg=*CS8900.ndi.reg,cs89xx.ndi.reg,cs89xx.params,cs8900.extra.params
LogConfig=*CS8900.LogConfig

[*CS8900.ndi.reg]
HKR,Ndi,DeviceID,,"*CSC0040"

[*CS8900.LogConfig]
ConfigPriority=HARDRECONFIG
IOConfig=10@200-3FF%FFF0(FFFF::)
IRQConfig=5,10,11,12
;MemConfig=1000@B0000-EFFFF%FFFFE000

;****************************************************************************
; CS89xx base section
;****************************************************************************
[cs89xx.ndi.reg]
HKR,,DevLoader,,*ndis
HKR,,DeviceVxDs,,ends3isa.sys
HKR,,EnumPropPages,,"netdi.dll,EnumPropPages"
; NDIS Info
HKR,NDIS,LogDriverName,,"ENDS3ISA"
HKR,NDIS,MajorNdisVersion,1,03
HKR,NDIS,MinorNdisVersion,1,0A
HKR,NDIS\NDIS2,DriverName,,"ENDSXX$"
HKR,NDIS\NDIS2,FileName,,"ends2isa.dos"
HKR,NDIS\ODI,DriverName,,"EODIISA"
HKR,NDIS\ODI,FileName,,"eodiisa.com"
; Interfaces
HKR,Ndi\Interfaces,DefUpper,,"ndis3"
HKR,Ndi\Interfaces,DefLower,,"ethernet"
HKR,Ndi\Interfaces,UpperRange,,"ndis3,odi"
;HKR,Ndi\Interfaces,UpperRange,,"ndis3,ndis2,odi"
HKR,Ndi\Interfaces,LowerRange,,"ethernet"
; Install sections
HKR,Ndi\Install,ndis3,,"cs89xx.ndis3"
;HKR,Ndi\Install,ndis2,,"cs89xx.ndis2"
HKR,Ndi\Install,odi,,"cs89xx.odi"
;; Fix ISAPNP problem
;;HKLM,System\CurrentControlSet\Services\VxD\ISAPNP,RDPOverRide,3,74,02


[cs89xx.params]

; Netaddress - Locally Administered Address
;
HKR,Ndi\params\NetworkAddress,ParamDesc,,"NetworkAddress"
HKR,Ndi\params\NetworkAddress,flag,1,20,00,00,00
HKR,Ndi\params\NetworkAddress,type,,edit
HKR,Ndi\params\NetworkAddress,LimitText,,12
HKR,Ndi\params\NetworkAddress,UpperCase,,1
HKR,Ndi\params\NetworkAddress,default,,""
HKR,Ndi\params\NetworkAddress,optional,,1

; Serial - Obtain it from LANAid.
;          Enter it for Multi cards installation.
;
HKR,Ndi\params\Serial,ParamDesc,,"SerialNumber"
HKR,Ndi\params\Serial,type,,edit
HKR,Ndi\params\Serial,LimitText,,8
HKR,Ndi\params\Serial,UpperCase,,1
HKR,Ndi\params\Serial,default,,""
HKR,Ndi\params\Serial,optional,,1
HKR,Ndi\params\Serial,flag,0,30,00,00,00

[cs8920.extra.params]
; DuplexMode
; - this is a NDIS3 only parameter
;
HKR,Ndi\params\DuplexMode,ParamDesc,,"DuplexMode"
HKR,Ndi\params\DuplexMode,flag,1,20,00,00,00
HKR,Ndi\params\DuplexMode,type,,enum
HKR,Ndi\params\DuplexMode,default,,0
HKR,Ndi\params\DuplexMode\enum,0,,"Auto Negotiate"
HKR,Ndi\params\DuplexMode\enum,1,,"Half Duplex"
HKR,Ndi\params\DuplexMode\enum,2,,"Full Duplex"

[cs8900.extra.params]
; DuplexMode
; - this is a NDIS3 only parameter
;
HKR,Ndi\params\DuplexMode,ParamDesc,,"DuplexMode"
HKR,Ndi\params\DuplexMode,flag,1,20,00,00,00
HKR,Ndi\params\DuplexMode,type,,enum
HKR,Ndi\params\DuplexMode,default,,1
HKR,Ndi\params\DuplexMode\enum,1,,"Half Duplex"
HKR,Ndi\params\DuplexMode\enum,2,,"Full Duplex"

; Install NDIS3
;
[cs89xx.ndis3]
CopyFiles=cs89xx.ndis3.CopyFiles,cs89xx.CopyInf

[cs89xx.ndis3.CopyFiles]
ends3isa.sys

[cs89xx.CopyInf]
netcsc.inf

; Install NDIS2
;
[cs89xx.ndis2]
CopyFiles=cs89xx.ndis2.CopyFiles,cs89xx.CopyInf

[cs89xx.ndis2.CopyFiles]
ends2isa.dos

; Install ODI
;
[cs89xx.odi]
CopyFiles=cs89xx.odi.CopyFiles,cs89xx.CopyInf

[cs89xx.odi.CopyFiles]
eodiisa.com

;****************************************************************************
; Destination Directories
;****************************************************************************
[DestinationDirs]
DefaultDestDir              =11 ; LDID_SYS
cs89xx.ndis3.CopyFiles      =11 ; LDID_SYS
cs89xx.ndis2.CopyFiles      =26 ; LDID_WINBOOT
cs89xx.CopyInf              =17 ; LDID_INF

;****************************************************************************
; Source Disk information
;****************************************************************************
[SourceDisksNames]
1=,,

[SourceDiskFiles]
netcsc.inf=1
ends3isa.sys=1
ends2isa.dos=1
eodiisa.com=1


;****************************************************************************
; Localizable Strings
;****************************************************************************
[strings]
; Provider and manufacturer
V_CRYSTAL="Crystal Semiconductor"
V_IBM="IBM"

; PNP Devices

*CS8900.DeviceDesc=   "Crystal LAN(tm) CS8900 Ethernet Adapter"
*CS8920.DeviceDesc=   "Crystal LAN(tm) CS8920 Ethernet Adapter"
pnpCS8920.DeviceDesc= "Crystal LAN(tm) CS8920 Ethernet Adapter (PnP mode)"
pnpIBM1010.DeviceDesc="IBM EtherJet Adapter (PnP Mode)"
*IBM1010.DeviceDesc=  "IBM EtherJet Adapter"
