NDIS4_CE_v326.zip
      4/2002 : 74 Kb
      CS8900A NDIS4 Ethernet driver for Windows CE v2.x and v3.x, embedded Win9x and NT. Source code only.

NDIS4_CEv4.x_V100.zip
      5/2004 : 58 Kb
      CS8900A NDIS4 Ethernet driver for CE.net v4.x. Source code only.

CS8900A_NDISDriver_CE5X.zip
      7/2005 : 63 Kb
      CS8900A NDIS Ethernet driver for Windows CE.Net version 5.x. Source code only.
