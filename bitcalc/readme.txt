INSTALLATION

To install BitCalc execute setup.exe from File Manager or click "file, 
"run..." and type a:setup

NOTE: You must turn off Virus Checker to install Bitcalc.

PREVIOUSLY INSTALLED

You may copy BitCalc3.exe from this diskette to your previously installed 
bitcalc directory, replacing an older version of bitcalc3.exe.

HISTORY

5/30/95 version 3.0a

improved the search path for infoscrn and bc_jed

8/29/95 version 3.0b

changed max freq for 2062V from 120 to 165.

8/31/95 version 3.0c

recompiled with older grid.vbx 4-28-93 

4/30/96 version 3.0d

changed max ref freq for 2062M and 2062V from 60 to 25 MHz.

5/7/96 version 3.0e

added reserve bit to first line of ouput display on 2051B.
the binary word and hex word were correct and not changed.

5/16/96 program is still version 3.0e

added text about CyClocks to the 2291 help and info files.
