FASTCOM(R) CD
Technical Documents
Software Drivers
Catalog

Copyright Information:

Copyright (c) 2000 Commtech, Inc.  All rights reserved.  The software and
information on this CD may be used by our customers free of charge only in
conjunction with our FASTCOM(R) adapters unless other arrangements have
been made with us in writing.  FASTCOM is a registered trademark of Commtech, Inc.

Commtech, Inc.
9011 E. 37th St. N
Wichita, KS   67226


Running this CD:

Install this CD in your CD drive and it will start automatically in most machines.
Otherwise, from your desktop, double click on the �My Computer� icon.  Double
click on the CD-ROM drive icon (usually D: or E:).  Double click on the 
FASTCOM_CD icon.

Manuals:

Before you install the FASTCOM board in your computer, read and print the 
reference manual that applies to your FASTCOM product.  That manual contains 
important configuration and installation information for your FASTCOM adapter.  

Data Sheets:

The UART Data Sheets (FASTCOM Async adapters) and the ESCC Data Sheet
(FASTCOM Sync adapters) are quite long, so you may wish to print only those 
sections that are useful to you.

Frequently Asked Questions:

Before calling for technical support, look through the FAQ section of this disk for
information that may be of assistance to you.  Open the FAQ section by going to
the FAQ directory (use Windows Explorer) and double click on the FAQ icon
(use WordPad).  



